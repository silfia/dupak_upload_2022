<?php
namespace App\Http\Libraries;
use App\Models\PakMasterKegiatan;
use App\Models\MasterType;
use App\Models\Pegawai;
use Auth;

class Formatters
{
	public static function convert_romawo($angka)
	{
		$hasil = "";
		if ($angka < 1 || $angka > 5000) {
			$hasil .= "Batas Angka 1 s/d 5000";
		}else{
			while ($angka >= 1000) {
				$hasil .= "M";
				$angka = $angka - 1000;
			}
		}

		if ($angka >= 500) {
			if ($angka > 500) {
				if ($angka >= 900) {
					$hasil .= "CM";
					$angka = $angka - 900;
				} else {
					$hasil .= "D";
					$angka = $angka - 500;
				}
			}
		}

		while ($angka >= 100) {
			if ($angka >= 400) {
				$hasil .= "CD";
				$angka = $angka - 400;
			} else {
				$angka = $angka - 100;
			}
		}

		if ($angka >= 50) {
			if ($angka >= 90) {
				$hasil .= "XC";
				$angka = $angka - 90;
			} else {
				$hasil .= "L";
				$angka = $angka - 50;
			}
		}

		while ($angka >= 10) {
			if ($angka >= 40) {
				$hasil .= "XL";
				$angka = $angka - 40;
			} else {
				$hasil .= "X";
				$angka = $angka - 10;
			}
		}

		if ($angka >= 5) {
			if ($angka == 9) {
				$hasil .= "IX";
				$angka = $angka - 9;
			} else {
				$hasil .= "V";
				$angka = $angka - 5;
			}
		}

		while ($angka >= 1) {
			if ($angka == 4) {
				$hasil .= "IV";
				$angka = $angka - 4;
			} else {
				$hasil .= "I";
				$angka = $angka - 1;
			}
		}
		return $hasil;
	}

	public static function get_bulan($bln){
		switch ($bln){
			case 1 :
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}

	public static function getBln($bln)
	{
		switch ($bln){
			case 1 :
				return "Jan";
				break;
			case 2:
				return "Peb";
				break;
			case 3:
				return "Mart";
				break;
			case 4:
				return "Aprl";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Jun";
				break;
			case 7:
				return "Jul";
				break;
			case 8:
				return "Agst";
				break;
			case 9:
				return "Sept";
				break;
			case 10:
				return "Okt";
				break;
			case 11:
				return "Nop";
				break;
			case 12:
				return "Des";
				break;
		}
	}

	public static function get_tanggal_lengkap($tgl)
	{
		$tanggal = date('d', strtotime($tgl));
		$bulan = Formatters::get_bulan(date('m', strtotime($tgl)));
		$tahun = date('Y', strtotime($tgl));

		return $tanggal.' '.$bulan.' '.$tahun;
	}

	public static function get_butir($id_profesi)
	{
		$butir = [];
		$no = 0;
		// $pegawai = Pegawai::find($id_pegawai);
		$unsur = PakMasterKegiatan::select('master_type.*')
																->join('master_type','master_type.id_master_type','pak_master_kegiatan.unsur_pak_id')
																->where('master_type.modul','SatuanUnsur')
																->where('pak_master_kegiatan.formasi_id', $id_profesi)
																->where('pak_master_kegiatan.jenis','Butir')
																->where('pak_master_kegiatan.level','1')
																->where('pak_master_kegiatan.bl_state', 'A')
																->orderBy('master_type.urutan','ASC')->distinct()->get();
		if (count($unsur) > 0) {
			foreach ($unsur as $uns) {
				$butir[$no] = $uns;
				$lv1 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																	->where('unsur_pak_id', $uns->id_master_type)
																	->where('jenis', 'Butir')
																	->where('level', '1')
																	->where('bl_state', 'A')
																	->get();
				if (count($lv1) > 0) {
					$butir1 = [];
					$noLv1 = 0;
					foreach ($lv1 as $key1) {
						$butir1[$noLv1] = $key1;
						$lv2 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																			->where('unsur_pak_id', $uns->id_master_type)
																			->where('jenis', 'Butir')
																			->where('level', '2')
																			->where('bl_state', 'A')
																			->where('parent_id', $key1->id_pak_master)
																			->get();
						if (count($lv2) > 0) {
							$butir2 = [];
							$noLv2 = 0;
							foreach ($lv2 as $key2) {
								$butir2[$noLv2] = $key2;
								$lv3 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																					->where('unsur_pak_id', $uns->id_master_type)
																					->where('jenis', 'Butir')
																					->where('level', '3')
																					->where('bl_state', 'A')
																					->where('parent_id', $key2->id_pak_master)
																					->get();
								if (count($lv3) > 0) {
									$butir3 = [];
									$noLv3 = 0;
									foreach ($lv3 as $key3) {
										$butir3[$noLv3] = $key3;
										$lv4 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																							->where('unsur_pak_id', $uns->id_master_type)
																							->where('jenis', 'Butir')
																							->where('level', '4')
																							->where('bl_state', 'A')
																							->where('parent_id', $key3->id_pak_master)
																							->get();
										if (count($lv4) > 0) {
											$butir4 = [];
											$noLv4 = 0;
											foreach ($lv4 as $key4) {
												$butir4[$noLv4] = $key4;
												$lv5 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																									->where('unsur_pak_id', $uns->id_master_type)
																									->where('jenis', 'Butir')
																									->where('level', '5')
																									->where('bl_state', 'A')
																									->where('parent_id', $key4->id_pak_master)
																									->get();
												if (count($lv5) > 0) {
													$butir5 = [];
													$noLv5 = 0;
													foreach ($lv5 as $key5) {
														$butir5[$noLv5] = $key5;
														$lv6 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																											->where('unsur_pak_id', $uns->id_master_type)
																											->where('jenis', 'Butir')
																											->where('level', '6')
																											->where('bl_state', 'A')
																											->where('parent_id', $key5->id_pak_master)
																											->get();
														if (count($lv6) > 0) {
															$butir6 = [];
															$noLv6 = 0;
															foreach ($lv6 as $key6) {
																$butir6[$noLv6] = $key6;
																if ($key6->is_title == '1') {
																	$butir6[$noLv6]['statusRow'] = 'Parent';
																}else{
																	$butir6[$noLv6]['statusRow'] = 'Child';
																	$butir6[$noLv6]['nama_jabatan'] = $key6->jabatan->nama;
																}
																$butir6[$noLv6]['row'] = [];
																$noLv6++;
															}
															$butir5[$noLv5]['statusRow'] = 'Parent';
															$butir5[$noLv5]['row'] = $butir6;
														}else{
															if ($key5->is_title == '1') {
																$butir5[$noLv5]['statusRow'] = 'Parent';
															}else{
																$butir5[$noLv5]['statusRow'] = 'Child';
																$butir5[$noLv5]['nama_jabatan'] = $key5->jabatan->nama;
															}
															$butir5[$noLv5]['row'] = [];
														}
														$noLv5++;
													}
													$butir4[$noLv4]['statusRow'] = 'Parent';
													$butir4[$noLv4]['row'] = $butir5;
												}else{
													if ($key4->is_title == '1') {
														$butir4[$noLv4]['statusRow'] = 'Parent';
													}else{
														$butir4[$noLv4]['statusRow'] = 'Child';
														$butir4[$noLv4]['nama_jabatan'] = $key4->jabatan->nama;
													}
													$butir4[$noLv4]['row'] = [];
												}
												$noLv4++;
											}
											$butir3[$noLv3]['statusRow'] = 'Parent';
											$butir3[$noLv3]['row'] = $butir4;
										}else{
											if ($key3->is_title == '1') {
												$butir3[$noLv3]['statusRow'] = 'Parent';
											}else{
												$butir3[$noLv3]['statusRow'] = 'Child';
												$butir3[$noLv3]['nama_jabatan'] = $key3->jabatan->nama;
											}
											$butir3[$noLv3]['row'] = [];
										}
										$noLv3++;
									}
									$butir2[$noLv2]['statusRow'] = 'Parent';
									$butir2[$noLv2]['row'] = $butir3;
								}else{
									if ($key2->is_title == '1') {
										$butir2[$noLv2]['statusRow'] = 'Parent';
									}else{
										$butir2[$noLv2]['statusRow'] = 'Child';
										$butir2[$noLv2]['nama_jabatan'] = $key2->jabatan->nama;
									}
									$butir2[$noLv2]['row'] = [];
								}
								$noLv2++;
							}
							$butir1[$noLv1]['statusRow'] = 'Parent';
							$butir1[$noLv1]['row'] = $butir2;
						}else{
							if ($key1->is_title == '1') {
								$butir1[$noLv1]['statusRow'] = 'Parent';
							}else{
								$butir1[$noLv1]['statusRow'] = 'Child';
								$butir1[$noLv1]['nama_jabatan'] = $key1->jabatan->nama;
							}
							$butir1[$noLv1]['row'] = [];
						}
						$noLv1++;
					}
					$butir[$no]['statusRow'] = 'Parent';
					$butir[$no]['row'] = $butir1;
				}else{
					$butir[$no]['statusRow'] = 'Parent';
					$butir[$no]['row'] = [];
				}
				$no++;
			}
			$data['butir'] = $butir;
			$return = ['status'=>'success','code'=>200,'row'=>$data];
		}else{
			$data['butir'] = $butir;
			$return = ['status'=>'error','code'=>404,'row'=>$data];
		}
		return $return;
	}

	public static function get_butirNew($id_profesi, $id_permenpan)
	{
		$butir = [];
		$no = 0;
		$unsur = MasterType::where('modul','SatuanUnsur')
							->where('tipe_profesi_id', $id_profesi)
							->where('permenpan_id', $id_permenpan)
							->where('bl_state', 'A')
							->orderBy('urutan', 'ASC')
							->get();
		if (count($unsur) > 0) {
			foreach ($unsur as $uns) {
				$butir[$no] = $uns;
				$lv1 = PakMasterKegiatan::where('formasi_id', $id_profesi)
										->where('unsur_pak_id', $uns->id_master_type)
										->where('permenpan_id', $id_permenpan)
										->where('jenis', 'Butir')
										->where('level', '1')
										->where('bl_state', 'A')
										->get();
				if (count($lv1) > 0) {
					$butir1 = [];
					$noLv1 = 0;
					foreach ($lv1 as $key1) {
						$butir1[$noLv1] = $key1;
						$lv2 = PakMasterKegiatan::where('formasi_id', $id_profesi)
												->where('unsur_pak_id', $uns->id_master_type)
												->where('permenpan_id', $id_permenpan)
												->where('jenis', 'Butir')
												->where('level', '2')
												->where('bl_state', 'A')
												->where('parent_id', $key1->id_pak_master)
												->get();
						if (count($lv2) > 0) {
							$butir2 = [];
							$noLv2 = 0;
							foreach ($lv2 as $key2) {
								$butir2[$noLv2] = $key2;
								$lv3 = PakMasterKegiatan::where('formasi_id', $id_profesi)
														->where('unsur_pak_id', $uns->id_master_type)
														->where('permenpan_id', $id_permenpan)
														->where('jenis', 'Butir')
														->where('level', '3')
														->where('bl_state', 'A')
														->where('parent_id', $key2->id_pak_master)
														->get();
								if (count($lv3) > 0) {
									$butir3 = [];
									$noLv3 = 0;
									foreach ($lv3 as $key3) {
										$butir3[$noLv3] = $key3;
										$lv4 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																->where('unsur_pak_id', $uns->id_master_type)
																->where('permenpan_id', $id_permenpan)
																->where('jenis', 'Butir')
																->where('level', '4')
																->where('bl_state', 'A')
																->where('parent_id', $key3->id_pak_master)
																->get();
										if (count($lv4) > 0) {
											$butir4 = [];
											$noLv4 = 0;
											foreach ($lv4 as $key4) {
												$butir4[$noLv4] = $key4;
												$lv5 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																		->where('unsur_pak_id', $uns->id_master_type)
																		->where('permenpan_id', $id_permenpan)
																		->where('jenis', 'Butir')
																		->where('level', '5')
																		->where('bl_state', 'A')
																		->where('parent_id', $key4->id_pak_master)
																		->get();
												if (count($lv5) > 0) {
													$butir5 = [];
													$noLv5 = 0;
													foreach ($lv5 as $key5) {
														$butir5[$noLv5] = $key5;
														$lv6 = PakMasterKegiatan::where('formasi_id', $id_profesi)
																				->where('unsur_pak_id', $uns->id_master_type)
																				->where('permenpan_id', $id_permenpan)
																				->where('jenis', 'Butir')
																				->where('level', '6')
																				->where('bl_state', 'A')
																				->where('parent_id', $key5->id_pak_master)
																				->get();
														if (count($lv6) > 0) {
															$butir6 = [];
															$noLv6 = 0;
															foreach ($lv6 as $key6) {
																$butir6[$noLv6] = $key6;
																if ($key6->is_title == '1') {
																	$butir6[$noLv6]['statusRow'] = 'Parent';
																}else{
																	$butir6[$noLv6]['statusRow'] = 'Child';
																	$butir6[$noLv6]['nama_jabatan'] = $key6->jabatan->nama;
																}
																$butir6[$noLv6]['row'] = [];
																$noLv6++;
															}
															$butir5[$noLv5]['statusRow'] = 'Parent';
															$butir5[$noLv5]['row'] = $butir6;
														}else{
															if ($key5->is_title == '1') {
																$butir5[$noLv5]['statusRow'] = 'Parent';
															}else{
																$butir5[$noLv5]['statusRow'] = 'Child';
																$butir5[$noLv5]['nama_jabatan'] = $key5->jabatan->nama;
															}
															$butir5[$noLv5]['row'] = [];
														}
														$noLv5++;
													}
													$butir4[$noLv4]['statusRow'] = 'Parent';
													$butir4[$noLv4]['row'] = $butir5;
												}else{
													if ($key4->is_title == '1') {
														$butir4[$noLv4]['statusRow'] = 'Parent';
													}else{
														$butir4[$noLv4]['statusRow'] = 'Child';
														$butir4[$noLv4]['nama_jabatan'] = $key4->jabatan->nama;
													}
													$butir4[$noLv4]['row'] = [];
												}
												$noLv4++;
											}
											$butir3[$noLv3]['statusRow'] = 'Parent';
											$butir3[$noLv3]['row'] = $butir4;
										}else{
											if ($key3->is_title == '1') {
												$butir3[$noLv3]['statusRow'] = 'Parent';
											}else{
												$butir3[$noLv3]['statusRow'] = 'Child';
												$butir3[$noLv3]['nama_jabatan'] = $key3->jabatan->nama;
											}
											$butir3[$noLv3]['row'] = [];
										}
										$noLv3++;
									}
									$butir2[$noLv2]['statusRow'] = 'Parent';
									$butir2[$noLv2]['row'] = $butir3;
								}else{
									if ($key2->is_title == '1') {
										$butir2[$noLv2]['statusRow'] = 'Parent';
									}else{
										$butir2[$noLv2]['statusRow'] = 'Child';
										$butir2[$noLv2]['nama_jabatan'] = $key2->jabatan->nama;
									}
									$butir2[$noLv2]['row'] = [];
								}
								$noLv2++;
							}
							$butir1[$noLv1]['statusRow'] = 'Parent';
							$butir1[$noLv1]['row'] = $butir2;
						}else{
							if ($key1->is_title == '1') {
								$butir1[$noLv1]['statusRow'] = 'Parent';
							}else{
								$butir1[$noLv1]['statusRow'] = 'Child';
								$butir1[$noLv1]['nama_jabatan'] = $key1->jabatan->nama;
							}
							$butir1[$noLv1]['row'] = [];
						}
						$noLv1++;
					}
					$butir[$no]['statusRow'] = 'Parent';
					$butir[$no]['row'] = $butir1;
				}else{
					$butir[$no]['statusRow'] = 'Parent';
					$butir[$no]['row'] = [];
				}
				$no++;
			}
			$data['butir'] = $butir;
			$return = ['status'=>'success','code'=>200,'row'=>$data];
		}else{
			$data['butir'] = $butir;
			$return = ['status'=>'error','code'=>404,'row'=>$data];
		}
		return $return;
	}
}
