<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\DataMaster;
use App\Models\Pegawai;
use App\Models\PengajuanDupak;
use App\Models\MasaPengajuan;
use App\Models\Visitor;
use Auth, Redirect, Validator, DB;

class DashboardController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "dashboard";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Dashboard';
      $this->data['smallTitle'] = "";
      // return Auth::getUser()->id;
      if (Auth::getUser()->level_user == '5') {
        $this->data['pegawai'] = Pegawai::where('user_id', Auth::getUser()->id)->first();

        $getPengajuan = MasaPengajuan::where('status_masa','=','Proses')->first();
        if (!empty($getPengajuan)) {
          $pengajuan = PengajuanDupak::join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                                      ->where('pengajuan_dupak.masa_pengajuan_id',$getPengajuan->id_masa_pengajuan)
                                      ->where('pengajuan_dupak.user_id',Auth::getUser()->id)
                                      // ->where('masa_pengajuan.status_masa','Proses')
                                      // ->where('pengajuan_dupak.status_pengajuan','!=','Selesai')
                                      ->where('masa_pengajuan.tgl_akhir','>=',date('Y-m-d'))
                                      ->first();          
        }

        $this->data['pengajuan'] = (!empty($pengajuan)) ? $pengajuan : '';
        $dateNow = date('Y-m-d');
        
        $masa = MasaPengajuan::where('status_masa','Proses')->first();
        if (!empty($masa)) {
          if ($dateNow >= $masa->tgl_awal && $dateNow <= $masa->tgl_akhir) {
            $this->data['status_masa_pengajuan'] = 'true';
            $this->data['masa_pengajuan'] = $masa;
          }else{
            if (!empty($pengajuan)) {
              $this->data['status_masa_pengajuan'] = 'true';
              $this->data['masa_pengajuan'] = MasaPengajuan::find($pengajuan->masa_pengajuan_id);
            }else{
              $this->data['status_masa_pengajuan'] = 'false';
            }
          }
        }else{      
          $this->data['status_masa_pengajuan'] = 'false';
        }        
        
        return view('dashboard.pegawai.main')->with('data', $this->data);
      }else{
        $this->data['jumlahPNS'] = Pegawai::where('status_pegawai','PNS')->count();
        $this->data['jumlahNonPNS'] = Pegawai::where('status_pegawai','Non-PNS')->count();
        $this->data['jumlahTimPenilai'] = Pegawai::where('status_penilai','Ya')->count();
        return view('dashboard.main')->with('data', $this->data);
      }
      $this->data;
    }
    
     public function getGrafikVisit(Request $request)
    {
        $totDatVis = count(Visitor::all());
        if($totDatVis > 7){
            $offs = $totDatVis - 7;
        }else{
            $offs = 0;
        }
        $visits = Visitor::limit(7)->offset($offs)->orderBy('id_Visitor','ASC')->get();
        $z = 0;
        if (count($visits) > 0) {
            foreach ($visits as $visit) {
                $totVis[$z] = [
                    'tanggal' => date('d', strtotime($visit->days)),
                    'bulan' => date('m', strtotime($visit->days)),
                    'tahun' => date('Y', strtotime($visit->days)),
                    'Visitor' => $visit->total
                ];
                $z++;
            }
        }else{
            $totVis = [];
        }
        $return = ['totVis'=>$totVis];
        return response()->json($return);
    }

    public function mainKaryawan(Request $request)
    {
      $this->data['mn_active'] = "dashboard";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Dashboard';
      $this->data['smallTitle'] = "";
      return view('dashboard.main')->with('data', $this->data);
    }
}
