<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ActivitySystems;
use Auth, Redirect, Validator, DB;

class LogButirKegiatanController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "LogButirKegiatan";
      $this->data['submn_active'] = "LogButirKegiatan";
      $this->data['title'] = 'Log Aktivitas Perubahan dan Penambahan Butir Kegiatan';
      $this->data['smallTitle'] = "";
      return view('master.butirKegiatan.new.log')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      $data = ActivitySystems::getJsonActivitySystems($request);
      return response()->json($data);      
    }

    public function detailData(Request $request)
    {
      // return $request->all();
      $data['log'] = ActivitySystems::find($request->id);
      $content = view('master.butirKegiatan.new.detaillog', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }
}
