<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PakMasterKegiatan;
use App\Models\Penilaian;
use App\Models\DetailKegiatanPegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Models\BebanKerja;
use App\Models\MasaPengajuan;
use App\Models\Pegawai;
use App\Models\Users;
use App\Models\BuktiFisikKegiatan;
use App\Models\PenerapanPermenpan;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use Auth, Redirect, Validator, DB;

class KegiatanPegawaiDownloadController extends Controller
{
     public function mainNew(Request $request)
    {
      $this->data['mn_active'] = "kegiatanHarianDownload";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Data Kegiatan Harian';
      $this->data['smallTitle'] = "";
      if (Auth::getUser()->level_user == '5') {
        $this->data['dtPegawai'] = Users::find(Auth::id());
        $this->data['dtJabatan'] = DataMaster::where('tipe_profesi_id', $this->data['dtPegawai']->pegawai->tipe_profesi_id)
                                      ->where('is_golongan','N')
                                      ->where('bl_state','A')
                                      ->orderBy('urutan','ASC')->get();
        return view('kegiatanPegawaiDownload.mainNew')->with('data', $this->data);
      }
    }
}
