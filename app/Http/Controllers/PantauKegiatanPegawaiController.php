<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetailKegiatanPegawai;
use App\Models\PakMasterKegiatan;
use App\Models\Pegawai;
use App\Models\Penilaian;
use App\Models\SatuanKerja;
use App\Models\Users;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;

class PantauKegiatanPegawaiController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "pantauKegiatan";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Kegiatan Pegawai';
      $this->data['smallTitle'] = "";
      if (Auth::getUser()->level_user == '1') {
        $this->data['pegawai'] = Pegawai::all();
      }else{
        $pegawai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        // $satuanKerja = SatuanKerja::where('pimpinan_id', $pegawai->id_pegawai)->first();
        // $this->data['pegawai'] = Pegawai::where('satuan_kerja_id', $satuanKerja->id_satuan_kerja)->get();
        
        $satuanKerja = SatuanKerja::select('id_satuan_kerja')->where('pimpinan_id', $pegawai->id_pegawai)->get();
        $this->data['pegawai'] = Pegawai::whereIn('satuan_kerja_id', $satuanKerja)->get();
      }

      return view('pantauKegiatan.main')->with('data', $this->data);
    }

    public function loadKegiatan(Request $request)
    {
      if ($request->bulan == null) {
        $tahun = date('Y'); //Mengambil tahun saat ini
        $bulan = date('m'); //Mengambil bulan saat ini
      }else{
        $blnLoad = explode('-', $request->bulan) ;
        $tahun = $blnLoad[1];
        $bulan = $blnLoad[0];
      }
      $data['jumlahTanggal'] = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
      $data['bulan'] = $bulan;
      $data['tahun'] = $tahun;
      $pegawai = Pegawai::find($request->id_pegawai);
      $getButir = Formatters::get_butir($pegawai->tipe_profesi_id);
      if ($getButir['status'] == 'success') {
        $data['butir'] = $getButir['row']['butir'];
        $acc = Penilaian::where('user_id', $pegawai->user_id)->where('tanggal', 'like', $tahun.'-'.$bulan.'%')->where('acc_kepala_status','B')->get();
        $data['acc'] = (count($acc) > 0) ? 'Belum' : 'Sudah';
        $data['jm'] = count($acc);
        // return $data;
        $return = ['status'=>'success','code'=>200,'row'=>$data];
      }else{
        $return = ['status'=>'error','code'=>404,'row'=>$data];
      }
      return response()->json($return);
    }

    public function getNilaiPantau(Request $request)
    {
      $blnLoad = explode('-', $request->bulan) ;
      $tahun = $blnLoad[1];
      $bulan = $blnLoad[0];
      $jumlahTanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

      $data = [];
      $jml = 0;
      $jmlAk = 0;
      $pegawai = Pegawai::find($request->idPegawai);
      for ($i=0; $i < $jumlahTanggal; $i++) {
        $dy = $i+1;
        $tglFull = $tahun.'-'.$bulan.'-'.$dy;
        $cekNilai = Penilaian::where('jenis','Harian')
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->where('tanggal', $tglFull)
                              ->where('user_id', $pegawai->user_id)
                              ->first();
        if (!empty($cekNilai)) {
          $idPenilaian  = $cekNilai->id_penilaian;
          $hasil        = $cekNilai->jumlah_px_real;
          $jml          = $jml + $cekNilai->jumlah_px_real;
          $jmlAk        = $jmlAk + $cekNilai->poin_pemohon;
        }else{
          $idPenilaian  = '';
          $hasil        = '-';
        }
        $data[$i] = [
          'idpak'       => $request->idPakMaster,
          'tgl'         => $i+1,
          'bln'         => $bulan,
          'thn'         => $tahun,
          'hasil'       => $hasil,
          'idPenilaian' => $idPenilaian,
        ];
      }

      $dtJml['idpak'] = $request->idPakMaster;
      $dtJml['jml'] = $jml;
      $dtJml['jmlAk'] = $jmlAk;

      $return = ['status'=>'success','code'=>200,'row'=>$data, 'dtJumlah'=>$dtJml];
      return response()->json($return);
    }

    public function accSatKer(Request $request)
    {
      $blnLoad = explode('-', $request->bulan) ;
      $tahun = $blnLoad[1];
      $bulan = $blnLoad[0];
      $pegawai = Pegawai::find($request->id_pegawai);
      $kepala = Pegawai::where('user_id', Auth::getUser()->id)->first();
      $penilaian = Penilaian::where('user_id', $pegawai->user_id)->where('tanggal', 'like', $tahun.'-'.$bulan.'%')->where('acc_kepala_status','B')->get();
      if (count($penilaian) > 0) {
          $update = Penilaian::where('user_id', $pegawai->user_id)
                              ->where('tanggal', 'like', $tahun.'-'.$bulan.'%')
                              ->where('acc_kepala_status','B')
                              ->update(['kepala_saker_id'=>$kepala->id_pegawai, 'acc_kepala_status' => 'Y', 'satuan_kerja_kepala_id'=>$kepala->satuan_kerja_id, 'tgl_acc_kepala'=>date('Y-m-d')]);
          if ($update) {
            $return = ['status'=>'success','code'=>200,'message'=>'Penilaian Berhasil di Setujui !!', 'data'=>''];
          }else{
            $return = ['status'=>'error','code'=>500,'message'=>'Penilaian Gagal di Setujui !!', 'data'=>''];
          }
      }else{
        $return = ['status'=>'error','code'=>404,'message'=>'Data Penilaian Tidak Ditemukan !!', 'data'=>''];
      }
      return response()->json($return);
    }
}
