<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Users;
use App\Models\Pegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Models\ChangePasswords;
use App\Models\Visitor;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/login';

    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
      $this->data['title'] = 'Login';
      $this->data['next_url'] = empty($request->next_url) ? '' : $request->next_url;
  		return view('component.login')->with('data', $this->data);
    }

    public function doLogin(Request $request)
    {
      $cekUsername = Users::where('username',$request->username)->first();
      if (empty($cekUsername)) {
        $cekEmail = Users::where('email',$request->username)->first();
        if (empty($cekEmail)) {
          $return = ['status'=>'error', 'message'=>'Akun Tidak Dapat Ditemukan, Silahkan Cek Kembali NIK / Email / Username Anda !!'];
          return response()->json($return);
        }else{
          $data['email'] = strip_tags($request->username);
          $userColumn = 'email';
        }
      }else{
        $data['username'] = strip_tags($request->username);
        $userColumn = 'username';
      }
      $data['password'] = strip_tags($request->password);
      $cek = Auth::attempt($data);

      if($cek){
        if (Auth::user()->is_banned == 0) {
            
            //hitung visitor
          $date_now = date('Y-m-d');
          $visitor_today = Visitor::where('days', $date_now)->first();
            
          if (!empty($visitor_today)) {
            $amount_visitor = $visitor_today->total + 1;
            $visitor = Visitor::find($visitor_today->id_visitor);
            $visitor->total = $amount_visitor;
            $datavit = $visitor->save();
          }else{
            $visitor = new Visitor;
            $visitor->total = 1;
            $visitor->days = $date_now;
            $datavit = $visitor->save();
          }
          date_default_timezone_set('Asia/Jakarta');
          $user = Users::find(Auth::user()->id);
          $user->last_login = date("Y-m-d H:i:s");
          $user->save();

          $requestActivity = [
            'action_type'   => 'Login',
            'message'       => 'Melakukan Login Sistem',
          ];
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'message'=>'Login Berhasil'];
        }elseif(Auth::user()->is_banned == 2){
          Auth::logout();
          $return = ['status'=>'error', 'message'=>'Akun sedang Menunggu Konfirmasi Admin, Silahkan menghubungin Admin untuk Aktifkan Akun !!'];
        }else{
          Auth::logout();
          $return = ['status'=>'error', 'message'=>'Akun sedang di Non-Aktfikan, Silahkan menghubungin Admin untuk Aktifkan Akun !!'];
        }
      }else{
        $cekChangePassword = ChangePasswords::join('users','users.id','change_passwords.user_id')
                                            ->where('old_password', $data['password'])
                                            ->where($userColumn, $request->email)->first();
        if (!empty($cekChangePassword)) {
          $return = ['status'=>'error', 'message'=>'Password telah diperbaharui pada '.date("d-m-Y", strtotime($cekChangePassword->created_at)).' !!'];
        }else{
          $return = ['status'=>'error', 'message'=>'Email atau Password Salah, silahkan Coba Kembali !!'];
        }
      }
      return response()->json($return);
    }

    public function logout(Request $request)
    {
      $logout = Auth::logout();
  		return Redirect::route('login')->with('title', 'Berhasil !')->with('message', 'Anda telah logout.')->with('type', 'success');
    }
    public function register(Request $request)
    {
      $data['title'] = 'Register Pegawai';
      $data['next_url'] = empty($request->next_url) ? '' : $request->next_url;
      $data['golongan'] = DataMaster::where('is_golongan','Y')->where('bl_state','A')->get();
      $data['profesi'] = MasterType::where('modul','StatusProfesi')->where('bl_state','A')->get();
      $data['satuankerja'] = SatuanKerja::where('bl_state','A')->get();
      $data['jabatan'] = DataMaster::where('is_golongan','N')
                                         ->where('bl_state','A')->get();
                                         // return $data;
      return view('component.register')->with('data', $data);
    }

    public function doRegistrasi(Request $request)
    {
      $rules = array(
        'status_pegawai'      => 'required',
        'email'               => 'required',
        'nama'                => 'required',
        'jenis_kelamin'       => 'required',
        'tanggal_lahir'       => 'required',
        'tempat_lahir'        => 'required',
        'no_nip'              => 'required',
        'pendidikan_terakhir'      => 'required',
        'pendidikan_tahun'         => 'required',
        'password'         => 'required',
        'satuan_kerja_id' => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
           //hitung visitor
          $date_now = date('Y-m-d');
          $visitor_today = Visitor::where('days', $date_now)->first();
            
          if (!empty($visitor_today)) {
            $amount_visitor = $visitor_today->total + 1;
            $visitor = Visitor::find($visitor_today->id_visitor);
            $visitor->total = $amount_visitor;
            $datavit = $visitor->save();
          }else{
            $visitor = new Visitor;
            $visitor->total = 1;
            $visitor->days = $date_now;
            $datavit = $visitor->save();
          }
          
        if (empty($request->id_pegawai)) {
          $user             = new Users;
          $user->username   = ($request->no_nip != '') ? str_replace(' ', '', $request->no_nip) : $request->email;
          $user->email      = $request->email;
          $user->password   = bcrypt($request->password);
          $user->name       = $request->nama;
          $user->gender     = $request->jenis_kelamin;
          $user->level_user = '5';
          $user->is_banned  = '0';
          $user->save();
        }
        $dtPegawai                        = new Pegawai;
        $dtPegawai->status_pegawai        = $request->status_pegawai;
        $dtPegawai->status_penilai        = "Tidak";
        $dtPegawai->nama                  = $request->nama;
        $dtPegawai->no_nip                = $request->no_nip;
        $dtPegawai->no_karpeng            = $request->no_karpeng;
        $dtPegawai->tempat_lahir          = $request->tempat_lahir;
        $dtPegawai->tanggal_lahir         = date('Y-m-d', strtotime($request->tanggal_lahir));
        $dtPegawai->jenis_kelamin         = $request->jenis_kelamin;
        $dtPegawai->golongan_id           = $request->golongan_id;
        $dtPegawai->golongan_tmt          = date('Y-m-d', strtotime($request->golongan_tmt));
        $dtPegawai->jabatan_id            = $request->jabatan_id;
        $dtPegawai->jabatan_tmt           = date('Y-m-d', strtotime($request->jabatan_tmt));
        $dtPegawai->pendidikan_terakhir   = $request->pendidikan_terakhir;
        $dtPegawai->pendidikan_tahun      = $request->pendidikan_tahun;
        $dtPegawai->satuan_kerja_id       = $request->satuan_kerja_id;
        $dtPegawai->bl_note               = '0';
        $dtPegawai->tipe_profesi_id       = $request->tipe_profesi_id;
        $dtPegawai->ac_conn               = $_SERVER['REMOTE_ADDR'];
        $dtPegawai->cluster_id            = 'LATIHAN1';
        $dtPegawai->bllokal               = '1';
        if (empty($request->id_pegawai)) {
          $dtPegawai->user_id             = $user->id;
        }
        $dtPegawai->save();
        if ($dtPegawai) {
          if (empty($request->id_pegawai)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Pengguna Pegawai '.$request->Nama,
            ];
          }
          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
}
