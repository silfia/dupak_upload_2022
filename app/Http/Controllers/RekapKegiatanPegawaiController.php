<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\DetailKegiatanPegawai;
// use App\Models\PakMasterKegiatan;
use App\Models\Pegawai;
use App\Models\Penilaian;
use App\Models\SatuanKerja;
// use App\Models\Users;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;

class RekapKegiatanPegawaiController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "rekapKegiatan";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Rekap Kegiatan Pegawai';
      $this->data['smallTitle'] = "";
      if (Auth::getUser()->level_user == '1') {
        $this->data['pegawai'] = Pegawai::where('satuan_kerja_id', '!=', null)->get();
      }else{
        if (Auth::getUser()->is_leader == 'Y') {
          $pegawai = Pegawai::where('user_id', Auth::getUser()->id)->first();
          $satuanKerja = SatuanKerja::where('pimpinan_id', $pegawai->id_pegawai)->first();
          $this->data['pegawai'] = Pegawai::where('satuan_kerja_id', $satuanKerja->id_satuan_kerja)->get();
        }else{
          $this->data['pegawai'] = Pegawai::where('user_id', Auth::getUser()->id)->first();
        }
      }

      return view('rekapKegiatan.main')->with('data', $this->data);
    }

    public function loadRekap(Request $request)
    {
      $data['tahun'] = $request->tahun;
      $pegawai = Pegawai::select('pegawai.id_pegawai as id_pegawai', 'pegawai.nama as nama_pegawai', 'pegawai.no_nip as no_nip', 'pegawai.tipe_profesi_id as tipe_profesi_id', 'pegawai.status_pegawai as status_pegawai', 'jabatan.nama as nama_jabatan', 'golongan.nama as nama_golongan', 'satuan_kerja.nama as nama_unit_kerja', 'master_type.nama as nama_profesi')
                          ->join('master_type','master_type.id_master_type','pegawai.tipe_profesi_id')
                          ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                          ->leftJoin('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                          ->leftJoin('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                          ->where('pegawai.id_pegawai', $request->idPegawai)
                          ->first();
      $data['pegawai'] = $pegawai;
      $getButir = Formatters::get_butir($pegawai->tipe_profesi_id);
      $data['butir_kegiatan'] = $getButir['row']['butir'];

      return ['status'=>'success','data'=>$data];
    }

    public function getNilai(Request $request)
    {
      $pegawai = Pegawai::find($request->idPegawai);
      $penilaian = Penilaian::selectRaw('id_penilaian, master_kegiatan_id, MONTH(tanggal) as bulan, tanggal, jumlah_px_pemohon , poin_pemohon')
                              ->where('user_id', $pegawai->user_id)
                              ->whereYear('tanggal', $request->tahun)
                              ->get();
                              // return $penilaian;

      if (count($penilaian) > 0) {
        $return = ['status'=>'success','code'=>200,'row'=>$penilaian];
      }else{
        $return = ['status'=>'error','code'=>404,'row'=>''];
      }
      return response()->json($return);
    }
}
