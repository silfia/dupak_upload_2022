<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasterType; // digunakan
use App\Models\PakMasterKegiatan;
use App\Models\SuratPernyataan;
use App\Models\UraianSuratPernyataan;
use App\Models\Penilaian;
use App\Models\Users;
use App\Models\TimPenilai;
use App\Models\DataSetting;
use App\Models\DataMaster;
use App\Models\StandartMaster;
use App\Models\HasilAngkaKredit; // digunakan
use App\Models\MasaPengajuan; // digunakan
use App\Models\Pegawai; // digunakan
use App\Models\PengajuanDupak; // digunakan
use App\Models\SatuanKerja; // digunakan
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB, PDF;

class PengajuanDupakController extends Controller
{
  public function form(Request $request)
  {
    $this->data['mn_active'] = "";
    $this->data['submn_active'] = "";
    $this->data['title'] = 'Pengajuan DUPAK';
    $this->data['smallTitle'] = "";
    $this->data['pengajuan'] = (!empty($request->id)) ? MasaPengajuan::where('user_id', Auth::getUser()->id): "";
    $this->data['pegawai'] = Pegawai::join('users','users.id','pegawai.user_id')
                                      ->where('users.id', Auth::getUser()->id)->first();
    return view('dashboard.pengajuanDupak.form')->with('data', $this->data);
  }

  public function savePengajuan(Request $request)
  {
    $rules = array(
      'tgl_awal'    => 'required',
      'tgl_akhir'   => 'required',
    );
    $messages = array(
      'required'    => 'Kolom Harus Diisi',
    );
    $validator 	= Validator::make($request->all(), $rules, $messages);
    if (!$validator->fails()) {
      $masaPengajuan = MasaPengajuan::where('status_masa','Proses')->first();
      if (!empty($masaPengajuan)) {
        $pegawai        = Pegawai::where('user_id', Auth::getUser()->id)->first();
        $satuan_kerja   = SatuanKerja::find($pegawai->satuan_kerja_id);
        $kepala         = Pegawai::find($satuan_kerja->pimpinan_id);
        if (empty($kepala)) {
          return ['status'=>'error', 'code'=>'500', 'message'=>'Anda Tidak Memiliki Kepala Unit Kerja, Silahkan Hubungi Admin untuk menambah Kepala Unit Kerja !!'];
        }

        $pengajuan                                  = new PengajuanDupak;
        $pengajuan->masa_pengajuan_id               = $masaPengajuan->id_masa_pengajuan;
        $pengajuan->no_skpak                        = '-';
        $pengajuan->tgl_pengajuan                   = date('Y-m-d');
        $pengajuan->tgl_awal_penilaian              = date('Y-m-d', strtotime($request->tgl_awal));
        $pengajuan->tgl_akhir_penilaian             = date('Y-m-d', strtotime($request->tgl_akhir));
        $pengajuan->status_pengajuan                = 'Pending';
        $pengajuan->user_id                         = Auth::getUser()->id;
        $pengajuan->bl_state                        = 'A';
        $pengajuan->jabatan_lama_id                 = $pegawai->jabatan_id;
        $pengajuan->jabatan_lama_tmt                = $pegawai->jabatan_tmt;
        $pengajuan->golongan_lama_id                = $pegawai->golongan_id;
        $pengajuan->golongan_lama_tmt               = $pegawai->golongan_tmt;
        $pengajuan->pendidikan_telah_diperhitungkan = $pegawai->pendidikan_terakhir;
        $pengajuan->masa_kerja_lama                 = $pegawai->masa_kerja;
        $pengajuan->kepala_id                       = $kepala->id_pegawai;
        $pengajuan->golongan_kepala_id              = $kepala->golongan_id;
        $pengajuan->golongan_kepala_tmt             = $kepala->golongan_tmt;
        $pengajuan->jabatan_kepala_id               = $kepala->jabatan_id;
        $pengajuan->satuan_kerja_kepala_id          = $kepala->satuan_kerja_id;
        $pengajuan->save();

        if ($pengajuan) {
          $requestActivity = [
            'action_type'   => 'Insert',
            'message'       => 'Create Pengajuan Dupak',
          ];
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else {
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
      }else{
        $return = ['status'=>'error', 'code'=>'500', 'message'=>'Tidak Ada Masa Pengajuan Dupak Ditemukan !!'];
      }
      return response()->json($return);
    } else {
      return $validator->messages();
    }
  }

  public function formCetakBerkas(Request $request)
  {
    $this->data['mn_active'] = "";
    $this->data['submn_active'] = "";
    $this->data['title'] = 'Cetak Berkas Pengajuan DUPAK';
    $this->data['smallTitle'] = "";
    $masaPengajuan = MasaPengajuan::where('status_masa','!=','Selesai')->orderBy('id_masa_pengajuan', 'DESC')->first();
    $pengajuanDupak = PengajuanDupak::where('masa_pengajuan_id', $masaPengajuan->id_masa_pengajuan)
                                      ->where('user_id', Auth::getUser()->id)
                                      ->first();
    // return $pengajuanDupak;
    $this->data['pengajuan'] = $pengajuanDupak;
    $this->data['pegawai'] = Pegawai::join('users','users.id','pegawai.user_id')
                                      ->where('users.id', Auth::getUser()->id)->first();
    $tahunPertama = date('Y', strtotime($pengajuanDupak->tgl_awal_penilaian));
    $tahunTerakhir = date('Y', strtotime($pengajuanDupak->tgl_akhir_penilaian));
    $tempTahun = $tahunPertama;
    $no = 0;
    $noT = 0;
    while ($tempTahun <= $tahunTerakhir) {
      if ($tempTahun == $tahunPertama) {
        $bulanPertama = date('m', strtotime($pengajuanDupak->tgl_awal_penilaian));
        if ($bulanPertama <= 6) {
          $tanggalAkhirSemester1 = cal_days_in_month(CAL_GREGORIAN, '06', $tempTahun);
          $dataSemester[$no] = [
            'title' => 'Semester Pertama '.$tempTahun,
            'tgl_awal' => date('Y-m-d', strtotime($pengajuanDupak->tgl_awal_penilaian)),
            'tgl_akhir' => $tempTahun.'-06-'.$tanggalAkhirSemester1,
            'kode' => $tempTahun.'1',
          ];
          $no++;

          $tanggalAkhirSemester2 = cal_days_in_month(CAL_GREGORIAN, '12', $tempTahun);
          $dataSemester[$no] = [
            'title' => 'Semester Kedua '.$tempTahun,
            'tgl_awal' => $tempTahun.'-07-01',
            'tgl_akhir' => $tempTahun.'-12-'.$tanggalAkhirSemester2,
            'kode' => $tempTahun.'2',
          ];
          $no++;
        }else{
          $tanggalAkhirSemester2 = cal_days_in_month(CAL_GREGORIAN, '12', $tempTahun);
          $dataSemester[$no] = [
            'title' => 'Semester Kedua '.$tempTahun,
            'tgl_awal' => date('Y-m-d', strtotime($pengajuanDupak->tgl_awal_penilaian)),
            'tgl_akhir' => $tempTahun.'-12-'.$tanggalAkhirSemester2,
            'kode' => $tempTahun.'2',
          ];
          $no++;
        }
        $tanggalAkhirBulanTahun = cal_days_in_month(CAL_GREGORIAN, '12', $tempTahun);
        $dataRekTahun[$noT] = [
            'title' => 'Tahun '.$tempTahun,
            'tgl_awal' => date('Y-m-d', strtotime($pengajuanDupak->tgl_awal_penilaian)),
            'tgl_akhir' => $tempTahun.'-12-'.$tanggalAkhirBulanTahun,
            'kode' => $tempTahun.'_thn',
        ];
        $noT++;
      }elseif ($tempTahun == $tahunTerakhir) {
        $bulanPertama = date('m', strtotime($pengajuanDupak->tgl_akhir_penilaian));
        if ($bulanPertama <= 6) {
          $dataSemester[$no] = [
            'title' => 'Semester Pertama '.$tempTahun,
            'tgl_awal' => $tempTahun.'-01-01',
            'tgl_akhir' => date('Y-m-d', strtotime($pengajuanDupak->tgl_akhir_penilaian)),
            'kode' => $tempTahun.'1',
          ];
          $no++;
        }else{
          $tanggalAkhirSemester1 = cal_days_in_month(CAL_GREGORIAN, '06', $tempTahun);
          $dataSemester[$no] = [
            'title' => 'Semester Pertama '.$tempTahun,
            'tgl_awal' => $tempTahun.'-01-01',
            'tgl_akhir' => $tempTahun.'-06-'.$tanggalAkhirSemester1,
            'kode' => $tempTahun.'1',
          ];
          $no++;

          $dataSemester[$no] = [
            'title' => 'Semester Kedua '.$tempTahun,
            'tgl_awal' => $tempTahun.'-07-01',
            'tgl_akhir' => date('Y-m-d', strtotime($pengajuanDupak->tgl_akhir_penilaian)),
            'kode' => $tempTahun.'2',
          ];
          $no++;
        }
        $dataRekTahun[$noT] = [
            'title' => 'Tahun '.$tempTahun,
            'tgl_awal' => $tempTahun.'-01-01',
            'tgl_akhir' => date('Y-m-d', strtotime($pengajuanDupak->tgl_akhir_penilaian)),
            'kode' => $tempTahun.'_thn',
        ];
        $noT++;
      }else{
        $tanggalAkhirSemester1 = cal_days_in_month(CAL_GREGORIAN, '06', $tempTahun);
        $dataSemester[$no] = [
          'title' => 'Semester Pertama '.$tempTahun,
          'tgl_awal' => $tempTahun.'-01-01',
          'tgl_akhir' => $tempTahun.'-06-'.$tanggalAkhirSemester1,
          'kode' => $tempTahun.'1',
        ];
        $no++;

        $tanggalAkhirSemester2 = cal_days_in_month(CAL_GREGORIAN, '12', $tempTahun);
        $dataSemester[$no] = [
          'title' => 'Semester Kedua '.$tempTahun,
          'tgl_awal' => $tempTahun.'-07-01',
          'tgl_akhir' => $tempTahun.'-12-'.$tanggalAkhirSemester2,
          'kode' => $tempTahun.'2',
        ];
        $no++;
        $tanggalAkhirBulanTahun = cal_days_in_month(CAL_GREGORIAN, '12', $tempTahun);
        $dataRekTahun[$noT] = [
            'title' => 'Tahun '.$tempTahun,
            'tgl_awal' => $tempTahun.'-01-01',
            'tgl_akhir' => $tempTahun.'-12-'.$tanggalAkhirBulanTahun,
            'kode' => $tempTahun.'_thn',
        ];
        $noT++;
      }
      $tempTahun++;
    }
    $this->data['semester'] = $dataSemester;
    $this->data['rekapTahun'] = $dataRekTahun;
    // return $this->data;
    return view('dashboard.pengajuanDupak.formCetakBerkas')->with('data', $this->data);
  }

  public function getDataSpmk(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->id_pengajuan);
    $pegawai = Pegawai::where('user_id',$pengajuan->user_id)->first();
    $data['pegawai'] = [
      'nama' => $pegawai->nama,
      'nip' => $pegawai->no_nip,
      'golongan' => $pengajuan->golonganPemohonLama->nama,
      'golonganTmt' => date('d-m-Y', strtotime($pengajuan->golongan_lama_tmt)),
      'jabatan' => $pengajuan->jabatanPemohonLama->nama,
      'unitKerja' => $pegawai->satuanKerja->nama,
      'profesi' => $pegawai->typeProfesi->nama,
    ];
    $kepala = Pegawai::find($pengajuan->kepala_id);
    $data['kepala'] = [
      'nama' => $kepala->nama, 
      'nip' => $kepala->no_nip,
      'golongan' => $pengajuan->golonganKepala->nama,
      'golonganTmt' => date('d-m-Y', strtotime($pengajuan->golongan_kepala_tmt)),
      'jabatan' => $pengajuan->jabatanKepala->nama,
      'unitKerja' => $pengajuan->satuanKerjaKepala->nama,
    ];
    $getButir = Formatters::get_butir($pegawai->tipe_profesi_id);
    $data['butir_kegiatan'] = $getButir['row']['butir'];
    return ['status'=>'success','data'=>$data];
    return $request->all();
  }

  public function getNilaiSpmk(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->idPengajuan);
    $pegawai = Pegawai::where('user_id',$pengajuan->user_id)->first();
    $master = PakMasterKegiatan::find($request->idPak);
    $penilaian = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                            ->where('master_kegiatan_id', $request->idPak)
                            ->where('user_id', $pengajuan->user_id)
                            ->whereBetween('tanggal',[$request->tglAwal,$request->tglAkhir])
                            ->get();
    if ($penilaian[0]['jumlah_px_pemohon'] != '') {
      $nilaiPx = $penilaian[0]['jumlah_px_pemohon'];
    }else{
      $nilaiPx = 0;
    }
    if ($penilaian[0]['poin_pemohon'] != '') {
      $nilaiPoint = number_format($penilaian[0]['poin_pemohon'], 3);
    }else{
      $nilaiPoint = 0;
    }
    $labelTanggal = date('d-m-y', strtotime($request->tglAwal)).' s/d '.date('d-m-y', strtotime($request->tglAkhir));
    $data = [
      'tanggal' => $labelTanggal,
      'satuan' => $master->satuan,
      'jumlah_px' => $nilaiPx,
      'jumlah_point' => $nilaiPoint,
    ];
    $return = ['status'=>'success','code'=>200,'row'=>$data];
    return response()->json($return);
  }

  public function getJumlahNilaiSpmk(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->id_pengajuan);
    $pegawai = Pegawai::where('user_id',$pengajuan->user_id)->first();
    $unsurs = MasterType::where('modul', 'SatuanUnsur')->where('tipe_profesi_id', $pegawai->tipe_profesi_id)->where('bl_state', 'A')->where('nama','!=','Pendidikan')->get();
    $i = 0;
    foreach ($unsurs as $key) {
      $penilaian = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                              ->join('pak_master_kegiatan','pak_master_kegiatan.id_pak_master','penilaian.master_kegiatan_id')
                              ->where('pak_master_kegiatan.unsur_pak_id', $key->id_master_type)
                              ->where('penilaian.user_id', $pengajuan->user_id)
                              ->whereBetween('penilaian.tanggal',[$request->tgl_awal,$request->tgl_akhir])
                              ->get();
      if ($penilaian[0]['jumlah_px_pemohon'] != '') {
        $nilaiPx = $penilaian[0]['jumlah_px_pemohon'];
      }else{
        $nilaiPx = 0;
      }
      if ($penilaian[0]['poin_pemohon'] != '') {
        $nilaiPoint = number_format($penilaian[0]['poin_pemohon'], 3);
      }else{
        $nilaiPoint = 0;
      }
      $data[$i] = [
        'id_master_type' => $key->id_master_type,
        'jumlah_px' => $nilaiPx,
        'jumlah_point' => $nilaiPoint,
      ];
      $i++;
    }
    $return = ['status'=>'success','code'=>200,'row'=>$data];
    return response()->json($return);
  }

  public function getDataRekapTahun(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->id_pengajuan);
    $pegawai = Pegawai::where('user_id',$pengajuan->user_id)->first();
    $data['pegawai'] = [
      'nama' => $pegawai->nama,
      'nip' => $pegawai->no_nip,
      'golongan' => $pengajuan->golonganPemohonLama->nama,
      'golonganTmt' => date('d-m-Y', strtotime($pengajuan->golongan_lama_tmt)),
      'jabatan' => $pengajuan->jabatanPemohonLama->nama,
      'unitKerja' => $pegawai->satuanKerja->nama,
      'profesi' => $pegawai->typeProfesi->nama,
    ];
    $kepala = Pegawai::find($pengajuan->kepala_id);
    $data['kepala'] = [
      'nama' => $kepala->nama,
      'nip' => $kepala->no_nip,
      'golongan' => $pengajuan->golonganKepala->nama,
      'golonganTmt' => date('d-m-Y', strtotime($pengajuan->golongan_kepala_tmt)),
      'jabatan' => $pengajuan->jabatanKepala->nama,
      'unitKerja' => $pengajuan->satuanKerjaKepala->nama,
    ];
    $getButir = Formatters::get_butir($pegawai->tipe_profesi_id);
    $data['butir_kegiatan'] = $getButir['row']['butir'];
    
    return ['status'=>'success','data'=>$data];
  }

  public function getNilaiRekapTahun(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->id_pengajuan);
    $penilaian = Penilaian::select('id_penilaian','master_kegiatan_id','tanggal','jumlah_px_pemohon','poin_pemohon')
                            ->where('user_id', $pengajuan->user_id)
                            ->whereBetween('tanggal',[$request->tgl_awal,$request->tgl_akhir])
                            ->get();
    if (count($penilaian) > 0) {
      $return = ['status'=>'success','code'=>200,'row'=>$penilaian];
    }else{
      $return = ['status'=>'error','code'=>404,'row'=>''];
    }
    return response()->json($return);
  }

  public function main(Request $request)
  {
    $this->data['mn_active'] = "pengajuanDupak";
    $this->data['submn_active'] = "";
    $this->data['title'] = 'Pengajuan DUPAK';
    $this->data['smallTitle'] = "";

    return view('pengajuanDupak.main')->with('data', $this->data);
  }

  public function loadDataMasa(Request $request)
  {
    if ($request->search != '') {
      $data = DB::table('masa_pengajuan')
                  ->where('tgl_awal','like','%'.$request->search.'%')
                  ->where('bl_state','A')
                  ->orderBy('tgl_awal','DESC')
                  ->paginate($request->lngDt);
    }else{
      $data = DB::table('masa_pengajuan')
                  ->where('bl_state','A')
                  ->orderBy('tgl_awal','DESC')
                  ->paginate($request->lngDt);
    }
    $return = ['status'=>'success','code'=>200,'row'=>$data];
    return response()->json($return);
  }

  public function listPengajuan(Request $request)
  {
    $data['masa'] = MasaPengajuan::find($request->id);
    $data['profesis'] = MasterType::where('modul','StatusProfesi')->where('bl_state','A')->get();
    $content = view('pengajuanDupak.listPengajuan', $data)->render();
    return ['status' => 'success', 'content' => $content];
  }

  public function loadDataList(Request $request)
  { 
    $data['pemohon'] = PengajuanDupak::select('id_pengajuan','pegawai.no_nip as no_nip','pegawai.nama as nama','pengajuan_dupak.tgl_pengajuan as tgl_pengajuan','profesi.nama as nama_profesi','satuan_kerja.nama as nama_satuan','status_pengajuan','penilai.nama as nama_penilai','penilai2.nama as nama_penilai2')
                                        ->join('users','users.id','pengajuan_dupak.user_id')
                                        ->join('pegawai','pegawai.user_id','users.id')
                                        ->join('master_type as profesi','profesi.id_master_type','pegawai.tipe_profesi_id')
                                        ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                                        ->leftJoin('pegawai as penilai','penilai.id_pegawai','pengajuan_dupak.penilai_id')
                                        ->leftJoin('pegawai as penilai2','penilai2.id_pegawai','pengajuan_dupak.penilai2_id')
                                        ->where('masa_pengajuan_id', $request->id_masa)
                                        ->where('pegawai.tipe_profesi_id', $request->id_profesi)
                                        ->where('pengajuan_dupak.bl_state', 'A')
                                        ->orderBy('pegawai.nama','ASC')
                                        ->get();
    $return = ['status'=>'success','code'=>200,'row'=>$data];
    return response()->json($return);
  }

  public function formPenilai(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->id);

    if($pengajuan->status_pengajuan!='Selesai'){
      $data['pengajuan'] = $pengajuan;
      $data['penilais'] = Pegawai::where('status_penilai', 'Ya')->where('bl_state','A')->get();
      if($pengajuan->status_pengajuan=='Proses'){
        $data['proses'] = 'true';
        $data['penilai_pakai'] = Pegawai::find($pengajuan->penilai_id);
        $data['penilai_pakai2'] = Pegawai::find($pengajuan->penilai2_id);
      }
      $content = view('pengajuanDupak.formPenilai', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }else{
      return ['status' => 'error'];
    }
  }

  function detailPenilai(Request $request)
  {
    $id_penilai = $request->id;
    $penilai = Pegawai::find($id_penilai);

    $data = [
      'det_penilai'=>$penilai,
    ];

    if(!empty($penilai)){
      $content = view('pengajuanDupak.detailPenilai',$data)->render();
      return ['status'=>'success','content'=>$content];
    }else{
      return ['status'=>'error'];
    }
  }

  function simpanPenilai(Request $request)
  {
    $id_pengajuan = $request->id_pengajuan;
    $penilai = Pegawai::find($request->penilai);
    $penilai2 = Pegawai::find($request->penilai2);
    $pengajuan = PengajuanDupak::find($id_pengajuan);
    $pengajuan->tgl_respon                = date('Y-m-d');
    $pengajuan->penilai_id                = $penilai->id_pegawai;
    $pengajuan->golongan_penilai_id       = $penilai->golongan_id;
    $pengajuan->jabatan_penilai_id        = $penilai->jabatan_id;
    $pengajuan->satuan_kerja_penilai_id   = $penilai->satuan_kerja_id;
    $pengajuan->penilai2_id               = $penilai2->id_pegawai;
    $pengajuan->golongan_penilai2_id      = $penilai2->golongan_id;
    $pengajuan->jabatan_penilai2_id       = $penilai2->jabatan_id;
    $pengajuan->satuan_kerja_penilai2_id  = $penilai2->satuan_kerja_id;
    $pengajuan->status_pengajuan          = 'Proses';
    $pengajuan->save();

    if($pengajuan){
      return ['status'=>'success'];
    }else{
      return ['status'=>'error'];
    }
  }

  // public function cekSkpakSebelum(Request $request)
  // {
  //   $pengajuan = PengajuanDupak::find($request->id);
  //   if ($pengajuan->penilai_id == '' || $pengajuan->penilai2_id == '') {
  //     return ['status' => 'error', 'message' => 'Tim Penilai Belum Dipilih !!', 'data' => ''];
  //   }else{
  //     $pegawai = Pegawai::where('user_id', $pengajuan->user_id)->first();
  //     $pengajuanSebelum = PengajuanDupak::join('hasil_penetapan_angka_kredit','hasil_penetapan_angka_kredit.pengajuan_id','pengajuan_dupak.id_pengajuan')
  //                                         ->where('tgl_pengajuan','<',$pengajuan->tgl_pengajuan)
  //                                         ->where('user_id', $pegawai->user_id)
  //                                         ->orderBy('tgl_pengajuan','DESC')
  //                                         ->first();
  //     if (!empty($pengajuanSebelum)) {
  //       return ['status' => 'already', 'message' => 'Pengajuan Sebelumnya Ditemukan !!', 'data' => $pengajuanSebelum];
  //     }else{
  //       $data['pengajuanNow'] = $pengajuan;
  //       $data['pegawai'] = Pegawai::where('user_id', $pengajuan->user_id)->first();
  //       $data['unsurs'] = $unsurs = MasterType::where('modul', 'SatuanUnsur')
  //                                             ->where('tipe_profesi_id', $data['pegawai']->tipe_profesi_id)
  //                                             ->where('bl_state', 'A')
  //                                             ->get();
  //       $content = view('pengajuanDupak.formSkpakLama', $data)->render();
  //       return ['status' => 'not found', 'message' => 'Pengajuan Sebelumnya Tidak Ditemukan !!', 'content' => $content];
  //     }
  //   }
  // }

  public function cekSkpakSebelum(Request $request)
  {
    // return $request->all();
    $pengajuan = PengajuanDupak::find($request->id);
    if ($pengajuan->penilai_id == '' || $pengajuan->penilai2_id == '') {
      return ['status' => 'error', 'message' => 'Tim Penilai Belum Dipilih !!', 'data' => ''];
    }else{
      $pegawai = Pegawai::where('user_id', $pengajuan->user_id)->first();
      $pengajuanSebelum = PengajuanDupak::join('hasil_penetapan_angka_kredit','hasil_penetapan_angka_kredit.pengajuan_id','pengajuan_dupak.id_pengajuan')
                                          ->where('tgl_pengajuan','<',$pengajuan->tgl_pengajuan)
                                          ->where('user_id', $pegawai->user_id)
                                          ->orderBy('tgl_pengajuan','DESC')
                                          ->first();
      if (!empty($pengajuanSebelum)) {
        return ['status' => 'already', 'message' => 'Pengajuan Sebelumnya Ditemukan !!', 'data' => $pengajuanSebelum];
      }else{
        $data['pengajuanNow'] = $pengajuan;
        $data['pegawai'] = Pegawai::where('user_id', $pengajuan->user_id)->first();
        $data['unsurs'] = $unsurs = MasterType::where('modul', 'SatuanUnsur')
                                              ->where('tipe_profesi_id', $data['pegawai']->tipe_profesi_id)
                                              ->where('bl_state', 'A')
                                              ->get();
        $content = view('pengajuanDupak.formSkpakLama', $data)->render();
        return ['status' => 'not found', 'message' => 'Pengajuan Sebelumnya Tidak Ditemukan !!', 'content' => $content];
      }
    }
  }

   public function cekSkpakSebelumEdit(Request $request)
  {
      $data['pengajuanLama'] = PengajuanDupak::find($request->idLama);      
      $data['pengajuan'] = PengajuanDupak::find($request->id);
      $data['pengajuanNow'] = $data['pengajuan'];      
      $data['pegawai'] = Pegawai::where('user_id', $data['pengajuan']->user_id)->first();
      $data['pengajuanSebelum'] = PengajuanDupak::join('hasil_penetapan_angka_kredit','hasil_penetapan_angka_kredit.pengajuan_id','pengajuan_dupak.id_pengajuan')
                                      ->where('tgl_pengajuan','<',$data['pengajuan']->tgl_pengajuan)
                                      ->where('user_id', $data['pegawai']->user_id)
                                      ->orderBy('tgl_pengajuan','DESC')
                                      ->first();
      $data['nilaiPakLamaUtama'] = MasterType::where('modul', 'SatuanUnsur')
                                              ->where('tipe_profesi_id', $data['pegawai']->tipe_profesi_id)
                                              ->where('bl_state', 'A')
                                              ->get();
      $data['unsurs'] = HasilAngkaKredit::select('hasil_penetapan_angka_kredit.id_pak', 'hasil_penetapan_angka_kredit.unsur_id','hasil_penetapan_angka_kredit.jumlah_nilai as jumlah_nilai','hasil_penetapan_angka_kredit.nilai_baru as jml_baru','hasil_penetapan_angka_kredit.nilai_lama as jml_lama','hasil_penetapan_angka_kredit.keterangan as keterangan','master_type.nama as nama')
                                              ->join('pengajuan_dupak','pengajuan_dupak.id_pengajuan','hasil_penetapan_angka_kredit.pengajuan_id')
                                              ->join('master_type','master_type.id_master_type','hasil_penetapan_angka_kredit.unsur_id')
                                              ->where('hasil_penetapan_angka_kredit.pengajuan_id', $data['pengajuanLama']->id_pengajuan)
                                              ->orderBy('id_master_type','ASC')
                                              ->orderBy('hasil_penetapan_angka_kredit.id_pak','ASC')
                                              ->get();

      $masa_lama = explode(' ',$data['pengajuanLama']->masa_kerja_lama);
      $masa_baru = explode(' ',$data['pengajuanLama']->masa_kerja_baru);
      
      $data['masa_lama_tahun'] = $masa_lama[0];
      $data['masa_lama_bulan'] = $masa_lama[2];
      $data['masa_baru_tahun'] = $masa_baru[0];
      $data['masa_baru_bulan'] = $masa_baru[2];

      $content = view('pengajuanDupak.formSkpakLamaEdit', $data)->render();
      return ['status' => 'already', 'message' => 'Pengajuan Sebelumnya Ditemukan !!', 'content' => $content];      
  }

  public function saveSkpakSebelum(Request $request)
  {    
    $pengajuanBaru = PengajuanDupak::find($request->id_pengajuanNow);
    $pengajuanLama = (!empty($request->id_pengajuan)) ? PengajuanDupak::find($request->id_pengajuan) : new PengajuanDupak;
    $pengajuanLama->masa_pengajuan_id = 0;
    $pengajuanLama->no_skpak = '-';
    $pengajuanLama->tgl_pengajuan = '1990-01-01';
    $pengajuanLama->tgl_awal_penilaian = '1990-01-01';
    $pengajuanLama->tgl_akhir_penilaian = '1990-01-01';
    $pengajuanLama->status_pengajuan = 'Selesai';
    $pengajuanLama->tgl_respon = '1990-01-01';
    $pengajuanLama->user_id = $pengajuanBaru->user_id;
    $masa_kerja_lama = $request->tahun_masa_kerja_lama.' Tahun '.$request->bulan_masa_kerja_lama.' Bulan';
    $pengajuanLama->masa_kerja_lama = $masa_kerja_lama;
    $masa_kerja_baru = $request->tahun_masa_kerja_baru.' Tahun '.$request->bulan_masa_kerja_baru.' Bulan';
    $pengajuanLama->masa_kerja_baru = $masa_kerja_baru;
    $pengajuanLama->bl_state = 'A';
    $pengajuanLama->save();

    if ($pengajuanLama) {
      $listUnsur = $request->unsur_id;
      $listLama = $request->nilai_lama;
      $listBaru = $request->nilai_baru;
      $listJumlah = $request->nilai_jumlah;
      $listKeterangan = $request->keterangan;
      $jumlahPointLama = 0;
      
      for ($i=0; $i < count($listUnsur); $i++) {
        $hasilskpak = (!empty($request->id_pengajuan)) ? HasilAngkaKredit::where('pengajuan_id',$request->id_pengajuan)->where('unsur_id',$listUnsur[$i])->first() : new HasilAngkaKredit;        
        $hasilskpak->pengajuan_id = $pengajuanLama->id_pengajuan;
        $hasilskpak->unsur_id = $listUnsur[$i];
        $hasilskpak->nilai_lama = $listLama[$i];
        $hasilskpak->nilai_baru = $listBaru[$i];
        $hasilskpak->jumlah_nilai = $listJumlah[$i];
        $hasilskpak->keterangan = $listKeterangan[$i];
        $hasilskpak->save();
        $jumlahPointLama = $jumlahPointLama + $listJumlah[$i];
      }

      $updatePegawai = Pegawai::where('user_id', $pengajuanBaru->user_id)->first();
      $updatePegawai->masa_kerja = $masa_kerja_baru;
      $updatePegawai->point_pegawai = $jumlahPointLama;
      $updatePegawai->save();

      $data['pengajuan_baru'] = $pengajuanBaru;
      $data['pengajuan_lama'] = $pengajuanLama;
      return ['status' => 'success', 'message' => 'Pengajuan Sebelumnya Berhasil Disimpan !!', 'data' => $data];
    }else{
      return ['status' => 'error', 'message' => 'Pengajuan Sebelumnya Gagal Disimpan !!', 'data' => ''];
    }
  }

  public function getTelaah(Request $request)
  {
    // return $request->all();
    $pengajuanLama = PengajuanDupak::find($request->idLama);
    $pengajuan = PengajuanDupak::find($request->idBaru);
    $pegawai = Pegawai::where('user_id', $pengajuan->user_id)->first();
    if ($pengajuan->jabatan_baru_id == null && $pengajuan->golongan_baru_id == null) {
      // start cek Jabatan atau Golongan yang Naik
        $golonganPegawai = DataMaster::find($pegawai->golongan_id); //6
        $jabatanPegawai = DataMaster::find($pegawai->jabatan_id); // 66
        $standartJabatanPegawai = StandartMaster::where('mst_id', $golonganPegawai->id_master)->first(); // 27
        
        if ($jabatanPegawai->id_master == $standartJabatanPegawai->master_id) {
          $urutNext = $golonganPegawai->urutan + 1;
          $golonganNextPegawai = DataMaster::where('is_golongan','Y')
                                            ->where('urutan', $urutNext)
                                            ->where('bl_state', 'A')
                                            ->first();
          // Cari Target Point
          $pointNext = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                      ->where('standart_master.master_id', $pegawai->jabatan_id)
                                      ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                      ->where('standart_master.bl_state', 'A')
                                      ->first();
          $status = 'Naik Golongan';
          // Cek Naik Jabatan Atau Tidak
          if (empty($pointNext)) {
            $urutJabatanNext = $jabatanPegawai->urutan + 1;
            $jabatanNextPegawai = DataMaster::where('is_golongan','N')
                                              ->where('tipe_profesi_id', $pegawai->tipe_profesi_id)
                                              ->where('urutan', $urutJabatanNext)
                                              ->where('bl_state', 'A')
                                              ->first();
            $golonganNextPegawai = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                                  ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                                  ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                                  ->where('standart_master.bl_state', 'A')
                                                  ->first();
            $pointNext = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                          ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                          ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                          ->where('standart_master.bl_state', 'A')
                                          ->first();
            $status = 'Naik Jabatan';
          }
          if ($status == 'Naik Jabatan') {
            $jabatan_baru_id = $jabatanNextPegawai->id_master;
          }else{
            $jabatan_baru_id = $pegawai->jabatan_id;
          }
          $golongan_baru_id = $golonganNextPegawai->id_master;
          $pointTarget = $pointNext->poin;
        }else{
          // Pengecekan Jika Golongan Lebih Tinggi Dari Jabatan
          $golonganPegawai = DataMaster::find($pegawai->golongan_id); //6                    
          $urutJabatanNext = $jabatanPegawai->urutan + 1; //3

          $jabatanNextPegawai = DataMaster::where('is_golongan','N')
                                            ->where('tipe_profesi_id', $pegawai->tipe_profesi_id)
                                            ->where('urutan', $urutJabatanNext)
                                            ->where('bl_state', 'A')
                                            ->first(); //id_master 67
          $pointMinimalSeharusnya = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                                ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                                ->where('standart_master.mst_id', $golonganPegawai->id_master)
                                                ->where('standart_master.bl_state', 'A')
                                                ->first();
          if ($pegawai->point_pegawai >= $pointMinimalSeharusnya->poin) {
            // golongan sesuai
            $urutNext = $golonganPegawai->urutan + 1;
            $golonganPegawai = DataMaster::find($pegawai->golongan_id);                                
            $urutGolonganNext = $golonganPegawai->urutan + 1;
            $golonganNextPegawai = DataMaster::where('is_golongan','Y')
                                              ->where('urutan', $urutNext)
                                              ->where('bl_state', 'A')
                                              ->first();
            $pointNext = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                        ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                        ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                        ->where('standart_master.bl_state', 'A')
                                        ->first();
            $status = 'Naik Jabatan';
            $jabatan_baru_id = $jabatanNextPegawai->id_master;
            $golongan_baru_id = $golonganNextPegawai->id_master;
            $pointTarget = $pointNext->poin;
          }else{
            $getTargetPoint = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                        ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                        ->where('standart_master.poin', '>', $pegawai->point_pegawai)
                                        ->where('standart_master.bl_state', 'A')
                                        ->first();
            $jabatan_baru_id = $jabatanNextPegawai->id_master;
            $golongan_baru_id = $golonganPegawai->id_master;
            $pointTarget = $getTargetPoint->poin;
          }
        }
      // End cek Jabatan atau Golongan yang Naik
      // Start Update Pengajuan
        $updatePengajuan                    = PengajuanDupak::find($pengajuan->id_pengajuan);
        $updatePengajuan->jabatan_baru_id   = $jabatan_baru_id;
        $updatePengajuan->golongan_baru_id  = $golongan_baru_id;
        $updatePengajuan->min_point_ak      = $pointTarget;
        $updatePengajuan->save();
      // End Update Pengajuan
    }
    $data['pengajuan'] = PengajuanDupak::find($pengajuan->id_pengajuan);
    $data['pegawai'] = $pegawai;
    $data['umum'] = DataSetting::find(1);
    $data['nilaiPakLamaUtama'] = HasilAngkaKredit::select('hasil_penetapan_angka_kredit.id_pak', 'hasil_penetapan_angka_kredit.unsur_id','hasil_penetapan_angka_kredit.jumlah_nilai as jumlah_nilai','hasil_penetapan_angka_kredit.keterangan as keterangan','master_type.nama as nama')
                                              ->join('pengajuan_dupak','pengajuan_dupak.id_pengajuan','hasil_penetapan_angka_kredit.pengajuan_id')
                                              ->join('master_type','master_type.id_master_type','hasil_penetapan_angka_kredit.unsur_id')
                                              ->where('master_type.keterangan','Unsur Utama')
                                              ->where('hasil_penetapan_angka_kredit.pengajuan_id', $pengajuanLama->id_pengajuan)
                                              ->orderBy('id_master_type','ASC')
                                              ->orderBy('hasil_penetapan_angka_kredit.id_pak','ASC')
                                              ->get();
    $data['nilaiPakLamaPenunjang'] = HasilAngkaKredit::select('hasil_penetapan_angka_kredit.id_pak', 'hasil_penetapan_angka_kredit.unsur_id','hasil_penetapan_angka_kredit.jumlah_nilai as jumlah_nilai','hasil_penetapan_angka_kredit.keterangan as keterangan','master_type.nama as nama')
                                              ->join('pengajuan_dupak','pengajuan_dupak.id_pengajuan','hasil_penetapan_angka_kredit.pengajuan_id')
                                              ->join('master_type','master_type.id_master_type','hasil_penetapan_angka_kredit.unsur_id')
                                              ->where('master_type.keterangan','Unsur Penunjang')
                                              ->where('hasil_penetapan_angka_kredit.pengajuan_id', $pengajuanLama->id_pengajuan)
                                              ->orderBy('id_master_type','ASC')
                                              ->orderBy('hasil_penetapan_angka_kredit.id_pak','ASC')
                                              ->get();
    // return view('pengajuanDupak.cetakTelaah', $data);
     // return $data;
    $pdf = PDF::loadView('pengajuanDupak.cetakTelaah', $data)
                ->setPaper([0, 0, 609.4488, 935.433], 'portrait');
                // ->setPaper('a4', 'portrait');
                // ->setPaper('a4', 'landscape');
    return $pdf->stream('Telaah Tim.pdf');
  }

  public function formNoSKPAK(Request $request)
  {
    $idPengajuan = $request->id;
    $cekPengajuan = PengajuanDupak::where('status_pengajuan', '!=', 'Selesai')->whereIn('id_pengajuan', $idPengajuan)->get();
    if (count($cekPengajuan) == 0) {
      $listPengajuan = PengajuanDupak::whereIn('id_pengajuan', $idPengajuan)->get();
      $no = 0;
      $dataPengajuan = [];
      foreach ($listPengajuan as $key) {
        $pengajuan = PengajuanDupak::select('id_pengajuan','pegawai.nama','pegawai.no_nip','pegawai.no_karpeng','jabatan_lama_id','jabatan_baru_id')
                                    ->join('pegawai','pegawai.user_id','pengajuan_dupak.user_id')
                                    ->where('id_pengajuan', $key->id_pengajuan)->first();
        if ($pengajuan->jabatan_lama_id == $pengajuan->jabatan_baru_id) {
          $statusTmtJabatan = 'Already';
        }else{
          $statusTmtJabatan = 'Request';
        }
        $dataPengajuan[$no] = $pengajuan;
        $dataPengajuan[$no]['statusTmtJabatan'] = $statusTmtJabatan;
        $no++;
      }
      $data['pengajuans'] = $dataPengajuan;
      $data['umum'] = DataSetting::find(1);
      $content = view('pengajuanDupak.formNoSKPAK', $data)->render();
      return ['status' => 'success', 'message' => 'Pengajuan Sebelumnya Tidak Ditemukan !!', 'content' => $content];
    }else{
      return ['status' => 'error', 'message' => 'Terdapat Pengajuan yang Belum Selesai Dilakukan Penilaian !!', 'data' => ''];
    }
  }

  public function saveNoSKPAK(Request $request)
  {
    $idPengajuan = $request->id_pengajuan;
    $noSkpak = $request->no_skpak;
    $tglSkpak = $request->tgl_skpak;
    $statusTmtJabatan = $request->statusTmtJabatan;
    $jabatanBaruTmt = $request->jabatan_baru_tmt;
    $umum = DataSetting::find(1);
    for ($i=0; $i < count($idPengajuan); $i++) {
      return $pengajuan = PengajuanDupak::find($idPengajuan[$i]);
      $noSk = $umum->kode_skpak.' / '.$noSkpak[$i].' / '.$umum->kode_dinas.' / '.date('Y');
      $pengajuan->no_skpak            = $noSk;
      if ($jabatanBaruTmt[$i] != '0000-00-00') {
        $pengajuan->jabatan_baru_tmt    = date('Y-m-d', strtotime($jabatanBaruTmt[$i]));
      }
      $bulanPengerjaan = date('m', strtotime($pengajuan->tgl_akhir_penilaian));
      $tahunPengerjaan = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
      if ($bulanPengerjaan == 12) {
        $bulanGolTmt = '10';
      }else{
        $bulanGolTmt = '06';
      }
      $tahunGolTmt = $tahunPengerjaan + 1;
      $pengajuan->golongan_baru_tmt   = $tahunGolTmt.'-'.$bulanGolTmt.'-01';
      $pengajuan->tgl_skpak           = date('Y-m-d', strtotime($tglSkpak[$i]));

      // Start Menghitung Masa Kerja
      $birthDt = new \DateTime($pengajuan->tgl_awal_penilaian);
      $today = new \DateTime($pengajuan->tgl_akhir_penilaian);
      $y = $today->diff($birthDt)->y;
      $m = $today->diff($birthDt)->m;
      $d = $today->diff($birthDt)->d;

      $masaKerjaLama = explode(' Tahun ', $pengajuan->masa_kerja_lama);;
      $tahunMKLama = $masaKerjaLama[0];
      $pecahBulanMKLama = explode(' ', $masaKerjaLama[1]);
      $bulanMKLama = $pecahBulanMKLama[0];

      $tempTahunMKBaru = 0;
      $cekBulanMKBaru = $bulanMKLama + $m;
      if ($cekBulanMKBaru > 12) {
        $bulanMKBaru = $cekBulanMKBaru - 12;
        $tempTahunMKBaru = 1;
      }else{
        $bulanMKBaru = $cekBulanMKBaru;
      }
      $tahunMKBaru = $tahunMKLama + $y + $tempTahunMKBaru;
      $pengajuan->masa_kerja_baru   = $tahunMKBaru.' Tahun '.$bulanMKBaru.' Bulan';
      $pengajuan->save();
    }

    return ['status'=>'success', 'message'=>'No SKPAK Berhasil Disimpan !!', 'data'=>''];
  }

  public function cekSKPAKPengajuan(Request $request)
  {
    $pengajuan = PengajuanDupak::find($request->id);
    if ($pengajuan->status_penilai1 == 'Y' && $pengajuan->status_penilai2 == 'Y') {
      if ($pengajuan->status_hasil == 'Naik') {
        if ($pengajuan->no_skpak != '' && $pengajuan->no_skpak != '-') {
          return ['status'=>'success', 'message'=>'Penilaian Selesai !!', 'data'=>$pengajuan];
        }else{
          return ['status'=>'error', 'message'=>'No SKPAK Belum Dibuat !!', 'type'=>'error'];
        }
      }else{
        return ['status'=>'error', 'message'=>'Point Pemohon Tidak Mencapai Nilai Angka Kredit Minimal !!', 'type'=>'error'];
      }
    }else{
      return ['status'=>'error', 'message'=>'Penilaian Belum Selesai Dilakukan !!', 'type'=>'error'];
    }
  }

  public function getSKPAKPengajuan(Request $request)
  {
    $data['all'] = $request->all();
    $pengajuan = PengajuanDupak::find($request->id);
    $pegawai = Pegawai::where('user_id', $pengajuan->user_id)->first();

    $data['pengajuan'] = $pengajuan;
    $data['pegawai'] = $pegawai;
    $data['umum'] = DataSetting::find(1);
    // $unsur = MasterType::where('tipe_profesi_id', $pegawai->tipe_profesi_id)
    //                     ->where('modul','SatuanUnsur')
    //                     ->where('bl_state','A')
    //                     ->get();
    // $data['unsurs'] = $unsur;

    $pengajuanSebelum = PengajuanDupak::where('tgl_pengajuan','<',$pengajuan->tgl_pengajuan)
                                        ->where('user_id', $pegawai->user_id)
                                        ->orderBy('tgl_pengajuan','DESC')
                                        ->first();
    $no = 0;
    $dataUnsurUtama = [];
    $unsurUtamas = MasterType::where('tipe_profesi_id', $pegawai->tipe_profesi_id)
                        ->where('modul','SatuanUnsur')
                        ->where('keterangan','Unsur Utama')
                        ->where('bl_state','A')
                        ->get();
    foreach ($unsurUtamas as $keyUtama) {
      if ($keyUtama->nama == 'Pendidikan') {
        $dataUnsurUtama[$no]['nama'] = $keyUtama->nama;
        $dataUnsurUtama[$no]['keterangan'] = 'Formal';
        $nilaiLamaFormal = HasilAngkaKredit::where('pengajuan_id', $pengajuanSebelum->id_pengajuan)
                                      ->where('unsur_id', $keyUtama->id_master_type)
                                      ->where('keterangan', 'Formal')
                                      ->first();
        $dataUnsurUtama[$no]['nilai_lama'] = $nilaiLamaFormal->jumlah_nilai;
        $nilaiBaruFormal = Penilaian::selectRaw('SUM(poin_penilai) as poin_penilai')
                                    ->join('pak_master_kegiatan','pak_master_kegiatan.id_pak_master','penilaian.master_kegiatan_id')
                                    ->where('pak_master_kegiatan.keterangan', 'Formal')
                                    ->where('pak_master_kegiatan.unsur_pak_id', $keyUtama->id_master_type)
                                    ->where('user_id', $pengajuan->user_id)
                                    ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                    ->get();
        $dataUnsurUtama[$no]['nilai_baru'] = $nilaiBaruFormal[0]['poin_penilai'];
        $dataUnsurUtama[$no]['nilai_jumlah'] = $nilaiLamaFormal->jumlah_nilai + $nilaiBaruFormal[0]['poin_penilai'];
        $no++;
        $dataUnsurUtama[$no]['nama'] = $keyUtama->nama;
        $dataUnsurUtama[$no]['keterangan'] = 'Pelatihan';
        $nilaiLamaPelatihan = HasilAngkaKredit::where('pengajuan_id', $pengajuanSebelum->id_pengajuan)
                                      ->where('unsur_id', $keyUtama->id_master_type)
                                      ->where('keterangan', 'Pelatihan')
                                      ->first();
        $dataUnsurUtama[$no]['nilai_lama'] = $nilaiLamaPelatihan->jumlah_nilai;
        $nilaiBaruPelatihan = Penilaian::selectRaw('SUM(poin_penilai) as poin_penilai')
                                    ->join('pak_master_kegiatan','pak_master_kegiatan.id_pak_master','penilaian.master_kegiatan_id')
                                    ->where('pak_master_kegiatan.keterangan', 'Pelatihan')
                                    ->where('pak_master_kegiatan.unsur_pak_id', $keyUtama->id_master_type)
                                    ->where('user_id', $pengajuan->user_id)
                                    ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                    ->get();
        $dataUnsurUtama[$no]['nilai_baru'] = $nilaiBaruPelatihan[0]['poin_penilai'];
        $dataUnsurUtama[$no]['nilai_jumlah'] = $nilaiLamaPelatihan->jumlah_nilai + $nilaiBaruPelatihan[0]['poin_penilai'];
        $no++;
      }else{
        $dataUnsurUtama[$no]['nama'] = $keyUtama->nama;
        $dataUnsurUtama[$no]['keterangan'] = '';
        $nilaiLama = HasilAngkaKredit::where('pengajuan_id', $pengajuanSebelum->id_pengajuan)
                                      ->where('unsur_id', $keyUtama->id_master_type)
                                      ->first();
        $dataUnsurUtama[$no]['nilai_lama'] = $nilaiLama->jumlah_nilai;
        $nilaiBaru = Penilaian::selectRaw('SUM(poin_penilai) as poin_penilai')
                                    ->join('pak_master_kegiatan','pak_master_kegiatan.id_pak_master','penilaian.master_kegiatan_id')
                                    ->where('pak_master_kegiatan.unsur_pak_id', $keyUtama->id_master_type)
                                    ->where('user_id', $pengajuan->user_id)
                                    ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                    ->get();
        $dataUnsurUtama[$no]['nilai_baru'] = $nilaiBaru[0]['poin_penilai'];
        $dataUnsurUtama[$no]['nilai_jumlah'] = $nilaiLama->jumlah_nilai + $nilaiBaru[0]['poin_penilai'];
        $no++;
      }
    }
    $data['nilaiPakLamaUtama'] = $dataUnsurUtama;
    $noP = 0;
    $dataUnsurPenunjang = [];
    $unsurPenunjangs = MasterType::where('tipe_profesi_id', $pegawai->tipe_profesi_id)
                        ->where('modul','SatuanUnsur')
                        ->where('keterangan','Unsur Penunjang')
                        ->where('bl_state','A')
                        ->get();
    foreach ($unsurPenunjangs as $keyPenunjang) {
      $dataUnsurPenunjang[$noP]['nama'] = $keyPenunjang->nama;
      $dataUnsurPenunjang[$noP]['keterangan'] = '';
      $nilaiLama = HasilAngkaKredit::where('pengajuan_id', $pengajuanSebelum->id_pengajuan)
                                    ->where('unsur_id', $keyPenunjang->id_master_type)
                                    ->first();
      $dataUnsurPenunjang[$noP]['nilai_lama'] = $nilaiLama->jumlah_nilai;
      $nilaiBaru = Penilaian::selectRaw('SUM(poin_penilai) as poin_penilai')
                                  ->join('pak_master_kegiatan','pak_master_kegiatan.id_pak_master','penilaian.master_kegiatan_id')
                                  ->where('pak_master_kegiatan.unsur_pak_id', $keyPenunjang->id_master_type)
                                  ->where('user_id', $pengajuan->user_id)
                                  ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                  ->get();
      $dataUnsurPenunjang[$noP]['nilai_baru'] = $nilaiBaru[0]['poin_penilai'];
      $dataUnsurPenunjang[$noP]['nilai_jumlah'] = $nilaiLama->jumlah_nilai + $nilaiBaru[0]['poin_penilai'];
      $noP++;
    }
    $data['nilaiPakLamaPenunjang'] = $dataUnsurPenunjang;
    // return view('pengajuanDupak.cetakSkPak', $data);
    $pdf = PDF::loadView('pengajuanDupak.cetakSkPak', $data)
                ->setPaper([0, 0, 609.4488, 935.433], 'portrait');
                // ->setPaper('a4', 'portrait');
                // ->setPaper('a4', 'landscape');
    return $pdf->stream('SK PAK.pdf');
  }

  // End sudah cek






  // gak tau ini dipakek apa gak
  public function getCetakPernyataanPelayanan(Request $request)
  {
    $butir = [];
    $no = 0;
    $unsur = PakMasterKegiatan::select('pak_master_kegiatan.*')
                                ->join('master_type','master_type.id_master_type','pak_master_kegiatan.unsur_pak_id')
                                ->where('master_type.modul','SatuanUnsur')
                                ->where('pak_master_kegiatan.formasi_id', $request->id_profesi)
                                ->where('pak_master_kegiatan.jenis','Butir')
                                // ->where('pak_master_kegiatan.level_1','1')
                                ->where('pak_master_kegiatan.bl_state', 'A')->limit(181)->get();

    if (count($unsur) > 0) {
      foreach ($unsur as $uns) {
        $butir[$no] = $uns;
        $lv1 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                  ->where('unsur_pak_id', $uns->id_master_type)
                                  ->where('jenis', 'Butir')
                                  ->where('level', '1')
                                  ->where('bl_state', 'A')
                                  ->get();
        if (count($lv1) > 0) {
          $butir1 = [];
          $noLv1 = 0;
          foreach ($lv1 as $key1) {
            $butir1[$noLv1] = $key1;
            $lv2 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                      ->where('unsur_pak_id', $uns->id_master_type)
                                      ->where('jenis', 'Butir')
                                      ->where('level', '2')
                                      ->where('bl_state', 'A')
                                      ->where('parent_id', $key1->id_pak_master)
                                      ->get();
            if (count($lv2) > 0) {
              $butir2 = [];
              $noLv2 = 0;
              foreach ($lv2 as $key2) {
                $butir2[$noLv2] = $key2;
                $lv3 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                          ->where('unsur_pak_id', $uns->id_master_type)
                                          ->where('jenis', 'Butir')
                                          ->where('level', '3')
                                          ->where('bl_state', 'A')
                                          ->where('parent_id', $key2->id_pak_master)
                                          ->get();
                if (count($lv3)) {
                  $butir3 = [];
                  $noLv3 = 0;
                  foreach ($lv3 as $key3) {
                    $butir3[$noLv3] = $key3;
                    $lv4 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                              ->where('unsur_pak_id', $uns->id_master_type)
                                              ->where('jenis', 'Butir')
                                              ->where('level', '4')
                                              ->where('bl_state', 'A')
                                              ->where('parent_id', $key3->id_pak_master)
                                              ->get();
                    if (count($lv4) > 0) {
                      $butir4 = [];
                      $noLv4 = 0;
                      foreach ($lv4 as $key4) {
                        $butir4[$noLv4] = $key4;
                        $lv5 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                                  ->where('unsur_pak_id', $uns->id_master_type)
                                                  ->where('jenis', 'Butir')
                                                  ->where('level', '5')
                                                  ->where('bl_state', 'A')
                                                  ->where('parent_id', $key4->id_pak_master)
                                                  ->get();
                        if (count($lv5) > 0) {
                          $butir5 = [];
                          $noLv5 = 0;
                          foreach ($lv5 as $key5) {
                            $butir5[$noLv5] = $key5;
                            $lv6 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                                      ->where('unsur_pak_id', $uns->id_master_type)
                                                      ->where('jenis', 'Butir')
                                                      ->where('level', '6')
                                                      ->where('bl_state', 'A')
                                                      ->where('parent_id', $key5->id_pak_master)
                                                      ->get();
                            if (count($lv6) > 0) {
                              $butir6 = [];
                              $noLv6 = 0;
                              foreach ($lv6 as $key6) {
                                $butir6[$noLv6] = $key6;
                                if ($key6->is_title == '1') {
                                  $butir6[$noLv6]['statusRow'] = 'Parent';
                                }else{
                                  $butir6[$noLv6]['statusRow'] = 'Child';
                                  $butir6[$noLv6]['nama_jabatan'] = $key6->jabatan->nama;
                                }
                                $butir6[$noLv6]['row'] = [];
                                $noLv6++;
                              }
                              $butir5[$noLv5]['statusRow'] = 'Parent';
                              $butir5[$noLv5]['row'] = $butir6;
                            }else{
                              if ($key5->is_title == '1') {
                                $butir5[$noLv5]['statusRow'] = 'Parent';
                              }else{
                                $butir5[$noLv5]['statusRow'] = 'Child';
                                $butir5[$noLv5]['nama_jabatan'] = $key5->jabatan->nama;
                              }
                              $butir5[$noLv5]['row'] = [];
                            }
                            $noLv5++;
                          }
                          $butir4[$noLv4]['statusRow'] = 'Parent';
                          $butir4[$noLv4]['row'] = $butir5;
                        }else{
                          if ($key4->is_title == '1') {
                            $butir4[$noLv4]['statusRow'] = 'Parent';
                          }else{
                            $butir4[$noLv4]['statusRow'] = 'Child';
                            $butir4[$noLv4]['nama_jabatan'] = $key4->jabatan->nama;
                          }
                          $butir4[$noLv4]['row'] = [];
                        }
                        $noLv4++;
                      }
                      $butir3[$noLv3]['statusRow'] = 'Parent';
                      $butir3[$noLv3]['row'] = $butir4;
                    }else{
                      if ($key3->is_title == '1') {
                        $butir3[$noLv3]['statusRow'] = 'Parent';
                      }else{
                        $butir3[$noLv3]['statusRow'] = 'Child';
                        $butir3[$noLv3]['nama_jabatan'] = $key3->jabatan->nama;
                      }
                      $butir3[$noLv3]['row'] = [];
                    }
                    $noLv3++;
                  }
                  $butir2[$noLv2]['statusRow'] = 'Parent';
                  $butir2[$noLv2]['row'] = $butir3;
                }else{
                  if ($key2->is_title == '1') {
                    $butir2[$noLv2]['statusRow'] = 'Parent';
                  }else{
                    $butir2[$noLv2]['statusRow'] = 'Child';
                    $butir2[$noLv2]['nama_jabatan'] = $key2->jabatan->nama;
                  }
                  $butir2[$noLv2]['row'] = [];
                }
                $noLv2++;
              }
              $butir1[$noLv1]['statusRow'] = 'Parent';
              $butir1[$noLv1]['row'] = $butir2;
            }else{
              if ($key1->is_title == '1') {
                $butir1[$noLv1]['statusRow'] = 'Parent';
              }else{
                $butir1[$noLv1]['statusRow'] = 'Child';
                $butir1[$noLv1]['nama_jabatan'] = $key1->jabatan->nama;
              }
              $butir1[$noLv1]['row'] = [];
            }
            $noLv1++;
          }
          $butir[$no]['statusRow'] = 'Parent';
          $butir[$no]['row'] = $butir1;
        }else{
          $butir[$no]['statusRow'] = 'Parent';
          $butir[$no]['row'] = [];
        }
        $no++;
      }
      $data['butir'] = $butir;
      $return = ['status'=>'success','code'=>200,'row'=>$data];
    }else{
      $data['butir'] = $butir;
      $return = ['status'=>'empty','code'=>404,'row'=>$data];
    }
    return response()->json($return);
  }

  function getInduk(Request $request)
  {
    $induk = [];
    $lv1 = PakMasterKegiatan::where('jenis','Butir')
                              ->where('formasi_id', $request->idProfesi)
                              ->where('unsur_pak_id', $request->unsur)
                              ->where('level', '1')
                              ->where('is_title', '1')
                              ->where('bl_state', 'A')->get();
    if (count($lv1) > 0) {
      $noLv1 = 0;
      foreach ($lv1 as $key1) {
        $induk[$noLv1] = $key1;
        $lv2 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                  ->where('unsur_pak_id', $request->unsur)
                                  ->where('jenis', 'Butir')
                                  ->where('level', '2')
                                  ->where('is_title', '1')
                                  ->where('bl_state', 'A')
                                  ->where('parent_id', $key1->id_pak_master)
                                  ->get();
        if (count($lv2) > 0) {
          $induk2 = [];
          $induk[$noLv1]['statusRow'] = 'Parent';
          $noLv2 = 0;
          foreach ($lv2 as $key2) {
            $induk2[$noLv2] = $key2;
            $lv3 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                      ->where('unsur_pak_id', $request->unsur)
                                      ->where('jenis', 'Butir')
                                      ->where('level', '3')
                                      ->where('is_title', '1')
                                      ->where('bl_state', 'A')
                                      ->where('parent_id', $key2->id_pak_master)
                                      ->get();
            if (count($lv3) > 0) {
              $induk3 = [];
              $induk2[$noLv2]['statusRow'] = 'Parent';
              $noLv3 = 0;
              foreach ($lv3 as $key3) {
                $induk3[$noLv3] = $key3;
                $lv4 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                          ->where('unsur_pak_id', $request->unsur)
                                          ->where('jenis', 'Butir')
                                          ->where('level', '4')
                                          ->where('is_title', '1')
                                          ->where('bl_state', 'A')
                                          ->where('parent_id', $key3->id_pak_master)
                                          ->get();
                if (count($lv4) > 0) {
                  $induk4 = [];
                  $induk3[$noLv3]['statusRow'] = 'Parent';
                  $noLv4 = 0;
                  foreach ($lv4 as $key4) {
                    $induk4[$noLv4] = $key4;
                    $lv5 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                              ->where('unsur_pak_id', $request->unsur)
                                              ->where('jenis', 'Butir')
                                              ->where('level', '5')
                                              ->where('is_title', '1')
                                              ->where('bl_state', 'A')
                                              ->where('parent_id', $key4->id_pak_master)
                                              ->get();
                    if (count($lv5) > 0) {
                      $induk5 = [];
                      $induk4[$noLv4]['statusRow'] = 'Parent';
                      $noLv5 = 0;
                      foreach ($lv5 as $key5) {
                        $induk5[$noLv5] = $key5;
                        $lv6 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                                  ->where('unsur_pak_id', $request->unsur)
                                                  ->where('jenis', 'Butir')
                                                  ->where('level', '6')
                                                  ->where('is_title', '1')
                                                  ->where('bl_state', 'A')
                                                  ->where('parent_id', $key5->id_pak_master)
                                                  ->get();
                        if (count($lv6) > 0) {
                          $induk6 = [];
                          $induk5[$noLv5]['statusRow'] = 'Parent';
                          $noLv6 = 0;
                          $induk6[$noLv6] = $lv6;
                        }else{
                          $induk5[$noLv5]['row'] = [];
                        }
                        $noLv5++;
                      }
                      $induk4[$noLv4]['row'] = $induk5;
                    }else{
                      $induk4[$noLv4]['row'] = [];
                    }
                    $noLv4++;
                  }
                  $induk3[$noLv3]['row'] = $induk4;
                }else{
                  $induk3[$noLv3]['row'] = [];
                }
                $noLv3++;
              }
              $induk2[$noLv2]['row'] = $induk3;
            }else{
              $induk2[$noLv2]['row'] = [];
            }
            $noLv2++;
          }
          $induk[$noLv1]['row'] = $induk2;
        }else{
          $induk[$noLv1]['statusRow'] = 'Parent';
          $induk[$noLv1]['row'] = [];
        }
        $noLv1++;
      }
    }

    $data['induk'] = $induk;
    return ['status' => 'success', 'data' => $data];
  }
}
