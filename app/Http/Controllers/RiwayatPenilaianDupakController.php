<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\TimPenilai;
use App\Models\PengajuanDupak;
use App\Models\PakMasterKegiatan;
use App\Models\Penilaian;
use App\Models\Pegawai;
use App\Models\Users;
use App\Models\StandartMaster;
use App\Models\DetailKegiatanPegawai;
use App\Models\RiwayatPenilaian;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;


class RiwayatPenilaianDupakController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "RiwayatpenilaianDupak";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Riwayat Penilaian DUPAK';
      $this->data['smallTitle'] = "";

      return view('RiwayatpenilaianDupak.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
    	$pegawai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        if ($request->search != '') {
          $data = DB::table('pengajuan_dupak')
                      ->select('id_pengajuan','pegawai.nama as nama', 'pegawai.no_nip', 'golongan.nama as nama_golongan', 'jabatan.nama as nama_jabatan','satuan_kerja.nama as nama_satuan_kerja','pengajuan_dupak.status_pengajuan as status','pengajuan_dupak.status_penilai1 as status_penilai1','pengajuan_dupak.status_penilai2 as status_penilai2','pengajuan_dupak.status_hasil as status_hasil')
                      ->join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                      ->join('users','users.id','pengajuan_dupak.user_id')
                      ->join('pegawai','pegawai.user_id','users.id')
                      ->join('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                      ->join('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('pegawai.nama','like','%'.$request->search.'%')
                      ->where('pengajuan_dupak.bl_state','A')                      
                      ->where(function ($query) use ($pegawai) {
                            $query->where('penilai_id', $pegawai->id_pegawai)
                                  ->orWhere('penilai2_id', $pegawai->id_pegawai);
                        })
                      ->where('pengajuan_dupak.status_pengajuan','=','Selesai')
                      ->where('masa_pengajuan.status_masa','!=','Selesai')
                      ->paginate($request->lngDt);
        }else{
          $data = DB::table('pengajuan_dupak')
                      ->select('id_pengajuan','pegawai.nama as nama', 'pegawai.no_nip', 'golongan.nama as nama_golongan', 'jabatan.nama as nama_jabatan','satuan_kerja.nama as nama_satuan_kerja','pengajuan_dupak.status_pengajuan as status','pengajuan_dupak.status_penilai1 as status_penilai1','pengajuan_dupak.status_penilai2 as status_penilai2','pengajuan_dupak.status_hasil as status_hasil','masa_pengajuan.tgl_awal as tahun_pengajuan')
                      ->join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                      ->join('users','users.id','pengajuan_dupak.user_id')
                      ->join('pegawai','pegawai.user_id','users.id')
                      ->join('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                      ->join('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where(function ($query) use ($pegawai) {
                            $query->where('penilai_id', $pegawai->id_pegawai)
                                  ->orWhere('penilai2_id', $pegawai->id_pegawai);
                        })
                      ->where('pengajuan_dupak.status_pengajuan','=','Selesai')                      
                      ->where('masa_pengajuan.status_masa','!=','Selesai')
                      ->where('pengajuan_dupak.bl_state','A')
                      ->paginate($request->lngDt);
        }
        $return = ['status'=>'success','code'=>200,'row'=>$data];
      	return response()->json($return);
    }

    public function detailRiwayat(Request $request)
    {
      // return $request->all();
      $pengajuan = PengajuanDupak::find($request->id);
      
      $data['pengajuan'] = $pengajuan;
      $butir = [];
      $no = 0;
      
      $thnAwal = date('Y', strtotime($data['pengajuan']->tgl_awal_penilaian));
      $thnAkhir = date('Y', strtotime($data['pengajuan']->tgl_akhir_penilaian));

      $TglAwalPengajuan = date('Y-m-d', strtotime($data['pengajuan']->tgl_awal_penilaian));
      $TglAkhirPengajuan = date('Y-m-d', strtotime($data['pengajuan']->tgl_akhir_penilaian));
      $noTh = 0;
      
      while ($thnAwal <= $thnAkhir) {
        $tahun[$noTh] = $thnAwal;
        $thnAwal++;
        $noTh++;
      }
      
      $data['tahun'] = $tahun;
      $pegawai = Pegawai::find($pengajuan->user->pegawai->id_pegawai);
      $data['pegawai'] = $pegawai;

      //get distinct master_kegiatan_id

      $penilaian = Penilaian::select('master_kegiatan_id')->where('user_id', $data['pengajuan']->user_id)
                              ->where('alasan_perubahan_1','!=','')
                              ->orWhere('alasan_perubahan_2','!=','')
                              ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                              ->distinct()
                              ->get();
      $unsur = PakMasterKegiatan::select('unsur_pak_id')->whereIn('id_pak_master',$penilaian)->distinct()->get();

      // Start Get Butir Kegiatan
      $butir = [];
      foreach ($penilaian as $key => $pnl) {
        $master_kegiatan = [];
        $pakMaster = PakMasterKegiatan::find($pnl->master_kegiatan_id);

        $idPak = $pakMaster->id_pak_master;
        $butir[$key] = PakMasterKegiatan::select('unsur_pak_id')->where('id_pak_master',$pnl->master_kegiatan_id)->first();

        for ($p=0; $p < $pakMaster->level; $p++) {
          $dt = PakMasterKegiatan::find($idPak);
          $master_kegiatan[$p] = $dt;
          $idPak = $dt->parent_id;

          if ($p == 0) {
            //get penilaian perbutir
            $arr_penilaian_perbutir = [];
            $penilaian_perbutir = Penilaian::where('master_kegiatan_id', $pnl->master_kegiatan_id)
                                            ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                                            ->get();
            foreach ($penilaian_perbutir as $ppb) {
              if ($ppb->alasan_perubahan_1 != '' || $ppb->alasan_perubahan_2 != '') {
                array_push($arr_penilaian_perbutir, $ppb);
              }
              $master_kegiatan[$p]->penilaian = $arr_penilaian_perbutir;
            }
          } else {
            $master_kegiatan[$p]->penilaian = [];
          }
        }
        $butir[$key]->butir = $master_kegiatan;
      }

      foreach ($unsur as $u) {
        $arr_butir = [];
        $u->unsur = MasterType::find($u->unsur_pak_id);
        foreach ($butir as $b) {
          if ($u->unsur_pak_id == $b->unsur_pak_id) {
            array_push($arr_butir, $b);
          }
        }
        $u->butir = $arr_butir;
      }

      $data['unsur'] = $unsur;
      // End Get Butir Kegiatan

      // return $data;
      $content = view('RiwayatpenilaianDupak.detailRiwayat', $data)->render();
      return ['status'=>'success', 'content'=>$content, 'data'=>$data];
    }

    public function detailRiwayatTahunan(Request $request)
    {
      // return $request->all();
      $pengajuan = PengajuanDupak::find($request->id);
      
      $data['pengajuan'] = $pengajuan;
      $butir = [];
      $no = 0;
      
      $thnAwal = date('Y', strtotime($data['pengajuan']->tgl_awal_penilaian));
      $thnAkhir = date('Y', strtotime($data['pengajuan']->tgl_akhir_penilaian));

      $TglAwalPengajuan = date('Y-m-d', strtotime($data['pengajuan']->tgl_awal_penilaian));
      $TglAkhirPengajuan = date('Y-m-d', strtotime($data['pengajuan']->tgl_akhir_penilaian));
      $noTh = 0;
      
      while ($thnAwal <= $thnAkhir) {
        $tahun[$noTh] = $thnAwal;
        $thnAwal++;
        $noTh++;
      }
      
      $data['tahun'] = $tahun;
      $pegawai = Pegawai::find($pengajuan->user->pegawai->id_pegawai);
      $data['pegawai'] = $pegawai;

      //get distinct master_kegiatan_id

      $penilaian = Penilaian::select('master_kegiatan_id')->where('user_id', $data['pengajuan']->user_id)
                              ->where('jumlah_px_penilai','!=','')
                              ->Where('poin_penilai','!=','')
                              ->Where('acc_status','=','Y')
                              ->Where('acc_penilai2_status','=','Y')
                              ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                              ->distinct()
                              ->get();

      $unsur = PakMasterKegiatan::select('unsur_pak_id')->whereIn('id_pak_master',$penilaian)->distinct()->get();

      // Start Get Butir Kegiatan
      $butir = [];
      foreach ($penilaian as $key => $pnl) {
        $master_kegiatan = [];
        $pakMaster = PakMasterKegiatan::find($pnl->master_kegiatan_id);

        $idPak = $pakMaster->id_pak_master;
        $butir[$key] = PakMasterKegiatan::select('unsur_pak_id')->where('id_pak_master',$pnl->master_kegiatan_id)->first();

        for ($p=0; $p < $pakMaster->level; $p++) {
          $dt = PakMasterKegiatan::find($idPak);
          $master_kegiatan[$p] = $dt;
          $idPak = $dt->parent_id;

          if ($p == 0) {
            //get penilaian perbutir
            $arr_penilaian_perbutir = [];
            // $penilaian_perbutir = Penilaian::where('master_kegiatan_id', $pnl->master_kegiatan_id)
            //                                 ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
            //                                 ->get();

            $penilaian_perbutir = RiwayatPenilaian::join('pengajuan_dupak','pengajuan_dupak.id_pengajuan','riwayat_penilaian_dupak.pengajuan_id')
                                            ->where('master_kegiatan_id', $pnl->master_kegiatan_id)
                                            ->where('riwayat_penilaian_dupak.pengajuan_id',$request->id)                                            
                                            ->first();                                            
            if (!empty($penilaian_perbutir)) {
              if ($penilaian_perbutir->alasan_perubahan1 != '' || $penilaian_perbutir->alasan_perubahan2 != '' || $penilaian_perbutir->tgl_perubahan != '' || $penilaian_perbutir->jml_awal_pemohon != '' || $penilaian_perbutir->jml_akhir_penilai != '') {
                $master_kegiatan[$p]->penilaian = $penilaian_perbutir;
              }              
            }

          } else {
            $master_kegiatan[$p]->penilaian = [];
          }
        }
        $butir[$key]->butir = $master_kegiatan;
      }

      foreach ($unsur as $u) {
        $arr_butir = [];
        $u->unsur = MasterType::find($u->unsur_pak_id);
        foreach ($butir as $b) {
          if ($u->unsur_pak_id == $b->unsur_pak_id) {
            array_push($arr_butir, $b);
          }
        }
        $u->butir = $arr_butir;
      }

      $data['unsur'] = $unsur;
      // End Get Butir Kegiatan

      // return $data;
      $content = view('RiwayatpenilaianDupak.detailRiwayatTahunan', $data)->render();
      return ['status'=>'success', 'content'=>$content, 'data'=>$data];
    }
}
