<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use Auth, Redirect, Validator, DB;

class OperatorController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "users";
      $this->data['submn_active'] = "userOperator";
      $this->data['title'] = 'Pengguna Operator';
      $this->data['smallTitle'] = "";
      return view('users.operator.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('users')
                  ->where('name','like','%'.$request->search.'%')
                  ->where('level_user','2')
                  ->where('bl_state','A')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('users')
                  ->where('level_user','2')
                  ->where('bl_state','A')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['user'] = (!empty($request->id)) ? Users::find($request->id) : "";
      $content = view('users.operator.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'Username'        => 'required',
        'Email'           => 'required',
        'Nama'            => 'required',
        'Jenis_Kelamin'   => 'required',
        'Alamat'          => 'required',
        'Telepon'         => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (!empty($request->id)) {
          $cekUsername = Users::where('username', $request->Username)->where('id', '!=', $request->id)->first();
        }else{
          $cekUsername = Users::where('username', $request->Username)->first();
        }
        if (empty($cekUsername)) {
          if (!empty($request->id)) {
            $cekEmail = Users::where('email', $request->Email)->where('id', '!=', $request->id)->first();
          }else{
            $cekEmail = Users::where('email', $request->Email)->first();
          }
          if (empty($cekEmail)) {
            $dtUser               = (!empty($request->id)) ? Users::find($request->id) : new Users;
            $dtUser->username     = $request->Username;
            $dtUser->email        = $request->Email;
            $dtUser->password     = bcrypt($request->Username);
            $dtUser->name         = $request->Nama;
            $dtUser->gender       = $request->Jenis_Kelamin;
            $dtUser->address      = $request->Alamat;
            $dtUser->phone        = $request->Telepon;
            if (!empty($request->photo)) {
              $ukuranFile = filesize($request->photo);
              if ($ukuranFile <= 500000) {
                $ext_foto	= $request->photo->getClientOriginalExtension();
                $filename	= "Operator/".date('Ymd-His')."_".$request->Username.".".$ext_foto;
                $temp_foto	= 'upload/users/Operator/';
                $proses		= $request->photo->move($temp_foto, $filename);
                $dtUser->photo = $filename;
              }else{
                $file=$_FILES['photo']['name'];
                if(!empty($file)){
                  $direktori	= "upload/users/Operator/"; //tempat upload foto
                  $name		= 'photo'; //name pada input type file
                  $namaBaru	= "Operator/".date('Ymd-His')."_".$request->Username; //name pada input type file
                  $quality	= 50; //konversi kualitas gambar dalam satuan %
                  $upload		= CompressFile::UploadCompress($namaBaru,$name,$direktori,$quality);
                }
                $ext_foto	= $request->photo->getClientOriginalExtension();
                $new_ext	= strtolower($ext_foto);
                $dtUser->photo = $namaBaru.".".$new_ext;
              }
            }
            $dtUser->level_user   = '2';
            $dtUser->is_banned    = '0';
            if (empty($request->id)) {
              $dtUser->creator_id   = Auth::getUser()->id;
            }
            $dtUser->save();

            if ($dtUser) {
              if (empty($request->id)) {
                $requestActivity = [
                  'action_type'   => 'Insert',
                  'message'       => 'Create Pengguna Operator '.$request->Nama,
                ];
              }else{
                $requestActivity = [
                  'action_type'   => 'Update',
                  'message'       => 'Update Pengguna Operator '.$request->Nama,
                ];
              }
              $saveActivity = Activitys::add($requestActivity);

              $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
              $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Email Sudah Digunakan, Silahkan menggunakan Email Lain !!'];
          }
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Username Sudah Digunakan, Silahkan menggunakan Username Lain !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function detailData(Request $request)
    {
      $data['user'] = Users::find($request->id);
      $content = view('users.operator.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function resetPassword(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $resetPass = Users::find($id_user);
        if (!empty($resetPass)) {
          if ($no == 1) {
            $nama = $resetPass->name;
          }else{
            $nama = $nama.', '.$resetPass->name;
          }
          $resetPass->password = bcrypt($resetPass->username);
          $resetPass->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Reset',
        'message'       => 'Reset Password Pengguna Operator '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($resetPass) {
        return ['status' => 'success', 'message' => 'Password Berhasil Direset !!'];
      }else{
        return ['status'=>'error', 'message'=>'Password Gagal Direset !!'];
      }
    }

    public function bannedAccount(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $banAcc = Users::find($id_user);
        if (!empty($banAcc)) {
          if ($no == 1) {
            $nama = $banAcc->name;
          }else{
            $nama = $nama.', '.$banAcc->name;
          }
          $banAcc->is_banned = '1';
          $banAcc->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Banned',
        'message'       => 'Banned Akun Pengguna Operator '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($banAcc) {
        return ['status' => 'success', 'message' => 'Akun Berhasil Dinon-Aktifkan !!'];
      }else{
        return ['status'=>'error', 'message'=>'Akun Gagal Dinon-Aktifkan !!'];
      }
    }

    public function activeAccount(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $activeAcc = Users::find($id_user);
        if (!empty($activeAcc)) {
          if ($no == 1) {
            $nama = $activeAcc->name;
          }else{
            $nama = $nama.', '.$activeAcc->name;
          }
          $activeAcc->is_banned = '0';
          $activeAcc->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Active',
        'message'       => 'Meng-Aktifkan Akun Pengguna Operator '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($activeAcc) {
        return ['status' => 'success', 'message' => 'Akun Berhasil Diaktifkan !!'];
      }else{
        return ['status'=>'error', 'message'=>'Akun Gagal Diaktifkan !!'];
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $remove = Users::find($id_user);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->name;
          }else{
            $nama = $nama.', '.$remove->name;
          }
          $remove->bl_state = 'D';
          $remove->is_banned = '1';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Akun Pengguna Operator '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Akun Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Akun Gagal Dihapus !!'];
      }
    }
}
