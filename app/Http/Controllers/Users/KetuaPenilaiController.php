<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\Pegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
// use App\Models\TimPenilai;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use Auth, Redirect, Validator, DB;

class KetuaPenilaiController extends Controller
{
  public function main(Request $request)
  {
    $this->data['mn_active'] = "users";
    $this->data['submn_active'] = "userKetuaPenilai";
    $this->data['title'] = 'Pengguna Ketua Penilai';
    $this->data['smallTitle'] = "";
    return view('users.Ketuapenilai.main')->with('data', $this->data);
  }

  public function loadData(Request $request)
  {
    $data = Users::getJsonUsersKetuaPenilai($request);
    return response()->json($data);  
  }

  public function form(Request $request)
  {
    if (!empty($request->id)) {
      $data['pegawais']     = Users::join('pegawai','pegawai.user_id','users.id')
                                      ->where('pegawai.status_pegawai','PNS')
                                      ->where('pegawai.status_penilai', 'Tidak')
                                      ->whereOr('users.id',$request->id)
                                      ->where('users.bl_state','A')
                                      ->get();
    }else{
      $data['pegawais']     = Users::join('pegawai','pegawai.user_id','users.id')
                                      ->where('pegawai.status_pegawai','PNS')
                                      ->where('pegawai.status_penilai', 'Tidak')
                                      ->where('users.bl_state','A')
                                      ->get();
    }
    $data['user']         = (!empty($request->id)) ? Users::find($request->id) : "";
    $content = view('users.Ketuapenilai.form', $data)->render();
    return ['status' => 'success', 'content' => $content];
  }

  public function saveData(Request $request)
  {
    $rules = array(
      'id_pegawai'  => 'required',
    );
    $messages = array(
      'required'    => 'Kolom Harus Diisi',
    );
    $validator   = Validator::make($request->all(), $rules, $messages);
    if (!$validator->fails()) {
      $pegawai = Pegawai::find($request->id_pegawai);
      if (!empty($pegawai)) {
        $pegawai->status_penilai = 'Ya';
        $pegawai->save();

        if ($pegawai) {
          $user = Users::find($pegawai->user_id);
          $user->is_penilai = 'Y';
          $user->is_ketua_penilai = 'Y';
          $user->save();

          $requestActivity = [
            'action_type'   => 'Insert',
            'message'       => 'Create Pegawai Penilai ' . $pegawai->nama,
          ];
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status' => 'success', 'code' => '200', 'message' => 'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status' => 'error', 'code' => '500', 'message' => 'Data Gagal Disimpan !!'];
        }
      }else{
        $return = ['status' => 'error', 'code' => '500', 'message' => 'Data Pegawai Tidak Ditemukan !!'];
      }
      return response()->json($return);
    } else {
      return $validator->messages();
    }
  }

  public function resetPassword(Request $request)
  {
    $nama = '';
    $no = 1;
    foreach ($request->id as $id_user) {
      $resetPass = Users::find($id_user);
      if (!empty($resetPass)) {
        if ($no == 1) {
          $nama = $resetPass->name;
        } else {
          $nama = $nama . ', ' . $resetPass->name;
        }
        $resetPass->password = bcrypt($resetPass->username);
        $resetPass->save();
      }
      $no++;
    }

    $requestActivity = [
      'action_type'   => 'Reset',
      'message'       => 'Reset Password Pengguna Penilai ' . $nama,
    ];
    $saveActivity = Activitys::add($requestActivity);

    if ($resetPass) {
      return ['status' => 'success', 'message' => 'Password Berhasil Direset !!'];
    } else {
      return ['status' => 'error', 'message' => 'Password Gagal Direset !!'];
    }
  }

  public function bannedAccount(Request $request)
  {
    $nama = '';
    $no = 1;
    foreach ($request->id as $id_user) {
      $banAcc = Users::find($id_user);
      if (!empty($banAcc)) {
        if ($no == 1) {
          $nama = $banAcc->name;
        } else {
          $nama = $nama . ', ' . $banAcc->name;
        }
        $banAcc->is_banned = '1';
        $banAcc->save();
      }
      $no++;
    }

    $requestActivity = [
      'action_type'   => 'Banned',
      'message'       => 'Banned Akun Pengguna Penilai ' . $nama,
    ];
    $saveActivity = Activitys::add($requestActivity);

    if ($banAcc) {
      return ['status' => 'success', 'message' => 'Akun Berhasil Dinon-Aktifkan !!'];
    } else {
      return ['status' => 'error', 'message' => 'Akun Gagal Dinon-Aktifkan !!'];
    }
  }

  public function activeAccount(Request $request)
  {
    $nama = '';
    $no = 1;
    foreach ($request->id as $id_user) {
      $activeAcc = Users::find($id_user);
      if (!empty($activeAcc)) {
        if ($no == 1) {
          $nama = $activeAcc->name;
        } else {
          $nama = $nama . ', ' . $activeAcc->name;
        }
        $activeAcc->is_banned = '0';
        $activeAcc->save();
      }
      $no++;
    }

    $requestActivity = [
      'action_type'   => 'Active',
      'message'       => 'Meng-Aktifkan Akun Pengguna Penilai ' . $nama,
    ];
    $saveActivity = Activitys::add($requestActivity);

    if ($activeAcc) {
      return ['status' => 'success', 'message' => 'Akun Berhasil Diaktifkan !!'];
    } else {
      return ['status' => 'error', 'message' => 'Akun Gagal Diaktifkan !!'];
    }
  }

  public function removeData(Request $request)
  {
    $nama = '';
    $no = 1;
    foreach ($request->id as $id_user) {
      $user = Users::find($id_user);
      if (!empty($user)) {
        if ($no == 1) {
          $nama = $user->name;
        } else {
          $nama = $nama . ', ' . $user->name;
        }
      }

      $user->is_penilai = 'N';
      $user->save();

      $removePenilai = Pegawai::where('user_id', $id_user)->first();
      if (!empty($removePenilai)) {
        $removePenilai->status_penilai = 'Tidak';
        $removePenilai->save();
      }
      $no++;
    }

    $requestActivity = [
      'action_type'   => 'Delete',
      'message'       => 'Delete Akun Pengguna Penilai ' . $nama,
    ];
    $saveActivity = Activitys::add($requestActivity);

    if ($user) {
      return ['status' => 'success', 'message' => 'Akun Berhasil Dihapus !!'];
    } else {
      return ['status' => 'error', 'message' => 'Akun Gagal Dihapus !!'];
    }
  }
}
