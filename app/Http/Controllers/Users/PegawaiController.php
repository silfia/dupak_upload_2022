<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\Pegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use Auth, Redirect, Validator, DB;

class PegawaiController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "users";
      $this->data['submn_active'] = "userPegawai";
      $this->data['title'] = 'Pengguna Pegawai';
      $this->data['smallTitle'] = "";
      return view('users.pegawai.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      $data = Users::getJsonPegawai($request);
      return response()->json($data);     
    }

    public function form(Request $request)
    {
      $data['pegawai'] = (!empty($request->id)) ? Pegawai::join('users','users.id','pegawai.user_id')->where('users.id', $request->id)->first() : "";
      $data['golongan'] = DataMaster::where('is_golongan','Y')->where('bl_state','A')->get();
      if (!empty($request->id)) {
        $data['jabatan'] = DataMaster::where('is_golongan','N')
                                      ->where('tipe_profesi_id',$data['pegawai']->tipe_profesi_id)
                                      ->where('bl_state','A')->get();
      }
      $data['profesi'] = MasterType::where('modul','StatusProfesi')->where('bl_state','A')->get();
      $data['satKers'] = SatuanKerja::where('bl_state','A')->get();
      $content = view('users.pegawai.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'Nama'            => 'required',
        'Jenis_Kelamin'   => 'required',
        'Tempat_Lahir'    => 'required',
        'Tanggal_Lahir'   => 'required',
        'Profesi'         => 'required',
        'Satuan_Kerja'    => 'required',
        'status_pegawai'  => 'required',
        'NIP'             => 'required',
        // 'No_Karpeng'      => 'required',
        'Pendidikan'      => 'required',
        'Tahun_Lulus'     => 'required',
        // 'Golongan'        => 'required',
        // 'Golongan_TMT'    => 'required',
        // 'Jabatan'         => 'required',
        // 'Jabatan_TMT'     => 'required',
      );
      if($request->status_pegawai == 'PNS') {
        $rules['status_penilai']  = 'required';
        // $rules['No_Karpeng']      = 'required';
        $rules['Golongan']        = 'required';
        $rules['Golongan_TMT']    = 'required';
        $rules['Jabatan']         = 'required';
        $rules['Jabatan_TMT']     = 'required';
        $rules['Point_Pegawai']   = 'required';
      }
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (empty($request->id_pegawai)) {
          $user             = new Users;
          $user->username   = ($request->NIP != '') ? str_replace(' ', '', $request->NIP) : $request->Email;
          $user->email      = $request->Email;
          $user->password   = bcrypt('123456');
          $user->name       = $request->Nama;
          // $user->gender     = ($request->Jenis_Kelamin == 'P') ? 'Laki - Laki' : 'Perempuan';
          $user->gender     = $request->Jenis_Kelamin;
          $user->level_user = '5';
          $user->is_banned  = '0';
          $user->creator_id = Auth::getUser()->id;
          $user->save();
        }

        $dtPegawai                        = (!empty($request->id_pegawai)) ? Pegawai::find($request->id_pegawai) : new Pegawai;
        $dtPegawai->nama                  = $request->Nama;
        $dtPegawai->no_nip                = $request->NIP;
        $dtPegawai->tempat_lahir          = $request->Tempat_Lahir;
        $dtPegawai->tanggal_lahir         = date('Y-m-d', strtotime($request->Tanggal_Lahir));
        $dtPegawai->jenis_kelamin         = $request->Jenis_Kelamin;
        if ($request->status_pegawai == 'PNS') {
          $dtPegawai->status_penilai        = $request->status_penilai;
          $dtPegawai->no_karpeng            = $request->No_Karpeng;
          $dtPegawai->golongan_id           = $request->Golongan;
          $dtPegawai->golongan_tmt          = date('Y-m-d', strtotime($request->Golongan_TMT));
          $dtPegawai->jabatan_id            = $request->Jabatan;
          $dtPegawai->jabatan_tmt           = date('Y-m-d', strtotime($request->Jabatan_TMT));
          $dtPegawai->point_pegawai         = $request->Point_Pegawai;
        }else{
          $dtPegawai->status_penilai        = 'Tidak';
          $dtPegawai->no_karpeng            = '';
          $dtPegawai->golongan_id           = '0';
          $dtPegawai->golongan_tmt          = '1000-01-01';
          $dtPegawai->jabatan_id            = '0';
          $dtPegawai->jabatan_tmt           = '1000-01-01';
          $dtPegawai->point_pegawai         = '0';
        }
        $dtPegawai->pendidikan_terakhir   = $request->Pendidikan;
        $dtPegawai->pendidikan_tahun      = $request->Tahun_Lulus;
        $dtPegawai->satuan_kerja_id       = $request->Satuan_Kerja;
        $dtPegawai->bl_note               = '0';
        $dtPegawai->tipe_profesi_id       = $request->Profesi;
        $dtPegawai->status_pegawai        = $request->status_pegawai;
        $dtPegawai->ac_conn               = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_master)) {
          $dtPegawai->creator_id          = Auth::getUser()->id;
        }
        $dtPegawai->cluster_id            = 'LATIHAN1';
        $dtPegawai->bllokal               = '1';
        if (empty($request->id_pegawai)) {
          $dtPegawai->user_id             = $user->id;
        }
        $dtPegawai->save();

        if (!empty($request->id_pegawai)) {
          $user             = Users::find($dtPegawai->user_id);
          $user->username   = ($request->NIP != '') ? str_replace(' ', '', $request->NIP) : $request->Email;
          $user->email      = $request->Email;
          $user->name       = $request->Nama;
          $user->gender     = $request->Jenis_Kelamin;
          $user->save();
        }

        if ($dtPegawai) {
          if (empty($request->id_pegawai)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Pengguna Pegawai '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Pengguna Pegawai '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function detailData(Request $request)
    {
      $data['pegawai'] = Pegawai::join('users','users.id','pegawai.user_id')
                                  ->where('users.id', $request->id)->first();
      $content = view('users.pegawai.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function resetPassword(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $resetPass = Users::find($id_user);
        if (!empty($resetPass)) {
          if ($no == 1) {
            $nama = $resetPass->name;
          }else{
            $nama = $nama.', '.$resetPass->name;
          }
          $resetPass->password = bcrypt($resetPass->username);
          $resetPass->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Reset',
        'message'       => 'Reset Password Pengguna Pegawai '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($resetPass) {
        return ['status' => 'success', 'message' => 'Password Berhasil Direset !!'];
      }else{
        return ['status'=>'error', 'message'=>'Password Gagal Direset !!'];
      }
    }

    public function bannedAccount(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $banAcc = Users::find($id_user);
        if (!empty($banAcc)) {
          if ($no == 1) {
            $nama = $banAcc->name;
          }else{
            $nama = $nama.', '.$banAcc->name;
          }
          $banAcc->is_banned = '1';
          $banAcc->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Banned',
        'message'       => 'Banned Akun Pengguna Pegawai '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($banAcc) {
        return ['status' => 'success', 'message' => 'Akun Berhasil Dinon-Aktifkan !!'];
      }else{
        return ['status'=>'error', 'message'=>'Akun Gagal Dinon-Aktifkan !!'];
      }
    }

    public function activeAccount(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $activeAcc = Users::find($id_user);
        if (!empty($activeAcc)) {
          if ($no == 1) {
            $nama = $activeAcc->name;
          }else{
            $nama = $nama.', '.$activeAcc->name;
          }
          $activeAcc->is_banned = '0';
          $activeAcc->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Active',
        'message'       => 'Meng-Aktifkan Akun Pengguna Pegawai '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($activeAcc) {
        return ['status' => 'success', 'message' => 'Akun Berhasil Diaktifkan !!'];
      }else{
        return ['status'=>'error', 'message'=>'Akun Gagal Diaktifkan !!'];
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_user) {
        $remove = Pegawai::where('user_id', $id_user)->first();
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();

          $user = Users::find($remove->user_id);
          if (!empty($user)) {
            $user->is_banned = '1';
            $user->bl_state = 'D';
            $user->save();
          }
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Akun Pengguna Pegawai '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }


}
