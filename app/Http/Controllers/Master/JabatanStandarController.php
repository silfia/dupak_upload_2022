<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DataMaster;
use App\Models\StandartMaster;
use App\Models\MasterType;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class JabatanStandarController extends Controller
{
    public function main(Request $request)
    {
      $data['master'] = DataMaster::find($request->id);
      $content = view('master.jabatan.standar.main', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function loadData(Request $request)
    {
      if ($request->search != '') {
        $data['standar'] = StandartMaster::join('data_master as str','str.id_master', 'standart_master.mst_id')
                                          ->where('master_id', $request->id_golongan)
                                          ->where('nama','like','%'.$request->search.'%')
                                          ->where('standart_master.bl_state', 'A')
                                          ->get();
      }else{
        $data['standar'] = StandartMaster::join('data_master as str','str.id_master', 'standart_master.mst_id')
                                          ->where('master_id', $request->id_golongan)
                                          ->where('standart_master.bl_state', 'A')
                                          ->get();
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['id_golongan'] = $request->idGolongan;
      $data['standar'] = (!empty($request->id)) ? StandartMaster::find($request->id) : "";
      if (!empty($request->id)) {
        $dtStandar = StandartMaster::select('mst_id')->where('master_id',$request->idGolongan)->where('mst_id', '!=', $data['standar']->mst_id)->where('bl_state','A')->get();
      }else{
        $dtStandar = StandartMaster::select('mst_id')->where('master_id',$request->idGolongan)->where('bl_state','A')->get();
      }
      $data['jabatan'] = DataMaster::where('is_golongan','Y')->where('bl_state','A')->whereNotIn('id_master', $dtStandar)->get();
      $content = view('master.golongan.standar.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function save(Request $request)
    {
      $rules = array(
        'id_golongan' => 'required',
        'mst_id'      => 'required',
        'poin'        => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $cekStandar = StandartMaster::where('master_id', $request->id_golongan)->where('mst_id',$request->mst_id)->first();
        if (empty($cekStandar)) {
          $newStandar = (empty($request->id_standart)) ? new StandartMaster : StandartMaster::find($request->id_standart);
          $newStandar->master_id = $request->id_golongan;
          $newStandar->mst_id = $request->mst_id;
          $newStandar->poin = $request->poin;
          if(empty($request->id_standart)){
            $newStandar->bl_state = 'A';
            $newStandar->creator_id = Auth::getUser()->id;
          }
          $newStandar->save();

          if ($newStandar) {
            if (empty($request->id_standart)) {
              $requestActivity = [
                'action_type'   => 'Insert',
                'message'       => 'Create Master Standar Jabatan '.$request->Nama,
              ];
            }else{
              $requestActivity = [
                'action_type'   => 'Update',
                'message'       => 'Update Master Standar Jabatan '.$request->Nama,
              ];
            }
            $saveActivity = Activitys::add($requestActivity);

            $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
          }
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Jabatan Sudah Pernah Tercatat !!'];
        }

        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_standart) {
        $remove = StandartMaster::find($id_standart);
        if (!empty($remove)) {
          if ($no == 1) {
            $namaGolongan = $remove->master->nama;
            $nama = $remove->mstStandar->nama;
          }else{
            $nama = $nama.', '.$remove->mstStandar->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Standart Jabatan '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
