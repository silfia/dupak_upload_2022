<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PermenpanKegiatan;
use App\Models\PakMasterKegiatan;
use App\Models\MasterType;
use App\Models\DataMaster;
use App\Models\MasterBuktiFisik;
use App\Models\BuktiFisikKegiatan;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;

class ButirKegiatanNewController extends Controller
{
	public function main(Request $request)
	{
		$this->data['mn_active'] = "master";
		$this->data['submn_active'] = "butirKegiatanNew";
		$this->data['title'] = 'Butir Kegiatan';
		$this->data['smallTitle'] = "";

		return view('master.butirKegiatan.new.main')->with('data', $this->data);
	}

	public function loadDataProfesi(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('master_type')
                  ->where('nama','like','%'.$request->search.'%')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }else{
      	if (Auth::getUser()->is_ketua_penilai == 'Y') {
      		$data = DB::table('master_type')->selectRaw("pegawai.nama as nama_pegawai,master_type.nama as nama,master_type.id_master_type")
      			  ->join('pegawai','pegawai.tipe_profesi_id','master_type.id_master_type')      			  
                  ->where('master_type.modul','StatusProfesi')
                  ->where('master_type.bl_state','A')                  
                  ->where('pegawai.user_id',Auth::getUser()->id)
                  ->orderBy('master_type.urutan','ASC')
                  ->paginate($request->lngDt);
      	}else{
        	$data = DB::table('master_type')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);      		
      	}
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function detailData(Request $request)
    {
      $data['id_profesi'] = $request->id;
      $data['permenpan'] = PermenpanKegiatan::where('tipe_profesi_id', $request->id)->where('status','Aktif')->get();

      $content = view('master.butirKegiatan.new.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function loadData(Request $request)
    {
    // return $request->all();
		$getButir = Formatters::get_butirNew($request->id_profesi, $request->id_permenpan);

		if ($getButir['status'] == 'success') {
			$data['butir'] = $getButir['row']['butir'];
			$return = ['status'=>'success','code'=>200, 'message'=>'Unsur Kegiatan Ditemukan !!', 'row'=>$data];
		}else{
			$data['butir'] = [];
			$return = ['status'=>'error','code'=>404, 'message'=>'Unsur Kegiatan Tidak Ditemukan !!','row'=>$data];
		}

		return response()->json($return);
	}

	public function form(Request $request)
	{
		$data['butir'] = "";
		$data['parent'] = (!empty($request->parent_id)) ? PakMasterKegiatan::find($request->parent_id) : "";
		$data['unsur'] = MasterType::find($request->unsur_id);
		$data['profesi'] = MasterType::find($request->id_profesi);
		$data['permenpan'] = PermenpanKegiatan::find($data['unsur']->permenpan_id);
		$data['jabatan'] = DataMaster::where('tipe_profesi_id', $request->id_profesi)
										->where('is_golongan', 'N')
										->where('bl_state','A')->get();
		$infoButir = [];
		if ($data['parent'] != '') {
			$tempId = $request->parent_id;
			$tempJml = $data['parent']->level - 1;
			for ($i=$tempJml; $i >= 0 ; $i--) {
				$info = PakMasterKegiatan::find($tempId);
				$infoButir[$i]['nama'] = $info->butir_kegiatan;
				$tempId = $info->parent_id;
			}
		}
		$data['infoParent'] = $infoButir;
		$data['master_bukti'] = MasterBuktiFisik::all();

		$content = view('master.butirKegiatan.new.form', $data)->render();
		return ['status' => 'success', 'content' => $content];
	}

	public function saveData(Request $request)
	{
		$rules = array(
			'unsur_id'		=> 'required',
			'id_permenpan'	=> 'required',
			'id_profesi'	=> 'required',
			'is_title'		=> 'required',
			'nama_butir'	=> 'required',
		);
		if ($request->is_title == '0') {
			$rules['jabatan']	= 'required';
			$rules['minimal']	= 'required';
			$rules['satuan']	= 'required';
			$rules['point']		= 'required';
			$rules['is_bukti']	= 'required';
			if ($request->is_bukti == 'Ya') {
				$rules['id_master_bukti'] = 'required';
			};
		};
		$messages = array(
			'required'  => 'Kolom Harus Diisi',
		);
		$validator 	= Validator::make($request->all(), $rules, $messages);
		if (!$validator->fails()) {
			$dtGroup = [];
			$tempGroup = 0;
			$dtGroup[$tempGroup]['jum_min'] = null;
			$dtGroup[$tempGroup]['satuan'] = null;
			$dtGroup[$tempGroup]['points'] = null;
			$dtGroup[$tempGroup]['jabatan_id'] = null;
			if ($request->is_title == 0 && $request->jabatan == 'semua') {
				$jabatan = DataMaster::where('tipe_profesi_id', $request->id_profesi)
										->where('is_golongan', 'N')
										->where('bl_state','A')->get();
				$tempGroup++;
				foreach ($jabatan as $keyJabatan) {
					$dtGroup[$tempGroup]['jum_min'] = $request->minimal;
					$dtGroup[$tempGroup]['satuan'] = $request->satuan;
					$dtGroup[$tempGroup]['points'] = $request->point;
					$dtGroup[$tempGroup]['jabatan_id'] = $keyJabatan->id_master;
					$tempGroup++;
				}
				$jmlLoop = count($jabatan) + 1;
				$stJenjang = 'semua';
			}else{
				$jmlLoop = 1;
				$stJenjang = 'satu';
				$dtGroup[$tempGroup]['jum_min'] = $request->minimal;
				$dtGroup[$tempGroup]['satuan'] = $request->satuan;
				$dtGroup[$tempGroup]['points'] = $request->point;
				$dtGroup[$tempGroup]['jabatan_id'] = $request->jabatan;
			}

			if ($request->parent_id != '') {
				$parentButir = PakMasterKegiatan::find($request->parent_id);
				$level = $parentButir->level + 1;
				$level_1 = ($parentButir->level == 1) ? $parentButir->id_pak_master : $parentButir->level_1;
				$parent_id = $parentButir->id_pak_master;
			}else{
				$level = 1;
				$level_1 = null;
				$parent_id = null;
			}

			for ($i=0; $i < $jmlLoop; $i++) { 
				$dtButir =  new PakMasterKegiatan;
				$dtButir->level 		= $level;
				$dtButir->level_1		= $level_1;
				$dtButir->parent_id		= $parent_id;
				if ($stJenjang == 'satu') {
					$dtButir->is_title		= $request->is_title;
					$dtButir->is_child		= ($request->parent_id != '') ? 1 : 0;
					$dtButir->is_bukti		= ($request->is_title == '0') ? $request->is_bukti : null;
				}else{
					$dtButir->is_title		= ($i == 0) ? 1 : 0;
					if ($i == 0) {
						$dtButir->is_child		= ($request->parent_id != '') ? 1 : 0;
						$dtButir->is_bukti		= null;
					}else{
						$dtButir->is_child		= 1;
						$dtButir->is_bukti		= $request->is_bukti;
					}
				}
				$dtButir->butir_kegiatan = $request->nama_butir;
				if ($dtButir->is_title == '1') {
					$dtButir->jum_min == null;
					$dtButir->satuan == null;
					$dtButir->points == null;
					$dtButir->jabatan_id == null;
				}else{
					$dtButir->jum_min = $dtGroup[$i]['jum_min'];
					$dtButir->satuan = $dtGroup[$i]['satuan'];
					$dtButir->points = $dtGroup[$i]['points'];
					$dtButir->jabatan_id = $dtGroup[$i]['jabatan_id'];
				}
				$dtButir->formasi_id		= $request->id_profesi;
				$dtButir->unsur_pak_id		= $request->unsur_id;
				$dtButir->permenpan_id		= $request->id_permenpan;
				$dtButir->jenis				= 'Butir';
				$dtButir->creator_id	= Auth::id();
				$dtButir->ac_conn		= $_SERVER['REMOTE_ADDR'];
				$dtButir->save();

				if ($dtButir) {
					if ($request->is_bukti == 'Ya') {
						$buktiFisik = $request->id_master_bukti;
						for ($j=0; $j < count($buktiFisik); $j++) { 
							$bukti = new BuktiFisikKegiatan;
							$bukti->pak_master_kegiatan_id = $dtButir->id_pak_master;
							$bukti->master_bukti_fisik_id = $buktiFisik[$j];
							$bukti->harus_isi = 'required';
							$bukti->save();
						}
					}
					if ($i == 0) {
						$level = $level + 1;
						$level_1 = ($level_1 == null) ? $dtButir->id_pak_master : $dtButir->level_1;
						$parent_id = $dtButir->id_pak_master;
					}
				}
			}

			if ($dtButir) {
				$requestActivity = [
					'action_type'	=> 'Insert',
					'message'		=> 'Create Master Butir Kegiatan '.$request->Nama,
					'nama_butir'    =>$request->nama_butir,
				];
				$saveActivity = Activitys::add($requestActivity);

				$return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
			}else{
				$return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
			}

			return response()->json($return);
		} else {
			return $validator->messages();
		}
	}

	public function formUpdate(Request $request)
	{
		$data['butir'] = PakMasterKegiatan::find($request->id_pak);
		$data['parent'] = (!empty($request->parent_id)) ? PakMasterKegiatan::find($request->parent_id) : "";
		$data['unsur'] = MasterType::find($request->unsur_id);
		$data['profesi'] = MasterType::find($request->id_profesi);
		$data['permenpan'] = PermenpanKegiatan::find($data['unsur']->permenpan_id);
		$data['jabatan'] = DataMaster::where('tipe_profesi_id', $request->id_profesi)
										->where('is_golongan', 'N')
										->where('bl_state','A')->get();
		$infoButir = [];
		if ($data['parent'] != '') {
			$tempId = $request->parent_id;
			$tempJml = $data['parent']->level - 1;
			for ($i=$tempJml; $i >= 0 ; $i--) {
				$info = PakMasterKegiatan::find($tempId);
				$infoButir[$i]['nama'] = $info->butir_kegiatan;
				$tempId = $info->parent_id;
			}
		}
		$data['infoParent'] = $infoButir;
		$data['master_bukti'] = MasterBuktiFisik::all();
		$data['buktiFisik'] = BuktiFisikKegiatan::where('pak_master_kegiatan_id', $request->id_pak)->get();
		
		$content = view('master.butirKegiatan.new.formUpdate', $data)->render();
		return ['status' => 'success', 'content' => $content];
	}

	public function updateData(Request $request)
	{
		$rules = array(
			'id_pak_master'	=> 'required',
			'unsur_id'		=> 'required',
			'id_permenpan'	=> 'required',
			'id_profesi'	=> 'required',
			'is_title'		=> 'required',
			'nama_butir'	=> 'required',
		);
		if ($request->is_title == '0') {
			$rules['jabatan']	= 'required';
			$rules['minimal']	= 'required';
			$rules['satuan']	= 'required';
			$rules['point']		= 'required';
			$rules['is_bukti']	= 'required';
			if ($request->is_bukti == 'Ya') {
				$rules['id_master_bukti'] = 'required';
			};
		};
		$messages = array(
			'required'  => 'Kolom Harus Diisi',
		);
		$validator 	= Validator::make($request->all(), $rules, $messages);
		if (!$validator->fails()) {
			$dtButir					=  PakMasterKegiatan::find($request->id_pak_master);
			$dtButir->is_title			= $request->is_title;
			$dtButir->is_child			= ($request->parent_id != '') ? 1 : 0;
			$dtButir->is_bukti			= ($request->is_title == '0') ? $request->is_bukti : null;
			$dtButir->butir_kegiatan	= $request->nama_butir;
			$dtButir->jum_min			= ($request->is_title == '0') ? $request->minimal : null;
			$dtButir->satuan			= ($request->is_title == '0') ? $request->satuan : null;
			$dtButir->points			= ($request->is_title == '0') ? $request->point : null;
			$dtButir->jabatan_id		= ($request->is_title == '0') ? $request->jabatan : null;
			$dtButir->is_bukti			= ($request->is_title == '0') ? $request->is_bukti : null;
			$dtButir->save();

			if ($dtButir) {
				if ($dtButir->is_bukti == 'Ya') {
					$buktiFisik = $request->id_master_bukti;
					$deleteBuktiFisik = BuktiFisikKegiatan::where('pak_master_kegiatan_id', $dtButir->id_pak_master)
															->whereNotIn('master_bukti_fisik_id', $buktiFisik)->delete();
					for ($j=0; $j < count($buktiFisik); $j++) {
						$cekFisik = BuktiFisikKegiatan::where('pak_master_kegiatan_id', $dtButir->id_pak_master)
														->where('master_bukti_fisik_id', $buktiFisik[$j])
														->first();
						if (!empty($cekFisik)) {
							$bukti = BuktiFisikKegiatan::find($cekFisik->id_bukti_fisik);
						}else{
							$bukti = new BuktiFisikKegiatan;
						}
						$bukti->pak_master_kegiatan_id = $dtButir->id_pak_master;
						$bukti->master_bukti_fisik_id = $buktiFisik[$j];
						$bukti->harus_isi = 'required';
						$bukti->save();
					}
				}else{
					$deleteBuktiFisik = BuktiFisikKegiatan::where('pak_master_kegiatan_id', $dtButir->id_pak_master)->delete();
				}

				$requestActivity = [
					'action_type'   => 'Update',
					'message'       => 'Update Master Butir Kegiatan '.$request->Nama,
					'nama_butir'    =>$request->nama_butir,
				];
				$saveActivity = Activitys::add($requestActivity);

				$return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
			}else{
				$return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
			}

			return response()->json($return);
		} else {
			return $validator->messages();
		}
	}
}
