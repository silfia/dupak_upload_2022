<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\Permenpan;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class PermenpanKegiatanController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "permenpan";
      $this->data['title'] = 'Permenpan';
      $this->data['smallTitle'] = "";
      $this->data['types'] = MasterType::where('modul','StatusProfesi')
                                          ->where('bl_state','A')
                                          ->orderBy('urutan','ASC')
                                          ->get();
      return view('master.permenpan.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      $data = Permenpan::getJsonPermenpan($request);
      return response()->json($data);     
    }

    public function form(Request $request)
    {
      // return $request->all();
      $data['permenpan'] = (!empty($request->id_permenpan)) ? Permenpan::find($request->id_permenpan) : "";
      $data['id_profesi'] = $request->id_profesi;          
      $content = view('master.permenpan.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      // return $request->all();
      $rules = array(
        'no_permenpan'    => 'required',
        'tahun'           => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {

        $dtPermenpan =  (!empty($request->id_permenpan)) ? Permenpan::find($request->id_permenpan) : new Permenpan;
        $dtPermenpan->no_permenpan = $request->no_permenpan;
        $dtPermenpan->tahun = $request->tahun;
        $dtPermenpan->tipe_profesi_id = $request->id_profesi;
        $dtPermenpan->ac_conn = $_SERVER['REMOTE_ADDR'];
        
        if (empty($request->id_permenpan)) {
          $dtPermenpan->status = 'Tidak Aktif';
          $dtPermenpan->creator_id = Auth::getUser()->id;
        }
        
        $dtPermenpan->save();
        if ($dtPermenpan) {
          if (empty($request->id_permenpan)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Permenpan '.$request->no_permenpan,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Permenpan '.$request->no_permenpan,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);
          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $no_permenpan = '';
      $no = 1;
      foreach ($request->id_permenpan as $id_permenpan) {
        $remove = Permenpan::find($id_permenpan);
        if (!empty($remove)) {
          if ($no == 1) {
            $no_permenpan = $remove->no_permenpan;
          }else{
            $no_permenpan = $no_permenpan.', '.$remove->no_permenpan;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Permenpan '.$no_permenpan,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }

      public function activePermenpan(Request $request)
    {
      // return $request->all();
      $cekPermen = Permenpan::where('id_permenpan',$request->id_permenpan)
                            ->where('tipe_profesi_id',$request->id_profesi)
                            ->first();
      $cekPermen->status = 'Aktif';
      $cekPermen->save();

      if ($cekPermen) {
        $requestActivity = [
        'action_type'   => 'Active',
        'message'       => 'Meng-Aktifkan Permenpan '.$cekPermen->no_permenpan,
        ];
        
        $saveActivity = Activitys::add($requestActivity);

        $nonPermen = Permenpan::where('tipe_profesi_id', $request->id_profesi)
                              ->where('id_permenpan', '!=', $request->id_permenpan)
                              ->where('bl_state','A')
                              ->update(['status' => 'Tidak Aktif']);
          return ['status' => 'success', 'message' => 'Permenpan Berhasil Diaktifkan !!'];
        }else{
        return ['status'=>'error', 'message'=>'Permenpan Gagal Diaktifkan !!'];
      }
    }
}
