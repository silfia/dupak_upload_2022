<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterType;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Desa;
use App\Models\SatuanKerja;
use App\Models\Users;
use App\Models\Pegawai;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class SatuanKerjaController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "satuanKerja";
      $this->data['title'] = 'Satuan Kerja';
      $this->data['smallTitle'] = "";
      return view('master.satuanKerja.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      $data = SatuanKerja::getJsonSatuanKerja($request);
      return response()->json($data);
      // $cari = '%'.$request->search.'%';      
      // if ($request->search != '') {
      //   $data = DB::table('satuan_kerja')
      //             ->select('id_satuan_kerja', 'kode', 'satuan_kerja.nama as nama', 'alamat', 'telepon', 'master_type.nama as nama_type', 'pegawai.nama as nama_pimpinan')
      //             ->join('master_type','master_type.id_master_type','satuan_kerja.tipe_satker_id')
      //             ->leftJoin('pegawai', 'pegawai.id_pegawai', 'satuan_kerja.pimpinan_id')
      //             ->where('master_type.modul','SatuanOrganisasi')
      //             ->where(function ($query) use ($cari) {
      //                   $query->where('satuan_kerja.nama', 'LIKE', $cari)
      //                         ->orWhere('satuan_kerja.kode', 'LIKE', $cari)
      //                         ->orWhere('satuan_kerja.alamat', 'LIKE', $cari)
      //                         ->orWhere('satuan_kerja.telepon', 'LIKE', $cari)
      //                         ->orWhere('pegawai.nama', 'LIKE', $cari);
      //             })
      //             ->where('satuan_kerja.bl_state','A')
      //             ->paginate($request->lngDt);
      // }else{
      //   $data = DB::table('satuan_kerja')
      //             ->select('id_satuan_kerja', 'kode', 'satuan_kerja.nama as nama', 'alamat', 'telepon', 'master_type.nama as nama_type', 'pegawai.nama as nama_pimpinan')
      //             ->join('master_type','master_type.id_master_type','satuan_kerja.tipe_satker_id')
      //             ->leftJoin('pegawai', 'pegawai.id_pegawai', 'satuan_kerja.pimpinan_id')
      //             ->where('master_type.modul','SatuanOrganisasi')
      //             ->where('satuan_kerja.bl_state','A')
      //             ->paginate($request->lngDt);
      // }
      // $return = ['status'=>'success','code'=>200,'row'=>$data];
      // return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['satuanKerja'] = (!empty($request->id)) ? SatuanKerja::find($request->id) : "";
      $data['type'] = MasterType::where('modul','SatuanOrganisasi')->get();
      $data['provinsi'] = Provinsi::all();
      $pimpinan = SatuanKerja::select('pimpinan_id')->where('bl_state','A')->get();
      if (!empty($request->id)) {
        $data['kabupaten'] = DB::table('kabupaten')->where('provinsi_id', $data['satuanKerja']->provinsi_id)->get();
        $data['kecamatan'] = DB::table('kecamatan')->where('kabupaten_id', $data['satuanKerja']->kabupaten_id)->get();
        $data['desa'] = DB::table('desa')->where('kecamatan_id', $data['satuanKerja']->kecamatan_id)->get();
        $pimpinan = SatuanKerja::select('pimpinan_id')
                                ->where('bl_state', 'A')
                                ->where('id_satuan_kerja', '!=', $data['satuanKerja']->id_satuan_kerja)
                                ->get();
      }
      $data['pegawais'] = Pegawai::where('status_pegawai', 'PNS')
                                  ->where('bl_state', 'A')
                                  ->whereNotIn('id_pegawai', $pimpinan)
                                  ->get();
      $content = view('master.satuanKerja.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'Kode'            => 'required',
        'Nama'            => 'required',
        'Type'            => 'required',
        // 'Telepon'        => 'required',
        // 'Fax'            => 'required',
        'Email'           => 'required',
        // 'Website'        => 'required',
        'Alamat'          => 'required',
        'jenis_layanan'   => 'required',
        'Provinsi'        => 'required',
        'Kabupaten'       => 'required',
        // 'Kecamatan'      => 'required',
        // 'Kelurahan'      => 'required',
      );
      $messages = array(
        'required'    => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (empty($request->id_satuan_kerja)) {
          $user             = new Users;
          $user->username   = $request->Email;
          $user->email      = $request->Email;
          $user->password   = bcrypt('123456');
          $user->name       = $request->Nama;
          $user->address    = $request->Alamat;
          $user->phone      = $request->Telepon;
          $user->level_user = '4';
          $user->is_banned  = '0';
          $user->creator_id = Auth::getUser()->id
          ;
          $user->save();
        }

        $dtSaKer =  (!empty($request->id_satuan_kerja)) ? SatuanKerja::find($request->id_satuan_kerja) : new SatuanKerja;
        $dtSaKer->kode            = $request->Kode;
        $dtSaKer->nama            = $request->Nama;
        $dtSaKer->alamat          = $request->Alamat;
        $dtSaKer->telepon         = $request->Telepon;
        $dtSaKer->fax             = $request->Fax;
        $dtSaKer->email           = $request->Email;
        $dtSaKer->website         = $request->Website;
        $dtSaKer->jenis_layanan   = $request->jenis_layanan;
        $dtSaKer->level           = '1';
        $dtSaKer->bl_leaf         = '1';
        $dtSaKer->ac_conn         = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_satuan_kerja)) {
          $dtSaKer->creator_id    = Auth::getUser()->id;
        }
        $dtSaKer->cluster_id      = 'LATIHAN1';
        $dtSaKer->bllokal         = '1';
        $dtSaKer->tipe_satker_id  = $request->Type;
        if ($request->Kelurahan != '') {
          $infoKel = DB::table('desa')->where('id_desa',$request->Kelurahan)->first();
          $dtSaKer->kelurahan     = $infoKel->nama_desa;
          $dtSaKer->kelurahan_id  = $infoKel->id_desa;
        }else{
          $dtSaKer->kelurahan     = null;
          $dtSaKer->kelurahan_id  = null;
        }
        if ($request->Kecamatan != '') {
          $infoKec = DB::table('kecamatan')->where('id_kecamatan',$request->Kecamatan)->first();
          $dtSaKer->kecamatan     = $infoKec->nama_kecamatan;
          $dtSaKer->kecamatan_id  = $infoKec->id_kecamatan;
        }else{
          $dtSaKer->kecamatan     = null;
          $dtSaKer->kecamatan_id  = null;
        }
        if ($request->Kabupaten != '') {
          $infoKab = DB::table('kabupaten')->where('id_kabupaten',$request->Kabupaten)->first();
          $dtSaKer->kabupaten     = $infoKab->nama_kabupaten;
          $dtSaKer->kabupaten_id  = $infoKab->id_kabupaten;
        }else{
          $dtSaKer->kabupaten     = null;
          $dtSaKer->kabupaten_id  = null;
        }
        if ($request->Provinsi != '') {
          $infoProv = DB::table('provinsi')->where('id_provinsi',$request->Provinsi)->first();
          $dtSaKer->provinsi      = $infoProv->nama_provinsi;
          $dtSaKer->provinsi_id   = $infoProv->id_provinsi;
        }else{
          $dtSaKer->provinsi = null;
          $dtSaKer->provinsi_id = null;
        }
        if (empty($request->id_satuan_kerja)) {
          $dtSaKer->user_id       = $user->id;
        }
        if (!empty($request->Pimpinan)) {
          if (!empty($request->id_satuan_kerja)) {
            if ($dtSaKer->pimpinan_id != '') {
              $dtUserLama = Users::find($dtSaKer->pimpinan->user_id);
              $dtUserLama->is_leader = 'N';
              $dtUserLama->save();
            }
          }
          $dtPegawai = Pegawai::find($request->Pimpinan);
          $dtUser = Users::find($dtPegawai->user_id);
          $dtUser->is_leader = 'Y';
          $dtUser->save();
          $dtSaKer->pimpinan_id   = $request->Pimpinan;
        }else{
          $dtSaKer->pimpinan_id   = '0';
        }
        $dtSaKer->save();

        if (!empty($request->id_satuan_kerja)) {
          $user = Users::find($dtSaKer->user_id);
          $user->username   = $request->Email;
          $user->email      = $request->Email;
          $user->name       = $request->Nama;
          $user->address    = $request->Alamat;
          $user->phone      = $request->Telepon;
          $user->save();
        }

        if ($dtSaKer) {
          if (empty($request->id_satuan_kerja)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Satuan Kerja '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Satuan Kerja '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function detailData(Request $request)
    {
      $data['satuanKerja'] = SatuanKerja::find($request->id);
      $content = view('master.satuanKerja.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_satuan_kerja) {
        $remove = SatuanKerja::find($id_satuan_kerja);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();

          $user = Users::find($remove->user_id);
          if (!empty($user)) {
            $user->is_banned = '1';
            $user->bl_state = 'D';
            $user->save();
          }
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Satuan Kerja '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
