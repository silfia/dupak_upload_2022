<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Models\Users;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class PegawaiController extends Controller
{
    public function main(Request $request)
    {
      if (Auth::getUser()->level_user == 1 || Auth::getUser()->level_user == 2 || Auth::getUser()->level_user == 4) {
        if (Auth::getUser()->level_user == 1 || Auth::getUser()->level_user == 2) {
          $this->data['mn_active'] = "master";
          $this->data['submn_active'] = "pegawai";
        }elseif (Auth::getUser()->level_user == 4) {
          $this->data['mn_active'] = "pegawai";
          $this->data['submn_active'] = "";
        }
        $this->data['title'] = 'Pegawai';
        $this->data['smallTitle'] = "";
        return view('master.pegawai.main')->with('data', $this->data);
      }else{
        return redirect::route('dashboard')->with('title','Maaf !')->with('message','Anda Tidak Memiliki Akses Untuk Fitur Ini !!')->with('type','warning');
      }
    }

    public function loadData(Request $request)
    {
      $data = Pegawai::getJsonPegawai($request);
      return response()->json($data);     
    }

    public function form(Request $request)
    {
      $data['pegawai'] = (!empty($request->id)) ? Pegawai::join('users','users.id','pegawai.user_id')->where('pegawai.id_pegawai', $request->id)->first() : "";
      $data['golongan'] = DataMaster::where('is_golongan','Y')->where('bl_state','A')->get();
      if (!empty($request->id)) {
        $data['jabatan'] = DataMaster::where('is_golongan','N')
                                      ->where('tipe_profesi_id',$data['pegawai']->tipe_profesi_id)
                                      ->where('bl_state','A')->get();
      }
      $data['profesi'] = MasterType::where('modul','StatusProfesi')->where('bl_state','A')->get();
      $data['satKers'] = SatuanKerja::where('bl_state','A')->get();
      $content = view('master.pegawai.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'Nama'            => 'required',
        'Jenis_Kelamin'   => 'required',
        'Tempat_Lahir'    => 'required',
        'Tanggal_Lahir'   => 'required',
        'Profesi'         => 'required',
        'Satuan_Kerja'    => 'required',
        'status_pegawai'  => 'required',
        'NIP'             => 'required',
        'Pendidikan'      => 'required',
        'Tahun_Lulus'     => 'required',
      );
      if($request->status_pegawai == 'PNS') {
        $rules['status_penilai']  = 'required';
        $rules['No_Karpeng']    = 'required';
        $rules['Golongan']      = 'required';
        $rules['Golongan_TMT']  = 'required';
        $rules['Jabatan']       = 'required';
        $rules['Jabatan_TMT']   = 'required';
      }
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (empty($request->id_pegawai)) {
          $user             = new Users;
          $user->username   = ($request->NIP != '') ? str_replace(' ', '', $request->NIP) : $request->Email;
          $user->email      = $request->Email;
          $user->password   = bcrypt('123456');
          $user->name       = $request->Nama;
          $user->gender     = ($request->Jenis_Kelamin == 'P') ? 'Laki - Laki' : 'Perempuan';
          $user->level_user = '5';
          $user->is_banned  = '0';
          if ($request->status_pegawai == 'PNS') {
            if ($request->status_penilai == 'Ya') {
              $user->is_penilai  = 'Y';
            }
          }
          $user->creator_id = Auth::getUser()->id;
          $user->save();
        }

        $dtPegawai                        = (!empty($request->id_pegawai)) ? Pegawai::find($request->id_pegawai) : new Pegawai;
        $dtPegawai->nama                  = $request->Nama;
        $dtPegawai->no_nip                = $request->NIP;
        $dtPegawai->tempat_lahir          = $request->Tempat_Lahir;
        $dtPegawai->tanggal_lahir         = date('Y-m-d', strtotime($request->Tanggal_Lahir));
        $dtPegawai->jenis_kelamin         = $request->Jenis_Kelamin;
        if ($request->status_pegawai == 'PNS') {
          $dtPegawai->status_penilai        = $request->status_penilai;
          $dtPegawai->no_karpeng            = $request->No_Karpeng;
          $dtPegawai->golongan_id           = $request->Golongan;
          $dtPegawai->golongan_tmt          = date('Y-m-d', strtotime($request->Golongan_TMT));
          $dtPegawai->jabatan_id            = $request->Jabatan;
          $dtPegawai->jabatan_tmt           = date('Y-m-d', strtotime($request->Jabatan_TMT));
        }else{
          $dtPegawai->status_penilai        = 'Tidak';
          $dtPegawai->no_karpeng            = '';
          $dtPegawai->golongan_id           = '0';
          $dtPegawai->golongan_tmt          = '1000-01-01';
          $dtPegawai->jabatan_id            = '0';
          $dtPegawai->jabatan_tmt           = '1000-01-01';
        }
        $dtPegawai->pendidikan_terakhir   = $request->Pendidikan;
        $dtPegawai->pendidikan_tahun      = $request->Tahun_Lulus;
        $dtPegawai->satuan_kerja_id       = $request->Satuan_Kerja;
        $dtPegawai->bl_note               = '0';
        $dtPegawai->tipe_profesi_id       = $request->Profesi;
        $dtPegawai->status_pegawai        = $request->status_pegawai;
        $dtPegawai->ac_conn               = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_master)) {
          $dtPegawai->creator_id          = Auth::getUser()->id;
        }
        $dtPegawai->cluster_id            = 'LATIHAN1';
        $dtPegawai->bllokal               = '1';
        if (empty($request->id_pegawai)) {
          $dtPegawai->user_id             = $user->id;
        }
        $dtPegawai->save();

        if (!empty($request->id_pegawai)) {
          $user             = Users::find($dtPegawai->user_id);
          $user->username   = ($request->NIP != '') ? str_replace(' ', '', $request->NIP) : $request->Email;
          $user->email      = $request->Email;
          $user->name       = $request->Nama;
          $user->gender     = ($request->Jenis_Kelamin == 'P') ? 'Laki - Laki' : 'Perempuan';
          if ($dtPegawai->status_pegawai == 'PNS') {
            if ($dtPegawai->status_penilai == 'Ya') {
              $user->is_penilai  = 'Y';
            }else{
              $user->is_penilai  = 'N';
            }
          }else{
            $user->is_penilai  = 'N';
          }
          $user->save();
        }

        if ($dtPegawai) {
          if (empty($request->id_pegawai)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Pegawai '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Pegawai '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function detailData(Request $request)
    {
      $data['pegawai'] = Pegawai::join('users','users.id','pegawai.user_id')
                                  ->where('pegawai.id_pegawai', $request->id)->first();
      $content = view('master.pegawai.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_pegawai) {
        $remove = Pegawai::find($id_pegawai);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();

          $user = Users::find($remove->user_id);
          if (!empty($user)) {
            $user->is_banned = '1';
            $user->bl_state = 'D';
            $user->save();
          }
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Pegawai '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
