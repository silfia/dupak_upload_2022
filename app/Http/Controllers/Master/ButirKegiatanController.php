<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterType;
use App\Models\DataMaster;
use App\Models\PakMasterKegiatan;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;

class ButirKegiatanController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "butirKegiatan";
      $this->data['title'] = 'Butir Kegiatan';
      $this->data['smallTitle'] = "";
      return view('master.butirKegiatan.main')->with('data', $this->data);
    }

    public function loadDataProfesi(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('master_type')
                  ->where('nama','like','%'.$request->search.'%')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('master_type')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function detailData(Request $request)
    {
      $data['id_profesi'] = $request->id;
      $content = view('master.butirKegiatan.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function loadData(Request $request)
    {
      $butir = [];
      $no = 0;
      $unsur = PakMasterKegiatan::select('master_type.*')
                                  ->join('master_type','master_type.id_master_type','pak_master_kegiatan.unsur_pak_id')
                                  ->where('master_type.modul','SatuanUnsur')
                                  ->where('pak_master_kegiatan.formasi_id', $request->id_profesi)
                                  ->where('pak_master_kegiatan.jenis','Butir')
                                  ->where('pak_master_kegiatan.level','1')
                                  ->where('pak_master_kegiatan.bl_state', 'A')
                                  ->orderBy('master_type.urutan','ASC')->distinct()->get();
      // return $unsur;
      if (count($unsur) > 0) {
        foreach ($unsur as $uns) {
          $butir[$no] = $uns;
          $lv1 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                    ->where('unsur_pak_id', $uns->id_master_type)
                                    ->where('jenis', 'Butir')
                                    ->where('level', '1')
                                    ->where('bl_state', 'A')
                                    ->get();
          if (count($lv1) > 0) {
            $butir1 = [];
            $noLv1 = 0;
            foreach ($lv1 as $key1) {
              $butir1[$noLv1] = $key1;
              $lv2 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                        ->where('unsur_pak_id', $uns->id_master_type)
                                        ->where('jenis', 'Butir')
                                        ->where('level', '2')
                                        ->where('bl_state', 'A')
                                        ->where('parent_id', $key1->id_pak_master)
                                        ->get();
              if (count($lv2) > 0) {
                $butir2 = [];
                $noLv2 = 0;
                foreach ($lv2 as $key2) {
                  $butir2[$noLv2] = $key2;
                  $lv3 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                            ->where('unsur_pak_id', $uns->id_master_type)
                                            ->where('jenis', 'Butir')
                                            ->where('level', '3')
                                            ->where('bl_state', 'A')
                                            ->where('parent_id', $key2->id_pak_master)
                                            ->get();
                  if (count($lv3)) {
                    $butir3 = [];
                    $noLv3 = 0;
                    foreach ($lv3 as $key3) {
                      $butir3[$noLv3] = $key3;
                      $lv4 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                                ->where('unsur_pak_id', $uns->id_master_type)
                                                ->where('jenis', 'Butir')
                                                ->where('level', '4')
                                                ->where('bl_state', 'A')
                                                ->where('parent_id', $key3->id_pak_master)
                                                ->get();
                      if (count($lv4) > 0) {
                        $butir4 = [];
                        $noLv4 = 0;
                        foreach ($lv4 as $key4) {
                          $butir4[$noLv4] = $key4;
                          $lv5 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                                    ->where('unsur_pak_id', $uns->id_master_type)
                                                    ->where('jenis', 'Butir')
                                                    ->where('level', '5')
                                                    ->where('bl_state', 'A')
                                                    ->where('parent_id', $key4->id_pak_master)
                                                    ->get();
                          if (count($lv5) > 0) {
                            $butir5 = [];
                            $noLv5 = 0;
                            foreach ($lv5 as $key5) {
                              $butir5[$noLv5] = $key5;
                              $lv6 = PakMasterKegiatan::where('formasi_id', $request->id_profesi)
                                                        ->where('unsur_pak_id', $uns->id_master_type)
                                                        ->where('jenis', 'Butir')
                                                        ->where('level', '6')
                                                        ->where('bl_state', 'A')
                                                        ->where('parent_id', $key5->id_pak_master)
                                                        ->get();
                              if (count($lv6) > 0) {
                                $butir6 = [];
                                $noLv6 = 0;
                                foreach ($lv6 as $key6) {
                                  $butir6[$noLv6] = $key6;
                                  if ($key6->is_title == '1') {
                                    $butir6[$noLv6]['statusRow'] = 'Parent';
                                  }else{
                                    $butir6[$noLv6]['statusRow'] = 'Child';
                                    $butir6[$noLv6]['nama_jabatan'] = $key6->jabatan->nama;
                                  }
                                  $butir6[$noLv6]['row'] = [];
                                  $noLv6++;
                                }
                                $butir5[$noLv5]['statusRow'] = 'Parent';
                                $butir5[$noLv5]['row'] = $butir6;
                              }else{
                                if ($key5->is_title == '1') {
                                  $butir5[$noLv5]['statusRow'] = 'Parent';
                                }else{
                                  $butir5[$noLv5]['statusRow'] = 'Child';
                                  $butir5[$noLv5]['nama_jabatan'] = $key5->jabatan->nama;
                                }
                                $butir5[$noLv5]['row'] = [];
                              }
                              $noLv5++;
                            }
                            $butir4[$noLv4]['statusRow'] = 'Parent';
                            $butir4[$noLv4]['row'] = $butir5;
                          }else{
                            if ($key4->is_title == '1') {
                              $butir4[$noLv4]['statusRow'] = 'Parent';
                            }else{
                              $butir4[$noLv4]['statusRow'] = 'Child';
                              $butir4[$noLv4]['nama_jabatan'] = $key4->jabatan->nama;
                            }
                            $butir4[$noLv4]['row'] = [];
                          }
                          $noLv4++;
                        }
                        $butir3[$noLv3]['statusRow'] = 'Parent';
                        $butir3[$noLv3]['row'] = $butir4;
                      }else{
                        if ($key3->is_title == '1') {
                          $butir3[$noLv3]['statusRow'] = 'Parent';
                        }else{
                          $butir3[$noLv3]['statusRow'] = 'Child';
                          $butir3[$noLv3]['nama_jabatan'] = $key3->jabatan->nama;
                        }
                        $butir3[$noLv3]['row'] = [];
                      }
                      $noLv3++;
                    }
                    $butir2[$noLv2]['statusRow'] = 'Parent';
                    $butir2[$noLv2]['row'] = $butir3;
                  }else{
                    if ($key2->is_title == '1') {
                      $butir2[$noLv2]['statusRow'] = 'Parent';
                    }else{
                      $butir2[$noLv2]['statusRow'] = 'Child';
                      $butir2[$noLv2]['nama_jabatan'] = $key2->jabatan->nama;
                    }
                    $butir2[$noLv2]['row'] = [];
                  }
                  $noLv2++;
                }
                $butir1[$noLv1]['statusRow'] = 'Parent';
                $butir1[$noLv1]['row'] = $butir2;
              }else{
                if ($key1->is_title == '1') {
                  $butir1[$noLv1]['statusRow'] = 'Parent';
                }else{
                  $butir1[$noLv1]['statusRow'] = 'Child';
                  $butir1[$noLv1]['nama_jabatan'] = $key1->jabatan->nama;
                }
                $butir1[$noLv1]['row'] = [];
              }
              $noLv1++;
            }
            $butir[$no]['statusRow'] = 'Parent';
            $butir[$no]['row'] = $butir1;
          }else{
            $butir[$no]['statusRow'] = 'Parent';
            $butir[$no]['row'] = [];
          }
          $no++;
        }
        $data['butir'] = $butir;
        $return = ['status'=>'success','code'=>200,'row'=>$data];
      }else{
        $data['butir'] = $butir;
        $return = ['status'=>'empty','code'=>404,'row'=>$data];
      }
      return response()->json($return);
    }

    function getInduk(Request $request)
    {
      $induk = [];
      $lv1 = PakMasterKegiatan::where('jenis','Butir')
                                ->where('formasi_id', $request->idProfesi)
                                ->where('unsur_pak_id', $request->unsur)
                                ->where('level', '1')
                                ->where('is_title', '1')
                                ->where('bl_state', 'A')->get();
      if (count($lv1) > 0) {
        $noLv1 = 0;
        foreach ($lv1 as $key1) {
          $induk[$noLv1] = $key1;
          $lv2 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                    ->where('unsur_pak_id', $request->unsur)
                                    ->where('jenis', 'Butir')
                                    ->where('level', '2')
                                    ->where('is_title', '1')
                                    ->where('bl_state', 'A')
                                    ->where('parent_id', $key1->id_pak_master)
                                    ->get();
          if (count($lv2) > 0) {
            $induk2 = [];
            $induk[$noLv1]['statusRow'] = 'Parent';
            $noLv2 = 0;
            foreach ($lv2 as $key2) {
              $induk2[$noLv2] = $key2;
              $lv3 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                        ->where('unsur_pak_id', $request->unsur)
                                        ->where('jenis', 'Butir')
                                        ->where('level', '3')
                                        ->where('is_title', '1')
                                        ->where('bl_state', 'A')
                                        ->where('parent_id', $key2->id_pak_master)
                                        ->get();
              if (count($lv3) > 0) {
                $induk3 = [];
                $induk2[$noLv2]['statusRow'] = 'Parent';
                $noLv3 = 0;
                foreach ($lv3 as $key3) {
                  $induk3[$noLv3] = $key3;
                  $lv4 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                            ->where('unsur_pak_id', $request->unsur)
                                            ->where('jenis', 'Butir')
                                            ->where('level', '4')
                                            ->where('is_title', '1')
                                            ->where('bl_state', 'A')
                                            ->where('parent_id', $key3->id_pak_master)
                                            ->get();
                  if (count($lv4) > 0) {
                    $induk4 = [];
                    $induk3[$noLv3]['statusRow'] = 'Parent';
                    $noLv4 = 0;
                    foreach ($lv4 as $key4) {
                      $induk4[$noLv4] = $key4;
                      $lv5 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                                ->where('unsur_pak_id', $request->unsur)
                                                ->where('jenis', 'Butir')
                                                ->where('level', '5')
                                                ->where('is_title', '1')
                                                ->where('bl_state', 'A')
                                                ->where('parent_id', $key4->id_pak_master)
                                                ->get();
                      if (count($lv5) > 0) {
                        $induk5 = [];
                        $induk4[$noLv4]['statusRow'] = 'Parent';
                        $noLv5 = 0;
                        foreach ($lv5 as $key5) {
                          $induk5[$noLv5] = $key5;
                          $lv6 = PakMasterKegiatan::where('formasi_id', $request->idProfesi)
                                                    ->where('unsur_pak_id', $request->unsur)
                                                    ->where('jenis', 'Butir')
                                                    ->where('level', '6')
                                                    ->where('is_title', '1')
                                                    ->where('bl_state', 'A')
                                                    ->where('parent_id', $key5->id_pak_master)
                                                    ->get();
                          if (count($lv6) > 0) {
                            $induk6 = [];
                            $induk5[$noLv5]['statusRow'] = 'Parent';
                            $noLv6 = 0;
                            $induk6[$noLv6] = $lv6;
                          }else{
                            $induk5[$noLv5]['row'] = [];
                          }
                          $noLv5++;
                        }
                        $induk4[$noLv4]['row'] = $induk5;
                      }else{
                        $induk4[$noLv4]['row'] = [];
                      }
                      $noLv4++;
                    }
                    $induk3[$noLv3]['row'] = $induk4;
                  }else{
                    $induk3[$noLv3]['row'] = [];
                  }
                  $noLv3++;
                }
                $induk2[$noLv2]['row'] = $induk3;
              }else{
                $induk2[$noLv2]['row'] = [];
              }
              $noLv2++;
            }
            $induk[$noLv1]['row'] = $induk2;
          }else{
            $induk[$noLv1]['statusRow'] = 'Parent';
            $induk[$noLv1]['row'] = [];
          }
          $noLv1++;
        }
      }

      $data['induk'] = $induk;
      return ['status' => 'success', 'data' => $data];
    }

    public function form(Request $request)
    {
      $data['kegiatan'] = (!empty($request->id)) ? PakMasterKegiatan::find($request->id) : "";
      $data['profesi'] = MasterType::find($request->id_profesi);
      $data['unsurs'] = MasterType::where('modul','SatuanUnsur')->where('tipe_profesi_id', $request->id_profesi)->where('bl_state','A')->get();
      $data['jabatan'] = DataMaster::where('tipe_profesi_id', $request->id_profesi)
                                    ->where('is_golongan', 'N')
                                    ->where('bl_state','A')->get();
      $content = view('master.butirKegiatan.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'id_profesi'  => 'required',
        'Unsur'       => 'required',
        'Nama'        => 'required',
        'Turunan'     => 'required',
        'Judul'       => 'required',
      );
      if ($request->Turunan == '1') {
        $rules['Induk']   = 'required';
      };
      if ($request->Judul == '0') {
        $rules['Jabatan'] = 'required';
        $rules['Minimal'] = 'required';
        $rules['Satuan']  = 'required';
        $rules['Point']   = 'required';
      };
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $dtButir =  (!empty($request->id_pak_master)) ? PakMasterKegiatan::find($request->id_pak_master) : new PakMasterKegiatan;
        if ($request->Turunan == 0) {
          $dtButir->level         = '1';
          $dtButir->level_1       = null;
          $dtButir->parent_id     = null;
        }else{
          $dtInduk = PakMasterKegiatan::find($request->Induk);
          $dtButir->level         = $dtInduk->level + 1;
          $lvInduk = $dtInduk->level;
          $dtIndukId = $dtInduk->id_pak_master;
          while ($lvInduk != 1) {
            $dtCrLv1 = PakMasterKegiatan::find($dtIndukId);
            $lvInduk = $dtCrLv1->level;
            if ($dtCrLv1->level == 1) {
              $dtIndukId = $dtCrLv1->id_pak_master;
            }else{
              $dtIndukId = $dtCrLv1->parent_id;
            }
          }
          $dtButir->level_1       = $dtIndukId;
          $dtButir->parent_id     = $request->Induk;
        }
        $dtButir->is_title        = $request->Judul;
        $dtButir->is_child        = $request->Turunan;
        $dtButir->butir_kegiatan  = $request->Nama;
        $dtButir->keterangan      = $request->Keterangan;
        if ($request->Judul == '0') {
          $dtButir->jum_min       = $request->Minimal;
          $dtButir->satuan        = $request->Satuan;
          $dtButir->points        = $request->Point;
          $dtButir->jabatan_id    = $request->Jabatan;
        }else{
          $dtButir->jum_min       = null;
          $dtButir->satuan        = null;
          $dtButir->points        = null;
          $dtButir->jabatan_id    = null;
        }
        $dtButir->formasi_id      = $request->id_profesi;
        $dtButir->unsur_pak_id    = $request->Unsur;
        $dtButir->jenis           = 'Butir';
        if (empty($request->id_master)) {
          $dtButir->creator_id    = Auth::getUser()->id;
        }
        $dtButir->ac_conn         = $_SERVER['REMOTE_ADDR'];
        $dtButir->save();

        if ($dtButir) {
          if (empty($request->id_pak_master)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Butir Kegiatan '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Butir Kegiatan '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_pak) {
        $remove = PakMasterKegiatan::find($id_pak);
        if (!empty($remove)) {
          if ($no == 1) {
            $profesi = $remove->formasi->nama;
            $nama = $remove->butir_kegiatan;
          }else{
            $nama = $nama.', '.$remove->butir_kegiatan;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Butir Kegiatan (Profesi : '.$profesi.') '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
