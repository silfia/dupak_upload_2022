<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DataMaster;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class GolonganController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "golongan";
      $this->data['title'] = 'Golongan';
      $this->data['smallTitle'] = "";
      return view('master.golongan.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('data_master')
                  ->where('nama','like','%'.$request->search.'%')
                  ->where('is_golongan','Y')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('data_master')
                  ->where('is_golongan','Y')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['master'] = (!empty($request->id)) ? DataMaster::find($request->id) : "";
      $content = view('master.golongan.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'Nama'    => 'required',
        'Urutan'  => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (empty($request->id_master)) { // Tambah Baru
          $cekGolongan = DataMaster::where('is_golongan', 'Y')->where('bl_state','A')->get();
          if (count($cekGolongan) > 0) {
            $cekUrutan = DataMaster::where('is_golongan', 'Y')->where('bl_state','A')->where('urutan', $request->Urutan)->first();
            if (!empty($cekUrutan)) {
              $getUrutans = DataMaster::where('is_golongan', 'Y')
                                        ->where('bl_state','A')
                                        ->where('urutan', '>=', $request->Urutan)
                                        ->orderBy('urutan','ASC')->get();
              if (count($getUrutans) > 0) {
                $urut = $request->Urutan;
                foreach ($getUrutans as $getUrutan) {
                  $urut = $urut + 1;
                  $updateUrut = DataMaster::where('id_master', $getUrutan->id_master)
                                            ->update(['urutan' => $urut]);
                }
              }
            }
          }
        }else{ // Untuk Edit
          $cekGolongan = DataMaster::where('is_golongan', 'Y')
                                    ->where('bl_state','A')
                                    ->where('id_master','!=',$request->id_master)->get();
          if (count($cekGolongan) > 0) {
            $dtLama = DataMaster::find($request->id_master);
            if ($dtLama->urutan < $request->Urutan) {
              $getUrutans = DataMaster::where('is_golongan','Y')
                                      ->where('bl_state','A')
                                      ->where('urutan','>=', $dtLama->urutan)
                                      ->where('id_master', '!=', $request->id_master)
                                      ->orderBy('urutan','ASC')->get();
              if (count($getUrutans) > 0) {
                $urut = $dtLama->urutan;
                foreach ($getUrutans as $getUrutan) {
                  if ($urut == $request->Urutan){ $urut = $urut + 1; }
                  $updateUrut = DataMaster::where('id_master', $getUrutan->id_master)
                                            ->update(['urutan' => $urut]);
                  $urut = $urut + 1;
                }
              }
            }elseif ($dtLama->urutan > $request->Urutan) {
              $cekUrutanBaru = DataMaster::where('is_golongan','Y')->where('bl_state','A')->where('urutan', $request->Urutan)->first();
              if (!empty($cekUrutanBaru)) {
                $getUrutans = DataMaster::where('is_golongan','Y')
                                        ->where('bl_state','A')
                                        ->where('urutan','>=', $request->Urutan)
                                        ->where('id_master','!=', $request->id_master)
                                        ->orderBy('urutan','ASC')->get();
                if (count($getUrutans) > 0) {
                  $urut = $request->Urutan;
                  foreach ($getUrutans as $getUrutan) {
                    $urut = $urut + 1;
                    $updateUrut = DataMaster::where('id_master', $getUrutan->id_master)
                                              ->update(['urutan' => $urut]);
                  }
                }
              }else{
                $getUrutans = DataMaster::where('is_golongan','Y')
                                        ->where('bl_state','A')
                                        ->where('urutan','>=', $request->Urutan)
                                        ->orderBy('urutan','ASC')->get();
                if (count($getUrutans) > 0) {
                  $urut = $request->Urutan;
                  foreach ($getUrutans as $getUrutan) {
                    $updateUrut = DataMaster::where('id_master', $getUrutan->id_master)
                                              ->update(['urutan' => $urut]);
                    $urut = $urut + 1;
                  }
                }
              }
            }
          }
        }

        $dtMaster =  (!empty($request->id_master)) ? DataMaster::find($request->id_master) : new DataMaster;
        $dtMaster->nama = $request->Nama;
        $dtMaster->urutan = $request->Urutan;
        $dtMaster->is_golongan = "Y";
        $dtMaster->ac_conn = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_master)) {
          $dtMaster->creator_id = Auth::getUser()->id;
        }
        $dtMaster->cluster_id = 'LATIHAN1';
        $dtMaster->bllokal = '1';
        $dtMaster->save();

        if ($dtMaster) {
          if (empty($request->id_master)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Golongan '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Golongan '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_master) {
        $remove = DataMaster::find($id_master);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Golongan '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
