<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterType;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class typeSaKerController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "typeSaKer";
      $this->data['title'] = 'Type Satuan Kerja';
      $this->data['smallTitle'] = "";
      return view('master.typeSaKer.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('master_type')
                  ->where('nama','like','%'.$request->search.'%')
                  ->where('modul','SatuanOrganisasi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('master_type')
                  ->where('modul','SatuanOrganisasi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['type'] = (!empty($request->id)) ? MasterType::find($request->id) : "";
      $content = view('master.typeSaKer.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'Nama'        => 'required',
        'Urutan'      => 'required',
        // 'Keterangan'  => 'required'
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (empty($request->id_master_type)) { // Tambah Baru
          $cekProfesi = MasterType::where('modul', 'SatuanOrganisasi')->where('bl_state','A')->get();
          if (count($cekProfesi) > 0) {
            $cekUrutan = MasterType::where('modul', 'SatuanOrganisasi')->where('bl_state','A')->where('urutan', $request->Urutan)->first();
            if (!empty($cekUrutan)) {
              $getUrutans = MasterType::where('modul', 'SatuanOrganisasi')
                                        ->where('bl_state','A')
                                        ->where('urutan', '>=', $request->Urutan)
                                        ->orderBy('urutan','ASC')->get();
              if (count($getUrutans) > 0) {
                $urut = $request->Urutan;
                foreach ($getUrutans as $getUrutan) {
                  $urut = $urut + 1;
                  $updateUrut = MasterType::where('id_master_type', $getUrutan->id_master_type)
                                            ->update(['urutan' => $urut]);
                }
              }
            }
          }
        }else{ // Untuk Edit
          $cekProfesi = MasterType::where('modul', 'SatuanOrganisasi')
                                    ->where('bl_state','A')
                                    ->where('id_master_type','!=',$request->id_master_type)->get();
          if (count($cekProfesi) > 0) {
            $dtLama = MasterType::find($request->id_master_type);
            if ($dtLama->urutan < $request->Urutan) {
              $getUrutans = MasterType::where('modul','SatuanOrganisasi')
                                      ->where('bl_state','A')
                                      ->where('urutan','>=', $dtLama->urutan)
                                      ->where('id_master_type', '!=', $request->id_master_type)
                                      ->orderBy('urutan','ASC')->get();
              if (count($getUrutans) > 0) {
                $urut = $dtLama->urutan;
                foreach ($getUrutans as $getUrutan) {
                  if ($urut == $request->Urutan){ $urut = $urut + 1; }
                  $updateUrut = MasterType::where('id_master_type', $getUrutan->id_master_type)
                                            ->update(['urutan' => $urut]);
                  $urut = $urut + 1;
                }
              }
            }elseif ($dtLama->urutan > $request->Urutan) {
              $cekUrutanBaru = MasterType::where('modul','SatuanOrganisasi')->where('bl_state','A')->where('urutan', $request->Urutan)->first();
              if (!empty($cekUrutanBaru)) {
                $getUrutans = MasterType::where('modul','SatuanOrganisasi')
                                        ->where('bl_state','A')
                                        ->where('urutan','>=', $request->Urutan)
                                        ->where('id_master_type','!=', $request->id_master_type)
                                        ->orderBy('urutan','ASC')->get();
                if (count($getUrutans) > 0) {
                  $urut = $request->Urutan;
                  foreach ($getUrutans as $getUrutan) {
                    $urut = $urut + 1;
                    $updateUrut = MasterType::where('id_master_type', $getUrutan->id_master_type)
                                              ->update(['urutan' => $urut]);
                  }
                }
              }else{
                $getUrutans = MasterType::where('modul','SatuanOrganisasi')
                                        ->where('bl_state','A')
                                        ->where('urutan','>=', $request->Urutan)
                                        ->orderBy('urutan','ASC')->get();
                if (count($getUrutans) > 0) {
                  $urut = $request->Urutan;
                  foreach ($getUrutans as $getUrutan) {
                    $updateUrut = MasterType::where('id_master_type', $getUrutan->id_master_type)
                                              ->update(['urutan' => $urut]);
                    $urut = $urut + 1;
                  }
                }
              }
            }
          }
        }

        $dtType =  (!empty($request->id_master_type)) ? MasterType::find($request->id_master_type) : new MasterType;
        $dtType->modul = "SatuanOrganisasi";
        $dtType->nama = $request->Nama;
        $dtType->keterangan = $request->Keterangan;
        $dtType->urutan = $request->Urutan;
        $dtType->ac_conn = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_master_type)) {
          $dtType->creator_id = Auth::getUser()->id;
        }
        $dtType->cluster_id = 'LATIHAN1';
        $dtType->bllokal = '1';
        $dtType->save();

        if ($dtType) {
          if (empty($request->id_master_type)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Type Satuan Kerja '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Type Satuan Kerja '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_master_type) {
        $remove = MasterType::find($id_master_type);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Type Satuan Kerja '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
