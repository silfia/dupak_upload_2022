<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterType;
use App\Models\PermenpanKegiatan;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class UnsurPAKController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "unsurPak";
      $this->data['title'] = 'Unsur Penetapan Angka Kredit';
      $this->data['smallTitle'] = "";
      return view('master.unsurPak.main')->with('data', $this->data);
    }

    public function loadDataProfesi(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('master_type')
                  ->where('nama','like','%'.$request->search.'%')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('master_type')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function detailData(Request $request)
    {
      $data['profesi'] = MasterType::find($request->id);
      $content = view('master.unsurPak.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function loadData(Request $request)
    {
      $data = DB::table('master_type')
                  ->where('modul','SatuanUnsur')
                  ->where('tipe_profesi_id',$request->id_profesi)
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->get();
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {      
      $data['profesi'] = MasterType::find($request->id_profesi);
      $data['permenpan'] = PermenpanKegiatan::where('tipe_profesi_id', $request->id_profesi)->where('status','Aktif')->get();
      $data['type'] = (!empty($request->id)) ? MasterType::find($request->id) : "";
      $content = view('master.unsurPak.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      // return $request->all();
      $rules = array(
        'Nama'        => 'required',
        'Jenis_Unsur' => 'required',
        'id_profesi'  => 'required',
        'permenpan'  => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $cekUrutan = MasterType::where('modul', 'SatuanUnsur')->where('bl_state','A')->orderBy('urutan', 'DESC')->first();

        $dtType                   =  (!empty($request->id_master_type)) ? MasterType::find($request->id_master_type) : new MasterType;
        $dtType->modul            = "SatuanUnsur";
        $dtType->nama             = $request->Nama;
        $dtType->keterangan       = $request->Jenis_Unsur;
        $dtType->tipe_profesi_id  = $request->id_profesi;
        $dtType->permenpan_id     = $request->permenpan;        
        $dtType->urutan           = (!empty($cekUrutan)) ? $cekUrutan->urutan + 1 : 1;
        $dtType->ac_conn          = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_master_type)) {
          $dtType->creator_id     = Auth::getUser()->id;
        }
        $dtType->cluster_id       = 'LATIHAN1';
        $dtType->bllokal          = '1';
        $dtType->save();

        if ($dtType) {
          if (empty($request->id_master_type)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Satuan Unsur PAK '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Satuan Unsur PAK '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_master_type) {
        $remove = MasterType::find($id_master_type);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Satuan Unsur PAK '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
