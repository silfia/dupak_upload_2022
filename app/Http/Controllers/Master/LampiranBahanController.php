<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterLb;
use App\Models\MasterType;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class LampiranBahanController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "lampiranBahan";
      $this->data['title'] = 'Lampiran Bahan';
      $this->data['smallTitle'] = "";
      return view('master.lampiranBahan.main')->with('data', $this->data);
    }

    public function loadDataProfesi(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('master_type')
                  ->where('nama','like','%'.$request->search.'%')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('master_type')
                  ->where('modul','StatusProfesi')
                  ->where('bl_state','A')
                  ->orderBy('urutan','ASC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function detailData(Request $request)
    {
      $data['id_profesi'] = $request->id;
      // $data['masterLbs'] = MasterLb::where('formasi_id', $request->id)->orderBy('jenis','ASC')->orderBy('urutan','ASC')->get();
      $content = view('master.lampiranBahan.detail', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function loadData(Request $request)
    {
      $data['bahan'] = MasterLb::where('formasi_id', $request->id_profesi)
                        ->where('jenis','lampiran_dinilai')
                        ->where('bl_state', 'A')
                        ->orderBy('urutan','ASC')
                        ->get();
      $data['dupak'] = MasterLb::where('formasi_id', $request->id_profesi)
                        ->where('jenis','lampiran_dupak')
                        ->where('bl_state', 'A')
                        ->orderBy('urutan','ASC')
                        ->get();
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['master'] = (!empty($request->id)) ? MasterLb::find($request->id) : "";
      $data['profesi'] = MasterType::find($request->id_profesi);
      $content = view('master.lampiranBahan.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveData(Request $request)
    {
      $rules = array(
        'id_profesi'  => 'required',
        'Jenis'       => 'required',
        'Nama'        => 'required',
        'Urutan'      => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        if (empty($request->id_master)) { // Tambah Baru
          $cekMaster = MasterLb::where('jenis', $request->Jenis)
                                ->where('formasi_id', $request->id_profesi)
                                ->where('bl_state', 'A')->get();
          if (count($cekMaster) > 0) {
            $cekUrutan = MasterLb::where('jenis', $request->Jenis)
                                  ->where('formasi_id', $request->id_profesi)
                                  ->where('bl_state','A')
                                  ->where('urutan', $request->Urutan)->first();
            if (!empty($cekUrutan)) {
              $getUrutans = MasterLb::where('jenis', $request->Jenis)
                                      ->where('formasi_id', $request->id_profesi)
                                      ->where('bl_state','A')
                                      ->where('urutan', '>=', $request->Urutan)
                                      ->orderBy('urutan','ASC')->get();
              if (count($getUrutans) > 0) {
                $urut = $request->Urutan;
                foreach ($getUrutans as $getUrutan) {
                  $urut = $urut + 1;
                  $updateUrut = MasterLb::where('id_master_lb', $getUrutan->id_master_lb)
                                          ->update(['urutan' => $urut]);
                }
              }
            }
          }
        }else{ // Untuk Edit
          $cekMaster = MasterLb::where('jenis', $request->Jenis)
                                  ->where('formasi_id', $request->id_profesi)
                                  ->where('bl_state','A')
                                  ->where('id_master_lb','!=',$request->id_master_lb)->get();
          if (count($cekMaster) > 0) {
            $dtLama = MasterLb::find($request->id_master_lb);
            if ($dtLama->urutan < $request->Urutan) {
              $getUrutans = MasterLb::where('jenis', $request->Jenis)
                                    ->where('formasi_id', $request->id_profesi)
                                    ->where('bl_state','A')
                                    ->where('urutan','>=', $dtLama->urutan)
                                    ->where('id_master_lb', '!=', $request->id_master_lb)
                                    ->orderBy('urutan','ASC')->get();
              if (count($getUrutans) > 0) {
                $urut = $dtLama->urutan;
                foreach ($getUrutans as $getUrutan) {
                  if ($urut == $request->Urutan){ $urut = $urut + 1; }
                  $updateUrut = MasterLb::where('id_master_lb', $getUrutan->id_master_lb)
                                        ->update(['urutan' => $urut]);
                  $urut = $urut + 1;
                }
              }
            }elseif ($dtLama->urutan > $request->Urutan) {
              $cekUrutanBaru = MasterLb::where('jenis', $request->Jenis)
                                        ->where('formasi_id', $request->id_profesi)
                                        ->where('bl_state','A')
                                        ->where('urutan', $request->Urutan)->first();
              if (!empty($cekUrutanBaru)) {
                $getUrutans = MasterLb::where('jenis', $request->Jenis)
                                        ->where('formasi_id', $request->id_profesi)
                                        ->where('bl_state','A')
                                        ->where('urutan','>=', $request->Urutan)
                                        ->where('id_master_lb','!=', $request->id_master_lb)
                                        ->orderBy('urutan','ASC')->get();
                if (count($getUrutans) > 0) {
                  $urut = $request->Urutan;
                  foreach ($getUrutans as $getUrutan) {
                    $urut = $urut + 1;
                    $updateUrut = MasterLb::where('id_master_lb', $getUrutan->id_master_lb)
                                          ->update(['urutan' => $urut]);
                  }
                }
              }else{
                $getUrutans = MasterLb::where('jenis', $request->Jenis)
                                        ->where('formasi_id', $request->id_profesi)
                                        ->where('bl_state','A')
                                        ->where('urutan','>=', $request->Urutan)
                                        ->orderBy('urutan','ASC')->get();
                if (count($getUrutans) > 0) {
                  $urut = $request->Urutan;
                  foreach ($getUrutans as $getUrutan) {
                    $updateUrut = MasterLb::where('id_master_lb', $getUrutan->id_master_lb)
                                          ->update(['urutan' => $urut]);
                    $urut = $urut + 1;
                  }
                }
              }
            }
          }
        }

        $dtMaster =  (!empty($request->id_master_lb)) ? MasterLb::find($request->id_master_lb) : new MasterLb;
        $dtMaster->jenis = $request->Jenis;
        $dtMaster->nama = $request->Nama;
        $dtMaster->keterangan = $request->Keterangan;
        $dtMaster->formasi_id = $request->id_profesi;
        $dtMaster->urutan = $request->Urutan;
        if (empty($request->id_master)) {
          $dtMaster->creator_id = Auth::getUser()->id;
        }
        $dtMaster->ac_conn = $_SERVER['REMOTE_ADDR'];
        $dtMaster->save();

        if ($dtMaster) {
          if (empty($request->id_master_lb)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Master Lampiran Bahan '.$request->Nama,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Master Lampiran Bahan '.$request->Nama,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_master_lb) {
        $remove = MasterLb::find($id_master_lb);
        if (!empty($remove)) {
          if ($no == 1) {
            $profesi = $remove->formasi->nama;
            $nama = $remove->nama;
          }else{
            $nama = $nama.', '.$remove->nama;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Lampiran Bahan (Profesi : '.$profesi.') '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
