<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DataSetting;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class SettingController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "master";
      $this->data['submn_active'] = "setting";
      $this->data['title'] = 'Setting';
      $this->data['smallTitle'] = "";
      $this->data['settings'] = DataSetting::orderBy('id_setting','DESC')->first();
      return view('master.setting.main')->with('data', $this->data);
    }

    public function saveSetting(Request $request)
    {
      $rules = array(
        'Nama_Kepala'           => 'required',
        'Jabatan_Kepala'        => 'required',
        'NIP_Kepala'            => 'required',
        'Nama_Koordinator'      => 'required',
        'Jabatan_Koordinator'   => 'required',
        'NIP_Koordinator'       => 'required',
        'Kode_SKPAK'            => 'required',
        'Kode_Dinas'            => 'required',
      );
      $messages = array(
        'required'              => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $setting                    = (!empty($request->id_setting)) ? DataSetting::find($request->id_setting) : new DataSetting;
        $setting->sebagai           = $request->Sebagai;
        $setting->keterangan        = $request->Keterangan;
        $setting->nama_kepala       = $request->Nama_Kepala;
        $setting->jabatan_kepala    = $request->Jabatan_Kepala;
        $setting->nip_kepala        = $request->NIP_Kepala;
        $setting->pengganti_jabatan = (!empty($request->penggati_jabatan)) ? $request->penggati_jabatan : '0';
        $setting->nama_koor         = $request->Nama_Koordinator;
        $setting->jabatan_koor      = $request->Jabatan_Koordinator;
        $setting->nip_koor          = $request->NIP_Koordinator;
        $setting->kode_skpak        = $request->Kode_SKPAK;
        $setting->kode_dinas        = $request->Kode_Dinas;
        $setting->save();

        if ($setting) {
          if (empty($request->id_setting)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Data Setting',
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Data Setting',
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
}
