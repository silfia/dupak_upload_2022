<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PakMasterKegiatan;
use App\Models\Penilaian;
use App\Models\DetailKegiatanPegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Models\BebanKerja;
use App\Models\MasaPengajuan;
use App\Models\Pegawai;
use App\Models\Users;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use Auth, Redirect, Validator, DB;

class KegiatanPegawaiController extends Controller
{
    public function mainNew(Request $request)
    {
      $this->data['mn_active'] = "kegiatanHarian";
      $this->data['title'] = 'Daftar Kegiatan Harian';
      $this->data['smallTitle'] = "";
      if (Auth::getUser()->level_user == '5') {
        $this->data['dtPegawai'] = Users::find(Auth::id());
        $this->data['dtJabatan'] = DataMaster::where('tipe_profesi_id', $this->data['dtPegawai']->pegawai->tipe_profesi_id)
                                      ->where('is_golongan','N')
                                      ->where('bl_state','A')
                                      ->orderBy('urutan','ASC')->get();
        return view('kegiatanPegawai.mainNew')->with('data', $this->data);
      }
    }

    public function main(Request $request)
    {
      $this->data['mn_active'] = "kegiatanHarian";
      $this->data['title'] = 'Daftar Kegiatan Harian';
      $this->data['smallTitle'] = "";
      if (Auth::getUser()->level_user == '5') {
        $this->data['dtPegawai'] = Users::find(Auth::id());
        $this->data['dtJabatan'] = DataMaster::where('tipe_profesi_id', $this->data['dtPegawai']->pegawai->tipe_profesi_id)
                                      ->where('is_golongan','N')
                                      ->where('bl_state','A')
                                      ->orderBy('urutan','ASC')->get();
        return view('kegiatanPegawai.main')->with('data', $this->data);
      }
    }

    public function loadHarian(Request $request)
    {
      if ($request->blnLoad == null) {
        $tahun = date('Y'); //Mengambil tahun saat ini
        $bulan = date('m'); //Mengambil bulan saat ini
      }else{
        $blnLoad = explode('-', $request->blnLoad) ;
        $tahun = $blnLoad[1];
        $bulan = $blnLoad[0];
      }
      $data['jumlahTanggal'] = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
      $data['bulan'] = $bulan;
      $data['tahun'] = $tahun;

      $user = Users::find(Auth::id());
      $butir = [];
      $no = 0;
      $unsur = PakMasterKegiatan::select('master_type.*')
                                  ->join('master_type','master_type.id_master_type','pak_master_kegiatan.unsur_pak_id')
                                  ->where('master_type.modul','SatuanUnsur')
                                  ->where('pak_master_kegiatan.formasi_id', $user->pegawai->tipe_profesi_id)
                                  ->where('pak_master_kegiatan.jenis','Butir')
                                  ->where('pak_master_kegiatan.level','1')
                                  ->where('pak_master_kegiatan.bl_state', 'A')
                                  ->orderBy('master_type.urutan','ASC')->distinct()->get();
      if (count($unsur) > 0) {
        foreach ($unsur as $uns) {
          $butir[$no] = $uns;
          $butir1 = [];
          $lv1 = PakMasterKegiatan::where('formasi_id', $user->pegawai->tipe_profesi_id)
                                    ->where('unsur_pak_id', $uns->id_master_type)
                                    ->where('jenis', 'Butir')
                                    ->where('level', '1')
                                    ->where('bl_state', 'A')
                                    ->get();
          if (count($lv1) > 0) {
            $noLv1 = 0;
            foreach ($lv1 as $key1) {
              $butir2 = [];
              if ($key1->is_title == '1') {
                $butir1[$noLv1] = $key1;
                $lv2 = PakMasterKegiatan::where('formasi_id', $user->pegawai->tipe_profesi_id)
                                          ->where('unsur_pak_id', $uns->id_master_type)
                                          ->where('jenis', 'Butir')
                                          ->where('level', '2')
                                          ->where('bl_state', 'A')
                                          ->where('parent_id', $key1->id_pak_master)
                                          ->get();
                if (count($lv2) > 0) {
                  $noLv2 = 0;
                  foreach ($lv2 as $key2) {
                    $butir3 = [];
                    if ($key2->is_title == '1') {
                      $butir2[$noLv2] = $key2;
                      $lv3 = PakMasterKegiatan::where('formasi_id', $user->pegawai->tipe_profesi_id)
                                                ->where('unsur_pak_id', $uns->id_master_type)
                                                ->where('jenis', 'Butir')
                                                ->where('level', '3')
                                                ->where('bl_state', 'A')
                                                ->where('parent_id', $key2->id_pak_master)
                                                ->get();
                      if (count($lv3) > 0) {
                        $noLv3 = 0;
                        foreach ($lv3 as $key3) {
                          $butir4 = [];
                          if ($key3->is_title == '1') {
                            $butir3[$noLv3] = $key3;
                            $lv4 = PakMasterKegiatan::where('formasi_id', $user->pegawai->tipe_profesi_id)
                                                      ->where('unsur_pak_id', $uns->id_master_type)
                                                      ->where('jenis', 'Butir')
                                                      ->where('level', '4')
                                                      ->where('bl_state', 'A')
                                                      ->where('parent_id', $key3->id_pak_master)
                                                      ->get();
                            if (count($lv4) > 0) {
                              $noLv4 = 0;
                              foreach ($lv4 as $key4) {
                                $butir5 = [];
                                if ($key4->is_title == '1') {
                                  $butir4[$noLv4] = $key4;
                                  $lv5 = PakMasterKegiatan::where('formasi_id', $user->pegawai->tipe_profesi_id)
                                                            ->where('unsur_pak_id', $uns->id_master_type)
                                                            ->where('jenis', 'Butir')
                                                            ->where('level', '5')
                                                            ->where('bl_state', 'A')
                                                            ->where('parent_id', $key4->id_pak_master)
                                                            ->get();
                                  if (count($lv5) > 0) {
                                    $noLv5 = 0;
                                    foreach ($lv5 as $key5) {
                                      $butir6 = [];
                                      if ($key5->is_title == '1') {
                                        $butir5[$noLv5] = $key5;
                                        $lv6 = PakMasterKegiatan::where('formasi_id', $user->pegawai->tipe_profesi_id)
                                                                  ->where('unsur_pak_id', $uns->id_master_type)
                                                                  ->where('jenis', 'Butir')
                                                                  ->where('level', '6')
                                                                  ->where('bl_state', 'A')
                                                                  ->where('parent_id', $key5->id_pak_master)
                                                                  ->get();
                                        if (count($lv6) > 0) {
                                          $noLv6 = 0;
                                          foreach ($lv6 as $key6) {
                                            $butir7 = [];
                                            if ($key6->is_title == '1') {
                                              $butir6[$noLv6] = $key6;
                                              $butir6[$noLv6]['statusRow'] = 'Parent';
                                              $butir6[$noLv6]['row'] = $butir7;
                                              $noLv6++;
                                            }else{
                                              if ($key6->jabatan_id == $request->idMaster) {
                                                $butir6[$noLv6] = $key6;
                                                $butir6[$noLv6]['statusRow'] = 'Child';
                                                $butir6[$noLv6]['row'] = $butir7;
                                                $butir6[$noLv6]['nama_jabatan'] = $key6->jabatan->nama;
                                                $noLv6++;
                                              }
                                            }
                                          }
                                        }
                                        $butir5[$noLv5]['statusRow'] = 'Parent';
                                        $butir5[$noLv5]['row'] = $butir6;
                                        $noLv5++;
                                      }else{
                                        if ($key5->jabatan_id == $request->idMaster) {
                                          $butir5[$noLv5] = $key5;
                                          $butir5[$noLv5]['statusRow'] = 'Child';
                                          $butir5[$noLv5]['row'] = $butir6;
                                          $butir5[$noLv5]['nama_jabatan'] = $key5->jabatan->nama;
                                          $noLv5++;
                                        }
                                      }
                                    }
                                  }
                                  $butir4[$noLv4]['statusRow'] = 'Parent';
                                  $butir4[$noLv4]['row'] = $butir5;
                                  $noLv4++;
                                }else{
                                  if ($key4->jabatan_id == $request->idMaster) {
                                    $butir4[$noLv4] = $key4;
                                    $butir4[$noLv4]['statusRow'] = 'Child';
                                    $butir4[$noLv4]['row'] = $butir5;
                                    $butir4[$noLv4]['nama_jabatan'] = $key4->jabatan->nama;
                                    $noLv4++;
                                  }
                                }
                              }
                            }
                            $butir3[$noLv3]['statusRow'] = 'Parent';
                            $butir3[$noLv3]['row'] = $butir4;
                            $noLv3++;
                          }else{
                            if ($key3->jabatan_id == $request->idMaster) {
                              $butir3[$noLv3] = $key3;
                              $butir3[$noLv3]['statusRow'] = 'Child';
                              $butir3[$noLv3]['row'] = $butir4;
                              $butir3[$noLv3]['nama_jabatan'] = $key3->jabatan->nama;
                              $noLv3++;
                            }
                          }
                        }
                      }
                      $butir2[$noLv2]['statusRow'] = 'Parent';
                      $butir2[$noLv2]['row'] = $butir3;
                      $noLv2++;
                    }else{
                      if ($key2->jabatan_id == $request->idMaster) {
                        $butir2[$noLv2] = $key2;
                        $butir2[$noLv2]['statusRow'] = 'Child';
                        $butir2[$noLv2]['row'] = $butir3;
                        $butir2[$noLv2]['nama_jabatan'] = $key2->jabatan->nama;
                        $noLv2++;
                      }
                    }
                  }
                }
                $butir1[$noLv1]['statusRow'] = 'Parent';
                $butir1[$noLv1]['row'] = $butir2;
                $noLv1++;
              }else{
                if ($key1->jabatan_id == $request->idMaster) {
                  $butir1[$noLv1] = $key1;
                  $butir1[$noLv1]['statusRow'] = 'Child';
                  $butir1[$noLv1]['row'] = $butir2;
                  $butir1[$noLv1]['nama_jabatan'] = $key1->jabatan->nama;
                  $noLv1++;
                }
              }
            }
          }
          $butir[$no]['statusRow'] = 'Parent';
          $butir[$no]['row'] = $butir1;
          $no++;
        }
        $data['butir'] = $butir;
        $return = ['status'=>'success','code'=>200, 'message'=>'Unsur Kegiatan Ditemukan !!', 'row'=>$data];
      }else{
        $data['butir'] = $butir;
        $return = ['status'=>'error','code'=>404, 'message'=>'Unsur Kegiatan Tidak Ditemukan !!','row'=>$data];
      }
      return response()->json($return);
    }

    public function getNilaiHarian(Request $request)
    {
      $blnLoad = explode('-', $request->bulan) ;
      $tahun = $blnLoad[1];
      $bulan = $blnLoad[0];
      $jumlahTanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

      $data = [];
      $jml = 0;
      $jmlReal = 0;
      $jmlAk = 0;
      $pegawai = Pegawai::where('user_id', Auth::getUser()->id)->first();
      for ($i=0; $i < $jumlahTanggal; $i++) {
        $dy = $i+1;
        $tglFull = $tahun.'-'.$bulan.'-'.$dy;
        $cekNilai = Penilaian::where('jenis','Harian')
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->where('tanggal', $tglFull)
                              ->where('user_id', Auth::getUser()->id)
                              ->first();
        if (!empty($cekNilai)) {
          if ($pegawai->status_pegawai == 'PNS') {
            $hasil = $cekNilai->jumlah_px_pemohon;
          }else{
            $hasil = $cekNilai->jumlah_px_real;
          }
          $idPenilaian = $cekNilai->id_penilaian;
          $jmlReal = $jml + $cekNilai->jumlah_px_real;
          $jml = $jml + $cekNilai->jumlah_px_pemohon;
          $jmlAk = $jmlAk + $cekNilai->poin_pemohon;
        }else{
          $hasil = '-';
          $idPenilaian = '';
        }

        $data[$i] = [
          'idjabatan'   => $request->idMaster,
          'idpak'       => $request->idPakMaster,
          'tgl'         => $i+1,
          'bln'         => $bulan,
          'thn'         => $tahun,
          'hasil'       => $hasil,
          'idPenilaian' => $idPenilaian,
        ];
      }

      if ($pegawai->status_pegawai == 'Non-PNS') {
        $jml_keg = $jmlReal;
        $jmlAk = '-';
      }else{
        $jml_keg = $jml;
        $jmlAk = number_format($jmlAk,3,',','.');
      }

      $dtJml['idJabatan'] = $request->idMaster;
      $dtJml['idpak'] = $request->idPakMaster;
      $dtJml['jml'] = $jml_keg;
      $dtJml['jmlAk'] = $jmlAk;

      $return = ['status'=>'success','code'=>200,'row'=>$data, 'dtJumlah'=>$dtJml];
      return response()->json($return);
    }

    public function formJumlahDetail(Request $request)
    {
      $data['idjabatan'] = $request->idMaster;
      $data['pakMaster'] = PakMasterKegiatan::find($request->idPakMaster);
      $infoButir = [];
      $tempId = $request->idPakMaster;
      $tempJml = $data['pakMaster']->level - 1;
      for ($i=$tempJml; $i >= 0 ; $i--) {
        $info = PakMasterKegiatan::find($tempId);
        $infoButir[$i]['nama'] = $info->butir_kegiatan;
        $tempId = $info->parent_id;
      }
      $data['infoButir'] = $infoButir;
      $data['tgl'] = $request->tgl;
      $data['pegawai'] = Pegawai::where('user_id', Auth::getUser()->id)->first();
      // $content = view('dashboard.pegawai.jumlahDetail', $data)->render();
      $content = view('kegiatanPegawai.jumlahDetail', $data)->render();
      return ['status'=>'success', 'content'=>$content];
    }

    public function formAddDetail(Request $request)
    {
      $data['idjabatan'] = $request->idjabatan;
      $data['idPakMaster'] = $request->idPakMaster;
      $data['dtPakMaster'] = PakMasterKegiatan::find($request->idPakMaster);
      $data['tgl'] = $request->tgl;
      $data['jumlah'] = $request->Jumlah;
      $data['status_pegawai'] = $request->status_pegawai;
      $data['jumlah_dupak'] = (!empty($request->Jumlah_Dupak)) ? $request->Jumlah_Dupak : 0;
      $data['pasiens'] = (!empty($request->id)) ? DetailKegiatanPegawai::find($request->id) : "";
      $content = view('kegiatanPegawai.formDetailPasien', $data)->render();
      return ['status'=>'success', 'content'=>$content];
    }

    public function addDetail(Request $request)
    {
      $rules = array(
        'idjabatan'     => 'required',
        'idPakMaster'   => 'required',
        'jenis_kegiatan' => 'required',
        'tgl'           => 'required',
        'jumlah'        => 'required',
        'status_pegawai'=> 'required',
        'jumlah_dupak'  => 'required',
      );
      if($request->jenis_kegiatan == 'Pasien' || $request->jenis_kegiatan == 'Jenazah') {
        // $rules['nik']           = 'required';
        // $rules['nama']          = 'required';
        $rules['inisial']       = 'required';
        $rules['no_rm']         = 'required';
        // $rules['no_bpjs']       = 'required';
        $rules['Jenis_Kelamin'] = 'required';
        $rules['umur']          = 'required';
        $rules['alamat']        = 'required';
      }elseif ($request->jenis_kegiatan == 'Sertifikat') {
        $rules['tglPelaksanaan']    = 'required';
        $rules['tglAkhirPelaksanaan']    = 'required';
        $rules['waktuPelaksanaan']  = 'required';
        $rules['penyelenggara']     = 'required';
        $rules['sertifikat']        = 'required';
        $rules['noSertifikat']      = 'required';
      }elseif ($request->jenis_kegiatan == 'Ijazah') {
        $rules['tglLulus']      = 'required';
        $rules['universitas']   = 'required';
        $rules['jurusan']       = 'required';
        $rules['noIjazah']      = 'required';
      }elseif ($request->jenis_kegiatan == 'Laporan' || $request->jenis_kegiatan == 'Kasus' || $request->jenis_kegiatan == 'Kali' || $request->jenis_kegiatan == 'Buku' || $request->jenis_kegiatan == 'Karya' || $request->jenis_kegiatan == 'Makalah' || $request->jenis_kegiatan == 'Naskah') {
        // $rules['tglBuat']       = 'required';
        $rules['judul']         = 'required';
        $rules['keterangan']    = 'required';
      }elseif ($request->jenis_kegiatan == 'Jam' || $request->jenis_kegiatan == 'Jam Pelajaran') {
        $rules['nama']          = 'required';
        $rules['foto']          = 'required';
        $rules['keterangan']    = 'required';
      }
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator  = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $pegawai = Users::join('pegawai','pegawai.user_id','users.id')->where('users.id', Auth::id())->first();
        $tingkatJenjang = '';
        if ($pegawai->jabatan_id == $request->idjabatan) {
          $tingkatJenjang = 'Sama';
        }else{
          $cekUrutJabatanPeg = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                          ->where('is_golongan', 'N')
                                          ->where('bl_state', 'A')
                                          ->where('id_master', $pegawai->jabatan_id)->first();
          $cekUrutJabIn = DataMaster::find($request->idjabatan);
          if ($cekUrutJabatanPeg->urutan > $cekUrutJabIn->urutan) {
            $jmlselisih = $cekUrutJabatanPeg->urutan - $cekUrutJabIn->urutan;
            if ($jmlselisih == 1) {
              $tingkatJenjang = '1 tingkat dibawah';
            }elseif ($jmlselisih == 2) {
              $tingkatJenjang = '2 tingkat dibawah';
            }else{
              $tingkatJenjang = 'lebih dari 2 tingkat dibawah';
            }
            // if ($jmlselisih >= 2) {
            //   $cekJabKr1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
            //                         ->where('is_golongan', 'N')
            //                         ->where('bl_state', 'A')
            //                         ->where('urutan', ($cekUrutJabatanPeg->urutan - 2))->first();
            //     if ($request->idjabatan == $cekJabKr1->id_master) {
            //       if ($pegawai->tipe_profesi_id = 10) {
            //         $persenPoint = 1;
            //       }elseif ($pegawai->tipe_profesi_id ==34) {
            //         $persenPoint = 1;
            //       }elseif ($pegawai->tipe_profesi_id == 6) {
            //         $persenPoint = 0.8;
            //       }elseif ($pegawai->tipe_profesi_id == 2) {
            //         $persenPoint = 1;
            //       }else{
            //         $persenPoint = 1;
            //       }
            //     }else{
            //       $persenPoint = 0;
            //     }
            // }else {
            //   $cekJabKr1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
            //                         ->where('is_golongan', 'N')
            //                         ->where('bl_state', 'A')
            //                         ->where('urutan', ($cekUrutJabatanPeg->urutan - 1))->first();
            //     if ($request->idjabatan == $cekJabKr1->id_master) {
            //       if ($pegawai->tipe_profesi_id = 10) {
            //         $persenPoint = 1;
            //       }elseif ($pegawai->tipe_profesi_id ==34) {
            //         $persenPoint = 1;
            //       }elseif ($pegawai->tipe_profesi_id == 6) {
            //         $persenPoint = 1;
            //       }elseif ($pegawai->tipe_profesi_id == 2) {
            //         $persenPoint = 1;
            //       }else{
            //         $persenPoint = 1;
            //       }
            //     }else{
            //       $persenPoint = 0;
            //     }
            // }                        
          }else{
            $jmlselisih = $cekUrutJabIn->urutan - $cekUrutJabatanPeg->urutan;
            if ($jmlselisih == 1) {
              $tingkatJenjang = '1 tingkat diatas';
            }elseif ($jmlselisih == 2) {
              $tingkatJenjang = '2 tingkat diatas';
            }else{
              $tingkatJenjang = 'lebih dari 2 tingkat diatas';
            }
            // if ($jmlselisih >=2) {
            //   $cekJabLb1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
            //                         ->where('is_golongan', 'N')
            //                         ->where('bl_state', 'A')
            //                         ->where('urutan', ($cekUrutJabatanPeg->urutan + 2))->first();
            //   if ($request->idjabatan == $cekJabLb1->id_master) {
            //     if ($pegawai->tipe_profesi_id = 10) {
            //         $persenPoint = 0.4;
            //     }elseif ($pegawai->tipe_profesi_id ==34) {
            //       $persenPoint = 0.4;
            //     }else{
            //       $persenPoint = 1;
            //     }                
            //   }else{
            //     $persenPoint = 0;
            //   }
            // } else {
            //   $cekJabLb1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
            //                           ->where('is_golongan', 'N')
            //                           ->where('bl_state', 'A')
            //                           ->where('urutan', ($cekUrutJabatanPeg->urutan + 1))->first();
            //   if ($request->idjabatan == $cekJabLb1->id_master) {
            //     if ($pegawai->tipe_profesi_id = 10) {
            //       $persenPoint = 0.8;
            //     }elseif ($pegawai->tipe_profesi_id ==34) {
            //       $persenPoint = 0.8;
            //     }elseif ($pegawai->tipe_profesi_id == 6) {
            //       $persenPoint = 0.8;
            //     }elseif ($pegawai->tipe_profesi_id == 2) {
            //       $persenPoint = 0.8;
            //     }else{
            //       $persenPoint = 0.8;
            //     }
            //     $persenPoint = 0.8;
            //   }else{
            //     $persenPoint = 0;
            //   }
            // }           
          }
        }
        switch ($tingkatJenjang) {
          case 'Sama':
            $persenPoint = 1;
            break;
          case '1 tingkat dibawah':
            switch ($pegawai->tipe_profesi_id) {
              case '1': // Profesi Dokter
                $persenPoint = 1; break;
              case '2': // Profesi Dokter Gigi
                $persenPoint = 1; break;
              case '3': // Profesi Bidan
                $persenPoint = 1; break;
              case '4': // Profesi Perawat
                $persenPoint = 1; break;
              default:
                $persenPoint = 0; break;
                break;
            }
            break;
          case '2 tingkat dibawah':
            switch ($pegawai->tipe_profesi_id) {
              case '1': // Profesi Dokter
                $persenPoint = 1; break;
              case '2': // Profesi Dokter Gigi
                $persenPoint = 1; break;
              case '3': // Profesi Bidan
                $persenPoint = 1; break;
              case '4': // Profesi Perawat
                $persenPoint = 1; break;
              default:
                $persenPoint = 0; break;
                break;
            }
            break;
          case 'lebih dari 2 tingkat dibawah':
            $persenPoint = 0;
            break;
          case '1 tingkat diatas':
            switch ($pegawai->tipe_profesi_id) {
              case '1': // Profesi Dokter
                $persenPoint = 1; break;
              case '2': // Profesi Dokter Gigi
                $persenPoint = 1; break;
              case '3': // Profesi Bidan
                $persenPoint = 1; break;
              case '4': // Profesi Perawat
                $persenPoint = 1; break;
              default:
                $persenPoint = 0; break;
                break;
            }
            break;
          case '2 tingkat diatas':
            switch ($pegawai->tipe_profesi_id) {
              case '1': // Profesi Dokter
                $persenPoint = 1; break;
              case '2': // Profesi Dokter Gigi
                $persenPoint = 1; break;
              case '3': // Profesi Bidan
                $persenPoint = 1; break;
              case '4': // Profesi Perawat
                $persenPoint = 1; break;
              default:
                $persenPoint = 0; break;
                break;
            }
            break;
          case 'lebih dari 2 tingkat diatas':
            $persenPoint = 0;
            break;
          default:
            $persenPoint = 0;
            break;
        }
        $cekPoint = PakMasterKegiatan::find($request->idPakMaster);
        if (!empty($cekPoint)) {
          $point = (($request->jumlah_dupak / $cekPoint->jum_min) * $cekPoint->points)*$persenPoint;
        }else{
          $point = 0;
        }
        $tglButir = date('Y-m').'-'.$request->tgl;
        $penilaian                      = new Penilaian;
        $penilaian->jenis               = 'Harian';
        $penilaian->master_kegiatan_id  = $request->idPakMaster;
        $penilaian->jumlah_px_real      = $request->jumlah;
        $penilaian->jumlah_px_pemohon   = $request->jumlah_dupak;
        if ($request->status_pegawai == 'PNS') {
          $penilaian->poin_pemohon      = $point;
        }else{
          $penilaian->poin_pemohon      = 0;
        }
        $penilaian->tanggal             = date('Y-m-d', strtotime($tglButir));
        $penilaian->label_tanggal       = $request->tgl;
        $penilaian->label_bulan         = date('m');
        $penilaian->label_tahun         = date('Y');
        $penilaian->user_id             = Auth::getUser()->id;
        $penilaian->save();

        $bebanKerja = new BebanKerja;
        $bebanKerja->penilaian_id = $penilaian->id_penilaian;
        $bebanKerja->satuan_kerja_id = $pegawai->satuan_kerja_id;
        $bebanKerja->jenis = 'Harian';
        $bebanKerja->master_kegiatan_id = $penilaian->master_kegiatan_id;
        $bebanKerja->jumlah_px_real = $request->jumlah;
        $bebanKerja->tanggal = $penilaian->tanggal;
        $bebanKerja->label_tanggal = $penilaian->label_tanggal;
        $bebanKerja->label_bulan = $penilaian->label_bulan;
        $bebanKerja->label_tahun = $penilaian->label_tahun;
        $bebanKerja->user_id = Auth::id();
        $bebanKerja->save();

        if ($penilaian) {
          for ($i=0; $i < $request->jumlah_dupak; $i++) {
            $detail                 = new DetailKegiatanPegawai;
            $detail->penilaian_id   = $penilaian->id_penilaian;
            $detail->tanggal        = $penilaian->tanggal;
            $detail->jenis_kegiatan = $request->jenis_kegiatan;
            if ($request->jenis_kegiatan == 'Pasien' || $request->jenis_kegiatan == 'Jenazah') {
              $detail->nik            = $request->nik[$i];
              // $detail->nama_pasien    = $request->nama[$i];
              $detail->inisial_pasien = $request->inisial[$i];
              $detail->no_rm          = $request->no_rm[$i];
              $detail->no_bpjs        = $request->no_bpjs[$i];
              $detail->jenis_kelamin  = $request->Jenis_Kelamin[$i];
              $detail->umur           = $request->umur[$i];
              $detail->alamat         = $request->alamat[$i];
            }elseif ($request->jenis_kegiatan == 'Sertifikat') {
              $detail->tgl_kegiatan   = date('Y-m-d', strtotime($request->tglPelaksanaan[$i]));
              $detail->tgl_akhir_kegiatan = date('Y-m-d', strtotime($request->tglAkhirPelaksanaan[$i]));
              $detail->waktu_kegiatan = $request->waktuPelaksanaan[$i];
              $detail->penyelenggara  = $request->penyelenggara[$i];
              $detail->nama_pasien    = $request->sertifikat[$i];
              $detail->nomor          = $request->noSertifikat[$i];
            }elseif($request->jenis_kegiatan == 'Ijazah'){
              $detail->tgl_kegiatan   = date('Y-m-d', strtotime($request->tglLulus[$i]));
              $detail->penyelenggara  = $request->universitas[$i];
              $detail->nama_pasien    = $request->jurusan[$i];
              $detail->nomor          = $request->noIjazah[$i];
            }elseif ($request->jenis_kegiatan == 'Laporan' || $request->jenis_kegiatan == 'Kasus' || $request->jenis_kegiatan == 'Kali' || $request->jenis_kegiatan == 'Buku' || $request->jenis_kegiatan == 'Karya' || $request->jenis_kegiatan == 'Makalah' || $request->jenis_kegiatan == 'Naskah') {
              // $detail->tgl_kegiatan   = date('Y-m-d', strtotime($request->tglBuat[$i]));
              $detail->tgl_kegiatan   = date('Y-m-d');
              $detail->nama_pasien    = $request->judul[$i];
              $detail->keterangan     = $request->keterangan[$i];
            }elseif ($request->jenis_kegiatan == 'Jam' || $request->jenis_kegiatan == 'Jam Pelajaran') {
              $detail->nama_pasien    = $request->nama[$i];
              if (!empty($request->foto[$i])) {
                $ukuranFile = filesize($request->foto[$i]);
                $ext_foto = $request->foto[$i]->getClientOriginalExtension();
                $filename = date('Ymd')."/".date('Ymd-His')."_".$pegawai->username.".".$ext_foto;
                $temp_foto  = 'upload/buktiFisik/'.date('Ymd');
                $proses   = $request->foto[$i]->move($temp_foto, $filename);
                $detail->gambar = $filename;
              }
              $detail->keterangan     = $request->keterangan[$i];
            }
            $detail->creator_id     = Auth::getUser()->id;
            $detail->ac_conn        = $_SERVER['REMOTE_ADDR'];
            $detail->save();
          }

          $data['idjabatan']    = $request->idjabatan;
          $data['idPakMaster']  = $request->idPakMaster;
          $data['tgl']          = $request->tgl;
          if ($request->status_pegawai == 'PNS') {
            $data['jumlah']       = $request->jumlah_dupak;
          }else{
            $data['jumlah']       = $request->jumlah;
          }
          $data['point']        = $point;
          $data['penilaian']    = $penilaian;
          $data['jumlahTanggal'] = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

          if ($detail) {
            if (empty($request->id_penilaian)) {
              $requestActivity = [
                'action_type'   => 'Insert',
                'message'       => 'Create Penilaian Kegiatan Pegawai tanggal '.date('d-m-Y', strtotime($penilaian->tanggal)),
              ];
            }else{
              $requestActivity = [
                'action_type'   => 'Update',
                'message'       => 'Update Penilaian Kegiatan Pegawai tanggal '.date('d-m-Y', strtotime($penilaian->tanggal)),
              ];
            }
            $saveActivity = Activitys::add($requestActivity);

            $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!', 'data'=>$data];
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
          }
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Penilaian Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function addKegiatanNonPns(Request $request)
    {
      $rules = array(
        'idjabatan'       => 'required',
        'idPakMaster'     => 'required',
        'tgl'             => 'required',
        'status_pegawai'  => 'required',
        'Jumlah'          => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator  = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $pegawai = Users::join('pegawai','pegawai.user_id','users.id')->where('users.id', Auth::id())->first();
        $tglButir = date('Y-m').'-'.$request->tgl;
        $penilaian                      = new Penilaian;
        $penilaian->jenis               = 'Harian';
        $penilaian->master_kegiatan_id  = $request->idPakMaster;
        $penilaian->jumlah_px_real      = $request->jumlah;
        $penilaian->jumlah_px_pemohon   = 0;
        $penilaian->poin_pemohon        = 0;
        $penilaian->tanggal             = date('Y-m-d', strtotime($tglButir));
        $penilaian->label_tanggal       = $request->tgl;
        $penilaian->label_bulan         = date('m');
        $penilaian->label_tahun         = date('Y');
        $penilaian->user_id             = Auth::getUser()->id;
        $penilaian->save();

        $bebanKerja = new BebanKerja;
        $bebanKerja->penilaian_id       = $penilaian->id_penilaian;
        $bebanKerja->satuan_kerja_id    = $pegawai->satuan_kerja_id;
        $bebanKerja->jenis              = 'Harian';
        $bebanKerja->master_kegiatan_id = $penilaian->master_kegiatan_id;
        $bebanKerja->jumlah_px_real     = $request->jumlah;
        $bebanKerja->tanggal            = $penilaian->tanggal;
        $bebanKerja->label_tanggal      = $penilaian->label_tanggal;
        $bebanKerja->label_bulan        = $penilaian->label_bulan;
        $bebanKerja->label_tahun        = $penilaian->label_tahun;
        $bebanKerja->user_id            = Auth::id();
        $bebanKerja->save();

        if ($penilaian) {
          $data['idjabatan']    = $request->idjabatan;
          $data['idPakMaster']  = $request->idPakMaster;
          $data['tgl']          = $request->tgl;
          $data['jumlah']       = $request->jumlah;
          $data['point']        = 0;
          $data['penilaian']    = $penilaian;
          $data['jumlahTanggal'] = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

          if (empty($request->id_penilaian)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Penilaian Kegiatan Pegawai tanggal '.date('d-m-Y', strtotime($penilaian->tanggal)),
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Penilaian Kegiatan Pegawai tanggal '.date('d-m-Y', strtotime($penilaian->tanggal)),
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!', 'data'=>$data];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function viewDetail(Request $request)
    {
      $data['penilaian'] = Penilaian::find($request->idPenilaian);
      $data['pakMaster'] = PakMasterKegiatan::find($data['penilaian']->master_kegiatan_id);
      $data['pegawai'] = Pegawai::where('user_id', $data['penilaian']->user_id)->first();
      $data['detailKegiatan'] = DetailKegiatanPegawai::where('penilaian_id', $request->idPenilaian)->get();
      $content = view('kegiatanPegawai.viewDetail', $data)->render();
      return ['status'=>'success', 'content'=>$content];
    }

    // public function ViewDupak(Request $request)
    // {
    //
    //   // return $data['pegawai'];
    //   $this->data['mn_active'] = "kegiatanHarian";
    //   $this->data['title'] = 'Daftar Data Pengajuan';
    //   $this->data['smallTitle'] = "";
    //   $this->data['pengajuan'] = (!empty($request->id)) ? MasaPengajuan::where('user_id', Auth::getUser()->id): "";
    //   $this->data['pegawai'] = Pegawai::join('users','users.id','pegawai.user_id')
    //                                   ->where('users.id', Auth::getUser()->id)->first();
    //   if (Auth::getUser()->level_user == '5') {
    //     return view('dashboard.pegawai.ViewDupak')->with('data', $this->data);
    //   }
    // }

    public function saveDataPj(Request $request)
    {
      $rules = array(
        'tgl_awal'   => 'required',
        'tgl_akhir'    => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator  = Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {

        $dtPengajuan                       = (!empty($request->id_Bpengajuan)) ? MasaPengajuan::where('user_id', Auth::getUser()->id) : new MasaPengajuan;
        $dtPengajuan->tgl_awal             = $request->tgl_awal;
        $dtPengajuan->tgl_akhir            = $request->tgl_akhir;
        $dtPengajuan->keterangan           = 'null';
        $dtPengajuan->bl_state             = 'A';
        $dtPengajuan->save();
        if ($dtPengajuan) {
          if (empty($request->id_Bpengajuan)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Informasi Pengajuan',
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Informasi Pengajuan',
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

}
