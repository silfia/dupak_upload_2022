<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Pegawai;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Models\DataMaster;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB, Hash;

class ProfileController extends Controller
{
	public function main(Request $request)
	{
		$this->data['mn_active'] = "";
		$this->data['submn_active'] = "";
		$this->data['title'] = 'Profile';
		$this->data['smallTitle'] = "";
		$data['pegawai'] = (Auth::getUser()->level_user != 1) ? Pegawai::where('user_id', Auth::getUser()->id)->first() : '';

		return view('users.profile.main', $data)->with('data', $this->data);
	}

	public function form(Request $request)
	{	
		$data['pegawai'] = (Auth::getUser()->level_user != 1) ? Pegawai::where('user_id', Auth::getUser()->id)->first() : '';
		$data['profesi'] = MasterType::where('modul','StatusProfesi')->where('bl_state','A')->get();
		$data['satKers'] = SatuanKerja::where('bl_state','A')->get();
		$data['golongan'] = DataMaster::where('is_golongan','Y')->where('bl_state','A')->get();
		
		$content = view('users.profile.form', $data)->render();
		return ['status' => 'success', 'content' => $content];
	}

	public function save(Request $request)
	{
		$rules = array(
			'Email'			=> 'required',
			'Nama'			=> 'required',
			'gender'	=> 'required',
		);
		if (Auth::getUser()->level_user != 1) {
			$rules['Tempat_Lahir']		= 'required';
			$rules['Tanggal_Lahir']		= 'required';
			$rules['Profesi']			= 'required';
			$rules['Satuan_Kerja']		= 'required';
			$rules['status_pegawai']	= 'required';
			$rules['NIP']				= 'required';
			$rules['Pendidikan']		= 'required';
			$rules['Tahun_Lulus']		= 'required';
			if ($request->status_pegawai == 'PNS') {
				$rules['No_Karpeng']	= 'required';
				$rules['Golongan']		= 'required';
				$rules['Golongan_TMT']	= 'required';
				$rules['Jabatan']		= 'required';
				$rules['Jabatan_TMT']	= 'required';
			}
		}else{
			$rules['Alamat']	= 'required';
			$rules['Telepon']	= 'required';
		}
		$messages = array(
			'required'	=> 'Kolom Harus Diisi',
		);
		$validator = Validator::make($request->all(), $rules, $messages);
		if (!$validator->fails()) {
			$user 				= Users::find(Auth::id());
			$user->username 	= (Auth::getUser()->level_user != 1) ? str_replace(' ', '', $request->NIP) : $request->Email;
			$user->email 		= $request->Email;
			$user->name 		= $request->Nama;
			$user->gender 		= $request->gender;
			if (Auth::getUser()->level_user == 1) {
				$user->address 	= $request->Alamat;
				$user->phone 	= $request->Telepon;
			}
			$user->save();

			if ($user) {
				if (Auth::getUser()->level_user != 1) {
					$pegawai 						= Pegawai::where('user_id', $user->id)->first();
					$pegawai->nama 					= $request->Nama;
					$pegawai->no_nip 				= $request->NIP;
					$pegawai->no_karpeng 			= ($request->status_pegawai == 'PNS') ? $request->No_Karpeng : null;
					$pegawai->tempat_lahir 			= $request->Tempat_Lahir;
					$pegawai->tanggal_lahir 		= date('Y-m-d', strtotime(($request->Tanggal_Lahir)));
					$pegawai->jenis_kelamin 		= $request->gender;
					$pegawai->golongan_id 			= ($request->status_pegawai == 'PNS') ? $request->Golongan : null;
					$pegawai->golongan_tmt 			= ($request->status_pegawai == 'PNS') ? date('Y-m-d', strtotime($request->Golongan_TMT)) : null;
					$pegawai->jabatan_id 			= ($request->status_pegawai == 'PNS') ? $request->Jabatan : null;
					$pegawai->jabatan_tmt 			= ($request->status_pegawai == 'PNS') ? date('Y-m-d', strtotime($request->Jabatan_TMT)) : null;
					$pegawai->pendidikan_terakhir	= $request->Pendidikan;
					$pegawai->pendidikan_tahun 		= $request->Tahun_Lulus;
					$pegawai->satuan_kerja_id 		= $request->Satuan_Kerja;
					$pegawai->tipe_profesi_id 		= $request->Profesi;
					$pegawai->status_pegawai 		= $request->status_pegawai;
					$pegawai->save();

					$stPengguna = 'Pengawai';
				}else{
					$stPengguna = 'Admin';
				}

				$requestActivity = [
					'action_type'	=> 'Update',
					'message'		=> 'Update Profile Pengguna '.$stPengguna.' '.$request->Nama,
				];
				$saveActivity = Activitys::add($requestActivity);

				$return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
			}else{
				$return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
			}

			return response()->json($return);
		} else {
			return $validator->messages();
		}
	}

	public function formkey(Request $request)
	{
		$this->data['mn_active'] = "";
		$this->data['submn_active'] = "";
		$this->data['title'] = 'Ganti Password';
		$this->data['smallTitle'] = "";
		$data['pegawai'] = (Auth::getUser()->level_user != 1) ? Pegawai::where('user_id', Auth::getUser()->id)->first() : '';

		return view('users.profile.formPassword', $data)->with('data', $this->data);
	}

	public function saveNewPass(Request $request)
	{
		$user = Users::find(Auth::getUser()->id);

		if(Hash::check($request->password_lama, $user->password)==true){
			$user->password = bcrypt($request->password_baru);
			$user->save();

			if ($user) {
				$requestActivity = [
					'action_type'	=> 'Update',
					'message'		=> 'Update Password Pengguna '.$user->name,
				];
				$saveActivity = Activitys::add($requestActivity);

				$return = ['status'=>'success', 'code'=>'200', 'message'=>'Password Berhasil Diperbaharui !!'];
			}else{
				$return = ['status'=>'error', 'code'=>'500', 'message'=>'Password Gagal Diperbaharui !!'];
			}
		}else{
			$return = ['status'=>'error', 'code'=>'500', 'message'=>'Password Gagal Diperbaharui, Password lama Anda SALAH !!'];
		}
		return response()->json($return);
	}
}
