<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PenilaianPendidikan;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use Auth, Redirect, Validator, DB;

use File;

class KegiatanPendidikanController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "unsurPendidikan";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Unsur Pendidikan';
      $this->data['smallTitle'] = "";

      return view('dashboard.pendidikan.main')->with('data', $this->data);
    }
    public function loadDataUP(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('penilaian_pendidikan')
                  ->where('nama_penyedia','like','%'.$request->search.'%')
                  ->where('user_id', Auth::id())
                  ->where('bl_state','A')
                  ->orderBy('nama_penyedia','ASC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('penilaian_pendidikan')
                  ->where('user_id', Auth::id())
                  ->where('bl_state','A')
                  ->orderBy('nama_penyedia','ASC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function formUP(Request $request)
    {
      $data['dtMaster'] =
      (!empty($request->id_penilaian_pendidikan)) ?
      PenilaianPendidikan::find($request->id_penilaian_pendidikan) : "";
      // return $data;

      $content = view('dashboard.pendidikan.create', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveDataUP(Request $request)
    {
      // return $request->all();
      $rules = array(
        'jenis'    => 'required',
        'tingkat'  => 'required',
        'nama_penyedia' => 'required',   
        'keterangan' => 'required',
        'lama_pelaksaan' => 'required',
        'tahun_masuk' => 'required',
        'tahun_keluar' => 'required',
        'tanggal' => 'required'
      );

      // Kondisional Edit Bukti Fisik
      if( empty($request->id_penilaian_pendidikan)) {
        $rules['bukti_fisik'] = 'required';
      }

      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );

      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $dtMaster =  (!empty($request->id_penilaian_pendidikan)) ? PenilaianPendidikan::find($request->id_penilaian_pendidikan) : new PenilaianPendidikan;
        $dtMaster->jenis = $request->jenis;
        $dtMaster->tingkat = $request->tingkat;
        $dtMaster->nama_penyedia = $request->nama_penyedia;
        $dtMaster->keterangan = $request->keterangan;
        $dtMaster->lama_pelaksaan = $request->lama_pelaksaan;
        $dtMaster->tahun_masuk = $request->tahun_masuk;
        $dtMaster->tahun_keluar = $request->tahun_keluar;
        $dtMaster->tanggal = $request->tanggal;
        $dtMaster->label_tanggal = date('d', strtotime($request->tanggal));
        $dtMaster->label_bulan = date('m', strtotime($request->tanggal));
        $dtMaster->label_tahun = date('Y', strtotime($request->tanggal));
        if (empty($request->id_penilaian_pendidikan)) {
          $dtMaster->user_id = Auth::getUser()->id;
        }

        if (!empty($request->bukti_fisik)) {

          

          // Kondisional Saat Bukti Fisik Diganti
          if( ! empty($request->id_penilaian_pendidikan)) {
            File::delete('upload/'.$dtMaster->bukti_fisik.'');
          }

          $ukuranFile = filesize($request->bukti_fisik);
          if ($ukuranFile <= 500000) {
            $ext_foto  = $request->bukti_fisik->getClientOriginalExtension();
            $filename  = "pendidikan/" . date('Ymd-His') . "." . $ext_foto;
            $temp_foto  = 'upload/pendidikan/';
            $proses    = $request->bukti_fisik->move($temp_foto, $filename);
            $dtMaster->bukti_fisik = $filename;
          } else {
            $file = $_FILES['bukti_fisik']['name'];
            if (!empty($file)) {
              $direktori  = "upload/pendidikan/"; //tempat upload foto
              $name    = 'bukti_fisik'; //name pada input type file
              $namaBaru  = "pendidikan/" . date('Ymd-His') . ""; //name pada input type file
              $quality  = 50; //konversi kualitas gambar dalam satuan %
              $upload    = CompressFile::UploadCompress($namaBaru, $name, $direktori, $quality);
            }
            $ext_foto  = $request->bukti_fisik->getClientOriginalExtension();
            $new_ext  = strtolower($ext_foto);
            $dtMaster->bukti_fisik = $namaBaru . "." . $new_ext;
          }
        }

        $dtMaster->save();

        if ($dtMaster) {
          if (empty($request->id_penilaian_pendidikan)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Unsur Pendidikan '.$request->nama_penyedia,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Unsur Pendidikan '.$request->nama_penyedia,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama_penyedia = '';
      $no = 1;
      foreach ($request->id as $id_penilaian_pendidikan) {
        $remove = PenilaianPendidikan::find($id_penilaian_pendidikan);
        if (!empty($remove)) {
          if ($no == 1) {
            $nama_penyedia = $remove->nama_penyedia;
          }else{
            $nama_penyedia = $nama_penyedia.', '.$remove->nama_penyedia;
          }
          $remove->bl_state = 'D';
          $remove->save();
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Master Golongan '.$nama_penyedia,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }

    public function previewBuktiFisik(Request $request)
    {
      $data['dtMaster'] =
      (!empty($request->id_penilaian_pendidikan)) ?
      PenilaianPendidikan::find($request->id_penilaian_pendidikan) : "";
      // return $data;

      $content = view('dashboard.pendidikan.previewBuktiFisik', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

}
