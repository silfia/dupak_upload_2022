<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\TimPenilai;
use App\Models\PengajuanDupak;
use App\Models\PakMasterKegiatan;
use App\Models\Penilaian;
use App\Models\Pegawai;
use App\Models\Users;
use App\Models\StandartMaster;
use App\Models\DetailKegiatanPegawai;
use App\Models\RiwayatPenilaian;
use App\Models\BuktiFisikKegiatan;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;

class PenilaianDupakController extends Controller
{
    public function main(Request $request)
    {
      $this->data['mn_active'] = "penilaianDupak";
      $this->data['submn_active'] = "";
      $this->data['title'] = 'Penilaian DUPAK';
      $this->data['smallTitle'] = "";

      return view('penilaianDupak.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      if (Auth::getUser()->level_user == 1) {
        if ($request->search != '') {
          $data = DB::table('pengajuan_dupak')
                      ->select('id_pengajuan','pegawai.nama as nama', 'pegawai.no_nip', 'golongan.nama as nama_golongan', 'jabatan.nama as nama_jabatan','satuan_kerja.nama as nama_satuan_kerja','pengajuan_dupak.status_pengajuan as status','pengajuan_dupak.status_penilai1 as status_penilai1','pengajuan_dupak.status_penilai2 as status_penilai2','pengajuan_dupak.status_hasil as status_hasil','masa_pengajuan.id_masa_pengajuan as id_masa_pengajuan','masa_pengajuan.tgl_awal as tgl_awal_masa','masa_pengajuan.tgl_akhir as tgl_akhir_masa')
                      ->join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                      ->join('users','users.id','pengajuan_dupak.user_id')
                      ->join('pegawai','pegawai.user_id','users.id')
                      ->join('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                      ->join('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('pegawai.nama','like','%'.$request->search.'%')
                      ->where('masa_pengajuan.status_masa','!=','Selesai')
                      ->where('pengajuan_dupak.bl_state','A')
                      ->paginate($request->lngDt);
        }else{
          $data = DB::table('pengajuan_dupak')
                      ->select('id_pengajuan','pegawai.nama as nama', 'pegawai.no_nip', 'golongan.nama as nama_golongan', 'jabatan.nama as nama_jabatan','satuan_kerja.nama as nama_satuan_kerja','pengajuan_dupak.status_pengajuan as status','pengajuan_dupak.status_penilai1 as status_penilai1','pengajuan_dupak.status_penilai2 as status_penilai2','pengajuan_dupak.status_hasil as status_hasil','masa_pengajuan.id_masa_pengajuan as id_masa_pengajuan','masa_pengajuan.tgl_awal as tgl_awal_masa','masa_pengajuan.tgl_akhir as tgl_akhir_masa')
                      ->join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                      ->join('users','users.id','pengajuan_dupak.user_id')
                      ->join('pegawai','pegawai.user_id','users.id')
                      ->join('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                      ->join('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('masa_pengajuan.status_masa','!=','Selesai')
                      ->where('pengajuan_dupak.bl_state','A')
                      ->paginate($request->lngDt);
        }
      }else{
        $pegawai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        if ($request->search != '') {
          $data = DB::table('pengajuan_dupak')
                      ->select('id_pengajuan','pegawai.nama as nama', 'pegawai.no_nip', 'golongan.nama as nama_golongan', 'jabatan.nama as nama_jabatan','satuan_kerja.nama as nama_satuan_kerja','pengajuan_dupak.status_pengajuan as status','pengajuan_dupak.status_penilai1 as status_penilai1','pengajuan_dupak.status_penilai2 as status_penilai2','pengajuan_dupak.status_hasil as status_hasil','masa_pengajuan.id_masa_pengajuan as id_masa_pengajuan','masa_pengajuan.tgl_awal as tgl_awal_masa','masa_pengajuan.tgl_akhir as tgl_akhir_masa')
                      ->join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                      ->join('users','users.id','pengajuan_dupak.user_id')
                      ->join('pegawai','pegawai.user_id','users.id')
                      ->join('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                      ->join('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('pegawai.nama','like','%'.$request->search.'%')
                      ->where('masa_pengajuan.status_masa','!=','Selesai')
                      ->where('pengajuan_dupak.bl_state','A')
                      ->where('penilai_id', $pegawai->id_pegawai)
                      ->orWhere('penilai2_id', $pegawai->id_pegawai)
                      ->paginate($request->lngDt);
        }else{
          $data = DB::table('pengajuan_dupak')
                      ->select('id_pengajuan','pegawai.nama as nama', 'pegawai.no_nip', 'golongan.nama as nama_golongan', 'jabatan.nama as nama_jabatan','satuan_kerja.nama as nama_satuan_kerja','pengajuan_dupak.status_pengajuan as status','pengajuan_dupak.status_penilai1 as status_penilai1','pengajuan_dupak.status_penilai2 as status_penilai2','pengajuan_dupak.status_hasil as status_hasil','masa_pengajuan.id_masa_pengajuan as id_masa_pengajuan','masa_pengajuan.tgl_awal as tgl_awal_masa','masa_pengajuan.tgl_akhir as tgl_akhir_masa')
                      ->join('masa_pengajuan','masa_pengajuan.id_masa_pengajuan','pengajuan_dupak.masa_pengajuan_id')
                      ->join('users','users.id','pengajuan_dupak.user_id')
                      ->join('pegawai','pegawai.user_id','users.id')
                      ->join('data_master as golongan','golongan.id_master','pegawai.golongan_id')
                      ->join('data_master as jabatan','jabatan.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('penilai_id', $pegawai->id_pegawai)
                      ->orWhere('penilai2_id', $pegawai->id_pegawai)
                      ->where('masa_pengajuan.status_masa','!=','Selesai')
                      ->where('pengajuan_dupak.bl_state','A')
                      ->paginate($request->lngDt);
        }
      }

      // return $data;
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function formPenilaian(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->id);
      $data['pengajuan'] = $pengajuan;
      $butir = [];
      $no = 0;
      $thnAwal = date('Y', strtotime($pengajuan->tgl_awal_penilaian));
      $thnAkhir = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
      $noTh = 0;
      while ($thnAwal <= $thnAkhir) {
        $tahun[$noTh] = $thnAwal;
        $thnAwal++;
        $noTh++;
      }
      $data['tahun'] = $tahun;
      $pegawai = Pegawai::find($pengajuan->user->pegawai->id_pegawai);
      $data['pegawai'] = $pegawai;
      if (Auth::getUser()->level_user != 1) {
        $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        $data['statusTmbSelesai'] = 'Ya';
        if ($pengajuan->penilai_id == $penilai->id_pegawai) {
          if ($pengajuan->status_penilai1 == 'Y') {
            $data['statusTmbSelesai'] = 'Tidak';
          }
        }elseif($pengajuan->penilai2_id == $penilai->id_pegawai){
          if ($pengajuan->status_penilai2 == 'Y') {
            $data['statusTmbSelesai'] = 'Tidak';
          }
        }
      }else{
        $data['statusTmbSelesai'] = 'Tidak';
      }

      // get Total penilaian
      $TglAwalPengajuan = date('Y-m-d', strtotime($data['pengajuan']->tgl_awal_penilaian));
      $TglAkhirPengajuan = date('Y-m-d', strtotime($data['pengajuan']->tgl_akhir_penilaian));

      $data['penilaian'] = Penilaian::selectRaw("SUM(jumlah_px_pemohon) as jumlah_awal,SUM(jumlah_px_penilai) as jumlah_akhir,SUM(poin_pemohon) as ak_awal,SUM(poin_penilai) as ak_akhir")
                          ->where('user_id', $pengajuan->user_id)
                          ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                          ->first();

      $total_pengajuan = Penilaian::where('user_id', $pengajuan->user_id)
                              ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                              ->count();

      $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();

      if (!empty($penilai) && $pengajuan->penilai_id == $penilai->id_pegawai) {
          $total_sesuaikan = Penilaian::where('user_id', $pengajuan->user_id)
                            ->whereNotNull('tgl_acc_penilai')
                            ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                            ->count();
      } else {
          $total_sesuaikan = Penilaian::where('user_id', $pengajuan->user_id)
                            ->whereBetween('tanggal',[$TglAwalPengajuan,$TglAkhirPengajuan])
                            ->count();
      }

      // return $total_sesuaikan;
      if ($total_pengajuan == $total_sesuaikan) {
        $data['cek_button_sesuaikan'] = '';
      } else {
        $data['cek_button_sesuaikan'] = 'Ada';
      }

      $content = view('penilaianDupak.formPenilaian', $data)->render();
      return ['status'=>'success', 'content'=>$content, 'data'=>$data];
    }

    public function loadTahun(Request $request)
    {
      $pegawai = Pegawai::find($request->id_pegawai);
      $getButir = Formatters::get_butir($pegawai->tipe_profesi_id);
      if ($getButir['status'] == 'success') {
        $data['butir'] = $getButir['row']['butir'];
        $return = ['status'=>'success','code'=>200,'row'=>$data];
      }else{
        $return = ['status'=>'error','code'=>404,'row'=>$data];
      }
      // return $data;
      return response()->json($return);
    }

    public function getNilaiPemohon(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      // Start Get Tanggal
        $tahunAwalPengajuan = date('Y', strtotime($pengajuan->tgl_awal_penilaian));
        $tahunAkhirPengajuan = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
        
        if ($tahunAwalPengajuan == $tahunAkhirPengajuan) {
          $tglAwal = $pengajuan->tgl_awal_penilaian;
          $tglAkhir = $pengajuan->tgl_akhir_penilaian;
        }else{
          if ($request->tahun == $tahunAwalPengajuan) {
            $tglAwal = $pengajuan->tgl_awal_penilaian;
            $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhir = $request->tahun.'-12-'.$tanggalAkhirDesember;
          }elseif ($request->tahun == $tahunAkhirPengajuan) {
            $tglAwal = $request->tahun.'-01-01';
            $tglAkhir = $pengajuan->tgl_akhir_penilaian;
          }else{
            $tglAwal = $request->tahun.'-01-01';
            $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhir = $request->tahun.'-12-'.$tanggalAkhirDesember;
          }
        }
        $data['tanggalAwal'] = $tglAwal;
        $data['tanggalAkhir'] = $tglAkhir;
      // End Get Tanggal

      // Start Get Nilai Pemohon
        $data['nilaiPemohon'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                          ->where('master_kegiatan_id', $request->idPakMaster)
                                          ->where('user_id', $pengajuan->user_id)
                                          ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                          ->first();
      // End Get Nilai Pemohon
 
      // Start Cek Sudah Ada yang Acc Atau Belum
        $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        // return $penilai;
        $seluruhPenilaian = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                      ->get();
        $penilaianAcc = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('jumlah_px_penilai','!=','')
                                      ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                      ->get();
        $penilaianBelumAcc = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','')
                                        ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                        ->get();
        $data['jumlahSeluruhPenilaian'] = count($seluruhPenilaian);
        if (count($seluruhPenilaian) > 0) {
          if (Auth::getUser()->level_user == 1) {
            $data['statusData'] = 'Admin';

            if (count($penilaianAcc) > 0 && count($penilaianAcc) == count($seluruhPenilaian)) {
              $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_status', 'Y')
                                                ->where('acc_penilai2_status', 'Y')                                                
                                                ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                                ->get();
              if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                $data['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
              }else{
                $data['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
              }
            }elseif(count($penilaianAcc) > 0 && count($penilaianAcc) != count($seluruhPenilaian)){
              $data['statusPenilaian'] = 'Belum'; 
            }else{
              $data['statusPenilaian'] = 'Menunggu'; 
            }

          }else{
            $data['statusData'] = 'Ada';

              if (count($penilaianAcc) > 0 && count($penilaianAcc) == count($seluruhPenilaian)) {
                if ($penilai->id_pegawai == $pengajuan->penilai_id) {
                  $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_status', 'Y')
                                                ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                                ->get();
                  if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                    $data['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                  }else{
                    $data['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                  }
                }else{
                  $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_penilai2_status', 'Y')
                                                ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                                ->get();
                  if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                    $data['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                  }else{
                    $data['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                  }
                }
              }elseif (count($penilaianAcc) > 0 && count($penilaianAcc) != count($seluruhPenilaian)) {
                $data['statusPenilaian'] = 'Belum'; //Sebagian Sudah Dinilai
              }else{
                $data['statusPenilaian'] = 'Menunggu'; // Belum Ada yang dinilai
              }
          }          
        }else{
          $data['statusData'] = 'Tidak Ada';
          $data['statusPenilaian'] = 'Menunggu';
        }
      // End Cek Sudah Ada yang Acc Atau Belum

      // Start Get Nilai Penilai        
        $data['penilaian'] = Penilaian::selectRaw("SUM(jumlah_px_pemohon) as jumlah_awal,SUM(jumlah_px_penilai) as jumlah_akhir,SUM(poin_pemohon) as ak_awal,SUM(poin_penilai) as ak_akhir")
                          ->where('user_id', $pengajuan->user_id)
                          ->whereBetween('tanggal',[$tglAwal,$tglAkhir])                          
                          ->first();

        if (count($seluruhPenilaian) > 0) {
          $idPenilaianAcc = Penilaian::select('id_penilaian')
                                      ->where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('jumlah_px_penilai','!=','')
                                      ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                      ->get();
          $nilaiSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                      ->where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereNotIn('id_penilaian', $idPenilaianAcc)
                                      ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                      ->get();
          $nilaSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                      ->where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereIn('id_penilaian', $idPenilaianAcc)
                                      ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                      ->get();
          $nilaiPx = $nilaiSebelumAcc[0]['jumlah_px_pemohon'] + $nilaSetelahAcc[0]['jumlah_px_penilai'];
          $nilaiPoint = $nilaiSebelumAcc[0]['poin_pemohon'] + $nilaSetelahAcc[0]['poin_penilai'];
          $data['nilaiPenilai'] = [
            'jumlah_px' => $nilaiPx,
            'jumlah_point' => $nilaiPoint,
          ];
        }else{
          $data['nilaiPenilai'] = '';
        }
        // return $data;
      // End Get Nilai Penilai
      return ['status'=>'success','row'=>$data];     
    }

    public function sesuaiTahunan(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      // Start Get Tanggal
        $tahunAwalPengajuan = date('Y', strtotime($pengajuan->tgl_awal_penilaian));
        $tahunAkhirPengajuan = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
        if ($tahunAwalPengajuan == $tahunAkhirPengajuan) {
          $tglAwal = $pengajuan->tgl_awal_penilaian;
          $tglAkhir = $pengajuan->tgl_akhir_penilaian;
        }else{
          if ($request->tahun == $tahunAwalPengajuan) {
            $tglAwal = $pengajuan->tgl_awal_penilaian;
            $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhir = $request->tahun.'-12-'.$tanggalAkhirDesember;
          }elseif ($request->tahun == $tahunAkhirPengajuan) {
            $tglAwal = $request->tahun.'-01-01';
            $tglAkhir = $pengajuan->tgl_akhir_penilaian;
          }else{
            $tglAwal = $request->tahun.'-01-01';
            $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhir = $request->tahun.'-12-'.$akhirDes;
          }
        }
        $data['tanggalAwal'] = $tglAwal;
        $data['tanggalAkhir'] = $tglAkhir;
      // End Get Tanggal

      // Start Sesuaikan Nilai
        $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        if (isset($request->semua) && $request->semua == 'Semua') {
          if ($penilai->id_pegawai == $pengajuan->penilai_id) {
            $no_penilai = 1;
            $accPenilaian1 = Penilaian::where('user_id', $pengajuan->user_id)
                                        ->where('acc_status','B')
                                        ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                        ->get();
          }else{
            $no_penilai = 2;
            $accPenilaian1 = Penilaian::where('user_id', $pengajuan->user_id)
                                        ->where('acc_penilai2_status','B')
                                        ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                        ->get();
          }
        } else {
          if ($penilai->id_pegawai == $pengajuan->penilai_id) {
            $no_penilai = 1;
            $accPenilaian1 = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                          ->where('user_id', $pengajuan->user_id)
                                          ->where('acc_status','B')
                                          ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                          ->get();
          }else{
            $no_penilai = 2;
            $accPenilaian1 = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                          ->where('user_id', $pengajuan->user_id)
                                          ->where('acc_penilai2_status','B')
                                          ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                          ->get();
          }
        }
        
        $total_update = 0;
        // return $accPenilaian1;
        foreach ($accPenilaian1 as $penilaian1) {
          $updatePenilaian = Penilaian::find($penilaian1->id_penilaian);
          if ($no_penilai == 1) {
            if ($updatePenilaian->acc_penilai2_status == 'B') {
              $updatePenilaian->jumlah_px_penilai = $penilaian1->jumlah_px_pemohon;
              $updatePenilaian->poin_penilai = $penilaian1->poin_pemohon;
            }elseif ($updatePenilaian->acc_penilai2_status == 'Y') {
              $updatePenilaian->jumlah_px_penilai = $penilaian1->jumlah_px_penilai;
              $updatePenilaian->poin_penilai = $penilaian1->poin_penilai;
            }
            $updatePenilaian->penilai_id = $penilai->id_pegawai;
            $updatePenilaian->acc_status = 'Y';
            $updatePenilaian->tgl_acc_penilai = date('Y-m-d');
          }else{
            if ($updatePenilaian->acc_status == 'B') {
              $updatePenilaian->jumlah_px_penilai = $penilaian1->jumlah_px_pemohon;
              $updatePenilaian->poin_penilai = $penilaian1->poin_pemohon;
            }elseif ($updatePenilaian->acc_status == 'Y') {
              $updatePenilaian->jumlah_px_penilai = $penilaian1->jumlah_px_penilai;
              $updatePenilaian->poin_penilai = $penilaian1->poin_penilai;
            }
            $updatePenilaian->penilai2_id = $penilai->id_pegawai;
            $updatePenilaian->acc_penilai2_status = 'Y';
            $updatePenilaian->tgl_acc_penilai2 = date('Y-m-d');
          }
          $updatePenilaian->save();

          $total_update += 1;
        }
      // End Sesuaikan Nilai
      // Start Get Nilai Akhir
        $data['nilaiPemohon'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                          ->where('master_kegiatan_id', $request->idPakMaster)
                                          ->where('user_id', $pengajuan->user_id)
                                          ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                          ->first();
        $idPenilaianAcc = Penilaian::select('id_penilaian')
                                    ->where('master_kegiatan_id', $request->idPakMaster)
                                    ->where('user_id', $pengajuan->user_id)
                                    ->where('jumlah_px_penilai','!=','')
                                    ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                    ->get();
        $nilaiSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                    ->where('master_kegiatan_id', $request->idPakMaster)
                                    ->where('user_id', $pengajuan->user_id)
                                    ->whereNotIn('id_penilaian', $idPenilaianAcc)
                                    ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                    ->get();
        $nilaSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                    ->where('master_kegiatan_id', $request->idPakMaster)
                                    ->where('user_id', $pengajuan->user_id)
                                    ->whereIn('id_penilaian', $idPenilaianAcc)
                                    ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                                    ->get();
        $nilaiPx = $nilaiSebelumAcc[0]['jumlah_px_pemohon'] + $nilaSetelahAcc[0]['jumlah_px_penilai'];
        $nilaiPoint = $nilaiSebelumAcc[0]['poin_pemohon'] + $nilaSetelahAcc[0]['poin_penilai'];
        $data['nilaiPenilai'] = [
          'jumlah_px' => $nilaiPx,
          'jumlah_point' => $nilaiPoint,
        ];
      // End Get Nilai Akhir
      if($accPenilaian1->count() == $total_update){
        return ['status'=>'success', 'message'=>'Acc Penilaian Berhasil Dilakukan !!', 'data'=>$data];
      }else{
        return ['status'=>'error', 'message'=>'Acc Penilaian Gagal Dilakukan !!', 'data'=>''];
      }
    }

    public function loadBulan(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      $data['pengajuan'] = $pengajuan;
      $data['tahun'] = $request->tahun;
      // Start Get Butir Kegiatan
        $pakMaster = PakMasterKegiatan::find($request->idPak);
        $data['dataPak'] = $pakMaster;
        $butir = [];
        $idPak = $pakMaster->id_pak_master;
        for ($p=0; $p < $pakMaster->level; $p++) {
          $dt = PakMasterKegiatan::find($idPak);
          $butir[$p] = $dt;
          $idPak = $dt->parent_id;
        }
        $data['butir'] = $butir;
        $data['unsur'] = MasterType::find($pakMaster->unsur->id_master_type);
      // End Get Butir Kegiatan

      // Start Get Tanggal
        $tahunAwalPengajuan = date('Y', strtotime($pengajuan->tgl_awal_penilaian));
        $tahunAkhirPengajuan = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
        if($tahunAwalPengajuan == $tahunAkhirPengajuan){
          $tglAwalTahun = $pengajuan->tgl_awal_penilaian;
          $tglAkhirTahun = $pengajuan->tgl_akhir_penilaian;
        }else{
          if ($request->tahun == $tahunAwalPengajuan) {
            $tglAwalTahun = $pengajuan->tgl_awal_penilaian;
            $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhirTahun = $request->tahun.'-12-'.$tanggalAkhirDesember;
          }elseif ($request->tahun == $tahunAkhirPengajuan) {
            $tglAwalTahun = $request->tahun.'-01-01';
            $tglAkhirTahun = $pengajuan->tgl_akhir_penilaian;
          }else{
            $tglAwalTahun = $request->tahun.'-01-01';
            $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhirTahun = $request->tahun.'-12-'.$tanggalAkhirDesember;
          }
        }
        $data['tglAwalTahun'] = $tglAwalTahun;
        $data['tglAkhirTahun'] = $tglAkhirTahun;
      // End Get Tanggal

      $blnAwalPenilaian = date('m', strtotime($tglAwalTahun));
      $blnAkhirPenilaian = date('m', strtotime($tglAkhirTahun));
      $i = 0;
      $nilai = [];
      $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
      while ($blnAwalPenilaian <= $blnAkhirPenilaian) {
        // Start Get Bulan Penilaian
          $data['bulan'][$i] = (int)$blnAwalPenilaian;

          if (strlen($blnAwalPenilaian) == 1) {
            $angkaBulanLengkap = '0'.$blnAwalPenilaian;
          }else{
            $angkaBulanLengkap = $blnAwalPenilaian;
          }
          if ($i == 0) {
            $tglAwalBulan = $tglAwalTahun;
          }else{
            $tglAwalBulan = $request->tahun.'-'.$angkaBulanLengkap.'-01';
          }
          $jumlahHari = cal_days_in_month(CAL_GREGORIAN, $angkaBulanLengkap, $request->tahun);
          $tglAkhirBulan = $request->tahun.'-'.$angkaBulanLengkap.'-'.$jumlahHari;
        // End Get Bulan Penilaian

        // Start Get Nilai Pemohon
          $nilai[$i]['nilaiPemohon'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                                  ->where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                                  ->first();
        // End Get Nilai Pemohon

        // Start Cek Sudah Ada yang Acc Atau Belum
          $seluruhPenilaian = Penilaian::where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                        ->get();
          $penilaianAcc = Penilaian::where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','!=','')
                                        ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                        ->get();
          $penilaianBelumAcc = Penilaian::where('master_kegiatan_id', $request->idPak)
                                          ->where('user_id', $pengajuan->user_id)
                                          ->where('jumlah_px_penilai','')
                                          ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                          ->get();

          $nilai[$i]['jumlahSeluruh1Bulan'] = count($seluruhPenilaian);
          if (count($seluruhPenilaian) > 0) {
            if (Auth::getUser()->level_user == 1) {
              $nilai[$i]['statusData'] = 'Admin';
            }else{
              $nilai[$i]['statusData'] = 'Ada';
            }
            if (count($penilaianAcc) > 0 && count($penilaianAcc) == count($seluruhPenilaian)) {
              if ($penilai->id_pegawai == $pengajuan->penilai_id) {
                $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPak)
                                              ->where('user_id', $pengajuan->user_id)
                                              ->where('acc_status', 'Y')
                                              ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                              ->get();
                if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                  $nilai[$i]['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                }else{
                  $nilai[$i]['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                }
              }else{
                $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPak)
                                              ->where('user_id', $pengajuan->user_id)
                                              ->where('acc_penilai2_status', 'Y')
                                              ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                              ->get();
                if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                  $nilai[$i]['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                }else{
                  $nilai[$i]['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                }
              }
            }elseif (count($penilaianAcc) > 0 && count($penilaianAcc) != count($seluruhPenilaian)) {
              $nilai[$i]['statusPenilaian'] = 'Belum'; //Sebagian Sudah Dinilai
            }else{
              $nilai[$i]['statusPenilaian'] = 'Menunggu'; // Belum Ada yang dinilai
            }
          }else{
            $nilai[$i]['statusData'] = 'Tidak Ada';
            $nilai[$i]['statusPenilaian'] = 'Menunggu';
          }
        // End Cek Sudah Ada yang Acc Atau Belum

        // Start Get Nilai Penilai
          if (count($seluruhPenilaian) > 0) {
            $idPenilaianAcc = Penilaian::select('id_penilaian')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','!=','')
                                        ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                        ->get();
            $nilaiSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereNotIn('id_penilaian', $idPenilaianAcc)
                                        ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                        ->get();
            $nilaSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereIn('id_penilaian', $idPenilaianAcc)
                                        ->whereBetween('tanggal',[$tglAwalBulan,$tglAkhirBulan])
                                        ->get();
            $nilaiPx = $nilaiSebelumAcc[0]['jumlah_px_pemohon'] + $nilaSetelahAcc[0]['jumlah_px_penilai'];
            $nilaiPoint = $nilaiSebelumAcc[0]['poin_pemohon'] + $nilaSetelahAcc[0]['poin_penilai'];
            $nilai[$i]['nilaiPenilai'] = [
              'jumlah_px' => $nilaiPx,
              'jumlah_point' => $nilaiPoint,
            ];
          }else{
            $nilai[$i]['nilaiPenilai'] = '';
          }
        // End Get Nilai Penilai

        $blnAwalPenilaian++;
        $i++;
      }
      $data['nilai'] = $nilai;
      // Start Get Data 1 Tahun
        // Start Get Nilai 1 Tahun Pemohon
          $data['nilaiPemohonTahun'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                                  ->where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                  ->first();
        // End Get Nilai 1 Tahun Pemohon

        // Start Get Status 1 Tahunan
          $seluruhPenilaianTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                              ->where('user_id', $pengajuan->user_id)
                                              ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                              ->get();
          if ($penilai->id_pegawai == $pengajuan->penilai_id) {
            $seluruhPenilaianSendiriTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_status', '!=', 'Y')
                                                ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                ->get();
            $seluruhPenilaianLainTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_penilai2_status', 'Y')
                                                ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                ->get();
          }else {
            $seluruhPenilaianSendiriTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_penilai2_status', '!=', 'Y')
                                                ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                ->get();
            $seluruhPenilaianLainTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->where('acc_status', 'Y')
                                                ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                ->get();
          }
          if (count($seluruhPenilaianTahun) == count($seluruhPenilaianSendiriTahun)) {
            if (count($seluruhPenilaianLainTahun) == 0) {
              $data['statusTahun'] = 'Menunggu';
            }else{
              $data['statusTahun'] = 'Belum';
            }
          }elseif (count($seluruhPenilaianSendiriTahun) == 0) {
            $data['statusTahun'] = 'Selesai';
          }else{
            $data['statusTahun'] = 'Menunggu';
          }
        // End Get Status 1 Tahunan

        // Start Get Nilai 1 Tahun Penilai
          $idPenilaianTahunAcc = Penilaian::select('id_penilaian')
                                      ->where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('jumlah_px_penilai','!=','')
                                      ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                      ->get();
          $nilaiTahunSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                      ->where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereNotIn('id_penilaian', $idPenilaianTahunAcc)
                                      ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                      ->get();
          $nilaiTahunSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                      ->where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereIn('id_penilaian', $idPenilaianTahunAcc)
                                      ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                      ->get();
          $nilaiTahunPx = $nilaiTahunSebelumAcc[0]['jumlah_px_pemohon'] + $nilaiTahunSetelahAcc[0]['jumlah_px_penilai'];
          $nilaiTahunPoint = $nilaiTahunSebelumAcc[0]['poin_pemohon'] + $nilaiTahunSetelahAcc[0]['poin_penilai'];
          $data['nilaiPenilaiTahun'] = [
            'jumlah_px' => $nilaiTahunPx,
            'jumlah_point' => $nilaiTahunPoint,
          ];
        // End Get Nilai 1 Tahun Penilai
      // End Get Data 1 Tahun
      $content = view('penilaianDupak.formBulan', $data)->render();
      return ['status'=>'success', 'content'=>$content, 'data'=>$data];      
    }

    public function loadHarian(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      $data['pengajuan'] = $pengajuan;
      $data['tahun'] = $request->tahun;
      $data['bulan'] = $request->bulan;
      // Start Get Unsur and Butir Kegiatan
        $pakMaster = PakMasterKegiatan::find($request->idPak);
        $data['dataPak'] = $pakMaster;
        $butir = [];
        $idPak = $pakMaster->id_pak_master;
        for ($i=0; $i < $pakMaster->level; $i++) {
          $dt = PakMasterKegiatan::find($idPak);
          $butir[$i] = $dt;
          $idPak = $dt->parent_id;
        }
        $data['butir'] = $butir;
        $data['unsur'] = MasterType::find($pakMaster->unsur->id_master_type);
      // End Get Unsur and Butir Kegiatan

      // Start Get Tanggal
      $angkaBulanLengkap = (strlen($request->bulan) == 1) ? '0'.$request->bulan : $request->bulan;
      $bulanTahun = $request->tahun.'-'.$angkaBulanLengkap;
      $bulanAwal = date('Y-m', strtotime($pengajuan->tgl_awal_penilaian));
      $bulanAkhir = date('Y-m', strtotime($pengajuan->tgl_akhir_penilaian));
      if ($bulanTahun == $bulanAwal) {
        $tangalAwal = date('d', strtotime($pengajuan->tgl_awal_penilaian));
        $tanggalAkhir = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($pengajuan->tgl_awal_penilaian)), $request->tahun);
      }elseif ($bulanTahun == $bulanAkhir) {
        $tangalAwal = 01;
        $tanggalAkhir = date('d', strtotime($pengajuan->tgl_akhir_penilaian));
      }else{
        $tangalAwal = 01;
        $tanggalAkhir = cal_days_in_month(CAL_GREGORIAN, $angkaBulanLengkap, $request->tahun);
      }
      $data['tangalAwal'] = $tangalAwal;
      $data['tanggalAkhir'] = $tanggalAkhir;

      $nilai = [];
      $tanggal = (int)$tangalAwal;
      $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
      for ($j=$tanggal; $j <= $tanggalAkhir; $j++) {
        $angkaTanggalLengkap = (strlen($j) == 1) ? '0'.$j : $j;
        $tanggalLengkap = $request->tahun.'-'.$angkaBulanLengkap.'-'.$angkaTanggalLengkap;
        // Start Get Data Penilaian
          $dataPenilaian = Penilaian::where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('tanggal',$tanggalLengkap)
                                      ->first();
          $nilai[$j]['penilaian'] = $dataPenilaian;
        // End Get Data Penilaian
        // Start Cek Sudah Ada yang Acc Atau Belum
          if (!empty($dataPenilaian)) {
            $nilai[$j]['statusData'] = 'Ada';
            if ($dataPenilaian->acc_status == 'B' && $dataPenilaian->acc_penilai2_status == 'B') {
              $nilai[$j]['statusPenilaian'] = 'Menunggu';
            }else{
              if ($penilai->id_pegawai == $pengajuan->penilai_id) {
                $nilai[$j]['statusPenilaian'] = ($dataPenilaian->acc_status == 'Y') ? 'Selesai' : 'Belum';
              }else{
                $nilai[$j]['statusPenilaian'] = ($dataPenilaian->acc_penilai2_status == 'Y') ? 'Selesai' : 'Belum';
              }
            }
          }else{
            $nilai[$j]['statusData'] = 'Tidak Ada';
            $nilai[$j]['statusPenilaian'] = 'Menunggu';
          }
        // End Cek Sudah Ada yang Acc Atau Belum
      }
      $data['nilai'] = $nilai;
      // End Get Tanggal

      // Start Get Nilai 1 Bulan
        $angkaTanggalAwalBulanLengkap = (strlen($tangalAwal) == 1) ? '0'.$tangalAwal : $tangalAwal;
        $tanggalAwalBulan = $request->tahun.'-'.$angkaBulanLengkap.'-'.$angkaTanggalAwalBulanLengkap;
        $angkaTanggalAkhirBulanLengkap = (strlen($tanggalAkhir) == 1) ? '0'.$tanggalAkhir : $tanggalAkhir;
        $tanggalAkhirBulan = $request->tahun.'-'.$angkaBulanLengkap.'-'.$angkaTanggalAkhirBulanLengkap;
        $data['tanggalAwalBulan'] = $tanggalAwalBulan;
        $data['tanggalAkhirBulan'] = $tanggalAkhirBulan;
        // Start Get Nilai 1 Bulan Pemohon
          $data['nilaiPemohonBulan'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                                  ->where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                  ->first();
        // End Get Nilai 1 Bulan Pemohon

        // Start Get Status 1 Bulan
          $seluruhPenilaianBulan = Penilaian::where('master_kegiatan_id', $request->idPak)
                                              ->where('user_id', $pengajuan->user_id)
                                              ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                              ->get();
          if ($penilai->id_pegawai == $pengajuan->penilai_id) {
            $seluruhPenilaianSendiriBulanYa = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                        ->where('user_id', $pengajuan->user_id)
                                                        ->where('acc_status', 'Y')
                                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                        ->get();
            $seluruhPenilaianSendiriBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                          ->where('user_id', $pengajuan->user_id)
                                                          ->where('acc_status', '!=', 'Y')
                                                          ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                          ->get();
            $seluruhPenilaianLainBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                        ->where('user_id', $pengajuan->user_id)
                                                        ->where('acc_penilai2_status', '!=', 'Y')
                                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                        ->get();
          }else {
            $seluruhPenilaianSendiriBulanYa = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                        ->where('user_id', $pengajuan->user_id)
                                                        ->where('acc_penilai2_status', 'Y')
                                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                        ->get();
            $seluruhPenilaianSendiriBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                          ->where('user_id', $pengajuan->user_id)
                                                          ->where('acc_penilai2_status', '!=', 'Y')
                                                          ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                          ->get();
            $seluruhPenilaianLainBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                        ->where('user_id', $pengajuan->user_id)
                                                        ->where('acc_status', '!=', 'Y')
                                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                        ->get();
          }
          if (count($seluruhPenilaianBulan) == count($seluruhPenilaianSendiriBulanYa)) {
            $data['statusBulan'] = 'Selesai';
          }else{
            if (count($seluruhPenilaianSendiriBulanTidak) == count($seluruhPenilaianBulan) && count($seluruhPenilaianLainBulanTidak) == count($seluruhPenilaianBulan)) {
              $data['statusBulan'] = 'Menunggu';
            }else{
              $data['statusBulan'] = 'Belum';
            }
          }
        // End Get Status 1 Bulan

        // Start Get Nilai 1 Bulan Penilai
          $idPenilaianBulanAcc = Penilaian::select('id_penilaian')
                                      ->where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('jumlah_px_penilai','!=','')
                                      ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                      ->get();
          $nilaiBulanSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                      ->where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereNotIn('id_penilaian', $idPenilaianBulanAcc)
                                      ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                      ->get();
          $nilaiBulanSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                      ->where('master_kegiatan_id', $request->idPak)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereIn('id_penilaian', $idPenilaianBulanAcc)
                                      ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                      ->get();
          $nilaiBulanPx = $nilaiBulanSebelumAcc[0]['jumlah_px_pemohon'] + $nilaiBulanSetelahAcc[0]['jumlah_px_penilai'];
          $nilaiBulanPoint = $nilaiBulanSebelumAcc[0]['poin_pemohon'] + $nilaiBulanSetelahAcc[0]['poin_penilai'];
          $data['nilaiPenilaiBulan'] = [
            'jumlah_px' => $nilaiBulanPx,
            'jumlah_point' => $nilaiBulanPoint,
          ];
        // End Get Nilai 1 Bulan Penilai

      // End Get Nilai 1 Bulan

      // return ['status'=>'error', 'message'=>'Cek Dulu CuY !!', 'data'=>$data];
      $content = view('penilaianDupak.formHarian', $data)->render();
      return ['status'=>'success', 'content'=>$content, 'data'=>$data];
    }

    public function detailKegiatan(Request $request)
    {
      $data['penilaian'] = Penilaian::find($request->idPenilaian);
      $data['pakMaster'] = PakMasterKegiatan::find($data['penilaian']->master_kegiatan_id);
      $data['pegawai'] = Pegawai::where('user_id', $data['penilaian']->user_id)->first();
      $data['detailKegiatan'] = DetailKegiatanPegawai::where('penilaian_id', $request->idPenilaian)->get();
      
      $data['bukti_fisik'] = BuktiFisikKegiatan::selectRaw("*")->join('master_bukti_fisik as mb','mb.id_master_bukti','bukti_fisik_kegiatan.master_bukti_fisik_id')->where('pak_master_kegiatan_id',$data['penilaian']->master_kegiatan_id)->get();
      $data['tgl'] = $request->tgl;
      
      $content = view('penilaianDupak.viewDetail', $data)->render();
      return ['status'=>'success', 'content'=>$content];
    }


    public function accNilai(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      // Start Proses Acc Nilai
        $tanggal = (strlen($request->tgl) == 1) ? '0'.$request->tgl : $request->tgl;
        $bulan = (strlen($request->bulan) == 1) ? '0'.$request->bulan : $request->bulan;
        $tanggalLengkap = $request->tahun.'-'.$bulan.'-'.$tanggal;
        $penilaian = Penilaian::where('user_id', $pengajuan->user_id)
                                ->where('tanggal', $tanggalLengkap)
                                ->where('master_kegiatan_id', $request->idPak)
                                ->first();
        $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
        if ($penilai->id_pegawai == $pengajuan->penilai_id) {
          $penilaian->penilai_id          = $penilai->id_pegawai;
          $penilaian->acc_status          = 'Y';
          $penilaian->tgl_acc_penilai     = date('Y-m-d');
          if ($penilaian->acc_penilai2_status == 'B') {
            $penilaian->jumlah_px_penilai = $penilaian->jumlah_px_pemohon;
            $penilaian->poin_penilai      = $penilaian->poin_pemohon;
          }
          $penilaian->save();
        }elseif ($penilai->id_pegawai == $pengajuan->penilai2_id) {
          $penilaian->penilai2_id           = $penilai->id_pegawai;
          $penilaian->acc_penilai2_status   = 'Y';
          $penilaian->tgl_acc_penilai2      = date('Y-m-d');
          if ($penilaian->acc_status == 'B') {
            $penilaian->jumlah_px_penilai   = $penilaian->jumlah_px_pemohon;
            $penilaian->poin_penilai        = $penilaian->poin_pemohon;
          }
          $penilaian->save();
        }else{
          return ['status'=>'error', 'code'=>500, 'message'=>'Tidak Memiliki Akses Sebagai Penilai Pemohon !!', 'data'=>''];
        }
      // End Proses Acc Nilai
      if ($penilaian) {
        $data['dataPenilaian'] = $penilaian;
        // start get nilai 1 bulan
          // Start Get Tanggal Awal & Akhir Bulan
            $bulanTahun = $request->tahun.'-'.$bulan;
            $bulanAwal = date('Y-m', strtotime($pengajuan->tgl_awal_penilaian));
            $bulanAkhir = date('Y-m', strtotime($pengajuan->tgl_akhir_penilaian));
            if ($bulanTahun == $bulanAwal) {
              $tangalAwal = date('d', strtotime($pengajuan->tgl_awal_penilaian));
              $tanggalAkhir = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($pengajuan->tgl_awal_penilaian)), $request->tahun);
            }elseif ($bulanTahun == $bulanAkhir) {
              $tangalAwal = 01;
              $tanggalAkhir = date('d', strtotime($pengajuan->tgl_akhir_penilaian));
            }else{
              $tangalAwal = 01;
              $tanggalAkhir = cal_days_in_month(CAL_GREGORIAN, $bulan, $request->tahun);
            }
            $angkaTanggalAwalBulanLengkap = (strlen($tangalAwal) == 1) ? '0'.$tangalAwal : $tangalAwal;
            $tanggalAwalBulan = $request->tahun.'-'.$bulan.'-'.$angkaTanggalAwalBulanLengkap;
            $angkaTanggalAkhirBulanLengkap = (strlen($tanggalAkhir) == 1) ? '0'.$tanggalAkhir : $tanggalAkhir;
            $tanggalAkhirBulan = $request->tahun.'-'.$bulan.'-'.$angkaTanggalAkhirBulanLengkap;
            $dataBulan['tanggalAwalBulan'] = $tanggalAwalBulan;
            $dataBulan['tanggalAkhirBulan'] = $tanggalAkhirBulan;
          // End Get Tanggal Awal & Akhir Bulan

          // Start Get Nilai 1 Bulan Pemohon
            $dataBulan['nilaiPemohonBulan'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                                    ->where('master_kegiatan_id', $request->idPak)
                                                    ->where('user_id', $pengajuan->user_id)
                                                    ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                    ->first();
          // End Get Nilai 1 Bulan Pemohon

          // Start Get Status 1 Bulan
            $seluruhPenilaianBulan = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                ->get();
            if ($penilai->id_pegawai == $pengajuan->penilai_id) {
              $seluruhPenilaianSendiriBulanYa = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                          ->where('user_id', $pengajuan->user_id)
                                                          ->where('acc_status', 'Y')
                                                          ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                          ->get();
              $seluruhPenilaianSendiriBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                            ->where('user_id', $pengajuan->user_id)
                                                            ->where('acc_status', '!=', 'Y')
                                                            ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                            ->get();
              $seluruhPenilaianLainBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                          ->where('user_id', $pengajuan->user_id)
                                                          ->where('acc_penilai2_status', '!=', 'Y')
                                                          ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                          ->get();
            }else {
              $seluruhPenilaianSendiriBulanYa = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                          ->where('user_id', $pengajuan->user_id)
                                                          ->where('acc_penilai2_status', 'Y')
                                                          ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                          ->get();
              $seluruhPenilaianSendiriBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                            ->where('user_id', $pengajuan->user_id)
                                                            ->where('acc_penilai2_status', '!=', 'Y')
                                                            ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                            ->get();
              $seluruhPenilaianLainBulanTidak = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                          ->where('user_id', $pengajuan->user_id)
                                                          ->where('acc_status', '!=', 'Y')
                                                          ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                                          ->get();
            }
            if (count($seluruhPenilaianBulan) == count($seluruhPenilaianSendiriBulanYa)) {
              $dataBulan['statusBulan'] = 'Selesai';
            }else{
              if (count($seluruhPenilaianSendiriBulanTidak) == count($seluruhPenilaianBulan) && count($seluruhPenilaianLainBulanTidak) == count($seluruhPenilaianBulan)) {
                $dataBulan['statusBulan'] = 'Menunggu';
              }else{
                $dataBulan['statusBulan'] = 'Belum';
              }
            }
          // End Get Status 1 Bulan

          // Start Get Nilai 1 Bulan Penilai
            $idPenilaianBulanAcc = Penilaian::select('id_penilaian')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','!=','')
                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                        ->get();
            $nilaiBulanSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereNotIn('id_penilaian', $idPenilaianBulanAcc)
                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                        ->get();
            $nilaiBulanSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereIn('id_penilaian', $idPenilaianBulanAcc)
                                        ->whereBetween('tanggal',[$tanggalAwalBulan,$tanggalAkhirBulan])
                                        ->get();
            $nilaiBulanPx = $nilaiBulanSebelumAcc[0]['jumlah_px_pemohon'] + $nilaiBulanSetelahAcc[0]['jumlah_px_penilai'];
            $nilaiBulanPoint = $nilaiBulanSebelumAcc[0]['poin_pemohon'] + $nilaiBulanSetelahAcc[0]['poin_penilai'];
            $dataBulan['nilaiPenilaiBulan'] = [
              'jumlah_px' => $nilaiBulanPx,
              'jumlah_point' => $nilaiBulanPoint,
            ];
          // End Get Nilai 1 Bulan Penilai
          $data['dataBulan'] = $dataBulan;
        // end get nilai 1 bulan
        // start get nilai 1 tahun
          // Start Get Tanggal Awal & Akhir Tahun
            $tahunAwalPengajuan = date('Y', strtotime($pengajuan->tgl_awal_penilaian));
            $tahunAkhirPengajuan = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
            if($tahunAwalPengajuan == $tahunAkhirPengajuan){
              $tglAwalTahun = $pengajuan->tgl_awal_penilaian;
              $tglAkhirTahun = $pengajuan->tgl_akhir_penilaian;
            }else{
              if ($request->tahun == $tahunAwalPengajuan) {
                $tglAwalTahun = $pengajuan->tgl_awal_penilaian;
                $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
                $tglAkhirTahun = $request->tahun.'-12-'.$tanggalAkhirDesember;
              }elseif ($request->tahun == $tahunAkhirPengajuan) {
                $tglAwalTahun = $request->tahun.'-01-01';
                $tglAkhirTahun = $pengajuan->tgl_akhir_penilaian;
              }else{
                $tglAwalTahun = $request->tahun.'-01-01';
                $tanggalAkhirDesember = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
                $tglAkhirTahun = $request->tahun.'-12-'.$tanggalAkhirDesember;
              }
            }
            $dataTahun['tglAwalTahun'] = $tglAwalTahun;
            $dataTahun['tglAkhirTahun'] = $tglAkhirTahun;
          // End Get Tanggal Awal & Akhir Tahun

          // Start Get Nilai 1 Tahun Pemohon
            $dataTahun['nilaiPemohonTahun'] = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                                    ->where('master_kegiatan_id', $request->idPak)
                                                    ->where('user_id', $pengajuan->user_id)
                                                    ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                    ->first();
          // End Get Nilai 1 Tahun Pemohon

          // Start Get Status 1 Tahun
            $seluruhPenilaianTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                ->where('user_id', $pengajuan->user_id)
                                                ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                ->get();
            if ($penilai->id_pegawai == $pengajuan->penilai_id) {
              $seluruhPenilaianSendiriTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_status', '!=', 'Y')
                                                  ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                  ->get();
              $seluruhPenilaianLainTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_penilai2_status', 'Y')
                                                  ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                  ->get();
            }else {
              $seluruhPenilaianSendiriTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_penilai2_status', '!=', 'Y')
                                                  ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                  ->get();
              $seluruhPenilaianLainTahun = Penilaian::where('master_kegiatan_id', $request->idPak)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_status', 'Y')
                                                  ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                                  ->get();
            }
            if (count($seluruhPenilaianTahun) == count($seluruhPenilaianSendiriTahun)) {
              if (count($seluruhPenilaianLainTahun) == 0) {
                $dataTahun['statusTahun'] = 'Menunggu';
              }else{
                $dataTahun['statusTahun'] = 'Belum';
              }
            }elseif (count($seluruhPenilaianSendiriTahun) == 0) {
              $dataTahun['statusTahun'] = 'Selesai';
            }else{
              $dataTahun['statusTahun'] = 'Menunggu';
            }
          // End Get Status 1 Tahun

          // Start Get Nilai 1 Tahun Penilai
            $idPenilaianTahunAcc = Penilaian::select('id_penilaian')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','!=','')
                                        ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                        ->get();
            $nilaiTahunSebelumAcc = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereNotIn('id_penilaian', $idPenilaianTahunAcc)
                                        ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                        ->get();
            $nilaiTahunSetelahAcc = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                        ->where('master_kegiatan_id', $request->idPak)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereIn('id_penilaian', $idPenilaianTahunAcc)
                                        ->whereBetween('tanggal',[$tglAwalTahun,$tglAkhirTahun])
                                        ->get();
            $nilaiTahunPx = $nilaiTahunSebelumAcc[0]['jumlah_px_pemohon'] + $nilaiTahunSetelahAcc[0]['jumlah_px_penilai'];
            $nilaiTahunPoint = $nilaiTahunSebelumAcc[0]['poin_pemohon'] + $nilaiTahunSetelahAcc[0]['poin_penilai'];
            $dataTahun['nilaiPenilaiTahun'] = [
              'jumlah_px' => $nilaiTahunPx,
              'jumlah_point' => $nilaiTahunPoint,
            ];
          // End Get Nilai 1 Tahun Penilai
          $data['dataTahun'] = $dataTahun;
        // end get nilai 1 tahun
        // return ['status'=>'error', 'code'=>500, 'message'=>'Cek Dulu Cuy !!', 'data'=>$data];

        return ['status'=>'success', 'code'=>200, 'message'=>'Kegiatan Berhasil Disetujui !!', 'data'=>$data];
      }else{
        return ['status'=>'error', 'code'=>500, 'message'=>'Kegiatan Gagal Disetujui !!', 'data'=>''];
      }
    }

    public function formGantiNilai(Request $request)
    {
      $data['idPak']        = $request->idPak;
      $data['idPengajuan']  = $request->idPengajuan;
      $data['tahun']        = $request->tahun;
      $data['bulan']        = $request->bulan;
      $data['tgl']          = $request->tgl;

      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      $tgl = (strlen($request->tgl) == 1) ? '0'.$request->tgl : $request->tgl;
      $bln = (strlen($request->bulan) == 1) ? '0'.$request->bulan : $request->bulan;
      $tanggalLengkap = $request->tahun.'-'.$bln.'-'.$tgl;
      $penilaian = Penilaian::where('user_id', $pengajuan->user_id)
                              ->where('tanggal', $tanggalLengkap)
                              ->where('master_kegiatan_id', $request->idPak)
                              ->first();
      $data['penilaian'] = $penilaian;
      $content = view('penilaianDupak.formGantiNilai', $data)->render();
      return ['status'=>'success', 'content'=>$content,];
    }

    public function gantiNilai(Request $request)
    {      
      
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      $data['pengajuan'] = $pengajuan;
      $tgl = (strlen($request->tgl) == 1) ? '0'.$request->tgl : $request->tgl;
      $bln = (strlen($request->bulan) == 1) ? '0'.$request->bulan : $request->bulan;
      $tanggalLengkap = $request->tahun.'-'.$bln.'-'.$tgl;
      $penilaian = Penilaian::where('user_id', $pengajuan->user_id)
                              ->where('tanggal', $tanggalLengkap)
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->first();
      $pakMaster = PakMasterKegiatan::find($request->idPakMaster);
      if ($request->Jumlah_Baru == 0) {
        $jumlahPx = 0;
        $jumlahPoint = 0;
      }else{
        $pegawai = Users::join('pegawai','pegawai.user_id','users.id')->where('users.id', $pengajuan->user_id)->first();
        if ($pegawai->jabatan_id == $pakMaster->jabatan_id) {
          $persenPoint = 1;
        }else{
          $cekUrutJabatanPeg = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                          ->where('is_golongan', 'N')
                                          ->where('bl_state', 'A')
                                          ->where('id_master', $pegawai->jabatan_id)->first();
          $cekUrutJabIn = DataMaster::find($pakMaster->jabatan_id);
          if ($cekUrutJabatanPeg->urutan > $cekUrutJabIn->urutan) {
            $cekJabKr1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                    ->where('is_golongan', 'N')
                                    ->where('bl_state', 'A')
                                    ->where('urutan', ($cekUrutJabatanPeg->urutan - 1))->first();
            if ($pakMaster->jabatan_id == $cekJabKr1->id_master) {
              $persenPoint = 1;
            }else{
              $persenPoint = 0;
            }
          }else{
            $cekJabLb1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                    ->where('is_golongan', 'N')
                                    ->where('bl_state', 'A')
                                    ->where('urutan', ($cekUrutJabatanPeg->urutan + 1))->first();
            if ($pakMaster->jabatan_id == $cekJabLb1->id_master) {
              $persenPoint = 0.8;
            }else{
              $persenPoint = 0;
            }
          }
        }
        if (!empty($pakMaster)) {
          $jumlahPoint = (($request->Jumlah_Baru / $pakMaster->jum_min) * $pakMaster->points)*$persenPoint;
        }else{
          $jumlahPoint = 0;
        }
        $jumlahPx = $request->Jumlah_Baru;
      }

      $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
      
      if ($penilai->id_pegawai == $pengajuan->penilai_id) {
        $penilaian->penilai_id          = $penilai->id_pegawai;
        $penilaian->acc_status          = 'Y';
        $penilaian->tgl_acc_penilai     = date('Y-m-d');
        $penilaian->alasan_perubahan_1     = $request->alasan_perubahan;
        
        if ($penilaian->acc_penilai2_status == 'B') {
          $penilaian->jumlah_px_penilai = $jumlahPx;
          $penilaian->poin_penilai      = $jumlahPoint;
        }

        $penilaian->save();
      }elseif ($penilai->id_pegawai == $pengajuan->penilai2_id) {
        $penilaian->penilai2_id           = $penilai->id_pegawai;
        $penilaian->acc_penilai2_status   = 'Y';
        $penilaian->tgl_acc_penilai2      = date('Y-m-d');
        $penilaian->alasan_perubahan_2     = $request->alasan_perubahan;   
            
        if ($penilaian->acc_status == 'B') {
          $penilaian->jumlah_px_penilai   = $jumlahPx;
          $penilaian->poin_penilai        = $jumlahPoint;
        }

        $penilaian->save();
      }else{
        return ['status'=>'error', 'code'=>500, 'message'=>'Tidak Memiliki Akses Sebagai Penilai Pemohon !!', 'data'=>''];
      }

      if ($penilaian) {
        $data['penilaian'] = $penilaian;
        // Start Create Date
        $noBln = (strlen($request->bulan) == 1) ? '0'.$request->bulan : $request->bulan;
        $bulanSelect = $request->tahun.'-'.$noBln;
        $bulanAwal = date('Y-m', strtotime($pengajuan->tgl_awal_penilaian));
        $bulanAkhir = date('Y-m', strtotime($pengajuan->tgl_akhir_penilaian));
        if ($bulanSelect == $bulanAwal) {
          $dayAwal = date('d', strtotime($pengajuan->tgl_awal_penilaian));
          $dayAkhir = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($pengajuan->tgl_awal_penilaian)), $request->tahun);
        }elseif ($bulanSelect == $bulanAkhir) {
          $dayAwal = 01;
          $dayAkhir = date('d', strtotime($pengajuan->tgl_akhir_penilaian));
        }else{
          $dayAwal = 01;
          $dayAkhir = cal_days_in_month(CAL_GREGORIAN, $noBln, $request->tahun);
        }
        $data['dayAwal'] = $dayAwal;
        $data['dayAkhir'] = $dayAkhir;
        $data['tanggal'] = $request->tgl;
        $data['tahun'] = $request->tahun;
        $data['bulan'] = $request->bulan;
        // End Create Date
        // start cek Semua Acc Atau Tidak
        $tg = (strlen($dayAwal) == 1) ? '0'.$dayAwal : $dayAwal;
        $tgEnd = (strlen($dayAkhir) == 1) ? '0'.$dayAkhir : $dayAkhir;
        $tglAwal = $request->tahun.'-'.$noBln.'-'.$tg;
        $tglAkhir = $request->tahun.'-'.$noBln.'-'.$tgEnd;
        $data['tglAwal'] = $tglAwal;
        $data['tglAkhir'] = $tglAkhir;
        $totaDataCek = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                            ->where('user_id', $pengajuan->user_id)
                            ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                            ->count();
        $jmlAcc1 = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                            ->where('user_id', $pengajuan->user_id)
                            ->where('acc_status', 'Y')
                            ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                            ->count();
        $jmlAcc2 = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                            ->where('user_id', $pengajuan->user_id)
                            ->where('acc_penilai2_status', 'Y')
                            ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                            ->count();
        $data['totDataCek'] = $totaDataCek;
        if ($totaDataCek == $jmlAcc1 && $totaDataCek == $jmlAcc2) {
          $data['statusJumlah'] = 'Sudah';
        }else{
          $data['statusJumlah'] = 'Belum';
        }
        // end cek Semua Acc Atau Tidak
        // start Get Nilai 1 Bulan
          //start Untuk pemohon
          $hasilPemohon = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->where('user_id', $pengajuan->user_id)
                              ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                              ->get();
          $data['pxPemohon'] = $hasilPemohon[0]['jumlah_px_pemohon'];
          $data['pointPemohon'] = $hasilPemohon[0]['poin_pemohon'];
          // end Untuk pemohon
          // start Untuk Penilai
          $idPxPenilaiCek = Penilaian::select('id_penilaian')
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->where('user_id', $pengajuan->user_id)
                              ->where('jumlah_px_penilai','!=','')
                              ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                              ->get();
          $pxBeforeCek = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->where('user_id', $pengajuan->user_id)
                              ->whereNotIn('id_penilaian', $idPxPenilaiCek)
                              ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                              ->get();
          $pxAfterCek = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                              ->where('master_kegiatan_id', $request->idPakMaster)
                              ->where('user_id', $pengajuan->user_id)
                              ->whereIn('id_penilaian', $idPxPenilaiCek)
                              ->whereBetween('tanggal',[$tglAwal,$tglAkhir])
                              ->get();
          $data['pxAfterCek'] = $pxBeforeCek[0]['jumlah_px_pemohon'] + $pxAfterCek[0]['jumlah_px_penilai'];
          $data['pointAfterCek'] = $pxBeforeCek[0]['poin_pemohon'] + $pxAfterCek[0]['poin_penilai'];
          $data['idAfterCek'] = (count($idPxPenilaiCek) > 0) ? 'Ada' : 'Tidak';
          // End Untuk Penilai
        // End Get Nilai 1 Bulan
        // Start Get Nilai 1 Tahun
        $tahunAwal = date('Y', strtotime($pengajuan->tgl_awal_penilaian));
        $tahunAkhir = date('Y', strtotime($pengajuan->tgl_akhir_penilaian));
        if($tahunAwal == $tahunAkhir){
          $tglAwalThn = $pengajuan->tgl_awal_penilaian;
          $tglAkhirThn = $pengajuan->tgl_akhir_penilaian;
        }else{
          if ($request->tahun == $tahunAwal) {
            $tglAwalThn = $pengajuan->tgl_awal_penilaian;
            $akhirDesThn = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhirThn = $request->tahun.'-12-'.$akhirDesThn;
          }elseif ($request->tahun == $tahunAkhir) {
            $tglAwalThn = $request->tahun.'-01-01';
            $tglAkhirThn = $pengajuan->tgl_akhir_penilaian;
          }else{
            $tglAwalThn = $request->tahun.'-01-01';
            $akhirDesThn = cal_days_in_month(CAL_GREGORIAN, '12', $request->tahun);
            $tglAkhirThn = $request->tahun.'-12-'.$akhirDesThn;
          }
        }
        $idPxPenilaiCekThn = Penilaian::select('id_penilaian')
                                        ->where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','!=','')
                                        ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                        ->get();
        $pxBeforeCekThn = Penilaian::selectRaw('SUM(jumlah_px_pemohon) as jumlah_px_pemohon, SUM(poin_pemohon) as poin_pemohon')
                                        ->where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereNotIn('id_penilaian', $idPxPenilaiCekThn)
                                        ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                        ->get();
        $pxAfterCekThn = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                        ->where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereIn('id_penilaian', $idPxPenilaiCekThn)
                                        ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                        ->get();
        $CekThnPemohon = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                        ->where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                        ->get();
        $data['thnPxAfterCek'] = $pxBeforeCekThn[0]['jumlah_px_pemohon'] + $pxAfterCekThn[0]['jumlah_px_penilai'];
        $data['thnPointAfterCek'] = $pxBeforeCekThn[0]['poin_pemohon'] + $pxAfterCekThn[0]['poin_penilai'];
        $data['thnIdAfterCek'] = (count($idPxPenilaiCekThn) > 0) ? 'Ada' : 'Tidak';
        $data['thnPxbeforePemohon'] = $CekThnPemohon[0]['jumlah_px_penilai'];
        $data['thnPobeforePemohon'] = $CekThnPemohon[0]['poin_penilai'];
        $totalPenilaiThn = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                      ->count();
        $totalPenilai1Thn = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('acc_status', 'Y')
                                      ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                      ->count();
        $totalPenilai2Thn = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                      ->where('user_id', $pengajuan->user_id)
                                      ->where('acc_penilai2_status', 'Y')
                                      ->whereBetween('tanggal',[$tglAwalThn,$tglAkhirThn])
                                      ->count();
        $data['totDataCek'] = $totalPenilaiThn;
        if ($totalPenilaiThn == $totalPenilai1Thn && $totalPenilaiThn == $totalPenilai2Thn) {
          $data['statusAccTahun'] = 'Sudah';
        }else{
          $data['statusAccTahun'] = 'Belum';
        }
        // $data['tglAwal'] = $tglAwalThn;
        // $data['tglAkhir'] = $tglAkhirThn;
        // End Get Nilai 1 Tahun

        return ['status'=>'success', 'code'=>200, 'message'=>'Kegiatan Berhasil Diperbaharui !!', 'data'=>$data];
      }else{
        return ['status'=>'error', 'code'=>500, 'message'=>'Kegiatan Gagal Diperbaharui !!', 'data'=>''];
      }
    }

    public function simpanPenilai(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPenilai);
      $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
      $jumlahPenilaian = Penilaian::where('user_id', $pengajuan->user_id)
                                    ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                    ->get();
      if ($pengajuan->penilai_id == $penilai->id_pegawai) {
        $jumlahAccPenilaian = Penilaian::where('user_id', $pengajuan->user_id)
                                          ->where('acc_status', 'Y')
                                          ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                          ->get();
        $no_penilai = 1;
      }elseif ($pengajuan->penilai2_id == $penilai->id_pegawai) {
        $jumlahAccPenilaian = Penilaian::where('user_id', $pengajuan->user_id)
                                      ->where('acc_penilai2_status', 'Y')
                                      ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                      ->get();
        $no_penilai = 2;
      }      

      // return count($jumlahPenilaian).' '.count($jumlahAccPenilaian);
      if (count($jumlahPenilaian) == count($jumlahAccPenilaian)) {
        $pegawai = Pegawai::where('user_id', $pengajuan->user_id)->first();
        $golonganPegawai = DataMaster::find($pegawai->golongan_id);
        $urutNext = $golonganPegawai->urutan + 1;
        $golonganNextPegawai = DataMaster::where('is_golongan','Y')
                                          ->where('urutan', $urutNext)
                                          ->first();
        $pointNext = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                      ->where('standart_master.master_id', $pegawai->jabatan_id)
                                      ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                      ->first();
        $status = 'Naik Golongan';
        if (empty($pointNext)) {
          $jabatanPegawai = DataMaster::find($pegawai->jabatan_id);
          $urutJabatanNext = $jabatanPegawai->urutan + 1;
          $jabatanNextPegawai = DataMaster::where('is_golongan','N')
                                            ->where('tipe_profesi_id', $pegawai->tipe_profesi_id)
                                            ->where('urutan', $urutJabatanNext)
                                            ->first();
                                            // terakhir
          $golonganNextPegawai = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                                ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                                ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                                ->first();
          $pointNext = StandartMaster::join('data_master as golongan','standart_master.mst_id','golongan.id_master')
                                        ->where('standart_master.master_id', $jabatanNextPegawai->id_master)
                                        ->where('standart_master.mst_id', $golonganNextPegawai->id_master)
                                        ->first();
          $status = 'Naik Jabatan';
        }
        $pointPenilaian = Penilaian::selectRaw('SUM(jumlah_px_penilai) as jumlah_px_penilai, SUM(poin_penilai) as poin_penilai')
                                    ->where('user_id', $pengajuan->user_id)
                                    ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                    ->first();
        $pointLama = $pegawai->point_pegawai;
        $pointBaru = $pointLama + $pointNext['poin_penilai'];

        if ($pointBaru >= $pointLama) {
          $status_hasil = 'Naik';
          if ($status == 'Naik Jabatan') {
            $pesan = 'Point Pegawai menjukkan bahwa pagawai naik Jabatan '.$jabatanNextPegawai->nama.' dan Golongan '.$golonganNextPegawai->nama.' !!';
          }else{
            $pesan = 'Point Pegawai menjukkan bahwa pagawai naik Golongan !!';
          }
        }else{
          $status_hasil = 'Tetap';
          $pesan = 'Point Pegawai Tidak Mencukupi untuk naik Gologan atau Jabatan !!';
        }

        $updatePengajuan = PengajuanDupak::find($request->idPenilai);
        if ($no_penilai == 1) {
          $updatePengajuan->status_penilai1 = 'Y';
          $updateAkhir = ($updatePengajuan->status_penilai2 == 'Y') ? 'Yes':'No';
        }elseif ($no_penilai == 2) {
          $updatePengajuan->status_penilai2 = 'Y';
          $updateAkhir = ($updatePengajuan->status_penilai1 == 'Y') ? 'Yes':'No';
        }
        $updatePengajuan->status_hasil = $status_hasil;
        if ($updateAkhir == 'Yes') {
          $updatePengajuan->status_pengajuan = 'Selesai';
          $updatePengajuan->jabatan_lama_id = $pegawai->jabatan_id;
          if ($status == 'Naik Jabatan') {
            $updatePengajuan->jabatan_baru_id = $jabatanNextPegawai->id_master;
          }else{
            $updatePengajuan->jabatan_baru_id = $pegawai->jabatan_id;
          }
          $updatePengajuan->golongan_lama_id = $pegawai->golongan_id;
          $updatePengajuan->golongan_baru_id = $golonganNextPegawai->id_master;
        }
        $updatePengajuan->save();

        if ($updatePengajuan) {
          $data['pegawai'] = $pegawai;
          $data['golonganPegawai'] = $golonganPegawai;
          $data['golonganNExtPegawai'] = $golonganNextPegawai;
          $data['pointNextGolongan'] = $pointNext;
          $data['point'] = $pointPenilaian;
          $data['hasilPoint'] = $pointPenilaian['poin_penilai'];

          return ['status'=>'success','code'=>200,'message'=>$pesan,'data'=>$data];
        }else{
          return ['status'=>'error','code'=>500,'message'=>'Proses Selesai Gagal Dilakukan !!','data'=>''];
        }
      }else{
        $data['dtPengajuan'] = $jumlahPenilaian;
        $data['jmlTotal'] = count($jumlahPenilaian);
        $data['jmlPengajuan'] = count($jumlahAccPenilaian);
        return ['status'=>'error','code'=>500,'message'=>'Terdapat Penilaian yang belum di Konfirmasi, silahkan cek kembali penilaian Anda !!','data'=>$data];
      }
    }

     public function formPerubahanData(Request $request)
    {
      // return $request->all();
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      $riwayat = RiwayatPenilaian::where('pengajuan_id',$request->idPengajuan)->where('master_kegiatan_id',$request->idPak)->first();

      $data['pengajuan'] = $pengajuan;
      $data['tahun'] = $request->tahun;
      $data['idPak'] = $request->idPak;
      $data['idPengajuan'] = $request->idPengajuan;
      $data['jml_awal'] = $request->jml_awal;

      if (!empty($riwayat)) {
        $data['idRiwayat'] = $riwayat->id_riwayat;        
      }else{
        $data['idRiwayat'] = '';        
      }

      $content = view('penilaianDupak.formEditNilai', $data)->render();
      return ['status'=>'success', 'content'=>$content, 'data'=>$data];      
    }

    public function gantiNilaiEdit(Request $request)
    {
      $pengajuan = PengajuanDupak::find($request->idPengajuan);
      $data['pengajuan'] = $pengajuan;

      $penilaian = Penilaian::where('user_id', $pengajuan->user_id)
                  ->where('master_kegiatan_id', $request->idPakMaster)
                  ->whereBetween('tanggal', [$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                  ->get();
      $jml_baru = $request->Jumlah_Baru;
      $array = [];

      if (count($penilaian) > 0) {
          for ($i=0; $i < count($penilaian); $i++) {          
            $update   = Penilaian::where('user_id', $pengajuan->user_id)
                                  ->where('master_kegiatan_id', $request->idPakMaster)
                                  ->where('id_penilaian',$penilaian[$i]->id_penilaian)
                                  ->first();
            array_push($array,$update->id_penilaian);
            $update->jumlah_px_penilai = 1;
            $jml_baru -=1;
            $update->save();
          }
      }

      $count_array = count($array);
      $jumlah_bagi = $jml_baru / $count_array;
      $count_s = count($array) - 1;
      $sisa = 0;
      $array_sisa = [];
      for ($i=0; $i < $count_array; $i++) { 
            $update   = Penilaian::where('id_penilaian',$array[$i])
                                  ->first();
            if ($i == $count_s) {
                $floor = floor($jumlah_bagi);
                $fu = $update->jumlah_px_pemohon - 1;
                $nilais = ($floor <= $fu) ? $floor : $fu;  
                $hd = ($floor <= $fu) ? 'pas' : 'lebih';
                if ($hd == 'pas') {
                  $jumlah_sisa = $fu - $floor;
                  $sisa += $jumlah_sisa;
                  array_push($array_sisa,$update->id_penilaian);
                }  
                $update->jumlah_px_penilai += $nilais;   
                  // return $update;
                $jml_baru -= $nilais;

            }else{
                $ceil = ceil($jumlah_bagi);
                $fu = $update->jumlah_px_pemohon - 1;
                $nilais = ($ceil <= $fu) ? $ceil : $fu;  
                $hd = ($ceil <= $fu) ? 'pas' : 'lebih';
                if ($hd == 'pas') {
                  $jumlah_sisa = $ceil - $fu;
                  $sisa += $jumlah_sisa;
                  array_push($array_sisa,$update->id_penilaian);
                  // return $jumlah_sisa;
                }  
                // return $hd;
                $update->jumlah_px_penilai += $nilais;   
                $jml_baru -= $nilais;
            }
            $update->save();
      }
      $count_sisa = count($array_sisa);
      // return $jml_baru;
      for ($i=0; $i < $count_sisa; $i++) { 
        if ($sisa != 0) {
            $update   = Penilaian::where('id_penilaian',$array_sisa[$i])
                                  ->first();
            $fu = $update->jumlah_px_pemohon;
            $nilais = ($jml_baru <= $fu) ? $jml_baru : $fu;  
            $update->jumlah_px_penilai += $nilais;  
            $jml_baru -= $nilais;
            $update->save();
        }
      }
        $pakMaster = PakMasterKegiatan::find($request->idPakMaster);
        $pegawai = Users::join('pegawai','pegawai.user_id','users.id')->where('users.id', $pengajuan->user_id)->first();
        $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();

          if ($pegawai->jabatan_id == $pakMaster->jabatan_id) {
            $persenPoint = 1;
          }else{
            $cekUrutJabatanPeg = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                            ->where('is_golongan', 'N')
                                            ->where('bl_state', 'A')
                                            ->where('id_master', $pegawai->jabatan_id)->first();
              
              $cekUrutJabIn = DataMaster::find($pakMaster->jabatan_id);
              if ($cekUrutJabatanPeg->urutan > $cekUrutJabIn->urutan) {
                $cekJabKr1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                        ->where('is_golongan', 'N')
                                        ->where('bl_state', 'A')
                                        ->where('urutan', ($cekUrutJabatanPeg->urutan - 1))->first();
                if ($pakMaster->jabatan_id == $cekJabKr1->id_master) {
                  $persenPoint = 1;
                }else{
                  $persenPoint = 0;
                }
              }else{
                $cekJabLb1 = DataMaster::where('tipe_profesi_id',$pegawai->tipe_profesi_id)
                                        ->where('is_golongan', 'N')
                                        ->where('bl_state', 'A')
                                        ->where('urutan', ($cekUrutJabatanPeg->urutan + 1))->first();
                if ($pakMaster->jabatan_id == $cekJabLb1->id_master) {
                  $persenPoint = 0.8;
                }else{
                  $persenPoint = 0;
                }
              }
          }
          
          foreach ($array as $key => $value) {
            $penilais = Penilaian::where('user_id', $pengajuan->user_id)
                  ->where('master_kegiatan_id', $request->idPakMaster)
                  ->where('id_penilaian',$value)
                  ->first();
            if (!empty($pakMaster)) {
              $jumlahPoint = (($penilais->jumlah_px_penilai / $pakMaster->jum_min) * $pakMaster->points)*$persenPoint;
            }else{
              $jumlahPoint = 0;
            }

            if ($penilai->id_pegawai == $pengajuan->penilai_id) {
              $penilais->penilai_id          = $penilai->id_pegawai;
              $penilais->acc_status          = 'Y';
              $penilais->tgl_acc_penilai     = date('Y-m-d');
              
              if ($penilais->acc_penilai2_status == 'B') {
                $penilais->poin_penilai = $jumlahPoint;
              }

              $penilais->save(); 
            }elseif ($penilai->id_pegawai == $pengajuan->penilai2_id) {
              $penilais->penilai2_id           = $penilai->id_pegawai;
              $penilais->acc_penilai2_status   = 'Y';
              $penilais->tgl_acc_penilai2      = date('Y-m-d');
                  
              if ($penilais->acc_status == 'B') {
                $penilais->poin_penilai = $jumlahPoint;
              }
              $penilais->save(); 
            }else{
              return ['status'=>'error', 'code'=>500, 'message'=>'Tidak Memiliki Akses Sebagai Penilai Pemohon !!', 'data'=>''];
            }                     
          }

        if (isset($penilais)) {

          $data['penilaian'] = Penilaian::selectRaw("SUM(jumlah_px_pemohon) as jumlah_awal,SUM(jumlah_px_penilai) as jumlah_akhir,SUM(poin_pemohon) as ak_awal,SUM(poin_penilai) as ak_akhir")
                          ->where('user_id', $pengajuan->user_id)
                          ->whereBetween('tanggal', [$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                          ->first();

          $riwayat = (!empty($request->idRiwayat)) ? RiwayatPenilaian::find($request->idRiwayat) : new RiwayatPenilaian;        
          $riwayat->user_id = $pengajuan->user_id;
          $riwayat->pengajuan_id = $pengajuan->id_pengajuan;
          $riwayat->master_kegiatan_id = $request->idPakMaster;
          $riwayat->jml_awal_pemohon = $request->Jumlah_Awal;
          $riwayat->jml_akhir_penilai = $request->Jumlah_Baru;
          $riwayat->tgl_perubahan = date('Y-m-d');        
          
          if ($penilai->id_pegawai == $pengajuan->penilai_id) {
            $riwayat->penilai1_id = $penilai->id_pegawai;
            $riwayat->alasan_perubahan1 = $request->alasan_perubahan;
          }else{
            $riwayat->penilai2_id = $penilai->id_pegawai;
            $riwayat->alasan_perubahan2 = $request->alasan_perubahan;          
          }
          $riwayat->save();
        }

        if (isset($riwayat)) {
          // return 'test';
          // Start Cek Sudah Ada yang Acc Atau Belum
          $penilai = Pegawai::where('user_id', Auth::getUser()->id)->first();
          // return $penilai;
          $seluruhPenilaian = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                        ->get();
          $penilaianAcc = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                        ->where('user_id', $pengajuan->user_id)
                                        ->where('jumlah_px_penilai','!=','')
                                        ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                        ->get();
          $penilaianBelumAcc = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                          ->where('user_id', $pengajuan->user_id)
                                          ->where('jumlah_px_penilai','')
                                          ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                          ->get();
          $data['jumlahSeluruhPenilaian'] = count($seluruhPenilaian);
          if (count($seluruhPenilaian) > 0) {
            if (Auth::getUser()->level_user == 1) {
              $data['statusData'] = 'Admin';

              if (count($penilaianAcc) > 0 && count($penilaianAcc) == count($seluruhPenilaian)) {
                $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_status', 'Y')
                                                  ->where('acc_penilai2_status', 'Y')                                                
                                                  ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                                  ->get();
                if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                  $data['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                }else{
                  $data['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                }
              }elseif(count($penilaianAcc) > 0 && count($penilaianAcc) != count($seluruhPenilaian)){
                $data['statusPenilaian'] = 'Belum'; 
              }else{
                $data['statusPenilaian'] = 'Menunggu'; 
              }

            }else{
              $data['statusData'] = 'Ada';

                if (count($penilaianAcc) > 0 && count($penilaianAcc) == count($seluruhPenilaian)) {
                  if ($penilai->id_pegawai == $pengajuan->penilai_id) {
                    $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_status', 'Y')
                                                  ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                                  ->get();
                    if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                      $data['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                    }else{
                      $data['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                    }
                  }else{
                    $penilaianSendiri = Penilaian::where('master_kegiatan_id', $request->idPakMaster)
                                                  ->where('user_id', $pengajuan->user_id)
                                                  ->where('acc_penilai2_status', 'Y')
                                                  ->whereBetween('tanggal',[$pengajuan->tgl_awal_penilaian,$pengajuan->tgl_akhir_penilaian])
                                                  ->get();
                    if (count($penilaianSendiri) == count($seluruhPenilaian)) {
                      $data['statusPenilaian'] = 'Selesai'; // sudah dinilai Semua
                    }else{
                      $data['statusPenilaian'] = 'Sementara'; // Belum Ada yang dinilai
                    }
                  }
                }elseif (count($penilaianAcc) > 0 && count($penilaianAcc) != count($seluruhPenilaian)) {
                  $data['statusPenilaian'] = 'Belum'; //Sebagian Sudah Dinilai
                }else{
                  $data['statusPenilaian'] = 'Menunggu'; // Belum Ada yang dinilai
                }
            }          
          }else{
            $data['statusData'] = 'Tidak Ada';
            $data['statusPenilaian'] = 'Menunggu';
          }
          return ['status'=>'success', 'code'=>200, 'message'=>'Kegiatan Berhasil Diperbaharui !!', 'data'=>$data];
        }else{
          return ['status'=>'error', 'code'=>500, 'message'=>'Kegiatan Gagal Diperbaharui !!', 'data'=>''];
        }
    }

}
