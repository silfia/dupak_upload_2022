<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Desa;
use App\Models\Pegawai;
use App\Models\SatuanKerja;
use App\Models\MasaPengajuan;
use App\Models\Users;
use App\Models\CekNotif;
use App\Models\MasterType;
use App\Models\PenerapanPermenpan;
use Auth, Redirect, Validator, DB;

class OtherController extends Controller
{
    public function getKabupaten(Request $request)
    {
      $kabupaten = DB::table('kabupaten')->where('provinsi_id', $request->id)->get();
      return response()->json($kabupaten);
    }

    public function getKecamatan(Request $request)
    {
      $kecamatan = DB::table('kecamatan')->where('kabupaten_id', $request->id)->get();
      return response()->json($kecamatan);
    }

    public function getDesa(Request $request)
    {
      $desa = DB::table('desa')->where('kecamatan_id', $request->id)->get();
      return response()->json($desa);
    }

    public function getjabatan(Request $request)
    {
      $jabatan = DB::table('data_master')
                    ->where('is_golongan', 'N')
                    ->where('tipe_profesi_id',$request->id)
                    ->where('bl_state','A')->get();
      return response()->json($jabatan);
    }

    public function getPegawai(Request $request)
    {
      $pegawai = Pegawai::where('pegawai.id_pegawai', $request->id)
                          ->with('user')
                          ->with('golongan')
                          ->with('jabatan')
                          ->with('satuanKerja')
                          ->with('typeProfesi')
                          ->first();
      if (!empty($pegawai)) {
        $return = ['status' => 'success', 'code' => '200', 'message' => 'Data Pegawai Ditemukan !!', 'data' => $pegawai];
      }else{
        $return = ['status' => 'warning', 'code' => '404', 'message' => 'Data Pegawai Tidak Ditemukan !!', 'data' => ''];
      }
      return response()->json($return);
    }

    public function getEmail(Request $request)
    {
      if($request->id == ''){
        $user = Users::select('email','name')->where('email',$request->email)->first();
      }else{
        $user = Users::select('email','name')->where('email',$request->email)->where('id', '!=', $request->id)->first();
      }
      if (!empty($user)) {
        $return = ['status' => 'success', 'code' => '200', 'message' => 'Data Ditemukan !!', 'data' => $user];
      }else{
        $return = ['status' => 'empty', 'code' => '404', 'message' => 'Data Tidak Ditemukan !!', 'data' => ''];
      }

      return response()->json($return);
    }

    public function getNip(Request $request)
    {
      $nip = str_replace(' ', '', $request->nip);
      if($request->id == ''){
        $user = Users::select('username','name')->where('username', $nip)->first();
      }else{
        $user = Users::select('username','name')->where('username', $nip)->where('id', '!=', $request->id)->first();
      }
      if (!empty($user)) {
        $return = ['status' => 'success', 'code' => '200', 'message' => 'Data Ditemukan !!', 'data' => $user];
      }else{
        $return = ['status' => 'empty', 'code' => '404', 'message' => 'Data Tidak Ditemukan !!', 'data' => ''];
      }

      return response()->json($return);
    }

    public function getKodeSatker(Request $request)
    {
      if($request->id == ''){
        $satuanKerja = SatuanKerja::select('kode','nama')->where('kode', $request->kode)->first();
      }else{
        $satuanKerja = SatuanKerja::select('kode','nama')->where('kode', $request->kode)->where('id_satuan_kerja', '!=', $request->id)->first();
      }
      if (!empty($satuanKerja)) {
        $return = ['status' => 'success', 'code' => '200', 'message' => 'Data Ditemukan !!', 'data' => $satuanKerja];
      }else{
        $return = ['status' => 'empty', 'code' => '404', 'message' => 'Data Tidak Ditemukan !!', 'data' => ''];
      }

      return response()->json($return);
    }

    public function cekWaktu(Request $request)
    {
      $dateNow = date('Y-m-d');
      $cekNotif = CekNotif::where('nama_notif','Cek Waktu Pengajuan DUPAK')->where('tgl_cek', $dateNow)->first();
      if (empty($cekNotif)) {
        // cek status Belum
        $masaBelum = MasaPengajuan::where('status_masa','Belum')->get();
        if (count($masaBelum) > 0) {
          foreach ($masaBelum as $belum) {
            if ($dateNow >= $belum->tgl_awal && $dateNow <= $belum->tgl_akhir) {
              $gantiBelum = MasaPengajuan::find($belum->id_masa_pengajuan);
              $gantiBelum->status_masa = 'Proses';
              $gantiBelum->save();
            }
          }
        }

        // cek status Proses
        $masaProses = MasaPengajuan::where('status_masa','Proses')->get();
        if (count($masaProses) > 0) {
          foreach ($masaProses as $proses) {
            if ($dateNow > $proses->tgl_akhir) {
              $gantiProses = MasaPengajuan::find($proses->id_masa_pengajuan);
              $gantiProses->status_masa = 'Tutup';
              $gantiProses->save();
            }
          }
        }

        $notif = CekNotif::where('nama_notif', 'Cek Waktu Pengajuan DUPAK')->first();
        if (!empty($notif)) {
          $updateNotif = CekNotif::find($notif->id_cek_notif);
          $updateNotif->tgl_cek = $dateNow;
          $updateNotif->save();
        }else{
          $newNotif = new CekNotif;
          $newNotif->nama_notif = 'Cek Waktu Pengajuan DUPAK';
          $newNotif->tgl_cek = $dateNow;
          $newNotif->save();
        }

        $return = ['status' => 'success', 'code' => '200', 'message' => 'Waktu Pengajuan Berhasil di Cek !!', 'data' => ''];
      }else{
        $return = ['status' => 'warning', 'code' => '200', 'message' => 'Waktu Pengajuan Sudah di Cek !!', 'data' => ''];
      }

      return response()->json($return);
    }

    public function cekPermenpan(Request $request)
    {
      $dateNow = date('Y-m-d');
      $yearNow = date('Y');
      $cekNotif = CekNotif::where('nama_notif','Cek Permenpan')->whereYear('tgl_cek', $yearNow)->first();
      if (empty($cekNotif)) {
        $permenProfesi = MasterType::select('permenpan_kegiatan.id_permenpan','master_type.id_master_type as id_profesi')
                                    ->join('permenpan_kegiatan', 'permenpan_kegiatan.tipe_profesi_id', 'master_type.id_master_type')
                                    ->where('master_type.modul', 'StatusProfesi')
                                    ->where('permenpan_kegiatan.status', 'Aktif')
                                    ->where('master_type.bl_state', 'A')
                                    ->get();
        foreach ($permenProfesi as $keyPermenProfesi) {
          $penerapan = new PenerapanPermenpan;
          $penerapan->tahun = $yearNow;
          $penerapan->permenpan_id = $keyPermenProfesi->id_permenpan;
          $penerapan->tipe_profesi_id = $keyPermenProfesi->id_profesi;
          $penerapan->save();
        }

        $newNotif = new CekNotif;
        $newNotif->nama_notif = 'Cek Permenpan';
        $newNotif->tgl_cek = $dateNow;
        $newNotif->save();

        $return = ['status' => 'success', 'code' => '200', 'message' => 'Permenpan Berhasil Ditetapkan !!', 'data' => ''];
      }else{
        $return = ['status' => 'warning', 'code' => '200', 'message' => 'Permenpan Sudah Ditetapkan !!', 'data' => ''];
      }
      return response()->json($return);
    }

    public function ManualGuide()
    {
      return view('component.ManualGuide');
    }
}
