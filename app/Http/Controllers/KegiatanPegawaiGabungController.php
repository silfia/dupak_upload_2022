<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\DataMaster;
use App\Models\Pegawai;
use App\Models\Penilaian;
use App\Models\PakMasterKegiatan;
use App\Http\Libraries\Activitys;
use App\Http\Libraries\CompressFile;
use App\Http\Libraries\Formatters;
use Auth, Redirect, Validator, DB;

class KegiatanPegawaiGabungController extends Controller
{
	public function main(Request $request)
	{
		$this->data['mn_active'] = "kegiatanHarianGab";
		$this->data['submn_active'] = "";
		$this->data['title'] = 'Daftar Kegiatan Harian Gabung';
		$this->data['smallTitle'] = "";
		if (Auth::getUser()->level_user == '5') {
			$this->data['dtPegawai'] = Users::find(Auth::id());
			$this->data['dtJabatan'] = DataMaster::where('tipe_profesi_id', $this->data['dtPegawai']->pegawai->tipe_profesi_id)
													->where('is_golongan','N')
													->where('bl_state','A')
													->orderBy('urutan','ASC')->get();
			return view('kegiatanPegawaiGabung.main')->with('data', $this->data);
		}
	}

	public function loadHarian(Request $request)
	{
		// return $request->all();
		if (!empty($request->blnSelect)) {
			$blnLoad = explode('-', $request->blnSelect) ;
			$tahun = $blnLoad[1];
			$bulan = $blnLoad[0];
		}else{
			$tahun = date('Y'); //Mengambil tahun saat ini
			$bulan = date('m'); //Mengambil bulan saat ini
		}

		$data['jumlahTanggal'] = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$pegawai = Pegawai::where('user_id', Auth::id())->first();
		$getButir = Formatters::get_butir($pegawai->tipe_profesi_id);

		if ($getButir['status'] == 'success') {
			$data['butir'] = $getButir['row']['butir'];
			$return = ['status'=>'success','code'=>200, 'message'=>'Unsur Kegiatan Ditemukan !!', 'row'=>$data];
		}else{
			$data['butir'] = [];
			$return = ['status'=>'error','code'=>404, 'message'=>'Unsur Kegiatan Tidak Ditemukan !!','row'=>$data];
		}
		return response()->json($return);
	}

	public function getNilaiHarian(Request $request)
	{
		// return $request->all();
		$blnLoad = explode('-', $request->bulan);
		$tahun = $blnLoad[1];
		$bulan = $blnLoad[0];
		$jumlahTanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

		$data = [];
		$jml = 0;
		$jmlReal = 0;
		$jmlAk = 0;
		$kegiatan = PakMasterKegiatan::find($request->idPakMaster);
		$pegawai = Pegawai::where('user_id', Auth::id())->first();
		for ($i=0; $i < $jumlahTanggal; $i++) { 
			$dy = $i+1;
			$tglFull = $tahun.'-'.$bulan.'-'.$dy;
			$cekNilai = Penilaian::where('jenis', 'Harian')
									->where('master_kegiatan_id', $request->idPakMaster)
									->where('tanggal', $tglFull)
									->where('user_id', Auth::id())
									->first();
			if (!empty($cekNilai)) {
				if ($pegawai->status_pegawai == 'PNS') {
					$hasil = $cekNilai->jumlah_px_pemohon;
				}else{
					$hasil = $cekNilai->jumlah_px_real;
				}
				$idPenilaian = $cekNilai->id_penilaian;
				$jmlReal = $jml + $cekNilai->jumlah_px_real;
				$jml = $jml + $cekNilai->jumlah_px_pemohon;
				$jmlAk = $jmlAk + $cekNilai->poin_pemohon;
			}else{
				$hasil = '-';
				$idPenilaian = '';
			}

			$data[$i] = [
				'idjabatan'   => $kegiatan->jabatan_id,
				'idpak'       => $request->idPakMaster,
				'tgl'         => $i+1,
				'bln'         => $bulan,
				'bulanload'   => $bulan,
				'thn'         => $tahun,
				'hasil'       => $hasil,
				'idPenilaian' => $idPenilaian,
			];
		}

		if ($pegawai->status_pegawai == 'Non-PNS') {
			$jml_keg = $jmlReal;
			$jmlAk = '-';
		}else{
			$jml_keg = $jml;
			$jmlAk = $jmlAk;
		}

		$dtJml['idJabatan'] = $kegiatan->jabatan_id;
		$dtJml['idpak'] = $request->idPakMaster;
		$dtJml['jml'] = $jml_keg;
		$dtJml['jmlAk'] = $jmlAk;

		$return = ['status'=>'success','code'=>200,'row'=>$data, 'dtJumlah'=>$dtJml];
		return response()->json($return);
	}
}
