<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\DataMaster;
use App\Models\MasterType;
use App\Models\SatuanKerja;
use App\Models\PengajuanDupak;
use App\Models\Users;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class PengajuanController extends Controller
{
  public function main(Request $request)
  {
    $this->data['mn_active'] = "buatPengajuan";
    $this->data['submn_active'] = "";
    $this->data['title'] = 'Buat Pengumuman Pengajuan Dupak';
    $this->data['smallTitle'] = "";

    return view('dashboard.pengajuan.main')->with('data', $this->data);
  }

  public function loadDataPengajuan(Request $request)
  {
    if ($request->search != '') {
      $data = DB::table('pengajuan_dupak')
                ->where('keterangan','like','%'.$request->search.'%')
                ->where('bl_state','A')
                ->orderBy('keterangan','ASC')
                ->paginate($request->lngDt);
    }else{
      $data = DB::table('pengajuan_dupak')
                ->where('bl_state','A')
                ->orderBy('keterangan','ASC')
                ->paginate($request->lngDt);
    }
    $return = ['status'=>'success','code'=>200,'row'=>$data];
    return response()->json($return);
  }

    public function formPengajuan(Request $request)
    {
      $data['pengajuan'] = (!empty($request->id)) ? PengajuanDupak::find($request->id) : "";
      $content = view('dashboard.pengajuan.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function saveDataPj(Request $request)
    {
      $rules = array(
        'keterangan'            => 'required',
        'tgl_awal'   => 'required',
        'tgl_akhir'    => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $dtPengajuan                       = (!empty($request->id_Bpengajuan)) ? PengajuanDupak::find($request->id_Bpengajuan) : new PengajuanDupak;
        $dtPengajuan->keterangan           = $request->keterangan;
        $dtPengajuan->tgl_awal             = $request->tgl_awal;
        $dtPengajuan->tgl_akhir            = $request->tgl_akhir;
        $dtPengajuan->bl_state             = 'A';
        $dtPengajuan->save();
        if ($dtPengajuan) {
          if (empty($request->id_Bpengajuan)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Informasi Pengajuan',
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Informasi Pengajuan',
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

}
