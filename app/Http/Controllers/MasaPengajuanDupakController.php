<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasaPengajuan;
use App\Http\Libraries\Activitys;
use Auth, Redirect, Validator, DB;

class MasaPengajuanDupakController extends Controller
{
    public function main(Request $request)
    {
        $this->data['mn_active'] = "masaPengajuanDupak";
        $this->data['submn_active'] = "";
        $this->data['title'] = 'Waktu Pengajuan Dupak';
        $this->data['smallTitle'] = "";

        return view('masaPengajuanDupak.main')->with('data', $this->data);
    }

    public function loadData(Request $request)
    {
      if ($request->search != '') {
        $data = DB::table('masa_pengajuan')
                  ->where('tgl_awal','like','%'.$request->search.'%')
                  ->where('bl_state','A')
                  ->orderBy('tgl_awal','DESC')
                  ->paginate($request->lngDt);
      }else{
        $data = DB::table('masa_pengajuan')
                  ->where('bl_state','A')
                  ->orderBy('tgl_awal','DESC')
                  ->paginate($request->lngDt);
      }
      $return = ['status'=>'success','code'=>200,'row'=>$data];
      return response()->json($return);
    }

    public function form(Request $request)
    {
      $data['masa_pengajuan'] = (!empty($request->id)) ? MasaPengajuan::find($request->id) : "";
      if (!empty($request->id)) {
        if ($data['masa_pengajuan']->status_masa == 'Selesai') {
          return ['status' => 'error', 'message' => 'Tidak Dapat Mengubah Data, Masa Pengajuan Sudah Berakhir !!'];
        // }elseif ($data['masa_pengajuan']->status_masa == 'Proses') {
        //   return ['status' => 'error', 'message' => 'Tidak Dapat Mengubah Data, Masa Pengajuan Sedang Berlangsung !!'];
        }
      }
      $content = view('masaPengajuanDupak.form', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function save(Request $request)
    {
      $rules = array(
        'tgl_awal'    => 'required',
        'tgl_akhir'   => 'required',
        'keterangan'  => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $tglAwal = date('Y-m-d', strtotime($request->tgl_awal));
        $tglAkhir = date('Y-m-d', strtotime($request->tgl_akhir));
        $dtMasaPengajuan                = (!empty($request->id_masa_pengajuan)) ? MasaPengajuan::find($request->id_masa_pengajuan) : new MasaPengajuan;
        $dtMasaPengajuan->keterangan    = $request->keterangan;
        $dtMasaPengajuan->tgl_awal      = $tglAwal;
        $dtMasaPengajuan->tgl_akhir     = $tglAkhir;
        $dtMasaPengajuan->ac_conn       = $_SERVER['REMOTE_ADDR'];
        if (empty($request->id_masa_pengajuan)) {
          $dtMasaPengajuan->creator_id  = Auth::getUser()->id;
          $dtMasaPengajuan->bl_state    = 'A';
          $dtMasaPengajuan->status_masa = 'Belum';
        }else{
          $dateNow = date('Y-m-d');
          if ($dateNow < $tglAwal) {
            $dtMasaPengajuan->status_masa = 'Belum';
          }elseif ($dateNow >= $tglAwal && $dateNow <= $tglAkhir) {
            $dtMasaPengajuan->status_masa = 'Proses';
          }elseif ($dateNow > $tglAkhir) {
            $dtMasaPengajuan->status_masa = 'Tutup';
          }
        }
        $dtMasaPengajuan->save();

        if ($dtMasaPengajuan) {
          if (empty($request->id_masa_pengajuan)) {
            $requestActivity = [
              'action_type'   => 'Insert',
              'message'       => 'Create Masa Pengajuan '.$dtMasaPengajuan->tgl_awal.' s/d '.$dtMasaPengajuan->tgl_akhir,
            ];
          }else{
            $requestActivity = [
              'action_type'   => 'Update',
              'message'       => 'Update Masa Pengajuan '.$dtMasaPengajuan->tgl_awal.' s/d '.$dtMasaPengajuan->tgl_akhir,
            ];
          }
          $saveActivity = Activitys::add($requestActivity);

          $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }

    public function removeData(Request $request)
    {
      $nama = '';
      $no = 1;
      foreach ($request->id as $id_masa) {
        $remove = MasaPengajuan::where('id_masa_pengajuan', $id_masa)->first();
        if (!empty($remove)) {
          if ($remove->status_masa == 'Belum') {
            if ($no == 1) {
              $nama = $remove->id_masa_pengajuan;
            }else{
              $nama = $nama.', '.$remove->id_masa_pengajuan;
            }
            $remove->bl_state = 'D';
            $remove->save();
          }
        }
        $no++;
      }

      $requestActivity = [
        'action_type'   => 'Delete',
        'message'       => 'Delete Masa Pengajuan '.$nama,
      ];
      $saveActivity = Activitys::add($requestActivity);

      if ($remove) {
        return ['status' => 'success', 'message' => 'Data Berhasil Dihapus !!'];
      }else{
        return ['status'=>'error', 'message'=>'Data Gagal Dihapus !!'];
      }
    }
}
