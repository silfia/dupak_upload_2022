<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BebanKerja extends Model
{
    protected $table = 'beban_kerja';
    protected $primaryKey = 'id_beban_kerja';

    public function penilaian(){
      return $this->belongsTo('App\Models\Penilaian','penilaian_id');
    }

    public function satuanKerja(){
      return $this->belongsTo('App\Models\SatuanKerja','satuan_kerja_id');
    }

    public function kegiatan(){
      return $this->belongsTo('App\Models\PakMasterKegiatan','master_kegiatan_id');
    }

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }
}
