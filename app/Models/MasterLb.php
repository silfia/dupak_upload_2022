<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterLb extends Model
{
  protected $table = 'master_lb';
  protected $primaryKey = 'id_master_lb';

  public function formasi(){
    return $this->belongsTo('App\Models\MasterType','formasi_id');
  }
  public function creator(){
    return $this->belongsTo('App\Models\Users','creator_id');
  }
}
