<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermenpanKegiatan extends Model
{
	protected $table = 'permenpan_kegiatan';
	protected $primaryKey = 'id_permenpan';

	public function profesi(){
		return $this->belongsTo('App\Models\MasterType','tipe_profesi_id');
	}

	public function creator(){
		return $this->belongsTo('App\Models\Users','creator_id');
	}
}
