<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StandartMaster extends Model
{
    protected $table = 'standart_master';
    protected $primaryKey = 'id_standart';
    public $timestamps = false;

    public function master(){
      return $this->belongsTo('App\Models\DataMaster','master_id');
    }
    public function mstStandar(){
      return $this->belongsTo('App\Models\DataMaster','mst_id');
    }
}
