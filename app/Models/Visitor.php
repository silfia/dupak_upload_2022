<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table = "visitor";
    protected $fillable = ['id_visitor','total','days'];
    protected $primaryKey = "id_visitor";
    public $timestamps = false;
}
