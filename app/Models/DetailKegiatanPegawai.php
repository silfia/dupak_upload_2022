<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailKegiatanPegawai extends Model
{
    protected $table = 'detail_kegiatan_pegawai';
    protected $primaryKey = 'id_detail_kegiatan';

    public function penilaian(){
      return $this->belongsTo('App\Models\Penilaian','penilaian_id');
    }

    public function creator(){
      return $this->belongsTo('App\Models\Users','creator_id');
    }
}
