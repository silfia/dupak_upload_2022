<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'id_pegawai';

    public function golongan(){
      return $this->belongsTo('App\Models\DataMaster','golongan_id');
    }

    public function jabatan(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_id');
    }

    public function satuanKerja(){
      return $this->belongsTo('App\Models\SatuanKerja','satuan_kerja_id');
    }

    public function typeProfesi(){
      return $this->belongsTo('App\Models\MasterType','tipe_profesi_id');
    }

    public function creator(){
      return $this->belongsTo('App\Models\Users','creator_id');
    }

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }

     public static function getJsonPegawai($input)
    {
      
      $table  = 'pegawai';
      $select = "id_pegawai, pegawai.nama as nama, pegawai.no_nip as no_nip, profesi.nama as nama_profesi, satuan_kerja.nama as nama_satuan, pegawai.status_pegawai";      
      $replace_field  = [
          ['old_name' => 'nama', 'new_name' => 'pegawai.nama'],        
          ['old_name' => 'no_nip', 'new_name' => 'pegawai.no_nip'],          
          ['old_name' => 'status_pegawai', 'new_name' => 'pegawai.status_pegawai'],          
          ['old_name' => 'nama_profesi', 'new_name' => 'profesi.nama'],          
          ['old_name' => 'nama_satuan', 'new_name' => 'satuan_kerja.nama'],          
      ];
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];        

      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        if (Auth::getUser()->level_user == 4) {
          $user = Users::find(Auth::id());
          $kolom = 'pegawai.satuan_kerja_id';
          $indikator = '=';
          $satKerId = $user->satuanKerja->id_satuan_kerja;
        }else{
          $kolom = 'pegawai.id_pegawai';
          $indikator = '!=';
          $satKerId = 'null';
        }
        
       return $data->join('master_type as profesi','profesi.id_master_type','pegawai.tipe_profesi_id')
                  ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                  ->join('users', 'users.id', 'pegawai.user_id')
                  ->where('pegawai.bl_state','A')
                  ->where('users.is_banned', '!=', 2)
                  ->where($kolom, $indikator, $satKerId)
                  ->orderBy('pegawai.status_pegawai','DESC')
                  ->orderBy('nama_satuan', 'ASC')
                  ->orderBy('pegawai.nama', 'ASC');
      });
      return $data;
    }
}