<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
  protected $table = 'pengajuan';
  protected $primaryKey = 'id_pengajuan';

  public function user(){
    return $this->belongsTo('App\Models\Users','user_id');
  }

  public function golongan(){
    return $this->belongsTo('App\Models\DataMaster','golongan_id');
  }

  public function jabatan(){
    return $this->belongsTo('App\Models\DataMaster','jabatan_id');
  }

  public function penilai(){
    return $this->belongsTo('App\Models\Users','penilai_id');
  }
}
