<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenerapanPermenpan extends Model
{
	protected $table = 'penerapan_permenpan';
	protected $primaryKey = 'id_penerapan';
	public $timestamps = false;
}
