<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenilaianUnsurLb extends Model
{
  protected $table = 'penilaian_unsur_ld';
  protected $primaryKey = 'id_penilaian_unsur';

  public function kegiatan(){
    return $this->belongsTo('App\Models\PakMasterKegiatan','master_kegiatan_id');
  }

  public function pengajuan(){
    return $this->belongsTo('App\Models\Pengajuan','pengajuan_id');
  }

  public function penilai(){
    return $this->belongsTo('App\Models\Users','penilai_id');
  }

  public function user(){
    return $this->belongsTo('App\Models\Users','user_id');
  }
}
