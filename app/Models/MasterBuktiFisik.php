<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterBuktiFisik extends Model
{
	protected $table = 'master_bukti_fisik';
	protected $primaryKey = 'id_master_bukti';
}
