<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class SatuanKerja extends Model
{
    protected $table = 'satuan_kerja';
    protected $primaryKey = 'id_satuan_kerja';

    public function parents(){
      return $this->belongsTo('App\Models\SatuanKerja','parent_id');
    }

    public function creator(){
      return $this->belongsTo('App\Models\Users','creator_id');
    }

    public function typeSaKer(){
      return $this->belongsTo('App\Models\MasterType','tipe_satker_id');
    }

    public function desa(){
      return $this->belongsTo('App\Models\Desa','kelurahan_id');
    }

    public function kecamatan(){
      return $this->belongsTo('App\Models\Kecamatan','kecamatan_id');
    }

    public function kabupaten(){
      return $this->belongsTo('App\Models\Kabupaten','kabupaten_id');
    }

    public function provinsi(){
      return $this->belongsTo('App\Models\Provinsi','provinsi_id');
    }

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }

    public function pimpinan(){
      return $this->belongsTo('App\Models\Pegawai','pimpinan_id');
    }

     public static function getJsonSatuanKerja($input)
    {
      
      $table  = 'satuan_kerja';
      $select = "id_satuan_kerja, kode, satuan_kerja.nama as nama, alamat, telepon, master_type.nama as nama_type, pegawai.nama as nama_pimpinan";      
      $replace_field  = [
          ['old_name' => 'email', 'new_name' => 'users.email'],        
          ['old_name' => 'no_nip', 'new_name' => 'pegawai.no_nip'],          
          ['old_name' => 'nama', 'new_name' => 'pegawai.nama'],          
          ['old_name' => 'namaPimpinan', 'new_name' => 'nama_pimpinan'],          
          ['old_name' => 'nama_golongan', 'new_name' => 'gol.nama'],          
          ['old_name' => 'nama_jabatan', 'new_name' => 'jab.nama'],          
          ['old_name' => 'nama_satuan', 'new_name' => 'satuan_kerja.nama'],    
          ['old_name' => 'status_pegawai', 'new_name' => 'pegawai.status_pegawai'],      
      ];
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
          return $data->join('master_type','master_type.id_master_type','satuan_kerja.tipe_satker_id')
                  ->leftJoin('pegawai', 'pegawai.id_pegawai', 'satuan_kerja.pimpinan_id')
                  ->where('master_type.modul','SatuanOrganisasi')
                  ->where('satuan_kerja.bl_state','A');
      });
      return $data;
    }
}
