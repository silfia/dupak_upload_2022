<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
  protected $table = 'penilaian';
  protected $primaryKey = 'id_penilaian';

  public function kegiatan(){
    return $this->belongsTo('App\Models\PakMasterKegiatan','master_kegiatan_id');
  }

  public function penilai(){
    return $this->belongsTo('App\Models\Users','penilai_id');
  }

  public function pengajuan(){
    return $this->belongsTo('App\Models\Pengajuan','pengajuan_id');
  }

  public function user(){
    return $this->belongsTo('App\Models\Users','user_id');
  }
}
