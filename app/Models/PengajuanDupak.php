<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengajuanDupak extends Model
{
    protected $table = 'pengajuan_dupak';
    protected $primaryKey = 'id_pengajuan';

    public function masaPengajuan(){
      return $this->belongsTo('App\Models\MasaPengajuan','masa_pengajuan_id');
    }

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }

    public function penilai(){
      return $this->belongsTo('App\Models\Pegawai','penilai_id');
    }

    public function golonganPenilai(){
      return $this->belongsTo('App\Models\DataMaster','golongan_penilai_id');
    }

    public function jabatanPenilai(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_penilai_id');
    }

    public function satuanKerjaPenilai(){
      return $this->belongsTo('App\Models\SatuanKerja','satuan_kerja_penilai_id');
    }

    public function penilai2(){
      return $this->belongsTo('App\Models\Pegawai','penilai2_id');
    }

    public function golonganPenilai2(){
      return $this->belongsTo('App\Models\DataMaster','golongan_penilai2_id');
    }

    public function jabatanPenilai2(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_penilai2_id');
    }

    public function satuanKerjaPenilai2(){
      return $this->belongsTo('App\Models\SatuanKerja','satuan_kerja_penilai2_id');
    }

    public function jabatanPemohonLama(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_lama_id');
    }
    public function jabatanPemohonBaru(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_baru_id');
    }
    public function golonganPemohonLama(){
      return $this->belongsTo('App\Models\DataMaster','golongan_lama_id');
    }
    public function golonganPemohonBaru(){
      return $this->belongsTo('App\Models\DataMaster','golongan_baru_id');
    }

    public function kepala(){
      return $this->belongsTo('App\Models\Pegawai','kepala_id');
    }
    public function golonganKepala(){
      return $this->belongsTo('App\Models\DataMaster','golongan_kepala_id');
    }
    public function jabatanKepala(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_kepala_id');
    }
    public function satuanKerjaKepala(){
      return $this->belongsTo('App\Models\SatuanKerja','satuan_kerja_kepala_id');
    }


}
