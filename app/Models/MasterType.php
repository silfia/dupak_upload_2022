<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterType extends Model
{
  protected $table = 'master_type';
  protected $primaryKey = 'id_master_type';

  public function creator(){
    return $this->belongsTo('App\Models\Users','creator_id');
  }
}
