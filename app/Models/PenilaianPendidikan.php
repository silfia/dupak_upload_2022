<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenilaianPendidikan extends Model
{
    protected $table = 'penilaian_pendidikan';
    protected $primaryKey = 'id_penilaian_pendidikan';

    public function penilai(){
      return $this->belongsTo('App\Models\Users','penilai_id');
    }

    public function pengajuan(){
      return $this->belongsTo('App\Models\Pengajuan','pengajuan_id');
    }

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }
}
