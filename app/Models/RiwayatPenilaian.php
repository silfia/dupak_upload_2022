<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiwayatPenilaian extends Model
{
  protected $table = 'riwayat_penilaian_dupak';
  protected $primaryKey = 'id_riwayat';
  public $timestamps = false; 
}
