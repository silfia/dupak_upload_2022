<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Http\Libraries\Datagrid;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Permenpan extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'permenpan_kegiatan';    
    protected $primaryKey = 'id_permenpan';

    public static function getJsonPermenpan($input)
    {
      $table  = 'permenpan_kegiatan';
      $select = '*';
      $replace_field  = [
        // ['old_name' => '', 'new_name' => ''],
      ];
      $param = [
        'input'         => $input->all(),
        'table'         => $table,
        'select'        => $select,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data) use ($input){
        return $data->join('master_type','master_type.id_master_type','permenpan_kegiatan.tipe_profesi_id')
                    ->where('permenpan_kegiatan.tipe_profesi_id',$input['id_master_type'])
                    ->where('master_type.modul','StatusProfesi')
                    ->where('permenpan_kegiatan.bl_state','A')
                    ->orderBy('permenpan_kegiatan.id_permenpan','ASC');
      });      
      return $data;
    }      
}