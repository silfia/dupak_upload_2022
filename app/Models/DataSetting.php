<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataSetting extends Model
{
    protected $table = 'data_setting';
    protected $primaryKey = 'id_setting';
}
