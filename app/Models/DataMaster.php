<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataMaster extends Model
{
    protected $table = 'data_master';
    protected $primaryKey = 'id_master';

    public function typeProfesi(){
      return $this->belongsTo('App\Models\MasterType','tipe_profesi_id');
    }
    public function creator(){
      return $this->belongsTo('App\Models\Users','creator_id');
    }
}
