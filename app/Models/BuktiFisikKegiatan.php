<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuktiFisikKegiatan extends Model
{
	protected $table = 'bukti_fisik_kegiatan';
	protected $primaryKey = 'id_bukti_fisik';

	public function kegiatan(){
		return $this->belongsTo('App\Models\PakMasterKegiatan','pak_master_kegiatan_id');
	}

	public function masterBukti(){
		return $this->belongsTo('App\Models\MasterBuktiFisik','master_bukti_fisik_id');
	}
}
