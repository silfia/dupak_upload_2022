<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CekNotif extends Model
{
    protected $table = 'cek_notif';
    protected $primaryKey = 'id_cek_notif';
}
