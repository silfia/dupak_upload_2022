<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratPernyataan extends Model
{
    protected $table = 'surat_pernyataan';
    protected $primaryKey = 'id_surat_pernyataan';

    public function pengajuanDupak(){
      return $this->belongsTo('App\Models\PengajuanDupak','pengajuan_id');
    }

    public function unsurPak(){
      return $this->belongsTo('App\Models\MasterType','unsur_pak_id');
    }
}
