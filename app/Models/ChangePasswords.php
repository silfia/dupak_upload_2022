<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangePasswords extends Model
{
    protected $table = 'change_passwords';
    protected $primaryKey = 'id_change';
    public $timestamps = false;

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }
}
