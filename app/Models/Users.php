<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Http\Libraries\Datagrid;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'users';
    protected $fillable = [
      'id','username','email','password','name','gender','address','phone','photo','level_user','is_banned','last_login','remember_token','created_at','updated_at',
    ];
    protected $primaryKey = 'id';
    protected $hidden = [
      'password', 'remember_token',
    ];

    public function pegawai(){
    	return $this->hasOne('App\Models\Pegawai','user_id');
    }

    public function satuanKerja(){
    	return $this->hasOne('App\Models\SatuanKerja','user_id');
    }

    public function timPenilai(){
    	return $this->hasOne('App\Models\TimPenilai','user_id');
    }

    public static function getJsonUsersAdmin($input)
    {
      $table  = 'users';
      $select = '*';
      $replace_field  = [
        // ['old_name' => '', 'new_name' => ''],
      ];
      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->where('users.level_user',1);
      });
      
      return $data;
    }

    public static function getJsonUsersPenilai($input)
    {
      
      $table  = 'users';
      $select = "users.id as id, users.email as email, pegawai.no_nip as no_nip, pegawai.nama as nama, gol.nama as nama_golongan, jab.nama as nama_jabatan, satuan_kerja.nama as nama_satuan, users.is_banned as is_banned, users.phone as phone_user";      
      $replace_field  = [
          ['old_name' => 'email', 'new_name' => 'users.email'],        
          ['old_name' => 'nama', 'new_name' => 'pegawai.nama'],          
          ['old_name' => 'nama_golongan', 'new_name' => 'gol.nama'],          
          ['old_name' => 'nama_jabatan', 'new_name' => 'jab.nama'],          
          ['old_name' => 'nama_satuan', 'new_name' => 'satuan_kerja.nama'],          
          ['old_name' => 'phone_user', 'new_name' => 'users.phone'],          
      ];
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
          return $data->join('pegawai','pegawai.user_id','users.id')
                      ->leftJoin('data_master as gol','gol.id_master','pegawai.golongan_id')
                      ->leftJoin('data_master as jab','jab.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('pegawai.status_penilai','Ya')
                      ->where('users.level_user','5')
                      ->where('users.bl_state','A')
                      ->orderBy('users.created_at','DESC');
      });
      return $data;
    }

      public static function getJsonSatuanKerja($input)
    {
      
      $table  = 'users';
      $select = "users.id as id, satuan_kerja.kode as kode, users.email as email, users.name as name, users.phone as phone, users.gender as gender, master_type.nama as nama_type, users.is_banned as is_banned, pegawai.nama as nama_pimpinan";      
      
      $replace_field  = [
          ['old_name' => 'email', 'new_name' => 'users.email'],        
          ['old_name' => 'name', 'new_name' => 'users.name'],          
          ['old_name' => 'phone', 'new_name' => 'users.phone'],          
          ['old_name' => 'gender', 'new_name' => 'users.gender'],          
          ['old_name' => 'phone', 'new_name' => 'users.phone'],          
          ['old_name' => 'namaPimpinan', 'new_name' => 'pegawai.nama'],          
      ];
      
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
          return $data->join('satuan_kerja','satuan_kerja.user_id','users.id')
                  ->join('master_type','master_type.id_master_type','satuan_kerja.tipe_satker_id')
                  ->leftJoin('pegawai', 'pegawai.id_pegawai', 'satuan_kerja.pimpinan_id')
                  ->where('level_user','4')
                  ->where('users.bl_state','A')
                  ->orderBy('users.created_at','DESC');;
      });
      return $data;
    }

    public static function getJsonPegawai($input)
    {
      
      $table  = 'users';
      $select = "users.id as id, users.email as email, pegawai.no_nip as no_nip, pegawai.nama as nama, gol.nama as nama_golongan, jab.nama as nama_jabatan, satuan_kerja.nama as nama_satuan, users.is_banned as is_banned, pegawai.status_pegawai";      
      $replace_field  = [
          ['old_name' => 'email', 'new_name' => 'users.email'],        
          ['old_name' => 'no_nip', 'new_name' => 'pegawai.no_nip'],          
          ['old_name' => 'nama', 'new_name' => 'pegawai.nama'],          
          ['old_name' => 'nama_golongan', 'new_name' => 'gol.nama'],          
          ['old_name' => 'nama_jabatan', 'new_name' => 'jab.nama'],          
          ['old_name' => 'nama_satuan', 'new_name' => 'satuan_kerja.nama'],    
          ['old_name' => 'status_pegawai', 'new_name' => 'pegawai.status_pegawai'],
          ['old_name' => 'Status', 'new_name' => 'users.is_banned'], 
      ];
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
          return $data->join('pegawai','pegawai.user_id','users.id')
                  ->leftJoin('data_master as gol','gol.id_master','pegawai.golongan_id')
                  ->leftJoin('data_master as jab','jab.id_master','pegawai.jabatan_id')
                  ->leftJoin('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                  ->where('users.level_user','5')
                  ->where('users.bl_state','A')
                  ->orderBy('users.created_at','DESC');
                  // ->orderBy('pegawai.status_pegawai','DESC')
                  // ->orderBy('nama_satuan', 'ASC')
                  // ->orderBy('pegawai.nama', 'ASC');
      });
      return $data;
    }

    public static function getJsonUsersKetuaPenilai($input)
    {
      
      $table  = 'users';
      $select = "users.id as id, users.email as email, pegawai.no_nip as no_nip, pegawai.nama as nama, gol.nama as nama_golongan, jab.nama as nama_jabatan, satuan_kerja.nama as nama_satuan, users.is_banned as is_banned, users.phone as phone_user";      
      $replace_field  = [
          ['old_name' => 'email', 'new_name' => 'users.email'],        
          ['old_name' => 'nama', 'new_name' => 'pegawai.nama'],          
          ['old_name' => 'nama_golongan', 'new_name' => 'gol.nama'],          
          ['old_name' => 'nama_jabatan', 'new_name' => 'jab.nama'],          
          ['old_name' => 'nama_satuan', 'new_name' => 'satuan_kerja.nama'],          
          ['old_name' => 'phone_user', 'new_name' => 'users.phone'],          
      ];
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
          return $data->join('pegawai','pegawai.user_id','users.id')
                      ->leftJoin('data_master as gol','gol.id_master','pegawai.golongan_id')
                      ->leftJoin('data_master as jab','jab.id_master','pegawai.jabatan_id')
                      ->join('satuan_kerja','satuan_kerja.id_satuan_kerja','pegawai.satuan_kerja_id')
                      ->where('pegawai.status_penilai','Ya')
                      ->where('users.level_user','5')
                      ->where('users.bl_state','A')
                      ->where('users.is_ketua_penilai','Y')
                      ->orderBy('users.created_at','DESC');
      });
      return $data;
    }
}