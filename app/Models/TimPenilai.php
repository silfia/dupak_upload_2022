<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimPenilai extends Model
{
    protected $table = 'tim_penilai';
    protected $primaryKey = 'id_penilai';

    public function satuanKerja(){
      return $this->belongsTo('App\Models\SatuanKerja','satuan_kerja_id');
    }

    public function golongan(){
      return $this->belongsTo('App\Models\DataMaster','golongan_id');
    }

    public function jabatan(){
      return $this->belongsTo('App\Models\DataMaster','jabatan_id');
    }

    public function typeProfesi(){
      return $this->belongsTo('App\Models\MasterType','tipe_profesi_id');
    }

    public function creator(){
      return $this->belongsTo('App\Models\Users','creator_id');
    }

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }
}
