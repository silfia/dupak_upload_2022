<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MasaPengajuan extends Model
{
  protected $table = 'masa_pengajuan';
  protected $primaryKey = 'id_masa_pengajuan';
  public $timestamps = false;

  public function user(){
    return $this->belongsTo('App\Models\Users','user_id');
  }
}
