<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class ActivitySystems extends Model
{
    protected $table = 'activity_system';
    protected $primaryKey = 'id_activity';
    public $timestamps = false;

    public function user(){
      return $this->belongsTo('App\Models\Users','user_id');
    }

     public static function getJsonActivitySystems($input)
    {
      
      $table  = 'activity_system';
      $select = "*";      
      $replace_field  = [
          ['old_name' => '', 'new_name' => ''],         
      ];
      $param = [
          'input'         => $input->all(),
          'select'        => $select,
          'table'         => $table,
          'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
          return $data->where('is_butir_kegiatan','Y');
      });
      return $data;
    }
}
