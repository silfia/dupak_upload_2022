<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PakMasterKegiatan extends Model
{
  protected $table = 'pak_master_kegiatan';
  protected $primaryKey = 'id_pak_master';

  public function parentKegiatan(){
    return $this->belongsTo('App\Models\PakMasterKegiatan','parent_id');
  }

  public function formasi(){
    return $this->belongsTo('App\Models\MasterType','formasi_id');
  }

  public function jabatan(){
    return $this->belongsTo('App\Models\DataMaster','jabatan_id');
  }

  public function unsur(){
    return $this->belongsTo('App\Models\MasterType','unsur_pak_id');
  }

  public function creator(){
    return $this->belongsTo('App\Models\Users','creator_id');
  }

}
