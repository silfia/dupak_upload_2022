<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UraianSuratPernyataan extends Model
{
    protected $table = 'uraian_surat_pernyataan';
    protected $primaryKey = 'id_uraian_sp';

    public function suratPernyataan(){
      return $this->belongsTo('App\Models\SuratPernyataan','surat_pernyataan_id');
    }

    public function pakMasterKegiatan(){
      return $this->belongsTo('App\Models\PakMasterKegiatan','master_kegiatan_id');
    }
}
