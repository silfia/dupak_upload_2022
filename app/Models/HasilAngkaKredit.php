<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HasilAngkaKredit extends Model
{
  protected $table = 'hasil_penetapan_angka_kredit';
  protected $primaryKey = 'id_pak';

  public function pengajuanDupak(){
    return $this->belongsTo('App\Models\PengajuanDupak','pengajuan_id');
  }
}
