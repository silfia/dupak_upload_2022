<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/maintenance', function (){
    return Artisan::call('down');
});

Route::get('/clear', function() {
   Artisan::call('cache:clear');
   Artisan::call('config:clear');
   Artisan::call('config:cache');
   Artisan::call('view:clear');
   return "Cleared!";
});

// Route::get('/', function () { return view('welcome'); });

Route::get('/', function(){ return redirect()->route('dashboard'); });
Route::get('/login',          'AuthController@login')->name('login');
Route::post('/doLogin',       'AuthController@doLogin')->name('doLogin');
Route::get('logout',          'AuthController@logout')->name('logout');
Route::get('/register',       'AuthController@register')->name('register');
Route::post('/doRegistrasi',  'AuthController@doRegistrasi')->name('doRegistrasi');
Route::post('/getEmail',      'OtherController@getEmail')->name('getEmail');
Route::post('/getNip',        'OtherController@getNip')->name('getNip');
Route::post('/getKodeSatker', 'OtherController@getKodeSatker')->name('getKodeSatker');
Route::post('/getjabatan',    'OtherController@getjabatan')->name('getjabatan');
Route::post('/cekWaktu',      'OtherController@cekWaktu')->name('cekWaktu');
Route::post('/cekPermenpan',  'OtherController@cekPermenpan')->name('cekPermenpan');
Route::get('/ManualGuide',       'OtherController@ManualGuide')->name('ManualGuide');

Route::group(['middleware' => 'auth'], function() {
  Route::get('/home',           'DashboardController@main')->name('dashboard');
  Route::post('getGrafikVisit', 'DashboardController@getGrafikVisit')->name('getGrafikVisit');
  Route::post('/getKabupaten',  'OtherController@getKabupaten')->name('getKabupaten');
  Route::post('/getKecamatan',  'OtherController@getKecamatan')->name('getKecamatan');
  Route::post('/getDesa',       'OtherController@getDesa')->name('getDesa');
  Route::post('/getPegawai',    'OtherController@getPegawai')->name('getPegawai');

  Route::group(array('prefix'=>'profile'), function(){
    Route::get('/',         'ProfileController@main')->name('profile');
    Route::post('/form',    'ProfileController@form')->name('formProfile');
    Route::post('/save',    'ProfileController@save')->name('saveProfile');
    Route::get('/formkey',  'ProfileController@formkey')->name('changePassword');
    Route::post('/savekey', 'ProfileController@saveNewPass')->name('saveChangePassword');
  });

  Route::group(array('prefix'=>'unsur-pendidikan'), function(){
    Route::get('/','KegiatanPendidikanController@main')->name('unsurPendidikan');
    Route::post('/loadDataUP','KegiatanPendidikanController@loadDataUP')->name('loadDataUP');
    Route::post('/formUP','KegiatanPendidikanController@formUP')->name('formUP');
    Route::post('/saveDataUP','KegiatanPendidikanController@saveDataUP')->name('saveDataUP');
    Route::post('/removeData','KegiatanPendidikanController@removeData')->name('removeData');
    Route::post('/previewBuktiFisik','KegiatanPendidikanController@previewBuktiFisik')->name('previewBuktiFisik');
  });


  Route::group(array('prefix'=>'pengajuan_dupak'), function(){ // gak dipakek
    // Route::get('/', 'PengajuanController@main')->name('PengajuanDupak');
    // Route::post('/loadDataPengajuan','PengajuanController@loadDataPengajuan')->name('loadDataPengajuan');
    Route::post('/formPengajuan','PengajuanController@formPengajuan')->name('formPengajuan');
    Route::post('/saveDataPj','PengajuanController@saveDataPj')->name('saveDataPj');
  });

  Route::group(array('prefix'=>'penilaian-dupak'), function(){
  });

  // =============== DIGUNAKAN =================================
  Route::group(array('prefix'=>'penilaian-dupak'), function(){ // Perlu Cek Ulang
    Route::get('/',                 'PenilaianDupakController@main')->name('penilaianDupak');
    Route::post('/loadData',        'PenilaianDupakController@loadData')->name('loadPemohon');
    Route::post('/formPenilaian',   'PenilaianDupakController@formPenilaian')->name('formPenilaian');
    Route::post('/loadTahun',       'PenilaianDupakController@loadTahun')->name('loadPenilaianTahun');
    Route::get('/getNilaiPemohon',  'PenilaianDupakController@getNilaiPemohon')->name('getNilaiPemohon');


    Route::post('/sesuaiTahunan',   'PenilaianDupakController@sesuaiTahunan')->name('sesuaiPenilaianTahunan');
    Route::post('/loadBulan',       'PenilaianDupakController@loadBulan')->name('loadPenilaianBulan');
    Route::post('/loadHarian',      'PenilaianDupakController@loadHarian')->name('loadPenilaianHarian');
    Route::post('/detailKegiatan',      'PenilaianDupakController@detailKegiatan')->name('detailKegiatanPenilaianHarian');
    Route::post('/accNilai','PenilaianDupakController@accNilai')->name('accNilaiHarian');
    Route::post('/formGantiNilai','PenilaianDupakController@formGantiNilai')->name('formGantiNilai');
    Route::post('/gantiNilai','PenilaianDupakController@gantiNilai')->name('gantiNilai');
    Route::post('/simpanPenilai','PenilaianDupakController@simpanPenilai')->name('simpanPenilai');

    Route::post('/gantiNilaiEdit','PenilaianDupakController@gantiNilaiEdit')->name('gantiNilaiEdit');    
    Route::post('/formPerubahanData',       'PenilaianDupakController@formPerubahanData')->name('formPerubahanData');


  });

  Route::group(array('prefix'=>'riwayat-penilaian'), function(){
    Route::get('/',                 'RiwayatPenilaianDupakController@main')->name('RiwayatpenilaianDupak');
    Route::post('/loadData',        'RiwayatPenilaianDupakController@loadData')->name('loadRiwayat');
    Route::post('/detailRiwayat',   'RiwayatPenilaianDupakController@detailRiwayat')->name('detailRiwayat');    
    Route::post('/detailRiwayatTahunan',   'RiwayatPenilaianDupakController@detailRiwayatTahunan')->name('detailRiwayatTahunan');    
  });

  Route::group(array('prefix'=>'pantau-kegiatan'), function(){
    Route::get('/',               'PantauKegiatanPegawaiController@main')->name('pantauKegiatan');
    Route::post('/loadKegiatan',  'PantauKegiatanPegawaiController@loadKegiatan')->name('loadPantauKegiatan');
    Route::get('/getNilai',       'PantauKegiatanPegawaiController@getNilaiPantau')->name('getNilaiPantau');
    Route::post('/accSatKer',     'PantauKegiatanPegawaiController@accSatKer')->name('accSatKerNilaiPantau');
  });

  Route::group(array('prefix'=>'kegitan-pegawai'), function(){
    Route::get('/old',                'KegiatanPegawaiBaruController@main')->name('kegiatanHarianOld');
    Route::post('/getIdLogin',        'KegiatanPegawaiBaruController@getIdLogin')->name('getIdLogin');
    
    Route::get('/',                   'KegiatanPegawaiBaruController@mainNew')->name('kegiatanHarian');
    Route::post('/loadHarian',        'KegiatanPegawaiBaruController@loadHarian')->name('loadHarianPegawai');
    Route::get('/getNilaiHarian',     'KegiatanPegawaiBaruController@getNilaiHarian')->name('getNilaiHarianPegawai');
    Route::post('/formJumlahDetail',  'KegiatanPegawaiBaruController@formJumlahDetail')->name('formJumlahDetailKegiatan');
    Route::post('/formAddDetail',     'KegiatanPegawaiBaruController@formAddDetail')->name('formAddDetailKegiatan');
    Route::post('/addKegiatanNonPns', 'KegiatanPegawaiBaruController@addKegiatanNonPns')->name('addKegiatanNonPns');
    Route::post('/addDetail',         'KegiatanPegawaiBaruController@addDetail')->name('addDetailKegiatan');
    Route::post('/addDetailNew',         'KegiatanPegawaiBaruController@addDetailNew')->name('addDetailKegiatanNew');
    Route::post('/viewDetail',        'KegiatanPegawaiBaruController@viewDetail')->name('viewDetailKegiatan');
    Route::post('/editData',        'KegiatanPegawaiBaruController@editData')->name('editDataKegiatan');
    Route::post('/addEdit',         'KegiatanPegawaiBaruController@addEdit')->name('addEditKegiatan');
    Route::post('/simpan_kegiatan_master',  'KegiatanPegawaiBaruController@simpan_kegiatan_master')->name('simpan_kegiatan_master');
    Route::post('/viewDetailEdit',        'KegiatanPegawaiBaruController@viewDetailEdit')->name('viewDetailEditKegiatan');
    Route::post('/hapusDetail',        'KegiatanPegawaiBaruController@hapusDetail')->name('hapusDetailKegiatan');
  });

  Route::group(array('prefix'=>'kegitan-pegawai-download'), function(){
    Route::get('/',                   'KegiatanPegawaiDownloadController@mainNew')->name('kegiatanHarianDownload');
  });

  Route::group(array('prefix'=>'kegitan-pegawai-gabung'), function(){
    Route::get('/',        'KegiatanPegawaiGabungController@main')->name('kegiatanHarianGabung');
    Route::post('/loadHarian', 'KegiatanPegawaiGabungController@loadHarian')->name('loadKegiatanHarianGabung');
    Route::get('/getNilaiHarian',     'KegiatanPegawaiGabungController@getNilaiHarian')->name('getNilaiHarianGabung');
  });
  
  Route::group(array('prefix'=>'rekap-kegiatan'), function(){
    Route::get('/',               'RekapKegiatanPegawaiController@main')->name('rekapKegiatan');
    Route::post('/loadRekap', 'RekapKegiatanPegawaiController@loadRekap')->name('loadRekapKegiatan');
    Route::post('/getNilai', 'RekapKegiatanPegawaiController@getNilai')->name('getNilaiRekapKegiatan');
  });

  Route::group(array('prefix'=>'pengajuan-dupak'), function(){
    Route::get('/form',                 'PengajuanDupakController@form')->name('formPengajuanDupak');
    Route::post('/savePengajuan',       'PengajuanDupakController@savePengajuan')->name('savePengajuanDupak');
    Route::get('/formCetakBerkas',      'PengajuanDupakController@formCetakBerkas')->name('formCetakBerkas');
    Route::post('/getDataSpmk',         'PengajuanDupakController@getDataSpmk')->name('getDataSpmk');
    Route::get('/getNilaiSpmk',         'PengajuanDupakController@getNilaiSpmk')->name('getNilaiSpmk');
    Route::post('/getJumlahNilaiSpmk',  'PengajuanDupakController@getJumlahNilaiSpmk')->name('getJumlahNilaiSpmk');
    Route::post('/getDataRekapTahun', 'PengajuanDupakController@getDataRekapTahun')->name('getDataRekapTahun');
    Route::post('/getNilaiRekapTahun', 'PengajuanDupakController@getNilaiRekapTahun')->name('getNilaiRekapTahun');
    Route::get('/',                     'PengajuanDupakController@main')->name('pengajuanDupak');
    Route::post('/loadDataMasa',        'PengajuanDupakController@loadDataMasa')->name('loadMasaPengajuan');
    Route::post('/listPengajuan',       'PengajuanDupakController@listPengajuan')->name('listPengajuanDupak');
    Route::post('/loadDataList',        'PengajuanDupakController@loadDataList')->name('loadListPengajuan');
    Route::post('/formPenilai',         'PengajuanDupakController@formPenilai')->name('formPenilaiPengajuan');
    Route::post('/detailPenilai',       'PengajuanDupakController@detailPenilai')->name('detailPenilaiPengajuan');
    Route::post('/simpanPenilai',       'PengajuanDupakController@simpanPenilai')->name('simpanPenilaiPengajuan');
    Route::post('/cekSkpakSebelum',     'PengajuanDupakController@cekSkpakSebelum')->name('cekSkpakSebelum');
    Route::post('/cekSkpakSebelumEdit',     'PengajuanDupakController@cekSkpakSebelumEdit')->name('cekSkpakSebelumEdit');
    
    Route::post('/saveSkpakSebelum',    'PengajuanDupakController@saveSkpakSebelum')->name('saveSkpakSebelum');
    Route::get('/getTelaah',            'PengajuanDupakController@getTelaah')->name('getTelaahPengajuan');

    Route::post('/formNoSKPAK', 'PengajuanDupakController@formNoSKPAK')->name('formNoSKPAK');
    Route::post('/saveNoSKPAK', 'PengajuanDupakController@saveNoSKPAK')->name('saveNoSKPAK');

    Route::post('/cekSKPAKPengajuan', 'PengajuanDupakController@cekSKPAKPengajuan')->name('cekSKPAKPengajuan');
    Route::get('/getSKPAKPengajuan', 'PengajuanDupakController@getSKPAKPengajuan')->name('getSKPAKPengajuan');

    // gak tau di pakek atau gak
    Route::post('/getCetakPernyataanPelayanan', 'PengajuanDupakController@getCetakPernyataanPelayanan')->name('getCetakPernyataanPelayanan');

  });

  Route::group(array('prefix'=>'waktu_pengajuan_dupak'), function(){
    Route::get('/',           'MasaPengajuanDupakController@main')->name('masaPengajuanDupak');
    Route::post('/loadData',  'MasaPengajuanDupakController@loadData')->name('loadMasaPengajuanDupak');
    Route::post('/form',      'MasaPengajuanDupakController@form')->name('formMasaPengajuanDupak');
    Route::post('/save',      'MasaPengajuanDupakController@save')->name('saveMasaPengajuanDupak');
    Route::post('/remove',    'MasaPengajuanDupakController@removeData')->name('removeMasaPengajuanDupak');
  });

  Route::group(array('prefix'=>'pengguna'), function(){
    Route::group(array('prefix'=>'admin'), function(){
      Route::get('/',               'Users\AdminController@main')->name('userAdmin');
      Route::post('/loadData',      'Users\AdminController@loadData')->name('loadUserAdmin');
      Route::post('/form',          'Users\AdminController@form')->name('formUserAdmin');
      Route::post('/save',          'Users\AdminController@saveData')->name('saveUserAdmin');
      Route::post('/detail',        'Users\AdminController@detailData')->name('detailUserAdmin');
      Route::post('/resetPassword', 'Users\AdminController@resetPassword')->name('resetUserAdmin');
      Route::post('/bannedAccount', 'Users\AdminController@bannedAccount')->name('bannedUserAdmin');
      Route::post('/activeAccount', 'Users\AdminController@activeAccount')->name('activeUserAdmin');
      Route::post('/removeData',    'Users\AdminController@removeData')->name('removeUserAdmin');
    });
    Route::group(array('prefix'=>'penilai'), function(){
      Route::get('/',               'Users\PenilaiController@main')->name('userPenilai');
      Route::post('/loadData',      'Users\PenilaiController@loadData')->name('loadUserPenilai');
      Route::post('/form',          'Users\PenilaiController@form')->name('formUserPenilai');
      Route::post('/save',          'Users\PenilaiController@saveData')->name('saveUserPenilai');
      Route::post('/resetPassword', 'Users\PenilaiController@resetPassword')->name('resetUserPenilai');
      Route::post('/bannedAccount', 'Users\PenilaiController@bannedAccount')->name('bannedUserPenilai');
      Route::post('/activeAccount', 'Users\PenilaiController@activeAccount')->name('activeUserPenilai');
      Route::post('/removeData',    'Users\PenilaiController@removeData')->name('removeUserPenilai');
    });

    Route::group(array('prefix'=>'ketua-penilai'), function(){
      Route::get('/',               'Users\KetuaPenilaiController@main')->name('userKetuaPenilai');
      Route::post('/loadData',      'Users\KetuaPenilaiController@loadData')->name('loadUserKetuaPenilai');
      Route::post('/form',          'Users\KetuaPenilaiController@form')->name('formUserKetuaPenilai');
      Route::post('/save',          'Users\KetuaPenilaiController@saveData')->name('saveUserKetuaPenilai');
      Route::post('/resetPassword', 'Users\KetuaPenilaiController@resetPassword')->name('resetUserKetuaPenilai');
      Route::post('/bannedAccount', 'Users\KetuaPenilaiController@bannedAccount')->name('bannedUserKetuaPenilai');
      Route::post('/activeAccount', 'Users\KetuaPenilaiController@activeAccount')->name('activeUserKetuaPenilai');
      Route::post('/removeData',    'Users\KetuaPenilaiController@removeData')->name('removeUserKetuaPenilai');
    });
    
    Route::group(array('prefix'=>'satuan-kerja'), function(){
      Route::get('/',               'Users\SatuanKerjaController@main')->name('userSatuanKerja');
      Route::post('/loadData',      'Users\SatuanKerjaController@loadData')->name('loadUserSatuanKerja');
      Route::post('/form',          'Users\SatuanKerjaController@form')->name('formUserSatuanKerja');
      Route::post('/save',          'Users\SatuanKerjaController@saveData')->name('saveUserSatuanKerja');
      Route::post('/detail',        'Users\SatuanKerjaController@detailData')->name('detailUserSatuanKerja');
      Route::post('/resetPassword', 'Users\SatuanKerjaController@resetPassword')->name('resetUserSatuanKerja');
      Route::post('/bannedAccount', 'Users\SatuanKerjaController@bannedAccount')->name('bannedUserSatuanKerja');
      Route::post('/activeAccount', 'Users\SatuanKerjaController@activeAccount')->name('activeUserSatuanKerja');
      Route::post('/removeData',    'Users\SatuanKerjaController@removeData')->name('removeUserSatuanKerja');
    });
    Route::group(array('prefix'=>'pegawai'), function(){
      Route::get('/',               'Users\PegawaiController@main')->name('userPegawai');
      Route::post('/loadData',      'Users\PegawaiController@loadData')->name('loadUserPegawai');
      Route::post('/form',          'Users\PegawaiController@form')->name('formUserPegawai');
      Route::post('/save',          'Users\PegawaiController@saveData')->name('saveUserPegawai');
      Route::post('/detail',        'Users\PegawaiController@detailData')->name('detailUserPegawai');
      Route::post('/resetPassword', 'Users\PegawaiController@resetPassword')->name('resetUserPegawai');
      Route::post('/bannedAccount', 'Users\PegawaiController@bannedAccount')->name('bannedUserPegawai');
      Route::post('/activeAccount', 'Users\PegawaiController@activeAccount')->name('activeUserPegawai');
      Route::post('/removeData',    'Users\PegawaiController@removeData')->name('removeUserPegawai');
    });
  });

    Route::group(array('prefix'=>'aktifitas-butir-kegiatan'), function(){
      Route::get('/',               'LogButirKegiatanController@main')->name('LogButirKegiatan');
      Route::post('/loadData',      'LogButirKegiatanController@loadData')->name('loadUserLogButirKegiatan');
      Route::post('/detail',        'LogButirKegiatanController@detailData')->name('detailLogButirKegiatan');
    });

  Route::group(array('prefix'=>'master'), function(){
    Route::group(array('prefix'=>'setting'), function(){
      Route::get('/',       'Master\SettingController@main')->name('setting');
      Route::post('/save',  'Master\SettingController@saveSetting')->name('saveSetting');
    });
    Route::group(array('prefix'=>'profesi'), function(){
      Route::get('/',           'Master\ProfesiController@main')->name('profesi');
      Route::post('/loadData',  'Master\ProfesiController@loadData')->name('loadProfesi');
      Route::post('/form',      'Master\ProfesiController@form')->name('formProfesi');
      Route::post('/save',      'Master\ProfesiController@saveData')->name('saveProfesi');
      Route::post('/remove',    'Master\ProfesiController@removeData')->name('removeProfesi');
    });
    Route::group(array('prefix'=>'golongan'), function(){
      Route::get('/',           'Master\GolonganController@main')->name('golongan');
      Route::post('/loadData',  'Master\GolonganController@loadData')->name('loadGolongan');
      Route::post('/form',      'Master\GolonganController@form')->name('formGolongan');
      Route::post('/save',      'Master\GolonganController@saveData')->name('saveGolongan');
      Route::post('/remove',    'Master\GolonganController@removeData')->name('removeGolongan');
      Route::group(array('prefix'=>'standar'), function(){
        Route::post('/',          'Master\GolonganStandarController@main')->name('golonganStandar');
        Route::post('/loadData',  'Master\GolonganStandarController@loadData')->name('loadGolonganStandar');
        Route::post('/form',      'Master\GolonganStandarController@form')->name('formGolonganStandar');
        Route::post('/save',      'Master\GolonganStandarController@save')->name('saveGolonganStandar');
        Route::post('/remove',    'Master\GolonganStandarController@removeData')->name('removeGolonganStandar');
      });
    });
    Route::group(array('prefix'=>'jabatan'), function(){
      Route::get('/',           'Master\JabatanController@main')->name('jabatan');
      Route::post('/loadData',  'Master\JabatanController@loadData')->name('loadJabatan');
      Route::post('/form',      'Master\JabatanController@form')->name('formJabatan');
      Route::post('/save',      'Master\JabatanController@saveData')->name('saveJabatan');
      Route::post('/remove',    'Master\JabatanController@removeData')->name('removeJabatan');
      Route::group(array('prefix'=>'standar'), function(){
        Route::post('/',          'Master\JabatanStandarController@main')->name('jabatanStandar');
        Route::post('/loadData',  'Master\JabatanStandarController@loadData')->name('loadJabatanStandar');
        Route::post('/form',      'Master\JabatanStandarController@form')->name('formJabatanStandar');
        Route::post('/save',      'Master\JabatanStandarController@save')->name('saveJabatanStandar');
        Route::post('/remove',    'Master\JabatanStandarController@removeData')->name('removeJabatanStandar');
      });
    });

     Route::group(array('prefix'=>'permenpan'), function(){
      Route::get('/',           'Master\PermenpanKegiatanController@main')->name('permenpan');
      Route::post('/loadData',  'Master\PermenpanKegiatanController@loadData')->name('loadPermenpan');
      Route::post('/form',      'Master\PermenpanKegiatanController@form')->name('formPermenpan');
      Route::post('/save',      'Master\PermenpanKegiatanController@saveData')->name('savePermenpan');
      Route::post('/remove',    'Master\PermenpanKegiatanController@removeData')->name('removePermenpan');
      Route::post('/activePermenpan', 'Master\PermenpanKegiatanController@activePermenpan')->name('activePermenpan');      
    });

    Route::group(array('prefix'=>'type-satuan-kerja'), function(){
      Route::get('/',           'Master\typeSaKerController@main')->name('typeSaKer');
      Route::post('/loadData',  'Master\typeSaKerController@loadData')->name('loadTypeSaKer');
      Route::post('/form',      'Master\typeSaKerController@form')->name('formTypeSaKer');
      Route::post('/save',      'Master\typeSaKerController@saveData')->name('saveTypeSaKer');
      Route::post('/remove',    'Master\typeSaKerController@removeData')->name('removeTypeSaKer');
    });
    Route::group(array('prefix'=>'satuan-kerja'), function(){
      Route::get('/',           'Master\SatuanKerjaController@main')->name('satuanKerja');
      Route::post('/loadData',  'Master\SatuanKerjaController@loadData')->name('loadSatuanKerja');
      Route::post('/form',      'Master\SatuanKerjaController@form')->name('formSatuanKerja');
      Route::post('/save',      'Master\SatuanKerjaController@saveData')->name('saveSatuanKerja');
      Route::post('/detail',    'Master\SatuanKerjaController@detailData')->name('detailSatuanKerja');
      Route::post('/remove',    'Master\SatuanKerjaController@removeData')->name('removeSatuanKerja');
    });
    Route::group(array('prefix'=>'pegawai'), function(){
      Route::get('/',           'Master\PegawaiController@main')->name('pegawai');
      Route::post('/loadData',  'Master\PegawaiController@loadData')->name('loadPegawai');
      Route::post('/form',      'Master\PegawaiController@form')->name('formPegawai');
      Route::post('/save',      'Master\PegawaiController@saveData')->name('savePegawai');
      Route::post('/detail',    'Master\PegawaiController@detailData')->name('detailPegawai');
      Route::post('/remove',    'Master\PegawaiController@removeData')->name('removePegawai');
    });
    Route::group(array('prefix'=>'lampiran-bahan'), function(){
      Route::get('/',                 'Master\LampiranBahanController@main')->name('masterLampiranBahan');
      Route::post('/loadDataProfesi', 'Master\LampiranBahanController@loadDataProfesi')->name('loadProfesiMLB');
      Route::post('/detail',          'Master\LampiranBahanController@detailData')->name('detailMasterLampiranBahan');
      Route::post('/loadData',        'Master\LampiranBahanController@loadData')->name('loadMasterLampiranBahan');
      Route::post('/form',            'Master\LampiranBahanController@form')->name('formMasterLampiranBahan');
      Route::post('/save',            'Master\LampiranBahanController@saveData')->name('saveMasterLampiranBahan');
      Route::post('/remove',          'Master\LampiranBahanController@removeData')->name('removeMasterLampiranBahan');
    });
    Route::group(array('prefix'=>'butir-kegiatan'), function(){
      Route::get('/',                 'Master\ButirKegiatanController@main')->name('butirKegiatan');
      Route::post('/loadDataProfesi', 'Master\ButirKegiatanController@loadDataProfesi')->name('loadProfesiButirKegiatan');
      Route::post('/detail',          'Master\ButirKegiatanController@detailData')->name('detailButirKegiatan');
      Route::post('/loadData',        'Master\ButirKegiatanController@loadData')->name('loadButirKegiatan');
      Route::post('/form',            'Master\ButirKegiatanController@form')->name('formButirKegiatan');
      Route::post('/getInduk',        'Master\ButirKegiatanController@getInduk')->name('getIndukButir');
      Route::post('/save',            'Master\ButirKegiatanController@saveData')->name('saveButirKegiatan');
      Route::post('/remove',          'Master\ButirKegiatanController@removeData')->name('removeButirKegiatan');
      Route::group(array('prefix'=>'new'), function(){
        Route::get('/', 'Master\ButirKegiatanNewController@main')->name('butirKegiatanNew');
        Route::post('/loadDataProfesi', 'Master\ButirKegiatanNewController@loadDataProfesi')->name('loadProfesiButirKegiatanNew');
        Route::post('/detail',          'Master\ButirKegiatanNewController@detailData')->name('detailButirKegiatanNew');
        Route::post('/loadData',        'Master\ButirKegiatanNewController@loadData')->name('loadButirKegiatanNew');
        Route::post('/form',            'Master\ButirKegiatanNewController@form')->name('formButirKegiatanNew');
        Route::post('/save',            'Master\ButirKegiatanNewController@saveData')->name('saveButirKegiatanNew');
        Route::post('/formUpdate',      'Master\ButirKegiatanNewController@formUpdate')->name('formupdateButirKegiatanNew');
        Route::post('/updateData',            'Master\ButirKegiatanNewController@updateData')->name('updateButirKegiatanNew');
      });
    });
    Route::group(array('prefix'=>'unsur-pak'), function(){
      Route::get('/',                 'Master\UnsurPAKController@main')->name('unsurPak');
      Route::post('/loadDataProfesi', 'Master\UnsurPAKController@loadDataProfesi')->name('loadProfesiUnsurPak');
      Route::post('/detail',          'Master\UnsurPAKController@detailData')->name('detailUnsurPak');
      Route::post('/loadData',        'Master\UnsurPAKController@loadData')->name('loadUnsurPak');
      Route::post('/form',            'Master\UnsurPAKController@form')->name('formUnsurPak');
      Route::post('/save',            'Master\UnsurPAKController@saveData')->name('saveUnsurPak');
      Route::post('/remove',          'Master\UnsurPAKController@removeData')->name('removeUnsurPak');
    });
  });

  // =============== TIDAK DIGUNAKAN ===========================
  Route::group(array('prefix'=>'pengguna'), function(){
    Route::group(array('prefix'=>'operator'), function(){ // tidak digunakan
      Route::get('/',               'Users\OperatorController@main')->name('userOperator');
      Route::post('/loadData',      'Users\OperatorController@loadData')->name('loadUserOperator');
      Route::post('/form',          'Users\OperatorController@form')->name('formUserOperator');
      Route::post('/save',          'Users\OperatorController@saveData')->name('saveUserOperator');
      Route::post('/detail',        'Users\OperatorController@detailData')->name('detailUserOperator');
      Route::post('/resetPassword', 'Users\OperatorController@resetPassword')->name('resetUserOperator');
      Route::post('/bannedAccount', 'Users\OperatorController@bannedAccount')->name('bannedUserOperator');
      Route::post('/activeAccount', 'Users\OperatorController@activeAccount')->name('activeUserOperator');
      Route::post('/removeData',    'Users\OperatorController@removeData')->name('removeUserOperator');
    });
  });

});
