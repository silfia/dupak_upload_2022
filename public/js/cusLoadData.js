function loadData(dtLoad, page) {
  var tableString = "<thead class='headData'></thead><tbody class='resultData'></tbody>";
  var head = '';
  var vwPg = '';
  head += '<tr>';
  head += '<td width="10px">&nbsp</td>';
  head += '<td width="10px">No</td>';
  $.each(dtLoad.column, function(k,h){
    head += '<td width="'+h.width+'">'+h.title+'</td>';
  });
  head += '</tr>';

  $('#loadData').append(tableString);
  $('.headData').append(head);

  // create select view data
  // vwPg += '<option value="5">5</option>';
  vwPg += '<option value="10" selected>10</option>';
  vwPg += '<option value="25">25</option>';
  vwPg += '<option value="50">50</option>';
  vwPg += '<option value="100">100</option>';
  $('#lngDt').append(vwPg);

  reloadData(1);
}
$('#lngDt').change(function(){ reloadData(1); });

function reloadData(page) {
  var jmlKolom = dtLoad.column.length + 2;
  $('.resultData').html('<tr><td colspan="'+jmlKolom+'" align="center">Memuat data...</td></tr>');
  var url = dtLoad.url+"?page="+page;
  var search = $('#search-data').val();
  var lngDt = $('#lngDt').val();
  if (dtLoad.customFilter != false) {
    var customFilter = [];
    dtLoad.customFilter.forEach(function(rowFilter) {
      var valFilter = document.getElementById(rowFilter.idName).value;
      var customFilters = {'field' : rowFilter.field, 'values' : valFilter};
      customFilter.push(customFilters);
    });
  }else{
    var customFilter = dtLoad.customFilter;
  };
  $.post(url,{search:search, lngDt:lngDt, customFilter:customFilter}).done(function(data){
    if (data.status == 'success') {
      $('.resultData').empty();
      $('#paging').empty();
      var tag = '';
      var info = '';
      var no = ((parseInt(page)-1)* lngDt) + 1;
      if (data.row.data.length != 0) {
        for(var i = 0; i < data.row.data.length; i++){
          tag += '<tr>';
          tag += '<td align="center" class="pl-0 pr-1">';
          // tag += '<input type="checkbox" name="'+dtLoad.primaryField+'" id="checkData" value="'+data.row.data[i][dtLoad.primaryField]+'">';
          tag += '<label class="custom-control custom-checkbox m-0">';
          tag += '<input type="checkbox" name="id_data[]" id="checkData" value="'+data.row.data[i][dtLoad.primaryField]+'" class="custom-control-input"><span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
          tag += '</label>';
          tag += '</td>';
          tag += '<td align="center">'+no+'</td>';
          dtLoad.column.forEach(function(rowColumn) {
            if (typeof data.row.data[i][rowColumn.field] !== 'undefined') {
              tag += '<td align="'+rowColumn.align+'">'+data.row.data[i][rowColumn.field]+'</td>';
            }else if (typeof rowColumn.rowStyler !== 'undefined') {
              rowData = data.row.data[i];
              rowIndex = i;
              tag += '<td align="'+rowColumn.align+'">'+rowColumn.rowStyler(data.row.data[i], i)+'</td>';
            }else{
              tag += '<td align="'+rowColumn.align+'">Undefined</td>';
            }
          })
          tag += '</tr>';
          no++;
        }
      }else {
        tag += '<tr><td colspan="'+jmlKolom+'" align="center">Tidak ada data ditemukan...</td></tr>';
      }
      $('.resultData').html(tag);

      // Informasi
      if (data.row.data.length != 0) {
        info += 'Menampilkan '+ (((parseInt(page)-1)* lngDt)+1);
      }else{
        info += 'Menampilkan 0';
      }
      no = no - 1;
      info += ' - '+no+' dari '+data.row.total+' data';
      $('.info-data').html(info);

      // pagination
      loadPagination(data.row);
    }else{
      swal("MAAF !","Terjadi Kesalahan !!", "warning");
    }
  }).fail(function() {
    reloadData(page);
  });
}

function loadPagination(row) {
  var hlm = '';
  var pgFirst = row.first_page_url.split('?page=');
  var pgLast = row.last_page_url.split('?page=');
  hlm += '<li class="page-item">';
  if (pgFirst[1] != row.current_page) {
    hlm += '<a class="page-link" href="javascript:void(0)" onclick="reloadData('+pgFirst[1]+')" aria-label="Previous">';
  }else{
    hlm += '<a class="page-link cusDisabled cur-noDrop" href="javascript:void(0)" aria-label="Previous">';
  }
  hlm += '<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>';
  hlm += '</a>';
  hlm += '</li>';
  if (row.prev_page_url != null) {
    var pgPrev = row.prev_page_url.split('?page=');
    if (row.current_page > 2) {
      var prevPgPrev = parseInt(pgPrev[1])-1;
      hlm += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="reloadData('+prevPgPrev+')">'+prevPgPrev+'</a></li>';
    }
    hlm += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="reloadData('+pgPrev[1]+')">'+pgPrev[1]+'</a></li>';
  }
  hlm += '<li class="page-item active"><a class="page-link" href="javascript:void(0)">'+row.current_page+'</a></li>';
  if (row.next_page_url != null) {
    var pgNext = row.next_page_url.split('?page=');
    hlm += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="reloadData('+pgNext[1]+')">'+pgNext[1]+'</a></li>';
    if (pgLast[1] > parseInt(row.current_page) + 1) {
      var nextpgNext = parseInt(pgNext[1])+1;
      hlm += '<li class="page-item"><a class="page-link" href="javascript:void(0)" onclick="reloadData('+nextpgNext+')">'+nextpgNext+'</a></li>';
    }
  }
  hlm += '<li class="page-item">';
  if (pgLast[1] != row.current_page) {
    hlm += '<a class="page-link" href="javascript:void(0)" onclick="reloadData('+pgLast[1]+')" aria-label="Next">';
  }else{
    hlm += '<a class="page-link cusDisabled cur-noDrop" href="javascript:void(0)" aria-label="Next">';
  }
  hlm += '<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>';
  hlm += '</a>';
  hlm += '</li>';
  $('#paging').html(hlm);
}
