@extends('component.layout')

@section('extended_css')
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.css">
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.css">
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/c3charts/c3.css">
@stop

@section('content')
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
      <!--  ..::: pageheader :::.. -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="page-header">
            <h2 class="pageheader-title">{{ $data['title'] }}</h2>
            <div class="page-breadcrumb">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!--  ..::: end pageheader :::.. -->
      <div class="ecommerce-widget">
        <div class="row">
          @if ($data['pegawai']->status_pegawai == 'PNS' && $data['status_masa_pengajuan'] == 'true')
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
              <img src="{{ url('/')}}/assets/images/orang.png" alt="" height="280px">
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
              <div class="card border-3 border-top border-top-primary">
                <div class="card-body" style="min-height:250px">
                  <h5 class="text-muted" style="text-align:center;font-size:18px;margin-top:15px;">Masa Pengajuan Telah Dibuka</h5>
                  <div class="metric-value d-inline-block" style="text-align:center;width:100%">
                    @if (empty($data['pengajuan']))
                      <div style="margin-top:15px;margin-bottom:35px">Silahkan klik tombol dibawah ini<br>untuk melakukan pengajuan DUPAK !!</div>
                      <a href="{{ route('formPengajuanDupak') }}" class="btn btn-large btn-primary">Mengajukan</a>
                    @else
                      <div style="margin-top:15px;margin-bottom:35px">Terima Kasih <br> Telah Melakukan Pengajuan DUPAK !!</div>
                      <a href="{{ route('formCetakBerkas') }}" class="btn btn-large btn-primary">Lihat Berkas</a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
              <div class="card border-3 border-top border-top-primary">
                <div class="card-body" style="min-height:250px;text-align:center">
                  <img src="{{ url('/')}}/assets/images/calendar.png" alt="" height="120px">
                  <div class="metric-value d-inline-block" style="text-align:center;width:100%">
                    <div style="margin-top:15px;margin-bottom:5px;font-size:18px;font-weight:bold;">WAKTU PENGAJUAN</div>
                    <div style="margin-top: 15px;margin-bottom:5px;font-size: 18px;">
                      {{ date('d', strtotime($data['masa_pengajuan']->tgl_awal)) }} {{ App\Http\Libraries\Formatters::get_bulan(date('m', strtotime($data['masa_pengajuan']->tgl_awal))) }} - {{ date('d', strtotime($data['masa_pengajuan']->tgl_akhir)) }} {{ App\Http\Libraries\Formatters::get_bulan(date('m', strtotime($data['masa_pengajuan']->tgl_akhir))) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif
        </div>

        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">Pengisian Aktivitas</h5>
              <div class="card-body">
                <ul>
                  {{-- <li><p>Pengisian Aktifitas Harian periode 1 - 7 terakhir pada tanggal 8 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 8 - 15 terakhir pada tanggal 16 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 16 - 22 terakhir pada tanggal 23 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 23 - 31 terakhir pada tanggal 1 bulan berikutnya </p></li> --}}
                  {{-- <li><p>Pengisian Aktifitas Harian periode 1 - 16 terakhir pada tanggal 16 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 16 - 31 terakhir pada tanggal Akhir Bulan </p></li> --}}
                  <li><p>Pengisian Aktivitas Harian bisa Dilakukan Ditanggal Aktif dan Tanggal Sebelumnya Selama 1 Bulan </p></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@stop

@section('extended_js')
  <!-- chart chartist js -->
  <script src="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
  <!-- sparkline js -->
  <script src="{{ url('/')}}/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
@stop
