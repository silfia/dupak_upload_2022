@extends('component.layout')

@section('extended_css')
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.css">
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.css">
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/c3charts/c3.css">
@stop

@section('content')
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
      <!--  ..::: pageheader :::.. -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="page-header">
            <h2 class="pageheader-title">{{ $data['title'] }}</h2>
            <div class="page-breadcrumb">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!--  ..::: end pageheader :::.. -->
      <div class="ecommerce-widget">
        <div class="row">
          <!--  ..::: sales :::.. -->
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Sales</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">$12099</h1></div>
                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5.86%</span>
                </div>
              </div>
            </div>
          </div>
          <!--  ..::: end sales :::.. -->

          <!--  ..::: new customer :::.. -->
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">New Customer</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">1255</h1></div>
                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">10%</span>
                </div>
              </div>
            </div>
          </div>
          <!--  ..::: end new customer :::.. -->

          <!--  ..::: visitor :::.. -->
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Visitor</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">13000</h1></div>
                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5%</span>
                </div>
              </div>
            </div>
          </div>
          <!--  ..::: end visitor :::.. -->

          <!--  ..::: total orders :::.. -->
          @if ($data['pegawai']->status_pegawai == 'PNS')
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Pengajuan Dupak</h5>
                  <div class="metric-value d-inline-block">
                      <a href="{{ route('ViewDupak') }}" class="btn btn-sm btn-primary">lihat disini</a>
                  </div>
                <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i class="fa fa-fw fa-arrow-right"></i></span><span class="ml-1">Jml PD 40</span>
                </div>
              </div>
            </div>
          </div>
        @endif
          <!--  ..::: end total orders :::.. -->
        </div>

        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">Pengisian Aktifitas</h5>
              <div class="card-body">
                <ul>
                  <li><p>Pengisian Aktifitas Harian periode 1 - 7 terakhir pada tanggal 8 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 8 - 15 terakhir pada tanggal 16 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 16 - 22 terakhir pada tanggal 23 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 23 - 31 terakhir pada tanggal 1 bulan berikutnya </p></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">Pengisian Aktifitas</h5>
              <div class="card-body">
                <ul>
                  <li><p><b>Penilaian Perilaku Kerja PNS</b> untuk bulan <b>Juli,Agustus, dan September 2019 dapat dilakukan </b> sampai dengan <b>Tanggal 07 Oktober 2019</b></p></li>
                  <li><p>Untuk Pejabat Struktural buka Menu <b>Penilaian -> Penilaian Perilaku 360</b></p></li>
                  <li><p>Untuk Pelaksana dan Jabatan Fungsional buka menu <b>Penilaian Perilaku 360</b></p></li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
@stop
@section('extended_js')
  <!-- chart chartist js -->
  <script src="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
  <!-- sparkline js -->
  <script src="{{ url('/')}}/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
  <!-- morris js -->
  <script src="{{ url('/')}}/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
  <script src="{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.js"></script>
  <!-- chart c3 js -->
  <script src="{{ url('/')}}/assets/vendor/charts/c3charts/c3.min.js"></script>
  <script src="{{ url('/')}}/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
  <script src="{{ url('/')}}/assets/vendor/charts/c3charts/C3chartjs.js"></script>
  <script src="{{ url('/')}}/assets/libs/js/dashboard-ecommerce.js"></script>
@stop
