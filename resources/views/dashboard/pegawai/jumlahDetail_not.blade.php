<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
			</div>
      <form class="formJumlah">
        <div class="modal-body">
          <input id="inputIdjabatan" type="hidden" name="idjabatan" value="{{ $idjabatan }}" class="form-control backWhite" readonly>
          <input id="inputIdPakMaster" type="hidden" name="idPakMaster" value="{{ $idPakMaster }}" class="form-control backWhite" readonly>
          <input id="inputDay" type="hidden" name="tgl" value="{{ $tgl }}" class="form-control backWhite" readonly>
          <input id="inputStPgw" type="hidden" name="status_pegawai" value="{{ $pegawai->status_pegawai }}" class="form-control backWhite" readonly>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah Total Pasien</label>
            <div class="col-8 col-lg-9">
              <input id="inputJumlah" type="number" name="Jumlah" placeholder="Jumlah Total Pasien" class="form-control">
            </div>
          </div>
          @if ($pegawai->status_pegawai == "PNS")
            <div class="form-group row">
              <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah DUPAK</label>
              <div class="col-8 col-lg-9">
                <input id="inputJumlah" type="number" name="Jumlah_Dupak" placeholder="Jumlah Pasien untuk Dupak" class="form-control">
              </div>
            </div>
          @endif
        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-chevron-left"></span> Batal</a>
          <a href="#" class="btn btn-primary btn-submitJumlah">Simpan <span class="fas fa-save"></span></a>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '70%'
    });
    $('#detail-dialog').modal('show');
  })();
  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  })

  $('.btn-submitJumlah').click(function(e){
    e.preventDefault();
    $('.btn-submitJumlah').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.formJumlah')[0]);
    $.ajax({
      url: "{{ route('formAddDetailKegiatan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.formJumlah').validate(data, 'has-error');
      $('.loading').show();
      $('.main-layer').hide();
      if(data.status == 'success'){
        $('#detail-dialog').modal('hide');
        $('.loading').hide();
        $('.other-page').html(data.content).fadeIn();
      } else if(data.status == 'error') {
        $('.btn-submitJumlah').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitJumlah').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitJumlah').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
