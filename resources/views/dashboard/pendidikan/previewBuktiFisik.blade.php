<div class="card col-md-12 p-0 panel-form">
      <div class="card-body">
    
          <div class="form-group">
            <img class="img-fluid" src="{{ url('upload/'.$dtMaster->bukti_fisik.'') }}" alt="">
          </div>
    
          <div class="row pt-2 pt-sm-2 mt-1">
            <div class="col-sm-12 pl-0">
              <p class="text-right">
                <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
              </p>
            </div>
          </div>
      </div>
    </div>
    
    <script type="text/javascript">
      var onLoad = (function() {
        $('.panel-form').animateCss('bounceInUp');
      })();
    
      $('.btn-cancel').click(function(e){
        e.preventDefault();
                $('.panel-form').animateCss('bounceOutDown');
                $('.other-page').fadeOut(function(){
                      $('.other-page').empty();
                      $('.main-layer').fadeIn();
                });
      });
      
    </script>
    