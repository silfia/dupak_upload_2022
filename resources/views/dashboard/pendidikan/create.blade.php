<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($dtMaster == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">

  <form class="form-save">
    @if ($dtMaster != "")
      <input type="hidden" name="id_penilaian_pendidikan" value="{{ $dtMaster->id_penilaian_pendidikan }}">
    @endif
      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Jenis</label>
        <div class="col-8 col-lg-9">
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <label class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="jenis" value="ijazah" class="custom-control-input" @if($dtMaster != "") @if($dtMaster->jenis == 'ijazah') checked @endif @endif><span class="custom-control-label">Ijazah</span>
            </label>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <label class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="jenis" value="sertifikat" class="custom-control-input" @if($dtMaster != "") @if($dtMaster->jenis == 'sertifikat') checked @endif @endif><span class="custom-control-label">Sertifikat</span>
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label for="inputStj" class="col-4 col-lg-3 col-form-label">Tingkat Pendidikan</label>
          <div class="col-8 col-lg-9">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline">
                <input type="radio" name="tingkat" value="D3" id="tingkatD3" class="custom-control-input" @if($dtMaster != "") @if($dtMaster->tingkat == 'D3') checked @endif @endif>
                <span class="custom-control-label" for="tingkatD3">D3</span>
              </label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline">
                <input type="radio" name="tingkat" value="S1" id="tingkatS1" class="custom-control-input" @if($dtMaster != "") @if($dtMaster->tingkat == 'S1') checked @endif @endif>
                <span class="custom-control-label" for="tingkatS1">S1</span>
              </label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline">
                <input type="radio" name="tingkat" value="S2" id="tingkatS2" class="custom-control-input" @if($dtMaster != "") @if($dtMaster->tingkat == 'S2') checked @endif @endif>
                <span class="custom-control-label" for="tingkatS2">S2</span>
              </label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline">
                <input type="radio" name="tingkat" value="S3" id="tingkatS3" class="custom-control-input" @if($dtMaster != "") @if($dtMaster->tingkat == 'S3') checked @endif @endif>
                <span class="custom-control-label" for="tingkatS3">S3</span>
              </label>
            </div>
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Nama Penyedia</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="nama_penyedia" value="@if($dtMaster != '') {{ $dtMaster->nama_penyedia }} @endif" placeholder="Nama Penyedia" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Bukti Fisik</label>
          <div class="col-8 col-lg-9">
            <input type="file" name="bukti_fisik" value="@if($dtMaster != '') {{ $dtMaster->bukti_fisik }} @endif" placeholder="Bukti Fisik" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Keterangan</label>
          <div class="col-8 col-lg-9">
            <textarea type="text" name="keterangan" rows="3" placeholder="Keterangan" class="form-control">@if($dtMaster != '') {{ $dtMaster->keterangan }} @endif</textarea>
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Lama Pelaksanaan</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="lama_pelaksaan" value="@if($dtMaster != '') {{ $dtMaster->lama_pelaksaan }} @endif" placeholder="Nama Pelaksanaan" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tahun Masuk</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_masuk" value="@if($dtMaster != '') {{ $dtMaster->tahun_masuk }} @endif" placeholder="Tahun Masuk" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tahun Keluar</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_keluar" value="@if($dtMaster != '') {{ $dtMaster->tahun_keluar }} @endif" placeholder="Tahun Keluar" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tanggal</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tanggal" value="@if($dtMaster != '') {{$dtMaster->tanggal}} @endif" data-date-format="yyyy-mm-dd" id="tanggal" class="form-control" placeholder="dd-mm-yyyy">
          </div>
      </div>

      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>

  </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
  });
  

  $('#tanggal').datetimepicker({
    weekStart: 2,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 4,
    minView: 2,
    forceParse: 0,
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveDataUP') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
