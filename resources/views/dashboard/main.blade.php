@extends('component.layout')
@section('extended_css')
  {{-- <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.css"> --}}
  {{-- <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.css"> --}}
  {{-- <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/c3charts/c3.css"> --}}
@stop

@section('content')
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
      <!--  ..::: pageheader :::.. -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="page-header">
            <h2 class="pageheader-title">{{ $data['title'] }}</h2>
            <div class="page-breadcrumb">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!--  ..::: end pageheader :::.. -->
      
      @if (Auth::getUser()->level_user == '1')
       <!-- ..:: GRAFIK ::.. -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">Grafik Pengunjung</h5>
              
              <div class="card-body">
                <div class="custom-bar-chart">
                 <div id="resultChart"></div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- ..:: GRAFIK ::.. -->
        
         <!-- ..:: GRAFIK ::.. -->
        <!--<div class="row">-->
        <!--  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">-->
        <!--    <div class="card">-->
        <!--      <h5 class="card-header">Grafik Pengunjung</h5>-->
              
        <!--      <div class="card-body">-->
        <!--        <div class="custom-bar-chart">-->
        <!--         <div id="histats_counter"></div>-->
        <!--        </div>-->
        <!--      </div>-->

        <!--    </div>-->
        <!--  </div>-->
        <!--</div>-->
        <!-- ..:: GRAFIK ::.. -->
      @endif
      
      <div class="ecommerce-widget">
        <div class="row">
          <!--  ..::: sales :::.. -->
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Pegawai PNS</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">{{ $data['jumlahPNS'] }} Pegawai</h1></div>
                {{-- <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5.86%</span>
                </div> --}}
              </div>
            </div>
          </div>
          <!--  ..::: end sales :::.. -->

          <!--  ..::: new customer :::.. -->
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Pegawai Non PNS</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">{{ $data['jumlahNonPNS'] }} Pegawai</h1></div>
                {{-- <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">10%</span>
                </div> --}}
              </div>
            </div>
          </div>
          <!--  ..::: end new customer :::.. -->

          <!--  ..::: Tim Penilai :::.. -->
          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Tim Penilai</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">{{ $data['jumlahTimPenilai'] }} Pegawai</h1></div>
              </div>
            </div>
          </div>
          <!--  ..::: end Tim Penilai :::.. -->

          <!--  ..::: visitor :::.. -->
          {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Visitor</h5>
                <div class="metric-value d-inline-block"><h1 class="mb-1">13000</h1></div>
                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5%</span>
                </div>
              </div>
            </div>
          </div> --}}
          <!--  ..::: end visitor :::.. -->

          <!--  ..::: total orders :::.. -->
          {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
            <div class="card border-3 border-top border-top-primary">
              <div class="card-body">
                <h5 class="text-muted">Pengajuan Dupak</h5>
                <div class="metric-value d-inline-block"><a href="" class="btn btn-sm btn-primary">lihat disini</a></div>
                <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                  <span class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i class="fa fa-fw fa-arrow-right"></i></span><span class="ml-1">Jml PD 40</span>
                </div>
              </div>
            </div>
          </div> --}}
          <!--  ..::: end total orders :::.. -->
        </div>

        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">Pengisian Aktivitas</h5>
              <div class="card-body">
                <ul>
                  {{-- <li><p>Pengisian Aktifitas Harian periode 1 - 7 terakhir pada tanggal 8 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 8 - 15 terakhir pada tanggal 16 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 16 - 22 terakhir pada tanggal 23 </p></li>
                  <li><p>Pengisian Aktifitas Harian periode 23 - 31 terakhir pada tanggal 1 bulan berikutnya </p></li> --}}
                  {{-- <li><p>Pengisian Aktifitas Harian periode 1 - 16 terakhir pada tanggal 16 </p></li> --}}
                 <li><p>Pengisian Aktivitas Harian bisa Dilakukan Ditanggal Aktif dan Tanggal Sebelumnya Selama 1 Bulan </p></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
@stop
@section('extended_js')
  
    <!-- Histats.com  START  (aync)-->
    {{-- <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,4503953,4,9,110,60,00011110']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
    var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
    hs.src = ('//s10.histats.com/js15_as.js');
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
    <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4503953&101" alt="counter statistics" border="0"></a></noscript> --}}
    <!-- Histats.com  END  -->

  <script src="https://code.highcharts.com/highcharts.js"></script>
  {{-- <script src="https://code.highcharts.com/modules/exporting.js"></script> --}}
  {{-- <script src="https://code.highcharts.com/modules/export-data.js"></script> --}}
  {{-- <script src="https://code.highcharts.com/modules/accessibility.js"></script> --}}
  
  <!-- chart chartist js -->
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.min.js"></script> --}}
  <!-- sparkline js -->
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/sparkline/jquery.sparkline.js"></script> --}}
  <!-- morris js -->
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/morris-bundle/raphael.min.js"></script> --}}
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.js"></script> --}}
  <!-- chart c3 js -->
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/c3charts/c3.min.js"></script> --}}
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script> --}}
  {{-- <script src="{{ url('/')}}/assets/vendor/charts/c3charts/C3chartjs.js"></script> --}}
  {{-- <script src="{{ url('/')}}/assets/libs/js/dashboard-ecommerce.js"></script> --}}
  
  <script type="text/javascript">
  $.post("{!!route('getGrafikVisit')!!}").done(function(vals){
    var dtBB_Us = [];
    for (var i = 0; i < vals.totVis.length; i++) {
      // dtBB_Us[i] = [ vals.totVis[i].tanggal, vals.totVis[i].Visitor ];
      dtBB_Us[i] = [ Date.UTC(vals.totVis[i].tahun, vals.totVis[i].bulan - 1, vals.totVis[i].tanggal), vals.totVis[i].Visitor ];
    }
    console.log(dtBB_Us);
    Highcharts.chart('resultChart', {
      chart: { type: 'spline' },
      title: { text: 'Grafik Pengunjung' },
      subtitle: { text: 'Website <b>dupakonline.com</b>'},
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
          day: '%e %b %y'
        },
        title: {
          text: 'Tanggal'
        }
      },
      yAxis: {
        title: { text: 'Pengunjung' },
        min: 0
      },
      tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x:%e %b %y}: {point.y:.0f} Pengunjung'
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      plotOptions: {
        spline: {
          marker: {enabled: true},
          dataLabels:{enabled:true}
        }
      },
      series: [{
        name: 'Pengunjung',
        marker: {enabled: true},
        data: dtBB_Us
      }]
    });
  });
  </script>
@stop
