@extends('component.layout')

@section('extended_css')
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.css">
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.css">
  <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/charts/c3charts/c3.css">
@stop

@section('content')
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
      <!--  ..::: pageheader :::.. -->
      <div class="ecommerce-widget">
        <div class="row">
          <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
            <h5 class="card-header bg-primary">
              <i class="fas fa-search-plus mr-2"></i>FORM PENGAJUAN DUPAK
            </h5>
            <div class="card-body">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>Nama Pegawai:</th>
                    <td colspan="6">{{ $data['pegawai']->nama }}</td>
                  </tr>
                  <tr>
                    <th>NIP:</th>
                    <td colspan="6">{{ $data['pegawai']->no_nip }}</td>
                  </tr>
                  <tr>
                    <th>Nomor Karpeg:</th>
                    <td colspan="6">{{ $data['pegawai']->no_karpeng }}</td>
                  </tr>
                  <tr>
                    <th>Jenis Kelamin:</th>
                    <td colspan="6">@if($data['pegawai']->jenis_kelamin == 'L') Pria @else Wanita @endif</td>
                  </tr>
                  <tr>
                    <th>Pangkat/Golongan</th>
                    <td colspan="3">{{ $data['pegawai']->golongan->nama }}</td>
                    <th>Ruang/TMT</th>
                    <td colspan="2">{{ $data['pegawai']->golongan_tmt }}</td>
                  </tr>
                  <tr>
                    <th>Jabatan:</th>
                    <td colspan="3">{{ $data['pegawai']->jabatan->nama}}</td>
                    <th>Ruang/TMT</th>
                    <td colspan="2">{{ $data['pegawai']->jabatan_tmt }}</td>
                  </tr>
                  <tr>
                    <th>Unit Kerja:</th>
                    <td colspan="6">{{ $data['pegawai']->satuanKerja->nama }}</td>
                  </tr>
                </tbody>
              </table>
              </br>
            </div>
          </div>

          <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
            <div class="card-body">
              <h6>PILIH TANGGAL DALAM MASA PENILAIAN ANDA :</h6>
              <form class="form-save">
                @if ($data['pengajuan'] != "")
                  <input type="text" name="id_Bpengajuan" value="{{ $data['pengajuan']->id_Bpengajuan }}">
                @endif
                <div class="form-group row">
                  <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tanggal Awal Penilaian</label>
                  <div class="col-8 col-lg-9">
                    <div class="input-group input-group-sm col-lg-12 col-md-12 col-sm-12 col-xs-12 pr-0">
                      <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                      <input id="inputtglawal" type="text" name="tgl_awal" value="@if($data['pengajuan'] != '') {{ $data['pengajuan']->tgl_awal }} @endif" placeholder="Masukkan tanggal awal masa Penilaian" class="form-control" data-date-format="dd-mm-yyyy">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tanggal Akhir Penilaian</label>
                  <div class="col-8 col-lg-9">
                    <div class="input-group input-group-sm col-lg-12 col-md-12 col-sm-12 col-xs-12 pr-0">
                      <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                      <input id="inputtglakhir" type="text" name="tgl_akhir" value="@if($data['pengajuan'] != '') {{ $data['pengajuan']->tgl_akhir }} @endif" placeholder="Masukkan tanggal akhir masa Penilaian" class="form-control" data-date-format="dd-mm-yyyy">
                    </div>
                  </div>
                </div>
                <div class="row pt-2 pt-sm-2 mt-1">
                  <div class="col-sm-12 pl-0">
                    <p class="text-right">
                      <a href="{{ route('dashboard') }}" class="btn btn-sm btn-space btn-secondary"><span class="fas fa-chevron-left"></span> Kembali</a>
                      <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('extended_js')
  <!-- chart chartist js -->
  <script src="{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
  <!-- sparkline js -->
  <script src="{{ url('/')}}/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
  <script type="text/javascript">
    $('#inputtglawal').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 2,
        forceParse: 0,
    });
    $('#inputtglakhir').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 2,
        forceParse: 0,
    });


    $('.btn-cancel').click(function(e){
      e.preventDefault();
  		$('.panel-form').animateCss('bounceOutDown');
  		$('.other-page').fadeOut(function(){
  			$('.other-page').empty();
  			$('.main-layer').fadeIn();
  		});
  	});

    $('.btn-submit').click(function(e){
      e.preventDefault();
      $('.btn-submit').html('Please wait...').attr('disabled', true);
      var data  = new FormData($('.form-save')[0]);
      $.ajax({
        url: "{{ route('savePengajuanDupak') }}",
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false
      }).done(function(data){
        $('.form-save').validate(data, 'has-error');
        if(data.status == 'success'){
          swal("Success !", data.message, "success");
          window.location.href = "{{ route('dashboard') }}";
        } else if(data.status == 'error') {
          $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
          swal('Whoops !', data.message, 'warning');
        } else {
          var n = 0;
          for(key in data){
            if (n == 0) {var dt0 = key;}
            n++;
          }
          $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
          swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
        }
      }).fail(function() {
        swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
      });
    });
  </script>
@stop
