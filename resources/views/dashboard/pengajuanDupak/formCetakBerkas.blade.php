@extends('component.layout')

@section('extended_css')
  <!--
  <link rel="stylesheet" href="@{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.css">
  <link rel="stylesheet" href="@{{ url('/')}}/assets/vendor/charts/morris-bundle/morris.css">
  <link rel="stylesheet" href="@{{ url('/')}}/assets/vendor/charts/c3charts/c3.css">
-->
@stop

@section('content')
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
      <!--  ..::: pageheader :::.. -->
      <div class="ecommerce-widget">
        <div class="row">
          <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
            <h5 class="card-header bg-primary"><i class="fas fa-search-plus mr-2"></i>Cetak Berkas</h5>
            <div class="card-body">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>Nama Pegawai:</th>
                    <td colspan="6">{{ $data['pegawai']->nama }}</td>
                  </tr>
                  <tr>
                    <th>NIP:</th>
                    <td colspan="6">{{ $data['pegawai']->no_nip }}</td>
                  </tr>
                  <tr>
                    <th>Nomor Karpeg:</th>
                    <td colspan="6">{{ $data['pegawai']->no_karpeng }}</td>
                  </tr>
                  <tr>
                    <th>Jenis Kelamin:</th>
                    <td colspan="6"><?php if($data['pegawai']->jenis_kelamin == 'L'){echo 'Pria';}else{echo 'Wanita';} ?></td>
                  </tr>
                  <tr>
                    <th>Pangkat/Golongan</th>
                    <td colspan="3">{{ $data['pegawai']->golongan->nama }}</td>
                    <th>Ruang/TMT</th>
                    <td colspan="2">{{ date('d-m-Y', strtotime($data['pegawai']->golongan_tmt)) }}</td>
                  </tr>
                  <tr>
                    <th>Jabatan:</th>
                    <td colspan="3">{{ $data['pegawai']->jabatan->nama}}</td>
                    <th>Ruang/TMT</th>
                    <td colspan="2">{{ date('d-m-Y', strtotime($data['pegawai']->jabatan_tmt)) }}</td>
                  </tr>
                  <tr>
                    <th>Unit Kerja:</th>
                    <td colspan="6">{{ $data['pegawai']->satuanKerja->nama }}</td>
                  </tr>
                  <tr>
                    <th>Profesi:</th>
                    <td colspan="6">{{ $data['pegawai']->typeProfesi->nama }}</td>
                  </tr>
                </tbody>
              </table>
              </br>
            </div>
          </div>

          <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
            <div class="card-body">
              @for ($i=0; $i < count($data['semester']); $i++)
                <a href="javascript:void(0)" class="btn btn-primary btn-xs buatSPMK_{{ $data['semester'][$i]['kode'] }}" onclick="getDataSpmk({{ $data['semester'][$i]['kode'] }}, '{{ $data['semester'][$i]['tgl_awal'] }}', '{{ $data['semester'][$i]['tgl_akhir'] }}')" style="width:310px;margin:5px;"><span class="fas fa-print"></span> Buat SPMK {{ $data['semester'][$i]['title'] }}</a>
                <a href="javascript:void(0)" class="btn btn-warning btn-xs menungguSPMK_{{ $data['semester'][$i]['kode'] }}" onclick="Waiting()" style="width:310px;margin:5px;display:none"><span class="fas fa-hourglass-start"></span> <i>Memuat Data ... </i></a>
                <a href="javascript:void(0)" class="btn btn-success btn-xs cetakSPMK_{{ $data['semester'][$i]['kode'] }}" onclick="tableToExcel('my-table-id_{{ $data['semester'][$i]['kode'] }}', 'SPMK {{ $data['semester'][$i]['title'] }}')" style="width:310px;margin:5px;display:none"><span class="fas fa-print"></span> Cetak SPMK {{ $data['semester'][$i]['title'] }}</a>
              @endfor
              <div class="clearfix"></div>
              @for($t=0; $t < count($data['rekapTahun']); $t++)
                <span class="button_{{ $data['rekapTahun'][$t]['kode'] }}">  
                  <a href="javascript:void(0)" class="btn btn-primary btn-xs buatRekap_{{ $data['rekapTahun'][$t]['kode'] }}" onclick="getDataRekapTahun('{{ $data['rekapTahun'][$t]['kode'] }}', '{{ $data['rekapTahun'][$t]['tgl_awal'] }}', '{{ $data['rekapTahun'][$t]['tgl_akhir'] }}', '{{ $data['rekapTahun'][$t]['title'] }}')" style="width:310px;margin:5px;"><span class="fas fa-print"></span> Buat Rekap {{ $data['rekapTahun'][$t]['title'] }}</a>
                </span>
              @endfor

              @for ($j=0; $j < count($data['semester']); $j++)
                <div class="PanelId_{{ $data['semester'][$j]['kode'] }}" style="display:none">PanelId_{{ $data['semester'][$j]['kode'] }}</div>
                <div class="" id="content_{{ $data['semester'][$j]['kode'] }}" style="display:none;">
                  <div id="my-table-id_{{ $data['semester'][$j]['kode'] }}"></div>
                </div>
              @endfor

              @for ($tr=0; $tr < count($data['rekapTahun']); $tr++)
                <div class="PanelId_{{ $data['rekapTahun'][$tr]['kode'] }}" style="display:none">PanelId_{{ $data['rekapTahun'][$tr]['kode'] }}</div>
                <div class="" id="content_{{ $data['rekapTahun'][$tr]['kode'] }}" style="display:block;">
                  <div id="my-table-id_{{ $data['rekapTahun'][$tr]['kode'] }}"></div>
                </div>
              @endfor
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@stop

@section('extended_js')
  <!-- chart chartist js -->
  <!-- <script src="@{{ url('/')}}/assets/vendor/charts/chartist-bundle/chartist.min.js"></script> -->
  <!-- sparkline js -->
  <!-- <script src="@{{ url('/')}}/assets/vendor/charts/sparkline/jquery.sparkline.js"></script> -->
  <script type="text/javascript">
    // var onLoad = (function() {
    //   $('.panel-form').animateCss('bounceInUp');
    // })();

    function Waiting(){
      swal({
        title: "MAAF !",
        text: "Belum Selesai Memuat Data !!",
        type: "warning",
        timer: 2000,
        showConfirmButton: false
      });
    };

    function getDataSpmk(kode, tgl_awal, tgl_akhir) {
      console.log("kode = "+kode+" | awal = "+tgl_awal+" | akhir = "+tgl_akhir);
      $("#my-table-id_"+kode).html('');
      $('.buatSPMK_'+kode).hide();
      $('.menungguSPMK_'+kode).show();
      // $('.menungguSPMK_'+kode).html('<i>Memuat Data ...</i> 10 %');
      $('.cetakSPMK_'+kode).hide();
      var id_pengajuan = {{ $data['pengajuan']->id_pengajuan }};
      var tag = '';
      var idPak = '';
      $.post("{!! route('getDataSpmk') !!}",{kode:kode,tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,id_pengajuan:id_pengajuan}).done(function(data){
        if (data.status == 'success') {
          $.each(data.data.butir_kegiatan, function(k,uns){
            if (uns.nama != 'Pendidikan') {
              tag += '<table>';
              tag += '<tbody>';
              tag += '<tr>';
              tag += '<td colspan="11" align="center" style="font-size:14pt;font-weight: bold">SURAT PERNYATAAN</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td colspan="11" align="center" style="font-size:14pt;font-weight: bold">MELAKUKAN KEGIATAN '+uns.nama.toUpperCase()+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td colspan="11" align="center" style="font-size:14pt;font-weight: bold">'+data.data.pegawai.profesi.toUpperCase()+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td></td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td colspan="6">Yang bertanda tangan dibawah ini :</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">1</td>';
              tag += '<td colspan="5" width="276px" align="left">Nama</td>';
              tag += '<td>: '+data.data.kepala.nama+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">2</td>';
              tag += '<td colspan="5" width="276px" align="left">NIP</td>';
              tag += '<td>: '+data.data.kepala.nip+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">3</td>';
              tag += '<td colspan="5" width="276px" align="left">Pangkat Golongan Ruang/TMT </td>';
              tag += '<td>: '+data.data.kepala.golongan+' / '+data.data.kepala.golonganTmt+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">4</td>';
              tag += '<td colspan="5" width="276px" align="left">Jabatan </td>';
              tag += '<td>: Kepala Puskesmas</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">5</td>';
              tag += '<td colspan="5" width="276px" align="left">Unit Kerja </td>';
              tag += '<td>: '+data.data.pegawai.unitKerja+'</td>';
              tag += '</tr>';
              tag += '<tr><td></td></tr>';
              tag += '<tr>';
              tag += '<td colspan="6">Menerangkan bahwa :</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">1</td>';
              tag += '<td colspan="5" width="276px" align="left">Nama</td>';
              tag += '<td>: '+data.data.pegawai.nama+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">2</td>';
              tag += '<td colspan="5" width="276px" align="left">NIP</td>';
              tag += '<td>: '+data.data.pegawai.nip+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">3</td>';
              tag += '<td colspan="5" width="276px" align="left">Pangkat Golongan Ruang/TMT </td>';
              tag += '<td>: '+data.data.pegawai.golongan+' / '+data.data.pegawai.golonganTmt+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">4</td>';
              tag += '<td colspan="5" width="276px" align="left">Jabatan </td>';
              tag += '<td>: '+data.data.pegawai.jabatan+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="left">5</td>';
              tag += '<td colspan="5" width="276px" align="left">Unit Kerja </td>';
              tag += '<td>: '+data.data.pegawai.unitKerja+'</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td></td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td colspan="7">Telah melaksanakan kegiatan Pengabdian Masyarakat :</td>';
              tag += '</tr>';
              tag += '</tbody>';
              tag += '</table>';
              tag += '<br>';
              tag += '<table id="body_excel" border="1">';
              tag += '<thead>';
              tag += '<tr>';
              tag += '<td rowspan="2" width="32px" align="center">No</td>';
              tag += '<td colspan="5" align="center">Uraian Kegiatan</td>';
              tag += '<td rowspan="2" width="90px" align="center">Tanggal</td>';
              tag += '<td width="85px" align="center">Satuan</td>';
              tag += '<td width="92px" align="center">Juml Vol</td>';
              tag += '<td width="100px" align="center">Juml</td>';
              tag += '<td width="92px" align="center">Ket</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td colspan="5" align="center">'+uns.nama+'</td>';
              tag += '<td width="85px" align="center">Hasil</td>';
              tag += '<td width="92px" align="center">Kegiatan</td>';
              tag += '<td width="100px" align="center">AK</td>';
              tag += '<td width="92px" align="center">Bukti Fisik</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px" align="center">1</td>';
              tag += '<td colspan="5" align="center">2</td>';
              tag += '<td width="90px" align="center">3</td>';
              tag += '<td width="85px" align="center">4</td>';
              tag += '<td width="92px" align="center">5</td>';
              tag += '<td width="100px" align="center">6</td>';
              tag += '<td width="92px" align="center">7</td>';
              tag += '</tr>';
              var tempNo1 = 0;
              $.each(uns.row, function(k,v1){
                tempNo1++;
                if (v1.statusRow == 'Parent') {
                  tag += '<tr>';
                  tag += '<td width="32px" align="center">'+KonDecRomawi(tempNo1)+'.</td>';
                  tag += '<td colspan="5" width="278px">'+v1.butir_kegiatan+'</td>';
                  tag += '<td width="90px"></td>';
                  tag += '<td width="85px"></td>';
                  tag += '<td width="92px"></td>';
                  tag += '<td width="100px"></td>';
                  tag += '<td width="92px"></td>';
                  tag += '</tr>';
                  if (v1.row.length > 0) {
                    var tempNo2 = 0; // Hurug Awal
                    var tempNo2n = 0; // Hurug Akhir
                    var No2 = '';
                    var No2n = '';
                    $.each(v1.row, function(k,v2){
                      if (tempNo2n == 26) {
                        tempNo2++;
                        No2 = String.fromCharCode(64 + tempNo2);
                      }
                      tempNo2n++;
                      No2n = String.fromCharCode(64 + tempNo2n);
                      if (v2.statusRow == 'Parent') {
                        tag += '<tr>';
                        tag += '<td width="32px" align="center"></td>';
                        tag += '<td width="32px" align="center">'+No2+''+No2n+'.</td>';
                        tag += '<td colspan="4" width="246px">'+v2.butir_kegiatan+'</td>';
                        tag += '<td width="90px"></td>';
                        tag += '<td width="85px"></td>';
                        tag += '<td width="92px"></td>';
                        tag += '<td width="100px"></td>';
                        tag += '<td width="92px"></td>';
                        tag += '</tr>';
                        if (v2.row.length > 0) {
                          var tempNo3 = 0;
                          $.each(v2.row, function(k,v3){
                            tempNo3++;
                            if (v3.statusRow == 'Parent') {
                              tag += '<tr>';
                              tag += '<td width="32px"></td>';
                              tag += '<td width="32px"></td>';
                              tag += '<td width="32px" align="center">'+tempNo3+'.</td>';
                              tag += '<td colspan="3" width="214px">'+v3.butir_kegiatan+'</td>';
                              tag += '<td width="90px"></td>';
                              tag += '<td width="85px"></td>';
                              tag += '<td width="92px"></td>';
                              tag += '<td width="100px"></td>';
                              tag += '<td width="92px"></td>';
                              tag += '</tr>';
                              if (v3.row.length > 0) {
                                var tempNo4 = 0; // Hurug Awal
                                var tempNo4n = 0; // Hurug Akhir
                                var No4 = '';
                                var No4n = '';
                                $.each(v3.row, function(k,v4){
                                  if (tempNo4n == 26) {
                                    tempNo4++;
                                    No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                  }
                                  tempNo4n++;
                                  No4n = String.fromCharCode(64 + tempNo4n).toLowerCase();
                                  if (v4.statusRow == 'Parent') {
                                    tag += '<tr>';
                                    tag += '<td width="32px"></td>';
                                    tag += '<td width="32px"></td>';
                                    tag += '<td width="32px"></td>';
                                    tag += '<td width="32px" align="center">'+No4+''+No4n+'.</td>';
                                    tag += '<td colspan="2" width="182px">'+v4.butir_kegiatan+'</td>';
                                    tag += '<td width="90px"></td>';
                                    tag += '<td width="85px"></td>';
                                    tag += '<td width="92px"></td>';
                                    tag += '<td width="100px"></td>';
                                    tag += '<td width="92px"></td>';
                                    tag += '</tr>';
                                    if (v4.row.length > 0) {
                                      var No5 = '<i class="fas fa-angle-double-right"></i>';
                                      $.each(v4.row, function(k,v5){
                                        if (v5.statusRow == 'Parent') {
                                          tag += '<tr>';
                                          tag += '<td width="32px"></td>';
                                          tag += '<td width="32px"></td>';
                                          tag += '<td width="32px"></td>';
                                          tag += '<td width="32px"></td>';
                                          tag += '<td width="32px" align="center">'+No5+'.</td>';
                                          tag += '<td width="150px">'+v5.butir_kegiatan+'</td>';
                                          tag += '<td width="90px"></td>';
                                          tag += '<td width="85px"></td>';
                                          tag += '<td width="92px"></td>';
                                          tag += '<td width="100px"></td>';
                                          tag += '<td width="92px"></td>';
                                          tag += '</tr>';
                                        }else{
                                          tag += '<tr>';
                                          tag += '<td rowspan="2" width="32px"></td>';
                                          tag += '<td rowspan="2" width="32px"></td>';
                                          tag += '<td rowspan="2" width="32px"></td>';
                                          tag += '<td rowspan="2" width="32px"></td>';
                                          tag += '<td rowspan="2" width="32px" align="center">'+No5+'.</td>';
                                          tag += '<td width="150px">'+v5.butir_kegiatan+'</td>';
                                          tag += '<td rowspan="2" width="90px" align="center" id="tgl_'+kode+'_'+v5.id_pak_master+'">tgl_'+kode+'_'+v5.id_pak_master+'</td>';
                                          tag += '<td rowspan="2" width="85px" align="center" id="satuan_'+kode+'_'+v5.id_pak_master+'">satuan_'+kode+'_'+v5.id_pak_master+'</td>';
                                          tag += '<td rowspan="2" width="92px" align="center" id="jmlKeg_'+kode+'_'+v5.id_pak_master+'">jmlKeg_'+kode+'_'+v5.id_pak_master+'</td>';
                                          tag += '<td rowspan="2" width="100px" align="center" id="jmlAk_'+kode+'_'+v5.id_pak_master+'">jmlAk_'+kode+'_'+v5.id_pak_master+'</td>';
                                          tag += '<td rowspan="2" width="92px" align="center" id="ketBFis_'+kode+'_'+v5.id_pak_master+'">ketBFis_'+kode+'_'+v5.id_pak_master+'</td>';
                                          tag += '</tr>';
                                          tag += '<tr>';
                                          tag += '<td width="150px">( Tiap '+v5.jum_min+' '+v5.satuan+' '+v5.points+' ) <span style="float:right;">'+v5.jabatan.nama+'</td>';
                                          tag += '</tr>';
                                          if (idPak == '') { idPak = v5.id_pak_master; }
                                          else{ idPak = idPak+","+v5.id_pak_master; }
                                        }
                                      });
                                    }
                                  }else{
                                    tag += '<tr>';
                                    tag += '<td rowspan="2" width="32px"></td>';
                                    tag += '<td rowspan="2" width="32px"></td>';
                                    tag += '<td rowspan="2" width="32px"></td>';
                                    tag += '<td rowspan="2" width="32px" align="center">'+No4+''+No4n+'.</td>';
                                    tag += '<td colspan="2" width="182px">'+v4.butir_kegiatan+'</td>';
                                    tag += '<td rowspan="2" width="90px" align="center" id="tgl_'+kode+'_'+v4.id_pak_master+'"></td>';
                                    tag += '<td rowspan="2" width="85px" align="center" id="satuan_'+kode+'_'+v4.id_pak_master+'"></td>';
                                    tag += '<td rowspan="2" width="92px" align="center" id="jmlKeg_'+kode+'_'+v4.id_pak_master+'"></td>';
                                    tag += '<td rowspan="2" width="100px" align="center" id="jmlAk_'+kode+'_'+v4.id_pak_master+'"></td>';
                                    tag += '<td rowspan="2" width="92px" align="center" id="ketBFis_'+kode+'_'+v4.id_pak_master+'"></td>';
                                    tag += '</tr>';
                                    tag += '<tr>';
                                    tag += '<td colspan="2" width="182px">( Tiap '+v4.jum_min+' '+v4.satuan+' '+v4.points+' ) <span style="float:right;">'+v4.jabatan.nama+'</td>';
                                    tag += '</tr>';
                                    if (idPak == '') { idPak = v4.id_pak_master; }
                                    else{ idPak = idPak+","+v4.id_pak_master; }
                                  }
                                });
                              }
                            }else{
                              tag += '<tr>';
                              tag += '<td rowspan="2" width="32px"></td>';
                              tag += '<td rowspan="2" width="32px"></td>';
                              tag += '<td rowspan="2" width="32px" align="center">'+tempNo3+'.</td>';
                              tag += '<td colspan="3" width="214px">'+v3.butir_kegiatan+'</td>';
                              tag += '<td rowspan="2" width="90px" align="center" id="tgl_'+kode+'_'+v3.id_pak_master+'"></td>';
                              tag += '<td rowspan="2" width="85px" align="center" id="satuan_'+kode+'_'+v3.id_pak_master+'"></td>';
                              tag += '<td rowspan="2" width="92px" align="center" id="jmlKeg_'+kode+'_'+v3.id_pak_master+'"></td>';
                              tag += '<td rowspan="2" width="100px" align="center" id="jmlAk_'+kode+'_'+v3.id_pak_master+'"></td>';
                              tag += '<td rowspan="2" width="92px" align="center" id="ketBFis_'+kode+'_'+v3.id_pak_master+'"></td>';
                              tag += '</tr>';
                              tag += '<tr>';
                              tag += '<td colspan="3" width="214px">( Tiap '+v3.jum_min+' '+v3.satuan+' '+v3.points+' ) <span style="float:right;">'+v3.jabatan.nama+'</td>';
                              tag += '</tr>';
                              if (idPak == '') { idPak = v3.id_pak_master; }
                              else{ idPak = idPak+","+v3.id_pak_master; }
                            }
                          });
                        }
                      }else{
                        tag += '<tr>';
                        tag += '<td rowspan="2" width="32px" align="center"></td>';
                        tag += '<td rowspan="2" width="32px" align="center">'+No2+''+No2n+'.</td>';
                        tag += '<td colspan="4" width="246px">'+v2.butir_kegiatan+'</td>';
                        tag += '<td rowspan="2" width="90px" align="center" id="tgl_'+kode+'_'+v2.id_pak_master+'"></td>';
                        tag += '<td rowspan="2" width="85px" align="center" id="satuan_'+kode+'_'+v2.id_pak_master+'"></td>';
                        tag += '<td rowspan="2" width="92px" align="center" id="jmlKeg_'+kode+'_'+v2.id_pak_master+'"></td>';
                        tag += '<td rowspan="2" width="100px" align="center" id="jmlAk_'+kode+'_'+v2.id_pak_master+'"></td>';
                        tag += '<td rowspan="2" width="92px" align="center" id="ketBFis_'+kode+'_'+v2.id_pak_master+'"></td>';
                        tag += '</tr>';
                        tag += '<tr>';
                        tag += '<td colspan="4" width="246px">( Tiap '+v2.jum_min+' '+v2.satuan+' '+v2.points+' ) <span style="float:right;">'+v2.jabatan.nama+'</td>';
                        tag += '</tr>';
                        if (idPak == '') { idPak = v2.id_pak_master; }
                        else{ idPak = idPak+","+v2.id_pak_master; }
                      }
                    });
                  }
                }else{
                  tag += '<tr>';
                  tag += '<td rowspan="2" width="32px" align="center">'+KonDecRomawi(tempNo1)+'.</td>';
                  tag += '<td colspan="5" width="278px">'+v1.butir_kegiatan+'</td>';
                  tag += '<td rowspan="2" width="90px" align="center" id="tgl_'+kode+'_'+v1.id_pak_master+'"></td>';
                  tag += '<td rowspan="2" width="85px" align="center" id="satuan_'+kode+'_'+v1.id_pak_master+'"></td>';
                  tag += '<td rowspan="2" width="92px" align="center" id="jmlKeg_'+kode+'_'+v1.id_pak_master+'"></td>';
                  tag += '<td rowspan="2" width="100px" align="center" id="jmlAk_'+kode+'_'+v1.id_pak_master+'"></td>';
                  tag += '<td rowspan="2" width="92px" align="center" id="ketBFis_'+kode+'_'+v1.id_pak_master+'"></td>';
                  tag += '</tr>';
                  tag += '<tr>';
                  tag += '<td colspan="5" width="278px">( Tiap '+v1.jum_min+' '+v1.satuan+' '+v1.points+' ) <span style="float:right;">'+v1.jabatan.nama+'</td>';
                  tag += '</tr>';
                  if (idPak == '') { idPak = v1.id_pak_master; }
                  else{ idPak = idPak+","+v1.id_pak_master; }
                }
              });
              tag += '<tr>';
              tag += '<td colspan="6" align="center">JUMLAH</td>';
              tag += '<td width="90px" align="center"></td>'; // tanggal
              tag += '<td width="85px" align="center"></td>'; // satuan
              tag += '<td width="92px" align="center" id="jmlKegUnsur_'+uns.id_master_type+'_'+kode+'"></td>'; // jumlah kegiatan
              tag += '<td width="100px" align="center" id="jmlAkUnsur_'+uns.id_master_type+'_'+kode+'"></td>'; // jumlah ak
              tag += '<td width="92px" align="center"></td>'; // bukti fisik
              tag += '</tr>';
              tag += '</thead>';
              tag += '</table>';
              tag += '<br>';
              tag += '<table>';
              tag += '<tbody>';
              tag += '<tr>';
              tag += '<td colspan="11" align="left">Demikian pernyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="150px"></td>';
              tag += '<td width="90px"></td>';
              tag += '<td width="85px"></td>';
              tag += '<td width="92px">Sidoarjo,</td>';
              tag += '<td width="100px"></td>';
              tag += '<td width="95px"></td>';
              tag += '</tr>';
              tag += '<tr><td></td></tr>';
              tag += '<tr>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="150px"></td>';
              tag += '<td width="90px"></td>';
              tag += '<td width="85px"></td>';
              tag += '<td colspan="2" width="92px">Kepala Puskesmas</td>';
              tag += '<td width="95px"></td>';
              tag += '</tr>';
              tag += '<tr>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="32px"></td>';
              tag += '<td width="150px"></td>';
              tag += '<td width="90px"></td>';
              tag += '<td width="85px"></td>';
              tag += '<td colspan="2" width="92px">'+data.data.kepala.nama+'</td>';              
              tag += '<td width="95px"></td>';
              tag += '</tr>';
              tag += '</tbody>';
              tag += '</table>';
            }
          });
          // $('#content_'+kode).show();
          $("#my-table-id_"+kode).html(tag);
          $(".PanelId_"+kode).html(idPak);
          LoadGrid(kode, tgl_awal, tgl_akhir, id_pengajuan);
        }else{
          swal('Whoops !!', 'Terjadi Kesalahan Silahkan Coba Lagi !!', 'warning');
          $('.buatSPMK_'+kode).show();
          $('.menungguSPMK_'+kode).hide();
          $('.cetakSPMK_'+kode).hide();
        }
      }).fail(function() {
        getDataSpmk(kode, tgl_awal, tgl_akhir);
      });
    }

    function LoadGrid(kode, tgl_awal, tgl_akhir, id_pengajuan) {
      var arrayId = $(".PanelId_"+kode).text().split(',');
      var totalArrayId = arrayId.length;
      var tempId = 0
      function valGrid(tempId) {
        if (tempId >= totalArrayId) {
          console.log('berhenti : '+kode);
          $('.menungguSPMK_'+kode).hide();
          $('.cetakSPMK_'+kode).show();
          getNilaiTotal(kode, tgl_awal, tgl_akhir, id_pengajuan);
          return ;
        }
        var nilai_persen = (tempId / totalArrayId) * 100;
        $('.menungguSPMK_'+kode).html('<span class="fas fa-hourglass-start"></span> <i>Memuat Data ... </i> '+nilai_persen.toFixed(0)+' %');
        return $.ajax({
          url: "{{ route('getNilaiSpmk') }}?idPengajuan="+id_pengajuan+"&tglAwal="+tgl_awal+"&tglAkhir="+tgl_akhir+"&idPak="+arrayId[tempId], success: function(result){
            if (result.status == 'success') {
              $('#tgl_'+kode+'_'+arrayId[tempId]).html(result.row.tanggal);
              $('#satuan_'+kode+'_'+arrayId[tempId]).html(result.row.satuan);
              $('#jmlKeg_'+kode+'_'+arrayId[tempId]).html(result.row.jumlah_px);
              $('#jmlAk_'+kode+'_'+arrayId[tempId]).html(result.row.jumlah_point);
            }else{
              return valGrid(tempId);
            }
          },
          error:function() {
            return valGrid(tempId);
          }
        }).then(function () {
          return valGrid(tempId + 1);
        });
      }
      valGrid(tempId);
    }

    function getDataRekapTahun(kode, tgl_awal, tgl_akhir, judul) {
      console.log("kode = "+kode+" | awal = "+tgl_awal+" | akhir = "+tgl_akhir);
      $('.button_'+kode).html('<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="Waiting()" style="width:310px;margin:5px;"><span class="fas fa-hourglass-start"></span> <i>Memuat Data ... </i></a>');
      $("#my-table-id_"+kode).html('');
      var id_pengajuan = {{ $data['pengajuan']->id_pengajuan }};
      var tag = '';
      var idPak = '';
      $.post("{!! route('getDataRekapTahun') !!}",{kode:kode,tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,id_pengajuan:id_pengajuan}).done(function(data){
        if (data.status == 'success') {
          tag += '<table>';
            tag += '<tbody>';
              tag += '<tr><td colspan="11" align="center" style="font-size:14pt;font-weight: bold">REKAP KEGIATAN HARIAN</td></tr>';
              tag += '<tr><td colspan="11" align="center" style="font-size:14pt;font-weight: bold">'+judul.toUpperCase()+'</td></tr>';
              tag += '<tr><td></td></tr>';
              // tag += '<tr><td colspan="6">Yang bertanda tangan dibawah ini :</td></tr>';
              tag += '<tr>';
                tag += '<td width="32px" align="left">1</td>';
                tag += '<td colspan="5" width="276px" align="left">Nama</td>';
                tag += '<td>: '+data.data.pegawai.nama+'</td>';
              tag += '</tr>';
              tag += '<tr>';
                tag += '<td width="32px" align="left">2</td>';
                tag += '<td colspan="5" width="276px" align="left">NIP</td>';
                tag += '<td>: '+data.data.pegawai.nip+'</td>';
              tag += '</tr>';
              tag += '<tr>';
                tag += '<td width="32px" align="left">3</td>';
                tag += '<td colspan="5" width="276px" align="left">Pangkat Golongan Ruang/TMT </td>';
                tag += '<td>: '+data.data.pegawai.golongan+' / '+data.data.pegawai.golonganTmt+'</td>';
              tag += '</tr>';
              tag += '<tr>';
                tag += '<td width="32px" align="left">4</td>';
                tag += '<td colspan="5" width="276px" align="left">Jabatan </td>';
                tag += '<td>: '+data.data.pegawai.jabatan+'</td>';
              tag += '</tr>';
              tag += '<tr>';
                tag += '<td width="32px" align="left">5</td>';
                tag += '<td colspan="5" width="276px" align="left">Unit Kerja </td>';
                tag += '<td>: '+data.data.pegawai.unitKerja+'</td>';
              tag += '</tr>';
            tag += '</tbody>';
          tag += '</table>';
          tag += '<br>';
          tag += '<table id="body_excel" border="1">';
            tag += '<thead>';
              tag += '<tr>';
                tag += '<td rowspan="2" width="32px" align="center">No</td>';
                tag += '<td rowspan="2" colspan="5" align="center">Uraian Kegiatan</td>';
                tag += '<td rowspan="2" width="90px" align="center">Tanggal</td>';
                tag += '<td width="85px" align="center">Satuan</td>';
                tag += '<td width="92px" align="center">Juml Vol</td>';
                tag += '<td width="100px" align="center">Juml</td>';
                tag += '<td width="92px" align="center">Ket</td>';
              tag += '</tr>';
              tag += '<tr>';
                tag += '<td width="85px" align="center">Hasil</td>';
                tag += '<td width="92px" align="center">Kegiatan</td>';
                tag += '<td width="100px" align="center">AK</td>';
                tag += '<td width="92px" align="center">Bukti Fisik</td>';
              tag += '</tr>';
              tag += '<tr>';
                tag += '<td width="32px" align="center">1</td>';
                tag += '<td colspan="5" align="center">2</td>';
                tag += '<td width="90px" align="center">3</td>';
                tag += '<td width="85px" align="center">4</td>';
                tag += '<td width="92px" align="center">5</td>';
                tag += '<td width="100px" align="center">6</td>';
                tag += '<td width="92px" align="center">7</td>';
              tag += '</tr>';
              $.each(data.data.butir_kegiatan, function(k,uns){
                tag += '<tr>';
                  tag += '<td width="32px"></td>';
                  tag += '<td colspan="5">'+uns.nama+'</td>';
                  tag += '<td width="90px"></td>';
                  tag += '<td width="85px"></td>';
                  tag += '<td width="92px"></td>';
                  tag += '<td width="100px"></td>';
                  tag += '<td width="92px"></td>';
                tag += '</tr>';
                if (uns.row.length > 0) {
                  var tempNo1 = 0;
                  $.each(uns.row, function(k,v1){
                    tempNo1++;
                    if (v1.statusRow == 'Parent') {
                      tag += tagRow(v1, KonDecRomawi(tempNo1), kode, 'parent');
                      if (v1.row.length > 0) {
                        var tempNo2 = 0; // Hurug Awal
                        var tempNo2n = 0; // Hurug Akhir
                        var No2 = '';
                        var No2n = '';
                        $.each(v1.row, function(k,v2){
                          if (tempNo2n == 26) {
                            tempNo2++;
                            No2 = String.fromCharCode(64 + tempNo2);
                          }
                          tempNo2n++;
                          No2n = No2+''+String.fromCharCode(64 + tempNo2n);
                          if (v2.statusRow == 'Parent') {
                            tag += tagRow(v2, No2n, kode, 'parent');
                            if (v2.row.length > 0) {
                              var tempNo3 = 0;
                              $.each(v2.row, function(k,v3){
                                tempNo3++;
                                if (v3.statusRow == 'Parent') {
                                  tag += tagRow(v3, tempNo3, kode, 'parent');
                                  if (v3.row.length > 0) {
                                    var tempNo4 = 0; // Hurug Awal
                                    var tempNo4n = 0; // Hurug Akhir
                                    var No4 = '';
                                    var No4n = '';
                                    $.each(v3.row, function(k,v4){
                                      if (tempNo4n == 26) {
                                        tempNo4++;
                                        No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                      }
                                      tempNo4n++;
                                      No4n = No4+''+String.fromCharCode(64 + tempNo4n).toLowerCase();
                                      if (v4.statusRow == 'Parent') {
                                        tag += tagRow(v4, No4n, kode, 'parent');
                                        if (v4.row.length > 0) {
                                          var No5 = '<i class="fas fa-angle-double-right"></i>';
                                          $.each(v4.row, function(k,v5){
                                            if (v5.statusRow == 'Parent') {
                                              tag += tagRow(v5, No5, kode, 'parent');
                                            }else{
                                              tag += tagRow(v5, No5, kode, 'child');
                                              if (idPak == '') { idPak = v5.id_pak_master; }
                                              else{ idPak = idPak+","+v5.id_pak_master; }
                                            }
                                          });
                                        }
                                      }else{
                                        tag += tagRow(v4, No4n, kode, 'child');
                                        if (idPak == '') { idPak = v4.id_pak_master; }
                                        else{ idPak = idPak+","+v4.id_pak_master; }
                                      }
                                    });
                                  }
                                }else{
                                  tag += tagRow(v3, tempNo3, kode, 'child');
                                  if (idPak == '') { idPak = v3.id_pak_master; }
                                  else{ idPak = idPak+","+v3.id_pak_master; }
                                }
                              });
                            }
                          }else{
                            tag += tagRow(v2, No2n, kode, 'child');
                            if (idPak == '') { idPak = v2.id_pak_master; }
                            else{ idPak = idPak+","+v2.id_pak_master; }
                          }
                        });
                      }
                    }else{
                      tag += tagRow(v1, KonDecRomawi(tempNo1), kode, 'child');
                      if (idPak == '') { idPak = v1.id_pak_master; }
                      else{ idPak = idPak+","+v1.id_pak_master; }
                    }
                  });
                }else{
                  var colTotal = result.row.jumlahTanggal + 4;
                  tag += '<tr><th colspan="11" class="text-left">.:: Data Butir Unsur Tidak Ditemukan ::.</th></tr>';
                }
              });
              tag += '<tr>';
                tag += '<td colspan="6" align="center">JUMLAH</td>';
                tag += '<td width="90px" align="center"></td>'; // tanggal
                tag += '<td width="85px" align="center"></td>'; // satuan
                tag += '<td width="92px" align="center" id="jmlKegUnsur_'+kode+'"></td>'; // jumlah kegiatan
                tag += '<td width="100px" align="center" id="jmlAkUnsur_'+kode+'"></td>'; // jumlah ak
                tag += '<td width="92px" align="center"></td>'; // bukti fisik
              tag += '</tr>';
            tag += '</thead>';
          tag += '</table>';
          $("#my-table-id_"+kode).html(tag);
          LoadGridRekapTahun(kode, tgl_awal, tgl_akhir, id_pengajuan, idPak, judul);
        }else{
          swal('Whoops !!', 'Terjadi Kesalahan Silahkan Coba Lagi !!', 'warning');
          $('.button_'+kode).html('<a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="getDataRekapTahun(\''+kode+'\', \''+tgl_awal+'\', \''+tgl_akhir+'\', \''+judul+'\')" style="width:310px;margin:5px;"><span class="fas fa-print"></span> Buat Rekap '+judul+'</a>');
        }
      }).fail(function() {
        getDataRekapTahun(kode, tgl_awal, tgl_akhir);
      });
    }

    function LoadGridRekapTahun(kode, tgl_awal, tgl_akhir, id_pengajuan, idPak, judul) {
      $.post("{!! route('getNilaiRekapTahun') !!}",{kode:kode,tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,id_pengajuan:id_pengajuan,id_paks:idPak}).done(function(result){
        var idpaks = idPak.split(',');
        var arrPaks = [];
        for (var i = 0; i < idpaks.length; i++) {
          arrPaks[i] = {'id_pak' : idpaks[i], 'jumlah_px' : 0, 'jumlah_point' : 0};
        }
        var totalAk = 0;
        var totalPoint = 0;
        if (result.status == 'success') {
          $.each(result.row, function(k,v){
            for (var j = 0; j < arrPaks.length; j++) {
              if (v.master_kegiatan_id == arrPaks[j]['id_pak']) {
                arrPaks[j]['jumlah_px'] = parseFloat(arrPaks[j]['jumlah_px']) + parseFloat(v.jumlah_px_pemohon);
                arrPaks[j]['jumlah_point'] = parseFloat(arrPaks[j]['jumlah_point']) + parseFloat(v.poin_pemohon);
              }
            }
          });

          for (var s = 0; s < arrPaks.length; s++) {
            $('#tgl_'+kode+'_'+arrPaks[s]['id_pak']).html(tglIndo(tgl_awal)+' s/d '+tglIndo(tgl_akhir));
            $('#jmlKeg_'+kode+'_'+arrPaks[s]['id_pak']).html(arrPaks[s]['jumlah_px']);
            $('#jmlAk_'+kode+'_'+arrPaks[s]['id_pak']).html(arrPaks[s]['jumlah_point']);
            $('#ketBFis_'+kode+'_'+arrPaks[s]['id_pak']).html('');
            totalAk = parseFloat(totalAk) + parseFloat(arrPaks[s]['jumlah_px']);
            totalPoint = parseFloat(totalPoint) + parseFloat(arrPaks[s]['jumlah_point']);
          }
          $('#jmlKegUnsur_'+kode).html(totalAk);
          $('#jmlAkUnsur_'+kode).html(totalPoint);
        }
        $('.button_'+kode).html('<a href="javascript:void(0)" class="btn btn-success btn-xs" onclick="tableToExcel(\'my-table-id_'+kode+'\', \'REKAP '+judul+'\')" style="width:310px;margin:5px;"><span class="fas fa-print"></span> Cetak REKAP REKAP '+judul+'</a>');
      }).fail(function() {
        LoadGridRekapTahun(kode, tgl_awal, tgl_akhir, id_pengajuan, idPak);
      });
    }

    var tableToExcel = (function() {
			var uri = 'data:application/vnd.ms-excel;base64,'
				, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
				, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
				, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			return function(table, name) {
				if (!table.nodeType) table = document.getElementById(table)
				var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
				window.location.href = uri + base64(format(template, ctx))
			}
		})();

    function getNilaiTotal(kode, tgl_awal, tgl_akhir, id_pengajuan) {
      $.post("{!! route('getJumlahNilaiSpmk') !!}",{kode:kode,tgl_awal:tgl_awal,tgl_akhir:tgl_akhir,id_pengajuan:id_pengajuan}).done(function(data){
        if (data.status == 'success') {
          $.each(data.row, function(k,v){
            $('#jmlKegUnsur_'+v.id_master_type+'_'+kode).html(v.jumlah_px);
            $('#jmlAkUnsur_'+v.id_master_type+'_'+kode).html(v.jumlah_point);
          });
        }else{
          swal('Whoops !!', 'Terjadi Kesalahan Silahkan Coba Lagi !!', 'warning');
          getNilaiTotal(kode, tgl_awal, tgl_akhir, id_pengajuan);
        }
      }).fail(function() {
        getNilaiTotal(kode, tgl_awal, tgl_akhir, id_pengajuan);
      });
    }


    function cetakKesehatan(){
      $('.resultDataButir').html('<tr><td colspan="25" align="center">Memuat data...</td></tr>');
      var id_profesi = {{ $data['pegawai']->typeProfesi->id_master_type }};
      $.post("{!! route('getCetakPernyataanPelayanan') !!}", {id_profesi:id_profesi}).done(function(data){
        if (data.status == 'success') {
          var tag = '';
          // var No = '';
          // var currentCode = 0;
          if (data.row.butir.length != 0) {
            $.each(data.row.butir, function(k,uns){
              tag += '<tr>';
              tag += '<td></td>';
              tag += '<td></td>';
              tag += '<td colspan="21" width="800px" style="font-weight:bold">'+uns.butir_kegiatan+'</td>';
              tag += '<td></td>';
              tag += '<td></td>';
              tag += '</tr>';
              if (uns.row.length > 0) {
                var tempNo1 = 0;
                $.each(uns.row, function(k,v1){
                  tempNo1++;
                  if (v1.statusRow == 'Parent') {
                    tag += '<tr>';
                    tag += '<td align="center" class="pl-0 pr-1">';
                    tag += '<label class="custom-control custom-checkbox m-0">';
                    tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v1.id_pak_master+'" class="custom-control-input">';
                    tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                    tag += '</label>';
                    tag += '</td>';
                    tag += '<td align="center" style="vertical-align:top">'+KonDecRomawi(tempNo1)+'.</td>';
                    tag += '<td colspan="21" width="800px">'+v1.butir_kegiatan+'</td>';
                    tag += '<td></td>';
                    tag += '<td></td>';
                    tag += '</tr>';
                    if (v1.row.length > 0) {
                      var tempNo2 = 0; // Hurug Awal
                      var tempNo2n = 0; // Hurug Akhir
                      var No2 = '';
                      var No2n = '';
                      $.each(v1.row, function(k,v2){
                        if (tempNo2n == 26) {
                          tempNo2++;
                          No2 = String.fromCharCode(64 + tempNo2);
                        }
                        tempNo2n++;
                        No2n = String.fromCharCode(64 + tempNo2n);
                        if (v2.statusRow == 'Parent') {
                          tag += '<tr>';
                          tag += '<td align="center" class="pl-0 pr-1">';
                          tag += '<label class="custom-control custom-checkbox m-0">';
                          tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v2.id_pak_master+'" class="custom-control-input">';
                          tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                          tag += '</label>';
                          tag += '</td>';
                          tag += '<td></td>';
                          tag += '<td align="center" style="vertical-align:top">'+No2+''+No2n+'.</td>';
                          tag += '<td colspan="20" width="760px">'+v2.butir_kegiatan+'</td>';
                          tag += '<td></td>';
                          tag += '<td></td>';
                          tag += '</tr>';
                          if (v2.row.length > 0) {
                            var tempNo3 = 0;
                            $.each(v2.row, function(k,v3){
                              tempNo3++;
                              if (v3.statusRow == 'Parent') {
                                tag += '<tr>';
                                tag += '<td align="center" class="pl-0 pr-1">';
                                tag += '<label class="custom-control custom-checkbox m-0">';
                                tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v3.id_pak_master+'" class="custom-control-input">';
                                tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                tag += '</label>';
                                tag += '</td>';
                                tag += '<td></td>';
                                tag += '<td></td>';
                                tag += '<td align="center" style="vertical-align:top">'+tempNo3+'.</td>';
                                tag += '<td colspan="19" width="720px">'+v3.butir_kegiatan+'</td>';
                                tag += '<td></td>';
                                tag += '<td></td>';
                                tag += '</tr>';
                                if (v3.row.length > 0) {
                                  var tempNo4 = 0; // Hurug Awal
                                  var tempNo4n = 0; // Hurug Akhir
                                  var No4 = '';
                                  var No4n = '';
                                  $.each(v3.row, function(k,v4){
                                    if (tempNo4n == 26) {
                                      tempNo4++;
                                      No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                    }
                                    tempNo4n++;
                                    No4n = String.fromCharCode(64 + tempNo4n).toLowerCase();
                                    if (v4.statusRow == 'Parent') {
                                      tag += '<tr>';
                                      tag += '<td align="center" class="pl-0 pr-1">';
                                      tag += '<label class="custom-control custom-checkbox m-0">';
                                      tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v4.id_pak_master+'" class="custom-control-input">';
                                      tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                      tag += '</label>';
                                      tag += '</td>';
                                      tag += '<td></td>';
                                      tag += '<td></td>';
                                      tag += '<td></td>';
                                      tag += '<td align="center" style="vertical-align:top">'+No4+''+No4n+'.</td>';
                                      tag += '<td colspan="18" width="680px">'+v4.butir_kegiatan+'</td>';
                                      tag += '<td></td>';
                                      tag += '<td></td>';
                                      tag += '</tr>';
                                      if (v4.row.length > 0) {
                                        var No5 = '<i class="fas fa-angle-double-right"></i>';
                                        $.each(v4.row, function(k,v5){
                                          if (v5.statusRow == 'Parent') {
                                            tag += '<tr>';
                                            tag += '<td align="center" class="pl-0 pr-1">';
                                            tag += '<label class="custom-control custom-checkbox m-0">';
                                            tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v5.id_pak_master+'" class="custom-control-input">';
                                            tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                            tag += '</label>';
                                            tag += '</td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '<td align="center" style="vertical-align:top">'+No5+'</td>';
                                            tag += '<td colspan="17" width="640px">'+v5.butir_kegiatan+'</td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '</tr>';
                                            if (v5.row.length > 0) {
                                              var No6 = '<i class="fas fa-terminal"></i>';
                                              $.each(v5.row, function(k,v6){
                                                if (v6.statusRow == 'Parent') {
                                                  tag += '<tr>';
                                                  tag += '<td align="center" class="pl-0 pr-1">';
                                                  tag += '<label class="custom-control custom-checkbox m-0">';
                                                  tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v6.id_pak_master+'" class="custom-control-input">';
                                                  tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                                  tag += '</label>';
                                                  tag += '</td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td align="center" style="vertical-align:top">'+No6+'</td>';
                                                  tag += '<td colspan="16" width="600px">'+v6.butir_kegiatan+'</td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '</tr>';
                                                }else{
                                                  tag += '<tr>';
                                                  tag += '<td align="center" class="pl-0 pr-1">';
                                                  tag += '<label class="custom-control custom-checkbox m-0">';
                                                  tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v6.id_pak_master+'" class="custom-control-input">';
                                                  tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                                  tag += '</label>';
                                                  tag += '</td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td></td>';
                                                  tag += '<td align="center" style="vertical-align:top">'+No6+'</td>';
                                                  tag += '<td colspan="16" width="600px">'+v6.butir_kegiatan+'</td>';
                                                  tag += '<td>( Tiap '+v6.jum_min+' '+v6.satuan+' '+v6.points+' )</td>';
                                                  tag += '<td>'+v6.jabatan.nama+'</td>';
                                                  tag += '</tr>';
                                                }
                                              });
                                            }
                                          }else{
                                            tag += '<tr>';
                                            tag += '<td align="center" class="pl-0 pr-1">';
                                            tag += '<label class="custom-control custom-checkbox m-0">';
                                            tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v5.id_pak_master+'" class="custom-control-input">';
                                            tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                            tag += '</label>';
                                            tag += '</td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '<td></td>';
                                            tag += '<td align="center" style="vertical-align:top">'+No5+'</td>';
                                            tag += '<td colspan="17" width="640px">'+v5.butir_kegiatan+'</td>';
                                            tag += '<td>( Tiap '+v5.jum_min+' '+v5.satuan+' '+v5.points+' )</td>';
                                            tag += '<td>'+v5.jabatan.nama+'</td>';
                                            tag += '</tr>';
                                          }
                                        });
                                      }
                                    }else{
                                      tag += '<tr>';
                                      tag += '<td align="center" class="pl-0 pr-1">';
                                      tag += '<label class="custom-control custom-checkbox m-0">';
                                      tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v4.id_pak_master+'" class="custom-control-input">';
                                      tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                      tag += '</label>';
                                      tag += '</td>';
                                      tag += '<td></td>';
                                      tag += '<td></td>';
                                      tag += '<td></td>';
                                      tag += '<td align="center" style="vertical-align:top">'+No4+''+No4n+'.</td>';
                                      tag += '<td colspan="18" width="680px">'+v4.butir_kegiatan+'</td>';
                                      tag += '<td>( Tiap '+v4.jum_min+' '+v4.satuan+' '+v4.points+' )</td>';
                                      tag += '<td>'+v4.jabatan.nama+'</td>';
                                      tag += '</tr>';
                                    }
                                  });
                                }
                              }else{
                                tag += '<tr>';
                                tag += '<td align="center" class="pl-0 pr-1">';
                                tag += '<label class="custom-control custom-checkbox m-0">';
                                tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v3.id_pak_master+'" class="custom-control-input">';
                                tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                tag += '</label>';
                                tag += '</td>';
                                tag += '<td></td>';
                                tag += '<td></td>';
                                tag += '<td align="center" style="vertical-align:top">'+tempNo3+'.</td>';
                                tag += '<td colspan="19" width="720px">'+v3.butir_kegiatan+'</td>';
                                tag += '<td>( Tiap '+v3.jum_min+' '+v3.satuan+' '+v3.points+' )</td>';
                                tag += '<td>'+v3.jabatan.nama+'</td>';
                                tag += '</tr>';
                              }
                            });
                          }
                        }else{
                          tag += '<tr>';
                          tag += '<td align="center" class="pl-0 pr-1">';
                          tag += '<label class="custom-control custom-checkbox m-0">';
                          tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v2.id_pak_master+'" class="custom-control-input">';
                          tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                          tag += '</label>';
                          tag += '</td>';
                          tag += '<td></td>';
                          tag += '<td align="center" style="vertical-align:top">'+No2+''+No2n+'.</td>';
                          tag += '<td colspan="20" width="760px">'+v2.butir_kegiatan+'</td>';
                          tag += '<td>( Tiap '+v2.jum_min+' '+v2.satuan+' '+v2.points+' )</td>';
                          tag += '<td>'+v2.jabatan.nama+'</td>';
                          tag += '</tr>';
                        }
                      });
                    }
                  }else{
                    tag += '<tr>';
                    tag += '<td align="center" class="pl-0 pr-1">';
                    tag += '<label class="custom-control custom-checkbox m-0">';
                    tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v1.id_pak_master+'" class="custom-control-input">';
                    tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                    tag += '</label>';
                    tag += '</td>';
                    tag += '<td align="center" style="vertical-align:top">'+KonDecRomawi(tempNo1)+'.</td>';
                    tag += '<td colspan="21" width="800px">'+v1.butir_kegiatan+'</td>';
                    tag += '<td>( Tiap '+v1.jum_min+' '+v1.satuan+' '+v1.points+' )</td>';
                    tag += '<td>'+v1.jabatan.nama+'</td>';
                    tag += '</tr>';
                  }
                });
              }
            });
          }else{
            tag += '<tr><td colspan="25" align="center">Tidak ada data ditemukan...</td></tr>';
          }
          $('.resultDataButir').empty();
          $('.resultDataButir').html(tag);
        } else if(data.status == 'empty') {
          tag += '<tr><td colspan="25" align="center">Tidak ada data ditemukan...</td></tr>';
          $('.resultDataButir').empty();
          $('.resultDataButir').html(tag);
        } else {
          swal("MAAF !","Terjadi Kesalahan !!", "warning");
        }
        console.log(tag);
      }).fail(function() {
        getDataMasterLampiran();
      });
    }

    function KonDecRomawi(angka) {
      var hasil = "";
      if (angka < 1 || angka > 5000) {
        hasil += "Batas Angka 1 s/d 5000";
      }else{
        while (angka >= 1000) {
          hasil += "M";
          angka = angka - 1000;
        }
      }

      if (angka >= 500) {
        if (angka > 500) {
          if (angka >= 900) {
            hasil += "CM";
            angka = angka - 900;
          } else {
            hasil += "D";
            angka = angka - 500;
          }
        }
      }

      while (angka >= 100) {
        if (angka >= 400) {
          hasil += "CD";
          angka = angka - 400;
        } else {
          angka = angka - 100;
        }
      }

      if (angka >= 50) {
        if (angka >= 90) {
          hasil += "XC";
          angka = angka - 90;
        } else {
          hasil += "L";
          angka = angka - 50;
        }
      }

      while (angka >= 10) {
        if (angka >= 40) {
          hasil += "XL";
          angka = angka - 40;
        } else {
          hasil += "X";
          angka = angka - 10;
        }
      }

      if (angka >= 5) {
        if (angka == 9) {
          hasil += "IX";
          angka = angka - 9;
        } else {
          hasil += "V";
          angka = angka - 5;
        }
      }

      while (angka >= 1) {
        if (angka == 4) {
          hasil += "IV";
          angka = angka - 4;
        } else {
          hasil += "I";
          angka = angka - 1;
        }
      }
      return hasil;
    }

    function tagRow(keyData, nomor, kode, status) {
      var tag = '';
      tag += '<tr>';
      if (status == 'parent') {
        var textRowSpan = '';
        var borders = '';
        var satuan = '';
      }else{
        var textRowSpan = 'rowspan="2" ';
        var borders = 'border-bottom:none;';
        var satuan = keyData.satuan;
      }
      var tagNo = '';
      var jmColspan = '';
      var jmWidthCol = '';
      switch(keyData.level) {
        case 1:
          tagNo += '<td '+textRowSpan+'width="32px" align="center" style="vertical-align:top">'+nomor+'.</td>';
          jmColspan = 5; jmWidthCol = '278px';
          break;
        case 2:
          tagNo += '<td '+textRowSpan+'width="32px" align="center"></td>';
          tagNo += '<td '+textRowSpan+'width="32px" align="center" style="vertical-align:top">'+nomor+'.</td>';
          jmColspan = 4; jmWidthCol = '246px';
          break;
        case 3:
          tagNo += '<td '+textRowSpan+'width="32px"></td>';
          tagNo += '<td '+textRowSpan+'width="32px"></td>';
          tagNo += '<td '+textRowSpan+'width="32px" align="center" style="vertical-align:top">'+nomor+'.</td>';
          jmColspan = 3; jmWidthCol = '214px';
          break;
        case 4:
          tagNo += '<td '+textRowSpan+'width="32px"></td>';
          tagNo += '<td '+textRowSpan+'width="32px"></td>';
          tagNo += '<td '+textRowSpan+'width="32px"></td>';
          tagNo += '<td '+textRowSpan+'width="32px" align="center" style="vertical-align:top">'+nomor+'.</td>';
          jmColspan = 2; jmWidthCol = '182px';
          break;
        case 5:
          tagNo += '<td width="32px"></td>';
          tagNo += '<td width="32px"></td>';
          tagNo += '<td width="32px"></td>';
          tagNo += '<td width="32px"></td>';
          tagNo += '<td width="32px" align="center">'+nomor+'.</td>';
          jmColspan = 1; jmWidthCol = '150px';
          break;
      }
      tag += tagNo;
      tag += '<td colspan="'+jmColspan+'" width="'+jmWidthCol+'" style="'+borders+'">'+keyData.butir_kegiatan+'</td>';
      tag += '<td '+textRowSpan+'width="90px" align="center" id="tgl_'+kode+'_'+keyData.id_pak_master+'" style="vertical-align:middle;"></td>';
      tag += '<td '+textRowSpan+'width="85px" align="center" id="satuan_'+kode+'_'+keyData.id_pak_master+'" style="vertical-align:middle;">'+satuan+'</td>';
      tag += '<td '+textRowSpan+'width="92px" align="center" id="jmlKeg_'+kode+'_'+keyData.id_pak_master+'" style="vertical-align:middle;"></td>';
      tag += '<td '+textRowSpan+'width="100px" align="center" id="jmlAk_'+kode+'_'+keyData.id_pak_master+'" style="vertical-align:middle;"></td>';
      tag += '<td '+textRowSpan+'width="92px" align="center" id="ketBFis_'+kode+'_'+keyData.id_pak_master+'" style="vertical-align:middle;"></td>';
      tag += '</tr>';
      if (status != 'parent') {
        tag += '<tr>';
        tag += '<td colspan="'+jmColspan+'" width="'+jmWidthCol+'" style="border-top:none">( Tiap '+keyData.jum_min+' '+keyData.satuan+' '+keyData.points+' ) <span style="float:right;">'+keyData.jabatan.nama+'</td>';
        tag += '</tr>';
      }
      return tag;
    }

    function tglIndo(tgl) {
      var pecah = tgl.split('-');

      return pecah[2]+'-'+pecah[1]+'-'+pecah[0];
    }
  </script>
@stop
