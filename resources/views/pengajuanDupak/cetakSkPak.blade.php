<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>SK Penetapan Angka Kredit</title>
    <style media="screen">
      @page { margin: 15px;font-family: Arial, Helvetica, sans-serif;font-size: 12px; }
      body{margin:15px;font-family: Arial, Helvetica, sans-serif;font-size: 12px; }
      .title{
        text-align: center;
        font-size : 14px;
      }
      .bold {
        font-weight: bold;
      }
      .row, .clearfix{
        overflow: auto;
      }
      .row::after, .clearfix::after {
        content: "";
        clear: both;
        display: table;
      }
      table {
        width: 100%;
      }
      table.utama, table.utama tbody td,table.utama tbody tr, table.utama thead tr,table.utama thead th{
        border :1px solid black;
        border-spacing: 0px;
        font-size: 12px;
      }
      table.utama tbody td{
        padding : 5px;
      }
      .noBottomBorder, .noBottomBorder td {
        border-bottom: transparent !important;
      }
      .noTopBorder, .noTopBorder td {
        border-top: transparent !important;
      }
      .noTopBottomBorder, .noTopBottomBorder td {
        border-bottom: transparent !important;
        border-top: transparent !important;
      }
      .noLeftRightBorder {
        border-left: transparent !important;
        border-right: transparent !important;
      }
      .noLeftBorder {
        border-left: transparent !important;
      }
      .noRightBorder {
        border-right: transparent !important;
      }
    </style>
  </head>
  <body>
    <div class="title">PEMERINTAH KABUPATEN SIDOARJO</div>
    <div class="title bold">PENETAPAN ANGKA KREDIT</div>
    <div class="title bold">JABATAN FUNGSIONAL {{ strtoupper($pegawai->typeProfesi->nama) }}</div>
    {{-- <div class="title bold">Nomor : 860 / 161 / 438.5.2 / @{{ date('Y') }}</div> --}}
    <div class="title bold">Nomor : {{ $pengajuan->no_skpak }}</div>
    <br>
    <div class="row">
      <div class="bold" style="float:left">UNIT KERJA : DINAS KESEHATAN</div>
      <div class="bold" style="float:right">MASA PENILAIAN : TGL. {{ date('d-m-Y', strtotime($pengajuan->tgl_awal_penilaian)) }} S/D {{ date('d-m-Y', strtotime($pengajuan->tgl_akhir_penilaian)) }}</div>
    </div>
    <table class="utama" border="1">
      <tr>
        <td width='15px' align='center'>I.</td>
        <td colspan="10">KETERANGAN PERORANGAN</td>
      </tr>
      <tr class="noBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>1.</td>
        <td colspan="4" class="noRightBorder">Nama</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pegawai->nama }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>2.</td>
        <td colspan="4" class="noRightBorder">NIP / No. Seri Karpeg</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pegawai->no_nip }} / {{ $pegawai->no_karpeng }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>3.</td>
        <td colspan="4" class="noRightBorder">Tempat, Tanggal Lahir</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pegawai->tempat_lahir }} / {{ App\Http\Libraries\Formatters::get_tanggal_lengkap($pegawai->tanggal_lahir) }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>4.</td>
        <td colspan="4" class="noRightBorder">Jenis Kelamin</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder"><?php if($pegawai->jenis_kelamin == 'P'){ echo "Laki-Laki"; }else{ echo "Perempuan"; } ?></td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>5.</td>
        <td colspan="4" class="noRightBorder">Pendidikan yang telah diperhitungkan angka kreditnya</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pegawai->pendidikan_terakhir }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>6.</td>
        <td colspan="4" class="noRightBorder">Pendidikan yang diusulkan angka kreditnya</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">-</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>7.</td>
        <td colspan="4" class="noRightBorder">Pangkat/golongan ruang, TMT</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pengajuan->golonganPemohonLama->nama }}, {{ date('d M Y', strtotime($pegawai->golongan_tmt)) }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>8.</td>
        <td colspan="4" class="noRightBorder">Jabatan {{ $pegawai->typeProfesi->nama }}, TMT</td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pengajuan->jabatanPemohonLama->nama }}, {{ date('d M Y', strtotime($pegawai->jabatan_tmt)) }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>9.</td>
        <td colspan="3" class="noRightBorder">Masa kerja golongan</td>
        <td class="noLeftRightBorder"><span style="float:right;margin-right:40px;">Lama</span></td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pengajuan->masa_kerja_lama }}</td>
      </tr>
      <tr class="noTopBottomBorder">
        <td></td>
        <td width='15px' align='center'></td>
        <td colspan="3" class="noRightBorder"></td>
        <td class="noLeftRightBorder"><span style="float:right;margin-right:40px;">Baru</span></td>
        <td class="noLeftRightBorder" align='center' width='5px'>:</td>
        <td colspan="4" class="noLeftBorder">{{ $pengajuan->masa_kerja_baru }}</td>
      </tr>
      <tr class="noTopBorder">
        <td></td>
        <td width='15px' align='center' valign='top'>10.</td>
        <td colspan="4" class="noRightBorder" valign='top'>Unit Kerja</td>
        <td class="noLeftRightBorder" align='center' width='5px' valign='top'>:</td>
        <td colspan="4" class="noLeftBorder" valign='top'>{{ $pegawai->satuanKerja->nama }}<br>KABUPATEN SIDOARJO</td>
      </tr>
      <tr>
        <td width='15px' align='center'>II.</td>
        <td colspan="7">PENETAPAN ANGKA KREDIT</td>
        <td width='80px' align='center'>LAMA</td>
        <td width='80px' align='center'>BARU</td>
        <td width='80px' align='center'>JUMLAH</td>
      </tr>
      <tr>
        <td width='15px' rowspan="11"></td>
        <td rowspan="7" width='15px' align='center' valign='top'>1.</td>
        <td colspan="6">UNSUR UTAMA</td>
        <td width='80px' align='right'></td>
        <td width='80px' align='right'></td>
        <td width='80px' align='right'></td>
      </tr>
      <?php $char = range('a', 'z'); $tempChar = 0;$tempPendidikan = 0;$jumlahLama = 0;$jumlahBaru = 0;$jumlahTotal = 0; ?>
      @for ($i=0; $i < count($nilaiPakLamaUtama); $i++)
        @if ($nilaiPakLamaUtama[$i]['nama'] == 'Pendidikan')
          @if ($tempPendidikan == 0)
            <tr>
              <td rowspan="3" width='15px' align='center' valign='top' class="noRightBorder">{{ $char[$tempChar] }}.</td>
              <td colspan="5" class="noLeftBorder noBottomBorder">Pendidikan</td>
              <td width='80px' align='right' class="noBottomBorder"></td>
              <td width='80px' align='right' class="noBottomBorder"></td>
              <td width='80px' align='right' class="noBottomBorder"></td>
            </tr>
            <?php $tempPendidikan++; $tempChar++; ?>
          @endif
          @if ($nilaiPakLamaUtama[$i]['keterangan'] == 'Formal')
            <tr>
              <td width='15px' align='center' class="noLeftRightBorder noTopBorder">1.</td>
              <td colspan="4" class="noTopBorder noLeftBorder">Pendidikan sekolah dan memperoleh Gelar/Ijazah</td>
              <td width='80px' align='right' class="noTopBorder"><?php if($nilaiPakLamaUtama[$i]['nilai_lama'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_lama'],3,',','.'); }else{ echo "-";} ?></td>
              <td width='80px' align='right' class="noTopBorder"><?php if($nilaiPakLamaUtama[$i]['nilai_baru'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_baru'],3,',','.'); }else{ echo "-";} ?></td>
              <td width='80px' align='right' class="noTopBorder"><?php if($nilaiPakLamaUtama[$i]['nilai_jumlah'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_jumlah'],3,',','.'); }else{ echo "-";} ?></td>
            </tr>
            <?php
              $jumlahLama = $jumlahLama + $nilaiPakLamaUtama[$i]['nilai_lama'];
              $jumlahBaru = $jumlahBaru + $nilaiPakLamaUtama[$i]['nilai_baru'];
              $jumlahTotal = $jumlahTotal + $nilaiPakLamaUtama[$i]['nilai_jumlah'];
            ?>
          @else
            <tr>
              <td width='15px' align='center' valign='top' class="noLeftRightBorder">2.</td>
              <td colspan="4" class="noLeftBorder">Pendidikan dan pelatihan fungsional dibidangnya dan mendapatkan Surat Tanda Tamat Pendidikan dan Pelatihan (STTPL)</td>
              <td width='80px' align='right' class="noTopBorder"><?php if($nilaiPakLamaUtama[$i]['nilai_lama'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_lama'],3,',','.'); }else{ echo "-";} ?></td>
              <td width='80px' align='right' class="noTopBorder"><?php if($nilaiPakLamaUtama[$i]['nilai_baru'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_baru'],3,',','.'); }else{ echo "-";} ?></td>
              <td width='80px' align='right' class="noTopBorder"><?php if($nilaiPakLamaUtama[$i]['nilai_jumlah'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_jumlah'],3,',','.'); }else{ echo "-";} ?></td>
            </tr>
            <?php
              $jumlahLama = $jumlahLama + $nilaiPakLamaUtama[$i]['nilai_lama'];
              $jumlahBaru = $jumlahBaru + $nilaiPakLamaUtama[$i]['nilai_baru'];
              $jumlahTotal = $jumlahTotal + $nilaiPakLamaUtama[$i]['nilai_jumlah'];
            ?>
          @endif


        @else
          <tr>
            <td width='15px' align='center' class="noRightBorder">{{ $char[$tempChar] }}.</td>
            <td colspan="5" class="noLeftBorder">{{ $nilaiPakLamaUtama[$i]['nama'] }}</td>
            <td width='80px' align='right'><?php if($nilaiPakLamaUtama[$i]['nilai_lama'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_lama'],3,',','.'); }else{ echo "-";} ?></td>
            <td width='80px' align='right'><?php if($nilaiPakLamaUtama[$i]['nilai_baru'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_baru'],3,',','.'); }else{ echo "-";} ?></td>
            <td width='80px' align='right'><?php if($nilaiPakLamaUtama[$i]['nilai_jumlah'] != 0){ echo number_format($nilaiPakLamaUtama[$i]['nilai_jumlah'],3,',','.'); }else{ echo "-";} ?></td>
          </tr>
          <?php
            $tempChar++;
            $jumlahLama = $jumlahLama + $nilaiPakLamaUtama[$i]['nilai_lama'];
            $jumlahBaru = $jumlahBaru + $nilaiPakLamaUtama[$i]['nilai_baru'];
            $jumlahTotal = $jumlahTotal + $nilaiPakLamaUtama[$i]['nilai_jumlah'];
          ?>
        @endif
      @endfor
      <tr>
        <td colspan="7" class="bold" align='center'>JUMLAH UNSUR UTAMA</td>
        <td width='80px' align='right'><?php if($jumlahLama != 0){ echo number_format($jumlahLama,3,',','.'); }else{ echo "-";} ?></td>
        <td width='80px' align='right'><?php if($jumlahBaru != 0){ echo number_format($jumlahBaru,3,',','.'); }else{ echo "-";} ?></td>
        <td width='80px' align='right'><?php if($jumlahTotal != 0){ echo number_format($jumlahTotal,3,',','.'); }else{ echo "-";} ?></td>
      </tr>
      <tr>
        <td rowspan="2" width='15px' align='center' valign='top'>2.</td>
        <td colspan="6">UNSUR PENUNJANG</td>
        <td width='80px' align='right'></td>
        <td width='80px' align='right'></td>
        <td width='80px' align='right'></td>
      </tr>
      <?php $jumlahLamaPenunjang = 0;$jumlahBaruPenunjang = 0;$jumlahTotalPenunjang = 0; ?>
      @for ($j=0; $j < count($nilaiPakLamaPenunjang); $j++)
        <tr>
          <td colspan="6">{{ $nilaiPakLamaPenunjang[$j]['nama'] }}</td>
          <td width='80px' align='right'><?php if($nilaiPakLamaPenunjang[$j]['nilai_lama'] != 0){ echo number_format($nilaiPakLamaPenunjang[$j]['nilai_lama'],3,',','.'); }else{ echo "-";} ?></td>
          <td width='80px' align='right'><?php if($nilaiPakLamaPenunjang[$j]['nilai_baru'] != 0){ echo number_format($nilaiPakLamaPenunjang[$j]['nilai_baru'],3,',','.'); }else{ echo "-";} ?></td>
          <td width='80px' align='right'><?php if($nilaiPakLamaPenunjang[$j]['nilai_jumlah'] != 0){ echo number_format($nilaiPakLamaPenunjang[$j]['nilai_jumlah'],3,',','.'); }else{ echo "-";} ?></td>
        </tr>
        <?php
          $jumlahLama = $jumlahLama + $nilaiPakLamaPenunjang[$j]['nilai_lama'];
          $jumlahLamaPenunjang = $jumlahLamaPenunjang + $nilaiPakLamaPenunjang[$j]['nilai_lama'];
          $jumlahBaru = $jumlahBaru + $nilaiPakLamaPenunjang[$j]['nilai_baru'];
          $jumlahBaruPenunjang = $jumlahBaruPenunjang + $nilaiPakLamaPenunjang[$j]['nilai_baru'];
          $jumlahTotal = $jumlahTotal + $nilaiPakLamaPenunjang[$j]['nilai_jumlah'];
          $jumlahTotalPenunjang = $jumlahTotalPenunjang + $nilaiPakLamaPenunjang[$j]['nilai_jumlah'];
        ?>
      @endfor
      <tr>
        <td colspan="7" class="bold" align='center'>JUMLAH UNSUR PENUNJANG</td>
        <td width='80px' align='right'><?php if($jumlahLamaPenunjang != 0){ echo number_format($jumlahLamaPenunjang,3,',','.'); }else{ echo "-";} ?></td>
        <td width='80px' align='right'><?php if($jumlahBaruPenunjang != 0){ echo number_format($jumlahBaruPenunjang,3,',','.'); }else{ echo "-";} ?></td>
        <td width='80px' align='right'><?php if($jumlahTotalPenunjang != 0){ echo number_format($jumlahTotalPenunjang,3,',','.'); }else{ echo "-";} ?></td>
      </tr>
      <tr>
        <td colspan="8" class="bold" align='center'>JUMLAH UNSUR UTAMA DAN UNSUR PENUNJANG</td>
        <td width='80px' align='right'><?php if($jumlahLama != 0){ echo number_format($jumlahLama,3,',','.'); }else{ echo "-";} ?></td>
        <td width='80px' align='right'><?php if($jumlahBaru != 0){ echo number_format($jumlahBaru,3,',','.'); }else{ echo "-";} ?></td>
        <td width='80px' align='right'><?php if($jumlahTotal != 0){ echo number_format($jumlahTotal,3,',','.'); }else{ echo "-";} ?></td>
      </tr>
      <tr>
        <td width='15px' align='center' valign='top'>III.</td>
        <td colspan="10" valign='top'>Dapat dipertimbangkan untuk dinaikkan dalam jabatan : {{ $pengajuan->jabatanPemohonBaru->nama }}, Pangkat/golongan ruang : {{ $pengajuan->golonganPemohonBaru->nama }}, TMT : {{ date('d-m-Y', strtotime($pengajuan->golongan_baru_tmt)) }}</td>
      </tr>
    </table>
    <br>
    <div style='float:left; width:300px;'>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <div><span style="font-weight:bold;text-decoration: underline;">ASLI</span> disampaikan dengan hormat kepada:</div>
      <div>Kepala Kantor Regional II BKN Surabaya</div>
      <br>
      <div><span style="font-weight:bold;text-decoration: underline;">TEMBUSAN</span> disampaikan dengan hormat kepada:</div>
      <div>1. Kepala BKD Kabupaten Sidoarjo;</div>
      <div>2. Sekretaris Tim Penilai yang bersangkutan;</div>
      <div>3. {{ $pegawai->typeProfesi->nama }} yang bersangkutan.</div>
    </div>
    <div style='float:right;width:300px;'>
      <div style="margin-left:20px;">Ditetapkan di : SIDOARJO</div>
      <div style="margin-left:20px;">Pada tanggal : {{ date('d-m-Y', strtotime($pengajuan->tgl_skpak)) }}</div>
      <br>
      <div class="">a.n BUPATI SIDOARJO</div>
      <div style="margin-left:20px;">KEPALA DINAS KESEHATAN</div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <div style="margin-left:20px;text-decoration: underline;font-weight:bold">{{ $umum->nama_kepala }}</div>
      <div style="margin-left:20px;">{{ $umum->jabatan_kepala }}</div>
      <div style="margin-left:20px;">NIP. {{ $umum->nip_kepala }}</div>
    </div>
    <br>
    <div class="clearfix"></div>

  </body>
</html>
