<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TELAAH TIM</title>
    <style media="screen">
      @page { margin: 15px;font-family: Arial, Helvetica, sans-serif; }
      body{margin:15px;font-family: Arial, Helvetica, sans-serif;}
      .romawi, .nomer {
        width: 25px;
        float: left;
      }
      .nomer{
        padding-left: 25px;
      }
      .judul {
        width: 300px;
        float: left;
      }
      .titik2 {
        text-align: center;
        width: 10px;
        float: left;
      }
      .row, .clearfix{
        overflow: auto;
      }
      .row::after, .clearfix::after {
        content: "";
        clear: both;
        display: table;
      }
      table {
        width: 100%;
      }
      .tablePenilai {
        margin-left: 25px;
      }

      table, table tbody td,table tbody tr, table thead tr,table thead th{
        border :1px solid black;
        border-spacing: 0px;
        font-size: 14px;
      }
      table tbody td{
        padding : 5px;
      }
      .ttd {
        margin-left: 450px;
      }
    </style>
  </head>
  <body>
    <center><b>TELAAH TIM</b></center><br>
    <div class="row">
      <div class="romawi">I.</div>
      <div class="isi">KETERANGAN PERORANGAN</div>
    </div>
    <div class="row">
      <div class="nomer">1.</div>
      <div class="judul">Nama</div>
      <div class="titik2">:</div>
      <div class="isi">{{ $pegawai->nama }}</div>
    </div>
    <div class="row">
      <div class="nomer">2.</div>
      <div class="judul">NIP / No. Seri Karpeg</div>
      <div class="titik2">:</div>
      <div class="isi">{{ $pegawai->no_nip }} / {{ $pegawai->no_karpeng }}</div>
    </div>
    <div class="row">
      <div class="nomer">3.</div>
      <div class="judul">Tempat, Tanggal Lahir</div>
      <div class="titik2">:</div>
      <div class="isi">{{ $pegawai->tempat_lahir }} / {{ App\Http\Libraries\Formatters::get_tanggal_lengkap($pegawai->tanggal_lahir) }}</div>
    </div>
    <div class="row">
      <div class="nomer">4.</div>
      <div class="judul">Jenis Kelamin</div>
      <div class="titik2">:</div>
      <div class="isi"><?php if($pegawai->jenis_kelamin == 'P'){ echo "Laki-Laki"; }else{ echo "Perempuan"; } ?></div>
    </div>
    <div class="row">
      <div class="nomer">5.</div>
      <div class="judul">Pendidikan</div>
      <div class="titik2">:</div>
      <div class="isi">{{ $pegawai->pendidikan_terakhir }}</div>
    </div>
    <div class="row">
      <div class="nomer">6.</div>
      <div class="judul">Pendidikan Diusulkan</div>
      <div class="titik2">:</div>
      <div class="isi">-</div>
    </div>
    <div class="row">
      <div class="nomer">7.</div>
      <div class="judul">Pangkat/golongan ruang, TMT</div>
      <div class="titik2">:</div>
      <div class="isi">{{ $pengajuan->golonganPemohonLama->nama }}, {{ date('d M Y', strtotime($pegawai->golongan_tmt)) }}</div>
    </div>
    <div class="row">
      <div class="nomer">8.</div>
      <div class="judul">Jabatan, TMT</div>
      <div class="titik2">:</div>
      <div class="isi">{{ $pengajuan->jabatanPemohonLama->nama }}, {{ date('d M Y', strtotime($pegawai->jabatan_tmt)) }}</div>
    </div>
    <div class="row">
      <div class="nomer">9.</div>
      <div class="judul">Unit Kerja</div>
      <div class="titik2">:</div>
      <div class="isi">{{ strtolower($pegawai->satuanKerja->nama) }}</div>
    </div>
    <div class="row">
      <div class="romawi">II.</div>
      <div class="isi">DAFTAR USUL PENETAPAN ANGKA KREDIT</div>
    </div>
    <div class="row">
      <div class="nomer">1.</div>
      <div class="judul">Jangka Waktu Penilaian</div>
      <div class="titik2">:</div>
      <div class="isi">{{ date('d M Y', strtotime($pengajuan->tgl_awal_penilaian)) }} s.d {{ date('d M Y', strtotime($pengajuan->tgl_akhir_penilaian)) }}</div>
    </div>
    <div class="row">
      <div class="nomer">2.</div>
      <div class="judul">Angka Kredit</div>
      <div class="titik2">:</div>
      <div class="isi"></div>
    </div>
    <table style="margin-left:50px">
      <thead>
        <tr>
          <th width='380px' style="text-align:left">Unsur</th>
          <th width='80px'>Lama</th>
          <th width='80px'>Baru</th>
          <th width='80px'>Jumlah</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="4"><b>1. Unsur Utama</b></td>
        </tr>
        <?php $char = range('a', 'z'); $tempChar = 0;$tempPendidikan = 0;$jumlahLama = 0; ?>
        @foreach ($nilaiPakLamaUtama as $utama)
          @if ($utama->nama == 'Pendidikan')
            @if ($tempPendidikan == 0)
              <tr>
                <td colspan="4"><span style='margin-left:18px;'>{{ $char[$tempChar] }}. </span>Pendidikan</td>
              </tr>
              <?php $tempPendidikan++; $tempChar++; ?>
            @endif
            @if ($utama->keterangan == 'Formal')
              <tr>
                <td><span style='margin-left:35px;'>* </span>Formal</td>
                <td align='right'><?php if($utama->jumlah_nilai != 0){ echo ($utama->jumlah_nilai); }else{ echo "-";} ?></td>
                <td></td>
                <td></td>
              </tr>
              <?php $jumlahLama = $jumlahLama + $utama->jumlah_nilai; ?>
            @elseif ($utama->keterangan == 'Pelatihan')
              <tr>
                <td><span style='margin-left:35px;'>* </span>Pelatihan</td>
                <td align='right'><?php if($utama->jumlah_nilai != 0){ echo ($utama->jumlah_nilai); }else{ echo "-";} ?></td>
                <td></td>
                <td></td>
              </tr>
              <?php $jumlahLama = $jumlahLama + $utama->jumlah_nilai; ?>
            @endif
          @else
            <tr>
              <td><span style='margin-left:18px;'>{{ $char[$tempChar] }}. </span>{{ $utama->nama }}</td>
              <td align='right'><?php if($utama->jumlah_nilai != 0){ echo ($utama->jumlah_nilai); }else{ echo "-";} ?></td>
              <td></td>
              <td></td>
            </tr>
            <?php $tempChar++; ?>
            <?php $jumlahLama = $jumlahLama + $utama->jumlah_nilai; ?>
          @endif
        @endforeach
        <tr>
          <td colspan="4"><b>2. Unsur Penunjang</b></td>
        </tr>
        @foreach ($nilaiPakLamaPenunjang as $penunjang)
          <tr>
            <td><span style='margin-left:18px;'>* </span>{{ $penunjang->nama }}</td>
            <td align='right'><?php if($penunjang->jumlah_nilai != 0){ echo ($penunjang->jumlah_nilai); }else{ echo "-";} ?></td>
            <td></td>
            <td></td>
          </tr>
          <?php $tempChar++; ?>
          <?php $jumlahLama = $jumlahLama + $penunjang->jumlah_nilai; ?>
        @endforeach
        <tr>
          <td><b>JUMLAH UNSUR UTAMA DAN PENUNJANG</b></td>
          <td align='right'><b>{{ ($jumlahLama) }}</b></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <div class="row">
      <div class="nomer">3.</div>
      <div class="judul">Angka Kredit Minimal</div>
      <div class="titik2">:</div>
      <div class="isi">{{ ($pengajuan->min_point_ak)}}</div>
    </div>
    <div class="row">
      <div class="nomer">4.</div>
      <div class="judul">Prosentase Unsur</div>
      <div class="titik2">:</div>
      <div class="isi"></div>
    </div>
    <div class="row">
      <div class="nomer"></div>
      <div class="judul">- Unsur Utama</div>
      <div class="titik2">:</div>
      <div class="isi">.......... %</div>
    </div>
    <div class="row">
      <div class="nomer"></div>
      <div class="judul">- Unsur Penunjang</div>
      <div class="titik2">:</div>
      <div class="isi">.......... %</div>
    </div>
    <div class="row">
      <div class="romawi">III.</div>
      {{-- <div class="isi">Dapat dipertimbangkan untuk dinaikkan dalam jabatan: <span style="text-decoration: line-through">Teknisi Elektromedis Pelaksana</span>, Pangkat/golongan ruang : <span style="text-decoration: line-through">Pengatur (IIc)</span></div> --}}
      <div class="isi">Dapat dipertimbangkan untuk dinaikkan dalam jabatan: {{ $pengajuan->jabatanPemohonBaru->nama }}, Pangkat/golongan ruang : {{ $pengajuan->golonganPemohonBaru->nama }}</div>
    </div>
    <table class="tablePenilai">
      <thead>
        <tr>
          <th width='25px'>No</th>
          <th width='200px'>NAMA</th>
          <th width='200px'>NIP</th>
          <th width='150px'>GOL</th>
          <th width='50px'>KET</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td align='center'>1.</td>
          <td>{{ $pengajuan->penilai->nama }}</td>
          <td>{{ $pengajuan->penilai->no_nip }}</td>
          <td>{{ $pengajuan->golonganPenilai->nama }}</td>
          <td></td>
        </tr>
        <tr>
          <td align='center'>2.</td>
          <td>{{ $pengajuan->penilai2->nama }}</td>
          <td>{{ $pengajuan->penilai2->no_nip }}</td>
          <td>{{ $pengajuan->golonganPenilai2->nama }}</td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <br>
    <br>
    <div class="row">
      <div class="ttd">Sidoarjo,</div>
      <div class="ttd">TIM PENILAI ANGKA KREDIT</div>
      <div class="ttd">JABFUNG TENAGA KESEHATAN</div>
      <div class="ttd">Koordinator</div>
      <br>
      <br>
      <br>
      <br>
      <div class="ttd"><b>({{ $umum->nama_koor }})</b></div>
      <div class="ttd">{{ $umum->jabatan_koor }}</div>
      <div class="ttd">NIP. {{ $umum->nip_koor }}</div>
    </div>
  </body>
</html>
