<div class="form-group row">
	<label for="inputJenis" class="col-4 col-lg-3 col-form-label">No NIP</label>
	<div class="col-8 col-lg-9">
		<input type="text" class="form-control" value="{{$det_penilai->no_nip}}">
	</div>
</div>
<div class="form-group row">
	<label for="inputJenis" class="col-4 col-lg-3 col-form-label">Nama Penilai</label>
	<div class="col-8 col-lg-9">
		<input type="text" class="form-control" value="{{$det_penilai->nama}}">
	</div>
</div>
<div class="form-group row">
	<label for="inputJenis" class="col-4 col-lg-3 col-form-label">Golongan</label>
	<div class="col-8 col-lg-9">
		<input type="text" class="form-control" value="{{$det_penilai->golongan->nama}}">
	</div>
</div>
<div class="form-group row">
	<label for="inputJenis" class="col-4 col-lg-3 col-form-label">Jabatan</label>
	<div class="col-8 col-lg-9">
		<input type="text" class="form-control" value="{{$det_penilai->jabatan->nama}}">
	</div>
</div>
<div class="form-group row">
	<label for="inputJenis" class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
	<div class="col-8 col-lg-9">
		<input type="text" class="form-control" value="{{$det_penilai->satuanKerja->nama}}">
	</div>
</div>
<div class="form-group row">
	<label for="inputJenis" class="col-4 col-lg-3 col-form-label">Profesi</label>
	<div class="col-8 col-lg-9">
		<input type="text" class="form-control" value="{{$det_penilai->typeProfesi->nama}}">
	</div>
</div>