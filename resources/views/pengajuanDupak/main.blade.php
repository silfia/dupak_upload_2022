@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12 col-lg-8 mb-2 mb-sm-2">
                <button type="button" class="btn btn-xs btn-info btn-list"><span class="fas fa-search"></span> Daftar Pemohon</button>
              </div>
              <div class="col-sm-12 col-lg-4 mb-2 mb-sm-2">
                <span style="float:right">
                  <input type="text" placeholder="Cari data" name="search-data" id="search-data" value="" class="textbox" autocomplete="off" aria-controls="idtables" onkeydown="if (event.keyCode == 13) {return searchData();}">
                </span>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-striped table-bordered first" id='loadData'></table>
                <div class="form-row mt-3">
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                    <select class="form-control" id="lngDt" style="width:60px;"></select>
                  </div>
                  <label class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2 text-center info-data"></label>
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                    <nav aria-label="Page navigation example" class="pl-right">
                      <ul class="pagination" id="paging"></ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
      <div class="col-12 second-page"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script src="{{ url('/')}}/js/cusLoadData.js"></script>
  <script type="text/javascript">
    var dtLoad = {
        url                 : "{!! route('loadMasaPengajuan') !!}",
        primaryField        : 'id_masa_pengajuan',
        customFilter        : false,
        column              : [
          {field: 'tglAwalPengajuan', title: 'Tgl Awal Pengajuan', width: 350, align: 'left',
              rowStyler: function(rowData, rowIndex) {
                return tglAwalPengajuan(rowData, rowIndex);
              }
          },
          {field: 'tglAkhirPengajuan', title: 'Tgl Akhir Pengajuan', width: 350, align: 'left',
              rowStyler: function(rowData, rowIndex) {
                return tglAkhirPengajuan(rowData, rowIndex);
              }
          },
          {field: 'Status', title: 'Status', width: 100, align: 'center',
              rowStyler: function(rowData, rowIndex) {
                return Status(rowData, rowIndex);
              }
          },
        ],
    }
    $(document).ready(function() {
      loadData(dtLoad, 1);
    });
    function searchData() { reloadData(1); }

    function tglAwalPengajuan() {
      var tanggal = rowData.tgl_awal;
      return changeDate2Indo(tanggal);
    }

    function tglAkhirPengajuan() {
      var tanggal = rowData.tgl_akhir;
      return changeDate2Indo(tanggal);
    }

    function Status(rowData, rowIndex) {
      var status = rowData.status_masa;
      if (status == 'Selesai') {
        var tmp = "<div style='color:#fff;background-color:#449d44;border-color:#449d44;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;' > Selesai</div>";
      }else if (status == 'Proses') {
        var tmp = '<div style="color:#fff;background-color:#1422a3 ;border-color:#1422a3;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Proses</div>';
      }else if (status == 'Tutup') {
        var tmp = '<div style="color:#fff;background-color:#2e2f39 ;border-color:#2e2f39;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Tutup</div>';
			}else{
        var tmp = '<div style="color:#fff;background-color:#f39c12 ;border-color:#f39c12;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Belum</div>';
			}
			return tmp;
    }

    $('.btn-list').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('listPengajuanDupak') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });
  </script>
@stop
