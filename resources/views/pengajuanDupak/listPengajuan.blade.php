<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-list">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-list">
      <div class="card">
        <h5 class="card-header">Daftar Pemohon DUPAK</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-lg-12 mb-2 mb-sm-2 row">
              <button type="button" class="btn btn-xs btn-primary btn-getTelaah m-l-10" style="height:32px"><span class="fas fa-print"></span> Cetak Telaah</button>
              <button type="button" class="btn btn-xs btn-primary btn-setSKPAK m-l-10" style="height:32px"><span class="fas fa-pencil-alt"></span> Pembuatan No SKPAK</button>
              <button type="button" class="btn btn-xs btn-primary btn-getSKPAK m-l-10" style="height:32px"><span class="fas fa-print"></span> Cetak SK PAK</button>
              <button type="button" class="btn btn-xs btn-warning btn-updatePenilai m-l-10" style="height:32px"><span class="fas fa-user"></span> Pilih Tim Penilai</button>
              <button type="button" class="btn btn-xs btn-secondary btn-cancel m-l-10" style="height:32px"><span class="fas fa-chevron-left"></span> Kembali</button>
              <div class="col-lg-4 form-group row m-l-0 p-0">
                <label for="typesProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
                <div class="col-8 col-lg-9">
                  <select name="types_profesi" id="typesProfesi">
                    @foreach ($profesis as $profesi)
                      <option value="{{ $profesi->id_master_type }}">{{ $profesi->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table class="table table-striped table-bordered first">
                <thead>
                  <tr>
                    <td width="10px">&nbsp</td>
                    <td width="10px" align='center'>No</td>
                    <td width="160px">NIP</td>
                    <td width="200px">Nama</td>
                    <td width="150px">Tgl Pengajuan</td>
                    <td width="150px">Profesi</td>
                    <td width="200px">Satuan Kerja</td>
                    <td width="200px">Penilai 1</td>
                    <td width="200px">Penilai 2</td>
                    <td>Status</td>
                  </tr>
                </thead>
                <tbody class='resultDataListPengajuan'></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-list').animateCss('bounceInUp');
    getDataListPengajuan();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-list').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  $('#typesProfesi').chosen();
  $('#typesProfesi').change(function(){ getDataListPengajuan(); });

  function getDataListPengajuan() {
    $('.resultDataListPengajuan').html('<tr><td colspan="10" align="center">Memuat data...</td></tr>');
    var id_masa = {{ $masa->id_masa_pengajuan }};
    var id_profesi = $('#typesProfesi').val();
    $.post("{!! route('loadListPengajuan') !!}", {id_masa:id_masa,id_profesi:id_profesi }).done(function(data){
      $('.resultDataListPengajuan').empty();
      var tag = '';
      if (data.status == 'success') {
        if (data.row.pemohon.length != 0) {
          var n1 = 1;
          $.each(data.row.pemohon, function(k,v){
            tag += '<tr>';
            tag += '<td align="center" class="pl-0 pr-1">';
            tag += '<label class="custom-control custom-checkbox m-0">';
            tag += '<input type="checkbox" name="id_dataPm[]" id="checkData" value="'+v.id_pengajuan+'" class="custom-control-input"><span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
            tag += '</label>';
            tag += '</td>';
            tag += '<td align="center">'+n1+'</td>';
            tag += '<td>'+v.no_nip+'</td>';
            tag += '<td>'+v.nama+'</td>';
            tag += '<td>'+changeDate2Indo(v.tgl_pengajuan)+'</td>';
            tag += '<td>'+v.nama_profesi+'</td>';
            tag += '<td>'+kapital(v.nama_satuan)+'</td>';
            if (v.nama_penilai == null) {
              tag += '<td align="center">-</td>';
            }else{
              tag += '<td>'+v.nama_penilai+'</td>';
            }
            if (v.nama_penilai2 == null) {
              tag += '<td align="center">-</td>';
            }else{
              tag += '<td>'+v.nama_penilai2+'</td>';
            }
            tag += '<td>';
            if (v.status_pengajuan == 'Pending') {
              tag += '<div style="color:#fff;background-color:#f39c12 ;border-color:#d43f3a;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Pending</div>';
            }else if(v.status_pengajuan == 'Proses'){
              tag += "<div style='color:#fff;background-color:#1422a3;border-color:#d43f3a;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;' > Proses</div>";
            }else{
              tag += "<div style='color:#fff;background-color:#449d44;border-color:#d43f3a;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;' > Selesai</div>";
            }
            tag += '</td>';
            tag += '</tr>';
            n1++;
          });
        }else{
          tag += '<tr><td colspan="10" align="center">Tidak ada data ditemukan...</td></tr>';
        }
        $('.resultDataListPengajuan').html(tag);
      }else{
        swal("MAAF !","Terjadi Kesalahan !!", "warning");
      }
    }).fail(function() {
      getDataListPengajuan();
    });
  }

  function kapital(str){
    return str.replace (/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  $('.btn-getTelaah').click(function(){
    $('.loading').show();
    $('.panel-list').hide();
    var idData = $(".resultDataListPengajuan input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length == 1) {
      $.post("{!! route('cekSkpakSebelum') !!}",{id:idData[0]}).done(function(data){
        if(data.status == 'already'){
          $('.loading').hide();
          // $('.panel-list').show();
          $.post("{!! route('cekSkpakSebelumEdit') !!}",{id:idData[0],idLama:data.data.id_pengajuan}).done(function(data){
            $('.second-page').html(data.content).fadeIn();          
          });
          // window.open("{{ route('getTelaahPengajuan') }}?idBaru="+idData[0]+"&idLama="+data.data.id_pengajuan, '_blank');
        } else if (data.status == 'not found') {
          $('.loading').hide();
          $('.second-page').html(data.content).fadeIn();
        } else {
          swal("MAAF !",data.message, "warning");
          $('.loading').hide();
          $('.panel-list').show();
        }
      });
    }else if (idData.length > 1) {
      swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
      $('.loading').hide();
      $('.panel-list').show();
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.panel-list').show();
    }
  });

  $('.btn-setSKPAK').click(function(){
    $('.loading').show();
    $('.panel-list').hide();
    var idData = $(".resultDataListPengajuan input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length > 0) {
      $.post("{!! route('formNoSKPAK') !!}",{id:idData}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.second-page').html(data.content).fadeIn();
        } else {
          swal("MAAF !",data.message, "warning");
          $('.loading').hide();
          $('.panel-list').show();
        }
      });
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.panel-list').show();
    }
  });

  $('.btn-getSKPAK').click(function(){
    var idData = $(".resultDataListPengajuan input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length == 1) {
      $.post("{!! route('cekSKPAKPengajuan') !!}", {id:idData[0]}).done(function(data){
        if(data.status == 'success'){
          window.open("{{ route('getSKPAKPengajuan') }}?id="+data.data.id_pengajuan, '_blank');
        } else {
          swal("MAAF !",data.message, "warning");
        }
      });
    }else if (idData.length > 1) {
      swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
    }
  });

  $('.btn-updatePenilai').click(function(){
    $('.loading').show();
    $('.panel-list').hide();
    var idData = $(".resultDataListPengajuan input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length == 1) {
      $.post("{!! route('formPenilaiPengajuan') !!}",{id:idData[0]}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.second-page').html(data.content).fadeIn();
        } else {
          swal("MAAF !","Pengajuan sudah selesai !!", "warning");
          $('.loading').hide();
          $('.panel-list').show();
        }
      });
    }else if (idData.length > 1) {
      swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
      $('.loading').hide();
      $('.panel-list').show();
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.panel-list').show();
    }
  });
</script>
