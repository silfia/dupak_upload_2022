<div class="card col-lg-10 col-md-10 col-sm-12 col-xs-12 offset-lg-1 offset-md-1 p-0 panel-form">
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data SKPAK Sebelumnya</h5>
  <div class="card-body">
    <form class="form-save">
      <?php $no = 1; ?>
      @foreach ($pengajuans as $pengajuan)
        <h5><span class="fas fa-chevron-right mr-3"></span> Pemohon ke-{{ $no++ }}</h5>
        <div class="form-group row">
          <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Nama</label>
          <div class="col-8 col-lg-9">
            <input id="inputProfesi" type="hidden" name="id_pengajuan[]" value="{{ $pengajuan->id_pengajuan }}" class="form-control backWhite" readonly>
            <input id="inputProfesi" type="text" value="{{ $pengajuan->nama }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">NIP / No. Seri Karpeg</label>
          <div class="col-8 col-lg-9">
            <input id="inputProfesi" type="text" value="{{ $pengajuan->no_nip }} / {{ $pengajuan->no_karpeng }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">No SKPAK</label>
          <div class="col-8 col-lg-9">
            <label class="col-form-label p-r-10 pl-left">{{ $umum->kode_skpak }} / </label><input id="inputProfesi" type="number" name="no_skpak[]" class="form-control pl-left" placeholder="NO SKPAK" style="width:100px"><label class="col-form-label p-l-10 pl-left"> / {{ $umum->kode_dinas }} / {{ date('Y') }}</label>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Tanggal SKPAK</label>
          <div class="col-8 col-lg-9">
            <div class="input-group input-group-sm col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0" style="height:32px">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
              <input id="tgl_skpak{{ $pengajuan->id_pengajuan }}" type="text" name="tgl_skpak[]" placeholder="Tanggal SKPAK" class="form-control" data-date-format="dd-mm-yyyy">
            </div>
          </div>
        </div>
        <input id="inputProfesi" type="hidden" name="statusTmtJabatan[]" value="{{ $pengajuan->statusTmtJabatan }}" class="form-control backWhite" readonly>
        @if ($pengajuan->statusTmtJabatan == 'Request')
          <div class="form-group row">
            <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
            <div class="col-8 col-lg-9">
              <div class="input-group input-group-sm col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0" style="height:32px">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                <input id="inputjabatantm{{ $pengajuan->id_pengajuan }}" type="text" name="jabatan_baru_tmt[]" placeholder="Jabatan TMT" class="form-control" data-date-format="dd-mm-yyyy">
              </div>
            </div>
          </div>
        @else
          <input id="inputProfesi" type="hidden" name="jabatan_baru_tmt[]" value="0000-00-00" class="form-control backWhite" readonly>
        @endif
      @endforeach
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelFNS"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelFNS').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.second-page').fadeOut(function(){
      $('.second-page').empty();
      $('.panel-list').fadeIn();
    });
  });

  @foreach ($pengajuans as $pengajuan)
    $('#tgl_skpak{{ $pengajuan->id_pengajuan }}').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 2,
        forceParse: 0,
    });
    $('#inputjabatantm{{ $pengajuan->id_pengajuan }}').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 2,
        forceParse: 0,
    });
  @endforeach

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);

    $.ajax({
      url: "{{ route('saveNoSKPAK') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      if(data.status=='success'){
        swal("Success !", data.message, "success");
        $('.panel-form').animateCss('bounceOutDown');
        $('.second-page').fadeOut(function(){
          $('.second-page').empty();
          $('.panel-list').fadeIn();
        });
        getDataListPengajuan();
      }else{
        swal("MAAF !", data.message, "warning");
        $('.btn-submit').html('<span class="fas fa-save"></span> Simpan').removeAttr('disabled');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('<span class="fas fa-save"></span> Simpan').removeAttr('disabled');
    });
  });
</script>
