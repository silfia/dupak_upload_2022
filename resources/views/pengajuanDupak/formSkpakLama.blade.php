<div class="card col-lg-10 col-md-10 col-sm-12 col-xs-12 offset-lg-1 offset-md-1 p-0 panel-form">
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data SKPAK Sebelumnya</h5>
  <div class="card-body">
    <form class="form-save">
      <input type="hidden" name="id_pengajuanNow" value="{{ $pengajuanNow->id_pengajuan }}">
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Nama</label>
        <div class="col-8 col-lg-9">
          <input type="text" value="{{ $pegawai->nama }}" class="form-control backWhite" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">NIP / No. Seri Karpeg</label>
        <div class="col-8 col-lg-9">
          <input type="text" value="{{ $pegawai->no_nip }} / {{ $pegawai->no_karpeng }}" class="form-control backWhite" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Tempat, Tanggal Lahir</label>
        <div class="col-8 col-lg-9">
          <input type="text" value="{{ $pegawai->tempat_lahir }}, {{ date('d-m-Y', strtotime($pegawai->tanggal_lahir)) }}" class="form-control backWhite" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
        <div class="col-8 col-lg-9">
          <input type="text" value="<?php if($pegawai->jenis_kelamin == 'P'){ echo "Laki-Laki"; }else{ echo "Perempuan"; } ?>" class="form-control backWhite" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Masa Kerja Lama</label>
        <div class="col-8 col-lg-9">
          <span style="float:left">
            <input type="number" name="tahun_masa_kerja_lama" class="form-control" placeholder="Tahun" style="width:100px;float:left">
            <label class="col-form-label p-r-10 p-l-10">Tahun</label>
          </span>
          <span style="float:left">
            <input type="number" name="bulan_masa_kerja_lama" class="form-control" placeholder="Bulan" style="width:100px;float:left">
            <label class="col-form-label p-r-10 p-l-10">Bulan</label>
          </span>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Masa Kerja Baru</label>
        <div class="col-8 col-lg-9">
          <span style="float:left">
            <input type="number" name="tahun_masa_kerja_baru" class="form-control" placeholder="Tahun" style="width:100px;float:left">
            <label class="col-form-label p-r-10 p-l-10">Tahun</label>
          </span>
          <span style="float:left">
            <input type="number" name="bulan_masa_kerja_baru" class="form-control" placeholder="Bulan" style="width:100px;float:left">
            <label class="col-form-label p-r-10 p-l-10">Bulan</label>
          </span>
        </div>
      </div>
      <table class="table table-striped">
        <thead>
          <tr>
            <th width="25px">NO</th>
            <th colspan="2">PENETAPAN ANGKA KREDIT</th>
            <th>LAMA</th>
            <th>BARU</th>
            <th>JUMLAH</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          @foreach ($unsurs as $unsur)
            @if ($unsur->nama == 'Pendidikan')
              <tr>
                <td align="center">{{ $no }}</td>
                <td colspan="2">{{ $unsur->nama }}</td>
                <td width='200px'></td>
                <td width='200px'></td>
                <td width='200px'></td>
              </tr>
              <tr>
                <td align="center"></td>
                <td width='25px' valign='top'>a.</td>
                <td>Pendidikan Sekolah dan Memperoleh Gelar/Ijazah</td>
                <td width='200px'><input type="number" name="nilai_lama[]" onkeyup="hitungTotal('1a')" id="lama_1a" class="form-control" placeholder="LAMA"></td>
                <td width='200px'><input type="number" name="nilai_baru[]" onkeyup="hitungTotal('1a')" id="baru_1a" class="form-control" placeholder="BARU"></td>
                <td width='200px'><input type="number" name="nilai_jumlah[]" class="form-control" id="jumlah_1a" placeholder="JUMLAH" readonly></td>
                <input type="hidden" name="unsur_id[]" value="{{ $unsur->id_master_type }}" class="form-control" placeholder="ID Unsur">
                <input type="hidden" name="keterangan[]" value="Formal" class="form-control" placeholder="Keterangan">
              </tr>
              <tr>
                <td align="center"></td>
                <td width='25px' valign='top'>b.</td>
                <td>Pendidikan dan pelatihan fungsional dibidangnya dan mendapatkan Surat Tanda Tamat Pendidikan dan Pelatihan (STTPL)</td>
                <td width='200px'><input type="number" name="nilai_lama[]" onkeyup="hitungTotal('1b')" id="lama_1b" class="form-control" placeholder="LAMA"></td>
                <td width='200px'><input type="number" name="nilai_baru[]" onkeyup="hitungTotal('1b')" id="baru_1b" class="form-control" placeholder="BARU"></td>
                <td width='200px'><input type="number" name="nilai_jumlah[]" class="form-control" id="jumlah_1b" placeholder="JUMLAH" readonly></td>
                <input type="hidden" name="unsur_id[]" value="{{ $unsur->id_master_type }}" class="form-control" placeholder="ID Unsur">
                <input type="hidden" name="keterangan[]" value="Pelatihan" class="form-control" placeholder="Keterangan">
              </tr>
            @else
              <tr>
                <td align="center">{{ $no }}</td>
                <td colspan="2">{{ $unsur->nama }}</td>
                <td width='200px'><input type="number" name="nilai_lama[]" onkeyup="hitungTotal({{ $no }})" id="lama_{{ $no }}" class="form-control" placeholder="LAMA"></td>
                <td width='200px'><input type="number" name="nilai_baru[]" onkeyup="hitungTotal({{ $no }})" id="baru_{{ $no }}" class="form-control" placeholder="BARU"></td>
                <td width='200px'><input type="number" name="nilai_jumlah[]" id="jumlah_{{ $no }}" class="form-control" placeholder="JUMLAH" readonly></td>
                <input type="hidden" name="unsur_id[]" value="{{ $unsur->id_master_type }}" class="form-control" placeholder="ID Unsur">
                <input type="hidden" name="keterangan[]" value="-" class="form-control" placeholder="Keterangan">
              </tr>
            @endif
            <?php $no++; ?>
          @endforeach
        </tbody>
      </table>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelFSL"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan dan Cetak</span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelFSL').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.second-page').fadeOut(function(){
      $('.second-page').empty();
      $('.panel-list').fadeIn();
    });
    getDataListPengajuan();
  });

  function hitungTotal(kode) {
    var lama = $('#lama_'+kode).val();
    var baru = $('#baru_'+kode).val();
    if (lama == '') { lama = 0; }
    if (baru == '') { baru = 0; }
    var jumlah = parseInt(lama) + parseInt(baru);
    $('#jumlah_'+kode).val(jumlah);
  }

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveSkpakSebelum') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        window.open("{{ route('getTelaahPengajuan') }}?idBaru="+data.data.pengajuan_baru.id_pengajuan+"&idLama="+data.data.pengajuan_lama.id_pengajuan, '_blank');
        $('.second-page').fadeOut(function(){
          $('.second-page').empty();
          $('.panel-list').fadeIn();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
