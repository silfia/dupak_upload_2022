<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  @if($pengajuan->penilai_id == "")
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Penilai</h5>
  @else
  <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Penilai</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      <input type="hidden" name="id_pengajuan" value="{{ $pengajuan->id_pengajuan }}" required>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">
            <h5 class="card-header">Data Pemohon</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group row">
                    <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">No NIP</label>
                    <div class="col-8 col-lg-9">
                      <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->no_nip }}" class="form-control backWhite" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Nama</label>
                    <div class="col-8 col-lg-9">
                      <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->nama }}" class="form-control backWhite" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
                    <div class="col-8 col-lg-9">
                      <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->satuanKerja->nama }}" class="form-control backWhite" readonly>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group row">
                    <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Golongan</label>
                    <div class="col-8 col-lg-9">
                      <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->golongan->nama }}" class="form-control backWhite" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Jabatan</label>
                    <div class="col-8 col-lg-9">
                      <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->jabatan->nama }}" class="form-control backWhite" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
                    <div class="col-8 col-lg-9">
                      <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->typeProfesi->nama }}" class="form-control backWhite" readonly>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="card">
            <h5 class="card-header">Data Tim Penilai 1</h5>
            <div class="card-body">
              <div class="form-group row">
                <label for="inputJenis" class="col-4 col-lg-3 col-form-label">Tim Penilai</label>
                <div class="col-8 col-lg-9">
                  <select id="inputJenis" class="form-control" name="penilai" required>
                    <option value="" selected> .:: Pilih Tim Penilai 1 ::. </option>
                    @foreach ($penilais as $penilai)
                    <option @if(isset($proses)) @if($penilai->id_pegawai==$penilai_pakai->id_pegawai) selected @endif @endif value="{{ $penilai->id_pegawai }}" @if($penilai->id_pegawai == $pengajuan->penilai_id) selected @endif>{{ $penilai->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div id="tempat_penilai"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="card">
            <h5 class="card-header">Data Tim Penilai 2</h5>
            <div class="card-body">
              <div class="form-group row">
                <label for="inputJenis2" class="col-4 col-lg-3 col-form-label">Tim Penilai</label>
                <div class="col-8 col-lg-9">
                  <select id="inputJenis2" class="form-control" name="penilai2" required>
                    <option value="" selected> .:: Pilih Tim Penilai 2 ::. </option>
                    @foreach ($penilais as $penilai2)
                    <option @if(isset($proses)) @if($penilai2->id_pegawai==$penilai_pakai2->id_pegawai) selected @endif @endif value="{{ $penilai2->id_pegawai }}" @if($penilai2->id_pegawai == $pengajuan->penilai2_id) selected @endif>{{ $penilai2->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div id="tempat_penilai2"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 text-right">
          <button type="button" class="btn btn-xs btn-secondary btn-cancelLB"><span class="fas fa-chevron-left"></span> Kembali</button>
          <button type="button" class="btn btn-xs btn-primary btn-submitLB"><span class="fas fa-save"></span> Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  getDetail();
  getDetail2();

  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelLB').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.second-page').fadeOut(function(){
      $('.second-page').empty();
      $('.panel-list').fadeIn();
    });
    getDataListPengajuan();
  });

  $('#inputJenis').chosen();

  $('#inputJenis').change(function(){ getDetail(); });
  $('#inputJenis2').change(function(){ getDetail2(); });

  function getDetail(){
    var id_penilai = $('#inputJenis').val();
    $.post("{{route('detailPenilaiPengajuan')}}",{id:id_penilai},function(data){
      if(data.status=='success'){
        $('#tempat_penilai').html(data.content);
      }else{
        $('#tempat_penilai').html('');
      }
    });
  }

  function getDetail2(){
    var id_penilai = $('#inputJenis2').val();
    $.post("{{route('detailPenilaiPengajuan')}}",{id:id_penilai},function(data){
      if(data.status=='success'){
        $('#tempat_penilai2').html(data.content);
      }else{
        $('#tempat_penilai2').html('');
      }
    });
  }

  $('.btn-submitLB').click(function(e){
    e.preventDefault();
    $('.btn-submitLB').html('Please wait...').attr('disabled', true);

    var penilai = $('select[name=penilai]').val();
    var penilai2 = $('select[name=penilai2]').val();
    if(penilai != '' && penilai2 != ''){
      var data  = new FormData($('.form-save')[0]);

      $.ajax({
        url: "{{ route('simpanPenilaiPengajuan') }}",
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false
      }).done(function(data){
        if(data.status=='success'){
          swal("Success !","Tim Penilai Berhasil Dipilih !!", "success");
          $('.panel-form').animateCss('bounceOutDown');
          $('.second-page').fadeOut(function(){
            $('.second-page').empty();
            $('.panel-list').fadeIn();
          });
          getDataListPengajuan();
        }else{
          swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
          $('.btn-submitLB').html('<span class="fas fa-save"></span> Simpan').removeAttr('disabled');
        }
      }).fail(function() {
        swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
        $('.btn-submitLB').html('<span class="fas fa-save"></span> Simpan').removeAttr('disabled');
      });
    }else{
      swal('Whooops','Pilih penilai','error');
      $('.btn-submitLB').html('<span class="fas fa-save"></span> Simpan').attr('disabled', false);
    }
  });
</script>
