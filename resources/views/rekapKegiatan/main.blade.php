@extends('component.layout')

@section('extended_css')
  <style media="screen">
    .panelIsiKegiatan {
      padding: 0px !important;
      position: relative !important;
      text-align: center;
      vertical-align: middle !important;
    }

    .btnIsiKegiatan {
      height: 100% !important;
      width: 100% !important;
      position: absolute !important;
      top: 0 !important;
      justify-content: center !important;
      display: flex !important;
      align-items: center !important;
    }
    .spinner-secondary {
      border: 3px solid transparent !important;
      border-top: 3px solid #5969ff !important;
      border-left: 3px solid #5969ff !important;
      z-index: 1;
      position: absolute !important;
      top: 10px;
    }

    .txt-middle {
      vertical-align: middle !important;
    }

    .table-scroll {
      position: relative;
      width:100%;
      z-index: 1;
      margin: auto;
      overflow: auto;
      height: 450px;
    }
    .table-scroll table {
      width: 100%;
      min-width: 1280px;
      margin: auto;
      border-collapse: separate;
      border-spacing: 0;
      table-layout: fixed;
    }
    .table-wrap {
      position: relative;
    }
    .table-scroll th,
    .table-scroll td {
      padding: 5px 10px;
      border: 1px solid #e6e6f2;
      vertical-align: top;
      background: #fff;
    }
    .table-scroll table tbody tr:nth-child(even) td, .table-scroll table tbody tr:nth-child(even) th {
      background-color: #f2f2f8 !important;
    }
    .table-scroll thead th, .table-scroll thead th:nth-child(2) {
      background: #9cb8e2;
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    }
    .table-scroll thead tr:nth-child(2) th {
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 33px;
      left: inherit;
    }
    /* safari and ios need the tfoot itself to be position:sticky also */
    .table-scroll tfoot,
    .table-scroll tfoot th,
    .table-scroll tfoot td {
      position: -webkit-sticky;
      position: sticky;
      bottom: 0;
      background: #9cb8e2;
      color: #fff;
      z-index:4;
    }

    .table-scroll th:first-child, .table-scroll th:nth-child(2) {
      position: -webkit-sticky;
      position: sticky;
      left: 0;
      z-index: 4;
    }
    .level2, .level3, .level4, .level5, .level6 {
      position: -webkit-sticky !important;
      position: sticky !important;
      z-index: 4 !important;
    }
    .level2{ left: 90px; }
    .level3{ left: 130px; }
    .level4{ left: 170px; }
    .level5{ left: 210px; }
    .level6{ left: 250px; }
    .table-scroll th:nth-child(2){
      left: 50px;
    }
    .table-scroll thead th{
      z-index: 4;
    }
    .table-scroll thead th:first-child, .table-scroll thead th:nth-child(2),
    .table-scroll tfoot th:first-child, .table-scroll tfoot th:nth-child(2) {
      z-index: 5;
    }
    .table-scroll thead tr:nth-child(2) th:first-child, .table-scroll thead tr:nth-child(2) th:nth-child(2){
      z-index: 1;
    }
    .fs13 {
      font-size: 13px !important;
    }
  </style>
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->
    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card mb-3">
          <div class="card-body pt-2 pb-2 pl-5">
            <div class="row">            
              <div class="col-4 col-lg-4">
                <div class="form-group row">
                  <label for="bln" class="col-4 col-lg-3 col-form-label">Tahun</label>
                  <div class="input-group input-group-sm col-8 col-lg-9">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                    <input id="tahunSelected" type="text" name="Bulan" value="{{ date('Y') }}" data-date-format="yyyy" class="form-control">
                  </div>
                </div>
              </div>
              @if(Auth::getUser()->level_user != '1' && Auth::getUser()->is_leader != 'Y')
                <input type="hidden" name="id_pegawai" id="idPegawai" value="{{ $data['pegawai']->id_pegawai }}" class="form-control">
              @else
                <div class="col-4 col-lg-4">
                  <div class="form-group row">
                    <label for="idPegawai" class="col-4 col-lg-3 col-form-label">Pegawai</label>
                    <div class="col-8 col-lg-9">
                      <select class="form-control" name="id_pegawai" id="idPegawai">
                        <option value="" disabled selected> .:: Pilih Pegawai ::. </option>
                        @foreach ($data['pegawai'] as $pegawai)
                          <option value="{{ $pegawai->id_pegawai }}">{{ $pegawai->nama }} @if(Auth::getUser()->level_user == '1')( {{$pegawai->satuanKerja->nama }} )@endif</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              @endif
              <div class="col-2 col-lg-2">
                <div class="form-group row">
                  <a href="javascript:void(0)" class="btn btn-xs btn-primary btn-search" onclick="getDataButir()"><span class="fas fa-search"></span> Cari</a>

                  <a href="javascript:void(0)" class="btn btn-xs btn-success btn-search" onclick="ExportToExcel('my-table-id', 'Rekap Kegiatan Pegawai')"><span class="fas fa-download"></span> Download</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-body">
            <div class="row" id="my-table-id">
              <div class="col-sm-12 col-lg-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panelHasilRekap" style="display: none">
                  <center><h5>REKAP KEGIATAN PEGAWAI <span class="textProfesi">-</span> TAHUN <span class="textTahun">-</span></h5></center>
                  <div class="row">
                  <div class="col-lg-6 col-md-6 col-xs-12 row">
                    <label class="col-4 fs13">NAMA</label>
                    <label class="col-7 fs13 textNama">: -</label>
                  </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-xs-12 row">
                      <label class="col-4 fs13">NIP</label>
                      <label class="col-7 fs13 textNIP">: -</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12 row">
                      <div class="col-12 row">
                        <label class="col-4 fs13">PANGKAT/GOLONGAN</label>
                        <label class="col-7 fs13 textPangkatGol" style="padding-left: 25px;">: -</label>
                      </div>
                      <div class="col-12 row">
                        <label class="col-4 fs13">UNIT KERJA</label>
                        <label class="col-7 fs13 textUnit" style="padding-left: 25px;">: -</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12 row">
                      <div class="col-12 row">
                        <label class="col-4 fs13">UNIT ORGANISASI</label>
                        <label class="col-7 fs13" style="padding-left: 25px;">: Dinas Kesehatan Kabupaten</label>
                      </div>
                      <div class="col-12 row">
                        <label class="col-4 fs13">KABUPATEN/KOTA</label>
                        <label class="col-7 fs13" style="padding-left: 25px;">: Sidoarjo</label>
                      </div>
                    </div>
                  </div>
                  <div id="table-scroll" class="table-scroll">
                    <table id="main-table" class="main-table">
                      <thead>
                        <tr>
                          <th rowspan="2" width="50px" class="text-center txt-middle">No</th>
                          <th rowspan="2" colspan="15" class="text-center txt-middle" width="600px">BUTIR KEGIATAN</th>
                          <th rowspan="2" class="text-center txt-middle" width='100px'>Angka Kredit</th>
                          <th rowspan="2" class=" text-center txt-middle" width='100px'>Satuan Hasil</th>
                          <th colspan="6" width='600px' class="text-center">Jumlah Prestasi Kerja Semester I</th>
                          <th rowspan="2" class="text-center txt-middle" width='70px'>JML</th>
                          <th rowspan="2" class="text-center txt-middle" width='70px'>AK</th>
                          <th colspan="6" width='600px' class="text-center">Jumlah Prestasi Kerja Semester I</th>
                          <th rowspan="2" class="text-center txt-middle" width='70px'>JML</th>
                          <th rowspan="2" class="text-center txt-middle" width='70px'>AK</th>
                          <th rowspan="2" class="text-center txt-middle" width='100px'>Angka Kredit</th>                          
                        </tr>
                        <tr>                          
                          <th width="50px" class="text-center">JAN</th>                          
                          <th width="50px" class="text-center">FEB</th>                          
                          <th width="50px" class="text-center">MARET</th>                          
                          <th width="50px" class="text-center">APRIL</th>                          
                          <th width="50px" class="text-center">MEI</th>                          
                          <th width="50px" class="text-center">JUNI</th>
                          <th width="50px" class="text-center">JULI</th>                          
                          <th width="50px" class="text-center">AGUS</th>                          
                          <th width="50px" class="text-center">SEPT</th>                          
                          <th width="50px" class="text-center">OKT</th>                          
                          <th width="50px" class="text-center">NOP</th>                          
                          <th width="50px" class="text-center">DES</th>                          
                        </tr>
                      </thead>
                      <tbody class="resultRekap">
                        <tr><th colspan="35">.:: Data Tidak Ditemukan ::.</th></tr>                     
                      </tbody>                      
                    </table>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script type="text/javascript">
    $(document).ready(function () {
    });

    @if(Auth::getUser()->level_user == '1' || Auth::getUser()->is_leader == 'Y')
      $('#idPegawai').chosen();
    @endif

    $('#tahunSelected').datetimepicker({ weekStart: 2, todayBtn:  1, autoclose: 1, todayHighlight: 0, startView: 4, minView: 4, forceParse: 0, });    

    function getDataButir() {
      $('.panelHasilRekap').show();
      $('.resultRekap').html('<tr><td colspan="35" style="font-style:italic">Memuat Data...</td></tr>');
      var tahun = $('#tahunSelected').val();
      var idPegawai = $('#idPegawai').val();
      var idPak = '';
      var tag = '';
      $.post("{!! route('loadRekapKegiatan') !!}",{tahun:tahun,idPegawai:idPegawai}).done(function(result){
        if (result.status == 'success') {
          $('.textProfesi').html(result.data.pegawai.nama_profesi.toUpperCase());
          $('.textTahun').html(tahun);
          $('.textNama').html(': '+result.data.pegawai.nama_pegawai);
          $('.textNIP').html(': '+result.data.pegawai.no_nip);
          if (result.data.pegawai.status_pegawai == 'PNS') {
            var pangkat = result.data.pegawai.nama_jabatan;
            var golong = result.data.pegawai.nama_golongan;
            var pangkatGol = pangkat+'/'+golong;
          }else{
            var pangkatGol = '-';
          }
          $('.textPangkatGol').html(': '+pangkatGol);
          $('.textUnit').html(': '+result.data.pegawai.nama_unit_kerja);

          $.each(result.data.butir_kegiatan, function(k,uns){
            tag += '<tr>';
            tag += '<th width="50px" style="background-color: #becee6 !important;"></th>';
            tag += '<th colspan="15" width="600px" style="background-color: #becee6 !important;">'+uns.nama+'</th>';
            tag += '<td colspan="19" style="background-color: #becee6 !important;"></td>';
            tag += '</tr>';
            if (uns.row.length > 0) {
              var tempNo1 = 0;
              $.each(uns.row, function(k,v1){
                tempNo1++;
                if (v1.statusRow == 'Parent') {
                  tag += tagRow(v1, KonDecRomawi(tempNo1), 'parent');
                  if (v1.row.length > 0) {
                    var tempNo2 = 0; // Hurug Awal
                    var tempNo2n = 0; // Hurug Akhir
                    var No2 = '';
                    var No2n = '';
                    $.each(v1.row, function(k,v2){
                      if (tempNo2n == 26) {
                        tempNo2++;
                        No2 = String.fromCharCode(64 + tempNo2);
                      }
                      tempNo2n++;
                      No2n = No2+''+String.fromCharCode(64 + tempNo2n);
                      if (v2.statusRow == 'Parent') {
                        tag += tagRow(v2, No2n, 'parent');
                        if (v2.row.length > 0) {
                          var tempNo3 = 0;
                          $.each(v2.row, function(k,v3){
                            tempNo3++;
                            if (v3.statusRow == 'Parent') {
                              tag += tagRow(v3, tempNo3, 'parent');
                              if (v3.row.length > 0) {
                                var tempNo4 = 0; // Hurug Awal
                                var tempNo4n = 0; // Hurug Akhir
                                var No4 = '';
                                var No4n = '';
                                $.each(v3.row, function(k,v4){
                                  if (tempNo4n == 26) {
                                    tempNo4++;
                                    No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                  }
                                  tempNo4n++;
                                  No4n = No4+''+String.fromCharCode(64 + tempNo4n).toLowerCase();
                                  if (v4.statusRow == 'Parent') {
                                    tag += tagRow(v4, No4n, 'parent');
                                    if (v4.row.length > 0) {
                                      var No5 = '<i class="fas fa-angle-double-right"></i>';
                                      $.each(v4.row, function(k,v5){
                                        if (v5.statusRow == 'Parent') {
                                          tag += tagRow(v5, No5, 'parent');
                                          if (v5.row.length > 0) {
                                            var No6 = '<i class="fas fa-terminal"></i>';
                                            $.each(v5.row, function(k,v6){
                                              if(v6.statusRow == 'Parent'){
                                                tag += tagRow(v6, No6, 'parent');
                                              }else{
                                                tag += tagRow(v6, No6, 'child');
                                                if (idPak == '') { idPak = v6.id_pak_master; }
                                                else{ idPak = idPak+","+v6.id_pak_master; }
                                              }
                                            });
                                          }
                                        }else{
                                          tag += tagRow(v5, No5, 'child');
                                          if (idPak == '') { idPak = v5.id_pak_master; }
                                          else{ idPak = idPak+","+v5.id_pak_master; }
                                        }
                                      });
                                    }
                                  }else{
                                    tag += tagRow(v4, No4n, 'child');
                                    if (idPak == '') { idPak = v4.id_pak_master; }
                                    else{ idPak = idPak+","+v4.id_pak_master; }
                                  }
                                });
                              }
                            }else{
                              tag += tagRow(v3, tempNo3, 'child');
                              if (idPak == '') { idPak = v3.id_pak_master; }
                              else{ idPak = idPak+","+v3.id_pak_master; }
                            }
                          });
                        }
                      }else{
                        tag += tagRow(v2, No2n, 'child');
                        if (idPak == '') { idPak = v2.id_pak_master; }
                        else{ idPak = idPak+","+v2.id_pak_master; }
                      }
                    });
                  }
                }else{
                  tag += tagRow(v1, KonDecRomawi(tempNo1), 'child');
                  if (idPak == '') { idPak = v1.id_pak_master; }
                  else{ idPak = idPak+","+v1.id_pak_master; }
                }
              });
            }else{
              var colTotal = result.row.jumlahTanggal + 4;
              tag += '<tr><th colspan="35" class="text-left">.:: Data Butir Unsur Tidak Ditemukan ::.</th></tr>';
            }
          });
        }else{
          var colTotal = result.row.jumlahTanggal + 4;
          tag += '<tr><th colspan="35" class="text-center">.:: Data Tidak Ditemukan ::.</th></tr>';
        }
        $('.resultRekap').empty();
        $('.resultRekap').html(tag);
        LoadGridRekap(idPegawai, tahun, idPak);
      }).fail(function() {
        getDataButir();
      });
    }

    function tagRow(keyData, nomor, status) {
      var tag = '';
      tag += '<tr>';
      if (status == 'parent') {
        var narasi = keyData.butir_kegiatan;
      }else{
        var narasi = keyData.butir_kegiatan+'<br>( Tiap '+keyData.jum_min+' '+keyData.satuan+' '+keyData.points+' ) <span style="float:right;">'+keyData.jabatan.nama+'</span>';
      }
      switch(keyData.level) {
        case 1:
          tag += '<th width="50px" class="text-center">'+nomor+'.</th>';
          tag += '<th colspan="15" width="600px">'+narasi+'</th>';
          break;
        case 2:
          tag += '<th width="50px"></th>';
          tag += '<th class="text-center" width="40px">'+nomor+'.</th>';
          tag += '<th class="level2" width="560px" colspan="14">'+narasi+'</th>';
          break;
        case 3:
          tag += '<th width="50px"></th>';
          tag += '<th width="40px"></th>';
          tag += '<th class="level2 text-center" width="40px">'+nomor+'.</th>';
          tag += '<th class="level3" width="520px" colspan="13">'+narasi+'</th>';
          break;
        case 4:
          tag += '<th width="50px"></th>';
          tag += '<th width="40px"></th>';
          tag += '<th class="level2" width="40px"></th>';
          tag += '<th class="level3 text-center" width="40px">'+nomor+'.</th>';
          tag += '<th class="level4" width="480px" colspan="12">'+narasi+'</th>';
          break;
        case 5:
          tag += '<th width="50px"></th>';
          tag += '<th  width="40px"></th>';
          tag += '<th class="level2" width="40px"></th>';
          tag += '<th class="level3" width="40px"></th>';
          tag += '<th class="level4 text-center" width="40px">'+nomor+'</th>';
          tag += '<th class="level5" width="440px" colspan="11">'+narasi+'</th>';
          break;
        case 6:
          tag += '<th width="50px"></th>';
          tag += '<th width="40px"></th>';
          tag += '<th class="level2" width="40px"></th>';
          tag += '<th class="level3" width="40px"></th>';
          tag += '<th class="level4" width="40px"></th>';
          tag += '<th class="level5 text-center" width="40px">'+nomor+'.</th>';
          tag += '<th class="level6" width="400px" colspan="10">'+narasi+'</th>';
          break;
      }
      if (status == 'parent') {
        tag += '<td colspan="19"></td>';
      }else{
        tag += '<td class="panelIsiKegiatan" id="ak_'+keyData.id_pak_master+'" align="center">'+keyData.points+'</td>';
        tag += '<td class="panelIsiKegiatan" id="satuan_'+keyData.id_pak_master+'" align="center">'+keyData.satuan+'</td>';
        for (var a1 = 1; a1 <= 6; a1++) {
          tag += '<td class="panelIsiKegiatan" id="nilai_'+keyData.id_pak_master+'-'+a1+'"></td>';
        }
        tag += '<td class="panelIsiKegiatan" id="jml_nilai1_'+keyData.id_pak_master+'" align="center"></td>';
        tag += '<td class="panelIsiKegiatan" id="jml_ak1_'+keyData.id_pak_master+'" align="center"></td>';
        for (var a2 = 7; a2 <= 12; a2++) {
          tag += '<td class="panelIsiKegiatan" id="nilai_'+keyData.id_pak_master+'-'+a2+'"></td>';
        }
        tag += '<td class="panelIsiKegiatan" id="jml_nilai2_'+keyData.id_pak_master+'" align="center"></td>';
        tag += '<td class="panelIsiKegiatan" id="jml_ak2_'+keyData.id_pak_master+'" align="center"></td>';
        tag += '<td class="panelIsiKegiatan" id="total_ak_'+keyData.id_pak_master+'" align="center"></td>';
      }
      tag += '</tr>';

      return tag;
    }

    function LoadGridRekap(idPegawai, tahun, idPak) {
      $.post("{!! route('getNilaiRekapKegiatan') !!}",{idPegawai:idPegawai,tahun:tahun,idPak:idPak}).done(function(result){
        var idpaks = idPak.split(',');
        var arrPaks = [];
        for (var i = 0; i < idpaks.length; i++) {
          arrPaks[i] = {
            'id_pak' : idpaks[i],
            'jm_1' : 0,
            'jm_2' : 0,
            'jm_3' : 0,
            'jm_4' : 0,
            'jm_5' : 0,
            'jm_6' : 0,
            'jumlah_px1' : 0,
            'jumlah_point1' : 0,
            'jm_7' : 0,
            'jm_8' : 0,
            'jm_9' : 0,
            'jm_10' : 0,
            'jm_11' : 0,
            'jm_12' : 0,
            'jumlah_px2' : 0,
            'jumlah_point2' : 0,
            'total_point' : 0
          };
        }
        var totalAk = 0;
        var totalPoint = 0;
        if (result.status == 'success') {
          $.each(result.row, function(k,v){
            for (var j = 0; j < arrPaks.length; j++) {
              if (v.master_kegiatan_id == arrPaks[j]['id_pak']) {
                arrPaks[j]['jm_'+v.bulan] = parseFloat(arrPaks[j]['jm_'+v.bulan]) + parseFloat(v.jumlah_px_pemohon);
                if (v.bulan <= 6) {
                  arrPaks[j]['jumlah_px1'] = parseFloat(arrPaks[j]['jumlah_px1']) + parseFloat(v.jumlah_px_pemohon);
                  arrPaks[j]['jumlah_point1'] = parseFloat(arrPaks[j]['jumlah_point1']) + parseFloat(v.poin_pemohon);
                }else{
                  arrPaks[j]['jumlah_px2'] = parseFloat(arrPaks[j]['jumlah_px2']) + parseFloat(v.jumlah_px_pemohon);
                  arrPaks[j]['jumlah_point2'] = parseFloat(arrPaks[j]['jumlah_point2']) + parseFloat(v.poin_pemohon);
                }
                arrPaks[j]['total_point'] = parseFloat(arrPaks[j]['total_point']) + parseFloat(v.poin_pemohon);
              }
            }
          });
        }
        for (var s = 0; s < arrPaks.length; s++) {
          for (var b = 1; b <= 12; b++) {
            $('#nilai_'+arrPaks[s]['id_pak']+'-'+b).html(arrPaks[s]['jm_'+b]);
          }
          $('#jml_nilai1_'+arrPaks[s]['id_pak']).html(arrPaks[s]['jumlah_px1']);
          $('#jml_ak1_'+arrPaks[s]['id_pak']).html(arrPaks[s]['jumlah_point1']);
          $('#jml_nilai2_'+arrPaks[s]['id_pak']).html(arrPaks[s]['jumlah_px2']);
          $('#jml_ak2_'+arrPaks[s]['id_pak']).html(arrPaks[s]['jumlah_point2']);
          $('#total_ak_'+arrPaks[s]['id_pak']).html(arrPaks[s]['total_point']);
        }
      });
    }

    function KonDecRomawi(angka) {
      var hasil = "";
      if (angka < 1 || angka > 5000) {
        hasil += "Batas Angka 1 s/d 5000";
      }else{
        while (angka >= 1000) {
          hasil += "M";
          angka = angka - 1000;
        }
      }
      if (angka >= 500) {
        if (angka > 500) {
          if (angka >= 900) {
            hasil += "CM";
            angka = angka - 900;
          } else {
            hasil += "D";
            angka = angka - 500;
          }
        }
      }
      while (angka >= 100) {
        if (angka >= 400) {
          hasil += "CD";
          angka = angka - 400;
        } else {
          angka = angka - 100;
        }
      }
      if (angka >= 50) {
        if (angka >= 90) {
          hasil += "XC";
          angka = angka - 90;
        } else {
          hasil += "L";
          angka = angka - 50;
        }
      }
      while (angka >= 10) {
        if (angka >= 40) {
          hasil += "XL";
          angka = angka - 40;
        } else {
          hasil += "X";
          angka = angka - 10;
        }
      }
      if (angka >= 5) {
        if (angka == 9) {
          hasil += "IX";
          angka = angka - 9;
        } else {
          hasil += "V";
          angka = angka - 5;
        }
      }
      while (angka >= 1) {
        if (angka == 4) {
          hasil += "IV";
          angka = angka - 4;
        } else {
          hasil += "I";
          angka = angka - 1;
        }
      }
      return hasil;
    }

    var ExportToExcel = (function() {
     var uri = 'data:application/vnd.ms-excel;base64,'
       , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
       , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
       , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
     return function(table, name) {
       if (!table.nodeType) table = document.getElementById(table)
       var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
       var downloadName = prompt('nama file yang akan di download', 'Rekap Kegiatan Pegawai');
       var link = document.createElement("a");
       link.download = downloadName + ".xls";
       // window.location.href = uri + base64(format(template, ctx))
       link.href = uri + base64(format(template, ctx));
       link.click();
     }
    })()
  </script>
@stop