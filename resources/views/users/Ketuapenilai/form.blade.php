<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  @if($user == "")
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
  <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Pegawai <small style="color:red;">*</small></label>
          <div class="col-8 col-lg-9">
            <select id="inputPegawai" class="form-control" name="id_pegawai">
              <option value="" disabled selected> .:: Pilih Pegawai ::. </option>
              @foreach ($pegawais as $pegawai)
                <option value="{{ $pegawai->id_pegawai }}" @if($user != '') @if($pegawai->id_pegawai == $user->pegawai->id_pegawai) selected @endif @endif>{{ $pegawai->nama }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="form-row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <h5 class="card-header">Data Pegawai</h5>
          <div class="form-group row">
            <label for="inputEmail" class="col-4 col-lg-3 col-form-label">Email <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputEmail" type="text" value="@if($user != ''){{ $user->email }}@endif" placeholder="Email" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputNama" type="text" value="@if($user != ''){{ $user->name }}@endif" placeholder="Nama" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputGender" class="col-4 col-lg-3 col-form-label">Jenis Kelamin <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <?php
                if ($user != '') {
                  if ($user->pegawai->jenis_kelamin == 'L') {
                    $gender = 'Laki - Laki';
                  }else{
                    $gender = 'Perempuan';
                  }
                }else{
                  $gender = "";
                }
              ?>
              <input id="inputGender" type="text" value="{{ $gender }}" placeholder="Jenis Kelamin" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">TTL</label>
            <div class="col-8 col-lg-9 row pr-0">
              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <input id="inputTmpLahir" type="text" value="@if($user != ""){{ $user->pegawai->tempat_lahir }}@endif" placeholder="Tempat Lahir" class="form-control backWhite" readonly>
              </div>
              <div class="input-group input-group-sm col-lg-5 col-md-5 col-sm-12 col-xs-12 pr-0">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                <input id="inputTglLahir" type="text" value="@if($user != ""){{ date('d-m-Y', strtotime($user->pegawai->tanggal_lahir)) }}@endif" placeholder="dd-mm-yyyy" class="form-control backWhite" readonly  data-date-format="dd-mm-yyyy">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Profesi <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputProfesi" type="text" value="@if($user != ""){{ $user->pegawai->typeProfesi->nama }}@endif" placeholder="Profesi" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Satuan Kerja <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputSatKer" type="text" value="@if($user != ""){{ $user->pegawai->satuanKerja->nama }}@endif" placeholder="Satuan Kerja" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">No NIP <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputNIP"  type="text" value="@if($user != ""){{ $user->pegawai->no_nip }}@endif" placeholder="No NIP" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">No Karpeg <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputKarpeng"  type="text" value="@if($user != ""){{ $user->pegawai->no_karpeng }}@endif" placeholder="No Kartu Pegawai" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label p-b-0 p-t-0">Pendidikan Terakhir<br>Sesuai SK</label>
            <div class="col-8 col-lg-9">
              <input id="inputPendidikan" type="text" value="@if($user != ''){{ $user->pegawai->pendidikan_terakhir }}@endif" placeholder="Pendidikan Terakhir Sesuai SK" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Tahun Lulus</label>
            <div class="col-8 col-lg-9">
              <input id="inputThnLulus" type="text" value="@if($user != ''){{ $user->pegawai->pendidikan_tahun }}@endif" placeholder="Tahun Lulus" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <h5 class="card-header">Data Jabatan & Fungsi <small style="color:red;">*</small></h5>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Golongan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputGolongan" type="text" value="@if($user != ""){{ $user->pegawai->golongan->nama }}@endif" placeholder="Golongan" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Golongan TMT</label>
            <div class="input-group input-group-sm col-8 col-lg-9">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
              <input id="inputGolonganTmt" type="text" value="@if($user != ''){{ date('d-m-Y', strtotime($user->pegawai->golongan_tmt)) }}@endif" placeholder="Golongan TMT" class="form-control backWhite" readonly data-date-format="dd-mm-yyyy">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Jabatan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputJabatan" type="text" value="@if($user != ''){{ $user->pegawai->jabatan->nama }}@endif" placeholder="Jabatan" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
            <div class="input-group input-group-sm col-8 col-lg-9">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
              <input id="inputJabatanTmt" type="text" value="@if($user != ''){{ date('d-m-Y', strtotime($user->pegawai->jabatan_tmt)) }}@endif" placeholder="Jabatan TMT" class="form-control backWhite" readonly data-date-format="dd-mm-yyyy">
            </div>
          </div>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span>
              Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span
                class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  $('#inputPegawai').chosen();
  $('#inputPegawai').change(function(){
    var id= $('#inputPegawai').val();
    $.post("{!! route('getPegawai') !!}", {id:id}).done(function(data){
      if (data.status == 'success') {
        $('#inputEmail').val(data.data.user.email);
        $('#inputNama').val(data.data.user.name);
        if (data.data.jenis_kelamin == 'L') {
          var gender = 'Laki - laki';
        }else{
          var gender = 'Perempuan'
        }
        $('#inputGender').val(gender);
        $('#inputTmpLahir').val(data.data.tempat_lahir);
        $('#inputTglLahir').val(formatTgl(data.data.tanggal_lahir));
        $('#inputProfesi').val(data.data.type_profesi.nama);
        $('#inputSatKer').val(data.data.satuan_kerja.nama);
        $('#inputNIP').val(data.data.no_nip);
        $('#inputKarpeng').val(data.data.no_karpeng);
        $('#inputPendidikan').val(data.data.pendidikan_terakhir);
        $('#inputThnLulus').val(data.data.pendidikan_tahun);
        $('#inputGolongan').val(data.data.golongan.nama);
        $('#inputGolonganTmt').val(formatTgl(data.data.golongan_tmt));
        $('#inputJabatan').val(data.data.jabatan.nama);
        $('#inputJabatanTmt').val(formatTgl(data.data.jabatan_tmt));
      }else{
        $('#inputEmail').val('');
        $('#inputNama').val('');
        $('#inputGender').val('');
        $('#inputTmpLahir').val('');
        $('#inputTglLahir').val('');
        $('#inputProfesi').val('');
        $('#inputSatKer').val('');
        $('#inputNIP').val('');
        $('#inputKarpeng').val('');
        $('#inputPendidikan').val('');
        $('#inputThnLulus').val('');
        $('#inputGolongan').val('');
        $('#inputGolonganTmt').val('');
        $('#inputJabatan').val('');
        $('#inputJabatanTmt').val('');
        swal('Whoops !', data.message, 'warning');
      }
    });
  });

  function formatTgl(tgl) {
    var pecah = tgl.split("-");
    var hasil = pecah[2]+'-'+pecah[1]+'-'+pecah[0];
    return hasil;
  }

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveUserKetuaPenilai') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
