 <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <h5 class="card-header bg-info"><i class="fas fa-search mr-2"></i> Detail Data</h5>
  <div class="card-body">
    <div class="form-row">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 offset-lg-1 offset-md-1">
          <div class="crop-edit">
            <center>
              @if ($user->photo != null && is_file('upload/users/'.$user->photo))
                <img src="{!! url('upload/users/'.$user->photo) !!}" class="img-thumbnail mr-3" height="250">
              @else
                @if ($user->gender == 'L')
                  <img src="{!! url('assets/images/user-man.png') !!}" class="img-thumbnail mr-3" height="250">
                @else
                  <img src="{!! url('assets/images/user_woman.png') !!}" class="img-thumbnail mr-3" height="250">
                @endif
              @endif
            </center>
          </div>
        </div>
      </div>
      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 offset-lg-1 offset-md-1">
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Username</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $user->username }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Email</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $user->email }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Nama</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $user->name }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
          <div class="col-8 col-lg-9 row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline mb-0">
                <input type="radio" class="custom-control-input" @if($user->gender == 'L') checked @endif disabled><span class="custom-control-label">Laki - laki</span>
              </label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline mb-0">
                <input type="radio"class="custom-control-input" @if($user->gender == 'P') checked @endif disabled><span class="custom-control-label">Perempuan</span>
              </label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Alamat</label>
          <div class="col-8 col-lg-9">
            <textarea rows="2" class="form-control backWhite" readonly>{{ $user->address }}</textarea>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Telepon</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $user->phone }}" class="form-control backWhite" readonly>
          </div>
        </div>
      </div>
    </div>
    <div class="row pt-2 pt-sm-2 mt-1">
      <div class="col-sm-12 pl-0">
        <p class="text-right">
          <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
        </p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });
</script>
