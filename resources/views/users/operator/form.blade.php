<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  @if($user == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($user != "")
        <input type="hidden" name="id" value="{{ $user->id }}">
      @endif
      <div class="form-row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 offset-lg-1 offset-md-1">
						<div class="crop-edit">
							<center>
                @if ($user != '')
                  @if ($user->photo != null && is_file('upload/users/'.$user->photo))
                    <img id="preview-photo" src="{!! url('upload/users/'.$user->photo) !!}" class="img-thumbnail mr-3" height="250">
                  @else
                    @if ($user->gender == 'L')
                      <img id="preview-photo" src="{!! url('assets/images/user-man.png') !!}" class="img-thumbnail mr-3" height="250">
                    @else
                      <img id="preview-photo" src="{!! url('assets/images/user_woman.png') !!}" class="img-thumbnail mr-3" height="250">
                    @endif
                  @endif
                @else
                  <img id="preview-photo" src="{!! url('assets/images/user-man.png') !!}" class="img-thumbnail mr-3" height="250">
                @endif
							</center>
						</div>
					</div>
					<div class='clearfix mb-2'></div>
          <div class="custom-file mb-3">
            <input type="file" class="custom-file-input" id="customFile" name="photo" onchange="loadFilePhoto(event)">
            <label class="custom-file-label" for="customFile">File Input</label>
            @if ($user != '')
              <i>* Kosongi jika foto tidak ingin dirubah</i>
            @endif
          </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 offset-lg-1 offset-md-1">
          <div class="form-group row">
            <label for="inputUsername" class="col-4 col-lg-3 col-form-label">Username</label>
            <div class="col-8 col-lg-9">
              <input id="inputUsername" type="text" name="Username" value="@if($user != '') {{ $user->username }} @endif" placeholder="Username" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail" class="col-4 col-lg-3 col-form-label">Email</label>
            <div class="col-8 col-lg-9">
              <input id="inputEmail" type="text" name="Email" value="@if($user != '') {{ $user->email }} @endif" placeholder="Email" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama</label>
            <div class="col-8 col-lg-9">
              <input id="inputNama" type="text" name="Nama" value="@if($user != '') {{ $user->name }} @endif" placeholder="Nama" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
            <div class="col-8 col-lg-9 row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="Jenis_Kelamin" value="Laki - Laki" class="custom-control-input" @if($user != '') @if($user->gender == 'L') checked @endif @endif><span class="custom-control-label">Laki - laki</span>
                </label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="Jenis_Kelamin" value="Perempuan" class="custom-control-input" @if($user != '') @if($user->gender == 'P') checked @endif @endif><span class="custom-control-label">Perempuan</span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputAlamat" class="col-4 col-lg-3 col-form-label">Alamat</label>
            <div class="col-8 col-lg-9">
              <textarea id="inputAlamat" name="Alamat" rows="2" placeholder="Alamat" class="form-control">@if($user != '') {{ $user->address }} @endif</textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputTelepon" class="col-4 col-lg-3 col-form-label">Telepon</label>
            <div class="col-8 col-lg-9">
              <input id="inputTelepon" type="text" name="Telepon" value="@if($user != '') {{ $user->phone }} @endif" placeholder="Telepon" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  function loadFilePhoto(event) {
		var image = URL.createObjectURL(event.target.files[0]);
		$('#preview-photo').fadeOut(function(){
			$(this).attr('src', image).fadeIn().css({
				'-webkit-animation' : 'showSlowlyElement 700ms',
				'animation'         : 'showSlowlyElement 700ms'
			});
		});
	};

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveUserOperator') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
