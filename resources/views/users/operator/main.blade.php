@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item">Pengguna</li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12 col-lg-8 mb-2 mb-sm-2">
                <button type="button" class="btn btn-xs btn-primary btn-add"><span class="fas fa-plus"></span> Tambah</button>
                <button type="button" class="btn btn-xs btn-warning btn-update"><span class="fas fa-pencil-alt"></span> Ubah</button>
                <button type="button" class="btn btn-xs btn-info btn-detail"><span class="fas fa-search"></span> Detail</button>
                <button type="button" class="btn btn-xs btn-secondary btn-resets"><span class="fas fa-sync"></span> Reset Password</button>
                <button type="button" class="btn btn-xs btn-dark btn-banned"><span class="fas fa-lock"></span> Non Aktifkan</button>
                <button type="button" class="btn btn-xs btn-success btn-actives"><span class="fas fa-lock-open"></span> Aktifkan</button>
                <button type="button" class="btn btn-xs btn-danger btn-delete"><span class="fas fa-trash"></span> Hapus</button>
              </div>
              <div class="col-sm-12 col-lg-4 mb-2 mb-sm-2">
                <span style="float:right">
                  <input type="text" placeholder="Cari data" name="search-data" id="search-data" value="" class="textbox" autocomplete="off" aria-controls="idtables" onkeydown="if (event.keyCode == 13) {return searchData();}">
                </span>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-striped table-bordered first" id='loadData'></table>
                <div class="form-row mt-3">
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                    <select class="form-control" id="lngDt" style="width:60px;"></select>
                  </div>
                  <label class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2 text-center info-data"></label>
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                    <nav aria-label="Page navigation example" class="pl-right">
                      <ul class="pagination" id="paging"></ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script src="{{ url('/')}}/js/cusLoadData.js"></script>
  <script type="text/javascript">
    var dtLoad = {
        url                 : "{!! route('loadUserOperator') !!}",
        primaryField        : 'id',
        customFilter        : false,
        column              : [
          {field: 'username', title: 'Username', width: 100, align: 'left'},
          {field: 'email', title: 'Email', width: 150, align: 'left'},
          {field: 'name', title: 'Nama', width: 200, align: 'left'},
          {field: 'jnKelamin', title: 'Jenis Kelamin', width: 100, align: 'left',
              rowStyler: function(rowData, rowIndex) {
                return jnKelamin(rowData, rowIndex);
              }
          },
          {field: 'phone', title: 'Telepon', width: 100, align: 'left'},
          {field: 'Status', title: 'Status', width: 100, align: 'center',
              rowStyler: function(rowData, rowIndex) {
                return Status(rowData, rowIndex);
              }
          },
        ],
    }
    $(document).ready(function() {
      loadData(dtLoad, 1);
    });
    function searchData() { reloadData(1); }

    function jnKelamin(rowData, rowIndex) {
      if (rowData.gender == 'L') {
        var tag = 'Laki - laki';
      }else{
        var tag = 'Perempuan';
      }
      return tag;
    }

    function Status(rowData, rowIndex) {
      var isBanned = rowData.is_banned;

      if (isBanned == '0') {
        var tmp = "<span class='badge badge-success'><i class='fas fa-lock-open'></i> Aktif</span>";
      }else{
        var tmp = "<span class='badge badge-danger'><i class='fas fa-lock'></i> Tidak Aktif</span>";
      }

      return tmp;
    }

    $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formUserOperator') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    $('.btn-update').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('formUserOperator') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-detail').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('detailUserOperator') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-resets').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin mereset Password Pengguna Ini?",
            text: "Password akan direset sesuai dengan Username Pengguna!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('resetUserOperator') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                reloadData(1);
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-banned').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin Menon-Aktifkan Akun Pengguna Ini?",
            text: "Akun Akan Dinon-Aktifkan, Sehingga Pengguna Tidak Dapat Login Sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('bannedUserOperator') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                reloadData(1);
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-actives').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin Meng-Aktifkan Akun Pengguna Ini?",
            text: "Akun Akan Diaktifkan, Sehingga Pengguna Dapat Kembali Login Sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('activeUserOperator') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                reloadData(1);
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-delete').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin menghapus Akun Pengguna Ini?",
            text: "Akun Akan Dihapus dari Sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('removeUserOperator') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                reloadData(1);
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });
  </script>
@stop
