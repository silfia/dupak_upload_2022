@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item">Pengguna</li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12 col-lg-8 mb-2 mb-sm-2">
                <button type="button" class="btn btn-xs btn-primary btn-add"><span class="fas fa-plus"></span> Tambah data baru</button>
                <button type="button" class="btn btn-xs btn-warning btn-update"><span class="fas fa-pencil-alt"></span> Ubah</button>
                <button type="button" class="btn btn-xs btn-info btn-detail"><span class="fas fa-search"></span> Detail</button>
                <button type="button" class="btn btn-xs btn-secondary btn-resets"><span class="fas fa-sync"></span> Reset Password</button>
                <button type="button" class="btn btn-xs btn-dark btn-banned"><span class="fas fa-lock"></span> Non Aktifkan</button>
                <button type="button" class="btn btn-xs btn-success btn-actives"><span class="fas fa-lock-open"></span> Aktifkan</button>
                <button type="button" class="btn btn-xs btn-danger btn-delete"><span class="fas fa-trash"></span> Hapus</button>
              </div>
              
              <div class="col-md-12 col-sm-12 col-xs-12 main-layer panelSearch m-b-10">
                  <div class="form-inline pos-absolute right-10" style="float:right">
                      <div class="form-group">
                          <select class="form-control-sm form-control input-s-sm inline v-middle option-search" id="search-option">
                          </select>
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control-sm form-control" placeholder="Search" id="search">
                      </div>
                  </div>
              </div>
              <div class='clearfix'></div>

              <div class="col-md-12 p-0">
                  <div class="table-responsive">
                      <table class="table table-striped b-t b-light" id="datagrid"></table>
                  </div>
                  <footer class="panel-footer">
                      <div class="row">
                          <div class="col-sm-1 hidden-xs">
                              <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                          </div>
                          <div class="col-sm-6 text-center">
                              <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                          </div>
                          <div class="col-sm-5 text-right text-center-xs">
                              <ul class="pagination pagination-sm justify-content-center float-r m-t-0 m-b-0" id="paging"></ul>
                          </div>
                      </div>
                  </footer>
              </div>
              <div class='clearfix'></div>

            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script src="{{ url('/')}}/js/cusLoadData.js"></script>
  <script type="text/javascript">

    var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('loadUserPegawai') !!}",
      primaryField          : 'id',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      columns               : [
        {field: 'checks', title: '&nbsp', editable: false, sortable: false, width: 30, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return checks(rowData, rowIndex);
          }
        },
        {field: 'email', title: 'Email', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'no_nip', title: 'No NIP', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'nama', title: 'Nama', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'nama_golongan', title: 'Golongan', editable: false, sortable: true, width: 800, align: 'left', search: true},
        {field: 'nama_jabatan', title: 'Jabatan', editable: false, sortable: true, width: 800, align: 'left', search: true},
        {field: 'nama_satuan', title: 'Satuan Kerja', editable: false, sortable: true, width: 800, align: 'left', search: true},        
        {field: 'status_pegawai', title: 'Pegawai', editable: false, sortable: true, width: 50, align: 'left', search: true},        
        {field: 'Status', title: 'Status', sortable: true, width: 50, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return Status(rowData, rowIndex);
          }
        }
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

    function checks(rowData, rowIndex) {
      var id = rowData.id;
      var tag = '';
      tag += '<input type="checkbox" value="'+id+'" name="id_data[]" id="id_data'+id+'"/>';
      tag += '<label for="id_data'+id+'"></label>';
      return tag;
    }

    function Status(rowData, rowIndex) {
      var isBanned = rowData.is_banned;

      if (isBanned == '0') {
        var tmp = "<span class='badge badge-success'><i class='fas fa-lock-open'></i> Aktif</span>";
      } else if (isBanned == '2'){
        var tmp = "<span class='badge badge-info'><i class='fas fa-hourglass-half'></i> Menunggu</span>";
      }else{
        var tmp = "<span class='badge badge-danger'><i class='fas fa-lock'></i> Tidak Aktif</span>";
      }
      return tmp;
    }

    $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formUserPegawai') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    $('.btn-update').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('formUserPegawai') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-detail').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('detailUserPegawai') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-resets').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin mereset Password Pengguna Ini?",
            text: "Password akan direset sesuai dengan Username Pengguna!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('resetUserPegawai') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                datagrid.reload();
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-banned').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin Menon-Aktifkan Akun Pengguna Ini?",
            text: "Akun Akan Dinon-Aktifkan, Sehingga Pengguna Tidak Dapat Login Sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('bannedUserPegawai') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                datagrid.reload();
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-actives').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin Meng-Aktifkan Akun Pengguna Ini?",
            text: "Akun Akan Diaktifkan, Sehingga Pengguna Dapat Kembali Login Sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('activeUserPegawai') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                datagrid.reload();
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-delete').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin menghapus Akun Pengguna Ini?",
            text: "Akun Akan Dihapus dari Sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('removeUserPegawai') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                datagrid.reload();
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });
  </script>
@stop
