<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
	<form class="form-save">
		<div class="row">
			@if(Auth::getUser()->level_user == 1)
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="card">
						{{-- <h5 class="card-header">Perbaharui Data Pegawai</h5> --}}
						<div class="card-body">
							<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 offset-lg-1 offset-md-1">
								<div class="crop-edit">
									<center>
										@if (Auth::getUser()->photo != null && is_file('upload/users/'.Auth::getUser()->photo))
											<img id="preview-photo" src="{!! url('upload/users/'.Auth::getUser()->photo) !!}" class="img-thumbnail mr-3" height="250">
										@else
											@if (Auth::getUser()->gender == 'L')
												<img id="preview-photo" src="{!! url('assets/images/user-man.png') !!}" class="img-thumbnail mr-3" height="250">
											@else
												<img id="preview-photo" src="{!! url('assets/images/user_woman.png') !!}" class="img-thumbnail mr-3" height="250">
											@endif
										@endif
									</center>
								</div>
							</div>
							<div class='clearfix mb-2'></div>
							<div class="custom-file mb-3">
								<input type="file" class="custom-file-input" id="customFile" name="photo" onchange="loadFilePhoto(event)">
								<label class="custom-file-label" for="customFile">File Input</label>
								<i>* Kosongi jika foto tidak ingin dirubah</i>
							</div>
						</div>
					</div>
				</div>
			@endif
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="card">
					<h5 class="card-header">Perbaharui Data Pegawai</h5>
					<div class="card-body">
						<div class="form-group row">
							<label for="inputEmail" class="col-4 col-lg-3 col-form-label">Email</label>
							<div class="col-8 col-lg-9">
								<input id="inputEmail" type="text" name="Email" value="{{ Auth::getUser()->email }}" onkeyup="cekMail()" placeholder="Email" class="form-control">
								<div class='iconStatus text-green' id='icon_email'><i class="fas fa-check-circle"></i></div>
								<p class='messageError errorMail'></p>
								<input type='hidden' name='statusMail' value='Ready' id='statusMail' class='form-control'>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama</label>
							<div class="col-8 col-lg-9">
								<input id="inputNama" type="text" name="Nama" value="{{ Auth::getUser()->name }}" placeholder="Nama Pegawai" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
							<div class="col-8 col-lg-9 row">
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<label class="custom-control custom-radio custom-control-inline">
										<input type="radio" name="gender" value="L" class="custom-control-input" @if(Auth::getUser()->gender == 'L') checked @endif><span class="custom-control-label">Laki - Laki</span>
									</label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<label class="custom-control custom-radio custom-control-inline">
										<input type="radio" name="gender" value="P" class="custom-control-input" @if(Auth::getUser()->gender == 'P') checked @endif><span class="custom-control-label">Perempuan</span>
									</label>
								</div>
							</div>
						</div>
						@if(Auth::getUser()->level_user == 1)
							<div class="form-group row">
								<label for="inputAlamat" class="col-4 col-lg-3 col-form-label">Alamat</label>
								<div class="col-8 col-lg-9">
									<textarea id="inputAlamat" name="Alamat" rows="2" placeholder="Alamat" class="form-control">{{ Auth::getUser()->address }}</textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputTelepon" class="col-4 col-lg-3 col-form-label">Telepon</label>
								<div class="col-8 col-lg-9">
									<input id="inputTelepon" type="text" name="Telepon" value="{{ Auth::getUser()->phone }}" placeholder="Telepon" class="form-control">
								</div>
							</div>
						@else
							<div class="form-group row">
								<label for="inputTempatLahir" class="col-4 col-lg-3 col-form-label">TTL</label>
								<div class="col-8 col-lg-9 row pr-0">
									<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
										<input id="inputTempatLahir" type="text" name="Tempat_Lahir" value="{{ $pegawai->tempat_lahir }}" placeholder="Tempat Lahir" class="form-control">
									</div>
									<div class="input-group input-group-sm col-lg-5 col-md-5 col-sm-12 col-xs-12 pr-0">
										<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
										<input id="inputTglLahir" type="text" name="Tanggal_Lahir" value="{{ $pegawai->tanggal_lahir }}" placeholder="Tgl Lahir" class="form-control" data-date-format="dd-mm-yyyy">
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
								<div class="col-8 col-lg-9">
									<select id="inputProfesi" class="form-control" name="Profesi">
										<option value="" disabled selected> .:: Pilih Profesi ::. </option>
										@foreach ($profesi as $pf)
											<option value="{{ $pf->id_master_type }}" @if($pf->id_master_type == $pegawai->tipe_profesi_id) selected @endif>{{ $pf->nama }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputSaKer" class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
								<div class="col-8 col-lg-9">
									<select id="inputSaKer" class="form-control" name="Satuan_Kerja">
										<option value="" disabled selected> .:: Pilih Satuan Kerja ::. </option>
										@foreach ($satKers as $satKer)
											<option value="{{ $satKer->id_satuan_kerja }}" @if($satKer->id_satuan_kerja == $pegawai->satuan_kerja_id) selected @endif>{{ $satKer->nama }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Status Pegawai</label>
								<div class="col-8 col-lg-9 row">
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="status_pegawai" value="PNS" onclick="formPns('PNS')" class="custom-control-input" @if($pegawai->status_pegawai == 'PNS') checked @endif><span class="custom-control-label">PNS</span>
										</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="status_pegawai" value="Non-PNS" onclick="formPns('Non-PNS')" class="custom-control-input" @if($pegawai->status_pegawai == 'Non-PNS') checked @endif><span class="custom-control-label">Non-PNS</span>
										</label>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputNip" class="col-4 col-lg-3 col-form-label" id='labelIdentitas'>@if($pegawai->status_pegawai == 'Non-PNS') No NIK @else No NIP @endif</label>
								<div class="col-8 col-lg-9">
									<input id="inputNip" type="text" name="NIP" value="{{ $pegawai->no_nip }}" onkeyup="cekNip()" placeholder="@if($pegawai->status_pegawai == 'Non-PNS') No NIK @else No NIP @endif" class="form-control">
									<div class='iconStatus text-green' id='icon_nip'><i class="fas fa-check-circle"></i></div>
									<p class='messageError errorNip'></p>
									<input type='hidden' name='statusNip' value='Ready' id='statusNip' class='form-control'>
								</div>
							</div>
							<div class="form-group row" id='panelKarpeg' @if($pegawai->status_pegawai == 'Non-PNS') style="display:none;" @endif>
								<label for="inputKarPeng" class="col-4 col-lg-3 col-form-label">No Karpeng</label>
								<div class="col-8 col-lg-9">
									<input id="inputKarPeng" type="text" name="No_Karpeng" value="{{ $pegawai->no_karpeng }}" placeholder="No Kartu Pengenal" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPendidikan" class="col-4 col-lg-3 col-form-label p-b-0 p-t-0">Pendidikan Terakhir<br>Sesuai SK</label>
								<div class="col-8 col-lg-9">
									<input id="inputPendidikan" type="text" name="Pendidikan" value="{{ $pegawai->pendidikan_terakhir }}" placeholder="Pendidikan Terakhir Sesuai SK" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputThnLulus" class="col-4 col-lg-3 col-form-label">Tahun Lulus</label>
								<div class="col-8 col-lg-9">
									<input id="inputThnLulus" type="text" name="Tahun_Lulus" value="{{ $pegawai->pendidikan_tahun }}" placeholder="Tahun Lulus" class="form-control">
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
			@if(Auth::getUser()->level_user != 1)
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id='panelJabFus' @if($pegawai->status_pegawai == 'Non-PNS') style="display:none;" @endif>
					<div class="card">
						<h5 class="card-header">Perbaharui Data Jabatan & Fungsi</h5>
						<div class="card-body">
							<div class="form-group row">
								<label for="inputGolongan" class="col-4 col-lg-3 col-form-label">Golongan</label>
								<div class="col-8 col-lg-9">
									<select id="inputGolongan" class="form-control" name="Golongan">
										<option value="" disabled selected> .:: Pilih Golongan ::. </option>
										@foreach ($golongan as $gln)
											<option value="{{ $gln->id_master }}" @if($gln->id_master == $pegawai->golongan_id) selected @endif>{{ $gln->nama }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputGolTmt" class="col-4 col-lg-3 col-form-label">Golongan TMT</label>
								<div class="input-group input-group-sm col-8 col-lg-9">
									<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
									<input id="inputGolTmt" type="text" name="Golongan_TMT" value="@if($pegawai->golongan_tmt != ''){{ $pegawai->golongan_tmt }}@endif" placeholder="Golongan TMT" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
								<div class="col-8 col-lg-9">
									<select id="inputJabatan" class="form-control" name="Jabatan">
										<option value="" disabled selected> .:: Pilih Jabatan ::. </option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputJabTmt" class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
								<div class="input-group input-group-sm col-8 col-lg-9">
									<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
									<input id="inputJabTmt" type="text" name="Jabatan_TMT" value="@if($pegawai->jabatan_tmt != ''){{ $pegawai->jabatan_tmt }}@endif" placeholder="Jabatan TMT" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
				<button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	var onLoad = (function() {
		$('.panel-form').animateCss('bounceInUp');
		@if (Auth::getUser()->level_user != 1)
			getjabatan({{ $pegawai->tipe_profesi_id }});
		@endif
	})();

	$('.btn-cancel').click(function(e){
		e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

	function loadFilePhoto(event) {
		var image = URL.createObjectURL(event.target.files[0]);
		$('#preview-photo').fadeOut(function(){
			$(this).attr('src', image).fadeIn().css({
				'-webkit-animation' : 'showSlowlyElement 700ms',
				'animation'         : 'showSlowlyElement 700ms'
			});
		});
	};

	function disabledBtn() {
		var stMail = $('#statusMail').val();
		var stNip = $('#statusNip').val();
		var level = {{ Auth::getUser()->level_user }};
		if (stMail == 'Ready') {
			if (level != 1) {
				if (stNip == 'Ready') {
					$('.btn-submit').removeAttr('disabled');
				}else{
					$('.btn-submit').attr('disabled', true);
				}
			}else{
				$('.btn-submit').removeAttr('disabled');
			}
		}else{
			$('.btn-submit').attr('disabled', true);
		}
	}

	function cekMail() {
		var id = {{ Auth::getUser()->id }};
		var email = $('#inputEmail').val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (email != '') {
			if (atpos < 1 || dotpos < atpos+2 || dotpos+2 >= email.length) {
				$('#inputEmail').attr('class','form-control is-invalid');
				$('#icon_email').attr('class','iconStatus text-red');
				$('#icon_email').html('<i class="fas fa-exclamation-triangle"></i>');
				$('.errorMail').html('Email Tidak Sesuai');
				$('#statusMail').val('Exist');
				disabledBtn();
			}else{
				$.post("{!! route('getEmail') !!}", {email:email,id:id}).done(function(data){
					if (data.status == 'success') {
						$('#inputEmail').attr('class','form-control is-invalid');
						$('#icon_email').attr('class','iconStatus text-red');
						$('#icon_email').html('<i class="fas fa-exclamation-triangle"></i>');
						$('.errorMail').html('Email Telah Terdaftar');
						$('#statusMail').val('Exist');
						disabledBtn();
					}else{
						$('#inputEmail').attr('class','form-control is-valid');
						$('#icon_email').attr('class','iconStatus text-green');
						$('#icon_email').html('<i class="fas fa-check-circle"></i>');
						$('.errorMail').html('');
						$('#statusMail').val('Ready');
						disabledBtn();
					}
				});
			}
		}else{
			$('#inputEmail').attr('class','form-control');
			$('#icon_email').attr('class','iconStatus');
			$('#icon_email').html('');
			$('.errorMail').html('');
			$('#statusMail').val('Exist');
			disabledBtn();
		}
	}

	function cekNip() {
		var id = {{ Auth::getUser()->id }};
		var nip = $('#inputNip').val();
		if (nip != '') {
			$.post("{!! route('getNip') !!}", {nip:nip, id:id}).done(function(data){
				if (data.status == 'success') {
					$('#inputNip').attr('class','form-control is-invalid');
					$('#icon_nip').attr('class','iconStatus text-red');
					$('#icon_nip').html('<i class="fas fa-exclamation-triangle"></i>');
					$('.errorNip').html('Kode Telah Terdaftar');
					$('#statusNip').val('Exist');
					disabledBtn();
				}else{
					$('#inputNip').attr('class','form-control is-valid');
					$('#icon_nip').attr('class','iconStatus text-green');
					$('#icon_nip').html('<i class="fas fa-check-circle"></i>');
					$('.errorNip').html('');
					$('#statusNip').val('Ready');
					disabledBtn();
				}
			});
		}else{
			$('#inputNip').attr('class','form-control');
			$('#icon_nip').attr('class','iconStatus');
			$('#icon_nip').html('');
			$('.errorNip').html('');
			$('#statusNip').val('Exist');
			disabledBtn();
		}
	}

	$('#inputProfesi').change(function(){
		var id= $('#inputProfesi').val();
		getjabatan(id);
	});

	function getjabatan(idProfesi) {
		@if(Auth::getUser()->level_user != 1)
			var idjab = '{{ $pegawai->jabatan_id }}';
		@else
			var idjab = '';
		@endif
		$.post("{!! route('getjabatan') !!}", {id:idProfesi}).done(function(data){
			var listData = '<option value="" disabled selected> .:: Pilih Jabatan ::. </option>';
			if(data.length > 0){
				$.each(data, function(k,v){
					if (idjab == v.id_master) {
						var jabSelect = 'selected';
					}else{
						var jabSelect = '';
					}
					listData += '<option value="'+v.id_master+'" '+jabSelect+'>'+v.nama+'</option>';
				});
			}
			$('#inputJabatan').html(listData);
			$('#inputJabatan').trigger('chosen:updated');
		});
	}

	function formPns(status) {
		if (status == 'PNS') {
			$('#labelIdentitas').html('No NIP');
			$('#inputNip').attr('placeholder','No NIP');
			$('#panelPenilai').show();
			$('#panelKarpeg').show();
			$('#panelJabFus').show();
		}else{
			$('#labelIdentitas').html('No NIK');
			$('#inputNip').attr('placeholder','No NIK');
			$('#panelPenilai').hide();
			$('#panelKarpeg').hide();
			$('#panelJabFus').hide();
		}
	}

	$('#inputTglLahir').datetimepicker({ weekStart: 2, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 2, minView: 2, forceParse: 0, });
	$('#inputProfesi').chosen();
	$('#inputSaKer').chosen();
	$('#inputGolongan').chosen();
	$('#inputGolTmt').datetimepicker({ weekStart: 2, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 2, minView: 2, forceParse: 0, });
	$('#inputJabatan').chosen();
	$('#inputJabTmt').datetimepicker({ weekStart: 2, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 2, minView: 2, forceParse: 0, });

	$('.btn-submit').click(function(e){
		e.preventDefault();
		$('.btn-submit').html('Please wait...').attr('disabled', true);
		var data  = new FormData($('.form-save')[0]);
		$.ajax({
			url: "{{ route('saveProfile') }}",
			type: 'POST',
			data: data,
			async: true,
			cache: false,
			contentType: false,
			processData: false
		}).done(function(data){
			$('.form-save').validate(data, 'has-error');
			if(data.status == 'success'){
				swal("Success !", data.message, "success");
				$('.other-page').fadeOut(function(){
					$('.other-page').empty();
					$('.main-layer').fadeIn();
				});
				location.reload();
			} else if(data.status == 'error') {
				$('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
				swal('Whoops !', data.message, 'warning');
			} else {
				var n = 0;
				for(key in data){
					if (n == 0) {var dt0 = key;}
					n++;
				}
				$('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
				swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
			}
		}).fail(function() {
			swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
			$('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
		});
	});
</script>