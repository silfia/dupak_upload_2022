@extends('component.layout')

@section('extended_css')
	<style type="text/css">
		.iconEye{
			position:absolute;
			right:25px;
			top:5px;
			font-size:18px;
		}
		.messageError{
			right: 55px !important;
		}
	</style>
@stop

@section('content')
	<div class="container-fluid dashboard-content">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-header">
					<h2 class="pageheader-title">{{ $data['title'] }}</h2>
					<div class="page-breadcrumb">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
								<li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
				<form class="form-save">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3">
							<div class="card">
								<div class="card-body">
									<div class="form-group row">
										<label for="inputPassLama" class="col-4 col-lg-3 col-form-label">Password Lama</label>
										<div class="col-8 col-lg-9">
											<input id="inputPassLama" type="password" name="password_lama" value="" placeholder="Password Lama" class="form-control">
											<div class='iconEye'>
												<a href="javascript:void(0)" onclick="showPassword('inputPassLama','eyePassLama')" id='eyePassLama'><i class="far fa-eye-slash"></i></a>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassBaru" class="col-4 col-lg-3 col-form-label">Password Baru</label>
										<div class="col-8 col-lg-9">
											<input id="inputPassBaru" type="password" name="password_baru" onkeyup="cekPass()" placeholder="Password Baru" class="form-control">
											<p class='messageError errorPass'></p>
											<input type='hidden' name='statusPass' value='Failed' id='statusPass' class='form-control'>
											<div class='iconEye'>
												<a href="javascript:void(0)" onclick="showPassword('inputPassBaru','eyePassBaru')" id='eyePassBaru'><i class="far fa-eye-slash"></i></a>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassKonfir" class="col-4 col-lg-3 col-form-label">Konfirmasi Password</label>
										<div class="col-8 col-lg-9">
											<input id="inputPassKonfir" type="password" name="konfirmasi_baru" onkeyup="konfPass()" placeholder="Password Lama" class="form-control">
											<p class='messageError errorPassKnf'></p>
											<input type='hidden' name='statusPassKnf' value='Failed' id='statusPassKnf' class='form-control'>
											<div class='iconEye'>
												<a href="javascript:void(0)" onclick="showPassword('inputPassKonfir','eyePassKonfir')" id='eyePassKonfir'><i class="far fa-eye-slash"></i></a>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-10px">
										<button type="submit" class="btn btn-sm btn-space btn-primary btn-submit" style="float: right;" disabled>Simpan <span class="fas fa-save"></span></button>
									</div>

								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('extended_js')
	<script type="text/javascript">
		function showPassword(id,ico) {
			var tag = document.getElementById(id).getAttribute("type");
			if (tag == 'password') {
				$('#'+id).attr('type','text');
				$('#'+ico).html('<i class="fas fa-eye"></i>');
			}else{
				$('#'+id).attr('type','password');
				$('#'+ico).html('<i class="far fa-eye-slash"></i>');
			}
		}

		function disabledBtn() {
			var passLama = $('#inputPassLama').val();
			var stPassBr = $('#statusPass').val();
			var stPassKnf = $('#statusPassKnf').val();
			if (passLama != '' && stPassBr == 'Ready' && stPassKnf == 'Ready') {
				$('.btn-submit').removeAttr('disabled');
			}else{
				$('.btn-submit').attr('disabled', true);
			}
		}

		function cekPass() {
			var password = $('#inputPassBaru').val();
			if (password.length >= 6) {
				$('#inputPassBaru').attr('class','form-control is-valid');
				$('.errorPass').html('');
				$('#statusPass').val('Ready');
			}else if(password.length == 0) {
				$('#inputPassBaru').attr('class','form-control');
				$('.errorPass').html('');
				$('#statusPass').val('Failed');
			}else{
				$('#inputPassBaru').attr('class','form-control is-invalid');
				$('.errorPass').html('Gunakan Sedikitnya 6 Karakter');
				$('#statusPass').val('Failed');
			}
			disabledBtn();
		}

		function konfPass() {
			var password = $('#inputPassBaru').val();
			var passwordKonf = $('#inputPassKonfir').val();

			if (passwordKonf == '') {
				$('#inputPassKonfir').attr('class','form-control');
				$('.errorPassKnf').html('');
				$('#statusPassKnf').val('Failed');
			}else if(password == passwordKonf) {
				$('#inputPassKonfir').attr('class','form-control is-valid');
				$('.errorPassKnf').html('');
				$('#statusPassKnf').val('Ready');
			}else{
				$('#inputPassKonfir').attr('class','form-control is-invalid');
				$('.errorPassKnf').html('Konfirmasi Password Tidak Cocok');
				$('#statusPassKnf').val('Failed');
			}
			disabledBtn();
		}

		$('.btn-submit').click(function(e){
			e.preventDefault();
			$('.btn-submit').html('Please wait...').attr('disabled', true);
			var data  = new FormData($('.form-save')[0]);
			$.ajax({
				url: "{{ route('saveChangePassword') }}",
				type: 'POST',
				data: data,
				async: true,
				cache: false,
				contentType: false,
				processData: false
			}).done(function(data){
				$('.form-save').validate(data, 'has-error');
				if(data.status == 'success'){
					swal("Success !", data.message, "success");
					location.reload();
				} else if(data.status == 'error') {
					$('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
					swal('Whoops !', data.message, 'warning');
				} else {
					var n = 0;
					for(key in data){
						if (n == 0) {var dt0 = key;}
						n++;
					}
					$('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
					swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
				}
			}).fail(function() {
				swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
				$('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
			});
		});
	</script>
@stop