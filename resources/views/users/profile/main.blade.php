@extends('component.layout')

@section('extended_css')
@stop

@section('content')
	<div class="container-fluid dashboard-content">
		<!--  ..::: pageheader :::.. -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-header">
					<h2 class="pageheader-title">{{ $data['title'] }}</h2>
					<div class="page-breadcrumb">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
								<li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!--  ..::: end pageheader :::.. -->

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="loading" align="center" style="display: none;">
				<img src="{!! url('assets/images/loading.gif') !!}" width="60%">
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Auth::getUser()->level_user == 1) offset-lg-3 offset-md-3 @endif">
						<div class="card">
							<h5 class="card-header">Data @if(Auth::getUser()->level_user == 1) Admin @else Pegawai @endif</h5>
							<div class="card-body">
								<div class="form-group row">
									<label class="col-4 col-lg-3 col-form-label">Email</label>
									<div class="col-8 col-lg-9">
										<input type="text" value="{{ Auth::getUser()->email }}" class="form-control backWhite" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-4 col-lg-3 col-form-label">Nama</label>
									<div class="col-8 col-lg-9">
										<input type="text" value="{{ Auth::getUser()->name }}" class="form-control backWhite" readonly>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
									<div class="col-8 col-lg-9 row">
										<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
											<label class="custom-control custom-radio custom-control-inline mb-0">
												<input type="radio" class="custom-control-input" @if(Auth::getUser()->gender == 'L') checked @endif disabled><span class="custom-control-label">Laki-Laki</span>
											</label>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
											<label class="custom-control custom-radio custom-control-inline mb-0">
												<input type="radio"class="custom-control-input" @if(Auth::getUser()->gender == 'P') checked @endif disabled><span class="custom-control-label">Perempuan</span>
											</label>
										</div>
									</div>
								</div>
								@if(Auth::getUser()->level_user == 1)
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Alamat</label>
										<div class="col-8 col-lg-9">
											<textarea rows="2" class="form-control backWhite" readonly>{{ Auth::getUser()->address }}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Telepon</label>
										<div class="col-8 col-lg-9">
											<input type="text" value="{{ Auth::getUser()->phone }}" class="form-control backWhite" readonly>
										</div>
									</div>
								@endif
								@if(Auth::getUser()->level_user != 1)
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">TTL</label>
										<div class="col-8 col-lg-9 row pr-0">
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input type="text" value="{{ $pegawai->tempat_lahir }}" class="form-control backWhite" readonly>
											</div>
											<div class="input-group input-group-sm col-lg-5 col-md-5 col-sm-12 col-xs-12 pr-0">
												<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
												<input type="text" value="{{ date('d-m-Y', strtotime($pegawai->tanggal_lahir)) }}" class="form-control backWhite" readonly>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Profesi</label>
										<div class="col-8 col-lg-9">
											<input type="text" @if($pegawai->typeProfesi != '') value="{{ $pegawai->typeProfesi->nama }}" @else value="" @endif  class="form-control backWhite" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
										<div class="col-8 col-lg-9">
											<input type="text" @if($pegawai->satuanKerja != '') value="{{ $pegawai->satuanKerja->nama }}" @else value="" @endif class="form-control backWhite" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Status Pegawai</label>
										<div class="col-8 col-lg-9">
											<input type="text" value="{{ $pegawai->status_pegawai }}" class="form-control backWhite" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">@if($pegawai->status_pegawai == 'Non-PNS') No NIK @else No NIP @endif</label>
										<div class="col-8 col-lg-9">
											<input type="text" value="{{ $pegawai->no_nip }}" class="form-control backWhite" readonly>
										</div>
									</div>
									@if ($pegawai->status_pegawai == 'PNS')
										<div class="form-group row">
											<label class="col-4 col-lg-3 col-form-label">No Karpeng</label>
											<div class="col-8 col-lg-9">
												<input type="text" value="{{ $pegawai->no_karpeng }}" class="form-control backWhite" readonly>
											</div>
										</div>
									@endif
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Pendidikan Terakhir<br>Sesuai SK</label>
										<div class="col-8 col-lg-9">
											<input type="text" value="{{ $pegawai->pendidikan_terakhir }}" class="form-control backWhite" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-4 col-lg-3 col-form-label">Tahun Lulus</label>
										<div class="col-8 col-lg-9">
											<input type="text" value="{{ $pegawai->pendidikan_tahun }}" class="form-control backWhite" readonly>
										</div>
									</div>
								@endif
							</div>
						</div>
					</div>
					@if(Auth::getUser()->level_user != 1)
						@if($pegawai->status_pegawai == 'PNS')
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="card">
									<h5 class="card-header">Data Jabatan & Fungsi</h5>
									<div class="card-body">
										<div class="form-group row">
											<label class="col-4 col-lg-3 col-form-label">Golongan</label>
											<div class="col-8 col-lg-9">
												<input type="text" @if($pegawai->golongan != '') value="{{ $pegawai->golongan->nama }}" @else value="" @endif class="form-control backWhite" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-4 col-lg-3 col-form-label">Golongan TMT</label>
											<div class="input-group input-group-sm col-8 col-lg-9">
												<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
												<input type="text" value="@if($pegawai->golongan_tmt != '') {{ date('d-m-Y', strtotime($pegawai->golongan_tmt)) }} @endif" class="form-control backWhite" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-4 col-lg-3 col-form-label">Jabatan</label>
											<div class="col-8 col-lg-9">
												<input type="text" @if(!empty($pegawai->jabatan)) value="{{ $pegawai->jabatan->nama }} @else value="" @endif" class="form-control backWhite" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
											<div class="input-group input-group-sm col-8 col-lg-9">
												<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
												<input type="text" value="@if($pegawai->jabatan_tmt != '') {{ date('d-m-Y', strtotime($pegawai->jabatan_tmt)) }} @endif" class="form-control backWhite" readonly>
											</div>
										</div>


									</div>
								</div>
							</div>
						@endif
					@endif
					<div class="col-lg-12">
						<button class="btn btn-sm btn-warning btn-update"><i class="fa fa-pencil-alt"></i>Edit Profile</button>
					</div>
				</div>
			</div>
			<div class="col-12 other-page"></div>
			<div class="col-12 modal-dialog"></div>
		</div>
	</div>
@stop

@section('extended_js')
	<script type="text/javascript">
		$('.btn-update').click(function(){
			$('.loading').show();
			$('.main-layer').hide();
			$.post("{!! route('formProfile') !!}").done(function(data){
				if(data.status == 'success'){
					$('.loading').hide();
					$('.other-page').html(data.content).fadeIn();
				} else {
					$('.main-layer').show();
				}
			});
		});
	</script>
@stop
