<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <h5 class="card-header bg-info"><i class="fas fa-search mr-2"></i> Detail Data</h5>
  <div class="card-body">
    <div class="form-row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Kode</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->kode }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Nama</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->nama }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Type Satuan</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->typeSaKer->nama }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Pimpinan</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="@if($satuanKerja->pimpinan_id != 0){{ $satuanKerja->pimpinan->nama }}@else-@endif" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Telepon</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->telepon }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Fax</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->fax }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Email</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->email }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Website</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->website }}" class="form-control backWhite" readonly>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group row m-b-15px">
          <label class="col-4 col-lg-3 col-form-label">Alamat</label>
          <div class="col-8 col-lg-9">
            <textarea rows="2" class="form-control backWhite" readonly>{{ $satuanKerja->alamat }}</textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Layanan</label>
          <div class="col-8 col-lg-9 row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline mb-0">
                <input type="radio" name="jenis_layanan" value="Rawat Inap" class="custom-control-input" @if($satuanKerja->jenis_layanan == 'Rawat Inap') checked @endif disabled><span class="custom-control-label">Rawat Inap</span>
              </label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <label class="custom-control custom-radio custom-control-inline mb-0">
                <input type="radio" name="jenis_layanan" value="Rawat Jalan" class="custom-control-input" @if($satuanKerja->jenis_layanan == 'Rawat Jalan') checked @endif disabled><span class="custom-control-label">Rawat Jalan</span>
              </label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Kelurahan</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->kelurahan }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Kecamatan</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->kecamatan }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Kabupaten</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->kabupaten }}" class="form-control backWhite" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Provinsi</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $satuanKerja->provinsi }}" class="form-control backWhite" readonly>
          </div>
        </div>
      </div>
    </div>
    <div class="row pt-2 pt-sm-2 mt-1">
      <div class="col-sm-12 pl-0">
        <p class="text-right">
          <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
        </p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });
</script>
