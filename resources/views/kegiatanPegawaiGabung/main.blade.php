@extends('component.layout')

@section('extended_css')
	<style media="screen">
		.panelIsiKegiatan {
			padding: 0px !important;
			position: relative !important;
		}

		.btnIsiKegiatan {
			height: 100% !important;
			width: 100% !important;
			position: absolute !important;
			top: 0 !important;
			justify-content: center !important;
			display: flex !important;
			align-items: center !important;
		}
		.txt-middle {
			vertical-align: middle !important;
		}
		.table-scroll {
			position: relative;
			width:100%;
			z-index: 1;
			margin: auto;
			overflow: auto;
			height: 450px;
		}
    .table-scroll table {
      width: 100%;
      min-width: 1280px;
      margin: auto;
      border-collapse: separate;
      border-spacing: 0;
      table-layout: fixed;
    }
    .table-wrap {
      position: relative;
    }
    .table-scroll th,
    .table-scroll td {
      padding: 5px 10px;
      border: 1px solid #e6e6f2;
      vertical-align: top;
      background: #fff;
    }
    .table-scroll table tbody tr:nth-child(even) td, .table-scroll table tbody tr:nth-child(even) th {
      background-color: #f2f2f8 !important;
    }
    .table-scroll thead th, .table-scroll thead th:nth-child(2) {
      background: #9cb8e2;
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    }
    .table-scroll thead tr:nth-child(2) th {
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 31px;
      left: inherit;
    }
    /* safari and ios need the tfoot itself to be position:sticky also */
    .table-scroll tfoot,
    .table-scroll tfoot th,
    .table-scroll tfoot td {
      position: -webkit-sticky;
      position: sticky;
      bottom: 0;
      background: #9cb8e2;
      color: #fff;
      z-index:4;
    }

    .table-scroll th:first-child, .table-scroll th:nth-child(2) {
      position: -webkit-sticky;
      position: sticky;
      left: 0;
      z-index: 4;
    }
    .level2, .level3, .level4, .level5, .level6 {
      position: -webkit-sticky !important;
      position: sticky !important;
      z-index: 4 !important;
    }
    .level2{ left: 90px; }
    .level3{ left: 130px; }
    .level4{ left: 170px; }
    .level5{ left: 210px; }
    .level6{ left: 250px; }
    .table-scroll th:nth-child(2){
      left: 50px;
    }
    .table-scroll thead th{
      z-index: 4;
    }
    .table-scroll thead th:first-child, .table-scroll thead th:nth-child(2),
    .table-scroll tfoot th:first-child, .table-scroll tfoot th:nth-child(2) {
      z-index: 5;
    }
    .table-scroll thead tr:nth-child(2) th:first-child, .table-scroll thead tr:nth-child(2) th:nth-child(2){
      z-index: 1;
    }
  </style>
@stop

@section('content')
	<div class="container-fluid dashboard-content">
		<!--  ..::: pageheader :::.. -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-header">
					<h2 class="pageheader-title">{{ $data['title'] }}</h2>
					<div class="page-breadcrumb">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!--  ..::: end pageheader :::.. -->

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="loading" align="center" style="display: none;">
				<img src="{!! url('assets/images/loading.gif') !!}" width="60%">
			</div>
		</div>

		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 main-layer">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group row">
								<label for="bln_kegiatan" class="col-4 col-lg-3 col-form-label p-l-35px">Bulan</label>
								<div class="input-group input-group-sm col-8 col-lg-9">
									<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
									<input id="bln_kegiatan" type="text" name="Bulan" value="{{ date('m-Y') }}" data-date-format="mm-yyyy" placeholder="Bulan Kegiatan" class="form-control">
								</div>
							</div>
							<div class="col-lg-8 col-sm-6">
								<label class="col-12 col-lg-12 col-form-label" style="text-align: right;"><strong>Jumlah Nilai AK : </strong><span class="hasilAk"></span></label>
							</div>
							<div class="tglAr" style="display:none"></div>
							<div class="idPaksAr" style="display:none"></div>
							<div class="col-12 m-t-15px">
								<div id="table-scroll" class="table-scroll"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 other-page"></div>
			<div class="col-12 modal-dialog m-0"></div>
			<div class="col-12 second-page"></div>
		</div>
	</div>
@stop

@section('extended_js')
	<script type="text/javascript">
		var arrayIds = []; // deklarasi array ID PAK
		var jmlAkTotal = 0;

		$('#bln_kegiatan').datetimepicker({ weekStart: 2, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 4, minView: 3, forceParse: 0, });

		getDataButir();
		$('#bln_kegiatan').change(function(){ getDataButir(); });


		function getDataButir() {
			var blnSelect = $('#bln_kegiatan').val();
			var tag = '';
			var tanggal = '';
			var idPak = '';
			$('#table-scroll').html('<div align="center"><img src="{!! url('assets/images/loading.gif') !!}" width="60%"></div>');
			console.log('Bulan : '+blnSelect);
			$.post("{!! route('loadKegiatanHarianGabung') !!}",{blnSelect:blnSelect}).done(function(result){
				var wIsi = 50 * result.row.jumlahTanggal;
				tag += '<table id="main-table" class="main-table">';
				tag += '<thead>';
				tag += '<tr>';
				tag += '<th rowspan="2" width="50px" class="text-center txt-middle">No</th>';
				tag += '<th rowspan="2" colspan="15" class="txt-middle" width="600px">Nama</th>';
				tag += '<th colspan="'+result.row.jumlahTanggal+'" width="'+wIsi+'px" class="text-center">Jumlah Presentasi Kerja Harian</th>';
				tag += '<th rowspan="2" width="70px" class="text-center">Jumlah</th>';
				tag += '<th rowspan="2" width="70px" class="text-center">Jumlah AK</th>';
				tag += '</tr>';
				tag += '<tr>';
				for (var TitleDate = 1; TitleDate <= result.row.jumlahTanggal; TitleDate++) {
					tag += '<th class="text-center" width="50px">'+TitleDate+'</th>';
					if (tanggal == '') { tanggal = TitleDate; }
					else{ tanggal = tanggal+","+TitleDate; }
				}
				$('.tglAr').html(tanggal);
				tag += '</tr>';
				tag += '</thead>';
				tag += '<tbody>';
				if (result.status == 'success') {
					$.each(result.row.butir, function(k,uns){
						tag += '<tr>';
						tag += '<th width="50px" style="background-color: #becee6 !important;"></th>';
						tag += '<th colspan="15" width="600px" style="background-color: #becee6 !important;">'+uns.nama+'</th>';
						tag += '<td colspan="'+result.row.jumlahTanggal+'" width="'+wIsi+'px" style="background-color: #becee6 !important;"></td>';
						tag += '<td width="70px" style="background-color: #becee6 !important;"></td>';
						tag += '<td width="70px" style="background-color: #becee6 !important;"></td>';
						tag += '</tr>';
						if (uns.row.length > 0) {
							var tempNo1 = 0;
							$.each(uns.row, function(k,v1){
								tempNo1++;
								if (v1.statusRow == 'Parent') {
									tag += tagRow(v1, KonDecRomawi(tempNo1), result.row.jumlahTanggal, wIsi, 'parent');
									if (v1.row.length > 0) {
										var tempNo2 = 0; // Hurug Awal
										var tempNo2n = 0; // Hurug Akhir
										var No2 = '';
										var No2n = '';
										$.each(v1.row, function(k,v2){
											if (tempNo2n == 26) {
												tempNo2++;
												No2 = String.fromCharCode(64 + tempNo2);
											}
											tempNo2n++;
											No2n = No2+''+String.fromCharCode(64 + tempNo2n);
											if (v2.statusRow == 'Parent') {
												tag += tagRow(v2, No2n, result.row.jumlahTanggal, wIsi, 'parent');
												if (v2.row.length > 0) {
													var tempNo3 = 0;
													$.each(v2.row, function(k,v3){
														tempNo3++;
														if (v3.statusRow == 'Parent') {
															tag += tagRow(v3, tempNo3, result.row.jumlahTanggal, wIsi, 'parent');
															if (v3.row.length > 0) {
																var tempNo4 = 0; // Hurug Awal
																var tempNo4n = 0; // Hurug Akhir
																var No4 = '';
																var No4n = '';
																$.each(v3.row, function(k,v4){
																	if (tempNo4n == 26) {
																		tempNo4++;
																		No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
																	}
																	tempNo4n++;
																	No4n = No4+''+String.fromCharCode(64 + tempNo4n).toLowerCase();
																	if (v4.statusRow == 'Parent') {
																		tag += tagRow(v4, No4n, result.row.jumlahTanggal, wIsi, 'parent');
																		if (v4.row.length > 0) {
																			var No5 = '<i class="fas fa-angle-double-right"></i>';
																			$.each(v4.row, function(k,v5){
																				if (v5.statusRow == 'Parent') {
																					tag += tagRow(v5, No5, result.row.jumlahTanggal, wIsi, 'parent');
																					if (v5.row.length > 0) {
																						var No6 = '<i class="fas fa-terminal"></i>';
																						$.each(v5.row, function(k,v6){
																							if(v6.statusRow == 'Parent'){
																								tag += tagRow(v6, No6, result.row.jumlahTanggal, wIsi, 'parent');
																							}else{
																								tag += tagRow(v6, No6, result.row.jumlahTanggal, wIsi, 'child');
																								if (idPak == '') { idPak = v6.id_pak_master; }
																								else{ idPak = idPak+","+v6.id_pak_master; }
																							}
																						});
																					}
																				}else{
																					tag += tagRow(v5, No5, result.row.jumlahTanggal, wIsi, 'child');
																					if (idPak == '') { idPak = v5.id_pak_master; }
																					else{ idPak = idPak+","+v5.id_pak_master; }
																				}
																			});
																		}
																	}else{
																		tag += tagRow(v4, No4n, result.row.jumlahTanggal, wIsi, 'child');
																		if (idPak == '') { idPak = v4.id_pak_master; }
																		else{ idPak = idPak+","+v4.id_pak_master; }
																	}
																});
															}
														}else{
															tag += tagRow(v3, tempNo3, result.row.jumlahTanggal, wIsi, 'child');
															if (idPak == '') { idPak = v3.id_pak_master; }
															else{ idPak = idPak+","+v3.id_pak_master; }
														}
													});
												}
											}else{
												tag += tagRow(v2, No2n, result.row.jumlahTanggal, wIsi, 'child');
												if (idPak == '') { idPak = v2.id_pak_master; }
												else{ idPak = idPak+","+v2.id_pak_master; }
											}
										});
									}
								}else{
									tag += tagRow(v1, KonDecRomawi(tempNo1), result.row.jumlahTanggal, wIsi, 'child');
									if (idPak == '') { idPak = v1.id_pak_master; }
									else{ idPak = idPak+","+v1.id_pak_master; }
								}
							});
						}else{
							var colTotal = result.row.jumlahTanggal + 4;
							tag += '<tr><th colspan="'+colTotal+'" class="text-left">.:: Data Butir Unsur Tidak Ditemukan ::.</th></tr>';
						}
					});
				}else{
					var colTotal = result.row.jumlahTanggal + 4;
					tag += '<tr><th colspan="'+colTotal+'" class="text-center">.:: Data Tidak Ditemukan ::.</th></tr>';
				}
				tag += '</tbody>';
				tag += '</table>';
				$('#table-scroll').empty();
				$('#table-scroll').html(tag);
				$('.idPaksAr').html(idPak);

				LoadGrid();
			}).fail(function() {
				getDataButir();
			});
		}

		function LoadGrid() {
			jmlAkTotal = 0;
			var arrayTanggal = [];
			arrayTanggal = $('.tglAr').text().split(',');
			var totalArrayTanggal = arrayTanggal.length;
			var arrayIdPak = [];
			arrayIdPak = $('.idPaksAr').text().split(',');
			var totalArrayIdPak = arrayIdPak.length;
			console.log('tpak',totalArrayIdPak)
			var tempIdPak = 0;
			var tempTanggal = 0;
			$('.hasilAk').html(jmlAkTotal);
			function valGrid(tempIdPak) {
				if (tempIdPak >= totalArrayIdPak) {
					console.log('berhenti');
					$('.hasilAk').html(jmlAkTotal);
					return ;
				}
				var blnSelect = $('#bln_kegiatan').val();
				return $.ajax({
					url: "{{ route('getNilaiHarianGabung') }}?idPakMaster="+arrayIdPak[tempIdPak]+"&bulan="+blnSelect, success: function(result){
						var dte = new Date();
						var hari = dte.getDate();
						var bulan = dte.getMonth() + 1;
						var tahun = dte.getFullYear();
						var hasilIcon = '<i class="fas fa-pencil-alt"></i>';
						// if (hari >= 1 && hari <= 16) {
						// 	var tglA = 1;
						// 	var tglN = 16;
						// }else if (hari > 16 && hari <= 31) {
						// 	var tglA = 16;
						// 	var tglN = 31;
						// }
						if (result.status == 'success') {
							var jmidjab = '';
							$.each(result.row, function(k,v){
								// if ((parseInt(v.tgl) >= parseInt(tglA) && parseInt(v.tgl) <= parseInt(tglN)) && parseInt(v.bln) == parseInt(bulan) && parseInt(v.thn) == parseInt(tahun)) {
								if ((parseInt(v.tgl) <= hari) && parseInt(v.bln) == parseInt(bulan) && parseInt(v.thn) == parseInt(tahun)) {
									if (v.hasil != '-') {
										var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan('+v.idPenilaian+')">'+v.hasil+'</a>';
									}else{
										var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="tambahKegiatan('+v.idjabatan+','+v.idpak+','+v.tgl+','+v.bulanload+')">'+hasilIcon+'</a>';
									}
								}else{
									if (v.hasil != '-') {
										var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan('+v.idPenilaian+')">'+v.hasil+'</a>';
									}else{
										var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="tambahKegiatan('+v.idjabatan+','+v.idpak+','+v.tgl+','+v.bulanload+')">'+hasilIcon+'</a>';
										// var isi = '<span class="btnIsiKegiatan">'+v.hasil+'</span>';
									}
								}
								$('#'+v.idjabatan+'-'+v.idpak+'_'+v.tgl).html(isi);
								jmidjab = v.idjabatan;
							});
							$('#'+jmidjab+'-'+result.dtJumlah.idpak+'_jml').html(result.dtJumlah.jml);
							$('#'+jmidjab+'-'+result.dtJumlah.idpak+'_jmlAk').html(result.dtJumlah.jmlAk);
							
							var stPeg = "{{ $data['dtPegawai']->pegawai->status_pegawai }}";
							if (stPeg == 'PNS') {
								jmlAkTotal = jmlAkTotal + result.dtJumlah.jmlAk;
							}
							console.log(jmlAkTotal);
								// console.log('jmlAkTotal = '+jmlAkTotal+' + '+result.dtJumlah.jmlAk+'');

						}else{
							return valGrid(tempIdPak);
						}
					},
					error:function() {
						return valGrid(tempIdPak);
					}
				}).then(function () {
					return valGrid(tempIdPak + 1);
				});
			}
			valGrid(tempIdPak);
		}

		function tambahKegiatan(idjabatan, idPakMaster, tgl, bulanload) {
			$.post("{!! route('formJumlahDetailKegiatan') !!}", {idMaster:idjabatan,idPakMaster:idPakMaster,tgl:tgl,bulanload:bulanload}).done(function(data){
				if (data.status == 'success') {
					$('.modal-dialog').html(data.content);
				}else{
					swal("MAAF !","Terjadi Kesalahan !!", "warning");
				}
			});
		}

		function detailKegiatan(idPenilaian) {
			$('.loading').show();
			$('.main-layer').hide();
			$.post("{!! route('viewDetailKegiatan') !!}", {idPenilaian:idPenilaian}).done(function(data){
				if (data.status == 'success') {
					$('.loading').hide();
					$('.other-page').html(data.content).fadeIn();
				}else{
					swal("MAAF !","Terjadi Kesalahan !!", "warning");
				}
			});
		}

		function KonDecRomawi(angka) {
			var hasil = "";
			if (angka < 1 || angka > 5000) {
				hasil += "Batas Angka 1 s/d 5000";
			}else{
				while (angka >= 1000) {
					hasil += "M";
					angka = angka - 1000;
				}
			}
			if (angka >= 500) {
				if (angka > 500) {
					if (angka >= 900) {
						hasil += "CM";
						angka = angka - 900;
					} else {
						hasil += "D";
						angka = angka - 500;
					}
				}
			}
			while (angka >= 100) {
				if (angka >= 400) {
					hasil += "CD";
					angka = angka - 400;
				} else {
					angka = angka - 100;
				}
			}
			if (angka >= 50) {
				if (angka >= 90) {
					hasil += "XC";
					angka = angka - 90;
				} else {
					hasil += "L";
					angka = angka - 50;
				}
			}
			while (angka >= 10) {
				if (angka >= 40) {
					hasil += "XL";
					angka = angka - 40;
				} else {
					hasil += "X";
					angka = angka - 10;
				}
			}
			if (angka >= 5) {
				if (angka == 9) {
					hasil += "IX";
					angka = angka - 9;
				} else {
					hasil += "V";
					angka = angka - 5;
				}
			}
			while (angka >= 1) {
				if (angka == 4) {
					hasil += "IV";
					angka = angka - 4;
				} else {
					hasil += "I";
					angka = angka - 1;
				}
			}
			return hasil;
		}

		function tagRow(keyData, nomor, jmlTgl, nilaiWidth, status) {
			var tag = '';
			tag += '<tr>';
			if (status == 'parent') {
				var narasi = keyData.butir_kegiatan;
			}else{
				var narasi = keyData.butir_kegiatan+'<br>( Tiap '+keyData.jum_min+' '+keyData.satuan+' '+keyData.points+' ) <span style="float:right;">'+keyData.jabatan.nama+'</span>';
			}
			switch(keyData.level) {
				case 1:
					tag += '<th width="50px" class="text-center">'+nomor+'.</th>';
					tag += '<th colspan="15" width="600px">'+narasi+'</th>';
					break;
				case 2:
					tag += '<th width="50px"></th>';
					tag += '<th class="text-center" width="40px">'+nomor+'.</th>';
					tag += '<th class="level2" width="560px" colspan="14">'+narasi+'</th>';
					break;
				case 3:
					tag += '<th width="50px"></th>';
					tag += '<th width="40px"></th>';
					tag += '<th class="level2 text-center" width="40px">'+nomor+'.</th>';
					tag += '<th class="level3" width="520px" colspan="13">'+narasi+'</th>';
					break;
				case 4:
					tag += '<th width="50px"></th>';
					tag += '<th width="40px"></th>';
					tag += '<th class="level2" width="40px"></th>';
					tag += '<th class="level3 text-center" width="40px">'+nomor+'.</th>';
					tag += '<th class="level4" width="480px" colspan="12">'+narasi+'</th>';
					break;
				case 5:
					tag += '<th width="50px"></th>';
					tag += '<th  width="40px"></th>';
					tag += '<th class="level2" width="40px"></th>';
					tag += '<th class="level3" width="40px"></th>';
					tag += '<th class="level4 text-center" width="40px">'+nomor+'</th>';
					tag += '<th class="level5" width="440px" colspan="11">'+narasi+'</th>';
					break;
				case 6:
					tag += '<th width="50px"></th>';
					tag += '<th width="40px"></th>';
					tag += '<th class="level2" width="40px"></th>';
					tag += '<th class="level3" width="40px"></th>';
					tag += '<th class="level4" width="40px"></th>';
					tag += '<th class="level5 text-center" width="40px">'+nomor+'.</th>';
					tag += '<th class="level6" width="400px" colspan="10">'+narasi+'</th>';
					break;
			}
			if (status == 'parent') {
				tag += '<td colspan="'+jmlTgl+'" width="'+nilaiWidth+'px"></td>';
				tag += '<td width="70px"></td>';
				tag += '<td width="70px"></td>';
			}else{
				for (var a = 1; a <= jmlTgl; a++) {
					tag += '<td class="panelIsiKegiatan" id="'+keyData.jabatan_id+'-'+keyData.id_pak_master+'_'+a+'"></td>';
				}
				tag += '<td id="'+keyData.jabatan_id+'-'+keyData.id_pak_master+'_jml" align="center" width="70px"></td>';
				tag += '<td id="'+keyData.jabatan_id+'-'+keyData.id_pak_master+'_jmlAk" align="center" width="70px"></td>';
			}
			tag += '</tr>';

			return tag;
		}
	</script>
@stop