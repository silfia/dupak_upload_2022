@extends('component.layout')

@section('extended_css')
  <style media="screen">
    .panelIsiKegiatan {
      padding: 0px !important;
      position: relative !important;
    }

    .btnIsiKegiatan {
      height: 100% !important;
      width: 100% !important;
      position: absolute !important;
      top: 0 !important;
      justify-content: center !important;
      display: flex !important;
      align-items: center !important;
    }
    .spinner-secondary {
      border: 3px solid transparent !important;
      border-top: 3px solid #5969ff !important;
      border-left: 3px solid #5969ff !important;
      z-index: 1;
      position: absolute !important;
      top: 10px;
    }

    .txt-middle {
      vertical-align: middle !important;
    }

    .table-scroll {
      position: relative;
      width:100%;
      z-index: 1;
      margin: auto;
      overflow: auto;
      height: 450px;
    }
    .table-scroll table {
      width: 100%;
      min-width: 1280px;
      margin: auto;
      border-collapse: separate;
      border-spacing: 0;
      table-layout: fixed;
    }
    .table-wrap {
      position: relative;
    }
    .table-scroll th,
    .table-scroll td {
      padding: 5px 10px;
      border: 1px solid #e6e6f2;
      vertical-align: top;
      background: #fff;
    }
    .table-scroll table tbody tr:nth-child(even) td, .table-scroll table tbody tr:nth-child(even) th {
      background-color: #f2f2f8 !important;
    }
    .table-scroll thead th, .table-scroll thead th:nth-child(2) {
      background: #9cb8e2;
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    }
    .table-scroll thead tr:nth-child(2) th {
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 33px;
      left: inherit;
    }
    /* safari and ios need the tfoot itself to be position:sticky also */
    .table-scroll tfoot,
    .table-scroll tfoot th,
    .table-scroll tfoot td {
      position: -webkit-sticky;
      position: sticky;
      bottom: 0;
      background: #9cb8e2;
      color: #fff;
      z-index:4;
    }

    .table-scroll th:first-child, .table-scroll th:nth-child(2) {
      position: -webkit-sticky;
      position: sticky;
      left: 0;
      z-index: 4;
    }
    .level2, .level3, .level4, .level5, .level6 {
      position: -webkit-sticky !important;
      position: sticky !important;
      z-index: 4 !important;
    }
    .level2{ left: 90px; }
    .level3{ left: 130px; }
    .level4{ left: 170px; }
    .level5{ left: 210px; }
    .level6{ left: 250px; }
    .table-scroll th:nth-child(2){
      left: 50px;
    }
    .table-scroll thead th{
      z-index: 4;
    }
    .table-scroll thead th:first-child, .table-scroll thead th:nth-child(2),
    .table-scroll tfoot th:first-child, .table-scroll tfoot th:nth-child(2) {
      z-index: 5;
    }
    .table-scroll thead tr:nth-child(2) th:first-child, .table-scroll thead tr:nth-child(2) th:nth-child(2){
      z-index: 1;
    }
  </style>
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card mb-3">
          <div class="card-body pt-2 pb-2 pl-5">
            <div class="row">
              <div class="col-6 col-lg-6">
                <div class="form-group row">
                  <label for="typesProfesi" class="col-4 col-lg-3 col-form-label">Pegawai</label>
                  <div class="col-8 col-lg-9">
                    <select class="form-control" name="id_pegawai" id="idPegawai">
                      <option value="" disabled selected> .:: Pilih Pegawai ::. </option>
                      @foreach ($data['pegawai'] as $pegawai)
                        <option value="{{ $pegawai->id_pegawai }}">{{ $pegawai->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-4 col-lg-4">
                <div class="form-group row">
                  <label for="bln" class="col-4 col-lg-3 col-form-label">Bulan</label>
                  <div class="input-group input-group-sm col-8 col-lg-9">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                    <input id="bln" type="text" name="Bulan" value="{{ date('m-Y') }}" data-date-format="mm-yyyy" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-2 col-lg-2">
                <div class="form-group row">
                  <button type="button" class="btn btn-xs btn-primary btn-search"><span class="fas fa-search"></span> Cari</button>
                  @if (Auth::getUser()->is_leader == 'Y')
                    <button type="button" class="btn btn-xs btn-success btn-checked ml-1" disabled><span class="fas fa-check"></span> Setujui</button>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12 col-lg-12">
                <div class="tgl" style="display:none"></div>
                <div class="idPaks" style="display:none"></div>
                <input type="hidden" name="hdIdPegwagai" class="hdIdPegwagai">
                <input type="hidden" name="hdBulan" class="hdBulan">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                  <div id="table-scroll" class="table-scroll">
                    {{-- <table id="main-table" class="main-table">
                      <thead>
                        <tr>
                          <th rowspan="2" width="50px" class="text-center txt-middle">No</th>
                          <th rowspan="2" colspan="15" class="txt-middle" width='600px'>Nama</th>
                          <th colspan="29" width='1450px' class="text-center">Jumlah Presentasi Kerja Harian</th>
                          <th rowspan="2" width="70px" class="text-center">Jumlah</th>
                          <th rowspan="2" width="70px" class="text-center">Jumlah AK</th>
                        </tr>
                        <tr>
                          @for($i = 1; $i <= 29; $i++)
                            <th width="50px" class="text-center">{{ $i }}</th>
                          @endfor
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th width="50px"></th>
                          <th colspan="15" width='600px'>level 1</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                        <tr>
                          <th width="50px"></th>
                          <th align="center" width="40px"></th>
                          <th class="level2" width="560px" colspan="14">Level 2</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                        <tr>
                          <th width="50px"></th>
                          <th align="center" width="40px"></th>
                          <th class="level2" align="center" width="40px"></th>
                          <th class="level3" width="520px" colspan="13">Level 3</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                        <tr>
                          <th width="50px"></th>
                          <th align="center" width="40px"></th>
                          <th class="level2" align="center" width="40px"></th>
                          <th class="level3" align="center" width="40px"></th>
                          <th class="level4" width="480px" colspan="12">Level 4</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                        <tr>
                          <th width="50px"></th>
                          <th align="center" width="40px"></th>
                          <th class="level2" align="center" width="40px"></th>
                          <th class="level3" align="center" width="40px"></th>
                          <th class="level4" align="center" width="40px"></th>
                          <th class="level5" width="440px" colspan="11">Level 5</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                        <tr>
                          <th width="50px"></th>
                          <th align="center" width="40px"></th>
                          <th class="level2" align="center" width="40px"></th>
                          <th class="level3" align="center" width="40px"></th>
                          <th class="level4" align="center" width="40px"></th>
                          <th class="level5" align="center" width="40px"></th>
                          <th class="level6" width="400px" colspan="10">Level 6</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                        @for ($j=0; $j < 10; $j++)
                          <tr>
                            <th width="50px"></th>
                            <th align="center" width="40px"></th>
                            <th class="level2" align="center" width="40px"></th>
                            <th class="level3" align="center" width="40px"></th>
                            <th class="level4" align="center" width="40px"></th>
                            <th class="level5" align="center" width="40px"></th>
                            <th class="level6" width="400px" colspan="10">Level 6<br>( Tiap 10 Pasien 0.013 ) <span style="float:right;">Dokter Ahli Pertama</th>
                            @for($a = 1; $a <= 29; $a++)
                              <td class="panelIsiKegiatan" id="idMaster-id_pak_master_a"><span class="dashboard-spinner spinner-secondary spinner-xs"></span></td>
                            @endfor
                            <td class="panelIsiKegiatan" id="idMaster-id_pak_master_jml" align="center" width="70px"><span class="btnIsiKegiatan">-</span></td>
                            <td id="idMaster-id_pak_master_jmlAk" align="center" width="70px"><span class="dashboard-spinner spinner-secondary spinner-xs"></span></td>
                          </tr>
                        @endfor
                      </tbody>
                      <tfoot>
                        <tr>
                          <th width="50px"></th>
                          <th colspan="15" width='600px'>level 1</th>
                          <td colspan="29" width='1450px'></td>
                          <td width="70px"></td>
                          <td width="70px"></td>
                        </tr>
                      </tfoot>
                    </table> --}}
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script type="text/javascript">
    $(document).ready(function () {
    });

    $('#idPegawai').chosen();

    $('#bln').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 4,
      minView: 3,
      forceParse: 0,
    });

    $('.btn-search').click(function(){
      var pegawai = $('#idPegawai').val();
      var bulan = $('#bln').val();
      var tgl = '';
      var idPak = '';
      var tag = '';
      $('.hdIdPegwagai').val(pegawai);
      $('.hdBulan').val(bulan);
      // var spiner = '<span class="dashboard-spinner spinner-secondary spinner-xs"></span>';
      var spiner = '';
      $.post("{{route('loadPantauKegiatan')}}",{id_pegawai:pegawai,bulan:bulan},function(data){
        var dte = new Date();
        var MonthNow = dte.getMonth();
        var YearNow = dte.getFullYear();
        var pecah = bulan.split('-')
        var dteIn = new Date(pecah[1]+'-'+pecah[0]+'-01');
        var MonthIn = dteIn.getMonth();
        var YearIn = dteIn.getFullYear();
        var akses = '';

        if (YearNow == YearIn) {
          if (MonthNow > MonthIn) { akses = 'Aktif'; }
          else{ akses = 'Tidak Aktif'; }
        }else if (YearNow > YearIn) { akses = 'Aktif'; }
        else{ akses = 'Tidak Aktif'; }        
        
        if(data.status=='success'){
          // var ini = data.row.acc;
          // console.log(ini);          
          if (data.row.acc == 'Belum' && akses == 'Aktif') {
            $('.btn-checked').removeAttr('disabled');
          }else{
            $('.btn-checked').attr('disabled', true);
          }
          
          var wIsi = 50 * data.row.jumlahTanggal;
          tag += '<table id="main-table" class="main-table">';
          tag += '<thead>';
          tag += '<tr>';
          tag += '<th rowspan="2" width="50px" class="text-center txt-middle">No</th>';
          tag += '<th rowspan="2" colspan="15" class="txt-middle" width="600px">Nama</th>';
          tag += '<th colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px" class="text-center">Jumlah Presentasi Kerja Harian</th>';
          tag += '<th rowspan="2" width="70px" class="text-center">Jumlah</th>';
          tag += '<th rowspan="2" width="70px" class="text-center">Jumlah AK</th>';
          tag += '</tr>';
          tag += '<tr>';
          for (var TitleDate = 1; TitleDate <= data.row.jumlahTanggal; TitleDate++) {
            tag += '<th class="text-center" width="50px">'+TitleDate+'</th>';
            if (tgl == '') { tgl = TitleDate; }
            else{ tgl = tgl+","+TitleDate; }
          }
          $('.tgl').html(tgl);
          tag += '</tr>';
          tag += '</thead>';
          tag += '<tbody>';
          if (data.row.butir.length != 0) {
            $.each(data.row.butir, function(k,uns){
              tag += '<tr>';
              tag += '<th width="50px"></th>';
              tag += '<th colspan="15" width="600px">'+uns.nama+'</th>';
              tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"</td>';
              tag += '<td width="70px"></td>';
              tag += '<td width="70px"></td>';
              tag += '</tr>';
              if (uns.row.length > 0) {
                var tempNo1 = 0;
                $.each(uns.row, function(k,v1){
                  tempNo1++;
                  if (v1.statusRow == 'Parent') {
                    tag += '<tr>';
                    tag += '<th class="text-center" width="50px">'+KonDecRomawi(tempNo1)+'.</th>';
                    tag += '<th colspan="15" width="600px">'+v1.butir_kegiatan+'</th>';
                    tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"</td>';
                    tag += '<td width="70px"></td>';
                    tag += '<td width="70px"></td>';
                    tag += '</tr>';
                    if (v1.row.length > 0) {
                      var tempNo2 = 0; // Hurug Awal
                      var tempNo2n = 0; // Hurug Akhir
                      var No2 = '';
                      var No2n = '';
                      $.each(v1.row, function(k,v2){
                        if (tempNo2n == 26) {
                          tempNo2++;
                          No2 = String.fromCharCode(64 + tempNo2);
                        }
                        tempNo2n++;
                        No2n = String.fromCharCode(64 + tempNo2n);
                        if (v2.statusRow == 'Parent') {
                          tag += '<tr>';
                          tag += '<th width="50px"></th>';
                          tag += '<th align="center" width="40px">'+No2+''+No2n+'.</th>';
                          tag += '<th class="level2" width="560px" colspan="14">'+v2.butir_kegiatan+'</th>';
                          tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"></td>';
                          tag += '<td width="70px"></td>';
                          tag += '<td width="70px"></td>';
                          tag += '</tr>';
                          if (v2.row.length > 0) {
                            var tempNo3 = 0;
                            $.each(v2.row, function(k,v3){
                              tempNo3++;
                              if (v3.statusRow == 'Parent') {
                                tag += '<tr>';
                                tag += '<th width="50px"></th>';
                                tag += '<th align="center" width="40px"></th>';
                                tag += '<th class="level2" align="center" width="40px">'+tempNo3+'.</th>';
                                tag += '<th class="level3" width="520px" colspan="13">'+v3.butir_kegiatan+'</th>';
                                tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"></td>';
                                tag += '<td width="70px"></td>';
                                tag += '<td width="70px"></td>';
                                tag += '</tr>';
                                if (v3.row.length > 0) {
                                  var tempNo4 = 0; // Hurug Awal
                                  var tempNo4n = 0; // Hurug Akhir
                                  var No4 = '';
                                  var No4n = '';
                                  $.each(v3.row, function(k,v4){
                                    if (tempNo4n == 26) {
                                      tempNo4++;
                                      No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                    }
                                    tempNo4n++;
                                    No4n = String.fromCharCode(64 + tempNo4n).toLowerCase();
                                    if (v4.statusRow == 'Parent') {
                                      tag += '<tr>';
                                      tag += '<th width="50px"></th>';
                                      tag += '<th align="center" width="40px"></th>';
                                      tag += '<th class="level2" align="center" width="40px"></th>';
                                      tag += '<th class="level3" align="center" width="40px">'+No4+''+No4n+'.</th>';
                                      tag += '<th class="level4" width="480px" colspan="12">'+v4.butir_kegiatan+'</th>';
                                      tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"></td>';
                                      tag += '<td width="70px"></td>';
                                      tag += '<td width="70px"></td>';
                                      tag += '</tr>';
                                      if (v4.row.length > 0) {
                                        var No5 = '<i class="fas fa-angle-double-right"></i>';
                                        $.each(v4.row, function(k,v5){
                                          if (v5.statusRow == 'Parent') {
                                            tag += '<tr>';
                                            tag += '<th width="50px"></th>';
                                            tag += '<th align="center" width="40px"></th>';
                                            tag += '<th class="level2" align="center" width="40px"></th>';
                                            tag += '<th class="level3" align="center" width="40px"></th>';
                                            tag += '<th class="level4" align="center" width="40px">'+No5+'.</th>';
                                            tag += '<th class="level5" width="440px" colspan="11">'+v5.butir_kegiatan+'</th>';
                                            tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"></td>';
                                            tag += '<td width="70px"></td>';
                                            tag += '<td width="70px"></td>';
                                            tag += '</tr>';
                                            if (v5.row.length > 0) {
                                              var No6 = '<i class="fas fa-terminal"></i>';
                                              $.each(v5.row, function(k,v6){
                                                if (v6.statusRow == 'Parent') {
                                                  tag += '<tr>';
                                                  tag += '<th width="50px"></th>';
                                                  tag += '<th align="center" width="40px"></th>';
                                                  tag += '<th class="level2" align="center" width="40px"></th>';
                                                  tag += '<th class="level3" align="center" width="40px"></th>';
                                                  tag += '<th class="level4" align="center" width="40px"></th>';
                                                  tag += '<th class="level5" align="center" width="40px">'+No6+'.</th>';
                                                  tag += '<th class="level6" width="400px" colspan="10">'+v6.butir_kegiatan+'</th>';
                                                  tag += '<td colspan="'+data.row.jumlahTanggal+'" width="'+wIsi+'px"></td>';
                                                  tag += '<td width="70px"></td>';
                                                  tag += '<td width="70px"></td>';
                                                  tag += '</tr>';
                                                }else{
                                                  tag += '<tr>';
                                                  tag += '<th width="50px"></th>';
                                                  tag += '<th align="center" width="40px"></th>';
                                                  tag += '<th class="level2" align="center" width="40px"></th>';
                                                  tag += '<th class="level3" align="center" width="40px"></th>';
                                                  tag += '<th class="level4" align="center" width="40px"></th>';
                                                  tag += '<th class="level5" align="center" width="40px">'+No6+'.</th>';
                                                  tag += '<th class="level6" width="400px" colspan="10">'+v6.butir_kegiatan+'<br>( Tiap '+v6.jum_min+' '+v6.satuan+' '+v6.points+' ) <span style="float:right;">'+v6.jabatan.nama+'</th>';
                                                  if (idPak == '') { idPak = v6.id_pak_master; }
                                                  else{ idPak = idPak+","+v6.id_pak_master; }
                                                  for (var a = 1; a <= data.row.jumlahTanggal; a++) {
                                                    tag += '<td class="panelIsiKegiatan" id="'+v6.id_pak_master+'_'+a+'">'+spiner+'</td>';
                                                  }
                                                  tag += '<td id="'+v6.id_pak_master+'_jml" align="center" width="70px">'+spiner+'</td>';
                                                  tag += '<td id="'+v6.id_pak_master+'_jmlAk" align="center" width="70px">'+spiner+'</td>';
                                                  tag += '</tr>';
                                                }
                                              });
                                            }
                                          }else{
                                            tag += '<tr>';
                                            tag += '<th width="50px"></th>';
                                            tag += '<th align="center" width="40px"></th>';
                                            tag += '<th class="level2" align="center" width="40px"></th>';
                                            tag += '<th class="level3" align="center" width="40px"></th>';
                                            tag += '<th class="level4" align="center" width="40px">'+No5+'.</th>';
                                            tag += '<th class="level5" width="440px" colspan="11">'+v5.butir_kegiatan+'<br>( Tiap '+v5.jum_min+' '+v5.satuan+' '+v5.points+' ) <span style="float:right;">'+v5.jabatan.nama+'</th>';
                                            if (idPak == '') { idPak = v5.id_pak_master; }
                                            else{ idPak = idPak+","+v5.id_pak_master; }
                                            for (var a = 1; a <= data.row.jumlahTanggal; a++) {
                                              tag += '<td class="panelIsiKegiatan" id="'+v5.id_pak_master+'_'+a+'">'+spiner+'</td>';
                                            }
                                            tag += '<td id="'+v5.id_pak_master+'_jml" align="center" width="70px">'+spiner+'</td>';
                                            tag += '<td id="'+v5.id_pak_master+'_jmlAk" align="center" width="70px">'+spiner+'</td>';
                                            tag += '</tr>';
                                          }
                                        });
                                      }
                                    }else{
                                      tag += '<tr>';
                                      tag += '<th width="50px"></th>';
                                      tag += '<th align="center" width="40px"></th>';
                                      tag += '<th class="level2" align="center" width="40px"></th>';
                                      tag += '<th class="level3" align="center" width="40px">'+No4+''+No4n+'.</th>';
                                      tag += '<th class="level4" width="480px" colspan="12">'+v4.butir_kegiatan+'<br>( Tiap '+v4.jum_min+' '+v4.satuan+' '+v4.points+' ) <span style="float:right;">'+v4.jabatan.nama+'</th>';
                                      if (idPak == '') { idPak = v4.id_pak_master; }
                                      else{ idPak = idPak+","+v4.id_pak_master; }
                                      for (var a = 1; a <= data.row.jumlahTanggal; a++) {
                                        tag += '<td class="panelIsiKegiatan" id="'+v4.id_pak_master+'_'+a+'">'+spiner+'</td>';
                                      }
                                      tag += '<td id="'+v4.id_pak_master+'_jml" align="center" width="70px">'+spiner+'</td>';
                                      tag += '<td id="'+v4.id_pak_master+'_jmlAk" align="center" width="70px">'+spiner+'</td>';
                                      tag += '</tr>';
                                    }
                                  });
                                }
                              }else{
                                tag += '<tr>';
                                tag += '<th width="50px"></th>';
                                tag += '<th align="center" width="40px"></th>';
                                tag += '<th class="level2" align="center" width="40px">'+tempNo3+'.</th>';
                                tag += '<th class="level3" width="520px" colspan="13">'+v3.butir_kegiatan+'<br>( Tiap '+v3.jum_min+' '+v3.satuan+' '+v3.points+' ) <span style="float:right;">'+v3.jabatan.nama+'</th>';
                                if (idPak == '') { idPak = v3.id_pak_master; }
                                else{ idPak = idPak+","+v3.id_pak_master; }
                                for (var a = 1; a <= data.row.jumlahTanggal; a++) {
                                  tag += '<td class="panelIsiKegiatan" id="'+v3.id_pak_master+'_'+a+'">'+spiner+'</td>';
                                }
                                tag += '<td id="'+v3.id_pak_master+'_jml" align="center" width="70px">'+spiner+'</td>';
                                tag += '<td id="'+v3.id_pak_master+'_jmlAk" align="center" width="70px">'+spiner+'</td>';
                                tag += '</tr>';
                              }
                            });
                          }
                        }else{
                          tag += '<tr>';
                          tag += '<th width="50px"></th>';
                          tag += '<th align="center" width="40px">'+No2+''+No2n+'.</th>';
                          tag += '<th class="level2" width="560px" colspan="14">'+v2.butir_kegiatan+'<br>( Tiap '+v2.jum_min+' '+v2.satuan+' '+v2.points+' ) <span style="float:right;">'+v2.jabatan.nama+'</th>';
                          if (idPak == '') { idPak = v2.id_pak_master; }
                          else{ idPak = idPak+","+v2.id_pak_master; }
                          for (var a = 1; a <= data.row.jumlahTanggal; a++) {
                            tag += '<td class="panelIsiKegiatan" id="'+v2.id_pak_master+'_'+a+'">'+spiner+'</td>';
                          }
                          tag += '<td id="'+v2.id_pak_master+'_jml" align="center" width="70px">'+spiner+'</td>';
                          tag += '<td id="'+v2.id_pak_master+'_jmlAk" align="center" width="70px">'+spiner+'</td>';
                          tag += '</tr>';
                        }
                      });
                    }
                  }else{
                    tag += '<tr>';
                    tag += '<th class="text-center" width="50px">'+KonDecRomawi(tempNo1)+'.</th>';
                    tag += '<th colspan="15" width="600px">'+v1.butir_kegiatan+'<br>( Tiap '+v1.jum_min+' '+v1.satuan+' '+v1.points+' ) <span style="float:right;">'+v1.jabatan.nama+'</th>';
                    if (idPak == '') { idPak = v1.id_pak_master; }
                    else{ idPak = idPak+","+v1.id_pak_master; }
                    for (var a = 1; a <= data.row.jumlahTanggal; a++) {
                      tag += '<td class="panelIsiKegiatan" id="'+v1.id_pak_master+'_'+a+'">'+spiner+'</td>';
                    }
                    tag += '<td id="'+v1.id_pak_master+'_jml" align="center" width="70px">'+spiner+'</td>';
                    tag += '<td id="'+v1.id_pak_master+'_jmlAk" align="center" width="70px">'+spiner+'</td>';
                    tag += '</tr>';
                  }
                });
              }
            });
          }else{
            var totLg = data.row.jumlahTanggal + 18;
            tag += '<tr><th colspan="'+totLg+'" class="text-center">Tidak ada data ditemukan...</th></tr>';
          }
          tag += '</tbody>';
          tag += '</table>';
          $('#table-scroll').empty();
          $('#table-scroll').html(tag);
          $('.idPaks').html(idPak);
          var arrayTamp = $('.idPaks').text().split(',');
          LoadGrid();
        }else{
          swal("MAAF !","Terjadi Kesalahan !!", "warning");
        }
      });
    });

    $('.btn-checked').click(function(){
      var pegawai = $('.hdIdPegwagai').val();
      var bulan = $('.hdBulan').val();
      $.post("{{route('accSatKerNilaiPantau')}}",{id_pegawai:pegawai,bulan:bulan},function(data){
        if(data.status=='success'){
          swal("Berhasil !", data.message, "success");
        }else{
          swal("Whooops !", data.message, "warning");
        }
      });
    });

    function LoadGrid() {
      var arrayIdPak = [];
      arrayIdPak = $('.idPaks').text().split(',');
      var arrayTanggal = [];
      arrayTanggal = $('.tgl').text().split(',');
      var totalArrayIdPak = arrayIdPak.length;
      var totalArrayTanggal = arrayTanggal.length;
      var tempIdPak = 0;
      var tempTanggal = 0;

      function valGrid(tempIdPak) {
        if (tempIdPak >= totalArrayIdPak) {
          console.log('berhenti !!');
          return ;
        }
        var idPegawai = $('.hdIdPegwagai').val();
        var sendBln = $('.hdBulan').val();
        return $.ajax({
          url: "{{ route('getNilaiPantau') }}?idPegawai="+idPegawai+"&idPakMaster="+arrayIdPak[tempIdPak]+"&bulan="+sendBln, success: function(result){
            if (result.status == 'success') {
              $.each(result.row, function(k,v){
                if (v.hasil != '-') {
                  var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan('+v.idPenilaian+')">'+v.hasil+'</a>';
                }else{
                  var isi = '<span class="btnIsiKegiatan">'+v.hasil+'</span>';
                }
                $('#'+v.idpak+'_'+v.tgl).html(isi);
              });
              $('#'+result.dtJumlah.idpak+'_jml').html(result.dtJumlah.jml);
              $('#'+result.dtJumlah.idpak+'_jmlAk').html(result.dtJumlah.jmlAk);
            }else{
              return valGrid(tempIdPak);
            }
          },
          error:function() {
            return valGrid(tempIdPak);
          }
        }).then(function () {
          return valGrid(tempIdPak + 1);
        });
      }

      valGrid(tempIdPak);
    }

    function detailKegiatan(idPenilaian) {
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('viewDetailKegiatan') !!}", {idPenilaian:idPenilaian}).done(function(data){
        if (data.status == 'success') {
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        }else{
          swal("MAAF !","Terjadi Kesalahan !!", "warning");
        }
      });
    }

    function KonDecRomawi(angka) {
      var hasil = "";
      if (angka < 1 || angka > 5000) {
        hasil += "Batas Angka 1 s/d 5000";
      }else{
        while (angka >= 1000) {
          hasil += "M";
          angka = angka - 1000;
        }
      }

      if (angka >= 500) {
        if (angka > 500) {
          if (angka >= 900) {
            hasil += "CM";
            angka = angka - 900;
          } else {
            hasil += "D";
            angka = angka - 500;
          }
        }
      }

      while (angka >= 100) {
        if (angka >= 400) {
          hasil += "CD";
          angka = angka - 400;
        } else {
          angka = angka - 100;
        }
      }

      if (angka >= 50) {
        if (angka >= 90) {
          hasil += "XC";
          angka = angka - 90;
        } else {
          hasil += "L";
          angka = angka - 50;
        }
      }

      while (angka >= 10) {
        if (angka >= 40) {
          hasil += "XL";
          angka = angka - 40;
        } else {
          hasil += "X";
          angka = angka - 10;
        }
      }

      if (angka >= 5) {
        if (angka == 9) {
          hasil += "IX";
          angka = angka - 9;
        } else {
          hasil += "V";
          angka = angka - 5;
        }
      }

      while (angka >= 1) {
        if (angka == 4) {
          hasil += "IV";
          angka = angka - 4;
        } else {
          hasil += "I";
          angka = angka - 1;
        }
      }
      return hasil;
    }
  </script>
@stop
