<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{ $data['title'] }} | Target Kinerja Utama (TKU)</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/')}}/assets/images/logo/bakti-husada.png"/>
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="{{ url('/')}}/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/style.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/animate.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/sweetalert/sweetalert.css">
		<script src="{{ url('/')}}/assets/vendor/sweetalert/sweetalert-dev.js"></script>
    <!-- ..::: Chosen :::.. -->
		<link href="{{ url('/')}}/assets/libs/css/chosen/chosen.css" rel="stylesheet" type="text/css" />
    <!-- ..::: DateTimePicker :::.. -->
    <link href="{{ url('/')}}/assets/libs/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ url('/')}}/css/custom.css">
    <!--  ..::: EXTRA CSS :::.. -->
		@yield('extended_css')
		<!--  ..::: END EXTRA CSS :::.. -->
  </head>
  <body>
    <div class="dashboard-main-wrapper">
      <!--  ..::: navbar :::.. -->
      @include('component.navbar')
      <!--  ..::: end navbar :::.. -->

      <!--  ..::: left sidebar :::.. -->
      @include('component.sidebar')
      <!--  ..::: end left sidebar :::.. -->

      <div class="dashboard-wrapper">
        @yield('content')

        <!--  ..::: footer :::.. -->
        <div class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                Copyright ©2019 Dinas Kesehatan Sidoarjo. Development by <a href="http://natusi.co.id" target="_blank">CV. Natusi</a>.
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="text-md-right footer-links d-none d-sm-block">
                  <b>Version</b> 1.0.0
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--  ..::: end footer :::.. -->
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="{{ url('/')}}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="{{ url('/')}}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="{{ url('/')}}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    {{-- <script src="{{ url('/')}}/assets/libs/js/main-js.js"></script> --}}
    <script src="{{ url('/')}}/assets/libs/js/animate.js"></script>
    <script src="{{ url('/')}}/assets/libs/js/validate.js"></script>
    <!-- Chosen -->
		<script src="{{ url('/')}}/assets/libs/js/chosen.jquery.min.js"></script>
    <!-- DateTimePicker -->
    <script src="{{ url('/')}}/assets/libs/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('/')}}/js/datagrid.js"></script>

    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      function Kosong(){
        swal({
          title: "MAAF !",
          text: "Fitur Belum Bisa Digunakan !!",
          type: "warning",
          timer: 2000,
          showConfirmButton: false
        });
      };

      function noAccess(){
        swal({
          title: "MAAF !",
          text: "Anda Tidak Memiliki Akses Untuk Fitur Ini !!",
          type: "warning",
          timer: 2000,
          showConfirmButton: false
        });
      };

      @if(!empty(Session::get('message')))
        swal({
          title : "{{ Session::get('title') }}",
          text : "{{ Session::get('message') }}",
          type : "{{ Session::get('type') }}",
          showConfirmButton: true
        });
      @endif

      var screenHight = window.innerHeight;
      var heightHeader = $('.navbar-expand-lg').height();
      var heightSidebar = screenHight - heightHeader + 7;
      $('.nav-left-sidebar').attr('style','height:'+heightSidebar+'px');

      $(document).ready(function() {
        cekWaktu();
        cekPermenpan();
        setInterval(cekWaktu, 300000); //request tiap 5 menit
      });

      function cekWaktu() {
        $.post("{!! route('cekWaktu') !!}").done(function(data){
          if(data.status == 'success'){
            console.log(data.message);
            // swal({
            //   title : "Berhasil !",
            //   text : data.message,
            //   type : data.status,
            //   timer : 2000,
            //   showConfirmButton : false
            // });
          }
        });
      }

      function cekPermenpan() {
        var dte = new Date();
        var hari = dte.getDate();
        var bulan = dte.getMonth() + 1;
        var tahun = dte.getFullYear();
        if ((parseInt(hari) >= 1 && parseInt(hari) <= 5) && parseInt(bulan) == 1) {
          $.post("{!! route('cekPermenpan') !!}").done(function(data){
            if(data.status == 'success'){
              console.log(data.message);
            }
          });
        }else{
          console.log('bukan waktu cek');
        }
        // console.log('Tgl '+hari+' bulan '+bulan+' tahun '+tahun);
      }

      function changeDate2Indo(tgl) {
        var pecah = tgl.split('-');
        var indo = pecah[2]+'-'+pecah[1]+'-'+pecah[0];
        switch (pecah[1]) {
          case '01':
            var nama = 'Januari';
            break;
          case '02':
            var nama = 'Februari';
            break;
          case '03':
            var nama = 'Maret';
            break;
          case '04':
            var nama = 'April';
            break;
          case '05':
            var nama = 'Mei';
            break;
          case '06':
            var nama = 'Juni';
            break;
          case '07':
            var nama = 'Juli';
            break;
          case '08':
            var nama = 'Agustus';
            break;
          case '09':
            var nama = 'September';
            break;
          case '10':
            var nama = 'Oktober';
            break;
          case '11':
            var nama = 'November';
            break;
          case '12':
            var nama = 'Desember';
            break;
          default:
            var nama = '-';
            break;
        }
        // return indo;
        return pecah[2]+' '+nama+' '+pecah[0];
      }
    </script>

    <!--  ..::: EXTRA CSS :::.. -->
		@yield('extended_js')
		<!--  ..::: END EXTRA CSS :::.. -->
  </body>
</html>
