<div class="nav-left-sidebar sidebar-dark">
  <div class="menu-list">
    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="d-xl-none d-lg-none" href="javascript:void(0)">Dashboard</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav flex-column">
          <li class="nav-divider">Menu</li>
          <li class="nav-item ">
            <a class="nav-link @if($data['mn_active'] == 'dashboard') active @endif" href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i>Dashboard</a>
          </li>
          @if (Auth::getUser()->level_user == '5')
            <li class="nav-item ">
              <a class="nav-link @if($data['mn_active'] == 'kegiatanHarian') active @endif" href="{{ route('kegiatanHarian') }}"><i class="fas fa-list-alt"></i>Kegiatan Harian</a>

              <a class="nav-link @if($data['mn_active'] == 'kegiatanHarianDownload') active @endif" href="{{ route('kegiatanHarianDownload') }}"><i class="fas fa-download"></i>Download Kegiatan Harian</a>

              <a class="nav-link @if($data['mn_active'] == 'kegiatanHarianGab') active @endif" href="{{ route('kegiatanHarianGabung') }}"><i class="fas fa-list-alt"></i>Kegiatan Harian Gabung</a>
              {{-- <a class="nav-link @if($data['mn_active'] == 'unsurPendidikan') active @endif" href="{{ route('unsurPendidikan') }}"><i class="fas fa-graduation-cap"></i>Unsur Pendidikan</a> --}}
            </li>
          @endif
          @if (Auth::getUser()->level_user == '1' || Auth::getUser()->is_leader == 'Y')
            <li class="nav-item ">
              <a class="nav-link @if($data['mn_active'] == 'pantauKegiatan') active @endif" href="{{ route('pantauKegiatan') }}"><i class="fas fa-user"></i>Kegiatan Pegawai</a>
            </li>
          @endif
          @if (Auth::getUser()->level_user == '1' || Auth::getUser()->is_penilai == 'Y')
            {{-- <li class="nav-item ">
              <a class="nav-link @if($data['mn_active'] == 'pantauKegiatan') active @endif" href="{{ route('pantauKegiatan') }}"><i class="fas fa-user"></i>Penilaian Dupak</a>
            </li> --}}
            <li class="nav-item"> 
              <a class="nav-link @if($data['mn_active'] == 'penilaianDupak') active @endif" href="{{ route('penilaianDupak') }}"><i class="fas fa-file"></i>Penilaian Dupak</a>
            </li>
          @endif
          @if (Auth::getUser()->is_penilai == 'Y')
            <li class="nav-item"> 
              <a class="nav-link @if($data['mn_active'] == 'RiwayatpenilaianDupak') active @endif" href="{{ route('RiwayatpenilaianDupak') }}"><i class="fas fa-list"></i>Riwayat Penilaian Dupak</a>
            </li>
          @endif

          @if (Auth::getUser()->level_user != '1')
          @php
            $isPns = \DB::table('pegawai')->where('user_id', Auth::getUser()->id)
                        ->first()->status_pegawai;
          @endphp  
          @endif

          @if (Auth::getUser()->level_user == '1' || (Auth::getUser()->level_user != '1' && $isPns == 'PNS'))
          <li class="nav-item">
            <a class="nav-link @if($data['mn_active'] == 'rekapKegiatan') active @endif" href="{{ route('rekapKegiatan') }}"><i class="fas fa-file-excel"></i>Rekap Kegiatan Harian</a>
          </li>
          @endif

          @if (Auth::getUser()->is_ketua_penilai == 'Y' )
         <li class="nav-item">
            <a class="nav-link @if($data['mn_active'] == 'master') active @endif" href="javascript:void(0);" data-toggle="collapse" @if($data['mn_active'] == 'master') aria-expanded="true" @else aria-expanded="false" @endif data-target="#submenu-master" aria-controls="submenu-master">
              <i class="fas fa-database"></i>Master
            </a>
            <div id="submenu-master" class="collapse submenu @if($data['mn_active'] == 'master') show @endif" style="">
              <ul class="nav flex-column">     
                <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'butirKegiatanNew') active @endif" href="{{ route('butirKegiatanNew') }}"><i class="fas fa-user"></i>Butir Kegiatan</a></li>
              </ul>
            </div>
          </li>
          @endif

          @if (Auth::getUser()->level_user == '1' || Auth::getUser()->level_user == '2')
            <li class="nav-item">
              <a class="nav-link @if($data['mn_active'] == 'pengajuanDupak') active @endif" href="{{ route('pengajuanDupak') }}"><i class="fas fa-file"></i>Pengajuan Dupak</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($data['mn_active'] == 'masaPengajuanDupak') active @endif" href="{{ route('masaPengajuanDupak') }}"><i class="fas fa-file"></i>Waktu Pengajuan Dupak</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($data['mn_active'] == 'users') active @endif" href="javascript:void(0);" data-toggle="collapse" @if($data['mn_active'] == 'users') aria-expanded="true" @else aria-expanded="false" @endif data-target="#submenu-users" aria-controls="submenu-users">
                <i class="fas fa-users"></i>Pengguna
              </a>
              <div id="submenu-users" class="collapse submenu @if($data['mn_active'] == 'users') show @endif" style="">
                <ul class="nav flex-column">
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'userAdmin') active @endif" href="{{ route('userAdmin') }}"><i class="fas fa-cog"></i>Admin</a></li>
                  {{-- <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'userOperator') active @endif" href="{{ route('userOperator') }}"><i class="fas fa-briefcase"></i>Operator</a></li> --}}
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'userPenilai') active @endif" href="{{ route('userPenilai') }}"><i class="fas fa-briefcase"></i>Penilai</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'userKetuaPenilai') active @endif" href="{{ route('userKetuaPenilai') }}"><i class="fas fa-user"></i>Ketua Penilai</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'userSatuanKerja') active @endif" href="{{ route('userSatuanKerja') }}"><i class="fas fa-university"></i>Satuan Kerja</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'userPegawai') active @endif" href="{{ route('userPegawai') }}"><i class="fas fa-briefcase"></i>Pegawai</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($data['mn_active'] == 'master') active @endif" href="javascript:void(0);" data-toggle="collapse" @if($data['mn_active'] == 'master') aria-expanded="true" @else aria-expanded="false" @endif data-target="#submenu-master" aria-controls="submenu-master">
                <i class="fas fa-database"></i>Master
              </a>
              <div id="submenu-master" class="collapse submenu @if($data['mn_active'] == 'master') show @endif" style="">
                <ul class="nav flex-column">
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'setting') active @endif" href="{{ route('setting') }}"><i class="fas fa-cog"></i>Setting</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'profesi') active @endif" href="{{ route('profesi') }}"><i class="fas fa-briefcase"></i>Profesi</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'golongan') active @endif" href="{{ route('golongan') }}"><i class="fas fa-briefcase"></i>Golongan</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'jabatan') active @endif" href="{{ route('jabatan') }}"><i class="fas fa-university"></i>Jabatan</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'typeSaKer') active @endif" href="{{ route('typeSaKer') }}"><i class="fas fa-briefcase"></i>Type Satuan Kerja</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'satuanKerja') active @endif" href="{{ route('satuanKerja') }}"><i class="fas fa-briefcase"></i>Satuan Kerja</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'pegawai') active @endif" href="{{ route('pegawai') }}"><i class="fas fa-user"></i>Pegawai</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'lampiranBahan') active @endif" href="{{ route('masterLampiranBahan') }}"><i class="fas fa-user"></i>Lampiran Bahan</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'permenpan') active @endif" href="{{ route('permenpan') }}"><i class="fas fa-university"></i>Permenpan</a></li>
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'unsurPak') active @endif" href="{{ route('unsurPak') }}"><i class="fas fa-user"></i>Unsur PAK</a></li>
                  {{-- <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'butirKegiatan') active @endif" href="{{ route('butirKegiatan') }}"><i class="fas fa-user"></i>Butir Kegiatan</a></li> --}}
                  <li class="nav-item"><a class="nav-link @if($data['submn_active'] == 'butirKegiatanNew') active @endif" href="{{ route('butirKegiatanNew') }}"><i class="fas fa-user"></i>Butir Kegiatan</a></li>
                </ul>
              </div>
            </li>
          @endif

          @if (Auth::getUser()->level_user == '4')
            <li class="nav-item ">
              <a class="nav-link @if($data['mn_active'] == 'pegawai') active @endif" href="{{ route('pegawai') }}"><i class="fas fa-user"></i>Pegawai</a>
            </li>
          @endif

          @if (Auth::getUser()->level_user == '1')
            <li class="nav-item ">
              <a class="nav-link @if($data['mn_active'] == 'LogButirKegiatan') active @endif" href="{{ route('LogButirKegiatan') }}"><i class="fas fa-database"></i>Aktifitas Butir Kegiatan</a>
            </li>
          @endif

        </ul>
      </div>
    </nav>
  </div>
</div>
