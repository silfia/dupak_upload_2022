<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{ $data['title'] }} | Target Kinerja Utama (TKU)</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/')}}/assets/images/logo/bakti-husada.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="{{ url('/')}}/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/style.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/animate.css">
    <link href="{{ url('/')}}/assets/libs/css/chosen/chosen.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/sweetalert/sweetalert.css">
		<script src="{{ url('/')}}/assets/vendor/sweetalert/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="{{ url('/')}}/css/custom.css">

    <!-- ..::: DateTimePicker :::.. -->
    <link href="{{ url('/')}}/assets/libs/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <style>
      html,
      body {
        height: 100%;
      }
      body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
      }
      .txt-cpRght {
        text-align: center;
        padding-top: 10px;
        font-size: 11px;
      }
      .txt-vrs {
        text-align: center;
        font-size: 10px;
      }
      .splash-description {
        padding-bottom: 0px;
        margin-top: 15px;
      }
      .iconStatus {
      	position:absolute;
      	right:20px;
      	top:5px;
      	font-size:18px;
      }

      .messageError {
      	font-size:12px;
      	padding-top:5px;
      	color:red;
        position: absolute;
        right: 45px;
        top: 3px;
        font-style: italic;
      }
      .text-red { color: red; }
      .text-green { color:green; }
      .btn.disabled, .btn:disabled {
        cursor: no-drop !important;
      }
      .errorConfirmPass{
        right: 75px !important;
      }
      #icon_confirmPass{
        right:50px !important;
      }
      .iconEye{
        position:absolute;
      	right:20px;
      	top:5px;
      	font-size:18px;
      }
    </style>
  </head>
  <body>
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
              <a href="{{ route('dashboard') }}">
                <img class="logo-img" src="{{ url('/')}}/assets/images/logo/bakti-husada.png" alt="logo" height="150px">
              </a>
              <span class="splash-description">Selamat Datang di Aplikasi<br>Target Kinerja Utama (TKU)</span>
              <span>Anda harus Registrasi terlebih dahulu untuk bisa mengakses aplikasi ini</span>
            </div>

            <div class="card-body">
              <span class="panelStatusPajak"></span>
              <span class="panelStatusPajak"></span>
              <form class="form-save" id="myform">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                      <h5 class="card-header">Form Registrasi Data Pegawai</h5>
                      <div class="card-body">
                        <div class="form-group row">
                          <label for="inputEmail" class="col-4 col-lg-3 col-form-label">Email <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <input id="inputEmail" type="text" name="email" onkeyup="cekMail()" placeholder="Email" class="form-control">
                            <div class='iconStatus text-green' id='icon_email'></div>
                            <p class='messageError errorMail'></p>
                            <input type='hidden' name='statusMail' value='Exist' id='statusMail' class='form-control'>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <input id="inputNama" type="text" name="nama" placeholder="Nama Pegawai" class="form-control">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Kelamin <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9 row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                              <label class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="jenis_kelamin" value="L" class="custom-control-input"><span class="custom-control-label">Pria</span>
                              </label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                              <label class="custom-control custom-radio custom-control-inline">
                                <input type="radio" name="jenis_kelamin" value="P" class="custom-control-input"><span class="custom-control-label">Wanita</span>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputTempatLahir" class="col-4 col-lg-3 col-form-label">TTL <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9 row pr-0">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                              <input id="inputTempatLahir" type="text" name="tempat_lahir" placeholder="Tempat Lahir" class="form-control">
                            </div>
                            <div class="input-group input-group-sm col-lg-5 col-md-5 col-sm-12 col-xs-12 pr-0">
                              <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                              <input type="text" name="tanggal_lahir" placeholder="Tgl Lahir" class="form-control inputTglLahir" id="inputTglLahir" data-date-format="dd-mm-yyyy">
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <select id="inputProfesi" class="form-control" name="tipe_profesi_id">
                              <option disabled selected> .:: Pilih Profesi ::. </option>
                              @foreach ($data['profesi'] as $pf)
                                <option value="{{ $pf->id_master_type }}">{{ $pf->nama }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputSaKer" class="col-4 col-lg -3 col-form-label">Satuan Kerja <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <select id="inputSaKer" class="form-control" name="satuan_kerja_id" required="">
                              <option disabled="" selected=""> .:: Pilih Satuan Kerja ::. </option>
                                @foreach ($data['satuankerja'] as $stk)
                                  <option value="{{ $stk->id_satuan_kerja }}">{{ $stk->nama }}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputJenis" class="col-4 col-lg-3 col-form-label">Status Pegawai <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9 row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                              <label class="custom-control custom-radio custom-control-inline status_pegawai">
                                <input type="radio" name="status_pegawai" value="PNS" id="jenisPns" onclick="formPns()" class="custom-control-input">
                                <label class="custom-control-label" for="jenisPns">PNS</label>
                              </label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                              <label class="custom-control custom-radio custom-control-inline status_pegawai">
                                <input type="radio" name="status_pegawai" value="Non-PNS" id="jenisNonPns" onclick="formNonPns()" class="custom-control-input">
                                <span class="custom-control-label">Non-PNS</span>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputNip" class="col-4 col-lg-3 col-form-label" id='labelNip'>No NIP <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <input id="inputNip" type="text" name="no_nip" onkeyup="cekNip()" placeholder="No NIP / NIK" class="form-control">
                            <div class='iconStatus text-green' id='icon_nip'></div>
                            <p class='messageError errorNip'></p>
                            <input type='hidden' name='statusNip' value='Exist' id='statusNip' class='form-control'>
                            <small style="color:red;">gunakan NIK *untuk pegawai dengan status NON PNS</small>
                          </div>
                        </div>
                        <div class="form-group row" id='panelKarpeg' style="display:none">
                          <label for="inputKarPeng" class="col-4 col-lg-3 col-form-label">No Karpeg <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <input id="inputKarPeng" type="text" name="no_karpeng" placeholder="No Kartu Pengenal" class="form-control">
                            <small style="color:red;">*hanya untuk pegawai dengan status PNS</small>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPendidikan" class="col-4 col-lg-3 col-form-label p-b-0 p-t-0">Pendidikan Terakhir<br>Sesuai SK</label>
                          <div class="col-8 col-lg-9">
                            <input id="inputPendidikan" type="text" name="pendidikan_terakhir" placeholder="Pendidikan Terakhir Sesuai SK" class="form-control">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputThnLulus" class="col-4 col-lg-3 col-form-label">Tahun Lulus</label>
                          <div class="col-8 col-lg-9">
                            <!--<input id="inputThnLulus" type="text" name="pendidikan_tahun" placeholder="Tahun Lulus" class="form-control">-->
                            <input type="text" id="inputThnLulus" name="pendidikan_tahun" autocomplete="off" class="form-control form-control-sm filter_tahunan" placeholder="Tahun (yyyy)" data-date-format="yyyy">
                          </div>
                        </div>
                        <span class="panel-jabatan" style="display:none">
                          <div class="form-group row">
                            <label for="inputGolongan" class="col-4 col-lg-3 col-form-label">Golongan</label>
                            <div class="col-8 col-lg-9">
                              <select id="inputGolongan" class="form-control" name="golongan_id">
                                <option disabled selected> .:: Pilih Golongan ::. </option>
                                @foreach ($data['golongan'] as $gln)
                                  <option value="{{ $gln->id_master }}">{{ $gln->nama }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputGolTmt" class="col-4 col-lg-3 col-form-label">Golongan TMT</label>
                            <div class="input-group input-group-sm col-8 col-lg-9">
                              <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                              <input type="text" name="golongan_tmt" placeholder="Golongan TMT" class="form-control inputGolTmt" data-date-format="dd-mm-yyyy">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
                            <div class="col-8 col-lg-9">
                              <select id="inputJabatan" class="form-control" name="jabatan_id">
                                <option disabled selected> .:: Pilih Jabatan ::. </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputJabTmt" class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
                            <div class="input-group input-group-sm col-8 col-lg-9">
                              <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                              <input type="text" name="jabatan_tmt" placeholder="Jabatan TMT" class="form-control inputJabTmt" data-date-format="dd-mm-yyyy">
                            </div>
                          </div>
                        </span>
                        <div class="form-group row">
                          <label for="inputPassword" class="col-4 col-lg-3 col-form-label">Password <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <input id="inputPassword" type="password" name="password" placeholder="Password" class="form-control">
                            <div class='iconEye'>
                              <a href="javascript:void(0)" onclick="showPassword()" id='eyePass'><i class="far fa-eye-slash"></i></a>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputConfirmPass" class="col-4 col-lg-3 col-form-label">Konfirmasi Password <small style="color:red;">*</small></label>
                          <div class="col-8 col-lg-9">
                            <input id="inputConfirmPass" type="password" name="confirm_pass" onkeyup="confirmPassword()" placeholder="Konfirmasi Password" class="form-control">
                            <div class='iconEye'>
                              <a href="javascript:void(0)" onclick="showCornPassword()" id='eyeCornPass'><i class="far fa-eye-slash"></i></a>
                            </div>
                            <div class='iconStatus text-green' id='icon_confirmPass'></div>
                            <p class='messageError errorConfirmPass'></p>
                            <input type='hidden' name='statusConfirmPass' value='Failed' id='statusConfirmPass' class='form-control'>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a href="{{ route('login') }}" class="btn btn-sm btn-space btn-secondary"><span class="fas fa-chevron-left"></span> Kembali</a>
                    <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit" disabled>Simpan <span class="fas fa-save"></span></button>
                  </div>
                </div>
              </form>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <script src="{{ url('/')}}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="{{ url('/')}}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="{{ url('/')}}/assets/libs/js/animate.js"></script>
    <script src="{{ url('/')}}/assets/libs/js/validate.js"></script>
    <script src="{{ url('/')}}/assets/libs/js/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="{{ url('/')}}/assets/libs/js/jquery.backstretch.min.js"></script>
		<script>
			$.backstretch("{!! url('assets/images/background/Preview Image.jpg') !!}", {speed: 500});
		</script>
    <!-- DateTimePicker -->
    <script src="{{ url('/')}}/assets/libs/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
    $('.inputTglLahir').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
    });
    $('.inputGolTmt').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
    });
    $('.inputJabTmt').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
    });
    $('.filter_tahunan').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 0,
        startView: 4,
        minView: 4,
        forceParse: 0,
    });
    
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function disabledBtn() {
      var stMail = $('#statusMail').val();
      var stNip = $('#statusNip').val();
      var stConfirmPass = $('#statusConfirmPass').val();
      if (stMail == 'Ready' && stNip == 'Ready' && stConfirmPass == 'Ready') {
        $('.btn-submit').removeAttr('disabled');
      }else{
        $('.btn-submit').attr('disabled', true);
      }
    }

    function showPassword() {
      var tag = document.getElementById("inputPassword").getAttribute("type");
      if (tag == 'password') {
        $('#inputPassword').attr('type','text');
        $('#eyePass').html('<i class="fas fa-eye"></i>');
      }else{
        $('#inputPassword').attr('type','password');
        $('#eyePass').html('<i class="far fa-eye-slash"></i>');
      }
    }

    function showCornPassword() {
      var tag = document.getElementById("inputConfirmPass").getAttribute("type");
      if (tag == 'password') {
        $('#inputConfirmPass').attr('type','text');
        $('#eyeCornPass').html('<i class="fas fa-eye"></i>');
      }else{
        $('#inputConfirmPass').attr('type','password');
        $('#eyeCornPass').html('<i class="far fa-eye-slash"></i>');
      }
    }

    function cekMail() {
      var email = $('#inputEmail').val();
      var atpos = email.indexOf("@");
      var dotpos = email.lastIndexOf(".");
      if (email != '') {
        if (atpos < 1 || dotpos < atpos+2 || dotpos+2 >= email.length) {
          $('#inputEmail').attr('class','form-control is-invalid');
          $('#icon_email').attr('class','iconStatus text-red');
          $('#icon_email').html('<i class="fas fa-exclamation-triangle"></i>');
          $('.errorMail').html('Email Tidak Sesuai');
          $('#statusMail').val('Exist');
          disabledBtn();
        }else{
          $.post("{!! route('getEmail') !!}", {email:email}).done(function(data){
            if (data.status == 'success') {
              $('#inputEmail').attr('class','form-control is-invalid');
              $('#icon_email').attr('class','iconStatus text-red');
              $('#icon_email').html('<i class="fas fa-exclamation-triangle"></i>');
              $('.errorMail').html('Email Telah Terdaftar');
              $('#statusMail').val('Exist');
              disabledBtn();
            }else{
              $('#inputEmail').attr('class','form-control is-valid');
              $('#icon_email').attr('class','iconStatus text-green');
              $('#icon_email').html('<i class="fas fa-check-circle"></i>');
              $('.errorMail').html('');
              $('#statusMail').val('Ready');
              disabledBtn();
            }
          });
        }
      }else{
        $('#inputEmail').attr('class','form-control');
        $('#icon_email').attr('class','iconStatus');
        $('#icon_email').html('');
        $('.errorMail').html('');
        $('#statusMail').val('Exist');
        disabledBtn();
      }
    }

    function cekNip() {
      var nip = $('#inputNip').val();
      var statusPeg = $("input[name='status_pegawai']:checked").val();

      // console.log(statusPeg);
      if (nip != '') {
        if(statusPeg == "PNS"){
          if (nip.length == 18) {
            $.post("{!! route('getNip') !!}", {nip:nip}).done(function(data){
              if (data.status == 'success') {
                $('#inputNip').attr('class','form-control is-invalid');
                $('#icon_nip').attr('class','iconStatus text-red');
                $('#icon_nip').html('<i class="fas fa-exclamation-triangle"></i>');
                $('.errorNip').html('NIP Telah Terdaftar');
                $('#statusNip').val('Exist');
                disabledBtn();
              }else{
                $('#inputNip').attr('class','form-control is-valid');
                $('#icon_nip').attr('class','iconStatus text-green');
                $('#icon_nip').html('<i class="fas fa-check-circle"></i>');
                $('.errorNip').html('');
                $('#statusNip').val('Ready');
                disabledBtn();
              }
            });
          }else{
            $('#inputNip').attr('class','form-control is-invalid');
            $('#icon_nip').attr('class','iconStatus text-red');
            $('#icon_nip').html('<i class="fas fa-exclamation-triangle"></i>');
            $('.errorNip').html('NIP Harus 18 Digit');
            $('#statusNip').val('Exist');
            disabledBtn();
          }
        }else{
          if (nip.length == 16) {
            $.post("{!! route('getNip') !!}", {nip:nip}).done(function(data){
              if (data.status == 'success') {
                $('#inputNip').attr('class','form-control is-invalid');
                $('#icon_nip').attr('class','iconStatus text-red');
                $('#icon_nip').html('<i class="fas fa-exclamation-triangle"></i>');
                $('.errorNip').html('NIK Telah Terdaftar');
                $('#statusNip').val('Exist');
                disabledBtn();
              }else{
                $('#inputNip').attr('class','form-control is-valid');
                $('#icon_nip').attr('class','iconStatus text-green');
                $('#icon_nip').html('<i class="fas fa-check-circle"></i>');
                $('.errorNip').html('');
                $('#statusNip').val('Ready');
                disabledBtn();
              }
            });
          }else{
            $('#inputNip').attr('class','form-control is-invalid');
            $('#icon_nip').attr('class','iconStatus text-red');
            $('#icon_nip').html('<i class="fas fa-exclamation-triangle"></i>');
            $('.errorNip').html('NIK Harus 16 Digit');
            $('#statusNip').val('Exist');
            disabledBtn();
          }
        }
      }else{
        $('#inputNip').attr('class','form-control');
        $('#icon_nip').attr('class','iconStatus');
        $('#icon_nip').html('');
        $('.errorNip').html('');
        $('#statusNip').val('Exist');
        disabledBtn();
      }
    }

    function confirmPassword() {
      var password = $('#inputPassword').val();
      var confirmPass = $('#inputConfirmPass').val();
      if (password != '' && confirmPass != '') {
        if (password != confirmPass) {
          $('#inputConfirmPass').attr('class','form-control is-invalid');
          $('#icon_confirmPass').attr('class','iconStatus text-red');
          $('#icon_confirmPass').html('<i class="fas fa-exclamation-triangle"></i>');
          $('.errorConfirmPass').html('Password Tidak Cocok');
          $('#statusConfirmPass').val('Failed');
          disabledBtn();
        }else{
          $('#inputConfirmPass').attr('class','form-control is-valid');
          $('#icon_confirmPass').attr('class','iconStatus text-green');
          $('#icon_confirmPass').html('<i class="fas fa-check-circle"></i>');
          $('.errorConfirmPass').html('');
          $('#statusConfirmPass').val('Ready');
          disabledBtn();
        }
      }else {
        $('#inputConfirmPass').attr('class','form-control');
        $('#icon_confirmPass').attr('class','iconStatus');
        $('#icon_confirmPass').html('');
        $('.errorConfirmPass').html('');
        $('#statusConfirmPass').val('Failed');
        disabledBtn();
      }
    }

    $('#inputProfesi').chosen();
    $('#inputSaKer').chosen();
    $('#inputGolongan').chosen();
    $('#inputJabatan').chosen();
    $('#inputProfesi').change(function(){
    	var id= $('#inputProfesi').val();
    	$.post("{!! route('getjabatan') !!}", {id :id }).done(function(data){
    		var listKab = '<option value="" disabled selected> .:: Pilih Jabatan ::. </option>';
    		if(data.length > 0){
    			$.each(data, function(k,v){
    				listKab += '<option value="'+v.id_master+'">'+v.nama+'</option>';
    			});
    		}
    		$('#inputJabatan').html(listKab);
    		$('#inputJabatan').trigger('chosen:updated');
    	});
    });

    function formPns() {
      $('#labelNip').html('No NIP / NIK');
      $('#panelKarpeg').show();
      $('.panel-jabatan').show();
    }
    function formNonPns() {
      $('#labelNip').html('No NIP / NIK');
      $('#panelKarpeg').hide();
      $('.panel-jabatan').hide();
    }

    $('.btn-submit').click(function(e){
      e.preventDefault();
      $('.btn-submit').html('Please wait...').attr('disabled', true);
      var data  = new FormData($('.form-save')[0]);
      $.ajax({
        url: "{{ route('doRegistrasi') }}",
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false
      }).done(function(data){
        $('.form-save').validate(data, 'has-error');
        if(data.status == 'success'){
          swal({
            title : "Success !",
            text : data.message,
            type : "success",
            showConfirmButton: false
          });
          setTimeout(function awal() {
            window.location.href = "{{ route('dashboard') }}";
          }, 2000);
        } else if(data.status == 'error') {
          $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
          swal('Whoops !', data.message, 'warning');
        } else {
          var n = 0;
          for(key in data){
            if (n == 0) {var dt0 = key;}
            n++;
          }
          $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
          swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
        }
      }).fail(function() {
        swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
      });
    });

      @if(!empty(Session::get('message')))
        swal({
          title : "{{ Session::get('title') }}",
          text : "{{ Session::get('message') }}",
          type : "{{ Session::get('type') }}",
          showConfirmButton: true
        });
      @endif

      $('#status_pns').click(function(){
      $('.pns').show();
      });
      $('#status_pns').click(function(){
        $('.nonpns').hide();
      });
      $('#status_nonpns').click(function(){
      $('.nonpns').show();
      });
      $('#status_nonpns').click(function(){
        $('.pns').hide();
      });

    </script>
  </body>
</html>
