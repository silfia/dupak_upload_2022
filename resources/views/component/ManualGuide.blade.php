<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Manual Guide | e-DUPAK</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/')}}/assets/images/logo/bakti-husada.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="{{ url('/')}}/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/style.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/animate.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <style>
      html,
      body {
        height: 100%;
      }
      body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
      }
      * {font-family: Arial;}

        /* Style the tab */
        .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        }

        /* Style the buttons inside the tab */
        .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
        background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
        background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: white;
        }
        .navbar-collapse .in{
            display: block;
            height: 169px;
        }
    </style>
  </head>
  <body style="background-image: url(http://localhost/develop/sidoarjosip/public/img/bg_login/bg.png);">
    <div class="container" >
    <div class="row justify-content-center" style="margin-top: 100px;">
        <div class="col-md-10">
            
            <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'Admin')">Admin</button>
                <button class="tablinks" onclick="openCity(event, 'Penilai')">Tim Penilai</button>
                <!-- <button class="tablinks" onclick="openCity(event, 'Register')">Register</button> -->
                <button class="tablinks" onclick="openCity(event, 'Pemohon')">Pemohon / Pegawai</button>    
                <button type="button" onclick="window.location='{{ URL::route('login') }}'"><span class="fas fa-chevron-left"></span> Kembali</button>            
            </div>

            <div id="Admin" class="tabcontent">
                <h3>Manual Guide Admin</h3>
                <iframe src="{{ url('/')}}/upload/manualGuide/PEGAWAI.pdf" width="100%" height="550px"></iframe>
            </div>

            <div id="Penilai" class="tabcontent">
                <h3>Manual Guide Tim Penilai</h3>
                <iframe src="{{ url('/')}}/upload/manualGuide/PEGAWAI.pdf" width="100%" height="550px"></iframe>
            </div>

            <div id="Pemohon" class="tabcontent">
                <h3>Manual Guide Pemohon</h3>
                <iframe src="{{ url('/')}}/upload/manualGuide/PEGAWAI.pdf" width="100%" height="550px"></iframe>
            </div>

            <div id="Register" class="tabcontent">
                <h3>Registrasi Pengguna Baru</h3>
                <iframe src="{{ url('/')}}/upload/manualGuide/PEGAWAI.pdf" width="100%" height="550px"></iframe>
            </div>

        </div>
    </div>
</div>

<script>
document.getElementsByClassName("tabcontent")[0].style.display = "block";
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>
