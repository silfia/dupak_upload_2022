<div class="dashboard-header">
  <nav class="navbar navbar-expand-lg bg-white fixed-top">
    <a class="navbar-brand" href="{{ route('dashboard') }}">Target Kinerja Utama (TKU)</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto navbar-right-top">
        {{-- <li class="nav-item">
          <div id="custom-search" class="top-search-bar">
            <input class="form-control" type="text" placeholder="Search..">
          </div>
        </li> --}}
        @if (Auth::getUser()->level_user == '1')
        <li class="nav-item dropdown notification">
          
          <?php
              $jml_pem = count(App\Models\Users::where('is_banned','2')->get());
          ?>

          <?php if ($jml_pem > 0): ?>
          <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> Pemberitahuan<span class="indicator"></span></a>
          <?php else: ?>
          <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> Pemberitahuan</a>
          <?php endif ?>
          
          <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
            <li>


              <?php if ($jml_pem > 0): ?>
              <div class="notification-title">{{$jml_pem}} Daftar Pengguna Baru</div>
              <?php else: ?>
              <div class="notification-title">Tidak Ada Daftar Pengguna Baru</div>                
              <?php endif ?>
              <div class="notification-list">                
              <?php
                $PenggunaBaru = App\Models\Users::where('is_banned','2')->get();
              ?>
                <?php if (!empty($PenggunaBaru)): ?>
                  <?php foreach ($PenggunaBaru as $key): ?>                    
                    <div class="list-group">
                      <a href="{{route('userPegawai')}}" class="list-group-item list-group-item-action active">
                        <div class="notification-info">
                          <div class="notification-list-user-img">
                            @if($key->gender == 'Perempuan')
                            <img src="{!! url('assets/images/user_woman.png') !!}" alt="" class="user-avatar-md rounded-circle">
                            @else
                            <img src="{!! url('assets/images/user-man.png') !!}"  alt="" class="user-avatar-md rounded-circle">                            
                            @endif
                            </div>
                          <div class="notification-list-user-block"><span class="notification-list-user-name">{{$key->name}}, {{$key->email}}</span>Pengguna baru Belum Diaktivasi
                            <div class="notification-date">{{$key->created_at}}</div>
                          </div>
                        </div>
                      </a>                  
                    </div>
                  <?php endforeach ?>
                <?php else: ?>
                 <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                      <div class="notification-info">
                        <div class="notification-list-user-img"><img src="{!! url('assets/images/user-man.png') !!}" alt="" class="user-avatar-md rounded-circle"></div>
                        <div class="notification-list-user-block"><span class="notification-list-user-name"></span>Belum Ada Pengguna Baru
                          <div class="notification-date"></div>
                        </div>
                      </div>
                    </a>                  
                  </div>
                <?php endif ?>

              </div>
            </li>
            <!-- <li>
              <div class="list-footer"> <a href="#">View all Users</a></div>
            </li> -->
          </ul>
        </li>
        @endif
        <!--
          <li class="nav-item dropdown connection">
            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-fw fa-th"></i> </a>
            <ul class="dropdown-menu dropdown-menu-right connection-dropdown">
              <li class="connection-list">
                <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                    <a href="#" class="connection-item"><img src="assets/images/github.png" alt="" > <span>Github</span></a>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                    <a href="#" class="connection-item"><img src="assets/images/dribbble.png" alt="" > <span>Dribbble</span></a>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                    <a href="#" class="connection-item"><img src="assets/images/dropbox.png" alt="" > <span>Dropbox</span></a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                    <a href="#" class="connection-item"><img src="assets/images/bitbucket.png" alt=""> <span>Bitbucket</span></a>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                    <a href="#" class="connection-item"><img src="assets/images/mail_chimp.png" alt="" ><span>Mail chimp</span></a>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                    <a href="#" class="connection-item"><img src="assets/images/slack.png" alt="" > <span>Slack</span></a>
                  </div>
                </div>
              </li>
              <li>
                <div class="conntection-footer"><a href="#">More</a></div>
              </li>
            </ul>
          </li>
        -->
        <li class="nav-item dropdown nav-user">
          <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @if(Auth::getUser()->photo != null && is_file('upload/users/'.Auth::getUser()->photo))
							<img src="{!! url('upload/users/'.Auth::getUser()->photo) !!}" alt="" class="user-avatar-md rounded-circle">
						@else
							@if (Auth::getUser()->gender == 'Laki - Laki')
								<img src="{!! url('assets/images/user-man.png') !!}" alt="" class="user-avatar-md rounded-circle" style="background:white;">
              @elseif (Auth::getUser()->gender == 'Perempuan')
                <img src="{!! url('assets/images/user_woman.png') !!}" alt="" class="user-avatar-md rounded-circle" style="background:white;">
              @else
                <img src="{!! url('assets/images/user_health.png') !!}" alt="" class="user-avatar-md rounded-circle" style="background:white;">
							@endif
						@endif
          </a>
          <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
            <div class="nav-user-info">
              <h5 class="mb-0 text-white nav-user-name">{{ Auth::getUser()->name }}</h5>
              <span class="status"></span><span class="ml-2">Online</span>
            </div>
            <a class="dropdown-item" href="{{ route('profile') }}"><i class="fas fa-user mr-2"></i>Profil Pengguna</a>
            <a class="dropdown-item" href="{{ route('changePassword') }}"><i class="fas fa-key mr-2"></i>Ubah Password</a>
            <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-power-off mr-2"></i>Logout</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
</div>
