<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{ $data['title'] }} | Target Kinerja Utama (TKU)</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/')}}/assets/images/logo/bakti-husada.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="{{ url('/')}}/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/style.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/libs/css/animate.css">
    <link rel="stylesheet" href="{{ url('/')}}/assets/vendor/sweetalert/sweetalert.css">
		<script src="{{ url('/')}}/assets/vendor/sweetalert/sweetalert-dev.js"></script>
    <style>
      html,
      body {
        height: 100%;
      }
      body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
      }
      .txt-cpRght {
        text-align: center;
        padding-top: 10px;
        font-size: 11px;
      }
      .txt-vrs {
        text-align: center;
        font-size: 10px;
      }
      .splash-description {
        padding-bottom: 0px;
        margin-top: 15px;
      }
      .splash-container {
        max-width: 375px !important;
      }
      .iconEye{
        position:absolute;
      	right:10px;
      	top:12px;
      	font-size:18px;
      }
    </style>
  </head>
  <body>
  <div class="splash-container">
      <div class="card">
          <div class="card-header text-center">
            <a href="{{ route('dashboard') }}">
              <img class="logo-img" src="{{ url('/')}}/assets/images/logo/bakti-husada.png" alt="logo" height="150px">
            </a>
            <span class="splash-description">Selamat Datang di Aplikasi<br>Target Kinerja Utama (TKU)</span>
            <span>Login untuk melanjutkan Akses</span>
          </div>
          <div class="card-body">
              <form class="form-login">
                  <div class="form-group">
                      <input class="form-control form-control-lg" id="username" name="username" type="text" placeholder="NIP / NIK" autocomplete="off">
                  </div>
                  <div class="form-group">
                    <div class="col-12 p-0">
                      <input class="form-control form-control-lg" id="password" name="password" type="password" placeholder="Password">
                      <div class='iconEye'>
                        <a href="javascript:void(0)" onclick="showPassword()" id='eyePass'><i class="far fa-eye-slash"></i></a>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary btn-lg btn-block btn-login">Sign in</button>
                  <div class="txt-cpRght">©2019 Dinas Kesehatan Sidoarjo</div>
                  <div class="txt-vrs">versi 1.0.0</div>
              </form>
          </div>
          <div class="card-footer bg-white p-0">
            <div class="card-footer-item card-footer-item-bordered">
              <a href="{{ route('register') }}" class="footer-link"><i class="fas fa-fw fa-plus"></i> Buat Akun</a>
            </div>
            <div class="card-footer-item card-footer-item-bordered">
              <a href="{{ route('ManualGuide') }}" class="footer-link"><i class="fas fa-fw fa-book"></i> Manual Guide</a>
            </div>
          </div>
      </div>
  </div>

    <!-- Optional JavaScript -->
    <script src="{{ url('/')}}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="{{ url('/')}}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="{{ url('/')}}/assets/libs/js/animate.js"></script>
    <script src="{{ url('/')}}/assets/libs/js/validate.js"></script>
    <script type="text/javascript" src="{{ url('/')}}/assets/libs/js/jquery.backstretch.min.js"></script>
		<script>
			$.backstretch("{!! url('assets/images/background/Preview Image.jpg') !!}", {speed: 500});
		</script>
    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      function showPassword() {
        var tag = document.getElementById("password").getAttribute("type");
        if (tag == 'password') {
          $('#password').attr('type','text');
          $('#eyePass').html('<i class="fas fa-eye"></i>');
        }else{
          $('#password').attr('type','password');
          $('#eyePass').html('<i class="far fa-eye-slash"></i>');
        }
      }

      $('.form-login').submit(function(e){
        e.preventDefault();
        var button = $('.btn-login');
        var data = $('.form-login').serialize();
        button.html('Please wait...').attr('disabled', true);
        $.post("{!! route('doLogin') !!}", data).done(function(data){
          button.html('Login').removeAttr('disabled');
          $('.form-login').validate(data.result, 'has-error');
          if(data.status == 'success'){
            window.location.replace("{!! url($data['next_url']) !!}");
          } else if(data.status == 'error' || data.status == 'warning'){
            swal('Maaf', data.message, data.status);
          } else {
            button.animateCss('shake');
          }
        }).fail(function() {
          swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
          button.html('Login').removeAttr('disabled');
        });
      });

      @if(!empty(Session::get('message')))
        swal({
          title : "{{ Session::get('title') }}",
          text : "{{ Session::get('message') }}",
          type : "{{ Session::get('type') }}",
          showConfirmButton: true
        });
      @endif
    </script>
  </body>
</html>
