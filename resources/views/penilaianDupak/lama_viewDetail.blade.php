<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <h5 class="card-header bg-primary"><i class="fas fa-search-plus mr-2"></i> Detail Kegiatan Pegawai</h5>
  <div class="card-body">
    <div class="row">
      <input id="inputIdPenilaian" type="hidden" name="id_penilaian" value="{{ $penilaian->id_penilaian }}" class="form-control backWhite" readonly>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Jumlah Total</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $penilaian->jumlah_px_real }}" placeholder="Jumlah Pasien" class="form-control backWhite" readonly>
          </div>
        </div>
      </div>
      @if ($pegawai->status_pegawai == 'PNS')
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Jumlah Dupak</label>
            <div class="col-8 col-lg-9">
              <input type="text" value="{{ $penilaian->jumlah_px_pemohon }}" placeholder="Jumlah Point" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Jumlah Point</label>
            <div class="col-8 col-lg-9">
              <input type="text" value="{{ $penilaian->poin_pemohon }}" placeholder="Jumlah Point" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
      @endif
    </div>
    <table class="table table-striped table-bordered first">
      <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          @if ($pakMaster->satuan == 'Pasien' || $pakMaster->satuan == 'Jenazah')
            <th>NIK</th>
            <th>Inisial {{ $pakMaster->satuan }}</th>
            {{-- <th>Nama {{ $pakMaster->satuan }}</th> --}}
            <th>No RM</th>
            <th>No BPJS</th>
            <th>L/P</th>
            <th>Umur</th>
            <th>Alamat</th>
          @elseif ($pakMaster->satuan == 'Sertifikat')
            <th>Tanggal Pelaksanaan</th>
            <th>Tanggal Akhir Pelaksanaan</th>
            <th>Waktu Pelaksanaan</th>
            <th>Penyelenggara</th>
            <th>Nama {{ $pakMaster->satuan }}</th>
            <th>Nomor {{ $pakMaster->satuan }}</th>
          @elseif ($pakMaster->satuan == 'Ijazah')
            <th>Tanggal Lulus</th>
            <th>Universitas</th>
            <th>Jurusan</th>
            <th>Nomor {{ $pakMaster->satuan }}</th>
          @elseif($pakMaster->satuan == 'Laporan' || $pakMaster->satuan == 'Kasus' || $pakMaster->satuan == 'Kali' || $pakMaster->satuan == 'Buku' || $pakMaster->satuan == 'Karya' || $pakMaster->satuan == 'Makalah' || $pakMaster->satuan == 'Naskah')
            <?php
              if($pakMaster->satuan == 'Laporan'){ $tgl = 'Pembuatan'; $jdl = 'Laporan'; }
              elseif ($pakMaster->satuan == 'Kasus') { $tgl = 'Kasus'; $jdl = 'Kasus'; }
              elseif ($pakMaster->satuan == 'Kali') { $tgl = 'Kegiatan'; $jdl = 'Nama Kegiatan'; }
              else{ $tgl = $pakMaster->satuan; $jdl = 'Nama '.$pakMaster->satuan; }
            ?>
            <th>Tanggal {{ $tgl }}</th>
            <th>{{ $jdl }}</th>
            <th>Keterangan</th>
          @elseif($pakMaster->satuan == 'Jam' || $pakMaster->satuan == 'Jam Pelajaran')
            <th>Nama</th>
            <th>Gambar</th>
            <th>Keterangan</th>
          @endif
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; ?>
        @foreach ($detailKegiatan as $key)
          <tr>
            <td align="center">{{ $no }}</td>
            <td>{{ date('d-m-Y', strtotime($key->tanggal)) }}</td>
            @if ($pakMaster->satuan == 'Pasien' || $pakMaster->satuan == 'Jenazah')
              <td>{{ $key->nik }}</td>
              <td>{{ $key->inisial_pasien }}</td>
              {{-- <td>{{ $key->nama_pasien }}</td> --}}
              <td>{{ $key->no_rm }}</td>
              <td>{{ $key->no_bpjs }}</td>
              <td align="center">{{ $key->jenis_kelamin }}</td>
              <td align="center">{{ $key->umur }} Tahun</td>
              <td>{{ $key->alamat }}</td>
            @elseif ($pakMaster->satuan == 'Sertifikat')
              <td>{{ date('d-m-Y', strtotime($key->tgl_kegiatan)) }}</td>
              <td>{{ date('d-m-Y', strtotime($key->tgl_akhir_kegiatan)) }}</td>
              <td>{{ $key->waktu_kegiatan }}</td>
              <td>{{ $key->penyelenggara }}</td>
              <td>{{ $key->nama_pasien }}</td>
              <td>{{ $key->nomor }}</td>
            @elseif ($pakMaster->satuan == 'Ijazah')
              <td>{{ date('d-m-Y', strtotime($key->tgl_kegiatan)) }}</td>
              <td>{{ $key->penyelenggara }}</td>
              <td>{{ $key->nama_pasien }}</td>
              <td>{{ $key->nomor }}</td>
            @elseif($pakMaster->satuan == 'Laporan'|| $pakMaster->satuan == 'Kasus' || $pakMaster->satuan == 'Kali' || $pakMaster->satuan == 'Buku' || $pakMaster->satuan == 'Karya' || $pakMaster->satuan == 'Makalah' || $pakMaster->satuan == 'Naskah')
              <td>{{ date('d-m-Y', strtotime($key->tgl_kegiatan)) }}</td>
              <td>{{ $key->nama_pasien }}</td>
              <td>{{ $key->keterangan }}</td>
            @elseif($pakMaster->satuan == 'Jam' || $pakMaster->satuan == 'Jam Pelajaran')
              <td>{{ $key->nama_pasien }}</td>
              <td><img src="{{ url('upload/buktiFisik'.$key->gambar) }}" alt="" height="100px"></td>
              <td>{{ $key->keterangan }}</td>
            @endif
          </tr>
          <?php $no++; ?>
        @endforeach
      </tbody>
    </table>

    <div class="row pt-2 pt-sm-2 mt-1">
      <div class="col-sm-12 pl-0">
        <p class="text-right">
          <button class="btn btn-sm btn-space btn-secondary btn-backDetail"><span class="fas fa-chevron-left"></span> Kembali</button>
        </p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
  })();
  $('.btn-backDetail').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.panel-kegiatan').fadeOut(function(){
      $('.panel-kegiatan').empty();
      $('.panel-harian').fadeIn();
    });
  });
</script>
