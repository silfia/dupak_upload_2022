<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <h5 class="card-header bg-primary"><i class="fas fa-search-plus mr-2"></i> Detail Kegiatan Pegawai</h5>
  <div class="card-body">
    <div class="row">
      <input id="inputIdPenilaian" type="hidden" name="id_penilaian" value="{{ $penilaian->id_penilaian }}" class="form-control backWhite" readonly>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
        <div class="form-group row">
          <label class="col-4 col-lg-3 col-form-label">Jumlah Total</label>
          <div class="col-8 col-lg-9">
            <input type="text" value="{{ $penilaian->jumlah_px_real }}" placeholder="Jumlah Pasien" class="form-control backWhite" readonly>
          </div>
        </div>
      </div>
      @if ($pegawai->status_pegawai == 'PNS')
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Jumlah Dupak</label>
            <div class="col-8 col-lg-9">
              <input type="text" value="{{ $penilaian->jumlah_px_pemohon }}" placeholder="Jumlah Point" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-4 col-lg-3 col-form-label">Jumlah Point</label>
            <div class="col-8 col-lg-9">
              <input type="text" value="{{ $penilaian->poin_pemohon }}" placeholder="Jumlah Point" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
      @endif
    </div>
    @if($detailKegiatan->count()!=0)    
    <table class="table table-striped table-bordered first">
      <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          @foreach($bukti_fisik as $key)
          <th>{{$key->nama_bukti}}</th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; ?>
        @foreach($detailKegiatan as $key)
        <tr>
          <td>{{$no}}</td>
          <td>{{$key->tanggal}}</td>
          @foreach($bukti_fisik as $key1)
          <?php 
          $kolom = $key1->no_kolom;
          ?>
          <td>{{$key->$kolom}}</td>
          @endforeach
        </tr>
        <?php $no++; ?>
        @endforeach
      </tbody>
    </table>
    @else
      <h1>
        Tidak ada/ tanpa bukti fisik
      </h1>
    @endif

    <div class="row pt-2 pt-sm-2 mt-1">
      <div class="col-sm-12 pl-0">
        <p class="text-right">
          <button class="btn btn-sm btn-space btn-secondary btn-backDetail"><span class="fas fa-chevron-left"></span> Kembali</button>
        </p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
  })();
  $('.btn-backDetail').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.panel-kegiatan').fadeOut(function(){
      $('.panel-kegiatan').empty();
      $('.panel-harian').fadeIn();
    });
  });
</script>
