<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
			</div>
      <form class="formJumlahBaru">
        <div class="modal-body">
          <input id="inputIdPakMaster" type="hidden" name="idRiwayat" value="{{ $idRiwayat }}" class="form-control backWhite" readonly>
          <input id="inputIdPakMaster" type="hidden" name="idPakMaster" value="{{ $idPak }}" class="form-control backWhite" readonly>
          <input id="inputIdPengajuan" type="hidden" name="idPengajuan" value="{{ $idPengajuan }}" class="form-control backWhite" readonly>
          <input id="inputTahun" type="hidden" name="tahun" value="{{ $tahun }}" class="form-control backWhite" readonly>          
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah Total Awal</label>
            <div class="col-8 col-lg-9">
              <input id="inputJumlah" type="number" name="Jumlah_Awal" value="{{$jml_awal}}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah Total Akhir</label>
            <div class="col-8 col-lg-9">
              <input id="inputJumlah" type="number" name="Jumlah_Baru" placeholder="Jumlah Total Akhir" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Alasan Perubahan</label>
            <div class="col-8 col-lg-9">
              <textarea id="inputAlasan" type="text" name="alasan_perubahan" placeholder="Masukkan Alasan Perubahan Data" class="form-control" required="" rows="2"></textarea>              
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-chevron-left"></span> Batal</a>
          <a href="javascript:void(0)" class="btn btn-primary btn-submitJumlahBaru">Simpan <span class="fas fa-save"></span></a>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '70%'
    });
    $('#detail-dialog').modal('show');
  })();
  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  })

  $('.btn-submitJumlahBaru').click(function(e){
    e.preventDefault();
    $('.btn-submitJumlahBaru').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.formJumlahBaru')[0]);
    $.ajax({
      url: "{{ route('gantiNilaiEdit') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.formJumlahBaru').validate(data, 'has-error');
      if(data.status == 'success'){
        $('#detail-dialog').modal('toggle');
        $('.modal-backdrop').remove();
        $('.ganti-modal').html('').fadeIn();
        $('body').removeClass('modal-open');
        $('.layer_tahun').show(); 
        LoadTahun();  
      } else if(data.status == 'error') {
        $('.btn-submitJumlahBaru').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitJumlahBaru').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitJumlahBaru').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
