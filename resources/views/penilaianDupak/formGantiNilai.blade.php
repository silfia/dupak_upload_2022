<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
			</div>
      <form class="formJumlahBaru">
        <div class="modal-body">
          <input id="inputIdPakMaster" type="hidden" name="idPakMaster" value="{{ $idPak }}" class="form-control backWhite" readonly>
          <input id="inputIdPengajuan" type="hidden" name="idPengajuan" value="{{ $idPengajuan }}" class="form-control backWhite" readonly>
          <input id="inputTahun" type="hidden" name="tahun" value="{{ $tahun }}" class="form-control backWhite" readonly>
          <input id="inputBulan" type="hidden" name="bulan" value="{{ $bulan }}" class="form-control backWhite" readonly>
          <input id="inputTgl" type="hidden" name="tgl" value="{{ $tgl }}" class="form-control backWhite" readonly>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah Awal</label>
            <div class="col-8 col-lg-9">
              <input id="inputJumlah" type="number" name="Jumlah_Awal" value="{{ $penilaian->jumlah_px_pemohon }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah Baru</label>
            <div class="col-8 col-lg-9">
              <input id="inputJumlah" type="number" name="Jumlah_Baru" placeholder="Jumlah Dupak Baru" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Alasan Perubahan</label>
            <div class="col-8 col-lg-9">
              <input id="inputAlasan" type="text" name="alasan_perubahan" placeholder="Masukkan Alasan Perubahan Data" class="form-control" required="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-chevron-left"></span> Batal</a>
          <a href="javascript:void(0)" class="btn btn-primary btn-submitJumlahBaru">Simpan <span class="fas fa-save"></span></a>
          {{-- <a href="#" class="btn btn-primary" onclick="Kosong()">Simpan <span class="fas fa-save"></span></a> --}}
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '70%'
    });
    $('#detail-dialog').modal('show');
  })();
  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  })

  $('.btn-submitJumlahBaru').click(function(e){
    e.preventDefault();
    $('.btn-submitJumlahBaru').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.formJumlahBaru')[0]);
    $.ajax({
      url: "{{ route('gantiNilai') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.formJumlahBaru').validate(data, 'has-error');
      if(data.status == 'success'){
        $('#detail-dialog').modal('hide');
        swal("Berhasil !","Berhasil !!", "success");
        $('#penilaiHarian-'+data.data.penilaian.master_kegiatan_id+'_'+data.data.tanggal+'_jml').html(data.data.penilaian.jumlah_px_penilai);
        if (data.data.statusJumlah == 'Belum') {
          // var pemoh = data.data.penilaian.jumlah_px_pemohon+'<br><span style="color:red">[ '+data.data.penilaian.jumlah_px_penilai+' ]</span>';
          $('#pemohonHarian-'+data.data.penilaian.master_kegiatan_id+'_'+data.data.tanggal+'_jml').html(data.data.penilaian.jumlah_px_pemohon);
        }
        if (data.data.totDataCek > 0) {
          if (data.data.statusJumlah == 'Sudah') {
            var pxBl = data.data.pxAfterCek;
            var poBl = data.data.pointAfterCek;
            var poVBl = data.data.pxAfterCek;
          }else{
            var pxBl = data.data.pxPemohon;
            var poBl = data.data.pointPemohon;
            if (data.data.idAfterCek == 'Ada') {
              pxBl += '<br><span style="color:red">[ '+data.data.pxAfterCek+' ]</span>';
              poBl += '<br><span style="color:red">[ '+data.data.pointAfterCek+' ]</span>';
            }
            var poVBl = '';
            poVBl += '<span class="btnIsiNilai">';
            poVBl += '<a href="javascript:void(0)" onclick="detailHarian('+data.data.penilaian.master_kegiatan_id+','+data.data.pengajuan.id_pengajuan+','+data.data.tahun+','+data.data.bulan+')" class="btn btn-xs btn-primary" style="width:60px">Detail</a>';
            poVBl += '</span>';
          }
        }else{
          var pxBl = '-';
          var poBl = '-';
        }
        $('#jumlahHarian').html(pxBl);
        $('#jumlahAkHarian').html(poBl);
        $('#pemohonBulan-'+data.data.penilaian.master_kegiatan_id+'_'+data.data.bulan+'_jml').html(pxBl);
        $('#penilaiBulan-'+data.data.penilaian.master_kegiatan_id+'_'+data.data.bulan+'_jml').html(poVBl);
        if (data.data.statusAccTahun == 'Sudah') {
          var pxTH = data.data.thnPxAfterCek;
          var poTH = data.data.thnPointAfterCek;
          var pxPemohonTH = data.data.thnPxAfterCek;
        }else{
          var pxTH = data.data.thnPxbeforePemohon;
          var poTH = data.data.thnPobeforePemohon;
          if (data.data.thnIdAfterCek == 'Ada') {
            pxTH += '<br><span style="color:red">[ '+data.data.thnPxAfterCek+' ]</span>';
            poTH += '<br><span style="color:red">[ '+data.data.thnPointAfterCek+' ]</span>';
          }
          var pxPemohonTH = '';
          pxPemohonTH += '<span class="btnIsiNilai">';
          pxPemohonTH += '<a href="javascript:void(0)" onclick="sesuai('+data.data.penilaian.master_kegiatan_id+','+data.data.pengajuan.id_pengajuan+','+data.data.tahun+')" class="btn btn-xs btn-success" style="width:60px">Sesuai</a>';
          pxPemohonTH += '<a href="javascript:void(0)" onclick="detailBulan('+data.data.penilaian.master_kegiatan_id+','+data.data.pengajuan.id_pengajuan+','+data.data.tahun+')" class="btn btn-xs btn-primary" style="width:60px">Detail</a>';
          pxPemohonTH += '</span>';
        }
        $('#jumlahBulan').html(pxTH);
        $('#jumlahAkBln').html(poTH);
        $('#pemohon-'+data.data.penilaian.master_kegiatan_id+'_jml').html('<span class="btnIsiNilai">'+pxBl+'</span>');
        $('#penilai-'+data.data.penilaian.master_kegiatan_id+'_jml').html(pxPemohonTH);
        $('#totalAk-'+data.data.penilaian.master_kegiatan_id+'_jml').html(poTH);
      } else if(data.status == 'error') {
        $('.btn-submitJumlahBaru').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitJumlahBaru').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitJumlahBaru').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
