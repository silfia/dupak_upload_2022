@extends('component.layout')

@section('extended_css')
  <style media="screen">
    /* .panelIsiNilai {
      padding: 0px !important;
      position: relative !important;
    }
    .btnIsiNilai {
      height: 100% !important;
      width: 100% !important;
      position: absolute !important;
      top: 0 !important;
      justify-content: center !important;
      display: flex !important;
      align-items: center !important;
    }
    .spinner-secondary {
      border: 3px solid transparent !important;
      border-top: 3px solid #5969ff !important;
      border-left: 3px solid #5969ff !important;
    } */
    .txt-middle {
      vertical-align: middle !important;
    }

    .table-scroll {
      position: relative;
      width:100%;
      z-index: 1;
      margin: auto;
      overflow: auto;
      height: 450px;
    }
    .table-scroll table {
      width: 100%;
      min-width: 1280px;
      margin: auto;
      border-collapse: separate;
      border-spacing: 0;
      table-layout: fixed;
    }
    .table-tahun table {
      min-width: 860px !important;
    }
    .table-wrap {
      position: relative;
    }
    .table-scroll th,
    .table-scroll td {
      padding: 5px 10px;
      border: 1px solid #e6e6f2;
      vertical-align: top;
      background: #fff;
    }
    .table-scroll table tbody tr:nth-child(even) td, .table-scroll table tbody tr:nth-child(even) th {
      background-color: #f2f2f8 !important;
    }
    .table-scroll thead th, .table-scroll thead th:nth-child(2) {
      background: #9cb8e2;
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    }
    .table-scroll thead tr:nth-child(2) th {
      color: #fff;
      position: -webkit-sticky;
      position: sticky;
      top: 33px;
      left: inherit;
    }
    /* safari and ios need the tfoot itself to be position:sticky also */
    .table-scroll tfoot,
    .table-scroll tfoot th,
    .table-scroll tfoot td {
      position: -webkit-sticky;
      position: sticky;
      bottom: 0;
      background: #9cb8e2;
      color: #fff;
      z-index:4;
    }

    .table-scroll th:first-child, .table-scroll th:nth-child(2) {
      position: -webkit-sticky;
      position: sticky;
      left: 0;
      z-index: 4;
    }
    .level2, .level3, .level4, .level5, .level6 {
      position: -webkit-sticky !important;
      position: sticky !important;
      z-index: 4 !important;
    }
    .level2{ left: 80px; }
    .level3{ left: 120px; }
    .level4{ left: 160px; }
    .level5{ left: 200px; }
    .level6{ left: 240px; }
    .table-scroll th:nth-child(2){
      left: 40px;
    }
    .table-scroll thead th{
      z-index: 4;
    }
    .table-scroll thead th:first-child, .table-scroll thead th:nth-child(2),
    .table-scroll tfoot th:first-child, .table-scroll tfoot th:nth-child(2) {
      z-index: 5;
    }
    .table-scroll thead tr:nth-child(2) th:first-child, .table-scroll thead tr:nth-child(2) th:nth-child(2){
      z-index: 1;
    }
    .btnIsiKegiatan {
      height: 100% !important;
      width: 100% !important;
      position: absolute !important;
      top: 0 !important;
      justify-content: center !important;
      display: flex !important;
      align-items: center !important;
    }
  </style>
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12 col-lg-8 mb-2 mb-sm-2">
                @if(Auth::getUser()->level_user == 1)
                <button type="button" class="btn btn-xs btn-info btn-penilaian"><span class="fas fa-search"></span> Detail Nilai</button>
                @else
                <button type="button" class="btn btn-xs btn-info btn-penilaian"><span class="fas fa-search"></span> Beri Nilai</button>
                @endif
              </div>
              <div class="col-sm-12 col-lg-4 mb-2 mb-sm-2">
                <span style="float:right">
                  <input type="text" placeholder="Cari data" name="search-data" id="search-data" value="" class="textbox" autocomplete="off" aria-controls="idtables" onkeydown="if (event.keyCode == 13) {return searchData();}">
                </span>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-striped table-bordered first" id='loadData'></table>
                <div class="form-row mt-3">
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                    <select class="form-control" id="lngDt" style="width:60px;"></select>
                  </div>
                  <label class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2 text-center info-data"></label>
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                    <nav aria-label="Page navigation example" class="pl-right">
                      <ul class="pagination" id="paging"></ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
      <div class="col-12 second-page"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script src="{{ url('/')}}/js/cusLoadData.js"></script>
  <script type="text/javascript">
    var dtLoad = {
        url                 : "{!! route('loadPemohon') !!}",
        primaryField        : 'id_pengajuan',
        customFilter        : false,
        column              : [
          {field: 'nama', title: 'Nama', width: 350, align: 'left'},
          {field: 'no_nip', title: 'No NIP', width: 350, align: 'left'},
          {field: 'nama_jabatan', title: 'Jabatan', width: 350, align: 'left'},
          {field: 'nama_golongan', title: 'Golongan', width: 350, align: 'left'},
          {field: 'nama_satuan_kerja', title: 'Satuan Kerja', width: 350, align: 'left'},
          {field: 'tgl_awal_masa', title: 'Tgl Awal Masa', width: 350, align: 'left'},
          {field: 'tgl_akhir_masa', title: 'Tgl Akhir Masa', width: 350, align: 'left'},
          {field: 'Status', title: 'Status', width: 100, align: 'center',
              rowStyler: function(rowData, rowIndex) {
                return Status(rowData, rowIndex);
              }
          },
        ],
    }
    $(document).ready(function() {
      loadData(dtLoad, 1);
    });
    function searchData() { reloadData(1); }

    function Status(rowData, rowIndex) {
      var status = rowData.status;
			if (status == 'Selesai') {
        var tmp = "<div style='color:#fff;background-color:#449d44;border-color:#d43f3a;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;' > Selesai</div>";
      }else if (status == 'Proses') {
        var statusAcPenilai1 = rowData.status_penilai1;
        var statusAcPenilai2 = rowData.status_penilai2;
        var statusHasil = rowData.status_hasil;
        if (statusAcPenilai1 == 'B' && statusAcPenilai2 == 'B') {
          var tmp = '<div style="color:#fff;background-color:#f39c12 ;border-color:#d43f3a;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Proses</div>';
        }else{
        var tmp = '<div style="color:#fff;background-color:#9212f3 ;border-color:#9212f3;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Menunggu Penilai Lain</div>';
        }
			}else{
        var tmp = '<div style="color:#fff;background-color:#f39c12 ;border-color:#d43f3a;padding:2px 5px;font-size:12px;line-height:1.5;border-radius:3px;display: inline-block;" > Belum</div>';
			}
			return tmp;
    }

    $('.btn-penilaian').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('formPenilaian') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });
  </script>
@stop
