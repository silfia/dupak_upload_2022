<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 layer_harian p-0">
  <div class="card">
    <div class="card-header">
      <h5>Data Harian</h5>
    </div>
    <div class="card-body pr-10 pl-10">
      <div class="loadingHarian" align="center" style="display: none;margin-top:-120px">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
      <div id="table-scroll" class="table-scroll table-harian" style="height:auto">
        <?php
          $tangalAwal = (int) $tangalAwal;
          $jumlahHarian = $tanggalAkhir - $tangalAwal;
          $wtHarian = $jumlahHarian * 140;
          $panjangHarian = 40 + 440 + ($jumlahHarian * 140) + 140;
        ?>
        <table id="main-table" class="main-table" style="min-width : {{ $panjangHarian }}px;">
          <thead>
            <tr>
              <th rowspan="2" width="40px" class="text-center pr-0 pl-0">No</th>
              <th rowspan="2" colspan="11" width="440px">Butir Kegiatan</th>
              @for ($i=$tangalAwal; $i <= $tanggalAkhir; $i++)
                <th colspan="2" class="text-center" width="140px">{{ $i }}</th>
              @endfor
              <th rowspan="2" class="text-center" width="70px">Jumlah Total</th>
              <th rowspan="2" class="text-center" width="70px">Total AK</th>
            </tr>
            <tr>
              @for ($j=$tangalAwal; $j <= $tanggalAkhir; $j++)
                <th class="text-center" width="70px">Jumlah</th>
                <th class="text-center" width="70px">Akhir</th>
              @endfor
            </tr>
          </thead>
          <tbody>
            @for ($b=count($butir); $b > 0; $b--)
              <?php $n = $b-1; ?>
              <tr>
                @if ($butir[$n]->level == 1)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px">I</th>
                    <th colspan="11" width="440px">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px">I</th>
                    <th colspan="11" width="440px">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 2)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px">A</th>
                    <th class="level2" width="400px" colspan="10">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px">A</th>
                    <th class="level2" width="400px" colspan="10">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 3)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px">1.</th>
                    <th class="level3" width="360px" colspan="9">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px">1.</th>
                    <th class="level3" width="360px" colspan="9">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 4)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px">a.</th>
                    <th class="level4" width="320px" colspan="8">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px">a.</th>
                    <th class="level4" width="320px" colspan="8">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 5)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"><i class="fas fa-angle-double-right"></i>.</th>
                    <th class="level5" width="280px" colspan="7">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"><i class="fas fa-angle-double-right"></i>.</th>
                    <th class="level5" width="280px" colspan="7">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 6)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"></th>
                    <th class="level5 text-center" width="40px"><i class="fas fa-terminal"></i>.</th>
                    <th class="level6" width="240px" colspan="6">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"></th>
                    <th class="level5 text-center" width="40px"><i class="fas fa-terminal"></i>.</th>
                    <th class="level6" width="240px" colspan="6">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @endif
                @if ($butir[$n]->is_title == '1')
                  <td colspan="{{ ($jumlahHarian + 1) * 2 }}"></td>
                  <th class="text-center" width="70px"></th>
                  <th class="text-center" width="70px"></th>
                @else
                  @for ($m=$tangalAwal; $m <= $tanggalAkhir; $m++)
                    <td id="pemohonHarian-{{ $butir[$n]->id_pak_master }}_{{$m}}_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0" style="position: relative;">
                      @if ($nilai[$m]['statusData'] == 'Ada')
                        <a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan({{ $nilai[$m]['penilaian']['id_penilaian'] }})">{{ $nilai[$m]['penilaian']['jumlah_px_pemohon'] }}@if ($nilai[$m]['statusPenilaian'] == 'Belum')<br><span style="color:red">[ {{ $nilai[$m]['penilaian']['jumlah_px_penilai'] }} ]</span>@endif
                        </a>
                      @else
                        0
                      @endif
                    </td>
                    <td id="penilaiHarian-{{ $butir[$n]->id_pak_master }}_{{$m}}_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">
                      @if ($nilai[$m]['statusData'] == 'Ada')
                        @if ($nilai[$m]['statusPenilaian'] == 'Selesai')
                          {{ $nilai[$m]['penilaian']['jumlah_px_penilai'] }}
                        @else
                          <span class="btnIsiNilai">
                            <a href="javascript:void(0)" onclick="sesuaiHarian({{ $butir[$n]->id_pak_master }},{{ $pengajuan->id_pengajuan }},{{ $tahun }},{{ $bulan }},{{ $m }})" class="btn btn-xs btn-success" style="width:60px">Sesuai</a>
                            <a href="javascript:void(0)" onclick="gantiNilai({{ $butir[$n]->id_pak_master }},{{ $pengajuan->id_pengajuan }},{{ $tahun }},{{ $bulan }},{{ $m }})" class="btn btn-xs btn-warning" style="width:60px">Ganti</a>
                          </span>
                        @endif
                      @else
                        0
                      @endif
                    </td>
                  @endfor
                  <th id="jumlahHarian" class="panelIsiNilai text-center txt-middle" width="70px">
                    @if ($statusBulan == 'Selesai')
                      {{ $nilaiPenilaiBulan['jumlah_px'] }}
                    @else
                      {{ $nilaiPemohonBulan['jumlah_px_pemohon'] }}
                      @if ($statusBulan == 'Belum')
                        <br><span style="color:red">[ {{ $nilaiPenilaiBulan['jumlah_px'] }} ]</span>
                      @endif
                    @endif
                  </th>
                  <th id="jumlahAkHarian" class="panelIsiNilai text-center txt-middle" width="70px">
                    @if ($statusBulan == 'Selesai')
                      {{ $nilaiPenilaiBulan['jumlah_point'] }}
                    @else
                      {{ $nilaiPemohonBulan['poin_pemohon'] }}
                      @if ($statusBulan == 'Belum')
                        <br><span style="color:red">[ {{ $nilaiPenilaiBulan['jumlah_point'] }} ]</span>
                      @endif
                    @endif
                  </th>
                @endif
              </tr>
            @endfor
          </tbody>
        </table>
      </div>
      <div class="col-lg-12 col-md-12 text-right pt-3">
        <button type="button" class="btn btn-xs btn-secondary btn-cancelHrn"><span class="fas fa-chevron-left"></span> Kembali</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.btn-cancelHrn').click(function(e){
    e.preventDefault();
    $('.layer_harian').animateCss('bounceOutDown');
    $('.panel-harian').fadeOut(function(){
      $('.panel-harian').empty();
      $('.layer_bulan').fadeIn();
    });
  });

  function sesuaiHarian(idPak, idPengajuan,tahun, bulan, tgl) {
    $.post("{{route('accNilaiHarian')}}",{idPak:idPak,idPengajuan:idPengajuan,tahun:tahun,bulan:bulan,tgl:tgl},function(result){
      if(result.status=='success'){
        swal("Berhasil !","Berhasil !!", "success");
        $('#pemohonHarian-'+idPak+'_'+tgl+'_jml').html(result.data.dataPenilaian.jumlah_px_pemohon); // Pada pemohon Form Harian
        $('#penilaiHarian-'+idPak+'_'+tgl+'_jml').html(result.data.dataPenilaian.jumlah_px_penilai); // Pada Penilai Form Harian
        var pxBulan = '';
        if (result.data.dataBulan.statusBulan == 'Selesai') {
          pxBulan += result.data.dataBulan.nilaiPenilaiBulan.jumlah_px;
        }else{
          pxBulan += result.data.dataBulan.nilaiPemohonBulan.jumlah_px_pemohon;
          if (result.data.dataBulan.statusBulan == 'Belum') {
            pxBulan += '<br><span style="color:red">[ '+result.data.dataBulan.nilaiPenilaiBulan.jumlah_px+' ]</span>';
          }
        }
        $('#jumlahHarian').html(pxBulan); // Pada jumlah form Harian
        $('#pemohonBulan-'+idPak+'_'+bulan+'_jml').html(pxBulan); // Pada pemohon form Bulanan

        if (result.data.dataBulan.statusBulan == 'Selesai') {
          var pxPenilaiBulan = result.data.dataBulan.nilaiPenilaiBulan.jumlah_px;
          $('#penilaiBulan-'+idPak+'_'+bulan+'_jml').html(pxPenilaiBulan); // Pada Penilai form Bulanan
        }

        var pointBulan = '';
        if (result.data.dataBulan.statusBulan == 'Selesai') {
          pointBulan += result.data.dataBulan.nilaiPenilaiBulan.jumlah_point;
        }else{
          pointBulan += result.data.dataBulan.nilaiPemohonBulan.poin_pemohon;
          if (result.data.dataBulan.statusBulan == 'Belum') {
            pointBulan += '<br><span style="color:red">[ '+result.data.dataBulan.nilaiPenilaiBulan.jumlah_point+' ]</span>';
          }
        }
        $('#jumlahAkHarian').html(pointBulan); // Pada AK form Harian

        var pxTahun = '';
        if (result.data.dataTahun.statusTahun == 'Selesai') {
          pxTahun += result.data.dataTahun.nilaiPenilaiTahun.jumlah_px;
        }else{
          pxTahun += result.data.dataTahun.nilaiPemohonTahun.jumlah_px_pemohon;
          if (result.data.dataTahun.statusTahun == 'Belum') {
            pxTahun += '<br><span style="color:red">[ '+result.data.dataTahun.nilaiPenilaiTahun.jumlah_px+' ]</span>';
          }
        }
        $('#pemohonTahunan-'+idPak+'_jml').html(pxTahun); // Pemohon Form Tahunan
        $('#jumlahBulan').html(pxTahun); // Pada Jumlah form Bulanan

        if (result.data.dataTahun.statusTahun == 'Selesai') {
          var pxPenilaiTahun = result.data.dataTahun.nilaiPenilaiTahun.jumlah_px;
          $('#penilaiTahunan-'+idPak+'_jml').html(pxPenilaiTahun); // Penilai Form Tahunan
        }

        var pointTahun = '';
        if (result.data.dataTahun.statusTahun == 'Selesai') {
          pointTahun += result.data.dataTahun.nilaiPenilaiTahun.jumlah_point;
        }else{
          pointTahun += result.data.dataTahun.nilaiPemohonTahun.poin_pemohon;
          if (result.data.dataTahun.statusTahun == 'Belum') {
            pointTahun += '<br><span style="color:red">[ '+result.data.dataTahun.nilaiPenilaiTahun.jumlah_point+' ]</span>';
          }
        }
        $('#totalAkTahunan-'+idPak+'_jml').html(pointTahun); // Pada AK form Harian
        $('#jumlahAkBln').html(pointTahun); // Pada AK form Bulan
      }else{
        swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      }
    });
  }

  function gantiNilai(idPak, idPengajuan,tahun, bulan, tgl) {
    $.post("{{route('formGantiNilai')}}",{idPak:idPak,idPengajuan:idPengajuan,tahun:tahun,bulan:bulan,tgl:tgl},function(data){
      if(data.status=='success'){
        $('.ganti-modal').html(data.content);
      }else{
        $('.ganti-modal').html('');
      }
    });
  }

  function detailKegiatan(idPenilaian) {
      $('.loadingForm').show();
      $('.panel-harian').hide();
      $.post("{!! route('detailKegiatanPenilaianHarian') !!}", {idPenilaian:idPenilaian}).done(function(data){
        if (data.status == 'success') {
          $('.loadingForm').hide();
          $('.panel-kegiatan').html(data.content).fadeIn();
        }else{
          swal("MAAF !","Terjadi Kesalahan !!", "warning");
        }
      });
    }
</script>
