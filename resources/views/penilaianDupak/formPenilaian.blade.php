<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Penilaian</h5>
  <div class="card-body">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputNoNip" class="col-4 col-lg-3 col-form-label">No NIP</label>
            <div class="col-8 col-lg-9">
              <input id="inputNoNip" type="text" value="{{ $pengajuan->user->pegawai->no_nip }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama</label>
            <div class="col-8 col-lg-9">
              <input id="inputNama" type="text" value="{{ $pengajuan->user->pegawai->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputSaker" class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
            <div class="col-8 col-lg-9">
              <input id="inputSaker" type="text" value="{{ $pengajuan->user->pegawai->satuanKerja->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputGolongan" class="col-4 col-lg-3 col-form-label">Golongan</label>
            <div class="col-8 col-lg-9">
              <input id="inputGolongan" type="text" value="{{ $pengajuan->user->pegawai->golongan->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
            <div class="col-8 col-lg-9">
              <input id="inputJabatan" type="text" value="{{ $pengajuan->user->pegawai->jabatan->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
            <div class="col-8 col-lg-9">
              <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->typeProfesi->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputPenilai1" class="col-4 col-lg-3 col-form-label">Penilai 1</label>
            <div class="col-8 col-lg-9">
              <input id="inputPenilai1" type="text" value="@if($pengajuan->penilai_id != null){{ $pengajuan->penilai->nama }}@else - @endif" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputPenilai2" class="col-4 col-lg-3 col-form-label">Penilai 2</label>
            <div class="col-8 col-lg-9">
              <input id="inputPenilai2" type="text" value="@if($pengajuan->penilai2_id != null){{ $pengajuan->penilai2->nama }}@else - @endif" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>        

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="loadingForm" align="center" style="display: none;margin-top:-120px">
            <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
          </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 layer_tahun">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-6 col-lg-6">
                  <h5></h5>
                </div>
                <div class="col-6 col-lg-6">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <!-- <th>Total AK Awal</th> -->
                        <th>Total Angka Kredit</th>
                        <!-- <th>Jumlah Awal</th> -->
                        <!-- <th>Jumlah Akhir</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <!-- <td>{{$penilaian->ak_awal}}</td> -->
                        <td><span id="totalAK">{{$penilaian->ak_akhir}}</span></td>                
                        <!-- <td>{{$penilaian->jumlah_awal}}</td> -->
                        <!-- <td>{{$penilaian->jumlah_akhir}}</td> -->
                      </tr>
                    </tbody>
                  </table>
                </div>    
              </div>
            </div>
            <div class="card-header">
              <div class="row">
                <div class="col-3 col-lg-2">
                  <h5>Data Tahunan</h5>
                </div>
                <div class="col-4 col-lg-4">
                  <div class="form-group row pt-0">
                    <label for="tahun" class="col-4 col-lg-3 col-form-label text-right p-0">Tahun</label>
                    <div class="col-8 col-lg-9">
                      <select class="form-control" name="tahun" id="tahun">
                        @foreach ($tahun as $thn)
                          <option value="{{ $thn }}">{{ $thn }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-2 col-lg-2">
                  <div class="form-group row">
                    <button type="button" class="btn btn-xs btn-primary btn-gantiTahun"><span class="fas fa-search"></span> Cari</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="loadingTahun" align="center" style="display: none;margin-top:-120px">
                <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
              </div>
              <div id="table-scroll" class="table-scroll table-tahun"></div>
             <!--  {{$cek_button_sesuaikan}}
              @if(!empty($cek_button_sesuaikan))
              <a href="javascript:void(0)" onclick="sesuaikanSemua({{$pengajuan->id_pengajuan}})" class="btn btn-xs btn-success mt-2 float-right" style="width:150px">Sesuaikan Semua</a>
              @endif -->
            </div>
          </div>
        </div>
        <span id='idPaks' style="display:none"></span>
        <input type="hidden" name="thnSelect" value="{{ $tahun[0] }}" id='thnSelect'>
        <input type="hidden" name="runTahun" value="Stop" id='runTahun'>
        <div class="col-12 panel-bulan"></div>
        <div class="col-12 panel-harian"></div>
        <div class="col-12 panel-kegiatan"></div>
        <div class="col-12 ganti-modal"></div>

        <div class="col-lg-12 col-md-12 text-right tombolUtama">
          <button type="button" class="btn btn-xs btn-secondary btn-cancelLB"><span class="fas fa-chevron-left"></span> Kembali</button>
          @if ($statusTmbSelesai == 'Ya')
            <button type="button" class="btn btn-xs btn-primary btn-savePenilaian"><span class="fas fa-save"></span> Selesai</button>
          @endif
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelLB').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  $('#tahun').chosen();
  $(document).ready(function() {
    LoadTahun();
  });

  $('.btn-gantiTahun').click(function(){
    $('#runTahun').val('Stop');
    var ganti = $('#tahun').val();
    console.log(ganti);
    $('#thnSelect').val(ganti);
    LoadTahun();
  });

  function LoadTahun() {
    var idPak = '';
    var tag = '';
    $('.loadingTahun').show();
    // var spiner = '<span class="dashboard-spinner spinner-secondary spinner-xs"></span>';
    var spiner = '';
    var id_pegawai = {{ $pegawai->id_pegawai }};
    $.post("{{route('loadPenilaianTahun')}}",{id_pegawai:id_pegawai},function(data){
      if(data.status=='success'){
        $('.loadingTahun').hide();
        tag += '<table id="main-table" class="main-table">';
        tag += '<thead>';
        tag += '<tr>';
        tag += '<th width="40px" class="text-center">No</th>';
        tag += '<th colspan="15" width="600px">Butir Kegiatan</th>';
        tag += '<th class="text-center" width="70px">Jumlah Pemohon</th>';
        tag += '<th class="text-center" width="70px">Jumlah Akhir</th>';
        tag += '<th class="text-center" width="70px">Total AK</th>';
        tag += '</tr>';
        tag += '</thead>';
        tag += '<tbody>';
        if (data.row.butir.length != 0) {
          $.each(data.row.butir, function(k,uns){
            tag += '<tr>';
            tag += '<th width="40px"></th>';
            tag += '<th colspan="15" width="600px">'+uns.nama+'</th>';
            tag += '<td colspan="3"></td>';
            tag += '</tr>';
            if (uns.row.length > 0) {
              var tempNo1 = 0;
              $.each(uns.row, function(k,v1){
                tempNo1++;
                if (v1.statusRow == 'Parent') {
                  tag += '<tr>';
                  tag += '<th class="text-center" width="40px">'+KonDecRomawi(tempNo1)+'.</th>';
                  tag += '<th colspan="15" width="600px">'+v1.butir_kegiatan+'</th>';
                  tag += '<td colspan="3"></td>';
                  tag += '</tr>';
                  if (v1.row.length > 0) {
                    var tempNo2 = 0; // Hurug Awal
                    var tempNo2n = 0; // Hurug Akhir
                    var No2 = '';
                    var No2n = '';
                    $.each(v1.row, function(k,v2){
                      if (tempNo2n == 26) {
                        tempNo2++;
                        No2 = String.fromCharCode(64 + tempNo2);
                      }
                      tempNo2n++;
                      No2n = String.fromCharCode(64 + tempNo2n);
                      if (v2.statusRow == 'Parent') {
                        tag += '<tr>';
                        tag += '<th width="40px"></th>';
                        tag += '<th class="text-center" width="40px">'+No2+''+No2n+'.</th>';
                        tag += '<th class="level2" width="560px" colspan="14">'+v2.butir_kegiatan+'</th>';
                        tag += '<td colspan="3"></td>';
                        tag += '</tr>';
                        if (v2.row.length > 0) {
                          var tempNo3 = 0;
                          $.each(v2.row, function(k,v3){
                            tempNo3++;
                            if (v3.statusRow == 'Parent') {
                              tag += '<tr>';
                              tag += '<th width="40px"></th>';
                              tag += '<th width="40px"></th>';
                              tag += '<th class="level2 text-center" width="40px">'+tempNo3+'.</th>';
                              tag += '<th class="level3" width="520px" colspan="13">'+v3.butir_kegiatan+'</th>';
                              tag += '<td colspan="3"></td>';
                              tag += '</tr>';
                              if (v3.row.length > 0) {
                                var tempNo4 = 0; // Hurug Awal
                                var tempNo4n = 0; // Hurug Akhir
                                var No4 = '';
                                var No4n = '';
                                $.each(v3.row, function(k,v4){
                                  if (tempNo4n == 26) {
                                    tempNo4++;
                                    No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                  }
                                  tempNo4n++;
                                  No4n = String.fromCharCode(64 + tempNo4n).toLowerCase();
                                  if (v4.statusRow == 'Parent') {
                                    tag += '<tr>';
                                    tag += '<th width="40px"></th>';
                                    tag += '<th width="40px"></th>';
                                    tag += '<th class="level2" width="40px"></th>';
                                    tag += '<th class="level3 text-center" width="40px">'+No4+''+No4n+'.</th>';
                                    tag += '<th class="level4" width="480px" colspan="12">'+v4.butir_kegiatan+'</th>';
                                    tag += '<td colspan="3"></td>';
                                    tag += '</tr>';
                                    if (v4.row.length > 0) {
                                      var No5 = '<i class="fas fa-angle-double-right"></i>';
                                      $.each(v4.row, function(k,v5){
                                        if (v5.statusRow == 'Parent') {
                                          tag += '<tr>';
                                          tag += '<th width="40px"></th>';
                                          tag += '<th width="40px"></th>';
                                          tag += '<th class="level2" width="40px"></th>';
                                          tag += '<th class="level3" width="40px"></th>';
                                          tag += '<th class="level4 text-center" width="40px">'+No5+'.</th>';
                                          tag += '<th class="level5" width="440px" colspan="11">'+v5.butir_kegiatan+'</th>';
                                          tag += '<td colspan="3"></td>';
                                          tag += '</tr>';
                                          if (v5.row.length > 0) {
                                            var No6 = '<i class="fas fa-terminal"></i>';
                                            $.each(v5.row, function(k,v6){
                                              if (v6.statusRow == 'Parent') {
                                                tag += '<tr>';
                                                tag += '<th width="40px"></th>';
                                                tag += '<th width="40px"></th>';
                                                tag += '<th class="level2" width="40px"></th>';
                                                tag += '<th class="level3" width="40px"></th>';
                                                tag += '<th class="level4" width="40px"></th>';
                                                tag += '<th class="level5 text-center" width="40px">'+No6+'.</th>';
                                                tag += '<th class="level6" width="400px" colspan="10">'+v6.butir_kegiatan+'</th>';
                                                tag += '<td colspan="3"></td>';
                                                tag += '</tr>';
                                              }else{
                                                tag += '<tr>';
                                                tag += '<th width="40px"></th>';
                                                tag += '<th width="40px"></th>';
                                                tag += '<th class="level2" width="40px"></th>';
                                                tag += '<th class="level3" width="40px"></th>';
                                                tag += '<th class="level4" width="40px"></th>';
                                                tag += '<th class="level5 text-center" width="40px">'+No6+'.</th>';
                                                tag += '<th class="level6" width="400px" colspan="10">'+v6.butir_kegiatan+'<br>( Tiap '+v6.jum_min+' '+v6.satuan+' '+v6.points+' ) <span style="float:right;">'+v6.jabatan.nama+'</th>';
                                                if (idPak == '') { idPak = v6.id_pak_master; }
                                                else{ idPak = idPak+","+v6.id_pak_master; }
                                                tag += '<td id="pemohonTahunan-'+v6.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                                tag += '<td id="penilaiTahunan-'+v6.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                                tag += '<td id="totalAkTahunan-'+v6.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                                tag += '</tr>';
                                              }
                                            });
                                          }
                                        }else{
                                          tag += '<tr>';
                                          tag += '<th width="40px"></th>';
                                          tag += '<th width="40px"></th>';
                                          tag += '<th class="level2" width="40px"></th>';
                                          tag += '<th class="level3" width="40px"></th>';
                                          tag += '<th class="level4 text-center" width="40px">'+No5+'.</th>';
                                          tag += '<th class="level5" width="440px" colspan="11">'+v5.butir_kegiatan+'<br>( Tiap '+v5.jum_min+' '+v5.satuan+' '+v5.points+' ) <span style="float:right;">'+v5.jabatan.nama+'</th>';
                                          if (idPak == '') { idPak = v5.id_pak_master; }
                                          else{ idPak = idPak+","+v5.id_pak_master; }
                                          tag += '<td id="pemohonTahunan-'+v5.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                          tag += '<td id="penilaiTahunan-'+v5.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                          tag += '<td id="totalAkTahunan-'+v5.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                          tag += '</tr>';
                                        }
                                      });
                                    }
                                  }else{
                                    tag += '<tr>';
                                    tag += '<th width="40px"></th>';
                                    tag += '<th width="40px"></th>';
                                    tag += '<th class="level2" width="40px"></th>';
                                    tag += '<th class="level3 text-center" width="40px">'+No4+''+No4n+'.</th>';
                                    tag += '<th class="level4" width="480px" colspan="12">'+v4.butir_kegiatan+'<br>( Tiap '+v4.jum_min+' '+v4.satuan+' '+v4.points+' ) <span style="float:right;">'+v4.jabatan.nama+'</th>';
                                    if (idPak == '') { idPak = v4.id_pak_master; }
                                    else{ idPak = idPak+","+v4.id_pak_master; }
                                    tag += '<td id="pemohonTahunan-'+v4.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                    tag += '<td id="penilaiTahunan-'+v4.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                    tag += '<td id="totalAkTahunan-'+v4.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                                    tag += '</tr>';
                                  }
                                });
                              }
                            }else{
                              tag += '<tr>';
                              tag += '<th width="40px"></th>';
                              tag += '<th width="40px"></th>';
                              tag += '<th class="level2 text-center" width="40px">'+tempNo3+'.</th>';
                              tag += '<th class="level3" width="520px" colspan="13">'+v3.butir_kegiatan+'<br>( Tiap '+v3.jum_min+' '+v3.satuan+' '+v3.points+' ) <span style="float:right;">'+v3.jabatan.nama+'</th>';
                              if (idPak == '') { idPak = v3.id_pak_master; }
                              else{ idPak = idPak+","+v3.id_pak_master; }
                              tag += '<td id="pemohonTahunan-'+v3.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                              tag += '<td id="penilaiTahunan-'+v3.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                              tag += '<td id="totalAkTahunan-'+v3.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                              tag += '</tr>';
                            }
                          });
                        }
                      }else{
                        tag += '<tr>';
                        tag += '<th width="40px"></th>';
                        tag += '<th class="text-center" width="40px">'+No2+''+No2n+'.</th>';
                        tag += '<th class="level2" width="560px" colspan="14">'+v2.butir_kegiatan+'<br>( Tiap '+v2.jum_min+' '+v2.satuan+' '+v2.points+' ) <span style="float:right;">'+v2.jabatan.nama+'</th>';
                        if (idPak == '') { idPak = v2.id_pak_master; }
                        else{ idPak = idPak+","+v2.id_pak_master; }
                        tag += '<td id="pemohonTahunan-'+v2.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                        tag += '<td id="penilaiTahunan-'+v2.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                        tag += '<td id="totalAkTahunan-'+v2.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                        tag += '</tr>';
                      }
                    });
                  }
                }else{
                  tag += '<tr>';
                  tag += '<th class="text-center" width="40px">'+KonDecRomawi(tempNo1)+'.</th>';
                  tag += '<th colspan="15" width="600px">'+v1.butir_kegiatan+'<br>( Tiap '+v1.jum_min+' '+v1.satuan+' '+v1.points+' ) <span style="float:right;">'+v1.jabatan.nama+'</th>';
                  if (idPak == '') { idPak = v1.id_pak_master; }
                  else{ idPak = idPak+","+v1.id_pak_master; }
                  tag += '<td id="pemohonTahunan-'+v1.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                  tag += '<td id="penilaiTahunan-'+v1.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                  tag += '<td id="totalAkTahunan-'+v1.id_pak_master+'_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">'+spiner+'</td>';
                  tag += '</tr>';
                }
              });
            }
          });
        }else{
          tag += '<tr><th colspan="19" class="text-center">Tidak ada data ditemukan...</th></tr>';
        }
        tag += '</tbody>';
        tag += '</table>';
        $('.table-tahun').empty();
        $('.table-tahun').html(tag);
        $('#idPaks').html('');
        $('#idPaks').html(idPak);
        $('#runTahun').val('Running');
        LoadGrid();
      }else{
        swal("MAAF !","Terjadi Kesalahan !!", "warning");
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      LoadTahun();
    });
  }

  function LoadGrid() {
    var arrayId = $('#idPaks').text().split(',');
    // console.log(arrayId);
    var totalArrayIdPak = arrayId.length;
    var tempIdPak = 0
    var idPengajuan = {{ $pengajuan->id_pengajuan }};
    $('#runTahun').val('Running');
    function valGrid(tempIdPak){
      console.log(tempIdPak);
      var statusRun = $('#runTahun').val();
      if (statusRun == 'Running') {
        if (tempIdPak >= totalArrayIdPak) {
          console.log('berhenti');
          return ;
        }
      }else{
        console.log('Ganti');
        return ;
      }
      var tahun = $('#thnSelect').val();
      return $.ajax({
        url: "{{ route('getNilaiPemohon') }}?idPengajuan="+idPengajuan+"&idPakMaster="+arrayId[tempIdPak]+"&tahun="+tahun, success: function(result){          
          // console.log('result');
          // console.log(result);
          if (result.status == 'success') {

            $('#totalAK').html(result.row.penilaian.ak_akhir);
            
            if (result.row.nilaiPemohon.jumlah_px_pemohon == null) {
              var nilaiPemohon = 0;
            }else{
              var nilaiPemohon = result.row.nilaiPemohon.jumlah_px_pemohon;
            }
            var isiPemohon = '';
            isiPemohon += '<span class="btnIsiNilai">'+nilaiPemohon;
            if (result.row.statusData == 'Ada') {
              if (result.row.statusPenilaian == 'Belum') {
                isiPemohon += '<br><span style="color:red">[ '+result.row.nilaiPenilai.jumlah_px+' ]</span>';
              }
            }
            isiPemohon += '</span>';
            $('#pemohonTahunan-'+arrayId[tempIdPak]+'_jml').html(isiPemohon);            

            var isiPenilai = '';
            if (result.row.statusData == 'Ada' || result.row.statusData == 'Admin') {
              if (result.row.statusPenilaian == 'Selesai') {
                isiPenilai += result.row.nilaiPenilai.jumlah_px;
              }else{
                isiPenilai += '<span class="btnIsiNilai">';
                if (result.row.statusData != 'Admin') {
                  isiPenilai += '<a href="javascript:void(0)" onclick="sesuaiTahunan('+arrayId[tempIdPak]+','+idPengajuan+','+tahun+')" class="btn btn-xs btn-success" style="width:60px">Sesuai</a>';
                }
                // isiPenilai += '<a href="javascript:void(0)" onclick="detailBulan('+arrayId[tempIdPak]+','+idPengajuan+','+tahun+')" class="btn btn-xs btn-info" style="width:60px">Detail</a>';
                isiPenilai += '<a href="javascript:void(0)" onclick="EditNilai('+arrayId[tempIdPak]+','+idPengajuan+','+tahun+','+result.row.nilaiPemohon.jumlah_px_pemohon+')" class="btn btn-xs btn-primary" style="width:60px">Edit</a>';
                isiPenilai += '</span>';
              }
            }else {
              isiPenilai += '0';
            }
            $('#penilaiTahunan-'+arrayId[tempIdPak]+'_jml').html(isiPenilai);

            var isiTotal = '';
            if (result.row.statusData == 'Ada') {
              if (result.row.statusPenilaian == 'Selesai') {
                isiTotal += result.row.nilaiPenilai.jumlah_point;
              }else{
                isiTotal += result.row.nilaiPemohon.poin_pemohon;
                if (result.row.statusPenilaian == 'Belum') {
                  isiTotal += '<br><span style="color:red">[ '+result.row.nilaiPenilai.jumlah_px+' ]</span>';
                }
              }
            }else if (result.row.statusData == 'Admin'){
              if (result.row.statusPenilaian == 'Selesai') {
                isiTotal += result.row.nilaiPenilai.jumlah_point;
              }else{
                isiTotal += result.row.nilaiPemohon.poin_pemohon;
                if (result.row.statusPenilaian == 'Belum') {
                  isiTotal += '<br><span style="color:red">[ '+result.row.nilaiPenilai.jumlah_px+' ]</span>';
                }
              }              
            }else{
              isiTotal += '0';              
            }            
                        
              var a = isiTotal.split('.');
            if (a[1] && a[1].length > 3) {
              var getN = a[1].toString().substring(0, 3);
              $('#totalAkTahunan-'+arrayId[tempIdPak]+'_jml').html(a[0]+','+getN);            
            } else {
              $('#totalAkTahunan-'+arrayId[tempIdPak]+'_jml').html(isiTotal);
            }             
          }else{
            return valGrid(tempIdPak);
          }
        },
        error:function() {
          return valGrid(tempIdPak);
        }
      }).then(function () {
        return valGrid(tempIdPak + 1);
      });
    }
    valGrid(tempIdPak);
  }

  function sesuaiTahunan(idPak, idPengajuan, tahun) {
    swal(
      {
        title: "Apa anda yakin dengan Penilaian Anda?",
        text: "Nilai akan disesuaikan dengan nilai dari Pemohon Dupak !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: false
      },
      function(){
        $.post("{!! route('sesuaiPenilaianTahunan') !!}", {idPengajuan:idPengajuan,idPakMaster:idPak,tahun:tahun}).done(function(result){
          if(result.status == 'success'){
            swal("Berhasil!", result.message, "success");
            $('#pemohonTahunan-'+idPak+'_jml').html(result.data.nilaiPemohon.jumlah_px_pemohon);
            $('#penilaiTahunan-'+idPak+'_jml').html(result.data.nilaiPenilai.jumlah_px);

            var a = result.data.nilaiPenilai.jumlah_point.split('.');
            if (a[1] && a[1].length > 3) {
              var getN = a[1].toString().substring(0, 3);
            $('#totalAkTahunan-'+idPak+'_jml').html(a[0]+','+getN);
            } else {
            $('#totalAkTahunan-'+idPak+'_jml').html(result.data.nilaiPenilai.jumlah_point);
            }

          }else{
            swal('Whoops !', result.message, 'warning');
          }
        });
      }
    );
  }

  function sesuaikanSemua(idPengajuan) {
    var tahun = $('#thnSelect').val();
    swal(
      {
        title: "Apa anda yakin dengan Penilaian Anda?",
        text: "Nilai akan disesuaikan dengan nilai dari Pemohon Dupak !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: false
      },
      function(){
      $('.loadingForm').show();
        $.post("{!! route('sesuaiPenilaianTahunan') !!}", {idPengajuan:idPengajuan,tahun:tahun,semua:'Semua'}).done(function(result){
          if(result.status == 'success'){
            $('.loadingForm').hide();
            swal("Berhasil!", result.message, "success");
            location.reload();
          }else{
            $('.loadingForm').hide();
            swal('Whoops !', result.message, 'warning');
          }
        });
      }
    );
  }

  // function detailBulan(idPak, idPengajuan, tahun) {
  //   $('.loadingForm').show();
  //   $('.layer_tahun').hide();
  //   $('.tombolUtama').hide();
  //   $.post("{!! route('loadPenilaianBulan') !!}",{idPak:idPak,idPengajuan:idPengajuan,tahun:tahun}).done(function(data){
  //     $('.loadingForm').hide();
  //     if(data.status == 'success'){
  //       $('.panel-bulan').html(data.content).fadeIn();
  //     } else {
  //       $('.tombolUtama').show();
  //       $('.layer_tahun').show();
  //     }
  //   }).fail(function() {
  //     swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
  //     $('.loadingForm').hide();
  //     $('.layer_tahun').show();
  //   });
  // }

  function EditNilai(idPak, idPengajuan, tahun, jml_awal) {
    $('.loadingForm').show();
    $('.layer_tahun').hide();
    $('.tombolUtama').hide();
    $.post("{!! route('formPerubahanData') !!}",{idPak:idPak,idPengajuan:idPengajuan,tahun:tahun,jml_awal}).done(function(data){
      $('.loadingForm').hide();
      if(data.status == 'success'){
        $('.ganti-modal').html(data.content).fadeIn();
      } else {
        $('.tombolUtama').show();
        $('.layer_tahun').show();
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.loadingForm').hide();
      $('.layer_tahun').show();
    });
  }

  $('.btn-savePenilaian').click(function(e){
    e.preventDefault();
    swal(
      {
        title: "Apa anda yakin menyelesaikan Penilaian Anda?",
        text: "Penilaian akan diakhiri untuk pemohon terpilih !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: false
      },
      function(){
        $('.panel-form').hide();
        $('.loading').show();
        var penilai = {{ $pengajuan->id_pengajuan }};
        $('.btn-savePenilaian').html('Please wait...').attr('disabled', true);
        $.post("{!! route('simpanPenilai') !!}",{idPenilai:penilai}).done(function(data){
          $('.loading').hide();
          if(data.status == 'success'){
            swal('Berhasil !!', data.message, 'success');
            reloadData(1);
            $('.other-page').fadeOut(function(){
              $('.other-page').empty();
              $('.main-layer').fadeIn();
            });
          } else {
            $('.panel-form').show();
            swal('Whooops', data.message, 'error');
            $('.btn-savePenilaian').html('<span class="fas fa-save"></span> Selesai').attr('disabled', false);
          }
        }).fail(function() {
          $('.loading').hide();
          $('.panel-form').show();
          swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
          $('.btn-savePenilaian').html('<span class="fas fa-save"></span> Selesai').attr('disabled', false);
        });
      }
    );
  });

  function KonDecRomawi(angka) {
    var hasil = "";
    if (angka < 1 || angka > 5000) {
      hasil += "Batas Angka 1 s/d 5000";
    }else{
      while (angka >= 1000) {
        hasil += "M";
        angka = angka - 1000;
      }
    }

    if (angka >= 500) {
      if (angka > 500) {
        if (angka >= 900) {
          hasil += "CM";
          angka = angka - 900;
        } else {
          hasil += "D";
          angka = angka - 500;
        }
      }
    }

    while (angka >= 100) {
      if (angka >= 400) {
        hasil += "CD";
        angka = angka - 400;
      } else {
        angka = angka - 100;
      }
    }

    if (angka >= 50) {
      if (angka >= 90) {
        hasil += "XC";
        angka = angka - 90;
      } else {
        hasil += "L";
        angka = angka - 50;
      }
    }

    while (angka >= 10) {
      if (angka >= 40) {
        hasil += "XL";
        angka = angka - 40;
      } else {
        hasil += "X";
        angka = angka - 10;
      }
    }

    if (angka >= 5) {
      if (angka == 9) {
        hasil += "IX";
        angka = angka - 9;
      } else {
        hasil += "V";
        angka = angka - 5;
      }
    }

    while (angka >= 1) {
      if (angka == 4) {
        hasil += "IV";
        angka = angka - 4;
      } else {
        hasil += "I";
        angka = angka - 1;
      }
    }
    return hasil;
  }

  function ribuan(nilai) {
    if (nilai != 0 && isNaN(nilai) != true && nilai != '') {
      if (nilai < 0) {
        var tandaDepan = '(';
        var tandaBelakang = ')';
      }else{
        var tandaDepan = '';
        var tandaBelakang = '';
      }
      var pecah = nilai.split('.');
      var reverse  = pecah[0].toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return tandaDepan+' '+ribuan+','+pecah[1]+' '+tandaBelakang;
    }else{
      return '-';
    }
  }

</script>
