<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 layer_bulan p-0">
  <div class="card">
    <div class="card-header">
      <h5>Data Bulanan</h5>
    </div>
    <div class="card-body pr-10 pl-10">
      <div class="loadingBulan" align="center" style="display: none;margin-top:-120px">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
      <div id="table-scroll" class="table-scroll table-bulan" style="height:auto">
        <?php
          $jumlahBulan = count($bulan);
          $panjangBulan = 40 + 440 + ($jumlahBulan * 140) + 140;
        ?>
        <table id="main-table" class="main-table" style="min-width : {{ $panjangBulan }}px;">
          <thead>
            <tr>
              <th rowspan="2" width="40px" class="text-center pr-0 pl-0">No</th>
              <th rowspan="2" colspan="11" width="440px">Butir Kegiatan</th>
              @for ($i=0; $i < $jumlahBulan; $i++)
                <th colspan="2" class="text-center" width="140px">{{ App\Http\Libraries\Formatters::get_bulan($bulan[$i]) }}</th>
              @endfor
              <th rowspan="2" class="text-center" width="70px">Jumlah Total</th>
              <th rowspan="2" class="text-center" width="70px">Total AK</th>
            </tr>
            <tr>
              @for ($j=0; $j < $jumlahBulan; $j++)
                <th class="text-center" width="70px">Jumlah</th>
                <th class="text-center" width="70px">Akhir</th>
              @endfor
            </tr>
          </thead>
          <tbody>
            <tr>
              <th class="text-center" width="40px"></th>
              <th colspan="11" width="440px">{{ $unsur->nama }}</th>
              <td colspan="{{ $jumlahBulan*2 }}"></td>
              <th class="text-center" width="70px"></th>
              <th class="text-center" width="70px"></th>
            </tr>
            @for ($b=count($butir); $b > 0; $b--)
              <?php $n = $b-1; ?>
              <tr>
                @if ($butir[$n]->level == 1)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px">I</th>
                    <th colspan="11" width="440px">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px">I</th>
                    <th colspan="11" width="440px">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 2)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px">A</th>
                    <th class="level2" width="400px" colspan="10">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px">A</th>
                    <th class="level2" width="400px" colspan="10">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 3)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px">1.</th>
                    <th class="level3" width="360px" colspan="9">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px">1.</th>
                    <th class="level3" width="360px" colspan="9">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 4)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px">a.</th>
                    <th class="level4" width="320px" colspan="8">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px">a.</th>
                    <th class="level4" width="320px" colspan="8">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 5)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"><i class="fas fa-angle-double-right"></i>.</th>
                    <th class="level5" width="280px" colspan="7">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"><i class="fas fa-angle-double-right"></i>.</th>
                    <th class="level5" width="280px" colspan="7">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @elseif ($butir[$n]->level == 6)
                  @if ($butir[$n]->is_title == '1')
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"></th>
                    <th class="level5 text-center" width="40px"><i class="fas fa-terminal"></i>.</th>
                    <th class="level6" width="240px" colspan="6">{{ $butir[$n]->butir_kegiatan }}</th>
                  @else
                    <th class="text-center" width="40px"></th>
                    <th class="text-center" width="40px"></th>
                    <th class="level2 text-center" width="40px"></th>
                    <th class="level3 text-center" width="40px"></th>
                    <th class="level4 text-center" width="40px"></th>
                    <th class="level5 text-center" width="40px"><i class="fas fa-terminal"></i>.</th>
                    <th class="level6" width="240px" colspan="6">{{ $butir[$n]->butir_kegiatan }}<br>( Tiap {{ $butir[$n]->jum_min }} {{ $butir[$n]->satuan }} {{ $butir[$n]->points }} ) <span style="float:right;">{{ $butir[$n]->jabatan->nama }}</th>
                  @endif
                @endif
                @if ($butir[$n]->is_title == '1')
                  <td colspan="{{ $jumlahBulan*2 }}"></td>
                  <th class="text-center" width="70px"></th>
                  <th class="text-center" width="70px"></th>
                @else
                  @for ($m=0; $m < $jumlahBulan; $m++)
                    <td id="pemohonBulan-{{ $butir[$n]->id_pak_master }}_{{ $bulan[$m] }}_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">
                      @if ($nilai[$m]['statusData'] == 'Ada' || $nilai[$m]['statusData'] == 'Admin')
                        {{ $nilai[$m]['nilaiPemohon']['jumlah_px_pemohon'] }}
                        @if ($nilai[$m]['statusPenilaian'] == 'Belum')
                          <br><span style="color:red">[ {{ $nilai[$m]['nilaiPenilai']['jumlah_px'] }} ]</span>
                        @endif
                      @else
                        0
                      @endif

                      {{-- @if ($nilai[$m]['nilaiPemohon']['jumlah_px_pemohon'] != '')
                        {{ $nilai[$m]['nilaiPemohon']['jumlah_px_pemohon'] }}
                        @if ($nilai[$m]['statusAccAll'] == 'Belum')
                          @if ($nilai[$m]['nilaiPenilai']['jumlah_px_penilai'] != '')
                            <br><span style="color:red">[ {{ $nilai[$m]['nilaiPenilai']['jumlah_px_penilai'] }} ]</span>
                          @endif
                        @endif
                      @else
                        -
                      @endif --}}
                    </td>
                    <td id="penilaiBulan-{{ $butir[$n]->id_pak_master }}_{{ $bulan[$m] }}_jml" class="panelIsiNilai text-center txt-middle pl-0 pr-0">
                      @if ($nilai[$m]['statusData'] == 'Ada' || $nilai[$m]['statusData'] == 'Admin')
                        @if ($nilai[$m]['statusPenilaian'] == 'Selesai')
                          {{ $nilai[$m]['nilaiPenilai']['jumlah_px'] }}
                        @else
                          <span class="btnIsiNilai">
                            <a href="javascript:void(0)" onclick="detailHarian({{ $butir[$n]->id_pak_master }},{{ $pengajuan->id_pengajuan }},{{ $tahun }},{{ $bulan[$m] }})" class="btn btn-xs btn-primary" style="width:60px">Detail</a>
                          </span>
                        @endif
                      @else
                        0
                      @endif

                      {{-- @if($nilai[$m]['statusAcc'] == 'Belum')
                        @if ($nilai[$m]['jmlDataHr'] == 'Ada')
                          <span class="btnIsiNilai">
                            <a href="javascript:void(0)" onclick="detailHarian({{ $butir[$n]->id_pak_master }},{{ $pengajuan->id_pengajuan }},{{ $tahun }},{{ $bulan[$m] }})" class="btn btn-xs btn-primary" style="width:60px">Detail</a>
                          </span>
                        @else
                          -
                        @endif
                      @else
                        {{ $nilai[$m]['nilaiPenilai']['jumlah_px_penilai'] }}
                      @endif --}}
                    </td>
                  @endfor
                  <th id="jumlahBulan" class="panelIsiNilai text-center txt-middle" width="70px">
                    @if ($statusTahun == 'Selesai')
                      {{ $nilaiPenilaiTahun['jumlah_px'] }}
                    @else
                      {{ $nilaiPemohonTahun['jumlah_px_pemohon'] }}
                      @if ($statusTahun == 'Belum')
                        <br><span style="color:red">[ {{ $nilaiPenilaiTahun['jumlah_px'] }} ]</span>
                      @endif
                    @endif

                    {{-- @if ($statusAccTahun == 'Sudah')
                      {{ $nilaiBulan['nilaiPenilai']['jumlah_px_penilai'] }}
                    @else
                      @if ($nilaiBulan['nilaiPemohon']['jumlah_px_pemohon'] != '')
                        {{ $nilaiBulan['nilaiPemohon']['jumlah_px_pemohon'] }}
                        @if ($totalIdAfterCek == 'Ada')
                          <br><span style="color:red">[ {{ $totalPxAfterCek }} ]</span>
                        @endif
                      @else
                        -
                      @endif
                    @endif --}}
                  </th>
                  <th id="jumlahAkBln" class="panelIsiNilai text-center txt-middle" width="70px">
                    @if ($statusTahun == 'Selesai')
                      {{ $nilaiPenilaiTahun['jumlah_point'] }}
                    @else
                      {{ $nilaiPemohonTahun['poin_pemohon'] }}
                      @if ($statusTahun == 'Belum')
                        <br><span style="color:red">[ {{ $nilaiPenilaiTahun['jumlah_point'] }} ]</span>
                      @endif
                    @endif

                    {{-- @if ($statusAccTahun == 'Sudah')
                      {{ $nilaiBulan['nilaiPointPenilai']['poin_penilai'] }}
                    @else
                      @if ($nilaiBulan['nilaiPoint']['poin_pemohon'] != '')
                        {{ $nilaiBulan['nilaiPoint']['poin_pemohon'] }}
                        @if ($totalIdAfterCek == 'Ada')
                          <br><span style="color:red">[ {{ $totalPointAfterCek }} ]</span>
                        @endif
                      @else
                        -
                      @endif
                    @endif --}}
                  </th>
                @endif
              </tr>
            @endfor
          </tbody>
        </table>
      </div>
      <div class="col-lg-12 col-md-12 text-right pt-3">
        <button type="button" class="btn btn-xs btn-secondary btn-cancelBln"><span class="fas fa-chevron-left"></span> Kembali</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.btn-cancelBln').click(function(e){
    e.preventDefault();
    $('.layer_bulan').animateCss('bounceOutDown');
    $('.panel-bulan').fadeOut(function(){
      $('.panel-bulan').empty();
      $('.layer_tahun').fadeIn();
      $('.tombolUtama').show();
    });
  });

  function detailHarian(idPak, idPengajuan, tahun, bulan) {
    $('.loadingForm').show();
    $('.layer_bulan').hide();
    $.post("{!! route('loadPenilaianHarian') !!}",{idPak:idPak,idPengajuan:idPengajuan,tahun:tahun,bulan:bulan}).done(function(data){
      $('.loadingForm').hide();
      if(data.status == 'success'){
        $('.panel-harian').html(data.content).fadeIn();
      } else {
        $('.layer_bulan').show();
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.loadingForm').hide();
      $('.layer_bulan').show();
    });
  }
</script>
