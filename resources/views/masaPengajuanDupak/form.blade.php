<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($masa_pengajuan == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($masa_pengajuan != "")
        <input type="hidden" name="id_masa_pengajuan" value="{{ $masa_pengajuan->id_masa_pengajuan }}">
      @endif
      <div class="form-group row">
        <label for="inputtglawal" class="col-4 col-lg-3 col-form-label">Tanggal Awal <small style="color:red;">*</small></label>
        <div class="input-group input-group-sm col-8 col-lg-9">
          <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
          <input id="inputtglawal" type="text" name="tgl_awal" value="@if($masa_pengajuan != ''){{ date('d-m-Y', strtotime($masa_pengajuan->tgl_awal)) }}@endif" placeholder="Masukkan tanggal awal masa Pengajuan Dupak" class="form-control" data-date-format="dd-mm-yyyy">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputtglakhir" class="col-4 col-lg-3 col-form-label">Tanggal Akhir <small style="color:red;">*</small></label>
        <div class="input-group input-group-sm col-8 col-lg-9">
          <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
          <input id="inputtglakhir" type="text" name="tgl_akhir" value="@if($masa_pengajuan != ''){{ date('d-m-Y', strtotime($masa_pengajuan->tgl_akhir)) }}@endif" placeholder="Masukkan tanggal alhir masa Pengajuan Dupak" class="form-control" data-date-format="dd-mm-yyyy">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputKeterangan" class="col-4 col-lg-3 col-form-label">Keterangan</label>
        <div class="col-8 col-lg-9">
          <textarea id="inputKeterangan" type="text" row="3" name="keterangan" placeholder="Tambahkan Keterangan dan Ketentuan tertentu" class="form-control">@if($masa_pengajuan != ''){{ $masa_pengajuan->keterangan }}@endif</textarea>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('#inputtglawal').datetimepicker({
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });
  $('#inputtglakhir').datetimepicker({
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  function loadFilePhoto(event) {
		var image = URL.createObjectURL(event.target.files[0]);
		$('#preview-photo').fadeOut(function(){
			$(this).attr('src', image).fadeIn().css({
				'-webkit-animation' : 'showSlowlyElement 700ms',
				'animation'         : 'showSlowlyElement 700ms'
			});
		});
	};

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveMasaPengajuanDupak') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
