<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">

  <h5 class="card-header bg-primary"><i class="fas fa-search-plus mr-2"></i> Detail Kegiatan Pegawai</h5>
  <form method="post" action="{{route('simpan_kegiatan_master')}}" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="card-body">
      <div class="row">
        <input id="inputIdPenilaian" type="hidden" name="id_penilaian" value="{{ $penilaian->id_penilaian }}" class="form-control backWhite" readonly>
        <input id="inputIdPenilaian" type="hidden" name="idPakMaster" value="{{ $pakMaster->id_pak_master }}" class="form-control backWhite" readonly>
        <input id="inputIdPenilaian" type="hidden" name="idjabatan" value="{{ $pegawai->jabatan_id }}" class="form-control backWhite" readonly>
        <input type="hidden" name="tgl" value="{{$penilaian->tanggal}}">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-6 col-lg-6 col-form-label">Jumlah Total</label>
            <div class="col-6 col-lg-6">
              <input type="number" value="{{ $penilaian->jumlah_px_real }}" name="jumlah" placeholder="Jumlah Pasien" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        @if ($pegawai->status_pegawai == 'PNS')
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-6 col-lg-6 col-form-label">Jumlah Dupak</label>
            <div class="col-6 col-lg-6">
              <input type="number" value="{{ $penilaian->jumlah_px_pemohon }}" name="jumlah_dupak" @if($detailKegiatan->count()!=0) onchange="jumlah_dupak_ganti('{{$tgl}}')" @endif placeholder="Jumlah Point" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6">
          <div class="form-group row">
            <label class="col-6 col-lg-6 col-form-label">Jumlah Point</label>
            <div class="col-6 col-lg-6">
              <input type="text" value="{{ $penilaian->poin_pemohon }}" placeholder="Jumlah Point" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6">
          <div style="text-align: right">
            <a href="javascript:void(0)" class="btn btn-xs btn-space btn-warning" id="btn-edit" onclick="editDetail()"><i class="fa fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" class="btn btn-xs btn-space btn-danger" id="btn-edit" onclick="hapusDetail()"><i class="fa fa-trash"></i> Hapus</a>
          </div>
        </div>
 	@else        
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6">
          <div style="text-align: right">
            <a href="javascript:void(0)" class="btn btn-xs btn-space btn-warning" id="btn-edit" onclick="editDetail()"><i class="fa fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" class="btn btn-xs btn-space btn-danger" id="btn-edit" onclick="hapusDetail()"><i class="fa fa-trash"></i> Hapus</a>
          </div>
        </div>
        @endif      
      </div>
      @if($detailKegiatan->count()!=0)
      <div id="bukti" style="overflow: auto">
        <table class="table table-striped table-bordered first">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              @foreach($bukti_fisik as $key)
              <th>{{$key->nama_bukti}}</th>
              @endforeach
            </tr>
          </thead>
          <tbody>
            <?php $no = 1; ?>
            @foreach($detailKegiatan as $key)
            <tr>
              <td>{{$no}}</td>
              <td>{{$key->tanggal}}</td>
              @foreach($bukti_fisik as $key1)
              <?php 
              $kolom = $key1->no_kolom;
              ?>
              <td>{{$key->$kolom}}</td>
              @endforeach
            </tr>
            <?php $no++; ?>
            @endforeach
          </tbody>
        </table>
      </div>
      @else
      <h1>
        Tidak ada/ tanpa bukti fisik
      </h1>
      @endif

      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right" id="view">
            <button class="btn btn-sm btn-space btn-secondary btn-backDetail"><span class="fas fa-chevron-left"></span> Kembali</button>
          </p>
          <p class="text-right" id="edit">
            <button class="btn btn-sm btn-space btn-secondary" onclick="batalkan()"><span class="fas fa-times"></span> Batal</button>
            <button class="btn btn-sm btn-space btn-primary" type="submit"><span class="fas fa-save"></span> Simpan</button>
          </p>
        </div>
      </div>
    </div>
  </form>
  
</div>
<div class="second-page"></div>
<div class="second-modal"></div>

<script type="text/javascript">//editDataKegiatan
var onLoad = (function() {
  $('.panel-form').animateCss('bounceInUp');
  $('#view').show();
  $('#edit').hide();
})();
$('.btn-backDetail').click(function(e){
  e.preventDefault();
  $('.panel-detail').animateCss('bounceOutDown');
  $('.other-page').fadeOut(function(){
    $('.other-page').empty();
    $('.main-layer').fadeIn();
  });
});

function jumlah_dupak_ganti(tgl){
  var dupak = $('input[name=jumlah_dupak]').val();
  $.post("{{route('viewDetailEditKegiatan')}}",{dupak:dupak,idPenilaian:'{{$penilaian->id_penilaian}}',tgl:tgl},function(data){
    if(data.status=='success'){
      $('#bukti').html('');
      $('#bukti').html(data.content);
    }
  });
}

function batalkan(){
  $('.other-page').empty();
  detailKegiatan('{{$penilaian->id_penilaian}}');
}

function editDetail(){
  $('#btn-edit').hide();
  $('#view').hide();
  $('#edit').show();
  $('input[name=jumlah]').removeAttr('readonly');
  $('input[name=jumlah_dupak]').removeAttr('readonly');
  jumlah_dupak_ganti('{{$tgl}}');
}

function editKegiatan(id_detail_kegiatan){
  var id_penilaian = '{{ $penilaian->id_penilaian }}';
  $.post("{{route('editDataKegiatan')}}",{id_detail_kegiatan:id_detail_kegiatan,id_penilaian:id_penilaian},function(data){
    if(data.status=='success'){
      $('.second-modal').html(data.content);
    }else{
      swal("MAAF !","Terjadi Kesalahan !!", "warning");
    }
  });
}

function hapusDetail(){    
  var id_penilaian = '{{ $penilaian->id_penilaian }}';
  console.log(id_penilaian);
  swal(
    {
        title: "Apa anda yakin menghapus Data ini?",
        text: "Data di Tanggal ini akan dihapus dari sistem dan tidak dapat dikembalikan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: true
    },
    function(){
        $.post("{!! route('hapusDetailKegiatan') !!}", {id_penilaian:id_penilaian}).done(function(data){
            if(data.status == 'success'){
                location.reload();                  
                swal(data.title, data.message, data.type);
            }else if(data.status == 'error'){
                location.reload();
                swal(data.title, data.message, data.type);
            }
        });
    }
  );
}

</script>
