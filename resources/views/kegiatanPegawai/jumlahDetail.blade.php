<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
			</div>
      <form class="formJumlah">
        <div class="modal-body">
          <input id="inputIdjabatan" type="hidden" name="idjabatan" value="{{ $idjabatan }}" class="form-control backWhite" readonly>
          <input id="inputIdPakMaster" type="hidden" name="idPakMaster" value="{{ $pakMaster->id_pak_master }}" class="form-control backWhite" readonly>
          <input id="inputDay" type="hidden" name="tgl" value="{{ $tgl }}" class="form-control backWhite" readonly>
          <input id="inputDay" type="hidden" name="bulanload" value="{{ $bulanload }}" class="form-control backWhite" readonly>
          <input id="inputStPgw" type="hidden" name="status_pegawai" value="{{ $pegawai->status_pegawai }}" class="form-control backWhite" readonly>
          <input id="inputStBukti" type="hidden" name="status_bukti" value="{{ $pakMaster->is_bukti }}" class="form-control backWhite" readonly>
          <div class="form-group row">
            <label class="col-12 col-form-label p-t-0 p-b-0">Tanggal : {{ $tgl }}-{{ $bulanload }} <small style="color:red;">*</small></label>
            {{-- <label class="col-12 col-form-label p-t-0 p-b-0">Tanggal : {{ $tgl }}-{{ date('m-Y') }} <small style="color:red;">*</small></label> --}}
          </div>
          <div class="form-group row">
            <label class="col-12 col-form-label p-t-0 p-b-0">Unsur Kegiatan : {{ $pakMaster->unsur->nama }} <small style="color:red;">*</small></label>
          </div>
          <div class="form-group row">
            <label class="col-lg-12 col-form-label p-t-0 p-b-0">Butir Kegiatan :</label>
            @for($i=0;$i<count($infoButir);$i++)
              @php
                $tanda = '- ';
                switch ($i) {
                  case 0: $jarakButir = ''; $tanda = ''; break;
                  case 1: $jarakButir = 'margin-left:10px;'; break;
                  case 2: $jarakButir = 'margin-left:20px;'; break;
                  case 3: $jarakButir = 'margin-left:30px;'; break;
                  case 4: $jarakButir = 'margin-left:40px;'; break;
                  case 5: $jarakButir = 'margin-left:50px;'; break;
                  case 6: $jarakButir = 'margin-left:60px;'; break;
                  case 7: $jarakButir = 'margin-left:70px;'; break;
                  default: $jarakButir = ''; break;
                }
              @endphp
              <label class="col-lg-11 offset-lg-1 col-form-label p-t-0 p-b-0"><span style="{{ $jarakButir }}">{{ $tanda }}{{ $infoButir[$i]['nama'] }}</span></label>
            @endfor
          </div>
          <div class="form-group row">
            <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah Total {{ $pakMaster->satuan }} <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputJumlah" type="number" name="jumlah" placeholder="Jumlah Total {{ $pakMaster->satuan }}" class="form-control">
            </div>
          </div>
          @if ($pegawai->status_pegawai == "PNS")
            <div class="form-group row">
              <label for="inputJumlah" class="col-4 col-lg-3 col-form-label">Jumlah DUPAK <small style="color:red;">*</small></label>
              <div class="col-8 col-lg-9">
                <input id="inputJumlah" type="number" name="jumlah_dupak" placeholder="Jumlah {{ $pakMaster->satuan }} untuk Dupak" class="form-control">
              </div>
            </div>
          @endif
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-chevron-left"></span> Batal</a>
          <a href="javascript:void(0)" class="btn btn-primary btn-submitJumlah">Simpan <span class="fas fa-save"></span></a>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '70%'
    });
    $('#detail-dialog').modal('show');
  })();
  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  })

  $('.btn-submitJumlah').click(function(e){
    e.preventDefault();
    @if ($pegawai->status_pegawai == "PNS")
      @if($pakMaster->is_bukti == 'Tidak')
        var targetUrl = "{{ route('addDetailKegiatanNew') }}";
      @else
        var targetUrl = "{{ route('formAddDetailKegiatan') }}";
      @endif
    @else
      var targetUrl = "{{ route('addKegiatanNonPns') }}";
    @endif
    $('.btn-submitJumlah').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.formJumlah')[0]);
    data.append('jawaban', $('#jawaban')[0])
    $.ajax({
      url: targetUrl,
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.formJumlah').validate(data, 'has-error');
      $('.loading').show();
      $('.main-layer').hide();
      if(data.status == 'success'){
        @if ($pegawai->status_pegawai == "PNS" && $pakMaster->is_bukti == 'Ya')
          $('#detail-dialog').modal('hide');
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        @else
          $('#detail-dialog').modal('hide');
          $('.loading').hide();
          $('.main-layer').fadeIn();
          getDataButir();
          var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan('+data.data.penilaian.id_penilaian+')">'+data.data.jumlah+'</a>';
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_'+data.data.tgl).html(isi);

          var jmlLama = $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jml').html();
          var jmlBaru = parseInt(jmlLama) + parseInt(data.data.jumlah);
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jml').html(jmlBaru);

          var jmlPakLama = $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jmlAk').html();
          var jmlPakBaru = parseFloat(jmlPakLama) + parseFloat(data.data.point);
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jmlAk').html(jmlPakBaru.toFixed(3));
        @endif
      } else if(data.status == 'error') {
        $('.btn-submitJumlah').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitJumlah').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitJumlah').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
