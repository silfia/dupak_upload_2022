<?php 
function genType($jenis,$kolom){
  if($jenis=='nik'){
    $return = 'type=text class=form-control minlength=16 maxlength=16 onkeyup=cekNumber(this)';
  }else if($jenis=='text'){
    $return = 'type=text class=form-control';
    if($kolom=='kolom_6'){
      $return.=' minlength=13 maxlength=13 onkeyup=cekNumber(this)';
    }
  }else if($jenis=='tanggal'){
    $return = 'type=date class=form-control';
  }else if($jenis=='angka bulat'){
    $return = 'type=number class=form-control';
  }else if($jenis=='angka desimal'){
    $return = 'type=number class=form-control';
  }else if($jenis=='file gambar'){
    $return = 'type=file accept=image/*';
  }else if($jenis=='file dokumen'){
    $return = 'type=file accept=.pdf';
  }
  return $return;
}
?>
<table class="table table-striped table-bordered first">
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal</th>
      @foreach($bukti_fisik as $key)
      <th>{{$key->nama_bukti}}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
    <?php $no = 1; ?>
    @for($i=0;$i<$dupak;$i++)
    @if(!empty($detailKegiatan[$i]))
    <tr>
      <td>{{$no}}</td>
      <td>{{$detailKegiatan[$i]->tanggal}}</td>
      @foreach($bukti_fisik as $key1)
      <?php 
      $kolom = $key1->no_kolom;
      ?>
      <td><input {{genType($key1->jenis_isi,$key1->no_kolom)}} name="{{$key1->no_kolom}}[]" required value="{{$detailKegiatan[$i]->$kolom}}"></td>
      @endforeach
    </tr>
    @else
    <tr>
      <td>{{$no}}</td>
      <td>{{date('Y-m-')}}{{$tgl}}</td>
      @foreach($bukti_fisik as $key1)
      <?php 
      $kolom = $key1->no_kolom;
      ?>
      <td><input {{genType($key1->jenis_isi,$key1->no_kolom)}} name="{{$key1->no_kolom}}[]" required value=""></td>
      @endforeach
    </tr>
    @endif
    <?php $no++; ?>
    @endfor
  </tbody>
</table>