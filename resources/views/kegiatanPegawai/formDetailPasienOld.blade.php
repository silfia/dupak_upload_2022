<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form" style="box-shadow:none; -moz-box-shadow:none; -webkit-box-shadow:none;background-color:transparent;">
  @if($pasiens == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body p-0">
    <form class="form-save">
      <input id="inputIdjabatan" type="hidden" name="idjabatan" value="{{ $idjabatan }}" class="form-control backWhite" readonly>
      <input id="inputIdPakMaster" type="hidden" name="idPakMaster" value="{{ $idPakMaster }}" class="form-control backWhite" readonly>
      <input id="inputDay" type="hidden" name="tgl" value="{{ $tgl }}" class="form-control backWhite" readonly>
      <input id="inputJumlah" type="hidden" name="jumlah" value="{{ $jumlah }}" class="form-control backWhite" readonly>
      <input id="inputStatusPegawai" type="hidden" name="status_pegawai" value="{{ $status_pegawai }}" class="form-control backWhite" readonly>
      <input id="inputJumlahDupak" type="hidden" name="jumlah_dupak" value="{{ $jumlah_dupak }}" class="form-control backWhite" readonly>
      <input id="inputJenisKegiatan" type="hidden" name="jenis_kegiatan" value="{{ $dtPakMaster->satuan }}" class="form-control backWhite" readonly>
      <div class="accrodion-regular">
        <div id="accordion3">
          @for ($i=0; $i < $jumlah_dupak; $i++)
            <div class="card @if ($i != 0) mb-2 @endif">
              <div class="card-header" id="heading{{ $i }}">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $i }}" aria-expanded="true" aria-controls="collapse{{ $i }}">
                    <span class="fas fa-angle-down mr-3"></span>{{ $dtPakMaster->satuan }} Ke - {{ $i+1 }}
                  </button>
                </h5>
              </div>
              <div id="collapse{{ $i }}" class="collapse @if ($i == 0) show @endif" aria-labelledby="heading{{ $i }}" data-parent="#accordion3">
                <div class="card-body">
                  @if ($dtPakMaster->satuan == 'Pasien' || $dtPakMaster->satuan == 'Jenazah')
                    <div class="form-group row">
                      <label for="inputNIK{{$i}}" class="col-4 col-lg-3 col-form-label">NIK <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputNIK{{$i}}" type="text" name="nik[{{ $i }}]" class="form-control" placeholder="NIK Pasien">
                      </div>
                    </div>
                    {{-- <div class="form-group row">
                      <label for="inputNama{{$i}}" class="col-4 col-lg-3 col-form-label">Nama {{ $dtPakMaster->satuan }} <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputNama{{$i}}" type="text" name="nama[{{ $i }}]" class="form-control" placeholder="Nama {{ $dtPakMaster->satuan }}">
                      </div>
                    </div> --}}
                    <div class="form-group row">
                      <label for="inputInisial{{$i}}" class="col-4 col-lg-3 col-form-label">Inisial {{ $dtPakMaster->satuan }} <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputInisial{{$i}}" type="text" name="inisial[{{ $i }}]" class="form-control" placeholder="Inisial {{ $dtPakMaster->satuan }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputRM{{$i}}" class="col-4 col-lg-3 col-form-label">No RM <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputRM{{$i}}" type="text" name="no_rm[{{ $i }}]" class="form-control" placeholder="No RM">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputBpjs{{$i}}" class="col-4 col-lg-3 col-form-label">No BPJS <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputBpjs{{$i}}" type="text" name="no_bpjs[{{ $i }}]" class="form-control" placeholder="No BPJS">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Kelamin <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9 row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="Jenis_Kelamin[{{ $i }}]" value="L" class="custom-control-input"><span class="custom-control-label">Laki - Laki</span>
                          </label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="Jenis_Kelamin[{{ $i }}]" value="P" class="custom-control-input"><span class="custom-control-label">Perempuan</span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputUmur{{$i}}" class="col-4 col-lg-3 col-form-label">Umur</label>
                      <div class="col-8 col-lg-9">
                        <input id="inputUmur{{$i}}" type="text" name="umur[{{ $i }}]" class="form-control" placeholder="Umur Pasien">
                      </div>
                    </div>
                    <!-- <div class="form-group row">
                      <label for="inputDiagnosa{{$i}}" class="col-4 col-lg-3 col-form-label">Diagnosa</label>
                      <div class="col-8 col-lg-9">
                        <input id="inputDiagnosa{{$i}}" type="text" name="diagnosa[{{ $i }}]" class="form-control" placeholder="Diagnosa Pasien">
                      </div>
                    </div> -->
                    <div class="form-group row">
                      <label for="inputAlamat{{$i}}" class="col-4 col-lg-3 col-form-label">Alamat</label>
                      <div class="col-8 col-lg-9">
                        <textarea id="inputAlamat{{$i}}" name="alamat[{{ $i }}]" rows="2" placeholder="Alamat Pasien" class="form-control"></textarea>
                      </div>
                    </div>
                  @elseif($dtPakMaster->satuan == 'Sertifikat')
                    <div class="form-group row">
                      <label for="inputTglPelaksanaan{{$i}}" class="col-4 col-lg-3 col-form-label">Tanggal Awal Pelaksanaan <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputTglPelaksanaan{{$i}}" type="text" name="tglPelaksanaan[{{ $i }}]" class="form-control" placeholder="Tanggal Pelaksanaan (Format : YYYY-mm-bb)">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputTglAkhirPelaksanaan{{$i}}" class="col-4 col-lg-3 col-form-label">Tanggal Akhir Pelaksanaan <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputTglAkhirPelaksanaan{{$i}}" type="text" name="tglAkhirPelaksanaan[{{ $i }}]" class="form-control" placeholder="Tanggal Akhir Pelaksanaan (Format : YYYY-mm-bb)">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputWaktuPelaksanaan{{$i}}" class="col-4 col-lg-3 col-form-label">Waktu Pelaksanaan <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputWaktuPelaksanaan{{$i}}" type="time" name="waktuPelaksanaan[{{ $i }}]" class="form-control" placeholder="Waktu Pelaksanaan">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPenyenggara{{$i}}" class="col-4 col-lg-3 col-form-label">Penyelenggara</label>
                      <div class="col-8 col-lg-9">
                        <input id="inputPenyenggara{{$i}}" type="text" name="penyelenggara[{{ $i }}]" class="form-control" placeholder="Nama Penyelenggara">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputSertifikat{{$i}}" class="col-4 col-lg-3 col-form-label">Nama Sertifikat <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputSertifikat{{$i}}" type="text" name="sertifikat[{{ $i }}]" class="form-control" placeholder="Nama Sertifikat">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputNoSertifikat{{$i}}" class="col-4 col-lg-3 col-form-label">Nomer Sertifikat <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputNoSertifikat{{$i}}" type="text" name="noSertifikat[{{ $i }}]" class="form-control" placeholder="Nomer Sertifikat">
                      </div>
                    </div>
                  @elseif($dtPakMaster->satuan == 'Ijazah')
                    <div class="form-group row">
                      <label for="inputTglPelaksanaan{{$i}}" class="col-4 col-lg-3 col-form-label">Tanggal Lulus <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputTglPelaksanaan{{$i}}" type="text" name="tglLulus[{{ $i }}]" class="form-control" placeholder="Tanggal Lulus (Format : YYYY-mm-bb)">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputUniversitas{{$i}}" class="col-4 col-lg-3 col-form-label">Universitas <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputUniversitas{{$i}}" type="text" name="universitas[{{ $i }}]" class="form-control" placeholder="Nama Universitas">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputJurusan{{$i}}" class="col-4 col-lg-3 col-form-label">Jurusan <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputJurusan{{$i}}" type="text" name="jurusan[{{ $i }}]" class="form-control" placeholder="Jurusan">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputNoIjazah{{$i}}" class="col-4 col-lg-3 col-form-label">Nomer Ijazah <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputNoIjazah{{$i}}" type="text" name="noIjazah[{{ $i }}]" class="form-control" placeholder="Nomer Ijazah">
                      </div>
                    </div>
                  @elseif($dtPakMaster->satuan == 'Laporan' || $dtPakMaster->satuan == 'Kasus' || $dtPakMaster->satuan == 'Kali' || $dtPakMaster->satuan == 'Buku' || $dtPakMaster->satuan == 'Karya' || $dtPakMaster->satuan == 'Makalah' || $dtPakMaster->satuan == 'Naskah')
                    <?php
                      if($dtPakMaster->satuan == 'Laporan'){
                        $tgl = 'Pembuatan'; $jdl = 'Laporan';
                      }elseif ($dtPakMaster->satuan == 'Kasus') {
                        $tgl = 'Kasus'; $jdl = 'Kasus';
                      }elseif ($dtPakMaster->satuan == 'Kali') {
                        $tgl = 'Kegiatan'; $jdl = 'Nama Kegiatan';
                      }else{
                        $tgl = $dtPakMaster->satuan; $jdl = 'Nama '.$dtPakMaster->satuan;
                      }
                    ?>
                    {{-- <div class="form-group row">
                      <label for="inputTglPelaksanaan{{$i}}" class="col-4 col-lg-3 col-form-label">Tanggal {{ $tgl }}</label>
                      <div class="col-8 col-lg-9">
                        <input id="inputTglPelaksanaan{{$i}}" type="text" name="tglBuat[{{ $i }}]" class="form-control" placeholder="Tanggal {{ $tgl }} (Format : YYYY-mm-bb)">
                      </div>
                    </div> --}}
                    <div class="form-group row">
                      <label for="inputJudul{{$i}}" class="col-4 col-lg-3 col-form-label">{{ $jdl }}</label>
                      <div class="col-8 col-lg-9">
                        <input id="inputJudul{{$i}}" type="text" name="judul[{{ $i }}]" class="form-control" placeholder="{{ $jdl }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputKeterangan{{$i}}" class="col-4 col-lg-3 col-form-label">Keterangan</label>
                      <div class="col-8 col-lg-9">
                        <textarea id="inputKeterangan{{$i}}" name="keterangan[{{ $i }}]" rows="3" class="form-control" placeholder="Keterangan"></textarea>
                      </div>
                    </div>
                  @elseif($dtPakMaster->satuan == 'Jam' || $dtPakMaster->satuan == 'Jam Pelajaran')
                    <div class="form-group row">
                      <label for="inputKegiatan{{$i}}" class="col-4 col-lg-3 col-form-label">Nama Kegiatan <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputKegiatan{{$i}}" type="text" name="nama[{{ $i }}]" class="form-control" placeholder="Nama Kegiatan">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputFoto{{$i}}" class="col-4 col-lg-3 col-form-label">Foto Kegiatan <small style="color:red;">*</small></label>
                      <div class="col-8 col-lg-9">
                        <input id="inputFoto{{$i}}" type="file" name="foto[{{ $i }}]" class="form-control" placeholder="Foto Kegiatan">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputKeterangan{{$i}}" class="col-4 col-lg-3 col-form-label">Keterangan</label>
                      <div class="col-8 col-lg-9">
                        <textarea id="inputKeterangan{{$i}}" name="keterangan[{{ $i }}]" rows="3" class="form-control" placeholder="Keterangan"></textarea>
                      </div>
                    </div>
                  @endif

                </div>
              </div>
            </div>
          @endfor
        </div>
      </div>

      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelButir"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submitDetail">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelButir').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });
  @if($dtPakMaster->satuan == 'Sertifikat' || $dtPakMaster->satuan == 'Ijazah')
   var jml = {{ $jumlah_dupak }};
   for (var i = 0; i < jml; i++) {
     $('#inputTglPelaksanaan'+i).datetimepicker({
         weekStart: 1,
         todayBtn:  1,
         autoclose: 1,
         todayHighlight: 1,
         startView: 2,
         minView: 2,
         forceParse: 0,
     });
     @if($dtPakMaster->satuan == 'Sertifikat')
       $('#inputTglAkhirPelaksanaan'+i).datetimepicker({
           weekStart: 1,
           todayBtn:  1,
           autoclose: 1,
           todayHighlight: 1,
           startView: 2,
           minView: 2,
           forceParse: 0,
       });
     @endif
   }
  @endif

  $('.btn-submitDetail').click(function(e){
    e.preventDefault();
    $('.btn-submitDetail').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addDetailKegiatan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      $('.panel-form').hide();
      $('.loading').show();
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.loading').hide();
          $('.main-layer').fadeIn();
          // getDataButir();
          var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan('+data.data.penilaian.id_penilaian+')">'+data.data.jumlah+'</a>';
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_'+data.data.tgl).html(isi);

          var jmlLama = $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jml').html();
          var jmlBaru = parseInt(jmlLama) + parseInt(data.data.jumlah);
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jml').html(jmlBaru);

          var jmlPakLama = $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jmlAk').html();
          var jmlPakBaru = parseFloat(jmlPakLama) + parseFloat(data.data.point);
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jmlAk').html(jmlPakBaru.toFixed(3));
        });
      } else if(data.status == 'error') {
        $('.btn-submitDetail').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitDetail').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitDetail').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
