<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form" style="box-shadow:none; -moz-box-shadow:none; -webkit-box-shadow:none;background-color:transparent;">
  @if($pasiens == "")
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
  <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body p-0">
    <form class="form-save" method="post" action="{{route('simpan_kegiatan_master')}}" enctype="multipart/form-data">
      {{csrf_field()}}
      <input id="inputIdjabatan" type="hidden" name="idjabatan" value="{{ $idjabatan }}" class="form-control backWhite" readonly>
      <input id="inputIdPakMaster" type="hidden" name="idPakMaster" value="{{ $idPakMaster }}" class="form-control backWhite" readonly>
      <input id="inputDay" type="hidden" name="tgl" value="{{ $tgl }}" class="form-control backWhite" readonly>
      <input id="inputBulan" type="hidden" name="bulanload" value="{{ $bulanload }}" class="form-control backWhite" readonly>
      <input id="inputJumlah" type="hidden" name="jumlah" value="{{ $jumlah }}" class="form-control backWhite" readonly>
      <input id="inputStatusPegawai" type="hidden" name="status_pegawai" value="{{ $status_pegawai }}" class="form-control backWhite" readonly>
      <input id="inputJumlahDupak" type="hidden" name="jumlah_dupak" value="{{ $jumlah_dupak }}" class="form-control backWhite" readonly>
      <input id="inputJenisKegiatan" type="hidden" name="jenis_kegiatan" value="{{ $dtPakMaster->satuan }}" class="form-control backWhite" readonly>
      <div class="accrodion-regular">
        <div id="accordion3">
          <?php 
          function genType($jenis,$kolom){
            if($jenis=='nik'){
              $return = 'type=text class=form-control minlength=16 maxlength=16 onkeyup=cekNumber(this)';
            }else if($jenis=='text'){
              $return = 'type=text class=form-control';
              if($kolom=='kolom_6'){
                $return.=' minlength=13 maxlength=13 onkeyup=cekNumber(this)';
              }
            }else if($jenis=='tanggal'){
              $return = 'type=date class=form-control';
            }else if($jenis=='angka bulat'){
              $return = 'type=number class=form-control';
            }else if($jenis=='angka desimal'){
              $return = 'type=number class=form-control';
            }else if($jenis=='file gambar'){
              $return = 'type=file accept=image/*';
            }else if($jenis=='file dokumen'){
              $return = 'type=file accept=.pdf';
            }
            return $return;
          }
          ?>
          @for ($i=0; $i < $jumlah_dupak; $i++)
          <div class="card @if ($i != 0) mb-2 @endif">
            <div class="card-header" id="heading{{ $i }}">
              <h5 class="mb-0">
                <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $i }}" aria-expanded="true" aria-controls="collapse{{ $i }}">
                  <span class="fas fa-angle-down mr-3"></span>{{ $dtPakMaster->satuan }} Ke - {{ $i+1 }}
                </button>
              </h5>
            </div>
            <div id="collapse{{ $i }}" class="collapse @if ($i == 0) show @endif" aria-labelledby="heading{{ $i }}" data-parent="#accordion3">
              <div class="card-body">
                @if($bukti_fisik->count()!=0)
                  @foreach($bukti_fisik as $key)
                    @if($key->master_bukti_fisik_id == 12)
                    <div class="row">
                      <label class="col-lg-4 col-md-4">{{$key->nama_bukti}}</label>
                      <div class="col-lg-8 col-md-8">
                        <input value="{{date('Y-m-d')}}" {{genType($key->jenis_isi,$key->no_kolom)}} name="{{$key->no_kolom}}[]" required>                      
                      </div>
                    </div>
                    <div class="clearfix" style="margin-bottom: 10px"></div>
                    @else
                    <div class="row">
                      <label class="col-lg-4 col-md-4">{{$key->nama_bukti}}</label>
                      <div class="col-lg-8 col-md-8">
                        <input {{genType($key->jenis_isi,$key->no_kolom)}} name="{{$key->no_kolom}}[]" required>                      
                      </div>
                    </div>
                    <div class="clearfix" style="margin-bottom: 10px"></div>
                    @endif
                  @endforeach
                @else
                Tidak ada Bukti Master
                @endif
              </div>
            </div>
          </div>
          @endfor
        </div>
      </div>

      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelButir"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  function cekNumber(ini){
    ini.value = ini.value.match(/[0-9]*/);
  }

  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  var inputs = document.querySelectorAll('form div input');

  [].forEach.call(inputs, function(input) {
    input.addEventListener('invalid',function(e){
      swal('Whooops','Form yang lain belum diisi','error');
    });
  });

  $('.btn-cancelButir').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });
  @if($dtPakMaster->satuan == 'Sertifikat' || $dtPakMaster->satuan == 'Ijazah')
  var jml = {{ $jumlah_dupak }};
  for (var i = 0; i < jml; i++) {
   $('#inputTglPelaksanaan'+i).datetimepicker({
     weekStart: 1,
     todayBtn:  1,
     autoclose: 1,
     todayHighlight: 1,
     startView: 2,
     minView: 2,
     forceParse: 0,
   });
   @if($dtPakMaster->satuan == 'Sertifikat')
   $('#inputTglAkhirPelaksanaan'+i).datetimepicker({
     weekStart: 1,
     todayBtn:  1,
     autoclose: 1,
     todayHighlight: 1,
     startView: 2,
     minView: 2,
     forceParse: 0,
   });
   @endif
 }
 @endif

 $('.btn-submitDetail').click(function(e){
  e.preventDefault();
  $('.btn-submitDetail').html('Please wait...').attr('disabled', true);
  var data  = new FormData($('.form-save')[0]);
  $.ajax({
    url: "{{ route('addDetailKegiatan') }}",
    type: 'POST',
    data: data,
    async: true,
    cache: false,
    contentType: false,
    processData: false
  }).done(function(data){
    $('.form-save').validate(data, 'has-error');
    $('.panel-form').hide();
    $('.loading').show();
    if(data.status == 'success'){
      swal("Success !", data.message, "success");
      $('.other-page').fadeOut(function(){
        $('.other-page').empty();
        $('.loading').hide();
        $('.main-layer').fadeIn();
          // getDataButir();
          var isi = '<a href="javascript:void(0);" class="btnIsiKegiatan" onclick="detailKegiatan('+data.data.penilaian.id_penilaian+')">'+data.data.jumlah+'</a>';
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_'+data.data.tgl).html(isi);

          var jmlLama = $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jml').html();
          var jmlBaru = parseInt(jmlLama) + parseInt(data.data.jumlah);
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jml').html(jmlBaru);

          var jmlPakLama = $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jmlAk').html();
          var jmlPakBaru = parseFloat(jmlPakLama) + parseFloat(data.data.point);
          $('#'+data.data.idjabatan+'-'+data.data.idPakMaster+'_jmlAk').html(jmlPakBaru.toFixed(3));
        });
    } else if(data.status == 'error') {
      $('.btn-submitDetail').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
      swal('Whoops !', data.message, 'warning');
    } else {
      var n = 0;
      for(key in data){
        if (n == 0) {var dt0 = key;}
        n++;
      }
      $('.btn-submitDetail').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
      swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
    }
  }).fail(function() {
    swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
    $('.btn-submitDetail').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
  });
});
</script>
