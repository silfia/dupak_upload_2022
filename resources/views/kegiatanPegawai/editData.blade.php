<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
        Edit Data Kegiatan
        <span style="float:right"><a data-dismiss="modal" onclick="closeModal()">Close</a></span>
      </div>
      <div class="modal-body">
        <form class="form-save">
          <input id="id_detail_kegiatanid_detail_kegiatan" type="hidden" name="id_detail_kegiatan" value="{{ $id_detail_kegiatan }}" class="form-control backWhite">
          <input id="pakMasterSatuan" type="hidden" name="pakMasterSatuan" value="{{ $pakMaster->satuan }}" class="form-control backWhite">
          <input id="id_pegawai" type="hidden" name="id_pegawai" value="{{ $pegawai->id_pegawai }}" class="form-control backWhite">
          <input id="id_penilaian" type="hidden" name="id_penilaian" value="{{ $penilaian->id_penilaian }}" class="form-control backWhite">

          <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <h4 style="font-weight: bold; color: #4682B4;">Edit Data Inputan</h4>
            <hr style="  border: 1px solid DimGray;">          

            @if ($pakMaster->satuan == 'Pasien' || $pakMaster->satuan == 'Jenazah')
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">NIK</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="nik" value="{{ $detailKegiatan->nik }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Inisial {{ $pakMaster->satuan }}</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="inisial" value="{{ $detailKegiatan->inisial_pasien }}"  class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">No RM</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="no_rm" value="{{ $detailKegiatan->no_rm }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">No BPJS</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="no_bpjs" value="{{ $detailKegiatan->no_bpjs }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">L/P</label>
                  <div class="col-8 col-lg-8">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" name="Jenis_Kelamin" @if($detailKegiatan->jenis_kelamin == 'L') checked @endif class="custom-control-input"><span class="custom-control-label">Laki - Laki</span>
                      </label>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" name="Jenis_Kelamin" @if($detailKegiatan->jenis_kelamin == 'P') checked @endif class="custom-control-input"><span class="custom-control-label">Perempuan</span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Umur</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="umur" value="{{ $detailKegiatan->umur }} Tahun" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Alamat</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="alamat" value="{{ $detailKegiatan->alamat }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            @elseif ($pakMaster->satuan == 'Sertifikat')            
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Tanggal Pelaksanaan</label>
                  <div class="col-8 col-lg-8">
                    <input id="tgl" type="text" name="tglPelaksanaan" value="{{ date('d-m-Y', strtotime($detailKegiatan->tgl_kegiatan)) }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Tanggal Akhir Pelaksanaan</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" id="tglAkhir" name="tglAkhirPelaksanaan" value="{{ date('d-m-Y', strtotime($detailKegiatan->tgl_akhir_kegiatan)) }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Waktu Pelaksanaan</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="waktuPelaksanaan" value="{{ $detailKegiatan->waktu_kegiatan }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Penyelenggara</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="penyelenggara" value="{{ $detailKegiatan->penyelenggara }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Nama {{ $pakMaster->satuan }}</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="sertifikat" value="{{ $detailKegiatan->nama_pasien }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Nomor {{ $pakMaster->satuan }}</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="noSertifikat" value="{{ $detailKegiatan->nomor }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>
            
            @elseif ($pakMaster->satuan == 'Ijazah')
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Tanggal Lulus</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" id="tgl" name="tglLulus" value="{{ date('d-m-Y', strtotime($detailKegiatan->tgl_kegiatan)) }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Universitas</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="universitas" value="{{ $detailKegiatan->penyelenggara }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Jurusan</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="jurusan" value="{{ $detailKegiatan->nama_pasien }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Nomor {{ $pakMaster->satuan }}</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="noIjazah" value="{{ $detailKegiatan->nomor }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            @elseif($pakMaster->satuan == 'Laporan' || $pakMaster->satuan == 'Kasus' || $pakMaster->satuan == 'Kali' || $pakMaster->satuan == 'Buku' || $pakMaster->satuan == 'Karya' || $pakMaster->satuan == 'Makalah' || $pakMaster->satuan == 'Naskah')

            <?php
                if($pakMaster->satuan == 'Laporan'){ $tgl = 'Pembuatan'; $jdl = 'Laporan'; }
                elseif ($pakMaster->satuan == 'Kasus') { $tgl = 'Kasus'; $jdl = 'Kasus'; }
                elseif ($pakMaster->satuan == 'Kali') { $tgl = 'Kegiatan'; $jdl = 'Nama Kegiatan'; }
                else{ $tgl = $pakMaster->satuan; $jdl = 'Nama '.$pakMaster->satuan; }
              ?>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Tanggal {{ $tgl }}</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" id="tgl" name="tglBuat" value="{{ date('d-m-Y', strtotime($detailKegiatan->tgl_kegiatan)) }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">{{ $jdl }}</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="judul" value="{{ $detailKegiatan->nama_pasien }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Keterangan</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="keterangan" value="{{ $detailKegiatan->keterangan }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            @elseif($pakMaster->satuan == 'Jam' || $pakMaster->satuan == 'Jam Pelajaran')
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Nama</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="nama" value="{{ $detailKegiatan->nama_pasien }}" class="form-control backWhite">
                  </div>
                </div>
              </div>

               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Keterangan</label>
                  <div class="col-8 col-lg-8">
                    <input type="text" name="keterangan" value="{{ $detailKegiatan->keterangan }}" class="form-control backWhite">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                <div class="form-group row">
                  <label class="col-4 col-lg-4 col-form-label">Gambar</label>
                  <div class="col-8 col-lg-8">
                    <input type="file" name="foto" class="form-control">
                    <img src="{{ url('upload/buktiFisik'.$detailKegiatan->gambar) }}" alt="" height="100px">
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>

          <div class="row pt-2 pt-sm-2 mt-1">
            <div class="col-sm-12 pl-0">
              <p class="text-right">
                <button type="submit" class="btn btn-sm btn-space btn-primary btn-submitEdit">Simpan <span class="fas fa-save"></span></button>
              </p>
            </div>
          </div>
        </form>
      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>
<script type="text/javascript">

  @if($pakMaster->satuan == 'Sertifikat' || $pakMaster->satuan == 'Ijazah')
     $('#tgl').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
         autoclose: 1,
         todayHighlight: 1,
         startView: 2,
         minView: 2,
         forceParse: 0,
     });
     @if($pakMaster->satuan == 'Sertifikat')
       $('#tglAkhir').datetimepicker({
           weekStart: 1,
           todayBtn:  1,
           autoclose: 1,
           todayHighlight: 1,
           startView: 2,
           minView: 2,
           forceParse: 0,
       });
     @endif
  @endif

  var onLoad = (function() {
    $('#detail-dialog').find('.second-modal').css({
      'width'     : '80%'
    });
    $('#detail-dialog').modal('show');
  })();

  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.second-modal').html('');
  });

  $('.btn-submitEdit').click(function(e){
    e.preventDefault();
    $('.btn-submitEdit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addEditKegiatan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      $('.panel-form').hide();
      $('.loading').show();
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.modal-backdrop').removeClass('modal-backdrop fade show');
          $('.second-modal').html('');
          $('.other-page').empty();
          $('.loading').hide();
          $('.main-layer').fadeIn();       
        });
      } else if(data.status == 'error') {
        $('.btn-submitEdit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitEdit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitEdit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>

