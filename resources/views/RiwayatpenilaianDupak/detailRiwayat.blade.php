<?php
  function KonDecRomawi($angka) {
    $angka = (int)$angka;
    $hasil = "";
    if ($angka < 1 || $angka > 5000) {
      $hasil .= "Batas $Angka 1 s/d 5000";
    }else{
      while ($angka >= 1000) {
        $hasil .= "M";
        $angka = $angka - 1000;
      }
    }

    if ($angka >= 500) {
      if ($angka > 500) {
        if ($angka >= 900) {
          $hasil .= "CM";
          $angka = $angka - 900;
        } else {
          $hasil .= "D";
          $angka = $angka - 500;
        }
      }
    }

    while ($angka >= 100) {
      if ($angka >= 400) {
        $hasil .= "CD";
        $angka = $angka - 400;
      } else {
        $angka = $angka - 100;
      }
    }

    if ($angka >= 50) {
      if ($angka >= 90) {
        $hasil .= "XC";
        $angka = $angka - 90;
      } else {
        $hasil .= "L";
        $angka = $angka - 50;
      }
    }

    while ($angka >= 10) {
      if ($angka >= 40) {
        $hasil .= "XL";
        $angka = $angka - 40;
      } else {
        $hasil .= "X";
        $angka = $angka - 10;
      }
    }

    if ($angka >= 5) {
      if ($angka == 9) {
        $hasil .= "IX";
        $angka = $angka - 9;
      } else {
        $hasil .= "V";
        $angka = $angka - 5;
      }
    }

    while ($angka >= 1) {
      if ($angka == 4) {
        $hasil .= "IV";
        $angka = $angka - 4;
      } else {
        $hasil .= "I";
        $angka = $angka - 1;
      }
    }
    return $hasil;
  }  
?>

<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Detail Riwayat Penilaian</h5>
  <div class="card-body">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputNoNip" class="col-4 col-lg-3 col-form-label">No NIP</label>
            <div class="col-8 col-lg-9">
              <input id="inputNoNip" type="text" value="{{ $pengajuan->user->pegawai->no_nip }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama</label>
            <div class="col-8 col-lg-9">
              <input id="inputNama" type="text" value="{{ $pengajuan->user->pegawai->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputSaker" class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
            <div class="col-8 col-lg-9">
              <input id="inputSaker" type="text" value="{{ $pengajuan->user->pegawai->satuanKerja->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputGolongan" class="col-4 col-lg-3 col-form-label">Golongan</label>
            <div class="col-8 col-lg-9">
              <input id="inputGolongan" type="text" value="{{ $pengajuan->user->pegawai->golongan->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
            <div class="col-8 col-lg-9">
              <input id="inputJabatan" type="text" value="{{ $pengajuan->user->pegawai->jabatan->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
            <div class="col-8 col-lg-9">
              <input id="inputProfesi" type="text" value="{{ $pengajuan->user->pegawai->typeProfesi->nama }}" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputPenilai1" class="col-4 col-lg-3 col-form-label">Penilai 1</label>
            <div class="col-8 col-lg-9">
              <input id="inputPenilai1" type="text" value="@if($pengajuan->penilai_id != null){{ $pengajuan->penilai->nama }}@else - @endif" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputPenilai2" class="col-4 col-lg-3 col-form-label">Penilai 2</label>
            <div class="col-8 col-lg-9">
              <input id="inputPenilai2" type="text" value="@if($pengajuan->penilai2_id != null){{ $pengajuan->penilai2->nama }}@else - @endif" class="form-control backWhite" readonly>
            </div>
          </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="loadingForm" align="center" style="display: none;margin-top:-120px">
            <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
          </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 layer_tahun">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-3 col-lg-2">
                  <h5>Data Tahunan</h5>
                </div>
                <div class="col-4 col-lg-4">
                  <div class="form-group row pt-0">
                    <label for="tahun" class="col-4 col-lg-3 col-form-label text-right p-0">Tahun</label>
                    <div class="col-8 col-lg-9">
                      <select class="form-control" name="tahun" id="tahun">
                        @foreach ($tahun as $thn)
                          <option value="{{ $thn }}">{{ $thn }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-2 col-lg-2">
                  <div class="form-group row">
                    <button type="button" class="btn btn-xs btn-primary btn-gantiTahun"><span class="fas fa-search"></span> Cari</button>
                  </div>
                </div>
              </div>
            </div>            
          </div>
        </div>
        <span id='idPaks' style="display:none"></span>
        <input type="hidden" name="thnSelect" value="{{ $tahun[0] }}" id='thnSelect'>
        <input type="hidden" name="runTahun" value="Stop" id='runTahun'>
        <div class="col-12 panel-bulan"></div>
        <div class="col-12 panel-harian"></div>
        <div class="col-12 panel-kegiatan"></div>
        <div class="col-12 ganti-modal"></div>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lbayer_bulan p-0">
        <div class="card">
          <div class="card-header">
            <h5>Data Bulanan</h5>
          </div>
          <div class="card-body pr-10 pl-10">
            <div class="loadingBulan" align="center" style="display: none;margin-top:-120px">
              <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
            </div>
            <div id="table-scroll" class="table-scroll table-bulan" style="height:auto">
              <?php
                $panjangBulan = 40 + 440 + 140;
              ?>
              <table id="main-table" class="main-table" style="min-width : {{ $panjangBulan }}px;">
                <thead>
                  <tr>
                    <th rowspan="2" width="40px" class="text-center pr-0 pl-0">No</th>
                    <th rowspan="2" colspan="11" width="340px">Butir Kegiatan</th>
                    <th rowspan="2" class="text-center" width="240px">Keterangan Perubahan</th>                    
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($unsur as $u)
                  <tr>
                    <td class="text-center" width="40px"></td>
                    <td colspan="11" width="340px">{{$u->unsur->nama}}</td>
                    <td class="text-center" width="240px"></td>
                  </tr>
                  @foreach($u->butir as $key1 => $butir)
                  <?php $i1 = 1; ?>
                  @for ($b=count($butir->butir); $b > 0; $b--)
                  <?php $i2 = 1; ?>
                  <?php $i3 = 1; ?>
                  <?php $i4 = 1; ?>
                  <?php $i5 = 1; ?>
                  <?php $n = $b-1; ?>
                  @if($butir->butir[$n]->level == 1)
                  <tr>
                    <td class="text-center" width="40px">{{KonDecRomawi($key1+1)}}</td>
                    <td colspan="11" width="340px">{{$butir->butir[$n]->butir_kegiatan}}</td>
                    <td class="text-center" width="240px"></td>
                  </tr>
                  @endif
                  @if($butir->butir[$n]->level == 2)
                  <tr>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px">{{$i1}}</td>
                    <td colspan="10" width="340px">{{$butir->butir[$n]->butir_kegiatan}}</td>
                    <td class="text-center" width="240px"></td>
                  </tr>
                  @endif
                  @if($butir->butir[$n]->level == 3)
                  <tr>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px">{{$i1}}.{{$i2}}</td>
                    <td colspan="9" width="340px">{{$butir->butir[$n]->butir_kegiatan}}</td>
                    <td class="text-center" width="240px">
                      <ol>
                        @foreach($butir->butir[$n]->penilaian as $p)
                        <li style="padding: 5px;background-color: #ffc107;margin-bottom: 5px;">Tanggal {{$p->tanggal}}, Jumlah Awal {{$p->jumlah_px_pemohon}} & Jumlah Akhir {{$p->jumlah_px_penilai}} dengan alasan ({{$p->alasan_perubahan_1}},{{$p->alasan_perubahan_2}})</li>
                        @endforeach
                      </ol>
                    </td>
                  </tr>
                  <?php $i2++; ?>
                  @endif
                  @if($butir->butir[$n]->level == 4)
                  <tr>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px">{{$i1}}.{{$i2}}.{{$i3}}</td>
                    <td colspan="8" width="340px">{{$butir->butir[$n]->butir_kegiatan}}</td>
                    <td class="text-center" width="240px">
                      <ol>
                        @foreach($butir->butir[$n]->penilaian as $p)
                        <li style="padding: 5px;background-color: #ffc107;margin-bottom: 5px;">Tanggal {{$p->tanggal}}, Jumlah Awal {{$p->jumlah_px_pemohon}} & Jumlah Akhir {{$p->jumlah_px_penilai}} dengan alasan ({{$p->alasan_perubahan_1}},{{$p->alasan_perubahan_2}})</li>
                        @endforeach
                      </ol>
                    </td>
                  </tr>
                  <?php $i3++; ?>
                  @endif
                  @if($butir->butir[$n]->level == 5)
                  <tr>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px">{{$i1}}.{{$i2}}.{{$i3}}.{{$i4}}</td>
                    <td colspan="7" width="340px">{{$butir->butir[$n]->butir_kegiatan}}</td>
                    <td class="text-center" width="240px">
                      <ol>
                        @foreach($butir->butir[$n]->penilaian as $p)
                        <li style="padding: 5px;background-color: #ffc107;margin-bottom: 5px;">Tanggal {{$p->tanggal}}, Jumlah Awal {{$p->jumlah_px_pemohon}} & Jumlah Akhir {{$p->jumlah_px_penilai}} dengan alasan ({{$p->alasan_perubahan_1}},{{$p->alasan_perubahan_2}})</li>
                        @endforeach
                      </ol>
                    </td>
                  </tr>
                  <?php $i4++; ?>
                  @endif
                  @if($butir->butir[$n]->level == 6)
                  <tr>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px"></td>
                    <td class="text-center" width="40px">{{$i1}}.{{$i2}}.{{$i3}}.{{$i4}}.{{$i5}}</td>
                    <td colspan="6" width="340px">{{$butir->butir[$n]->butir_kegiatan}}</td>
                    <td class="text-center" width="240px">
                      <ol>
                        @foreach($butir->butir[$n]->penilaian as $p)
                        <li style="padding: 5px;background-color: #ffc107;margin-bottom: 5px;">Tanggal {{$p->tanggal}}, Jumlah Awal {{$p->jumlah_px_pemohon}} & Jumlah Akhir {{$p->jumlah_px_penilai}} dengan alasan ({{$p->alasan_perubahan_1}},{{$p->alasan_perubahan_2}})</li>
                        @endforeach
                      </ol>
                    </td>
                  </tr>
                  <?php $i5++; ?>
                  @endif
                  @endfor
                  <?php $i1++; ?>
                  @endforeach
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="col-lg-12 col-md-12 text-right tombolUtama">
              <button type="button" class="btn btn-xs btn-secondary btn-cancelLB"><span class="fas fa-chevron-left"></span> Kembali</button>         
            </div>
          </div>
        </div>
      </div>

  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelLB').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  $('#tahun').chosen();
  $(document).ready(function() {
    // LoadTahun();
  });

  $('.btn-gantiTahun').click(function(){
    $('#runTahun').val('Stop');
    var ganti = $('#tahun').val();
    console.log(ganti);
    $('#thnSelect').val(ganti);
    // LoadTahun();
  });

  // function LoadTahun() {
  //   var idPak = '';
  //   var tag = '';
  //   $('.loadingTahun').show();
  //   var spiner = '';
  //   var id_pegawai = {{ $pegawai->id_pegawai }};
  //   $.post("{{route('loadPenilaianTahun')}}",{id_pegawai:id_pegawai},function(data){
  //     if(data.status=='success'){
  //       $('.loadingTahun').hide();
  //       tag += '<table id="main-table" class="main-table">';
  //       tag += '<thead>';
  //       tag += '<tr>';
  //       tag += '<th class="text-center" width="5%">No</th>';
  //       tag += '<th class="text-center" width="">Butir Kegiatan</th>';
  //       tag += '<th class="text-center" width="7%">Jml Awal</th>';
  //       tag += '<th class="text-center" width="7%">Jml Akhir</th>';
  //       tag += '<th class="text-center" width="">Alasan Perubahan</th>';
  //       tag += '</tr>';
  //       tag += '</thead>';
  //       tag += '<tbody>';
  //       if (data.row.butir.length != 0) {
  //         var tempNo1 = 0;          
  //         $.each(data.row.butir, function(k,uns){
  //           tempNo1++;

  //           tag += '<tr>';
  //           tag += '<th class="text-center">'+tempNo1+'.</th>';
  //           tag += '<th></th>';
  //           tag += '<th></th>';
  //           tag += '<th></th>';
  //           tag += '<th></th>';
  //           tag += '</tr>';
  //         });
  //       }else{
  //         tag += '<tr><th colspan="19" class="text-center">Tidak ada data ditemukan...</th></tr>';
  //       }
  //       tag += '</tbody>';
  //       tag += '</table>';
  //       $('.table-tahun').empty();
  //       $('.table-tahun').html(tag);
  //       $('#idPaks').html('');
  //       $('#idPaks').html(idPak);1
  //       $('#runTahun').val('Running');
  //     }else{
  //       swal("MAAF !","Terjadi Kesalahan !!", "warning");
  //     }
  //   }).fail(function() {
  //     swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
  //     LoadTahun();
  //   });
  // }
</script>
