<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  @if($satuanKerja == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($satuanKerja != "")
        <input type="hidden" name="id_satuan_kerja" value="{{ $satuanKerja->id_satuan_kerja }}">
      @endif
      <div class="form-row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputKode" class="col-4 col-lg-3 col-form-label">Kode <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputKode" type="text" name="Kode" value="@if($satuanKerja != ''){{ $satuanKerja->kode }}@endif" placeholder="Kode" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama Satuan Kerja <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputNama" type="text" name="Nama" value="@if($satuanKerja != ''){{ $satuanKerja->nama }}@endif" placeholder="Nama Satuan Kerja" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputType" class="col-4 col-lg-3 col-form-label">Type Satuan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <select id="inputType" class="form-control" name="Type">
                <option value="" disabled selected> .:: Pilih Type Satuan Kerja ::. </option>
                @foreach ($type as $tp)
                  <option value="{{ $tp->id_master_type }}" @if($satuanKerja != '') @if($tp->id_master_type == $satuanKerja->tipe_satker_id) selected @endif @endif>{{ $tp->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPimpinan" class="col-4 col-lg-3 col-form-label">Pimpinan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <select id="inputPimpinan" class="form-control" name="Pimpinan">
                <option value="" disabled selected> .:: Pilih Pimpinan Satuan Kerja ::. </option>
                @foreach ($pegawais as $pegawai)
                  <option value="{{ $pegawai->id_pegawai }}" @if($satuanKerja != '') @if($pegawai->id_pegawai == $satuanKerja->pimpinan_id) selected @endif @endif>{{ $pegawai->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputTelepon" class="col-4 col-lg-3 col-form-label">Telepon</label>
            <div class="col-8 col-lg-9">
              <input id="inputTelepon" type="text" name="Telepon" value="@if($satuanKerja != ''){{ $satuanKerja->telepon }}@endif" placeholder="Telepon" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputFax" class="col-4 col-lg-3 col-form-label">Fax</label>
            <div class="col-8 col-lg-9">
              <input id="inputFax" type="text" name="Fax" value="@if($satuanKerja != ''){{ $satuanKerja->fax }}@endif" placeholder="Fax" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail" class="col-4 col-lg-3 col-form-label">Email <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <input id="inputEmail" type="text" name="Email" value="@if($satuanKerja != ''){{ $satuanKerja->email }}@endif" placeholder="Email" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputWebsite" class="col-4 col-lg-3 col-form-label">Website</label>
            <div class="col-8 col-lg-9">
              <input id="inputWebsite" type="text" name="Website" value="@if($satuanKerja != ''){{ $satuanKerja->website }}@endif" placeholder="Website" class="form-control">
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row m-b-15px">
            <label for="inputAlamat" class="col-4 col-lg-3 col-form-label">Alamat <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <textarea id="inputAlamat" name="Alamat" rows="2" placeholder="Alamat" class="form-control">@if($satuanKerja != ''){{ $satuanKerja->alamat }}@endif</textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Layanan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9 row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="jenis_layanan" value="Rawat Inap" class="custom-control-input" @if($satuanKerja != '') @if($satuanKerja->jenis_layanan == 'Rawat Inap') checked @endif @endif><span class="custom-control-label">Rawat Inap</span>
                </label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="jenis_layanan" value="Rawat Jalan" class="custom-control-input" @if($satuanKerja != '') @if($satuanKerja->jenis_layanan == 'Rawat Jalan') checked @endif @endif><span class="custom-control-label">Rawat Jalan</span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputProvinsi" class="col-4 col-lg-3 col-form-label">Provinsi <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <select id="inputProvinsi" class="form-control" name="Provinsi">
                <option value="" disabled selected> .:: Pilih Provinsi ::. </option>
                @foreach ($provinsi as $pv)
                  <option value="{{ $pv->id_provinsi }}" @if($satuanKerja != '') @if($pv->id_provinsi == $satuanKerja->provinsi_id) selected @endif @endif>{{ $pv->nama_provinsi }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputKabupaten" class="col-4 col-lg-3 col-form-label">Kabupaten <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <select id="inputKabupaten" class="form-control" name="Kabupaten">
                <option value="" disabled selected> .:: Pilih Kabupaten ::. </option>
                @if ($satuanKerja != '')
                  @foreach ($kabupaten as $kb)
                    <option value="{{ $kb->id_kabupaten }}" @if($satuanKerja != '') @if($kb->id_kabupaten == $satuanKerja->kabupaten_id) selected @endif @endif>{{ $kb->nama_kabupaten }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputKecamatan" class="col-4 col-lg-3 col-form-label">Kecamatan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <select id="inputKecamatan" class="form-control" name="Kecamatan">
                <option value="" disabled selected> .:: Pilih Kecamatan ::. </option>
                @if ($satuanKerja != '')
                  @foreach ($kecamatan as $kc)
                    <option value="{{ $kc->id_kecamatan }}" @if($satuanKerja != '') @if($kc->id_kecamatan == $satuanKerja->kecamatan_id) selected @endif @endif>{{ $kc->nama_kecamatan }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputKelurahan" class="col-4 col-lg-3 col-form-label">Kelurahan <small style="color:red;">*</small></label>
            <div class="col-8 col-lg-9">
              <select id="inputKelurahan" class="form-control" name="Kelurahan">
                <option value="" disabled selected> .:: Pilih Kelurahan ::. </option>
                @if ($satuanKerja != '')
                  @foreach ($desa as $ds)
                    <option value="{{ $ds->id_desa }}" @if($satuanKerja != '') @if($ds->id_desa == $satuanKerja->kelurahan_id) selected @endif @endif>{{ $ds->nama_desa }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });
  $('#inputType').chosen();
  $('#inputPimpinan').chosen();
  $('#inputProvinsi').chosen();
  $('#inputKabupaten').chosen();
  $('#inputKecamatan').chosen();
  $('#inputKelurahan').chosen();

  $('#inputProvinsi').change(function(){
    var id= $('#inputProvinsi').val();
    $.post("{!! route('getKabupaten') !!}", {id:id}).done(function(data){
      var listData = '<option value="" disabled selected> .:: Pilih Kabupaten ::. </option>';
      if(data.length > 0){
        $.each(data, function(k,v){
          listData += '<option value="'+v.id_kabupaten+'">'+v.nama_kabupaten+'</option>';
        });
      }
      $('#inputKabupaten').html(listData);
      $('#inputKabupaten').trigger('chosen:updated');
      var listKecamatan = '<option value="" disabled selected> .:: Pilih Kecamatan ::. </option>';
      $('#inputKecamatan').html(listKecamatan);
      $('#inputKecamatan').trigger('chosen:updated');
      var listDesa = '<option value="" disabled selected> .:: Pilih Kelurahan ::. </option>';
      $('#inputKelurahan').html(listDesa);
      $('#inputKelurahan').trigger('chosen:updated');
    });
  });

  $('#inputKabupaten').change(function(){
    var id= $('#inputKabupaten').val();
    $.post("{!! route('getKecamatan') !!}", {id:id}).done(function(data){
      var listData = '<option value="" disabled selected> .:: Pilih Kecamatan ::. </option>';
      if(data.length > 0){
        $.each(data, function(k,v){
          listData += '<option value="'+v.id_kecamatan+'">'+v.nama_kecamatan+'</option>';
        });
      }
      $('#inputKecamatan').html(listData);
      $('#inputKecamatan').trigger('chosen:updated');
      var listDesa = '<option value="" disabled selected> .:: Pilih Kelurahan ::. </option>';
      $('#inputKelurahan').html(listDesa);
      $('#inputKelurahan').trigger('chosen:updated');
    });
  });

  $('#inputKecamatan').change(function(){
    var id= $('#inputKecamatan').val();
    $.post("{!! route('getDesa') !!}", {id:id}).done(function(data){
      var listData = '<option value="" disabled selected> .:: Pilih Kelurahan ::. </option>';
      if(data.length > 0){
        $.each(data, function(k,v){
          listData += '<option value="'+v.id_desa+'">'+v.nama_desa+'</option>';
        });
      }
      $('#inputKelurahan').html(listData);
      $('#inputKelurahan').trigger('chosen:updated');
    });
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveSatuanKerja') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
