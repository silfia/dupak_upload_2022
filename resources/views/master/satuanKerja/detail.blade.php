<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Satuan Kerja</h5>
        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
      </div>
      <div class="modal-body">
        <section class="panel panel-default m-b-0">
          <div class="panel-body form-row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Kode</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->kode }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Nama</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->nama }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Type Satuan</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->typeSaKer->nama }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Pimpinan</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="@if($satuanKerja->pimpinan_id != 0){{ $satuanKerja->pimpinan->nama }}@else-@endif" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Telepon</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->telepon }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Fax</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->fax }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Email</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->email }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Website</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->website }}" class="form-control backWhite" readonly>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row m-b-15px">
                <label class="col-4 col-lg-3 col-form-label">Alamat</label>
                <div class="col-8 col-lg-9">
                  <textarea rows="2" class="form-control backWhite" readonly>{{ $satuanKerja->alamat }}</textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Layanan</label>
                <div class="col-8 col-lg-9 row">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <label class="custom-control custom-radio custom-control-inline mb-0">
                      <input type="radio" name="jenis_layanan" value="Rawat Inap" class="custom-control-input" @if($satuanKerja->jenis_layanan == 'Rawat Inap') checked @endif disabled><span class="custom-control-label">Rawat Inap</span>
                    </label>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <label class="custom-control custom-radio custom-control-inline mb-0">
                      <input type="radio" name="jenis_layanan" value="Rawat Jalan" class="custom-control-input" @if($satuanKerja->jenis_layanan == 'Rawat Jalan') checked @endif disabled><span class="custom-control-label">Rawat Jalan</span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Kelurahan</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->kelurahan }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Kecamatan</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->kecamatan }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Kabupaten</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->kabupaten }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Provinsi</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $satuanKerja->provinsi }}" class="form-control backWhite" readonly>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '70%'
    });
    $('#detail-dialog').modal('show');
  })();
  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  })
</script>
