<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($type == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($type != "")
        <input type="hidden" name="id_master_type" value="{{ $type->id_master_type }}">
      @endif
      <div class="form-group row">
        <label for="inputNama" class="col-4 col-lg-3 col-form-label">Profesi <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputidProfesi" type="hidden" name="id_profesi" value="{{ $profesi->id_master_type }}" placeholder="Nama Unsur PAK" class="form-control">
          <input id="inputNamaProfesi" type="text" name="Nama_Profesi" value="{{ $profesi->nama }}" placeholder="Nama Unsur PAK" class="form-control" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama Unsur <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputNama" type="text" name="Nama" value="@if($type != ''){{ $type->nama }}@endif" placeholder="Nama Unsur PAK" class="form-control">
        </div>
      </div>

      <div class="form-group row">
          <label for="inputNama" class="col-4 col-lg-3 col-form-label">Permenpan <small style="color:red;">*</small></label>
          <div class="col-8 col-lg-9">
            <select class="form-control" id="permenpan" name="permenpan">              
              @foreach ($permenpan as $permen)
                <option value="{{ $permen->id_permenpan }}" @if($permen->status == 'Aktif') selected @endif>{{ $permen->no_permenpan }}</option>
              @endforeach
            </select>
          </div>
      </div>
      
      <div class="form-group row">
        <label for="inputJenis" class="col-4 col-lg-3 col-form-label">Jenis Unsur <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <label class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="Jenis_Unsur" value="Unsur Utama" class="custom-control-input" @if($type != '') @if($type->keterangan == 'Unsur Utama') checked @endif @endif><span class="custom-control-label">Unsur Utama</span>
            </label>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <label class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="Jenis_Unsur" value="Unsur Penunjang" class="custom-control-input" @if($type != '') @if($type->keterangan == 'Unsur Penunjang') checked @endif @endif><span class="custom-control-label">Unsur Penunjang</span>
            </label>
          </div>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelUsr"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelUsr').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.second-page').fadeOut(function(){
      $('.second-page').empty();
      $('.panel-detail').fadeIn();
    });
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveUnsurPak') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.second-page').fadeOut(function(){
          $('.second-page').empty();
          $('.panel-detail').fadeIn();
          getDataUnsurPAK();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
