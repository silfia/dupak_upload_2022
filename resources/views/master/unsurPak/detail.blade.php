<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-detail">
      <div class="card">
        <h5 class="card-header">Unsur Penetapan Angka Kredit {{ $profesi->nama }}</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-lg-12 mb-2 mb-sm-2">
              <button type="button" class="btn btn-xs btn-primary btn-addUsr"><span class="fas fa-plus"></span> Tambah</button>
              <button type="button" class="btn btn-xs btn-warning btn-updateUsr"><span class="fas fa-pencil-alt"></span> Ubah</button>
              <button type="button" class="btn btn-xs btn-danger btn-deleteUsr"><span class="fas fa-trash"></span> Hapus</button>
              <button type="button" class="btn btn-xs btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table class="table table-striped table-bordered first">
                <thead>
                  <tr>
                    <td width="10px">&nbsp</td>
                    <td width="10px" align='center'>No</td>
                    <td>Nama</td>
                    <td>Unsur</td>
                  </tr>
                </thead>
                <tbody class='resultDataUnsur'></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
    getDataUnsurPAK();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function getDataUnsurPAK() {
    $('.resultDataUnsur').html('<tr><td colspan="4" align="center">Memuat data...</td></tr>');
    var id_profesi = {{ $profesi->id_master_type }};
    $.post("{!! route('loadUnsurPak') !!}", {id_profesi:id_profesi}).done(function(data){
      $('.resultDataUnsur').empty();
      var tag = '';
      if (data.status == 'success') {
        if (data.row.length != 0) {
          var nUsr = 1;
          $.each(data.row, function(k,v){
            tag += '<tr>';
            tag += '<td align="center" class="pl-0 pr-1">';
            tag += '<label class="custom-control custom-checkbox m-0">';
            tag += '<input type="checkbox" name="id_dataUsr[]" id="checkData" value="'+v.id_master_type+'" class="custom-control-input"><span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
            tag += '</label>';
            tag += '</td>';
            tag += '<td align="center">'+nUsr+'</td>';
            tag += '<td>'+v.nama+'</td>';
            tag += '<td>'+v.keterangan+'</td>';
            tag += '</tr>';
            nUsr++;
          });
        }else{
          tag += '<tr><td colspan="4" align="center">Tidak ada data ditemukan...</td></tr>';
        }
        $('.resultDataUnsur').html(tag);
      }else{
        swal("MAAF !","Terjadi Kesalahan !!", "warning");
      }
    }).fail(function() {
      getDataUnsurPAK();
    });
  }

  $('.btn-addUsr').click(function(){
    $('.loading').show();
    $('.panel-detail').hide();
    var id_profesi = {{ $profesi->id_master_type }};
    $.post("{!! route('formUnsurPak') !!}", {id_profesi:id_profesi}).done(function(data){
      if(data.status == 'success'){
        $('.loading').hide();
        $('.second-page').html(data.content).fadeIn();
      } else {
        $('.panel-detail').show();
      }
    });
  });

  $('.btn-updateUsr').click(function(){
    $('.loading').show();
    $('.panel-detail').hide();
    var idData = $(".resultDataUnsur input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length == 1) {
      var id_profesi = {{ $profesi->id_master_type }};
      $.post("{!! route('formUnsurPak') !!}",{id:idData[0],id_profesi:id_profesi}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.second-page').html(data.content).fadeIn();
        } else {
          $('.panel-detail').show();
        }
      });
    }else if (idData.length > 1) {
      swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
      $('.loading').hide();
      $('.panel-detail').show();
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.panel-detail').show();
    }
  });

  $('.btn-deleteUsr').click(function(){
    var idData = $(".resultDataUnsur input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length != 0) {
      swal(
        {
          title: "Apa anda yakin menghapus Data Ini?",
          text: "Data akan dihapus dari sistem!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Saya yakin!",
          cancelButtonText: "Batal!",
          closeOnConfirm: false
        },
        function(){
          $.post("{!! route('removeUnsurPak') !!}", {id:idData}).done(function(data){
            if(data.status == 'success'){
              getDataUnsurPAK();
              swal("Berhasil!", data.message, "success");
            }else{
              swal('Whoops !', data.message, 'warning');
            }
          });
        }
      );
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.main-layer').show();
    }
  });
</script>
