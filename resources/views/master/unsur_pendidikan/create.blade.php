<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($master == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">

  <form class="form-saveStandar">
    <input type="hidden" name="id_penilaian_pendidikan" value="" class="form-control">

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Jenis</label>
        <div class="col-8 col-lg-9">
          <input type="text" name="jenis" value="" placeholder="Jenis" class="form-control">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputStj" class="col-4 col-lg-3 col-form-label">Tingkat Pendidikan</label>
          <div class="col-8 col-lg-9">
            <select id="inputStj" class="form-control" name="mst_id">
              <option disabled selected> .:: Pilih Tingkat ::. </option>
              <option value="" selected ></option>
            </select>
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Nama Penyedia</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="nama_penyedia" value="" placeholder="Nama Penyedia" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Bukti Fisik</label>
          <div class="col-8 col-lg-9">
            <input type="file" name="bukti_fisik" value="" placeholder="Bukti Fisik" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Keterangan</label>
          <div class="col-8 col-lg-9">
            <textarea type="text" name="keterangan" value="" rows="3" placeholder="Keterangan" class="form-control"></textarea>
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Lama Pelaksanaan</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="lama_pelaksanaan" value="" placeholder="Nama Pelaksanaan" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tahun Masuk</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_masuk" value="" placeholder="Tahun Masuk" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tahun Keluar</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_keluar" value="" placeholder="Tahun Keluar" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tanggal</label>
          <div class="col-8 col-lg-9">
            <input type="date" name="tanggal" value="" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Label Tanggal</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_keluar" value="" placeholder="Tahun Keluar" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Label Bulan</label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_keluar" value="" placeholder="Tahun Keluar" class="form-control">
          </div>
      </div>

      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Label Tahun </label>
          <div class="col-8 col-lg-9">
            <input type="text" name="tahun_keluar" value="" placeholder="Tahun Keluar" class="form-control">
          </div>
      </div>

      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelStandar"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submitStandar">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>

  </form>
  </div>
</div>



<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  $('#inputProfesi').change(function(){
    var id= $('#inputProfesi').val();
    $.post("{!! route('getjabatan') !!}", {id:id}).done(function(data){
      var listData = '<option value="" disabled selected> .:: Pilih Tingkatan ::. </option>';
      if(data.length > 0){
        $.each(data, function(k,v){
          listData += '<option value="'+v.id_master+'">'+v.nama+'</option>';
        });
      }
      $('#inputJabatan').html(listData);
      $('#inputJabatan').trigger('chosen:updated');
    });
  });

  $('#inputTglLahir').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });
  $('#inputProfesi').chosen();
  $('#inputSaKer').chosen();
  $('#inputGolongan').chosen();
  $('#inputJabatan').chosen();
  $('#inputGolTmt').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });
  $('#inputJabTmt').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('savePegawai') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
