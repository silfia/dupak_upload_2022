<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
	<h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
	<div class="card-body">
		<form class="form-save">
			<input type="hidden" name="id_pak_master" value="{{ $butir->id_pak_master }}">
			<input type="hidden" name="parent_id" value="@if($parent != ""){{ $parent->id_pak_master }}@endif">
			<input type="hidden" name="unsur_id" value="{{ $unsur->id_master_type }}">
			<input type="hidden" name="id_permenpan" value="{{ $permenpan->id_permenpan }}">
			<input type="hidden" name="id_profesi" value="{{ $profesi->id_master_type }}">
			<div class="form-row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group row">
						<label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Permenpan</label>
						<div class="col-8 col-lg-9">
							<input id="inputProfesi" type="text" name="permenpan" value="{{ $permenpan->no_permenpan }}" class="form-control backWhite" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
						<div class="col-8 col-lg-9">
							<input id="inputProfesi" type="text" name="Profesi" value="{{ $profesi->nama }}" class="form-control backWhite" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Unsur Kegiatan</label>
						<div class="col-8 col-lg-9">
							<input id="inputProfesi" type="text" name="unsur" value="{{ $unsur->nama }}" class="form-control backWhite" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-12 col-form-label p-t-0 p-b-0">Parent Butir Kegiatan :</label>
						@if(count($infoParent) > 0)
							@for($i=0;$i<count($infoParent);$i++)
								@php
									$tanda = '- ';
									switch ($i) {
										case 0: $jarakButir = ''; $tanda = ''; break;
										case 1: $jarakButir = 'margin-left:10px;'; break;
										case 2: $jarakButir = 'margin-left:20px;'; break;
										case 3: $jarakButir = 'margin-left:30px;'; break;
										case 4: $jarakButir = 'margin-left:40px;'; break;
										case 5: $jarakButir = 'margin-left:50px;'; break;
										case 6: $jarakButir = 'margin-left:60px;'; break;
										case 7: $jarakButir = 'margin-left:70px;'; break;
										default: $jarakButir = ''; break;
									}
								@endphp
								<label class="col-lg-11 offset-lg-1 col-form-label p-t-0 p-b-0"><span style="{{ $jarakButir }}">{{ $tanda }}{{ $infoParent[$i]['nama'] }}</span></label>
							@endfor
						@else
							<label class="col-lg-11 offset-lg-1 col-form-label p-t-0 p-b-0">-</label>
						@endif
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group row">
						<label for="inputJudul" class="col-4 col-lg-3 col-form-label">Hanya Judul</label>
						<div class="col-8 col-lg-9 row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label class="custom-control custom-radio custom-control-inline mb-0">
									<input type="radio" name="is_title" value="1" class="custom-control-input" onclick="judulYa()" @if($butir->is_title == '1') checked @endif><span class="custom-control-label">Ya</span>
								</label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label class="custom-control custom-radio custom-control-inline mb-0">
									<input type="radio" name="is_title" value="0" class="custom-control-input" onclick="judulTidak()" @if($butir->is_title == '0') checked @endif><span class="custom-control-label">Tidak</span>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputButir" class="col-4 col-lg-3 col-form-label">Butir Kegiatan</label>
						<div class="col-8 col-lg-9">
							<input id="inputButir" type="text" name="nama_butir" value="{{ $butir->butir_kegiatan }}" placeholder="Nama Butir Kegiatan" class="form-control">
						</div>
					</div>
					<span class="panel-pointButir" @if($butir->is_title == 1) style="display:none;" @endif>
						<div class="form-group row">
							<label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
							<div class="col-8 col-lg-9">
								<select id="inputpJabatan" class="form-control" name="jabatan">
									<option value="" disabled selected> .:: Pilih Jabatan ::. </option>
									@foreach ($jabatan as $jbt)
										<option value="{{ $jbt->id_master }}" @if($butir->jabatan_id == $jbt->id_master) selected @endif>{{ $jbt->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="inputJml" class="col-4 col-lg-3 col-form-label">Jumlah Minimal</label>
							<div class="col-8 col-lg-9">
								<input id="inputJml" type="number" name="minimal" value="{{ $butir->jum_min }}" placeholder="Jumlah Minimal Kegiatan" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputJml" class="col-4 col-lg-3 col-form-label">Satuan</label>
							<div class="col-8 col-lg-9">
								<input id="inputJml" type="text" name="satuan" value="{{ $butir->satuan }}" placeholder="Satuan Kegiatan" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputJml" class="col-4 col-lg-3 col-form-label">Jumlah Point</label>
							<div class="col-8 col-lg-9">
								<input id="inputJml" type="number" name="point" value="{{ $butir->points }}" placeholder="Jumlah Point" class="form-control">
							</div>
						</div>
					</span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 choosBukti" @if($butir->is_title == 1) style="display:none;" @endif>
					<div class="form-group row">
						<label for="inputJudul" class="col-4 col-lg-3 col-form-label">Menggunakan Bukti Fisiki</label>
						<div class="col-8 col-lg-9 row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label class="custom-control custom-radio custom-control-inline mb-0">
									<input type="radio" name="is_bukti" value="Ya" class="custom-control-input" onclick="buktiYa()" @if($butir->is_bukti == 'Ya') checked @endif><span class="custom-control-label">Ya</span>
								</label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label class="custom-control custom-radio custom-control-inline mb-0">
									<input type="radio" name="is_bukti" value="Tidak" class="custom-control-input" onclick="buktiTidak()" @if($butir->is_bukti == 'Tidak') checked @endif><span class="custom-control-label">Tidak</span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panelBuktiList row" @if($butir->is_title == 1 || ($butir->is_title == 0 && $butir->is_bukti == 'Tidak')) style="display:none;" @endif>
					@foreach($master_bukti as $bukti)
						@php
							$checkBukti = '';
							foreach ($buktiFisik as $keyBukti) {
								if ($bukti->id_master_bukti == $keyBukti->master_bukti_fisik_id) {
									$checkBukti = 'checked';
								}
							}
						@endphp
						<div class="col-lg-4 col-md-4 col-sm-6">
							<label class="custom-control custom-radio custom-control-inline mb-0">
								<input type="checkbox" name="id_master_bukti[]" value="{{ $bukti->id_master_bukti }}" class="custom-control-input" {{ $checkBukti }}><span class="custom-control-label">{{ $bukti->nama_bukti }}</span>
							</label>
						</div>
					@endforeach
				</div>
			</div>
			<div class="row pt-2 pt-sm-2 mt-1">
				<div class="col-sm-12 pl-0">
					<p class="text-right">
						<button class="btn btn-sm btn-space btn-secondary btn-cancelButir"><span class="fas fa-chevron-left"></span> Kembali</button>
						<button type="submit" class="btn btn-sm btn-space btn-primary btn-submitButir">Simpan <span class="fas fa-save"></span></button>
					</p>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	var onLoad = (function() {
		$('.panel-form').animateCss('bounceInUp');
	})();

	$('.btn-cancelButir').click(function(e){
		e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.second-page').fadeOut(function(){
			$('.second-page').empty();
			$('.panel-detail').fadeIn();
		});
	});

	function judulYa() { $('.panel-pointButir').hide(); $('.choosBukti').hide(); }
	function judulTidak() { $('.panel-pointButir').show(); $('.choosBukti').show(); }
	$('#inputpJabatan').chosen();
	function buktiYa() { $('.panelBuktiList').show(); }
	function buktiTidak() { $('.panelBuktiList').hide(); }

	$('.btn-submitButir').click(function(e){
		e.preventDefault();
		$('.btn-submitButir').html('Please wait...').attr('disabled', true);
		var data  = new FormData($('.form-save')[0]);
		$.ajax({
			url: "{{ route('updateButirKegiatanNew') }}",
			type: 'POST',
			data: data,
			async: true,
			cache: false,
			contentType: false,
			processData: false
		}).done(function(data){
			$('.form-save').validate(data, 'has-error');
			if(data.status == 'success'){
				swal("Success !", data.message, "success");
				$('.second-page').fadeOut(function(){
					$('.second-page').empty();
					$('.panel-detail').fadeIn();
					getDataButir();
				});
			} else if(data.status == 'error') {
				$('.btn-submitButir').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
				swal('Whoops !', data.message, 'warning');
			} else {
				var n = 0;
				for(key in data){
					if (n == 0) {var dt0 = key;}
					n++;
				}
				$('.btn-submitButir').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
				swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
			}
		}).fail(function() {
			swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
			$('.btn-submitButir').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
		});
	});
</script>