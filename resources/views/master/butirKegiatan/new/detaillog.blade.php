<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Log Aktivitas Perubahan Butir Kegiatan</h5>
        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
      </div>
      <div class="modal-body">
        <section class="panel panel-default m-b-0">
          <div class="panel-body form-row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table class="table">
                <tbody>
                  <tr>
                    <th>Tanggal Aktivitas</th>
                    <td>{{ $log->created_at }}</td>
                  </tr>
                  <tr>
                    <th>Tipe Aktivitas</th>
                    <td>{{ $log->action_type }}</td>
                  </tr>
                  <tr>
                    <th>Nama User</th>
                    <td>{{ $log->created_name }}</td>
                  </tr>
                  <tr>
                    <th>Keterangan</th>
                    <td>{{ $log->message }}</td>
                  </tr>
                  <tr>
                    <th>Uraian Butir Kegiatan yang Diubah</th>
                    <td>{{ $log->uraian_butir_kegiatan }}</td>
                  </tr>
                </tbody>
              </table>             
            </div>           
          </div>
        </section>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '70%'
    });
    $('#detail-dialog').modal('show');
  })();
  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  })
</script>
