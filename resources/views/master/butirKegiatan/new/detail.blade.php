<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-detail">
      <div class="card">
        <h5 class="card-header">Butir Kegiatan</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-lg-12 mb-2 mb-sm-2">
              <!-- <button type="button" class="btn btn-xs btn-primary btn-addButir"><span class="fas fa-plus"></span> Tambah</button>
              <button type="button" class="btn btn-xs btn-warning btn-updateBK"><span class="fas fa-pencil-alt"></span> Ubah</button>
              <button type="button" class="btn btn-xs btn-danger btn-deleteBK"><span class="fas fa-trash"></span> Hapus</button>
               --><button type="button" class="btn btn-xs btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
              <a href="javascript:void(0)" class="btn btn-info btn-xs btn-excel" onclick="ExportToExcel('my-table-id', 'Format Excel')"><i class="fas fa-file-excel"></i> Excel</a>
            </div>
            <div id="my-table-id">
            <div class="col-lg-4 col-md-5 col-sm-12">
              <div class="form-group row">
                <label for="permenpan" class="col-4 col-lg-3 col-form-label">Permenpan</label>
                <div class="col-8 col-lg-9">
                  <select class="form-control" id="id_permenpan">
                    @foreach ($permenpan as $permen)
                      <option value="{{ $permen->id_permenpan }}" @if($permen->status == 'Aktif') selected @endif>{{ $permen->no_permenpan }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table class="table table-striped table-bordered first">
                <thead>
                  <tr>
                    <td width="40px">&nbsp</td>
                    <td width="40px" align='center'>No</td>
                    <td colspan="21" width="750px">Nama</td>
                    <td width="180px">Point</td>
                    <td width="200px">Golongan</td>
                  </tr>
                </thead>
                <tbody class='resultDataButir'></tbody>
              </table>
              <div class="clearfix"></div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
    getDataButir();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  $('#id_permenpan').chosen();

  function getDataButir() {
    $('.resultDataButir').html('<tr><td colspan="25" align="center">Memuat data...</td></tr>');
    var id_profesi = {{ $id_profesi }};
    var id_permenpan = $('#id_permenpan').val();
    $.post("{!! route('loadButirKegiatanNew') !!}", {id_profesi:id_profesi,id_permenpan:id_permenpan}).done(function(result){
      var tag = '';
      if (result.status == 'success') {
        $.each(result.row.butir, function(k,uns){
          tag += '<tr>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td colspan="21" width="800px" style="font-weight:bold;position:relative;padding-right:65px;">'+uns.nama
          tag += '<span class="panelTombolAksi">';
          tag += '<a href="javascript:void(0)" onclick="tambahButir('+id_profesi+','+uns.id_master_type+',0,0)" class="btn btn-primary btn-tambahButir"><span class="fas fa-plus"></span></a>';
          tag += '</span>';
          tag += '</td>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '</tr>';
          if (uns.row.length > 0) {
            var tempNo1 = 0;
            $.each(uns.row, function(k,v1){
              tempNo1++;
              tag += tagRow(v1, KonDecRomawi(tempNo1), id_profesi, uns.id_master_type, v1.statusRow);
              if (v1.row.length > 0) {
                var tempNo2 = 0; // Hurug Awal
                var tempNo2n = 0; // Hurug Akhir
                var No2 = '';
                var No2n = '';
                $.each(v1.row, function(k,v2){
                  if (tempNo2n == 26) {
                    tempNo2++;
                    No2 = String.fromCharCode(64 + tempNo2);
                  }
                  tempNo2n++;
                  No2n = No2+''+String.fromCharCode(64 + tempNo2n);
                  tag += tagRow(v2, No2n, id_profesi, uns.id_master_type, v2.statusRow);
                  if (v2.row.length > 0) {
                    var tempNo3 = 0;
                    $.each(v2.row, function(k,v3){
                      tempNo3++;
                      tag += tagRow(v3, tempNo3, id_profesi, uns.id_master_type, v3.statusRow);
                      if (v3.row.length > 0) {
                        var tempNo4 = 0; // Hurug Awal
                        var tempNo4n = 0; // Hurug Akhir
                        var No4 = '';
                        var No4n = '';
                        $.each(v3.row, function(k,v4){
                          if (tempNo4n == 26) {
                            tempNo4++;
                            No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                          }
                          tempNo4n++;
                          No4n = No4+''+String.fromCharCode(64 + tempNo4n).toLowerCase();
                          tag += tagRow(v4, No4n,  id_profesi, uns.id_master_type, v4.statusRow);
                          if (v4.row.length > 0) {
                            var No5 = '<i class="fas fa-angle-double-right"></i>';
                            $.each(v4.row, function(k,v5){
                              tag += tagRow(v5, No5, id_profesi, uns.id_master_type, v5.statusRow);
                              if (v5.row.length > 0) {
                                var No6 = '<i class="fas fa-terminal"></i>';
                                $.each(v5.row, function(k,v6){
                                  tag += tagRow(v6, No6, id_profesi, uns.id_master_type, v6.statusRow);
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }else{
            tag += '<tr><td colspan="25" align="center">Tidak ada data ditemukan...</td></tr>';
          }
        });
      }else{
        tag += '<tr><td colspan="25" align="center">Tidak ada data ditemukan...</td></tr>';
      }
      $('.resultDataButir').empty();
      $('.resultDataButir').html(tag);
    }).fail(function() {
      getDataMasterLampiran();
    });
  }

  function getDataButirOld() {
    $('.resultDataButir').html('<tr><td colspan="25" align="center">Memuat data...</td></tr>');
    var id_profesi = {{ $id_profesi }};
    $.post("{!! route('loadButirKegiatan') !!}", {id_profesi:id_profesi}).done(function(data){
      if (data.status == 'success') {
        var tag = '';
        // var No = '';
        // var currentCode = 0;
        if (data.row.butir.length != 0) {
          $.each(data.row.butir, function(k,uns){
            tag += '<tr>';
            tag += '<td></td>';
            tag += '<td></td>';
            tag += '<td colspan="21" width="800px" style="font-weight:bold">'+uns.nama+'</td>';
            tag += '<td></td>';
            tag += '<td></td>';
            tag += '</tr>';
            if (uns.row.length > 0) {
              var tempNo1 = 0;
              $.each(uns.row, function(k,v1){
                tempNo1++;
                if (v1.statusRow == 'Parent') {
                  tag += '<tr>';
                  tag += '<td align="center" class="pl-0 pr-1">';
                  tag += '<label class="custom-control custom-checkbox m-0">';
                  tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v1.id_pak_master+'" class="custom-control-input">';
                  tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                  tag += '</label>';
                  tag += '</td>';
                  tag += '<td align="center" style="vertical-align:top">'+KonDecRomawi(tempNo1)+'.</td>';
                  tag += '<td colspan="21" width="800px">'+v1.butir_kegiatan+'</td>';
                  tag += '<td></td>';
                  tag += '<td></td>';
                  tag += '</tr>';
                  if (v1.row.length > 0) {
                    var tempNo2 = 0; // Hurug Awal
                    var tempNo2n = 0; // Hurug Akhir
                    var No2 = '';
                    var No2n = '';
                    $.each(v1.row, function(k,v2){
                      if (tempNo2n == 26) {
                        tempNo2++;
                        No2 = String.fromCharCode(64 + tempNo2);
                      }
                      tempNo2n++;
                      No2n = String.fromCharCode(64 + tempNo2n);
                      if (v2.statusRow == 'Parent') {
                        tag += '<tr>';
                        tag += '<td align="center" class="pl-0 pr-1">';
                        tag += '<label class="custom-control custom-checkbox m-0">';
                        tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v2.id_pak_master+'" class="custom-control-input">';
                        tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                        tag += '</label>';
                        tag += '</td>';
                        tag += '<td></td>';
                        tag += '<td align="center" style="vertical-align:top">'+No2+''+No2n+'.</td>';
                        tag += '<td colspan="20" width="760px">'+v2.butir_kegiatan+'</td>';
                        tag += '<td></td>';
                        tag += '<td></td>';
                        tag += '</tr>';
                        if (v2.row.length > 0) {
                          var tempNo3 = 0;
                          $.each(v2.row, function(k,v3){
                            tempNo3++;
                            if (v3.statusRow == 'Parent') {
                              tag += '<tr>';
                              tag += '<td align="center" class="pl-0 pr-1">';
                              tag += '<label class="custom-control custom-checkbox m-0">';
                              tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v3.id_pak_master+'" class="custom-control-input">';
                              tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                              tag += '</label>';
                              tag += '</td>';
                              tag += '<td></td>';
                              tag += '<td></td>';
                              tag += '<td align="center" style="vertical-align:top">'+tempNo3+'.</td>';
                              tag += '<td colspan="19" width="720px">'+v3.butir_kegiatan+'</td>';
                              tag += '<td></td>';
                              tag += '<td></td>';
                              tag += '</tr>';
                              if (v3.row.length > 0) {
                                var tempNo4 = 0; // Hurug Awal
                                var tempNo4n = 0; // Hurug Akhir
                                var No4 = '';
                                var No4n = '';
                                $.each(v3.row, function(k,v4){
                                  if (tempNo4n == 26) {
                                    tempNo4++;
                                    No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                                  }
                                  tempNo4n++;
                                  No4n = String.fromCharCode(64 + tempNo4n).toLowerCase();
                                  if (v4.statusRow == 'Parent') {
                                    tag += '<tr>';
                                    tag += '<td align="center" class="pl-0 pr-1">';
                                    tag += '<label class="custom-control custom-checkbox m-0">';
                                    tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v4.id_pak_master+'" class="custom-control-input">';
                                    tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                    tag += '</label>';
                                    tag += '</td>';
                                    tag += '<td></td>';
                                    tag += '<td></td>';
                                    tag += '<td></td>';
                                    tag += '<td align="center" style="vertical-align:top">'+No4+''+No4n+'.</td>';
                                    tag += '<td colspan="18" width="680px">'+v4.butir_kegiatan+'</td>';
                                    tag += '<td></td>';
                                    tag += '<td></td>';
                                    tag += '</tr>';
                                    if (v4.row.length > 0) {
                                      var No5 = '<i class="fas fa-angle-double-right"></i>';
                                      $.each(v4.row, function(k,v5){
                                        if (v5.statusRow == 'Parent') {
                                          tag += '<tr>';
                                          tag += '<td align="center" class="pl-0 pr-1">';
                                          tag += '<label class="custom-control custom-checkbox m-0">';
                                          tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v5.id_pak_master+'" class="custom-control-input">';
                                          tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                          tag += '</label>';
                                          tag += '</td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '<td align="center" style="vertical-align:top">'+No5+'</td>';
                                          tag += '<td colspan="17" width="640px">'+v5.butir_kegiatan+'</td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '</tr>';
                                          if (v5.row.length > 0) {
                                            var No6 = '<i class="fas fa-terminal"></i>';
                                            $.each(v5.row, function(k,v6){
                                              if (v6.statusRow == 'Parent') {
                                                tag += '<tr>';
                                                tag += '<td align="center" class="pl-0 pr-1">';
                                                tag += '<label class="custom-control custom-checkbox m-0">';
                                                tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v6.id_pak_master+'" class="custom-control-input">';
                                                tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                                tag += '</label>';
                                                tag += '</td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td align="center" style="vertical-align:top">'+No6+'</td>';
                                                tag += '<td colspan="16" width="600px">'+v6.butir_kegiatan+'</td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '</tr>';
                                              }else{
                                                tag += '<tr>';
                                                tag += '<td align="center" class="pl-0 pr-1">';
                                                tag += '<label class="custom-control custom-checkbox m-0">';
                                                tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v6.id_pak_master+'" class="custom-control-input">';
                                                tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                                tag += '</label>';
                                                tag += '</td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td></td>';
                                                tag += '<td align="center" style="vertical-align:top">'+No6+'</td>';
                                                tag += '<td colspan="16" width="600px">'+v6.butir_kegiatan+'</td>';
                                                tag += '<td>( Tiap '+v6.jum_min+' '+v6.satuan+' '+v6.points+' )</td>';
                                                tag += '<td>'+v6.jabatan.nama+'</td>';
                                                tag += '</tr>';
                                              }
                                            });
                                          }
                                        }else{
                                          tag += '<tr>';
                                          tag += '<td align="center" class="pl-0 pr-1">';
                                          tag += '<label class="custom-control custom-checkbox m-0">';
                                          tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v5.id_pak_master+'" class="custom-control-input">';
                                          tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                          tag += '</label>';
                                          tag += '</td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '<td></td>';
                                          tag += '<td align="center" style="vertical-align:top">'+No5+'</td>';
                                          tag += '<td colspan="17" width="640px">'+v5.butir_kegiatan+'</td>';
                                          tag += '<td>( Tiap '+v5.jum_min+' '+v5.satuan+' '+v5.points+' )</td>';
                                          tag += '<td>'+v5.jabatan.nama+'</td>';
                                          tag += '</tr>';
                                        }
                                      });
                                    }
                                  }else{
                                    tag += '<tr>';
                                    tag += '<td align="center" class="pl-0 pr-1">';
                                    tag += '<label class="custom-control custom-checkbox m-0">';
                                    tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v4.id_pak_master+'" class="custom-control-input">';
                                    tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                                    tag += '</label>';
                                    tag += '</td>';
                                    tag += '<td></td>';
                                    tag += '<td></td>';
                                    tag += '<td></td>';
                                    tag += '<td align="center" style="vertical-align:top">'+No4+''+No4n+'.</td>';
                                    tag += '<td colspan="18" width="680px">'+v4.butir_kegiatan+'</td>';
                                    tag += '<td>( Tiap '+v4.jum_min+' '+v4.satuan+' '+v4.points+' )</td>';
                                    tag += '<td>'+v4.jabatan.nama+'</td>';
                                    tag += '</tr>';
                                  }
                                });
                              }
                            }else{
                              tag += '<tr>';
                              tag += '<td align="center" class="pl-0 pr-1">';
                              tag += '<label class="custom-control custom-checkbox m-0">';
                              tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v3.id_pak_master+'" class="custom-control-input">';
                              tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                              tag += '</label>';
                              tag += '</td>';
                              tag += '<td></td>';
                              tag += '<td></td>';
                              tag += '<td align="center" style="vertical-align:top">'+tempNo3+'.</td>';
                              tag += '<td colspan="19" width="720px">'+v3.butir_kegiatan+'</td>';
                              tag += '<td>( Tiap '+v3.jum_min+' '+v3.satuan+' '+v3.points+' )</td>';
                              tag += '<td>'+v3.jabatan.nama+'</td>';
                              tag += '</tr>';
                            }
                          });
                        }
                      }else{
                        tag += '<tr>';
                        tag += '<td align="center" class="pl-0 pr-1">';
                        tag += '<label class="custom-control custom-checkbox m-0">';
                        tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v2.id_pak_master+'" class="custom-control-input">';
                        tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                        tag += '</label>';
                        tag += '</td>';
                        tag += '<td></td>';
                        tag += '<td align="center" style="vertical-align:top">'+No2+''+No2n+'.</td>';
                        tag += '<td colspan="20" width="760px">'+v2.butir_kegiatan+'</td>';
                        tag += '<td>( Tiap '+v2.jum_min+' '+v2.satuan+' '+v2.points+' )</td>';
                        tag += '<td>'+v2.jabatan.nama+'</td>';
                        tag += '</tr>';
                      }
                    });
                  }
                }else{
                  tag += '<tr>';
                  tag += '<td align="center" class="pl-0 pr-1">';
                  tag += '<label class="custom-control custom-checkbox m-0">';
                  tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+v1.id_pak_master+'" class="custom-control-input">';
                  tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
                  tag += '</label>';
                  tag += '</td>';
                  tag += '<td align="center" style="vertical-align:top">'+KonDecRomawi(tempNo1)+'.</td>';
                  tag += '<td colspan="21" width="800px">'+v1.butir_kegiatan+'</td>';
                  tag += '<td>( Tiap '+v1.jum_min+' '+v1.satuan+' '+v1.points+' )</td>';
                  tag += '<td>'+v1.jabatan.nama+'</td>';
                  tag += '</tr>';
                }
              });
            }
          });
        }else{
          tag += '<tr><td colspan="25" align="center">Tidak ada data ditemukan...</td></tr>';
        }
        $('.resultDataButir').empty();
        $('.resultDataButir').html(tag);
      } else if(data.status == 'empty') {
        tag += '<tr><td colspan="25" align="center">Tidak ada data ditemukan...</td></tr>';
        $('.resultDataButir').empty();
        $('.resultDataButir').html(tag);
      } else {
        swal("MAAF !","Terjadi Kesalahan !!", "warning");
      }
    }).fail(function() {
      getDataMasterLampiran();
    });
  }

  function KonDecRomawi(angka) {
    var hasil = "";
    if (angka < 1 || angka > 5000) {
      hasil += "Batas Angka 1 s/d 5000";
    }else{
      while (angka >= 1000) {
        hasil += "M";
        angka = angka - 1000;
      }
    }

    if (angka >= 500) {
      if (angka > 500) {
        if (angka >= 900) {
          hasil += "CM";
          angka = angka - 900;
        } else {
          hasil += "D";
          angka = angka - 500;
        }
      }
    }

    while (angka >= 100) {
      if (angka >= 400) {
        hasil += "CD";
        angka = angka - 400;
      } else {
        angka = angka - 100;
      }
    }

    if (angka >= 50) {
      if (angka >= 90) {
        hasil += "XC";
        angka = angka - 90;
      } else {
        hasil += "L";
        angka = angka - 50;
      }
    }

    while (angka >= 10) {
      if (angka >= 40) {
        hasil += "XL";
        angka = angka - 40;
      } else {
        hasil += "X";
        angka = angka - 10;
      }
    }

    if (angka >= 5) {
      if (angka == 9) {
        hasil += "IX";
        angka = angka - 9;
      } else {
        hasil += "V";
        angka = angka - 5;
      }
    }

    while (angka >= 1) {
      if (angka == 4) {
        hasil += "IV";
        angka = angka - 4;
      } else {
        hasil += "I";
        angka = angka - 1;
      }
    }
    return hasil;
  }

  function tagRow(keyData, nomor, id_profesi, id_unsur, status) {
    var tag = '';
    tag += '<tr>';
      if (status == 'Parent') {
        var point = ' ';
        var golongan = ' ';
      }else{
        var point = '( Tiap '+keyData.jum_min+' '+keyData.satuan+' '+keyData.points+' )';
        var golongan = keyData.jabatan.nama;
      }
      tag += '<td align="center" class="pl-0 pr-1">';
      tag += '<label class="custom-control custom-checkbox m-0">';
      tag += '<input type="checkbox" name="id_dataBK[]" id="checkData" value="'+keyData.id_pak_master+'" class="custom-control-input">';
      tag += '<span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
      tag += '</label>';
      tag += '</td>';
      switch(keyData.level) {
        case 1:
          tag += '<td align="center" style="vertical-align:top">'+nomor+'.</td>';
          var jmlColspan = '21';
          var jmlWidth = '800px';
          break;
        case 2:
          tag += '<td></td>';
          tag += '<td align="center" style="vertical-align:top">'+nomor+'.</td>';
          var jmlColspan = '20';
          var jmlWidth = '760px';
          break;
        case 3:
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td align="center" style="vertical-align:top">'+nomor+'.</td>';
          var jmlColspan = '19';
          var jmlWidth = '720px';
          break;
        case 4:
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td align="center" style="vertical-align:top">'+nomor+'.</td>';
          var jmlColspan = '18';
          var jmlWidth = '680px';
          break;
        case 5:
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td align="center" style="vertical-align:top">'+nomor+'.</td>';
          var jmlColspan = '17';
          var jmlWidth = '640px';
          break;
        case 6:
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td></td>';
          tag += '<td align="center" style="vertical-align:top">'+nomor+'.</td>';
          var jmlColspan = '16';
          var jmlWidth = '600px';
          break;
      }
      tag += '<td colspan="'+jmlColspan+'" width="'+jmlWidth+'" style="font-weight:bold;position:relative;padding-right:65px;">'+keyData.butir_kegiatan;
      tag += '<span class="panelTombolAksi">';
      if (status == 'Parent') {
        tag += '<a href="javascript:void(0)" onclick="tambahButir(\''+id_profesi+'\',\''+id_unsur+'\','+keyData.id_pak_master+',0)" class="btn btn-primary btn-tambahButir"><span class="fas fa-plus"></span></a>';
      }
      tag += '<a href="javascript:void(0)" onclick="updateButir(\''+id_profesi+'\',\''+id_unsur+'\','+keyData.parent_id+','+keyData.id_pak_master+')" class="btn btn-warning btn-tambahButir m-l-5px"><span class="fas fa-pencil-alt"></span></a>';
          tag += '</span>';
      tag += '</td>';
      tag += '<td>'+point+'</td>';
      tag += '<td>'+golongan+'</td>';
      tag += '</tr>';

      return tag;
  }

  function tambahButir(id_profesi, unsur_id, parent_id, id_pak) {
    $('.loading').show();
    $('.panel-detail').hide();
    $.post("{!! route('formButirKegiatanNew') !!}", {id_profesi:id_profesi,unsur_id:unsur_id,parent_id:parent_id,id_pak:id_pak}).done(function(data){
      if(data.status == 'success'){
        $('.loading').hide();
        $('.second-page').html(data.content).fadeIn();
      } else {
        $('.panel-detail').show();
      }
    });
  }

  function updateButir(id_profesi, unsur_id, parent_id, id_pak) {
    $('.loading').show();
    $('.panel-detail').hide();
    $.post("{!! route('formupdateButirKegiatanNew') !!}", {id_profesi:id_profesi,unsur_id:unsur_id,parent_id:parent_id,id_pak:id_pak}).done(function(data){
      if(data.status == 'success'){
        $('.loading').hide();
        $('.second-page').html(data.content).fadeIn();
      } else {
        $('.panel-detail').show();
      }
    });
  }

  $('.btn-updateBK').click(function(){
    $('.loading').show();
    $('.panel-detail').hide();
    var idData = $(".resultDataButir input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length == 1) {
      var id_profesi = {{ $id_profesi }};
      $.post("{!! route('formButirKegiatan') !!}",{id:idData[0],id_profesi:id_profesi}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.second-page').html(data.content).fadeIn();
        } else {
          $('.panel-detail').show();
        }
      });
    }else if (idData.length > 1) {
      swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
      $('.loading').hide();
      $('.panel-detail').show();
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.panel-detail').show();
    }
  });

  $('.btn-deleteBK').click(function(){
    var idData = $(".resultDataButir input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length != 0) {
      swal(
        {
          title: "Apa anda yakin menghapus Data Ini?",
          text: "Data akan dihapus dari sistem!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Saya yakin!",
          cancelButtonText: "Batal!",
          closeOnConfirm: false
        },
        function(){
          $.post("{!! route('removeButirKegiatan') !!}", {id:idData}).done(function(data){
            if(data.status == 'success'){
              getDataButir();
              swal("Berhasil!", data.message, "success");
            }else{
              swal('Whoops !', data.message, 'warning');
            }
          });
        }
      );
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.main-layer').show();
    }
  });
   var ExportToExcel = (function() {
     var uri = 'data:application/vnd.ms-excel;base64,'
       , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
       , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
       , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
     return function(table, name) {
       if (!table.nodeType) table = document.getElementById(table)
       var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
       var downloadName = prompt('nama file yang akan di download', 'Format Excel');
       var link = document.createElement("a");
       link.download = downloadName + ".xls";
       // window.location.href = uri + base64(format(template, ctx))
       link.href = uri + base64(format(template, ctx));
       link.click();
     }
   })()
</script>
