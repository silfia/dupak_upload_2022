@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item">Master</li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12 col-lg-6 mb-2 mb-sm-2">
                <button type="button" class="btn btn-xs btn-info btn-detail"><span class="fas fa-search"></span> Detail</button>
              </div>
             
             <div class="col-md-12 col-sm-12 col-xs-12 main-layer panelSearch m-b-10">
                  <div class="form-inline pos-absolute right-10" style="float:right">
                      <div class="form-group">
                          <select class="form-control-sm form-control input-s-sm inline v-middle option-search" id="search-option">
                          </select>
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control-sm form-control" placeholder="Search" id="search">
                      </div>
                  </div>
              </div>
              <div class='clearfix'></div>

              <div class="col-md-12 p-0">
                  <div class="table-responsive">
                      <table class="table table-striped b-t b-light" id="datagrid"></table>
                  </div>
                  <footer class="panel-footer">
                      <div class="row">
                          <div class="col-sm-1 hidden-xs">
                              <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                          </div>
                          <div class="col-sm-6 text-center">
                              <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                          </div>
                          <div class="col-sm-5 text-right text-center-xs">
                              <ul class="pagination pagination-sm justify-content-center float-r m-t-0 m-b-0" id="paging"></ul>
                          </div>
                      </div>
                  </footer>
              </div>
              <div class='clearfix'></div>

            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script src="{{ url('/')}}/js/cusLoadData.js"></script>
  <script type="text/javascript">
     var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('loadUserLogButirKegiatan') !!}",
      primaryField          : 'id_activity',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      columns               : [
        {field: 'checks', title: '&nbsp', editable: false, sortable: false, width: 30, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return checks(rowData, rowIndex);
          }
        },
        {field: 'created_at', title: 'Tgl Aktivitas', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'action_type', title: 'Tipe Aktivitas', editable: false, sortable: true, width: 100, align: 'left', search: true},                    
        {field: 'message', title: 'Keterangan', editable: false, sortable: true, width: 200, align: 'left', search: true},
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

    function checks(rowData, rowIndex) {
      var id = rowData.id_activity;
      var tag = '';
      tag += '<input type="checkbox" value="'+id+'" name="id_data[]" id="id_data'+id+'"/>';
      tag += '<label for="id_data'+id+'"></label>';
      return tag;
    }   

    $('.btn-detail').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('detailLogButirKegiatan') !!}",{id:idData[0]}).done(function(data){
          if(data.status == 'success'){
            $('.modal-dialog').html(data.content);
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      }
    });    
  </script>
@stop
