<div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  @if($kegiatan == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($kegiatan != "")
        <input type="hidden" name="id_pak_master" value="{{ $kegiatan->id_pak_master }}">
      @endif
      <div class="form-row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
            <div class="col-8 col-lg-9">
              <input id="inputProfesi" type="text" name="Profesi" value="{{ $profesi->nama }}" class="form-control backWhite" readonly>
              <input id="inputIDProfesi" type="hidden" name="id_profesi" value="{{ $profesi->id_master_type }}" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputUnsur" class="col-4 col-lg-3 col-form-label">Unsur</label>
            <div class="col-8 col-lg-9">
              <select id="inputUnsur" class="form-control" name="Unsur">
                <option value="" disabled selected> .:: Pilih Unsur ::. </option>
                @foreach ($unsurs as $unsur)
                  <option value="{{ $unsur->id_master_type }}" @if($kegiatan != '') @if($kegiatan->unsur_pak_id == $unsur->id_master_type) selected @endif @endif>{{ $unsur->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama</label>
            <div class="col-8 col-lg-9">
              <input id="inputNama" type="text" name="Nama" value="@if($kegiatan != ''){{ $kegiatan->butir_kegiatan }}@endif" placeholder="Nama Butir" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputKeterangan" class="col-4 col-lg-3 col-form-label">Keterangan</label>
            <div class="col-8 col-lg-9">
              <textarea id="inputKeterangan" name="Keterangan" rows="2" placeholder="Keterangan" class="form-control">@if($kegiatan != ''){{ $kegiatan->keterangan }}@endif</textarea>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group row">
            <label for="inputTurunan" class="col-4 col-lg-3 col-form-label">Turunan</label>
            <div class="col-8 col-lg-9 row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="Turunan" value="1" class="custom-control-input" onclick="turunanYa()" @if($kegiatan != '') @if($kegiatan->is_child == '1') checked @endif @endif><span class="custom-control-label">Ya</span>
                </label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="Turunan" value="0" class="custom-control-input" onclick="turunanTidak()" @if($kegiatan != '') @if($kegiatan->is_child == '0') checked @endif @else checked @endif><span class="custom-control-label">Tidak</span>
                </label>
              </div>
            </div>
          </div>
          <span class="panel-parents" @if($kegiatan != '' && $kegiatan->is_child == 1) &nbsp @else style="display:none;" @endif>
            <div class="form-group row">
              <label for="inputInduk" class="col-4 col-lg-3 col-form-label">Induk</label>
              <div class="col-8 col-lg-9">
                <select id="inputInduk" class="form-control" name="Induk">
                  <option value="" disabled selected> .:: Pilih Induk ::. </option>
                </select>
              </div>
            </div>
          </span>
          <div class="form-group row">
            <label for="inputJudul" class="col-4 col-lg-3 col-form-label">Hanya Judul</label>
            <div class="col-8 col-lg-9 row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="Judul" value="1" class="custom-control-input" onclick="judulYa()" @if($kegiatan != '') @if($kegiatan->is_title == '1') checked @endif @else checked @endif><span class="custom-control-label">Ya</span>
                </label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="custom-control custom-radio custom-control-inline mb-0">
                  <input type="radio" name="Judul" value="0" class="custom-control-input" onclick="judulTidak()" @if($kegiatan != '') @if($kegiatan->is_title == '0') checked @endif @endif><span class="custom-control-label">Tidak</span>
                </label>
              </div>
            </div>
          </div>
          <span class="panel-pointButir" @if($kegiatan != '' && $kegiatan->is_title == 0) &nbsp @else style="display:none;" @endif>
            <div class="form-group row">
              <label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
              <div class="col-8 col-lg-9">
                <select id="inputpJabatan" class="form-control" name="Jabatan">
                  <option value="" disabled selected> .:: Pilih Jabatan ::. </option>
                  @foreach ($jabatan as $jbt)
                    <option value="{{ $jbt->id_master }}" @if($kegiatan != '') @if($kegiatan->jabatan_id == $jbt->id_master) selected @endif @endif>{{ $jbt->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputJml" class="col-4 col-lg-3 col-form-label">Jumlah Minimal</label>
              <div class="col-8 col-lg-9">
                <input id="inputJml" type="number" name="Minimal" value="@if($kegiatan != ''){{ $kegiatan->jum_min }}@endif" placeholder="Jumlah Minimal Kegiatan" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputJml" class="col-4 col-lg-3 col-form-label">Satuan</label>
              <div class="col-8 col-lg-9 row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Ijazah" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Ijazah') checked @endif @endif><span class="custom-control-label">Ijazah</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Sertifikat" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Sertifikat') checked @endif @endif><span class="custom-control-label">Sertifikat</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Pasien" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Pasien') checked @endif @endif><span class="custom-control-label">Pasien</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Laporan" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Laporan') checked @endif @endif><span class="custom-control-label">Laporan</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Kasus" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Kasus') checked @endif @endif><span class="custom-control-label">Kasus</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Jenazah" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Jenazah') checked @endif @endif><span class="custom-control-label">Jenazah</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Kegiatan" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Kegiatan') checked @endif @endif><span class="custom-control-label">Kegiatan</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Jam" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Jam') checked @endif @endif><span class="custom-control-label">Jam</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Tahun" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Tahun') checked @endif @endif><span class="custom-control-label">Tahun</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Buku" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Buku') checked @endif @endif><span class="custom-control-label">Buku</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Naskah" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Naskah') checked @endif @endif><span class="custom-control-label">Naskah</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Makalah" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Makalah') checked @endif @endif><span class="custom-control-label">Makalah</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Jam Pelajaran" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Jam Pelajaran') checked @endif @endif><span class="custom-control-label">Jam Pelajaran</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Ijazah/Gelar" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Ijazah/Gelar') checked @endif @endif><span class="custom-control-label">Ijazah/Gelar</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Gelar" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Gelar') checked @endif @endif><span class="custom-control-label">Gelar</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline mb-0">
                    <input type="radio" name="Satuan" value="Penghargaan" class="custom-control-input" @if($kegiatan != '') @if($kegiatan->satuan == 'Penghargaan') checked @endif @endif><span class="custom-control-label">Penghargaan</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPoint" class="col-4 col-lg-3 col-form-label">Jumlah Point</label>
              <div class="col-8 col-lg-9">
                <input id="inputPoint" type="number" name="Point" value="@if($kegiatan != ''){{ $kegiatan->points }}@endif" placeholder="Jumlah Point" class="form-control">
              </div>
            </div>
          </span>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelButir"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submitButir">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelButir').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.second-page').fadeOut(function(){
      $('.second-page').empty();
      $('.panel-detail').fadeIn();
    });
  });

  $('#inputUnsur').chosen();
  $('#inputpJabatan').chosen();
  $('#inputInduk').chosen();
  function turunanYa() { $('.panel-parents').show(); getInduk(); }
  function turunanTidak() { $('.panel-parents').hide(); }
  function judulYa() { $('.panel-pointButir').hide(); }
  function judulTidak() { $('.panel-pointButir').show(); }

  $('#inputUnsur').change(function(){
    getInduk();
  });

  @if ($kegiatan != "")
    getInduk();
  @endif

  function getInduk() {
    var unsur = $('#inputUnsur').val();
    if (unsur != null) {
      var radios = document.getElementsByName('Turunan');
    	var valTurunan = '';
    	for (var i = 0, length = radios.length; i < length; i++){
    		if (radios[i].checked){
    			valTurunan = radios[i].value;break;
    		}
    	}
      if (valTurunan == 1) {
        var idProfesi = {{ $profesi->id_master_type }};
        var tag = '';
        $.post("{!! route('getIndukButir') !!}", {unsur:unsur,idProfesi:idProfesi}).done(function(result){
          if (result.status == 'success') {
            console.log('jm : '+result.data.induk.length);
            if (result.data.induk.length > 0) {
              var tempNo1 = 0;
              $.each(result.data.induk, function(k,v1){
                tempNo1++;
                tag += '<option value="'+v1.id_pak_master+'">'+KonDecRomawi(tempNo1)+'. '+v1.butir_kegiatan+'</option>';
                if (v1.row.length > 0) {
                  var tempNo2 = 0; // Hurug Awal
                  var tempNo2n = 0; // Hurug Akhir
                  var No2 = '';
                  var No2n = '';
                  $.each(v1.row, function(k,v2){
                    if (tempNo2n == 26) {
                      tempNo2++;
                      No2 = String.fromCharCode(64 + tempNo2);
                    }
                    tempNo2n++;
                    No2n = String.fromCharCode(64 + tempNo2n);
                    tag += '<option value="'+v2.id_pak_master+'">'+No2+''+No2n+'. '+v2.butir_kegiatan+'</option>';
                    if (v2.row.length > 0) {
                      var tempNo3 = 0;
                      $.each(v2.row, function(k,v3){
                        tempNo3++;
                        tag += '<option value="'+v3.id_pak_master+'">'+tempNo3+'. '+v3.butir_kegiatan+'</option>';
                        if (v3.row.length > 0) {
                          var tempNo4 = 0; // Hurug Awal
                          var tempNo4n = 0; // Hurug Akhir
                          var No4 = '';
                          var No4n = '';
                          $.each(v3.row, function(k,v4){
                            if (tempNo4n == 26) {
                              tempNo4++;
                              No4 = String.fromCharCode(64 + tempNo4).toLowerCase();
                            }
                            tempNo4n++;
                            No4n = String.fromCharCode(64 + tempNo4n).toLowerCase();
                            tag += '<option value="'+v4.id_pak_master+'">'+No4+''+No4n+'. '+v4.butir_kegiatan+'</option>';
                            if (v4.row.length > 0) {
                              var No5 = '<i class="fas fa-angle-double-right"></i>';
                              $.each(v4.row, function(k,v5){
                                tag += '<option value="'+v5.id_pak_master+'">'+No5+'. '+v5.butir_kegiatan+'</option>';
                                if (v5.row.length > 0) {
                                  var No6 = '<i class="fas fa-terminal"></i>';
                                  $.each(v5.row, function(k,v6){
                                    tag += '<option value="'+v6.id_pak_master+'">'+No6+'. '+v6.butir_kegiatan+'</option>';
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }else{
              console.log(1);
              tag += '<option value="" disabled selected> .:: Pilih Induk ::. </option>';
            }
					}else{
            console.log(2);
						tag += '<option value="" disabled selected> .:: Pilih Induk ::. </option>';
					}
          console.log(tag);
          $('#inputInduk').html(tag);
          $('#inputInduk').trigger('chosen:updated');
				});
      }
    }else{
      swal('Whoops !', 'Unsur Harus Dipilih !!', 'error');
    }
  }

  $('.btn-submitButir').click(function(e){
    e.preventDefault();
    $('.btn-submitButir').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveButirKegiatan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.second-page').fadeOut(function(){
          $('.second-page').empty();
          $('.panel-detail').fadeIn();
          getDataButir();
        });
      } else if(data.status == 'error') {
        $('.btn-submitButir').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitButir').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitButir').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
