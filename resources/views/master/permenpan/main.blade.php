@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item">Master</li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->
    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="loading" align="center" style="display: none;">
        <img src="{!! url('assets/images/loading.gif') !!}" width="60%">
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 main-layer">
        <div class="card">
          <div class="card mb-3">
            <div class="card-body pt-2 pb-2 pl-5">
              <div class="row">
                <div class="form-group row">
                  <label for="typesProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
                  <div class="col-8 col-lg-9">
                    <select class="form-control" name="types_profesi" id="typesProfesi">
                      @foreach ($data['types'] as $typeProfesi)
                        <option value="{{ $typeProfesi->id_master_type }}">{{ $typeProfesi->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              
              <div class="col-sm-12 col-lg-8 mb-2 mb-sm-2">
                <button type="button" class="btn btn-xs btn-primary btn-add"><span class="fas fa-plus"></span> Tambah</button>
                <button type="button" class="btn btn-xs btn-warning btn-update"><span class="fas fa-pencil-alt"></span> Ubah</button>
                <button type="button" class="btn btn-xs btn-success btn-actives"><span class="fas fa-lock-open"></span> Aktifkan</button>
                <button type="button" class="btn btn-xs btn-danger btn-delete"><span class="fas fa-trash"></span> Hapus</button>
              </div>
              
              <div class="col-md-12 col-sm-12 col-xs-12 main-layer panelSearch m-b-10">
                  <div class="form-inline pos-absolute right-10" style="float:right">
                      <div class="form-group">
                          <select class="form-control-sm form-control input-s-sm inline v-middle option-search" id="search-option">
                          </select>
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control-sm form-control" placeholder="Search" id="search">
                      </div>
                  </div>
              </div>
              <div class='clearfix'></div>

              <div class="col-md-12 p-0">
                  <div class="table-responsive">
                      <table class="table table-striped b-t b-light" id="datagrid"></table>
                  </div>
                  <footer class="panel-footer">
                      <div class="row">
                          <div class="col-sm-1 hidden-xs">
                              <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                          </div>
                          <div class="col-sm-6 text-center">
                              <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                          </div>
                          <div class="col-sm-5 text-right text-center-xs">
                              <ul class="pagination pagination-sm justify-content-center float-r m-t-0 m-b-0" id="paging"></ul>
                          </div>
                      </div>
                  </footer>
              </div>
              <div class='clearfix'></div>

            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-12 other-page"></div>
      <div class="col-12 modal-dialog"></div>
    </div>
  </div>
@stop

@section('extended_js')
  <script src="{{ url('/')}}/js/cusLoadData.js"></script>
  <script type="text/javascript">
    var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('loadPermenpan') !!}",
      primaryField          : 'id_permenpan',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      queryParams             : {
          'id_master_type'    : $('#typesProfesi').val(),
      },
      columns               : [
        {field: 'checks', title: '&nbsp', editable: false, sortable: false, width: 30, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return checks(rowData, rowIndex);
          }
        },
        {field: 'no_permenpan', title: 'No Permenpan', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'tahun', title: 'Tahun', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'Status', title: 'Status', sortable: false, width: 50, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return Status(rowData, rowIndex);
          }
        }
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

    $("#typesProfesi").on("input", function(){
        datagrid.queryParams({
            'id_master_type'    : $('#typesProfesi').val()
        });
        datagrid.reload();
    });

    function Status(rowData, rowIndex) {
      var status = rowData.status;
      if (status == 'Aktif') {
        var tmp = "<span class='badge badge-success'><i class='fas fa-lock-open'></i> Aktif</span>";
      }else{
        var tmp = "<span class='badge badge-danger'><i class='fas fa-lock'></i> Tidak Aktif</span>";
      }
      return tmp;
    }

    function checks(rowData, rowIndex) {
      var id_permenpan = rowData.id_permenpan;
      var tag = '';
      tag += '<input type="checkbox" value="'+id_permenpan+'" name="id_data[]" id="id_data'+id_permenpan+'"/>';
      tag += '<label for="id_data'+id_permenpan+'"></label>';
      return tag;
    }

    $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      var id_profesi = $('#typesProfesi').val();
      $.post("{!! route('formPermenpan') !!}",{id_profesi:id_profesi}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    $('.btn-update').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      
      var id_profesi = $('#typesProfesi').val();      
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
        $.post("{!! route('formPermenpan') !!}",{id_permenpan:idData[0],id_profesi:id_profesi}).done(function(data){
          if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
          } else {
            $('.main-layer').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-delete').click(function(){
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin menghapus Data Ini?",
            text: "Data akan dihapus dari sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('removePermenpan') !!}", {id_permenpan:idData}).done(function(data){
              if(data.status == 'success'){
                datagrid.reload();
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

    $('.btn-actives').click(function(){
      $('.loading').show();
      $('.main-layer').hide();

      var id_profesi = $('#typesProfesi').val();      
      var idData = [];
      $(':checkbox:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length == 1) {
         swal(
          {
            title: "Apa anda yakin Meng-Aktifkan Permenpan Ini?",
            text: "Permenpan Akan Diaktifkan  ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('activePermenpan') !!}", {id_permenpan:idData,id_profesi:id_profesi}).done(function(data){
              if(data.status == 'success'){
                swal("Berhasil!", data.message, "success");                
                location.reload();
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });

  </script>
@stop
