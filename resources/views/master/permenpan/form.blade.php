<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($permenpan == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($permenpan != "")
        <input type="hidden" name="id_permenpan" value="{{ $permenpan->id_permenpan }}">
        <input type="hidden" name="id_profesi" value="{{ $id_profesi }}">              
      @else
        <input type="hidden" name="id_profesi" value="{{ $id_profesi }}">        
      @endif
      <div class="form-group row">
        <label for="inputGolongan" class="col-4 col-lg-3 col-form-label">No Permenpan <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputGolongan" type="text" name="no_permenpan" value="@if($permenpan != ''){{ $permenpan->no_permenpan }}@endif" placeholder="No Permenpan" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Tahun <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputUrutan" type="number" name="tahun" value="@if($permenpan != ''){{ $permenpan->tahun }}@endif" placeholder="Tahun" class="form-control">
        </div>
      </div>    
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('savePermenpan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
