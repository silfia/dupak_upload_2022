@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <div class="container-fluid dashboard-content">
    <!--  ..::: pageheader :::.. -->
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
          <h2 class="pageheader-title">{{ $data['title'] }}</h2>
          <div class="page-breadcrumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item">Master</li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data['title'] }}</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!--  ..::: end pageheader :::.. -->
    <form class="form-add">
      <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
              <h5 class="card-header">Kepala Dinas & Kepala Daerah</h5>
              <div class="card-body">
                @if ($data['settings'] != null)
                  <input type="hidden" name="id_setting" value="{{ $data['settings']->id_setting }}">
                @endif
                <div class="form-group row">
                  <label for="inputSebagai" class="col-4 col-lg-3 col-form-label">Sebagai</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputSebagai" type="text" name="Sebagai" value="@if($data['settings'] != null){!! $data['settings']->sebagai !!}@endif" placeholder="Isi jika bukan pejabat definitif" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputKeterangan" class="col-4 col-lg-3 col-form-label">Keterangan</label>
                  <div class="col-4 col-lg-9">
                    <input id="inputKeterangan" type="text" name="Keterangan" value="@if($data['settings'] != null){!! $data['settings']->keterangan !!}@endif" placeholder="Keterangan Tambahan" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputNamaKaDin" class="col-4 col-lg-3 col-form-label">Nama</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputNamaKaDin" type="text" name="Nama_Kepala" value="@if($data['settings'] != null){!! $data['settings']->nama_kepala !!}@endif" placeholder="Nama" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputJabatanKaDin" class="col-4 col-lg-3 col-form-label">Jabatan</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputJabatanKaDin" type="text" name="Jabatan_Kepala" value="@if($data['settings'] != null){!! $data['settings']->jabatan_kepala !!}@endif" placeholder="Jabatan" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputNIPKaDin" class="col-4 col-lg-3 col-form-label">NIP</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputNIPKaDin" type="text" name="NIP_Kepala" value="@if($data['settings'] != null){!! $data['settings']->nip_kepala !!}@endif" placeholder="Nomor Induk Pegawai" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-4 col-lg-3 col-form-label">Kepala Daerah</label>
                  <div class="col-8 col-lg-9">
                    <label class="custom-control custom-checkbox">
                        <input id="penggatiJabatan" name="penggati_jabatan" type="checkbox" value="1" class="custom-control-input" @if($data['settings'] != null) @if($data['settings']->pengganti_jabatan == 1) checked @endif @endif>
                        <span class="custom-control-label">Ya, saat ini adalah penggati jabatan</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card m-b-10px">
              <h5 class="card-header">Koordinator Telaah</h5>
              <div class="card-body">
                <div class="form-group row">
                  <label for="inputNamaKoor" class="col-4 col-lg-3 col-form-label">Nama</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputNamaKoor" type="text" name="Nama_Koordinator" value="@if($data['settings'] != null){!! $data['settings']->nama_koor !!}@endif" placeholder="Nama" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputJabatanKoor" class="col-4 col-lg-3 col-form-label">Jabatan</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputJabatanKoor" type="text" name="Jabatan_Koordinator" value="@if($data['settings'] != null){!! $data['settings']->jabatan_koor !!}@endif" placeholder="Jabatan" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputNIPKoor" class="col-4 col-lg-3 col-form-label">NIP</label>
                  <div class="col-8 col-lg-9">
                    <input id="inputNIPKoor" type="text" name="NIP_Koordinator" value="@if($data['settings'] != null){!! $data['settings']->nip_koor !!}@endif" placeholder="Nomor Induk Pegawai" class="form-control">
                  </div>
                </div>
              </div>
            </div>

            <div class="card">
              <h5 class="card-header">No SKPAK</h5>
              <div class="card-body">
                <div class="form-group row">
                  <div class="col-3">
                    <input id="inputKodeSkpak" type="text" name="Kode_SKPAK" value="@if($data['settings'] != null){!! $data['settings']->kode_skpak !!}@endif" placeholder="Kode SKPAK" class="form-control">
                  </div>
                  <div class="col-1">
                    /
                  </div>
                  <div class="col-2">
                    000
                  </div>
                  <div class="col-1">
                    /
                  </div>
                  <div class="col-3">
                    <input id="inputKodeDinas" type="text" name="Kode_Dinas" value="@if($data['settings'] != null){!! $data['settings']->kode_dinas !!}@endif" placeholder="Kode Dinas" class="form-control">
                  </div>
                  <div class="col-2">
                    / {{ date('Y') }}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success btn-sm btn-submit"><i class="fas fa-save"></i> Simpan</button>
          </div>
      </div>
    </form>
  </div>
@stop

@section('extended_js')
  <script type="text/javascript">
    $('.btn-submit').click(function(e){
      e.preventDefault();
      $('.btn-submit').html('Please wait...').attr('disabled', true);
      var data  = new FormData($('.form-add')[0]);
      $.ajax({
        url: "{{ route('saveSetting') }}",
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false
      }).done(function(data){
        $('.form-add').validate(data, 'has-error');
        if(data.status == 'success'){
          swal("Success !", data.message, "success");
          location.reload();
        } else if(data.status == 'error') {
          $('.btn-submit').html('<i class="fas fa-save"></i> Simpan').removeAttr('disabled');
          swal('Whoops !', data.message, 'warning');
        } else {
          var n = 0;
          for(key in data){
            if (n == 0) {var dt0 = key;}
            n++;
          }
          $('.btn-submit').html('<i class="fas fa-save"></i> Simpan').removeAttr('disabled');
          swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
        }
      }).fail(function() {
        swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
        $('.btn-submit').html('<i class="fas fa-save"></i> Simpan').removeAttr('disabled');
      });
    });
  </script>
@stop
