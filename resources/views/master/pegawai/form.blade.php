<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  <form class="form-save">
    <div class="row">
      @if ($pegawai != "")
        <input type="hidden" name="id_pegawai" value="{{ $pegawai->id_pegawai }}">
      @endif
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
          <h5 class="card-header">@if($pegawai != "") Perbaharui @endif Data Pegawai</h5>
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail" class="col-4 col-lg-3 col-form-label">Email</label>
              <div class="col-8 col-lg-9">
                <input id="inputEmail" type="text" name="Email" value="@if($pegawai != ''){{ $pegawai->email }}@endif" placeholder="Email" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama</label>
              <div class="col-8 col-lg-9">
                <input id="inputNama" type="text" name="Nama" value="@if($pegawai != ''){{ $pegawai->nama }}@endif" placeholder="Nama Pegawai" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
              <div class="col-8 col-lg-9 row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="Jenis_Kelamin" value="L" class="custom-control-input" @if($pegawai != '') @if($pegawai->jenis_kelamin == 'L') checked @endif @endif><span class="custom-control-label">Laki - laki</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="Jenis_Kelamin" value="W" class="custom-control-input" @if($pegawai != '') @if($pegawai->jenis_kelamin == 'P') checked @endif @endif><span class="custom-control-label">Perempuan</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputTempatLahir" class="col-4 col-lg-3 col-form-label">TTL</label>
              <div class="col-8 col-lg-9 row pr-0">
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                  <input id="inputTempatLahir" type="text" name="Tempat_Lahir" value="@if($pegawai != ''){{ $pegawai->tempat_lahir }}@endif" placeholder="Tempat Lahir" class="form-control">
                </div>
                <div class="input-group input-group-sm col-lg-5 col-md-5 col-sm-12 col-xs-12 pr-0">
                  <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                  <input id="inputTglLahir" type="text" name="Tanggal_Lahir" value="@if($pegawai != ''){{ date('d-m-Y', strtotime($pegawai->tanggal_lahir)) }}@endif" placeholder="Tgl Lahir" class="form-control" data-date-format="dd-mm-yyyy">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
              <div class="col-8 col-lg-9">
                <select id="inputProfesi" class="form-control" name="Profesi">
                  <option value="" disabled selected> .:: Pilih Profesi ::. </option>
                  @foreach ($profesi as $pf)
                    <option value="{{ $pf->id_master_type }}" @if($pegawai != '') @if($pf->id_master_type == $pegawai->tipe_profesi_id) selected @endif @endif>{{ $pf->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputSaKer" class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
              <div class="col-8 col-lg-9">
                <select id="inputSaKer" class="form-control" name="Satuan_Kerja">
                  <option value="" disabled selected> .:: Pilih Satuan Kerja ::. </option>
                  @foreach ($satKers as $satKer)
                    <option value="{{ $satKer->id_satuan_kerja }}" @if($pegawai != '') @if($satKer->id_satuan_kerja == $pegawai->satuan_kerja_id) selected @endif @endif>{{ $satKer->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputJenisKelamin" class="col-4 col-lg-3 col-form-label">Status Pegawai</label>
              <div class="col-8 col-lg-9 row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status_pegawai" value="PNS" onclick="formPns('PNS')" class="custom-control-input" @if($pegawai != '') @if($pegawai->status_pegawai == 'PNS') checked @endif @endif><span class="custom-control-label">PNS</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status_pegawai" value="Non-PNS" onclick="formPns('Non-PNS')" class="custom-control-input" @if($pegawai != '') @if($pegawai->status_pegawai == 'Non-PNS') checked @endif @endif><span class="custom-control-label">Non-PNS</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group row" id='panelPenilai' @if($pegawai != '') @if($pegawai->status_pegawai == 'Non-PNS') style="display:none;" @endif @endif>
              <label for="inputStatusPenilai" class="col-4 col-lg-3 col-form-label">Status Penilai</label>
              <div class="col-8 col-lg-9 row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status_penilai" value="Ya" class="custom-control-input" @if($pegawai != '') @if($pegawai->status_penilai == 'Ya') checked @endif @endif><span class="custom-control-label">Ya</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="status_penilai" value="Tidak" class="custom-control-input" @if($pegawai != '') @if($pegawai->status_penilai == 'Tidak') checked @endif @endif><span class="custom-control-label">Tidak</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputNip" class="col-4 col-lg-3 col-form-label" id='labelIdentitas'>@if($pegawai != '') @if($pegawai->status_pegawai == 'Non-PNS') No NIK @else No NIP @endif @else No NIP @endif</label>
              <div class="col-8 col-lg-9">
                <input id="inputNip" type="text" name="NIP" value="@if($pegawai != ''){{ $pegawai->no_nip }}@endif"  placeholder="@if($pegawai != '') @if($pegawai->status_pegawai == 'Non-PNS') No NIK @else No NIP @endif @else No NIP @endif" class="form-control">
              </div>
            </div>
            <div class="form-group row" id='panelKarpeg' @if($pegawai != '') @if($pegawai->status_pegawai == 'Non-PNS') style="display:none;" @endif @endif>
              <label for="inputKarPeng" class="col-4 col-lg-3 col-form-label">No Karpeg</label>
              <div class="col-8 col-lg-9">
                <input id="inputKarPeng" type="text" name="No_Karpeng" value="@if($pegawai != ''){{ $pegawai->no_karpeng }}@endif" placeholder="No Kartu Pegawai" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPendidikan" class="col-4 col-lg-3 col-form-label p-b-0 p-t-0">Pendidikan Terakhir<br>Sesuai SK</label>
              <div class="col-8 col-lg-9">
                <input id="inputPendidikan" type="text" name="Pendidikan" value="@if($pegawai != ''){{ $pegawai->pendidikan_terakhir }}@endif" placeholder="Pendidikan Terakhir Sesuai SK" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputThnLulus" class="col-4 col-lg-3 col-form-label">Tahun Lulus</label>
              <div class="col-8 col-lg-9">
                <input id="inputThnLulus" type="text" name="Tahun_Lulus" value="@if($pegawai != ''){{ $pegawai->pendidikan_tahun }}@endif" placeholder="Tahun Lulus" class="form-control">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id='panelJabFus' @if($pegawai != '') @if($pegawai->status_pegawai == 'Non-PNS') style="display:none;" @endif @endif>
        <div class="card">
          <h5 class="card-header">@if($pegawai != "") Perbaharui @endif Data Jabatan & Fungsi</h5>
          <div class="card-body">
            <div class="form-group row">
              <label for="inputGolongan" class="col-4 col-lg-3 col-form-label">Golongan</label>
              <div class="col-8 col-lg-9">
                <select id="inputGolongan" class="form-control" name="Golongan">
                  <option value="" disabled selected> .:: Pilih Golongan ::. </option>
                  @foreach ($golongan as $gln)
                    <option value="{{ $gln->id_master }}" @if($pegawai != '') @if($gln->id_master == $pegawai->golongan_id) selected @endif @endif>{{ $gln->nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputGolTmt" class="col-4 col-lg-3 col-form-label">Golongan TMT</label>
              <div class="input-group input-group-sm col-8 col-lg-9">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                <input id="inputGolTmt" type="text" name="Golongan_TMT" value="@if($pegawai != ''){{ date('d-m-Y', strtotime($pegawai->golongan_tmt)) }}@endif" placeholder="Golongan TMT" class="form-control" data-date-format="dd-mm-yyyy">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputJabatan" class="col-4 col-lg-3 col-form-label">Jabatan</label>
              <div class="col-8 col-lg-9">
                <select id="inputJabatan" class="form-control" name="Jabatan">
                  <option value="" disabled selected> .:: Pilih Jabatan ::. </option>
                  @if ($pegawai != '')
                    @foreach ($jabatan as $jb)
                      <option value="{{ $jb->id_master }}" @if($pegawai != '') @if($jb->id_master == $pegawai->jabatan_id) selected @endif @endif>{{ $jb->nama }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputJabTmt" class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
              <div class="input-group input-group-sm col-8 col-lg-9">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                <input id="inputJabTmt" type="text" name="Jabatan_TMT" value="@if($pegawai != ''){{ date('d-m-Y', strtotime($pegawai->jabatan_tmt)) }}@endif" placeholder="Jabatan TMT" class="form-control" data-date-format="dd-mm-yyyy">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
        <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  $('#inputProfesi').change(function(){
    var id= $('#inputProfesi').val();
    $.post("{!! route('getjabatan') !!}", {id:id}).done(function(data){
      var listData = '<option value="" disabled selected> .:: Pilih Jabatan ::. </option>';
      if(data.length > 0){
        $.each(data, function(k,v){
          listData += '<option value="'+v.id_master+'">'+v.nama+'</option>';
        });
      }
      $('#inputJabatan').html(listData);
      $('#inputJabatan').trigger('chosen:updated');
    });
  });

  function formPns(status) {
    console.log(status);
    if (status == 'PNS') {
      $('#labelIdentitas').html('No NIP');
      $('#inputNip').attr('placeholder','No NIP');
      $('#panelPenilai').show();
      $('#panelKarpeg').show();
      $('#panelJabFus').show();
    }else{
      $('#labelIdentitas').html('No NIK');
      $('#inputNip').attr('placeholder','No NIK');
      $('#panelPenilai').hide();
      $('#panelKarpeg').hide();
      $('#panelJabFus').hide();
    }
  }

  $('#inputTglLahir').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });
  $('#inputProfesi').chosen();
  $('#inputSaKer').chosen();
  $('#inputGolongan').chosen();
  $('#inputJabatan').chosen();
  $('#inputGolTmt').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });
  $('#inputJabTmt').datetimepicker({
      weekStart: 2,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('savePegawai') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
