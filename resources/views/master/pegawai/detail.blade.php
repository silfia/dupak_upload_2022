<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
{{-- <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail"> --}}
  {{-- <div class="card-body"> --}}
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
          <h5 class="card-header">Data Pegawai</h5>
          <div class="card-body">
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Email</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->email }}" class="form-control backWhite" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Nama</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->nama }}" class="form-control backWhite" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Jenis Kelamin</label>
              <div class="col-8 col-lg-9 row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" @if($pegawai->jenis_kelamin == 'L') checked @endif disabled><span class="custom-control-label">Laki - Laki</span>
                  </label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" @if($pegawai->jenis_kelamin == 'P') checked @endif disabled><span class="custom-control-label">Perempuan</span>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">TTL</label>
              <div class="col-8 col-lg-9 row pr-0">
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                  <input type="text" value="{{ $pegawai->tempat_lahir }}" class="form-control backWhite" readonly>
                </div>
                <div class="input-group input-group-sm col-lg-5 col-md-5 col-sm-12 col-xs-12 pr-0">
                  <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                  <input type="text" value="{{ date('d-m-Y', strtotime($pegawai->tanggal_lahir)) }}" class="form-control backWhite" readonly>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Profesi</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->typeProfesi->nama }}" class="form-control backWhite" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Satuan Kerja</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->satuanKerja->nama }}" class="form-control backWhite" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Status Pegawai</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->status_pegawai }}" class="form-control backWhite" readonly>
              </div>
            </div>
            @if ($pegawai->status_pegawai == 'PNS')
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Status Penilai</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $pegawai->status_penilai }}" class="form-control backWhite" readonly>
                </div>
              </div>
            @endif
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">@if($pegawai->status_pegawai == 'Non-PNS') No NIK @else No NIP @endif</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->no_nip }}" class="form-control backWhite" readonly>
              </div>
            </div>
            @if($pegawai->status_pegawai == 'PNS')
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">No Karpeg</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $pegawai->no_karpeng }}" class="form-control backWhite" readonly>
                </div>
              </div>
            @endif
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label p-b-0 p-t-0">Pendidikan Terakhir<br>Sesuai SK</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->pendidikan_terakhir }}" class="form-control backWhite" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4 col-lg-3 col-form-label">Tahun Lulus</label>
              <div class="col-8 col-lg-9">
                <input type="text" value="{{ $pegawai->pendidikan_tahun }}" class="form-control backWhite" readonly>
              </div>
            </div>
          </div>
        </div>
      </div>
      @if($pegawai->status_pegawai == 'PNS')
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="card">
            <h5 class="card-header">Data Jabatan & Fungsi</h5>
            <div class="card-body">
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Golongan</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $pegawai->golongan->nama }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Golongan TMT</label>
                <div class="input-group input-group-sm col-8 col-lg-9">
                  <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                  <input type="text" value="@if($pegawai->golongan_tmt != ''){{ date('d-m-Y', strtotime($pegawai->golongan_tmt)) }}@endif" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Jabatan</label>
                <div class="col-8 col-lg-9">
                  <input type="text" value="{{ $pegawai->jabatan->nama }}" class="form-control backWhite" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-4 col-lg-3 col-form-label">Jabatan TMT</label>
                <div class="input-group input-group-sm col-8 col-lg-9">
                  <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                  <input type="text" value="@if($pegawai->jabatan_tmt != ''){{ date('d-m-Y', strtotime($pegawai->jabatan_tmt)) }}@endif" class="form-control backWhite" readonly>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="col-sm-12 p-0">
      {{-- <button type="submit" class="btn btn-success btn-sm btn-submit"><i class="fas fa-save"></i> Simpan</button> --}}
      <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
    </div>
  {{-- </div> --}}
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });
</script>
