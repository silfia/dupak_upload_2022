<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-formStandar">
  <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  <div class="card-body">
    <form class="form-saveStandar">
      <input type="hidden" name="id_golongan" value="{{ $id_golongan }}" class="form-control">
      @if ($standar != '')
        <input type="hidden" name="id_standart" value="{{ $standar->id_standart }}" class="form-control">
      @endif
      <div class="form-group row">
        <label for="inputStj" class="col-4 col-lg-3 col-form-label">Nama Jabatan <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <select id="inputStj" class="form-control" name="mst_id">
            <option disabled selected> .:: Pilih Jabatan ::. </option>
            @foreach ($jabatan as $jbt)
              <option value="{{ $jbt->id_master }}" @if($standar != '') @if($jbt->id_master == $standar->mst_id) selected @endif @endif>{{ $jbt->nama }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Poin <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputGolongan" type="text" name="poin" value="@if($standar != ''){{ $standar->poin }}@endif" placeholder="Poin" class="form-control">
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelStandar"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submitStandar">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-formStandar').animateCss('bounceInUp');
  })();

  $('.btn-cancelStandar').click(function(e){
    e.preventDefault();
    $('.panel-formStandar').animateCss('bounceOutDown');
    $('.other-standar').fadeOut(function(){
      $('.other-standar').empty();
      $('.panel-detail').fadeIn();
    });
  });

  $('#inputStj').chosen();

  $('.btn-submitStandar').click(function(e){
    e.preventDefault();
    $('.btn-submitStandar').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-saveStandar')[0]);
    $.ajax({
      url: "{{ route('saveGolonganStandar') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-saveStandar').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        console.log('apanya ya??');
        $('.panel-formStandar').animateCss('bounceOutDown');
        $('.other-standar').fadeOut(function(){
          $('.other-standar').empty();
          $('.panel-detail').fadeIn();
          getDataStandar();
        });
      } else if(data.status == 'error') {
        $('.btn-submitStandar').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitStandar').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitStandar').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
