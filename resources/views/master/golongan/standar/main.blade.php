<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-detail">
      <div class="card">
        <h5 class="card-header">Standar Jabatan {{ $master->nama }}</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-lg-12 mb-2 mb-sm-2">
              <button type="button" class="btn btn-xs btn-primary btn-addST"><span class="fas fa-plus"></span> Tambah</button>
              <button type="button" class="btn btn-xs btn-warning btn-updateST"><span class="fas fa-pencil-alt"></span> Ubah</button>
              <button type="button" class="btn btn-xs btn-danger btn-deleteST"><span class="fas fa-trash"></span> Hapus</button>
              <button type="button" class="btn btn-xs btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table class="table table-striped table-bordered first">
                <thead>
                  <tr>
                    <td width="10px">&nbsp</td>
                    <td width="10px" align='center'>No</td>
                    <td>Nama</td>
                    <td>Point</td>
                  </tr>
                </thead>
                <tbody class='resultDataStandar'></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-12 other-standar"></div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
    getDataStandar();
    console.log('apa aja');
  })();

  $(document).ready(function() {
    // loadData(dtLoad, 1);
  });
  function searchData() { reloadData(1); }

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  $('.btn-addST').click(function(){
    $('.loading').show();
    $('.panel-detail').hide();
    var id = {{ $master->id_master }};
    $.post("{!! route('formGolonganStandar') !!}", {idGolongan:id}).done(function(data){
      $('.loading').hide();
      if(data.status == 'success'){
        $('.other-standar').html(data.content).fadeIn();
      } else {
        $('.panel-detail').show();
      }
    });
  });

  function getDataStandar() {
    $('.resultDataStandar').html('<tr><td colspan="4" align="center">Memuat data...</td></tr>');
    var id_golongan = {{ $master->id_master }};
    $.post("{!! route('loadGolonganStandar') !!}", {id_golongan:id_golongan}).done(function(data){
      $('.resultDataStandar').empty();
      var tag = '';
      if (data.status == 'success') {
        if (data.row.standar.length != 0) {
          var n1 = 1;
          $.each(data.row.standar, function(k,v){
            tag += '<tr>';
            tag += '<td align="center" class="pl-0 pr-1">';
            tag += '<label class="custom-control custom-checkbox m-0">';
            tag += '<input type="checkbox" name="id_dataST[]" id="checkData" value="'+v.id_standart+'" class="custom-control-input"><span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
            tag += '</label>';
            tag += '</td>';
            tag += '<td align="center">'+n1+'</td>';
            tag += '<td>'+v.nama+'</td>';
            tag += '<td>'+v.poin+'</td>';
            tag += '</tr>';
            n1++;
          });
        }else{
          tag += '<tr><td colspan="4" align="center">Tidak ada data ditemukan...</td></tr>';
        }
        $('.resultDataStandar').html(tag);
      }else{
        swal("MAAF !","Terjadi Kesalahan !!", "warning");
      }
    }).fail(function() {
      getDataStandar();
    });
  }
    $('.btn-updateST').click(function(){
      $('.loading').show();
      $('.panel-detail').hide();
      var idData = [];
      $('.resultDataStandar input:checked').each(function(i){
        idData[i] = $(this).val();
      });
      var id = {{ $master->id_master }};
      if (idData.length == 1) {
        $.post("{!! route('formGolonganStandar') !!}",{id:idData[0],idGolongan:id}).done(function(data){
          $('.loading').hide();
          if(data.status == 'success'){
            $('.other-standar').html(data.content).fadeIn();
          } else {
            $('.panel-detail').show();
          }
        });
      }else if (idData.length > 1) {
        swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
        $('.loading').hide();
        $('.panel-detail').show();
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.panel-detail').show();
      }
    });

    $('.btn-deleteST').click(function(){
      var idData = [];
      $('.resultDataStandar input:checked').each(function(i){
        idData[i] = $(this).val();
      });
      if (idData.length != 0) {
        swal(
          {
            title: "Apa anda yakin menghapus Data Ini?",
            text: "Data akan dihapus dari sistem!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Saya yakin!",
            cancelButtonText: "Batal!",
            closeOnConfirm: false
          },
          function(){
            $.post("{!! route('removeGolonganStandar') !!}", {id:idData}).done(function(data){
              if(data.status == 'success'){
                getDataStandar();
                swal("Berhasil!", data.message, "success");
              }else{
                swal('Whoops !', data.message, 'warning');
              }
            });
          }
        );
      }else{
        swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
        $('.loading').hide();
        $('.main-layer').show();
      }
    });
</script>
