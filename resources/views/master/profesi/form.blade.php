<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($type == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($type != "")
        <input type="hidden" name="id_master_type" value="{{ $type->id_master_type }}">
      @endif
      <div class="form-group row">
        <label for="inputGolongan" class="col-4 col-lg-3 col-form-label">Nama Profesi <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputGolongan" type="text" name="Nama" value="@if($type != ''){{ $type->nama }}@endif" placeholder="Nama Profesi" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Urutan <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <input id="inputUrutan" type="text" name="Urutan" value="@if($type != ''){{ $type->urutan }}@endif" placeholder="Urutan" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Keterangan <small style="color:red;">*</small></label>
        <div class="col-8 col-lg-9">
          <textarea id="inputUrutan" name="Keterangan" rows="2" placeholder="Keterangan" class="form-control">@if($type != ''){{ $type->keterangan }}@endif</textarea>
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submit">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
		$('.panel-form').animateCss('bounceOutDown');
		$('.other-page').fadeOut(function(){
			$('.other-page').empty();
			$('.main-layer').fadeIn();
		});
	});

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveProfesi') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          reloadData(1);
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
