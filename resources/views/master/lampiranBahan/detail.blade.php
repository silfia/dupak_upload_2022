<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-detail">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-detail">
      <div class="card">
        <h5 class="card-header">Lampiran Bahan</h5>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-lg-12 mb-2 mb-sm-2">
              <button type="button" class="btn btn-xs btn-primary btn-addLD"><span class="fas fa-plus"></span> Tambah</button>
              <button type="button" class="btn btn-xs btn-warning btn-updateLD"><span class="fas fa-pencil-alt"></span> Ubah</button>
              <button type="button" class="btn btn-xs btn-danger btn-deleteLD"><span class="fas fa-trash"></span> Hapus</button>
              <button type="button" class="btn btn-xs btn-secondary btn-cancel"><span class="fas fa-chevron-left"></span> Kembali</button>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <table class="table table-striped table-bordered first">
                <thead>
                  <tr>
                    <td width="10px">&nbsp</td>
                    <td width="10px" align='center'>No</td>
                    <td>Nama</td>
                  </tr>
                </thead>
                <tbody class='resultDataLampiran'></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-detail').animateCss('bounceInUp');
    getDataMasterLampiran();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('.panel-detail').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function getDataMasterLampiran() {
    $('.resultDataLampiran').html('<tr><td colspan="3" align="center">Memuat data...</td></tr>');
    var id_profesi = {{ $id_profesi }};
    $.post("{!! route('loadMasterLampiranBahan') !!}", {id_profesi:id_profesi}).done(function(data){
      $('.resultDataLampiran').empty();
      var tag = '';
      if (data.status == 'success') {
        tag += '<tr>';
        tag +='<td colspan="3">LAMPIRAN USUL / BAHAN YANG DINILAI</td>';
        tag += '</tr>';
        if (data.row.bahan.length != 0) {
          var n1 = 1;
          $.each(data.row.bahan, function(k,v){
            tag += '<tr>';
            tag += '<td align="center" class="pl-0 pr-1">';
            tag += '<label class="custom-control custom-checkbox m-0">';
            tag += '<input type="checkbox" name="id_dataLB[]" id="checkData" value="'+v.id_master_lb+'" class="custom-control-input"><span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
            tag += '</label>';
            tag += '</td>';
            tag += '<td align="center">'+n1+'</td>';
            tag += '<td>'+v.nama+'</td>';
            tag += '</tr>';
            n1++;
          });
        }else{
          tag += '<tr><td colspan="3" align="center">Tidak ada data ditemukan...</td></tr>';
        }
        tag += '<tr>';
        tag +='<td colspan="3">LAMPIRAN - LAMPIRAN DUPAK</td>';
        tag += '</tr>';
        if (data.row.dupak.length != 0) {
          var n2 = 1;
          $.each(data.row.dupak, function(k,d){
            tag += '<tr>';
            tag += '<td align="center" class="pl-0 pr-1">';
            tag += '<label class="custom-control custom-checkbox m-0">';
            tag += '<input type="checkbox" name="id_dataLB[]" id="checkData" value="'+d.id_master_lb+'" class="custom-control-input"><span class="custom-control-label" style="position:absolute;right:60%;">&nbsp</span>';
            tag += '</label>';
            tag += '</td>';
            tag += '<td align="center">'+n2+'</td>';
            tag += '<td>'+d.nama+'</td>';
            tag += '</tr>';
            n2++;
          });
        }else{
          tag += '<tr><td colspan="3" align="center">Tidak ada data ditemukan...</td></tr>';
        }
        $('.resultDataLampiran').html(tag);
      }else{
        swal("MAAF !","Terjadi Kesalahan !!", "warning");
      }
    }).fail(function() {
      getDataMasterLampiran();
    });
  }

  $('.btn-addLD').click(function(){
    $('.loading').show();
    $('.panel-detail').hide();
    var id_profesi = {{ $id_profesi }};
    $.post("{!! route('formMasterLampiranBahan') !!}", {id_profesi:id_profesi}).done(function(data){
      if(data.status == 'success'){
        $('.loading').hide();
        $('.second-page').html(data.content).fadeIn();
      } else {
        $('.panel-detail').show();
      }
    });
  });

  $('.btn-updateLD').click(function(){
    $('.loading').show();
    $('.panel-detail').hide();
    var idData = $(".resultDataLampiran input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length == 1) {
      var id_profesi = {{ $id_profesi }};
      $.post("{!! route('formMasterLampiranBahan') !!}",{id:idData[0],id_profesi:id_profesi}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.second-page').html(data.content).fadeIn();
        } else {
          $('.panel-detail').show();
        }
      });
    }else if (idData.length > 1) {
      swal("MAAF !","Hanya Boleh Memilih 1 Data !!", "warning");
      $('.loading').hide();
      $('.panel-detail').show();
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.panel-detail').show();
    }
  });

  $('.btn-deleteLD').click(function(){
    var idData = $(".resultDataLampiran input:checked").map(function(i,el){return el.value;}).get();
    if (idData.length != 0) {
      swal(
        {
          title: "Apa anda yakin menghapus Data Ini?",
          text: "Data akan dihapus dari sistem!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Saya yakin!",
          cancelButtonText: "Batal!",
          closeOnConfirm: false
        },
        function(){
          $.post("{!! route('removeMasterLampiranBahan') !!}", {id:idData}).done(function(data){
            if(data.status == 'success'){
              getDataMasterLampiran();
              swal("Berhasil!", data.message, "success");
            }else{
              swal('Whoops !', data.message, 'warning');
            }
          });
        }
      );
    }else{
      swal("MAAF !","Tidak Ada Data yang Dipilih !!", "warning");
      $('.loading').hide();
      $('.main-layer').show();
    }
  });
</script>
