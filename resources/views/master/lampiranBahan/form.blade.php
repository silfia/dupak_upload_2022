<div class="card col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-lg-3 offset-md-3 p-0 panel-form">
  @if($master == "")
    <h5 class="card-header bg-primary"><i class="fas fa-plus-square mr-2"></i> Form Tambah Data</h5>
  @else
    <h5 class="card-header bg-warning"><i class="fas fa-pencil-alt mr-2"></i> Form Perbaharui Data</h5>
  @endif
  <div class="card-body">
    <form class="form-save">
      @if ($master != "")
        <input type="hidden" name="id_master_lb" value="{{ $master->id_master_lb }}">
      @endif
      <div class="form-group row">
        <label for="inputProfesi" class="col-4 col-lg-3 col-form-label">Profesi</label>
        <div class="col-8 col-lg-9">
          <input id="inputProfesi" type="text" name="Profesi" value="{{ $profesi->nama }}" class="form-control backWhite" readonly>
          <input id="inputIDProfesi" type="hidden" name="id_profesi" value="{{ $profesi->id_master_type }}" class="form-control" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputJenis" class="col-4 col-lg-3 col-form-label">Jenis Lampiran</label>
        <div class="col-8 col-lg-9">
          <select id="inputJenis" class="form-control" name="Jenis">
            <option value="" disabled selected> .:: Pilih Jenis Lampiran ::. </option>
            <option value="lampiran_dinilai" @if($master != '') @if($master->jenis == 'lampiran_dinilai') selected @endif @endif>Lampiran Usul / Bahan yang Dinilai</option>
            <option value="lampiran_dupak" @if($master != '') @if($master->jenis == 'lampiran_dupak') selected @endif @endif>Lampiran - Lampiran DUPAK</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputNama" class="col-4 col-lg-3 col-form-label">Nama Lampiran</label>
        <div class="col-8 col-lg-9">
          <input id="inputNama" type="text" name="Nama" value="@if($master != ''){{ $master->nama }}@endif" placeholder="Nama Lampiran" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputKeterangan" class="col-4 col-lg-3 col-form-label">Keterangan</label>
        <div class="col-8 col-lg-9">
          <textarea id="inputKeterangan" name="Keterangan" rows="2" placeholder="Keterangan" class="form-control">@if($master != ''){{ $master->keterangan }}@endif</textarea>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputUrutan" class="col-4 col-lg-3 col-form-label">Urutan</label>
        <div class="col-8 col-lg-9">
          <input id="inputUrutan" type="text" name="Urutan" value="@if($master != ''){{ $master->urutan }}@endif" placeholder="Urutan" class="form-control">
        </div>
      </div>
      <div class="row pt-2 pt-sm-2 mt-1">
        <div class="col-sm-12 pl-0">
          <p class="text-right">
            <button class="btn btn-sm btn-space btn-secondary btn-cancelLB"><span class="fas fa-chevron-left"></span> Kembali</button>
            <button type="submit" class="btn btn-sm btn-space btn-primary btn-submitLB">Simpan <span class="fas fa-save"></span></button>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0 panel-form">
  <form class="form-save">
    <div class="row">
    </div>
  </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('.panel-form').animateCss('bounceInUp');
  })();

  $('.btn-cancelLB').click(function(e){
    e.preventDefault();
    $('.panel-form').animateCss('bounceOutDown');
    $('.second-page').fadeOut(function(){
      $('.second-page').empty();
      $('.panel-detail').fadeIn();
    });
  });

  $('#inputJenis').chosen();

  $('.btn-submitLB').click(function(e){
    e.preventDefault();
    $('.btn-submitLB').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('saveMasterLampiranBahan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.second-page').fadeOut(function(){
          $('.second-page').empty();
          $('.panel-detail').fadeIn();
          getDataMasterLampiran();
        });
      } else if(data.status == 'error') {
        $('.btn-submitLB').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submitLB').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submitLB').html('Simpan <span class="fas fa-save"></span>').removeAttr('disabled');
    });
  });
</script>
