<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKepalaSakerToPengajuanDupakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_dupak', function (Blueprint $table) {
          $table->integer('kepala_id')->nullable()->comment('foreign key dari tabel pegawai')->after('golongan_baru_tmt');
          $table->integer('golongan_kepala_id')->nullable()->after('kepala_id');
          $table->date('golongan_kepala_tmt')->nullable()->after('golongan_kepala_id');
          $table->integer('jabatan_kepala_id')->nullable()->after('golongan_kepala_tmt');
          $table->integer('satuan_kerja_kepala_id')->nullable()->after('jabatan_kepala_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengajuan_dupak', function (Blueprint $table) {
          $table->dropColumn('kepala_id');
          $table->dropColumn('golongan_kepala_id');
          $table->dropColumn('golongan_kepala_tmt');
          $table->dropColumn('jabatan_kepala_id');
          $table->dropColumn('satuan_kerja_kepala_id');
        });
    }
}
