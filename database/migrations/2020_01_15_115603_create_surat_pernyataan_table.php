<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratPernyataanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_pernyataan', function (Blueprint $table) {
            $table->bigIncrements('id_surat_pernyataan');
            $table->bigInteger('pengajuan_id')->comment('relation to pengajuan_dupak table');
            $table->bigInteger('unsur_pak_id')->nullable()->comment('relation to master_type table with modul SatuanUnsur');
            $table->string('nama_kepala')->nullable();
            $table->string('nip_kepala')->nullable();
            $table->string('pangkat_golongan_kepala')->nullable();
            $table->string('tmt_pangkat_kepala')->nullable();
            $table->string('jabatan_kepala')->nullable();
            $table->string('unit_kerja_kepala')->nullable();
            $table->string('nama_pegawai')->nullable();
            $table->string('nip_pemohon')->nullable();
            $table->string('pangkat_golongan_pemohon')->nullable();
            $table->string('tmt_pemohon')->nullable();
            $table->string('jabatan_pemohon')->nullable();
            $table->string('unit_kerja_pemohon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_pernyataan');
    }
}
