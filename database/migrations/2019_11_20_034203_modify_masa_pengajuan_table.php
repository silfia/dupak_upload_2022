<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMasaPengajuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('masa_pengajuan', function (Blueprint $table) {
          $table->string('ac_conn',45)->nullable()->after('keterangan');
          $table->integer('creator_id')->nullable()->after('ac_conn');
          $table->string('status_masa',45)->nullable()->comment('Belum / Proses / Selesai')->after('bl_state');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('masa_pengajuan', function (Blueprint $table) {
        $table->dropColumn('ac_conn');
        $table->dropColumn('creator_id');
        $table->dropColumn('status_masa');
      });
    }
}
