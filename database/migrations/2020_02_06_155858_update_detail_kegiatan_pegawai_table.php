<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDetailKegiatanPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('detail_kegiatan_pegawai', function (Blueprint $table) {
          $table->string('jenis_kegiatan')->nullable()->after('tanggal')->comment('pasien / ijazah / sertifikat / laporan / kegiatan');
          $table->date('tgl_kegiatan')->nullable()->after('terapi');
          $table->string('waktu_kegiatan')->nullable()->after('tgl_kegiatan');
          $table->string('penyelenggara')->nullable()->after('waktu_kegiatan');
          $table->string('nomor')->nullable()->after('penyelenggara');
          $table->string('gambar')->nullable()->after('nomor');
          $table->text('berita_acara')->nullable()->after('gambar');
          $table->text('keterangan')->nullable()->after('berita_acara');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('detail_kegiatan_pegawai', function (Blueprint $table) {
        $table->dropColumn('jenis_kegiatan');
        $table->dropColumn('tgl_kegiatan');
        $table->dropColumn('waktu_kegiatan');
        $table->dropColumn('penyelenggara');
        $table->dropColumn('nomor');
        $table->dropColumn('gambar');
        $table->dropColumn('berita_acara');
        $table->dropColumn('keterangan');
      });
    }
}
