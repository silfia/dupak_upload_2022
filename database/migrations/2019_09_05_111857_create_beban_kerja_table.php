<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBebanKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beban_kerja', function (Blueprint $table) {
            $table->bigIncrements('id_beban_kerja');
            $table->bigInteger('penilaian_id')->nullable();
            $table->bigInteger('satuan_kerja_id')->nullable();
            $table->string('jenis')->nullable()->comment('harian, bulanan, tahunan');
            $table->integer('master_kegiatan_id')->nullable();
            $table->integer('jumlah_px_real')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('label_tanggal')->nullable();
            $table->string('label_bulan')->nullable();
            $table->string('label_tahun')->nullable();
            $table->string('tahun_ke')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beban_kerja');
    }
}
