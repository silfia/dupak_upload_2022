<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUraianSuratPernyataanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uraian_surat_pernyataan', function (Blueprint $table) {
            $table->bigIncrements('id_uraian_sp');
            $table->bigInteger('surat_pernyataan_id')->comment('relation to surat_pernyataan table');
            $table->bigInteger('master_kegiatan_id')->nullable()->comment('relation to pak_master_kegiatan table');
            $table->date('tanggal')->nullable();
            $table->string('satuan_hasil')->nullable();
            $table->double('jumlah_kegiatan')->nullable();
            $table->double('jumlah_ak')->nullable();
            $table->text('keterangan_fisik')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uraian_surat_pernyataan');
    }
}
