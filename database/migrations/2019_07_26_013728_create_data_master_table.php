<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_master', function (Blueprint $table) {
            $table->increments('id_master');
            $table->string('nama')->nullable();
            $table->smallInteger('urutan')->nullable();
            $table->bigInteger('tipe_profesi_id')->nullable()->comment('foreign key dari tabel data master Type Modul : StatusProfesi');
            $table->enum('is_golongan', ['Y', 'N'])->nullable();
            $table->string('ac_conn',45)->nullable();
            $table->integer('creator_id')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->string('cluster_id',45)->nullable();
            $table->smallInteger('bllokal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_master');
    }
}
