<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
        $table->increments('id');
        $table->string('username');
        $table->string('email');
        $table->string('password');
        $table->string('name');
        $table->string('gender')->nullable();
        $table->text('address')->nullable();
        $table->string('phone',45)->nullable();
        $table->string('photo')->nullable();
        $table->tinyInteger('level_user')->comment('1 => Admin');
        $table->tinyInteger('is_banned')->comment('0 => Active, 1 => Banned');
        $table->enum('is_leader', ['Y', 'N'])->nullable()->default('N');
        $table->enum('is_penilai', ['Y', 'N'])->nullable()->default('N');
        $table->integer('creator_id')->nullable();
        $table->char('bl_state',1)->nullable()->default('A');
        $table->dateTime('last_login')->nullable();
        $table->rememberToken();
        $table->timestamps();
      });

      DB::table('users')->insert([
          'username' => 'admin',
          'email' => 'admin@dinkessda.com',
          'password' => bcrypt('admin'),
          'name' => 'Admin',
          'gender' => 'Laki - Laki',
          'address' => 'Kmp. Laok Sungai Blega Bangkalan',
          'phone' => '081938512146',
          'level_user' => 1,
          'is_banned' => 0,
          'bl_state' => 'A',
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
