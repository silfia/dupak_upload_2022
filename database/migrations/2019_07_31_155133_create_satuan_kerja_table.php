<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSatuanKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satuan_kerja', function (Blueprint $table) {
            $table->bigIncrements('id_satuan_kerja');
            $table->bigInteger('parent_id')->nullable();
            $table->string('kode',50)->nullable();
            $table->string('nama')->nullable();
            $table->string('alamat')->nullable();
            $table->string('telepon',50)->nullable();
            $table->string('fax',50)->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('jenis_layanan')->nullable();
            $table->smallInteger('level')->nullable();
            $table->text('tree')->nullable();
            $table->smallInteger('bl_leaf')->nullable();
            $table->smallInteger('urutan')->nullable();
            $table->string('ac_conn',45)->nullable();
            $table->integer('creator_id')->nullable()->command('relasi ke tabel user. User Penginput');
            $table->timestamps();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->string('cluster_id',45)->nullable();
            $table->smallInteger('bllokal')->nullable();
            $table->double('batasan')->nullable();
            $table->bigInteger('tipe_satker_id')->nullable()->command('relasi ke tabel master type dengan modul : SatuanOrganisasi');
            $table->bigInteger('kelompok_pekerjaan_id')->nullable();
            $table->string('kelurahan')->nullable();
            $table->char('kelurahan_id',10)->nullable();
            $table->string('kecamatan')->nullable();
            $table->integer('kecamatan_id')->nullable();
            $table->string('kabupaten')->nullable();
            $table->integer('kabupaten_id')->nullable();
            $table->string('provinsi')->nullable();
            $table->integer('provinsi_id')->nullable();
            $table->string('penanggung_jawab_nama')->nullable();
            $table->string('penyelia_nama')->nullable();
            $table->string('penanggung_jawab_nip')->nullable();
            $table->string('penyelia_nip')->nullable();
            $table->double('luas_wilayah')->nullable();
            $table->double('jumlah_penduduk')->nullable();
            $table->double('jumlah_kk')->nullable();
            $table->double('jumlah_ibu_hamil')->nullable();
            $table->double('jumlah_ibu_melahirkan')->nullable();
            $table->double('jumlah_ibu_menyusui')->nullable();
            $table->double('jumlah_balita')->nullable();
            $table->integer('last_update_tahun')->nullable();
            $table->integer('last_update_bulan')->nullable();
            $table->integer('employee_aktif')->nullable();
            $table->integer('employee_all')->nullable();
            $table->integer('user_id')->nullable();
            $table->bigInteger('pimpinan_id')->nullable()->default('0')->command('relasi ke tabel pegawai');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satuan_kerja');
    }
}
