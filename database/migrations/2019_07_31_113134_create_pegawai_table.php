<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->bigIncrements('id_pegawai');
            $table->string('nama')->nullable();
            $table->string('no_nip')->nullable();
            $table->string('no_karpeng')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->char('jenis_kelamin',1)->nullable();
            $table->integer('golongan_id')->nullable();
            $table->date('golongan_tmt')->nullable();
            $table->integer('jabatan_id')->nullable();
            $table->date('jabatan_tmt')->nullable();
            $table->date('tanggal_pengangkatan')->nullable();
            $table->string('pendidikan_terakhir')->nullable();
            $table->integer('pendidikan_tahun')->nullable();
            $table->bigInteger('satuan_kerja_id')->nullable();
            $table->integer('bl_note')->nullable();
            $table->bigInteger('last_penilaian_id')->nullable();
            $table->bigInteger('tipe_profesi_id')->nullable()->comment('Relasi pada tabel master_type, modul => StatusProfesi');
            $table->string('ac_conn',45)->nullable();
            $table->integer('creator_id')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->string('cluster_id',45)->nullable();
            $table->smallInteger('bllokal')->nullable();
            $table->integer('masa_kerja')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
