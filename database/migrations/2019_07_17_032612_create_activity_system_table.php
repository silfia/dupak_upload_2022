<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitySystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_system', function (Blueprint $table) {
            $table->bigIncrements('id_activity');
            $table->timestamp('created_at')->nullable();
            $table->string('action_type',45)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('actor_id')->nullable();
            $table->string('created_name')->nullable();
            $table->string('ip_address')->nullable();
            $table->text('message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_system');
    }
}
