<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailKegiatanPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_kegiatan_pegawai', function (Blueprint $table) {
            $table->bigIncrements('id_detail_kegiatan');
            $table->bigInteger('penilaian_id')->nullable()->comment('foreign key dari tabel penilaian');
            $table->date('tanggal')->nullable();
            $table->string('nik')->nullable();
            $table->string('nama_pasien')->nullable();
            $table->string('inisial_pasien')->nullable();
            $table->string('no_reg')->nullable();
            $table->string('no_rm')->nullable();
            $table->string('no_bpjs')->nullable();
            $table->string('jenis_kelamin')->nullable()->comment('L : Laki - Laki; P : Perempuan');
            $table->double('umur')->nullable();
            $table->text('alamat')->nullable();
            $table->text('diagnosa')->nullable();
            $table->text('terapi')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->integer('creator_id')->nullable();
            $table->string('ac_conn',45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_kegiatan_pegawai');
    }
}
