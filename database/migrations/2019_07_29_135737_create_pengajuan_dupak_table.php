<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanDupakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_dupak', function (Blueprint $table) {
            $table->bigIncrements('id_pengajuan');
            $table->date('masa_pengajuan_id');
            $table->date('tgl_pengajuan')->nullable();
            $table->date('tgl_awal_penilaian')->nullable();
            $table->date('tgl_akhir_penilaian')->nullable();
            $table->string('status_pengajuan')->nullable();
            $table->date('tgl_respon')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('penilai_id')->nullable();
            $table->integer('golongan_penilai_id')->nullable();
            $table->integer('jabatan_penilai_id')->nullable();
            $table->integer('satuan_kerja_penilai_id')->nullable();
            $table->enum('status_penilai1', ['Y', 'B'])->nullable()->default('B');
            $table->integer('penilai2_id')->nullable();
            $table->integer('golongan_penilai2_id')->nullable();
            $table->integer('jabatan_penilai2_id')->nullable();
            $table->integer('satuan_kerja_penilai2_id')->nullable();
            $table->enum('status_penilai2', ['Y', 'B'])->nullable()->default('B');
            $table->enum('status_hasil', ['Naik', 'Tetap'])->nullable();
            $table->integer('jabatan_lama_id')->nullable();
            $table->integer('jabatan_baru_id')->nullable();
            $table->integer('golongan_lama_id')->nullable();
            $table->integer('golongan_baru_id')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_dupak');
    }
}
