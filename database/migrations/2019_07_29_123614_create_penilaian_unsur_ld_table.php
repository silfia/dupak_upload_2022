<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianUnsurLdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_unsur_ld', function (Blueprint $table) {
            $table->bigIncrements('id_penilaian_unsur');
            $table->integer('master_kegiatan_id')->nullable();
            $table->double('poin_pemohon')->nullable();
            $table->double('poin_penilai')->nullable();
            $table->text('keterangan')->nullable();
            $table->bigInteger('pengajuan_id')->nullable();
            $table->integer('penilai_id')->nullable()->comment('foreign key dari tabel user');
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_unsur_ld');
    }
}
