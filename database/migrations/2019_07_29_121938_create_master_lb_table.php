<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterLbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_lb', function (Blueprint $table) {
            $table->bigIncrements('id_master_lb');
            $table->string('jenis')->nullable()->comment('lampiran_dinilai, lampiran_dupak');
            $table->string('nama')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('formasi_id')->nullable()->comment('foreign key dari tabel master type StatusProfesi');
            $table->integer('urutan')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->integer('creator_id')->nullable();
            $table->string('ac_conn',45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_lb');
    }
}
