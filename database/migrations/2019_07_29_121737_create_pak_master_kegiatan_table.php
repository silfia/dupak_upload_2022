<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePakMasterKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pak_master_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id_pak_master');
            $table->tinyInteger('level')->nullable();
            $table->bigInteger('level_1')->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->tinyInteger('is_title')->nullable();
            $table->tinyInteger('is_child')->nullable();
            $table->text('butir_kegiatan')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('jum_min')->nullable();
            $table->double('points')->nullable();
            $table->bigInteger('formasi_id')->nullable()->comment('foreign key dari tabel master type StatusProfesi');
            $table->integer('jabatan_id')->nullable()->comment('foreign key dari tabel master data golongan Tidak');
            $table->bigInteger('unsur_pak_id')->nullable()->comment('foreign key dari tabel master type SatuanUnsur');
            $table->string('satuan_hasil')->nullable();
            $table->string('jenis')->nullable()->comment('unsur, butir');
            $table->char('bl_state',1)->nullable()->default('A');
            $table->integer('creator_id')->nullable();
            $table->string('ac_conn',45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pak_master_kegiatan');
    }
}
