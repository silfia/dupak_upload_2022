<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian', function (Blueprint $table) {
            $table->bigIncrements('id_penilaian');
            $table->string('jenis')->nullable()->comment('harian, bulanan, tahunan');
            $table->integer('master_kegiatan_id')->nullable();
            $table->integer('jumlah_px_pemohon')->nullable();
            $table->integer('jumlah_px_penilai')->nullable();
            $table->double('poin_pemohon')->nullable();
            $table->double('poin_penilai')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('label_tanggal')->nullable();
            $table->string('label_bulan')->nullable();
            $table->string('label_tahun')->nullable();
            $table->string('tahun_ke')->nullable();
            $table->integer('penilai_id')->nullable()->comment('foreign key dari tabel user');
            $table->enum('acc_status', ['Y', 'N', 'B'])->default('B')->nullable();
            $table->string('satuan_hasil')->nullable();
            $table->double('jumlah_volume')->nullable();
            $table->text('ket_bukti_fisik')->nullable();
            $table->bigInteger('pengajuan_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian');
    }
}
