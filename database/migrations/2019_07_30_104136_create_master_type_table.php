<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_type', function (Blueprint $table) {
            $table->bigIncrements('id_master_type');
            $table->string('modul')->nullable();
            $table->string('nama')->nullable();
            $table->text('keterangan')->nullable();
            $table->bigInteger('tipe_profesi_id')->nullable()->comment('untuk Modul Satuan Unsur, Relasi pada tabel master_type, modul => StatusProfesi');
            $table->integer('urutan')->nullable();
            $table->string('ac_conn',45)->nullable();
            $table->integer('creator_id')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->string('cluster_id',45)->nullable();
            $table->smallInteger('bllokal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_type');
    }
}
