<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandartMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standart_master', function (Blueprint $table) {
            $table->increments('id_standart');
            $table->integer('master_id')->nullable();
            $table->integer('mst_id')->nullable();
            $table->double('poin')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->integer('creator_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standart_master');
    }
}
