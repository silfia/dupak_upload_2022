<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimPenilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tim_penilai', function (Blueprint $table) {
            $table->bigIncrements('id_penilai');
            $table->string('nama')->nullable();
            $table->string('no_nip')->nullable();
            $table->bigInteger('satuan_kerja_id')->nullable();
            $table->integer('golongan_id')->nullable();
            $table->integer('jabatan_id')->nullable();
            $table->bigInteger('tipe_profesi_id')->nullable()->comment('Relasi pada tabel master_type, modul => StatusProfesi');
            $table->string('ac_conn',45)->nullable();
            $table->integer('creator_id')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tim_penilai');
    }
}
