<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataPakToPengajuanDupakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_dupak', function (Blueprint $table) {
            $table->date('jabatan_lama_tmt')->nullable()->after('jabatan_lama_id');
            $table->date('jabatan_baru_tmt')->nullable()->after('jabatan_baru_id');
            $table->date('golongan_lama_tmt')->nullable()->after('golongan_lama_id');
            $table->date('golongan_baru_tmt')->nullable()->after('golongan_baru_id');
            // $table->double('min_point_ak')->nullable()->after('golongan_baru_tmt');
            $table->string('pendidikan_telah_diperhitungkan')->nullable()->after('min_point_ak');
            $table->string('pendidikan_diusulkan')->nullable()->after('pendidikan_telah_diperhitungkan');
            $table->string('masa_kerja_lama')->nullable()->after('pendidikan_diusulkan');
            $table->string('masa_kerja_baru')->nullable()->after('masa_kerja_lama');
            $table->date('tgl_skpak')->nullable()->after('masa_kerja_baru');
            $table->string('no_skpak')->nullable()->after('masa_pengajuan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengajuan_dupak', function (Blueprint $table) {
            $table->dropColumn('jabatan_lama_tmt');
            $table->dropColumn('jabatan_baru_tmt');
            $table->dropColumn('golongan_lama_tmt');
            $table->dropColumn('golongan_baru_tmt');
            $table->dropColumn('min_point_ak');
            $table->dropColumn('pendidikan_telah_diperhitungkan');
            $table->dropColumn('pendidikan_diusulkan');
            $table->dropColumn('masa_kerja_lama');
            $table->dropColumn('masa_kerja_baru');
            $table->dropColumn('tgl_skpak');
            $table->dropColumn('no_skpak');
        });
    }
}
