<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilPenetapanAngkaKreditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_penetapan_angka_kredit', function (Blueprint $table) {
            $table->bigIncrements('id_pak');
            $table->bigInteger('pengajuan_id')->nullable();
            $table->bigInteger('unsur_id')->nullable();
            $table->double('nilai_lama')->nullable();
            $table->double('nilai_baru')->nullable();
            $table->double('jumlah_nilai')->nullable();
            $table->string('keterangan',45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_penetapan_angka_kredit');
    }
}
