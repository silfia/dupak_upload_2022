<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccKapusNPenilai2ToPenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penilaian', function (Blueprint $table) {
          $table->integer('kepala_saker_id')->nullable()->comment('foreign key dari tabel pegawai')->after('tahun_ke');
          $table->enum('acc_kepala_status', ['Y', 'N', 'B'])->nullable()->default('B')->after('kepala_saker_id');
          $table->bigInteger('satuan_kerja_kepala_id')->nullable()->after('acc_kepala_status');
          $table->date('tgl_acc_kepala')->nullable()->after('satuan_kerja_kepala_id');
          $table->date('tgl_acc_penilai')->nullable()->after('acc_status');
          $table->integer('penilai2_id')->nullable()->comment('foreign key dari tabel pegawai')->after('tgl_acc_penilai');
          $table->enum('acc_penilai2_status', ['Y', 'N', 'B'])->nullable()->default('B')->after('penilai2_id');
          $table->date('tgl_acc_penilai2')->nullable()->after('acc_penilai2_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penilaian', function (Blueprint $table) {
          $table->dropColumn('kepala_saker_id');
          $table->dropColumn('acc_kepala_status');
          $table->dropColumn('satuan_kerja_kepala_id');
          $table->dropColumn('tgl_acc_kepala');
          $table->dropColumn('tgl_acc_penilai');
          $table->dropColumn('penilai2_id');
          $table->dropColumn('acc_penilai2_status');
          $table->dropColumn('tgl_acc_penilai2');
        });
    }
}
