<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianPendidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_pendidikan', function (Blueprint $table) {
            $table->bigIncrements('id_penilaian_pendidikan');
            $table->string('jenis')->nullable()->comment('ijazah, sertifikat');
            $table->string('tingkat')->nullable()->comment('D3, S1, dll');
            $table->string('nama_penyedia')->nullable();
            $table->string('bukti_fisik')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('lama_pelaksaan')->nullable();
            $table->string('tahun_masuk')->nullable();
            $table->string('tahun_keluar')->nullable();
            $table->double('poin_pemohon')->nullable();
            $table->double('poin_penilai')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('label_tanggal')->nullable();
            $table->string('label_bulan')->nullable();
            $table->string('label_tahun')->nullable();
            $table->integer('penilai_id')->nullable()->comment('foreign key dari tabel user');
            $table->enum('acc_status', ['Y', 'N'])->nullable();
            $table->bigInteger('pengajuan_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->char('bl_state',1)->nullable()->default('A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_pendidikan');
    }
}
