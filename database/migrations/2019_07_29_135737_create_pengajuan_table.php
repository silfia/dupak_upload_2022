<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan', function (Blueprint $table) {
            $table->bigIncrements('id_pengajuan');
            $table->date('tanggal');
            $table->integer('user_id')->nullable();
            $table->integer('golongan_id')->nullable()->comment('foreign key dari tabel data master');
            $table->integer('jabatan_id')->nullable()->comment('foreign key dari tabel data master');
            $table->integer('masa_kerja_tahun_baru')->nullable();
            $table->integer('masa_kerja_bulan_baru')->nullable();
            $table->integer('masa_kerja_tahun_lama')->nullable();
            $table->integer('masa_kerja_bulan_lama')->nullable();
            $table->string('unit_kerja')->nullable();
            $table->double('pendidikan_diperhitungkan')->nullable();
            $table->text('catatan_tim_penilai')->nullable();
            $table->text('catatan_pejabat_penilai')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('penilai_id')->nullable()->comment('foreign key dari tabel user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan');
    }
}
