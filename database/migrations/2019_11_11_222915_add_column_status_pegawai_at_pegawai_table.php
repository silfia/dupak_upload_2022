<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusPegawaiAtPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pegawai', function (Blueprint $table) {
            $table->string('status_pegawai')->nullable()->comment('PNS / Non-PNS')->after('tipe_profesi_id');
            $table->string('status_penilai')->nullable()->comment('Ya / Tidak')->after('status_pegawai');
            $table->double('point_pegawai')->nullable()->comment('Point terakhir yang dimiliki')->after('masa_kerja');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pegawai', function (Blueprint $table) {
          $table->dropColumn('status_pegawai');
          $table->dropColumn('status_penilai');
          $table->dropColumn('point_pegawai');
        });
    }
}
