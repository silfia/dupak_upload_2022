<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoSkToDataSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_setting', function (Blueprint $table) {
            $table->string('kode_skpak',45)->nullable()->after('nip_koor');
            $table->string('kode_dinas',45)->nullable()->after('kode_skpak');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_setting', function (Blueprint $table) {
            $table->dropColumn('kode_skpak');
            $table->dropColumn('kode_dinas');
        });
    }
}
