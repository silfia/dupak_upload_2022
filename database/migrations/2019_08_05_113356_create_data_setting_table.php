<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_setting', function (Blueprint $table) {
            $table->bigIncrements('id_setting');
            $table->string('sebagai')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('nama_kepala')->nullable();
            $table->string('jabatan_kepala')->nullable();
            $table->string('nip_kepala')->nullable();
            $table->tinyInteger('pengganti_jabatan')->nullable();
            $table->string('nama_koor')->nullable();
            $table->string('jabatan_koor')->nullable();
            $table->string('nip_koor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_setting');
    }
}
