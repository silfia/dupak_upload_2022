-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table natusi_dupak_sda.activity_system
CREATE TABLE IF NOT EXISTS `activity_system` (
  `id_activity` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `action_type` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `created_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_activity`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.activity_system: ~15 rows (approximately)
DELETE FROM `activity_system`;
/*!40000 ALTER TABLE `activity_system` DISABLE KEYS */;
INSERT INTO `activity_system` (`id_activity`, `created_at`, `action_type`, `user_id`, `actor_id`, `created_name`, `ip_address`, `message`) VALUES
	(1, '2019-07-26 09:42:24', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(2, '2019-07-26 10:04:06', 'Login', 1, 1, 'Admin', '192.168.1.200', 'Melakukan Login Sistem'),
	(3, '2019-07-26 10:05:49', 'Insert', 1, 1, 'Admin', '127.0.0.1', 'Create Master Golongan Ia'),
	(4, '2019-07-26 10:06:39', 'Login', 1, 1, 'Admin', '192.168.1.38', 'Melakukan Login Sistem'),
	(5, '2019-07-26 10:15:13', 'Insert', 1, 1, 'Admin', '127.0.0.1', 'Create Master Golongan Ib'),
	(6, '2019-07-26 10:17:09', 'Insert', 1, 1, 'Admin', '127.0.0.1', 'Create Master Golongan Ic'),
	(7, '2019-07-26 10:27:39', 'Insert', 1, 1, 'Admin', '127.0.0.1', 'Create Master Golongan Id'),
	(8, '2019-07-26 10:28:08', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(9, '2019-07-26 10:29:05', 'Insert', 1, 1, 'Admin', '127.0.0.1', 'Create Master Golongan Ie'),
	(10, '2019-07-26 14:07:37', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(11, '2019-07-26 14:23:05', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(12, '2019-07-26 16:00:14', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(13, '2019-07-26 16:06:50', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(14, '2019-07-26 16:13:19', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem'),
	(15, '2019-07-29 07:15:18', 'Login', 1, 1, 'Admin', '127.0.0.1', 'Melakukan Login Sistem');
/*!40000 ALTER TABLE `activity_system` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.change_passwords
CREATE TABLE IF NOT EXISTS `change_passwords` (
  `id_change` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `old_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_change`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.change_passwords: ~0 rows (approximately)
DELETE FROM `change_passwords`;
/*!40000 ALTER TABLE `change_passwords` DISABLE KEYS */;
/*!40000 ALTER TABLE `change_passwords` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.data_master
CREATE TABLE IF NOT EXISTS `data_master` (
  `id_master` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` smallint(6) DEFAULT NULL,
  `tipe_profesi` smallint(6) DEFAULT NULL,
  `is_golongan` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ac_conn` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bl_state` char(1) COLLATE utf8mb4_unicode_ci DEFAULT 'A',
  `cluster_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bllokal` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_master`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.data_master: ~5 rows (approximately)
DELETE FROM `data_master`;
/*!40000 ALTER TABLE `data_master` DISABLE KEYS */;
INSERT INTO `data_master` (`id_master`, `nama`, `urutan`, `tipe_profesi`, `is_golongan`, `ac_conn`, `user_id`, `bl_state`, `cluster_id`, `bllokal`, `created_at`, `updated_at`) VALUES
	(1, 'Ia', 4, NULL, 'Y', '127.0.0.1', 1, 'A', 'LATIHAN1', 1, '2019-07-26 03:05:49', '2019-07-26 10:27:38'),
	(2, 'Ib', 1, NULL, 'Y', '127.0.0.1', 1, 'A', 'LATIHAN1', 1, '2019-07-26 03:15:12', '2019-07-26 03:15:12'),
	(3, 'Ic', 2, NULL, 'Y', '127.0.0.1', 1, 'A', 'LATIHAN1', 1, '2019-07-26 03:17:09', '2019-07-26 03:17:09'),
	(4, 'Id', 3, NULL, 'Y', '127.0.0.1', 1, 'A', 'LATIHAN1', 1, '2019-07-26 10:27:38', '2019-07-26 10:27:38'),
	(5, 'Ie', 5, NULL, 'Y', '127.0.0.1', 1, 'A', 'LATIHAN1', 1, '2019-07-26 10:29:05', '2019-07-26 10:29:05');
/*!40000 ALTER TABLE `data_master` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.master_lb
CREATE TABLE IF NOT EXISTS `master_lb` (
  `id_master_lb` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lampiran_dinilai, lampiran_dupak',
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formasi_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ac_conn` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_master_lb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.master_lb: ~0 rows (approximately)
DELETE FROM `master_lb`;
/*!40000 ALTER TABLE `master_lb` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_lb` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.migrations: ~9 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_07_17_030943_create_change_passwords_table', 1),
	(3, '2019_07_17_032612_create_activity_system_table', 1),
	(4, '2019_07_26_013728_create_data_master_table', 1),
	(5, '2019_07_29_121737_create_pak_master_kegiatan_table', 2),
	(6, '2019_07_29_121938_create_master_lb_table', 2),
	(7, '2019_07_29_123412_create_penilaian_table', 2),
	(8, '2019_07_29_123614_create_penilaian_unsur_ld_table', 2),
	(9, '2019_07_29_135737_create_pengajuan_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.pak_master_kegiatan
CREATE TABLE IF NOT EXISTS `pak_master_kegiatan` (
  `id_pak_master` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `level` tinyint(4) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `is_child` tinyint(4) DEFAULT NULL,
  `butir_kegiatan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formasi_id` int(11) DEFAULT NULL,
  `satuan_hasil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'unsur, butir',
  `user_id` int(11) DEFAULT NULL,
  `ac_conn` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pak_master`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.pak_master_kegiatan: ~0 rows (approximately)
DELETE FROM `pak_master_kegiatan`;
/*!40000 ALTER TABLE `pak_master_kegiatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pak_master_kegiatan` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.pengajuan
CREATE TABLE IF NOT EXISTS `pengajuan` (
  `id_pengajuan` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `golongan_id` int(11) DEFAULT NULL COMMENT 'foreign key dari tabel data master',
  `jabatan_id` int(11) DEFAULT NULL COMMENT 'foreign key dari tabel data master',
  `masa_kerja_tahun_baru` int(11) DEFAULT NULL,
  `masa_kerja_bulan_baru` int(11) DEFAULT NULL,
  `masa_kerja_tahun_lama` int(11) DEFAULT NULL,
  `masa_kerja_bulan_lama` int(11) DEFAULT NULL,
  `unit_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan_diperhitungkan` double DEFAULT NULL,
  `catatan_tim_penilai` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan_pejabat_penilai` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penilai_id` int(11) DEFAULT NULL COMMENT 'foreign key dari tabel user',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.pengajuan: ~0 rows (approximately)
DELETE FROM `pengajuan`;
/*!40000 ALTER TABLE `pengajuan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengajuan` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.penilaian
CREATE TABLE IF NOT EXISTS `penilaian` (
  `id_penilaian` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'harian, bulanan, tahunan',
  `master_kegiatan_id` int(11) DEFAULT NULL,
  `poin_pemohon` double DEFAULT NULL,
  `poin_penilai` double DEFAULT NULL,
  `tanggal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_tanggal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_bulan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_tahun` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_ke` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penilai_id` int(11) DEFAULT NULL COMMENT 'foreign key dari tabel user',
  `acc_status` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `satuan_hasil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_volume` double DEFAULT NULL,
  `ket_bukti_fisik` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengajuan_id` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_penilaian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.penilaian: ~0 rows (approximately)
DELETE FROM `penilaian`;
/*!40000 ALTER TABLE `penilaian` DISABLE KEYS */;
/*!40000 ALTER TABLE `penilaian` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.penilaian_unsur_ld
CREATE TABLE IF NOT EXISTS `penilaian_unsur_ld` (
  `id_penilaian_unsur` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `master_kegiatan_id` int(11) DEFAULT NULL,
  `poin_pemohon` double DEFAULT NULL,
  `poin_penilai` double DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengajuan_id` bigint(20) DEFAULT NULL,
  `penilai_id` int(11) DEFAULT NULL COMMENT 'foreign key dari tabel user',
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_penilaian_unsur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.penilaian_unsur_ld: ~0 rows (approximately)
DELETE FROM `penilaian_unsur_ld`;
/*!40000 ALTER TABLE `penilaian_unsur_ld` DISABLE KEYS */;
/*!40000 ALTER TABLE `penilaian_unsur_ld` ENABLE KEYS */;

-- Dumping structure for table natusi_dupak_sda.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_user` tinyint(4) NOT NULL COMMENT '1 => Admin',
  `is_banned` tinyint(4) NOT NULL COMMENT '0 => Active, 1 => Banned',
  `last_login` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table natusi_dupak_sda.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `password`, `name`, `gender`, `address`, `phone`, `photo`, `level_user`, `is_banned`, `last_login`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@dinkessda.com', '$2y$10$hgBnnme4VoMIG5PgfS/cgOU5IVa9x7WUFAolk3WRUrXAb3DZlDxZO', 'Admin', 'Laki - Laki', 'Kmp. Laok Sungai Blega Bangkalan', '081938512146', NULL, 1, 0, '2019-07-29 07:15:10', NULL, NULL, '2019-07-29 07:15:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
